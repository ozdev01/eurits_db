<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\gesanc_empresas;

class AutocompleteController extends Controller
{
    public function index(){

    	return view('autocomplete');

    }
    public function autocomplete(){

    	$queries = gesanc_empresas::where(function($query){

    		$term=Request::get('term');
    		$query->where('Empresa', 'like', '%'.$term.'%');
    	})->take(6)->get();

    	foreach ($queries as $query) {
    		$results[] = ['Empresa'=>$query->empresa];
    		return Response::json($results);
    	}

    }
}
