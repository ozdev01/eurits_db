<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\gesanc_sanciones;

use App\gesanc_lista_motivos_denuncia;

use App\gesanc_reduccion;

use App\gesanc_contencioso;

use App\gesanc_clientes;

use DateTime;

use Illuminate\Database\QueryException;

use App\gesanc_contratos;

use PhpOffice\PhpWord\TemplateProcessor;

use PHPExcel; 
use PHPExcel_IOFactory;

class ContactosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_its()
    {
            $contactos = DB::table('its_contactos')->orderBy('Empresa')->get();
            
            return view('contratos/contactos/contactosits',['contactos'=>$contactos]);
    }

     public function europa()
    {
           

             $contactos = DB::table(DB::raw("europa_contactos a, europa_provincias b"))->select(DB::raw("a.*, b.Provincia"))->whereRaw("a.codProvincia = b.codProvincia")->orderby("razonSocial","ASC")->limit(200)->get();

                 $empresas = DB::table("europa_contactos")->select("razonSocial","CodContacto")->get();

     
                $comerciales = DB::table("gesanc_comerciales")->selectRaw("CONCAT(Nombre,' ',Apellidos) as comercial,`Id-comercial` as ID")->whereRaw("`Id-comercial` in (select CodContacto from europa_contactos)")->get();

                $paises = DB::table("europa_paises")->orderBy("Pais","ASC")->get();

                $provincias = DB::table("europa_provincias")->orderBy("Provincia","ASC")->get();

                $funciones = DB::table("europa_tipo_funcion")->get();

            return view('contratos/europa/contactos/contactos',['contactos'=>$contactos,'empresas'=>$empresas,'comerciales'=>$comerciales,'paises'=>$paises,'provincias'=>$provincias,'funciones'=>$funciones]);
    }

    public function gesanc(){
      $contactos = DB::table("gesanc_clientes")->orderby("empresa","ASC")->limit(200)->get();

      $empresas = DB::table('gesanc_clientes')->whereRaw('cif IN (select cif from gesanc_sanciones)')->orderBy('empresa')->get();

      $comerciales = DB::table("gesanc_comerciales")->selectRaw("CONCAT(Nombre,' ',Apellidos) as comercial,`Id-comercial` as ID")->whereRaw("`Id-comercial` in (select CodContacto from europa_contactos)")->get();

      $provincias = DB::table("gesanc_provincias")->get();

      $paises = DB::table("gesanc_paises")->selectRaw("*,`Codigo Pais` as 'codigopais'")->get();

      return view('contratos/gesanc/contactos/contactos',['contactos'=>$contactos,'empresas'=>$empresas,'comerciales'=>$comerciales,'paises'=>$paises,'provincias'=>$provincias]);    }

    public function filtra_europa(Request $request){
            
        $output="<p style='text-align:center;'>No se han encontrado resultados.</p>";

            if($request->ajax()){

            $filtros = array();

            $consulta = "";

              

            if($request->codContacto != null){
                $contratante = $request->codContacto;
                $filtro_codContacto = "CodContacto = '".$codContacto."'";
                $filtros[] = $filtro_codContacto;
            }

             if($request->cif != null){
                $cif = $request->cif;
                $filtro_cif = "cif = '".$cif."'";
                $filtros[] = $filtro_cif;
            } 

             if($request->empresa != null){
                $empresa = $request->empresa;
                $filtro_empresa = "razonSocial = '".$empresa."'";
                $filtros[] = $filtro_empresa;
            } 

             if($request->direccion != null){
                $direccion = $request->direccion;
                $filtro_direccion = "descuento LIKE '%".$direccion."%'";
                $filtros[] = $filtro_direccion;
            }

             if($request->codpostal != null){
                $codpostal = $request->codpostal;
                $filtro_codpostal = "codPostal = '".$codpostal."'";
                $filtros[] = $filtro_codpostal;
            }

            if($request->aptocorreos != null){
                $aptocorreos = $request->aptocorreos;
                $filtro_aptocorreos = "apartadoCorreos = '".$aptocorreos."'";
                $filtros[] = $filtro_aptocorreos;
            }

              if($request->poblacion != null){
                $poblacion = $request->poblacion;
                $filtro_poblacion = "Poblacion = '".$poblacion."'";
                $filtros[] = $filtro_poblacion;
            }

              if($request->telefono != null){
                $telefono = $request->telefono;
                $filtro_telefono = "CodContacto in (select codContacto from europa_contactos_telefonos where codTelefono = '".$telefono."')";
                $filtros[] = $filtro_telefono;
            }

              if($request->email != null){
                $email = $request->email;
                $filtro_aptocorreos = "apartadoCorreos = '".$email."'";
                $filtros[] = $filtro_aptocorreos;
            }

              if($request->pais != null){
                $pais = $request->pais;
                $filtro_pais = "a.codPais = '".$pais."'";
                $filtros[] = $filtro_pais;
            }

             if($request->provincia != null){
                $provincia = $request->provincia;
                $filtro_provincia = "a.codProvincia = '".$provincia."'";
                $filtros[] = $filtro_provincia;
            }

              if($request->tipoContacto != null){
                $tipoContacto = $request->tipoContacto;
                $filtro_tipocontacto = "a.CodTipoFuncion = '".$tipoContacto."'";
                $filtros[] = $filtro_tipocontacto;
            }

             if($request->comercial != null){
                $comercial = $request->comercial;
                $filtro_comercial = "codComercial = '".$comercial."'";
                $filtros[] = $filtro_comercial;
            } 

              if($request->cuentabancaria != null){
                $cuentabancaria = $request->cuentabancaria;
                $filtro_banco = "CC = '".$cuentabancaria."'";
                $filtros[] = $filtro_banco;
            }


             if($request->otrocomercial != null){
                $otrocomercial = $request->otrocomercial;
                $filtro_otrocomercial = "cteDeOtroComercial = '".$otrocomercial."'";
                $filtros[] = $filtro_otrocomercial;
            }  


             if($request->sinprovision != null){
                $sinprovision = $request->sinprovision;
                $filtro_sinprovision = "sinProvisionFondos = '".$sinprovision."'";
                $filtros[] = $filtro_sinprovision;
            } 

            if($request->poremail != null){
                $poremail = $request->poremail;
                $filtro_poremail = "mailing = '".$poremail."'";
                $filtros[] = $filtro_poremail;
            }   

             if($request->conflictivo != null){
                $conflictivo = $request->conflictivo;
                $filtro_conflictivo = "cteConflictivo = '".$conflictivo."'";
                $filtros[] = $filtro_conflictivo;
            } 

              if($request->impagado != null){
                $impagado = $request->impagado;
                $filtro_impagado = "impagado_pdteDev = '".$impagado."'";
                $filtros[] = $filtro_impagado;
            }
         

            for ($i =count($filtros) - 1; $i >= 0 ; $i--) { 
                $consulta .= $filtros[$i];
                $consulta .= " ";

                if($i > 0){
                    $consulta .= "AND ";
                }
            }

            $contactos = DB::table(DB::raw("europa_contactos a, europa_provincias b"))->select(DB::raw("a.*, b.Provincia"))->whereRaw("a.codProvincia = b.codProvincia")->orderby("razonSocial","ASC")->limit(200)->get();



            
            if($consulta !=""){


               $contactos = DB::table(DB::raw("europa_contactos a, europa_provincias b"))->select(DB::raw("a.*, b.Provincia"))->whereRaw("a.codProvincia = b.codProvincia AND ".$consulta)->orderby("razonSocial","ASC")->limit(200)->get();

             }
            
           if($contactos){
                foreach ($contactos as $key => $contacto) {

                    $output.='<tr>'.
                             '<td>'.$contacto->razonSocial.'</td>';
                             if($contacto->impagado_pdteDev == 0){
                            $output.='<td>NO</td>';
                             }else{
                                $output.='<td>SI</td>';
                             }
                   $output.= '<td>'.$contacto->direccion.'</td>'.
                             '<td>'.$contacto->Poblacion.'</td>'.
                             '<td>'.$contacto->codPostal.'</td>'.
                             '<td>'.$contacto->Provincia.'</td>'.
                             '<td><a class="btn btn-sm btn-success" href="/contratos/europa/contactos/'.$contacto->CodContacto.'"><i class="fa fa-edit"></i> </a></td>'.
                             '</tr>';

                }

                
               
            }

            return Response($output);
             
    }
}

public function filtra_gesanc(Request $request){
            
        $output="<p style='text-align:center;'>No se han encontrado resultados.</p>";

            if($request->ajax()){

            $filtros = array();

            $consulta = "";

            if($request->codContacto != null){
                $contratante = $request->codContacto;
                $filtro_codContacto = "CodContacto = '".$codContacto."'";
                $filtros[] = $filtro_codContacto;
            }

             if($request->cif != null){
                $cif = $request->cif;
                $filtro_cif = "cif = '".$cif."'";
                $filtros[] = $filtro_cif;
            } 

             if($request->empresa != null){
                $empresa = $request->empresa;
                $filtro_empresa = "empresa = '".$empresa."'";
                $filtros[] = $filtro_empresa;
            } 

             if($request->direccion != null){
                $direccion = $request->direccion;
                $filtro_direccion = "descuento LIKE '%".$direccion."%'";
                $filtros[] = $filtro_direccion;
            }

             if($request->codpostal != null){
                $codpostal = $request->codpostal;
                $filtro_codpostal = "codPostal = '".$codpostal."'";
                $filtros[] = $filtro_codpostal;
            }

            if($request->aptocorreos != null){
                $aptocorreos = $request->aptocorreos;
                $filtro_aptocorreos = "apartadoCorreos = '".$aptocorreos."'";
                $filtros[] = $filtro_aptocorreos;
            }

              if($request->poblacion != null){
                $poblacion = $request->poblacion;
                $filtro_poblacion = "poblacion = '".$poblacion."'";
                $filtros[] = $filtro_poblacion;
            }

              if($request->telefono != null){
                $telefono = $request->telefono;
                $filtro_telefono = "CodContacto in (select codContacto from europa_contactos_telefonos where codTelefono = '".$telefono."')";
                $filtros[] = $filtro_telefono;
            }

              if($request->email != null){
                $email = $request->email;
                $filtro_aptocorreos = "apartadoCorreos = '".$email."'";
                $filtros[] = $filtro_aptocorreos;
            }

              if($request->pais != null){
                $pais = $request->pais;
                $filtro_pais = "pais = '".$pais."'";
                $filtros[] = $filtro_pais;
            }

             if($request->provincia != null){
                $provincia = $request->provincia;
                $filtro_provincia = "a.codProvincia = '".$provincia."'";
                $filtros[] = $filtro_provincia;
            }

         

             if($request->comercial != null){
                $comercial = $request->comercial;
                $filtro_comercial = "`Id-comercial` = '".$comercial."'";
                $filtros[] = $filtro_comercial;
            } 

              if($request->cuentabancaria != null){
                $cuentabancaria = $request->cuentabancaria;
                $filtro_banco = "CC = '".$cuentabancaria."'";
                $filtros[] = $filtro_banco;
            }


             if($request->mandato != null){
                $mandato = $request->mandato;
                $filtro_otrocomercial = "mandato = '".$mandato."'";
                $filtros[] = $filtro_otrocomercial;
            }  


             if($request->sinprovision != null){
                $sinprovision = $request->sinprovision;
                $filtro_sinprovision = "sinProvisionFondos = '".$sinprovision."'";
                $filtros[] = $filtro_sinprovision;
            } 

            if($request->poremail != null){
                $poremail = $request->poremail;
                $filtro_poremail = "Mailing = '".$poremail."'";
                $filtros[] = $filtro_poremail;
            }   

             if($request->conflictivo != null){
                $conflictivo = $request->conflictivo;
                $filtro_conflictivo = "cteConflictivo = '".$conflictivo."'";
                $filtros[] = $filtro_conflictivo;
            } 

              if($request->impagado != null){
                $impagado = $request->impagado;
                $filtro_impagado = "impagado_pdteDev = '".$impagado."'";
                $filtros[] = $filtro_impagado;
            }
         

            for ($i =count($filtros) - 1; $i >= 0 ; $i--) { 
                $consulta .= $filtros[$i];
                $consulta .= " ";

                if($i > 0){
                    $consulta .= "AND ";
                }
            }

            $contactos = DB::table("gesanc_clientes")->orderby("empresa","ASC")->limit(200)->get();



            
            if($consulta !=""){


                $contactos = DB::table("gesanc_clientes")->whereRaw($consulta)->orderby("empresa","ASC")->limit(200)->get();

             }
            
           if($contactos){
                foreach ($contactos as $key => $contacto) {

                    $output.='<tr>'.
                             '<td>'.$contacto->cif.'</td>';
                   $output.= '<td>'.$contacto->empresa.'</td>'.
                             '<td>'.$contacto->PWD.'</td>'.
                             '<td>'.$contacto->direccion.'</td>'.
                             '<td>'.$contacto->poblacion.'</td>'.
                             '<td>'.$contacto->cp.'</td>'.
                             '<td><a class="btn btn-sm btn-success" href="/contratos/gesanc/contactos/'.$contacto->CODCLI.'"><i class="fa fa-edit"></i> </a></td>'.
                             '</tr>';

                }

                
               
            }

            return Response($output);
             
    }
}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    public function create_its()
    {
         $provincias = DB::table('its_provincias')->orderBy('Provincia')->get();
         $paises = DB::table('its_paises')->orderBy('Pais')->get();

         return view('contratos/contactos/nuevo',['provincias'=>$provincias,'paises'=>$paises]);

    }

    public function create_europa(){


                $paises = DB::table("europa_paises")->orderBy("Pais","ASC")->get();

                $provincias = DB::table("europa_provincias")->orderBy("Provincia","ASC")->get();

            
            return view('contratos/europa/contactos/nuevo',['paises'=>$paises,'provincias'=>$provincias]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

      public function store_its(Request $request)
    {
         //TODO
        if($request->ajax()){

            $datos = array();
            parse_str($request->datos,$datos);

            $cif= null;
            $empresa = null;
            $nombre = null;
            $apellidos= null;
            $siglas = null;
            $direccion = null;
            $cp = null;
            $poblacion = null;
            //$provincia = null;
            $pais = null;
            $web = null;
            $banco = null;
            $telefono = null;
            $fax = null;
            $movil = null;
            $cc = null;
            $email = null;
            $observaciones = null;

            }

            if($datos["cif"]!=null){
                $cif = $datos["cif"];
            }

            if($datos["empresa"]!=null){
                $empresa = $datos["empresa"];
            }

            if($datos["nombre"]!=null){
                $nombre = $datos["nombre"];
            }

             if($datos["apellidos"]!=null){
                $apellidos = $datos["apellidos"];
            }

             if($datos["siglas"]!=null){
                $siglas = $datos["siglas"];
            }

             if($datos["direccion"]!=null){
                $direccion = $datos["direccion"];
            }

             if($datos["cp"]!=null){
                $cp = $datos["cp"];
            }

             if($datos["poblacion"]!=null){
                $poblacion = $datos["poblacion"];
            }

             /*if($datos["provincia"]!=null){
                $provincia = $datos["provincia"];
            }*/

             if($datos["pais"]!=null){
                $pais = $datos["pais"];
            }

             if($datos["web"]!=null){
                $web = $datos["web"];
            }

             if($datos["banco"]!=null){
                $banco = $datos["banco"];
            }

             if($datos["telefono"]!=null){
                $telefono = $datos["telefono"];
            }

             if($datos["fax"]!=null){
                $fax = $datos["fax"];
            }

             if($datos["movil"]!=null){
                $movil = $datos["movil"];
            }

             if($datos["cc"]!=null){
                $cc = $datos["cc"];
            }

            if($datos["email"]!=null){
                $email = $datos["email"];
            }

            if($datos["observaciones"]!=null){
                $observaciones = $datos["observaciones"];
            }


            $id_pais = null;
            $paises = DB::table('its_paises')->whereRaw("Pais ='".$pais."'")->orderBy('Pais')->get();

            
              if($datos["pais"]!=null){
                foreach ($paises as $key => $pais) {
                $id_pais = $pais->Codais;
            }
            }

           


            try { 
              DB::table('its_contactos')->insertGetId(
                ['CIF' => $cif,'Empresa' => $empresa,'Nombre'=>$nombre, 'Apellidos' => $apellidos, 'Siglas' => $siglas,'Direccion' => $direccion,'CP' => $cp,'poblacion' => $poblacion,'Pais' => $id_pais,'Web' => $web,'Banco' => $banco,'Tel' => $telefono,'fax' => $fax,'movil' => $movil,'CC' => $cc,'email' => $email,'Observaciones' => $observaciones,'FechaIntroDatos' => date("Y-m-d H:i:s"),'FechaModif'=>date("Y-m-d H:i:s")]
                );

            return  Response("El contacto se ha añadido correctamente");

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }

    }

     public function store_europa(Request $request)
    {
        if($request->ajax()){

            $datos = array();
            parse_str($request->datos,$datos);

            $cif= null;
            $empresa = null;
            $apellidos = null;
            $siglas = null;
            $direccion = null;
            $cp = null;
            $poblacion = null;
            $provincia = null;
            $pais = null;
            $apartadoCorreos = null;
            $web = null;
            $cc = null;
            $email = null;
            $observaciones = null;

            $cteDeOtroComercial = 0;
            $sinProvisionFondos = 0;
            $impagado_pdteDev = 0;
            $cteConflictivo = 0;
            $mailing = 0;

            }



            if(array_key_exists("cteDeOtroComercial",$datos)){
               $cteDeOtroComercial = 1;
            }

            if(array_key_exists("sinProvisionFondos",$datos)){
               $sinProvisionFondos = 1;
            }

            if(array_key_exists("impagado_pdteDev",$datos)){
               $impagado_pdteDev = 1;
            }

            if(array_key_exists("cteConflictivo",$datos)){
               $cteConflictivo = 1;
            }

            if(array_key_exists("mailing",$datos)){
               $mailing = 1;
            }

            if($datos["cif"]!=null){
                $cif = $datos["cif"];
            }

            if($datos["empresa"]!=null){
                $empresa = $datos["empresa"];
            }

             if($datos["apellidos"]!=null){
                $apellidos = $datos["apellidos"];
            }

            if($datos["siglas"]!=null){
                $siglas = $datos["siglas"];
            }


             if($datos["direccion"]!=null){
                $direccion = $datos["direccion"];
            }

             if($datos["cp"]!=null){
                $cp = $datos["cp"];
            }

            if($datos["apartadoCorreos"]!=null){
                $apartadoCorreos = $datos["apartadoCorreos"];
            }

             if($datos["poblacion"]!=null){
                $poblacion = $datos["poblacion"];
            }

             if($datos["provincia"]!=null){
                $provincia = $datos["provincia"];
            }

             if($datos["pais"]!=null){
                $pais = $datos["pais"];
            }

             if($datos["web"]!=null){
                $web = $datos["web"];
            }


             if($datos["cc"]!=null){
                $cc = $datos["cc"];
            }

            if($datos["email"]!=null){
                $email = $datos["email"];
            }

            if($datos["observaciones"]!=null){
                $observaciones = $datos["observaciones"];
            }


            try { 
                 DB::enableQueryLog();
        //dd(DB::getQueryLog());

                 DB::table('europa_contactos')->insertGetId(
                    ['cif'=>$cif, 'razonSocial'=>$empresa, 'apellidos'=>$apellidos, 'direccion'=>$direccion, 'apartadoCorreos'=>$apartadoCorreos, 'codPostal'=>$cp, 'Poblacion'=>$poblacion, 'codProvincia'=>$provincia, 'codPais'=>$pais, 'impagado_pdteDev'=>$impagado_pdteDev, 'cteConflictivo'=>$cteConflictivo, 'CC'=>$cc, 'email'=>$email, 'web'=>$web, 'siglas'=>$siglas, 'mailing'=>$mailing, 'observaciones'=>$observaciones,'fechaAlta'=>date('Y-m-d H:i:s'),'fechamodif'=>date('Y-m-d H:i:s'), 'cteDeOtroComercial'=>$cteDeOtroComercial, 'sinProvisionFondos'=>$sinProvisionFondos]);

                $contacto_creado = DB::table('europa_contactos')->orderby('fechaAlta', 'desc')->first();
             
              $CodContacto = $contrato_creado->CodContacto;
          

            return  Response($CodContacto);

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function show_its($id)
    {
        $contactos = DB::table('its_contactos')->whereRaw('CIF = "'.$id.'"')->get();


        $paises = DB::table('its_paises')->whereRaw("Codais = (select Pais from its_contactos where CIF ='".$id."')")->get();

        return view('contratos/contactos/detail',['contactos'=>$contactos,'paises'=>$paises]);
    }

    public function show_europa($id)
    {
                $contacto = DB::table(DB::raw("europa_contactos a, europa_provincias b"))->select(DB::raw("a.*, b.Provincia"))->whereRaw("a.codProvincia = b.codProvincia and CodContacto='".$id."'")->orderby("razonSocial","ASC")->limit(200)->first();

                $empresas = DB::table("europa_contactos")->select("razonSocial","CodContacto")->get();

                $comerciales = DB::table("gesanc_comerciales")->selectRaw("CONCAT(Nombre,' ',Apellidos) as comercial,`Id-comercial` as ID")->whereRaw("`Id-comercial` in (select CodContacto from europa_contactos)")->get();

                $paises = DB::table("europa_paises")->orderBy("Pais","ASC")->get();

                $provincias = DB::table("europa_provincias")->orderBy("Provincia","ASC")->get();

                

                $funciones = DB::table("europa_tipo_funcion")->get();

            return view('contratos/europa/contactos/detail',['contacto'=>$contacto,'empresas'=>$empresas,'comerciales'=>$comerciales,'paises'=>$paises,'provincias'=>$provincias,'funciones'=>$funciones]);
    }

     public function show_gesanc($id)
    {
      
      $contacto = DB::table("gesanc_clientes")->selectRaw("*,`Id-comercial` as 'id_comercial'")->where("CODCLI","=",$id)->first();


      $comerciales = DB::table("gesanc_comerciales")->selectRaw("CONCAT(Nombre,' ',Apellidos) as comercial,`Id-comercial` as ID")->whereRaw("`Id-comercial` in (select CodContacto from europa_contactos)")->get();

      $contratantes = DB::table("gesanc_empresas")->orderBy("Num_empresa",'ASC')->get();

      $provincias = DB::table("gesanc_provincias")->get();

      $paises = DB::table("gesanc_paises")->selectRaw("*,`Codigo Pais` as 'codigopais'")->get();

      return view('contratos/gesanc/contactos/detail',['contacto'=>$contacto,'comerciales'=>$comerciales,'paises'=>$paises,'provincias'=>$provincias,'contratantes'=>$contratantes]);
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function update_its(Request $request, $id)
    {
        //TODO
        if($request->ajax()){

            $datos = array();
            parse_str($request->datos,$datos);

            $cif= null;
            $empresa = null;
            $nombre = null;
            $apellidos= null;
            $siglas = null;
            $direccion = null;
            $cp = null;
            $poblacion = null;
            //$provincia = null;
            $pais = null;
            $web = null;
            $banco = null;
            $telefono = null;
            $fax = null;
            $movil = null;
            $cc = null;
            $email = null;
            $observaciones = null;

            }

            if($datos["cif"]!=null){
                $cif = $datos["cif"];
            }

            if($datos["empresa"]!=null){
                $empresa = $datos["empresa"];
            }

            if($datos["nombre"]!=null){
                $nombre = $datos["nombre"];
            }

             if($datos["apellidos"]!=null){
                $apellidos = $datos["apellidos"];
            }

             if($datos["siglas"]!=null){
                $siglas = $datos["siglas"];
            }

             if($datos["direccion"]!=null){
                $direccion = $datos["direccion"];
            }

             if($datos["cp"]!=null){
                $cp = $datos["cp"];
            }

             if($datos["poblacion"]!=null){
                $poblacion = $datos["poblacion"];
            }

             /*if($datos["provincia"]!=null){
                $provincia = $datos["provincia"];
            }*/

             if($datos["pais"]!=null){
                $pais = $datos["pais"];
            }

             if($datos["web"]!=null){
                $web = $datos["web"];
            }

             if($datos["banco"]!=null){
                $banco = $datos["banco"];
            }

             if($datos["telefono"]!=null){
                $telefono = $datos["telefono"];
            }

             if($datos["fax"]!=null){
                $fax = $datos["fax"];
            }

             if($datos["movil"]!=null){
                $movil = $datos["movil"];
            }

             if($datos["cc"]!=null){
                $cc = $datos["cc"];
            }

            if($datos["email"]!=null){
                $email = $datos["email"];
            }

            if($datos["observaciones"]!=null){
                $observaciones = $datos["observaciones"];
            }


            $id_pais = null;
            $paises = DB::table('its_paises')->whereRaw("Pais ='".$pais."'")->orderBy('Pais')->get();

            
              if($datos["pais"]!=null){
                foreach ($paises as $key => $pais) {
                $id_pais = $pais->Codais;
            }
            }

           


            try { 

                 DB::table('its_contactos')->where('CIF', $id)->update(
                    ['CIF' => $cif,'Empresa' => $empresa,'Nombre'=>$nombre, 'Apellidos' => $apellidos, 'Siglas' => $siglas,'Direccion' => $direccion,'CP' => $cp,'poblacion' => $poblacion,'Pais' => $id_pais,'Web' => $web,'Banco' => $banco,'Tel' => $telefono,'fax' => $fax,'movil' => $movil,'CC' => $cc,'email' => $email,'Observaciones' => $observaciones,'FechaModif'=>date("Y-m-d H:i:s")]);

            return  Response("El contacto se ha modificado correctamente");

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }

    }

    public function update_europa(Request $request, $id)
    {
        //TODO
        if($request->ajax()){

            $datos = array();
            parse_str($request->datos,$datos);

            $cif= null;
            $empresa = null;
            $apellidos = null;
            $siglas = null;
            $direccion = null;
            $cp = null;
            $poblacion = null;
            $provincia = null;
            $pais = null;
            $apartadoCorreos = null;
            $web = null;
            $cc = null;
            $email = null;
            $observaciones = null;

            $cteDeOtroComercial = 0;
            $sinProvisionFondos = 0;
            $impagado_pdteDev = 0;
            $cteConflictivo = 0;
            $mailing = 0;

            }



            if(array_key_exists("cteDeOtroComercial",$datos)){
               $cteDeOtroComercial = 1;
            }

            if(array_key_exists("sinProvisionFondos",$datos)){
               $sinProvisionFondos = 1;
            }

            if(array_key_exists("impagado_pdteDev",$datos)){
               $impagado_pdteDev = 1;
            }

            if(array_key_exists("cteConflictivo",$datos)){
               $cteConflictivo = 1;
            }

            if(array_key_exists("mailing",$datos)){
               $mailing = 1;
            }

            if($datos["cif"]!=null){
                $cif = $datos["cif"];
            }

            if($datos["empresa"]!=null){
                $empresa = $datos["empresa"];
            }

             if($datos["apellidos"]!=null){
                $apellidos = $datos["apellidos"];
            }

            if($datos["siglas"]!=null){
                $siglas = $datos["siglas"];
            }


             if($datos["direccion"]!=null){
                $direccion = $datos["direccion"];
            }

             if($datos["cp"]!=null){
                $cp = $datos["cp"];
            }

            if($datos["apartadoCorreos"]!=null){
                $apartadoCorreos = $datos["apartadoCorreos"];
            }

             if($datos["poblacion"]!=null){
                $poblacion = $datos["poblacion"];
            }

             if($datos["provincia"]!=null){
                $provincia = $datos["provincia"];
            }

             if($datos["pais"]!=null){
                $pais = $datos["pais"];
            }

             if($datos["web"]!=null){
                $web = $datos["web"];
            }


             if($datos["cc"]!=null){
                $cc = $datos["cc"];
            }

            if($datos["email"]!=null){
                $email = $datos["email"];
            }

            if($datos["observaciones"]!=null){
                $observaciones = $datos["observaciones"];
            }


            /*$id_pais = null;
            $paises = DB::table('its_paises')->whereRaw("Pais ='".$pais."'")->orderBy('Pais')->get();

            
              if($datos["pais"]!=null){
                foreach ($paises as $key => $pais) {
                $id_pais = $pais->Codais;
            }
            }*/

           


            try { 
                 DB::enableQueryLog();
        //dd(DB::getQueryLog());

                 DB::table('europa_contactos')->where('CodContacto', $id)->update(
                    ['cif'=>$cif, 'razonSocial'=>$empresa, 'apellidos'=>$apellidos, 'direccion'=>$direccion, 'apartadoCorreos'=>$apartadoCorreos, 'codPostal'=>$cp, 'Poblacion'=>$poblacion, 'codProvincia'=>$provincia, 'codPais'=>$pais, 'impagado_pdteDev'=>$impagado_pdteDev, 'cteConflictivo'=>$cteConflictivo, 'CC'=>$cc, 'email'=>$email, 'web'=>$web, 'siglas'=>$siglas, 'mailing'=>$mailing, 'observaciones'=>$observaciones,'fechamodif'=>date('Y-m-d H:i:s'), 'cteDeOtroComercial'=>$cteDeOtroComercial, 'sinProvisionFondos'=>$sinProvisionFondos]);

                //dd(DB::getQueryLog());

            return  Response("el Contacto se ha editado correctamente");

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }

    }

    public function update_gesanc(Request $request)
    {
        //TODO
        if($request->ajax()){

            $datos = array();
            parse_str($request->datos,$datos);

            $codcli = null;
            $cif= null;
            $empresa = null;
            $direccion = null;
            $cp = null;
            $poblacion = null;
            $provincia = null;
            $pais = null;
            $comercial = null;
            $contratante = null;
            $apartadoCorreos = null;
            $web = null;
            $cc = null;
            $email = null;
            $observaciones = null;
            $pwd = null;

            $tel1 = null;
            $tel2 = null;
            $movil1 = null;
            $movil2 = null;
            $fax1 = null;
            $fax2 = null;

            $mandato = 0;
            $impagado_pdteDev = 0;
            $cteConflictivo = 0;
            $mailing = 0;

            }



            if(array_key_exists("mandato",$datos)){
               $mandato = 1;
            }

            if(array_key_exists("impagado_pdteDev",$datos)){
               $impagado_pdteDev = 1;
            }

            if(array_key_exists("cteConflictivo",$datos)){
               $cteConflictivo = 1;
            }

            if(array_key_exists("mailing",$datos)){
               $mailing = 1;
            }

              if($datos["codcli"]!=null){
                $codcli = $datos["codcli"];
            }


            if($datos["cif"]!=null){
                $cif = $datos["cif"];
            }

            if($datos["empresa"]!=null){
                $empresa = $datos["empresa"];
            }


             if($datos["direccion"]!=null){
                $direccion = $datos["direccion"];
            }

             if($datos["cp"]!=null){
                $cp = $datos["cp"];
            }

             if($datos["pwd"]!=null){
                $pwd = $datos["pwd"];
            }

            if($datos["apartadoCorreos"]!=null){
                $apartadoCorreos = $datos["apartadoCorreos"];
            }

             if($datos["poblacion"]!=null){
                $poblacion = $datos["poblacion"];
            }

             if($datos["provincia"]!=null){
                $provincia = $datos["provincia"];
            }

             if($datos["pais"]!=null){
                $pais = $datos["pais"];
            }

               if($datos["contratante"]!=null){
                $contratante = $datos["contratante"];
            }

             if($datos["web"]!=null){
                $web = $datos["web"];
            }


            if($datos["comercial"]!=null){
                $comercial = $datos["comercial"];
            }

             if($datos["cc"]!=null){
                $cc = $datos["cc"];
            }

            if($datos["email"]!=null){
                $email = $datos["email"];
            }

            if($datos["tel1"]!=null){
                $tel1 = $datos["tel1"];
            }

            if($datos["tel2"]!=null){
                $tel2 = $datos["tel2"];
            }

            if($datos["movil1"]!=null){
                $movil1 = $datos["movil1"];
            }

            if($datos["movil2"]!=null){
                $movil2 = $datos["movil2"];
            }

            if($datos["fax1"]!=null){
                $fax1 = $datos["fax1"];
            }

            if($datos["fax2"]!=null){
                $fax2 = $datos["fax2"];
            }

            if($datos["observaciones"]!=null){
                $observaciones = $datos["observaciones"];
            }


            /*$id_pais = null;
            $paises = DB::table('its_paises')->whereRaw("Pais ='".$pais."'")->orderBy('Pais')->get();

            
              if($datos["pais"]!=null){
                foreach ($paises as $key => $pais) {
                $id_pais = $pais->Codais;
            }
            }*/

           


            try { 
                 DB::enableQueryLog();
        //dd(DB::getQueryLog());

                 DB::table('gesanc_clientes')->where('CODCLI', $codcli)->update(
                    ['cif'=>$cif, 'empresa'=>$empresa, 'direccion'=>$direccion, 'cp'=>$cp, 'poblacion'=>$poblacion, 'provincia'=>$provincia, 'pais'=>$pais, 'tel1'=>$tel1, 'tel2'=>$tel2, 'movil1'=>$movil1, 'movil2'=>$movil2, 'fax1'=>$fax1, 'fax2'=>$fax2, 'delegacion'=>$contratante, 'Mailing'=>$mailing, 'Observacion'=>$observaciones, 'email'=>$email, 'url'=>$web, 'Id-comercial'=>$comercial,'fechamodifacion'=>date('Y-m-d H:i:s'), 'mandato'=>$mandato,'PWD'=>$pwd,'impagado_pdteDev'=>$impagado_pdteDev, 'CC'=>$cc, 'cteConflictivo'=>$cteConflictivo, 'apartadoCorreos'=>$apartadoCorreos]);

                //dd(DB::getQueryLog());

            return  Response("el contacto se ha editado correctamente");

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }

     public function search_its(Request $request){

        if($request->ajax()){
            $output="<p style='text-align:center;'>No se han encontrado resultados.</p>";
             $contactos = DB::table('its_contactos')->where('Empresa','LIKE','%'.$request->search.'%')
                                ->orWhere('CIF','LIKE','%'.$request->search.'%')
                                ->orWhere('Nombre','LIKE','%'.$request->search.'%')
                                ->orWhere('Apellidos','LIKE','%'.$request->search.'%')
                                ->orWhere('Pais','LIKE','%'.$request->search.'%')
                                ->orWhere('Poblacion','LIKE','%'.$request->search.'%')
                                ->take(200)
                                ->orderBy('Empresa')
                                ->get();

            if($contactos){
                foreach ($contactos as $key => $contacto) {

                    $output.='<tr>'.
                             '<td>'.$contacto->Empresa.'</td>'.
                             '<td>'.$contacto->CIF.'</td>'.
                             '<td>'.$contacto->Nombre.'</td>'.
                             '<td>'.$contacto->Apellidos.'</td>'.
                             '<td>'.$contacto->Pais.'</td>'.
                             '<td>'.$contacto->Poblacion.'</td>'.
                             '<td><a class="btn btn-sm btn-success" href="sanciones/'.$contacto->CIF.'"><i class="fa fa-edit"></i> </a> <a class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a></td>'.
                             '</tr>';

                }
                return Response($output);
            }
        }
    }

     public function telefonos_its(Request $request)
    {

          if($request->ajax()){

            $cif = $request->cif; 

            
             $output = "<table class='table' id='table_telefonos'>
                        <thead class='table-header'>
                            <th>Telefono</th>                        
                            <th>Tipo</th>
                            <th>Contacto</th>
                            <th></th>
                            <th style='display:none'></th>
                        </thead>
                        <tbody id='myTable'>";
            $telefonos = DB::table(DB::raw("its_contactos_telefonos a, its_tipo_telefono b"))->select(DB::raw("a.*, b.codTipoTelefono,b.TipoTelefono"))->whereRaw("a.codTipoTelefono = b.codTipoTelefono and a.cif ='".$cif."'")->get();

            $tipo_telefono = DB::table("its_tipo_telefono")->get();


            if($telefonos->count()){

               

                foreach ($telefonos as $key => $telefono) {

                     $output.='<tr>'.
                              '<td class="ID">'.$telefono->CodTelefono.'</td>'.
                             '<td>'.$telefono->TipoTelefono.'</td>'.
                             '<td>'.$telefono->Contacto.'</td>'.
                             '<td><a class="btn btn-sm btn-danger" id="add_telefono"><i class="fa fa-trash"></i></a></td>'.
                             '</tr>';

                }


                
            }
            $output.='<tr>'.
                     '<td><input type="text" id="addtelefono" class="form-control" value=""></td>'.   
                     '<td><select class="selectpicker" data-container="body" data-live-search="true" title="buscar..." id="addtipo" name="addtipo">';
                      foreach ($tipo_telefono as $key => $tipo) {
                            $output.='<option>'.$tipo->TipoTelefono.'</option>';
                        }
            $output.='</select></td>';

            $output.='<td><input type="text" id="addcontacto" class="form-control" value=""></td>
            <td><a class="btn btn-success" id="add_telefono"><i class="fa fa-plus"></i></a></td>'.
                     '</tr>';


            $output .= "</tbody></table><input type='hidden' id='tabla_tipo' value='telefono'>";
            return Response($output);
        }
    }

    public function telefonos_europa(Request $request)
    {

          if($request->ajax()){

            $codContacto = $request->codContacto; 

            
             $output = "<table class='table' id='table_telefonos'>
                        <thead class='table-header'>
                            <th>Orden</th>
                            <th>Telefono</th>                        
                            <th>Tipo</th>
                            <th></th>
                            <th style='display:none'></th>
                        </thead>
                        <tbody id='myTable'>";
            $telefonos = DB::table(DB::raw("europa_contactos_telefonos a, europa_tipo_telefono b"))->select(DB::raw("a.*, b.codTipoTelefono,b.TipoTelefono"))->whereRaw("a.codTipoTelefono = b.codTipoTelefono and a.CodContacto ='".$codContacto."'")->get();

            $tipo_telefono = DB::table("its_tipo_telefono")->get();


            if($telefonos->count()){

            
                foreach ($telefonos as $key => $telefono) {

                     $output.='<tr>'.
                              '<td><input type="number" class="form-control orden" value="'.$telefono->numOrdenGesanc.'" disabled></td>'.
                             '<td class="ID"><input type="number"  class="form-control telefono" value="'.$telefono->CodTelefono.'"><input type="hidden"  class="form-control antiguo" value="'.$telefono->CodTelefono.'"></td>'.
                             '<td><select class="selectpicker tipo" data-container="body" data-live-search="true" title="buscar..." name="addtipo">';
                      foreach ($tipo_telefono as $key => $tipo) {
                            if($telefono->codTipoTelefono==$tipo->codTipoTelefono)
                            $output.='<option value="'.$tipo->codTipoTelefono.'" selected>'.$tipo->TipoTelefono.'</option>';
                        else
                            $output.='<option value="'.$tipo->codTipoTelefono.'">'.$tipo->TipoTelefono.'</option>';
                        }
            $output.='</select></td>'.
                             '<td><a class="btn btn-sm btn-success editar_telefono" href="#"><i class="fa fa-edit"></i></a> <a class="btn btn-sm btn-danger eliminar_telefono"><i class="fa fa-trash"></i></a></td>'.
                             '</tr>';

                }


                
            }
            $output.='<tr>'.
                     '<td></td>'.
                     '<td><input type="number" id="" class="form-control telefono" value=""></td>'.    
                     '<td><select class="selectpicker tipo" data-container="body" data-live-search="true" title="buscar..." id="addtipo" name="addtipo">';
                      foreach ($tipo_telefono as $key => $tipo) {
                            $output.='<option value="'.$tipo->codTipoTelefono.'">'.$tipo->TipoTelefono.'</option>';
                        }
            $output.='</select></td>';

            $output.='<td><a class="btn btn-success" id="add_telefono"><i class="fa fa-plus"></i></a></td>'.
                     '</tr>';


            $output .= "</tbody></table><input type='hidden' id='tabla_tipo' value='telefono'>";
            return Response($output);
        }
    }

     public function direcciones_its(Request $request)
    {

          if($request->ajax()){

            $cif = $request->cif; 

            
             $output = "<table class='table' id='table_telefonos'>
                        <thead class='table-header'>
                            <th style='display:none'>ID</th>   
                            <th>Dirección</th>                        
                            <th>Población</th>
                            <th>CP</th>
                            <th>Pais</th>
                            <th></th>
                        </thead>
                        <tbody id='myTable'>";
            $direcciones_base = DB::table(DB::raw("`its_bases vehiculos` a, its_paises b"))->select(DB::raw("a.*, b.Codais,b.Pais as nombrepais"))->whereRaw("a.Pais = b.Codais and a.CIF ='".$cif."'")->get();

            $paises = DB::table("its_paises")->get();


            if($direcciones_base->count()){

               

                foreach ($direcciones_base as $key => $direccion) {

                     $output.='<tr>'.
                             '<td class="ID" style="display:none">'.$direccion->Iddireccion.'</td>'.
                             '<td>'.$direccion->Direccion.'</td>'.
                             '<td>'.$direccion->Poblacion.'</td>'.
                             '<td>'.$direccion->CP.'</td>'.
                             '<td>'.$direccion->nombrepais.'</td>'.
                             '<td><a class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a></td>'.
                             '</tr>';

                }


                
            }
            $output.='<tr>'.
                     '<td style="display:none"></td>'.
                     '<td><input type="text" class="form-control" value="" id="adddireccion"></td>'.
                     '<td><input type="text" class="form-control" value="" id="addpoblacion" ></td>'.  
                     '<td><input type="text" class="form-control" value="" id?"addcp"></td>'.       
                     '<td><select class="selectpicker" data-container="body" data-live-search="true" title="buscar..." id="addpais" name="addpais">';
                      foreach ($paises as $key => $pais) {
                            $output.='<option>'.$pais->Pais.'</option>';
                        }
            $output.='</select></td>';

            $output.='<td><a class="btn btn-success" id="add_direccion"><i class="fa fa-plus"></i></a></td>'.
                     '</tr>';


            $output .= "</tbody></table><input type='hidden' id='tabla_tipo' value='direcciones_base'>";
            return Response($output);
        }
    }

     public function direcciones_europa(Request $request)
    {

          if($request->ajax()){

              $codContacto = $request->codContacto; 

            
             $output = "<table class='table' id='table_telefonos'>
                        <thead class='table-header'>
                            <th>Dirección</th>                        
                            <th>Cod.Postal</th>
                            <th>Apdo. Correo</th>
                            <th>Población</th>
                            <th>Provincia</th>
                            <th>Pais</th>
                            <th>Facturar</th>
                            <th>Activar</th>
                            <th></th>
                        </thead>
                        <tbody id='myTable'>";
            $direcciones_base = DB::table(DB::raw("europa_direcciones_alternativas a, europa_paises b,europa_provincias c "))->select(DB::raw("a.*, b.codPais,b.Pais as nombrepais,c.Provincia"))->whereRaw("a.codPais = b.codPais and a.codProvincia=c.codProvincia and a.CodContacto ='".$codContacto."'")->get();

            $paises = DB::table("europa_paises")->get();

            $provincias = DB::table("europa_provincias")->get();

            $contactos = DB::table("europa_contactos")->get();


            if($direcciones_base->count()){

               

                foreach ($direcciones_base as $key => $direccion) {

                     $output.='<tr>'.
                             '<td><input type="text" id="" class="form-control direccion" value="'.$direccion->direccion.'"><input type="hidden" id="" class="form-control orden" value="'.$direccion->numOrden.'"></td>'.
                             '<td><input type="number" id="" class="form-control codPostal" value="'.$direccion->codPostal.'"></td>'.
                             '<td><input type="number" id="" class="form-control apartadoCorreos" value="'.$direccion->apartadoCorreos.'"></td>'.
                             '<td><input type="text" id="" class="form-control poblacion" value="'.$direccion->Poblacion.'"></td>'.
                             '<td><select class="selectpicker provincia" data-container="body" data-live-search="true" title="buscar..." id="addtipo" name="addtipo">';
                foreach ($provincias as $key => $provincia) {
                            if($direccion->codProvincia==$provincia->codProvincia)
                            $output.='<option value="'.$provincia->codProvincia.'" selected>'.$provincia->Provincia.'</option>';
                        else
                            $output.='<option value="'.$provincia->codProvincia.'">'.$provincia->Provincia.'</option>';
                        }
            $output.='</select></td>'.
            '<td><select class="selectpicker pais" data-container="body" data-live-search="true" title="buscar..." id="addtipo" name="addtipo">';
             foreach ($paises as $key => $pais) {
                            if($direccion->codPais==$pais->codPais)
                            $output.='<option value="'.$pais->codPais.'" selected>'.$pais->Pais.'</option>';
                        else
                            $output.='<option value="'.$pais->codPais.'">'.$pais->Pais.'</option>';
                        }
            $output.='</select></td>'.
            '<td><select class="selectpicker contacto" data-container="body" data-live-search="true" title="buscar..." id="addtipo" name="addtipo">';
            foreach ($contactos as $key => $contacto) {
                            if($direccion->facturarA==$contacto->CodContacto)
                            $output.='<option value="'.$contacto->CodContacto.'" selected>'.$contacto->razonSocial.'</option>';
                        else
                            $output.='<option value="'.$contacto->CodContacto.'">'.$contacto->razonSocial.'</option>';
                        }
            $output.='</select></td>';
                            if($direccion->facturacion==1)
                             $output.= '<td><input type="checkbox" class="facturar" name="Conflictivo" value="" checked></td>';
                            else
                             $output.= '<td><input type="checkbox" class="facturar" name="Conflictivo" value=""></td>';

                             $output.='<td><a class="btn btn-sm btn-success editar_direccion" href="#"><i class="fa fa-edit"></i></a> <a class="btn btn-sm btn-danger eliminar_direccion"><i class="fa fa-trash"></i></a></td>'.
                             '</tr>';

                }

            }
            $output.='<tr>'.
                             '<td><input type="text" id="" class="form-control direccion" value=""></td>'.
                             '<td><input type="number" id="" class="form-control codPostal" value=""></td>'.
                             '<td><input type="number" id="" class="form-control apartadoCorreos" value=""></td>'.
                             '<td><input type="text" id="" class="form-control Poblacion" value=""></td>'.
                             '<td><select class="selectpicker provincia" data-container="body" data-live-search="true" title="buscar..." name="provincias">';
                foreach ($provincias as $key => $provincia) {
                            $output.='<option value="'.$provincia->codProvincia.'">'.$provincia->Provincia.'</option>';
                        }
            $output.='</select></td>'.
            '<td><select class="selectpicker pais" data-container="body" data-live-search="true" title="buscar..." id="addtipo" name="addtipo">';
             foreach ($paises as $key => $pais) {
                            $output.='<option value="'.$pais->codPais.'">'.$pais->Pais.'</option>';
                        }
            $output.='</select></td>'.
            '<td><select class="selectpicker contacto" data-container="body" data-live-search="true" title="buscar..." id="addtipo" name="addtipo">';
            foreach ($contactos as $key => $contacto) {
                            $output.='<option value="'.$contacto->CodContacto.'">'.$contacto->razonSocial.'</option>';
                        }
            $output.='</select></td>';
                             $output.= '<td><input type="checkbox" class="facturar" name="facturar" value=""></td>'.
                             '<td><a class="btn btn-success" id="guardar_direccion"><i class="fa fa-plus"></i></a></td>'.
                             '</tr>';


            $output .= "</tbody></table><input type='hidden' id='tabla_tipo' value='direcciones_base'>";
            return Response($output);
        }
    }

       public function funciones_its(Request $request)
    {

          if($request->ajax()){

            $cif = $request->cif; 

            
             $output = "<table class='table' id='table_telefonos'>
                        <thead class='table-header'>
                            <th>Categoria</th>                        
                            <th>Vinculación</th>
                            <th>ID</th>
                            <th></th>
                        </thead>
                        <tbody id='myTable'>";

            $funciones = DB::table(DB::raw("eurits_db.its_agenempafines a, eurits_db.its_calidad b"))->selectRaw("a.IdAgentDeleg as ID,b.Calidad AS Categoria,(select Empresa from eurits_db.its_contactos where CIF=a.CifSuperJerarq) as Vinculacion")->whereRaw("a.Clase = b.IdCalidad and a.CifDelAg='".$cif."'")->get();

            $categorias = DB::table("its_calidad")->get();

            $vinculaciones = DB::table("its_contactos")->select("Empresa")->get();

            if($funciones->count()){

               

                foreach ($funciones as $key => $funcion) {

                     $output.='<tr>';
                           
                        $output.='<td>'.$funcion->Categoria.'</td>'.
                             '<td>'.$funcion->Vinculacion.'</td>'.
                             '<td>'.$funcion->ID.'</td>'.
                             '<td><a class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a></td>'.
                             '</tr>';

                }


                
            }

           $output.='<tr>'.      
                     '<td><select class="selectpicker" data-container="body" data-live-search="true" title="buscar..." id="addcategoria" name="categoria">';
                      foreach ($categorias as $key => $categoria) {
                            $output.='<option>'.$categoria->Calidad.'</option>';
                        }
            $output.='</select></td>';
            $output.='<td><select class="selectpicker" data-container="body" data-live-search="true" title="buscar..." id="addvinculacion" name="vinculacion">';
                      foreach ($vinculaciones as $key => $vinculacion) {
                            $output.='<option>'.$vinculacion->Empresa.'</option>';
                        }
            $output.='</select></td>';

            $output.='<td></td>';

            $output.='<td><a class="btn btn-success" id="add_vinculacion"><i class="fa fa-plus"></i></a></td>'.
                     '</tr>';


            $output .= "</tbody></table><input type='hidden' id='tabla_tipo' value='funciones'>";
            return Response($output);
        }
    }

    public function guardar_europa(Request $request){

         if($request->ajax()){  

    $action = $request->action;
    $CodContacto = $request->CodContacto;
   
    $output = "<i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Se han encontrado errores.</strong> No se ha podido llevar a cabo la inserción de datos.Si el problema persiste, póngase en contacto con el administrador.";

    switch ($action) {
        case 1:

            $telefono = $request->telefono;
            $antiguo = $request->antiguo;
            $tipo = $request->tipo;
            $codTipoTelefono = null;
            $tiposTelefono = DB::table("europa_tipo_telefono")->Where('TipoTelefono',$tipo)->get();


            

            foreach ($tiposTelefono as $key => $tipoTelefono) {
              $codTipoTelefono = $tipoTelefono->codTipoTelefono;
            }

                  try { 


            //DB::enableQueryLog();
    
                DB::table('europa_contactos_telefonos')->where("CodTelefono","=",$antiguo)->update(
                array('CodTelefono' => $telefono,
                      'codTipoTelefono'=>$tipo));

                //dd(DB::getQueryLog());

             $output = "<a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a><i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Datos modificados correctamente.</strong>";

            return  Response($output);

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }

            break;

             case 2:

            $poblacion = $request->poblacion;
            $direccion = $request->direccion;
            $cp = $request->cp;
            $apartadoCorreos = $request->apartadoCorreos;
            $facturarA = $request->facturarA;
            $provincia = $request->provincia;
            $pais = $request->pais;
            $activar = $request->activar;
            $orden = $request->orden;


       
                  try { 


            //DB::enableQueryLog();
                DB::table("europa_direcciones_alternativas")->whereRaw("codContacto = '".$CodContacto."' AND numOrden='".$orden."'")->update(
                array('codContacto'=>$CodContacto, 'facturacion'=>$activar, 'direccion'=>$direccion, 'codPostal'=>$cp, 'apartadoCorreos'=>$apartadoCorreos, 'codProvincia'=>$provincia, 'Poblacion'=>$poblacion, 'codPais'=>$pais, 'facturarA'=>$facturarA));

            //dd(DB::getQueryLog());

            $output = "<a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a><i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Datos modificados correctamente.</strong>";
            
             return Response($output);

               return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }

        

            break;

            case 3:

            $categoria = $request->categoria;
            $vinculacion = $request->vinculacion;

            try { 

             DB::table("europa_contactos_funcion")->insert(
                array('CodContacto' => $CodContacto,
                      'CodSupJerarquico' => $vinculacion,
                      'codTipoFuncion'=>$categoria));

            $output = "<a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a><i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Datos insertados correctamente.</strong>";
            
             return Response($output);

               return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }
      

                

            break;

        default:
            # code...
            break;
            
    }

}

    }

     public function eliminar_europa(Request $request){

         if($request->ajax()){  

    $action = $request->action;
    $CodContacto = $request->CodContacto;
   
    $output = "<i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Se han encontrado errores.</strong> No se ha podido llevar a cabo la acción.Si el problema persiste, póngase en contacto con el administrador.";

    switch ($action) {
        case 1:

            $telefono = $request->telefono;
            $codTipoTelefono = $request->tipo;

                  try { 


          
    
                DB::table('europa_contactos_telefonos')->whereRaw("CodTelefono = '".$telefono."' AND CodContacto='".$CodContacto."'")->delete();

               $telefonos = DB::table('europa_contactos_telefonos')->where("codTipoTelefono","=",$codTipoTelefono)->get();

                 

                foreach ($telefonos as $key => $telefono) {

                    $orden = $telefono->numOrdenGesanc;

                    $orden--;
                    
                    DB::table("europa_contactos_telefonos")->whereRaw("CodTelefono = '".$telefono->CodTelefono."' and numOrdenGesanc > 1 ")->update(
                        ['numOrdenGesanc'=>$orden]);


                }

             

             $output = "<a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a><i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Datos eliminados correctamente.</strong>";

            return  Response($output);

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }

            break;

             case 2:

            $orden = $request->orden;


       
                  try { 


            //DB::enableQueryLog();
                DB::table("europa_direcciones_alternativas")->whereRaw("codContacto = '".$CodContacto."' AND numOrden='".$orden."'")->delete();

                   $direcciones = DB::table('europa_direcciones_alternativas')->where("codContacto","=",$CodContacto)->get();

                 

                foreach ($direcciones as $key => $direccion) {

                    $orden = $direccion->numOrden;

                    $orden--;
                    
                    DB::table("europa_direcciones_alternativas")->whereRaw("codContacto = '".$direccion->CodContacto."' and numOrden > 1 ")->update(
                        ['numOrdenGesanc'=>$orden]);
                }


            //dd(DB::getQueryLog());

            $output = "<a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a><i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Datos eliminados correctamente.</strong>";
            
             return Response($output);

               return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }

        

            break;

            case 3:

            $categoria = $request->categoria;
            $vinculacion = $request->vinculacion;

            try { 

             DB::table("europa_contactos_funcion")->insert(
                array('CodContacto' => $CodContacto,
                      'CodSupJerarquico' => $vinculacion,
                      'codTipoFuncion'=>$categoria));

            $output = "<a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a><i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Datos insertados correctamente.</strong>";
            
             return Response($output);

               return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }
      

                

            break;

        default:
            # code...
            break;
            
    }

}

    }

    

     public function funciones_europa(Request $request)
    {

          if($request->ajax()){

             $codContacto = $request->codContacto; 

            
             $output = "<table class='table' id='table_telefonos'>
                        <thead class='table-header'>
                            <th>Categoria</th>                        
                            <th>Vinculación</th>
                            <th>ID</th>
                            <th></th>
                        </thead>
                        <tbody id='myTable'>";

            $funciones = DB::table(DB::raw("eurits_db.europa_contactos_funcion a, eurits_db.europa_tipo_funcion b"))->selectRaw("a.CodContacto as ID,b.Funcion AS Categoria,(select razonSocial from eurits_db.europa_contactos where CodContacto=a.CodSupJerarquico) as Vinculacion")->whereRaw("a.codTipoFuncion = b.CodTipoFuncion and a.CodContacto='".$codContacto."'")->get();

            $categorias = DB::table("europa_tipo_funcion")->get();

            $vinculaciones = DB::table("europa_contactos")->get();

            if($funciones->count()){

               

                foreach ($funciones as $key => $funcion) {

                     $output.='<tr>';
                           
                        $output.='<td>'.$funcion->Categoria.'</td>'.
                             '<td>'.$funcion->Vinculacion.'</td>'.
                             '<td>'.$funcion->ID.'</td>'.
                             '<td><a class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a></td>'.
                             '</tr>';

                }

                
            }

           $output.='<tr>'.      
                     '<td><select class="selectpicker categoria" data-container="body" data-live-search="true" title="buscar..." id="addcategoria" name="categoria">';
                      foreach ($categorias as $key => $categoria) {
                            $output.='<option value="'.$categoria->CodTipoFuncion.'">'.$categoria->Funcion.'</option>';
                        }
            $output.='</select></td>';
            $output.='<td><select class="selectpicker vinculacion" data-container="body" data-live-search="true" title="buscar..." id="addvinculacion" name="vinculacion">';
                      foreach ($vinculaciones as $key => $vinculacion) {
                            $output.='<option value="'.$vinculacion->CodContacto.'">'.$vinculacion->razonSocial.'</option>';
                        }
            $output.='</select></td>';

            $output.='<td></td>';

            $output.='<td><a class="btn btn-success" id="add_vinculacion"><i class="fa fa-plus"></i></a></td>'.
                     '</tr>';


            $output .= "</tbody></table><input type='hidden' id='tabla_tipo' value='funciones'>";
            return Response($output);
        }
    }

      public function tarjetas_its(Request $request)
    {

          if($request->ajax()){

            $cif = $request->cif; 

            
             $output = "<table class='table' id='table_telefonos'>
                        <thead class='table-header'>
                            <th>Firmado</th>                        
                            <th>Entidad</th>
                            <th>Empresa</th>
                        </thead>
                        <tbody id='myTable'>";
            $tarjetas = DB::table("its_tarjetas")->where('cif',$cif)->get();

            if($tarjetas->count()){

               

                foreach ($tarjetas as $key => $tarjeta) {

                     $output.='<tr>';
                            if($tarjeta->docFirmado == 1)
                         $output.='<td>Si</td>';
                            else
                         $output.='<td>No</td>';
                           
                        $output.='<td>CAIXA</td>'.
                             '<td>'.$tarjeta->empCom.'</td>'.
                             '</tr>';

                }


                
            }

            $output .= "</tbody></table><input type='hidden' id='tabla_tipo' value='direcciones_base'>";
            return Response($output);
        }
    }

    public function insertar_its(Request $request)
    {

          if($request->ajax()){  

    $action = $request->action;
    $cif = $request->cif;
   
    $output = "<i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Se han encontrado errores.</strong> No se ha podido llevar a cabo la inserción de datos.Si el problema persiste, póngase en contacto con el administrador.";

    switch ($action) {
        case 1:

            $telefono = $request->telefono;
            $tipo = $request->tipo;
            $contacto = $request->contacto;
            $numeros = DB::table("its_contactos_telefonos")->selectRaw("max(numOrden) as max_num")->where('cif',$cif)->get();
            $orden = null;
            $codTipoTelefono = null;

            $tiposTelefono = DB::table("its_tipo_telefono")->Where('TipoTelefono',$tipo)->get();


            foreach ($numeros as $key => $numero) {
               if($numero !=null){
                $orden = $numero->max_num;
                $orden++;
               }else{
                $orden = 1;
               }
            }

            foreach ($tiposTelefono as $key => $tipoTelefono) {
              $codTipoTelefono = $tipoTelefono->codTipoTelefono;
            }
            


                 DB::table('its_contactos_telefonos')->insert(
                array('cif' => $cif,
                      'CodTelefono' => $telefono,
                      'numOrden'=>$orden,
                      'codTipoTelefono'=>$codTipoTelefono,
                      'Contacto'=>$contacto));

            $output = "<a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a><i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Datos insertados correctamente.</strong>";
            
             return Response($output);

            break;

             case 2:

            $poblacion = $request->poblacion;
            $direccion = $request->direccion;
            $cp = $request->cp;
            $pais = $request->pais;
            $paises = DB::table("its_paises")->where('Pais',$pais)->get();
            $codpais = null;

            foreach ($paises as $key => $dato) {
              $codpais = $dato->Codais;
            }
            


                 DB::table("its_bases vehiculos")->insert(
                array('CIF' => $cif,
                      'Direccion' => $direccion,
                      'Poblacion'=>$poblacion,
                      'CP'=>$cp,
                      'Pais'=>$codpais));

            $output = "<a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a><i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Datos insertados correctamente.</strong>";
            
             return Response($output);

            break;

            case 3:

            $categoria = $request->categoria;
            $vinculacion = $request->vinculacion;
            $clase = null;
            $cifempresa = null;

            $empresas = DB::table("its_contactos")->where('Empresa',$vinculacion)->get();

            $clases = DB::table("its_calidad")->where('Calidad',$categoria)->get();


            foreach ($empresas as $key => $empresa) {
              $cifempresa = $empresa->CIF;
            }

            foreach ($clases as $key => $dato) {
              $clase = $dato->IdCalidad;
            }
            


                 DB::table("its_agenempafines")->insert(
                array('CifDelAg' => $cif,
                      'CifSuperJerarq' => $cifempresa,
                      'Clase'=>$clase));

            $output = "<a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a><i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Datos insertados correctamente.</strong>";
            
             return Response($output);

            break;

        default:
            # code...
            break;
            
    }

}

    }

    public function insertar_europa(Request $request)
    {

          if($request->ajax()){  

    $action = $request->action;
    $CodContacto = $request->CodContacto;
   
    $output = "<i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Se han encontrado errores.</strong> No se ha podido llevar a cabo la inserción de datos.Si el problema persiste, póngase en contacto con el administrador.";

    switch ($action) {
        case 1:

            $telefono = $request->telefono;
            $tipo = $request->tipo;
            $orden = null;
            $codTipoTelefono = $request->tipo;
        

            $numeros = DB::table("europa_contactos_telefonos")->selectRaw("max(numOrdenGesanc) as max_num")->whereRaw('CodContacto ="'.$CodContacto.'" AND codTipoTelefono="'.$codTipoTelefono.'"')->get();
            foreach ($numeros as $key => $numero) {
               if($numero !=null){
                $orden = $numero->max_num;
                $orden++;
               }else{
                $orden = 1;
               }
            }

                  try { 


            //DB::enableQueryLog();
    
                DB::table('europa_contactos_telefonos')->insertGetId(
                array('CodContacto'=>$CodContacto,
                    'CodTelefono' => $telefono,
                      'numOrdenGesanc'=>$orden,
                      'codTipoTelefono'=>$codTipoTelefono));

                //dd(DB::getQueryLog());

             $output = "<a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a><i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Datos modificados correctamente.</strong>";

            return  Response($output);

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }

            break;

             case 2:

            $poblacion = $request->poblacion;
            $direccion = $request->direccion;
            $cp = $request->cp;
            $apartadoCorreos = $request->apartadoCorreos;
            $facturarA = $request->facturarA;
            $provincia = $request->provincia;
            $pais = $request->pais;
            $activar = $request->activar;
            $orden = null;

            $numeros = DB::table("europa_direcciones_alternativas")->selectRaw("max(numOrden) as max_num")->whereRaw('codContacto ="'.$CodContacto.'"')->get();

            foreach ($numeros as $key => $numero) {
               if($numero !=null){
                $orden = $numero->max_num;
                $orden++;
               }else{
                $orden = 1;
               }
            }

                  try { 


            //DB::enableQueryLog();
                DB::table("europa_direcciones_alternativas")->insertGetId(
                array('codContacto'=>$CodContacto, 'numOrden'=>$orden, 'facturacion'=>$activar, 'direccion'=>$direccion, 'codPostal'=>$cp, 'apartadoCorreos'=>$apartadoCorreos, 'codProvincia'=>$provincia, 'Poblacion'=>$poblacion, 'codPais'=>$pais, 'facturarA'=>$facturarA));

            //dd(DB::getQueryLog());

            $output = "<a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a><i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Datos insertados correctamente.</strong>";
            
             return Response($output);

               return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }

        

            break;

            case 3:

            $categoria = $request->categoria;
            $vinculacion = $request->vinculacion;

            try { 

             DB::table("europa_contactos_funcion")->insert(
                array('CodContacto' => $CodContacto,
                      'CodSupJerarquico' => $vinculacion,
                      'codTipoFuncion'=>$categoria));

            $output = "<a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a><i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Datos insertados correctamente.</strong>";
            
             return Response($output);

               return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }
      

                

            break;

        default:
            # code...
            break;
            
    }

}

    }

    public function eliminar_gesanc(Request $request){

          if($request->ajax()){

            $codcli = $request->codcli;
            $cif = $request->cif;

            try { 

             DB::table("gesanc_clientes")->where("CODCLI","=",$codcli)->delete();

             DB::table("gesanc_contratos")->where("cif","=",$cif)->delete();

            $output = "<a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a><i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Datos eliminados correctamente.</strong>";
            
             return Response($output);

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }

    }

          }

  public function exportar_excel(Request $request){
    $filtros = array();

            $consulta = "";

            if($request->codContacto != null){
                $contratante = $request->codContacto;
                $filtro_codContacto = "CodContacto = '".$codContacto."'";
                $filtros[] = $filtro_codContacto;
            }

             if($request->cif != null){
                $cif = $request->cif;
                $filtro_cif = "cif = '".$cif."'";
                $filtros[] = $filtro_cif;
            } 

             if($request->empresa != null){
                $empresa = $request->empresa;
                $filtro_empresa = "empresa = '".$empresa."'";
                $filtros[] = $filtro_empresa;
            } 

             if($request->direccion != null){
                $direccion = $request->direccion;
                $filtro_direccion = "descuento LIKE '%".$direccion."%'";
                $filtros[] = $filtro_direccion;
            }

             if($request->codpostal != null){
                $codpostal = $request->codpostal;
                $filtro_codpostal = "codPostal = '".$codpostal."'";
                $filtros[] = $filtro_codpostal;
            }

            if($request->aptocorreos != null){
                $aptocorreos = $request->aptocorreos;
                $filtro_aptocorreos = "apartadoCorreos = '".$aptocorreos."'";
                $filtros[] = $filtro_aptocorreos;
            }

              if($request->poblacion != null){
                $poblacion = $request->poblacion;
                $filtro_poblacion = "poblacion = '".$poblacion."'";
                $filtros[] = $filtro_poblacion;
            }

              if($request->telefono != null){
                $telefono = $request->telefono;
                $filtro_telefono = "CodContacto in (select codContacto from europa_contactos_telefonos where codTelefono = '".$telefono."')";
                $filtros[] = $filtro_telefono;
            }

              if($request->email != null){
                $email = $request->email;
                $filtro_aptocorreos = "apartadoCorreos = '".$email."'";
                $filtros[] = $filtro_aptocorreos;
            }

              if($request->pais != null){
                $pais = $request->pais;
                $filtro_pais = "pais = '".$pais."'";
                $filtros[] = $filtro_pais;
            }

             if($request->provincia != null){
                $provincia = $request->provincia;
                $filtro_provincia = "a.codProvincia = '".$provincia."'";
                $filtros[] = $filtro_provincia;
            }

         

             if($request->comercial != null){
                $comercial = $request->comercial;
                $filtro_comercial = "`Id-comercial` = '".$comercial."'";
                $filtros[] = $filtro_comercial;
            } 

              if($request->cuentabancaria != null){
                $cuentabancaria = $request->cuentabancaria;
                $filtro_banco = "CC = '".$cuentabancaria."'";
                $filtros[] = $filtro_banco;
            }


             if($request->mandato != null){
                $mandato = $request->mandato;
                $filtro_otrocomercial = "mandato = '".$mandato."'";
                $filtros[] = $filtro_otrocomercial;
            }  


             if($request->sinprovision != null){
                $sinprovision = $request->sinprovision;
                $filtro_sinprovision = "sinProvisionFondos = '".$sinprovision."'";
                $filtros[] = $filtro_sinprovision;
            } 

            if($request->poremail != null){
                $poremail = $request->poremail;
                $filtro_poremail = "Mailing = '".$poremail."'";
                $filtros[] = $filtro_poremail;
            }   

             if($request->conflictivo != null){
                $conflictivo = $request->conflictivo;
                $filtro_conflictivo = "cteConflictivo = '".$conflictivo."'";
                $filtros[] = $filtro_conflictivo;
            } 

              if($request->impagado != null){
                $impagado = $request->impagado;
                $filtro_impagado = "impagado_pdteDev = '".$impagado."'";
                $filtros[] = $filtro_impagado;
            }
         

            for ($i =count($filtros) - 1; $i >= 0 ; $i--) { 
                $consulta .= $filtros[$i];
                $consulta .= " ";

                if($i > 0){
                    $consulta .= "AND ";
                }
            }

            $contactos = DB::table("gesanc_clientes")->orderby("empresa","ASC")->limit(200)->get();



            
            if($consulta !=""){


                $contactos = DB::table("gesanc_clientes")->whereRaw($consulta)->orderby("empresa","ASC")->limit(200)->get();

             }

              $objPHPExcel = new PHPExcel();
        // Se asignan las propiedades del libro
        $objPHPExcel->getProperties()->setCreator("ITS") // Nombre del autor
        ->setLastModifiedBy("ITS") //Ultimo usuario que lo modificó
        ->setTitle("Sanciones filtradas") // Titulo
        ->setDescription("Sanciones filtradas"); //Descripción
        $titulosColumnas = array('CIF'
                                ,'Empresa'
                                ,'Matricula'
                                ,'PWD'
                                ,'Dirección'
                                ,'Población'
                                ,'CP');
        
        // Se combinan las celdas, para colocar ahí el titulo del reporte
        //$objPHPExcel->setActiveSheetIndex(0)
        //->mergeCells('A1:D1');
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1', $titulosColumnas[0])
        ->setCellValue('B1', $titulosColumnas[1])
        ->setCellValue('C1', $titulosColumnas[2])
        ->setCellValue('D1', $titulosColumnas[3])
        ->setCellValue('E1', $titulosColumnas[4])
        ->setCellValue('F1', $titulosColumnas[5]);

        $i=2;
        foreach ($contactos as $key => $contacto) {
              $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.$i, $contacto->cif)
            ->setCellValue('B'.$i, $contacto->empresa)
            ->setCellValue('C'.$i, $contacto->PWD)
            ->setCellValue('D'.$i, $contacto->direccion)
            ->setCellValue('E'.$i, $contacto->poblacion)
            ->setCellValue('F'.$i, $contacto->cp);
            $i++;
        }



    header('Content-Encoding: UTF-8');
    header('Content-type: text/csv; charset=UTF-8');
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="contactos_filtrados.xlsx"');
    header('Cache-Control: max-age=0');
    header("Pragma: no-cache");
    header("Expires: 0");
    header('Content-Transfer-Encoding: binary');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');

  }

  public function generar_excel_contactos(Request $request){
            
        $output="<p style='text-align:center;'>No se han encontrado resultados.</p>";

            $filtros = array();

            $consulta = "";

              

            if($request->codContacto != null){
                $contratante = $request->codContacto;
                $filtro_codContacto = "CodContacto = '".$codContacto."'";
                $filtros[] = $filtro_codContacto;
            }

             if($request->cif != null){
                $cif = $request->cif;
                $filtro_cif = "cif = '".$cif."'";
                $filtros[] = $filtro_cif;
            } 

             if($request->empresa != null){
                $empresa = $request->empresa;
                $filtro_empresa = "razonSocial = '".$empresa."'";
                $filtros[] = $filtro_empresa;
            } 

             if($request->direccion != null){
                $direccion = $request->direccion;
                $filtro_direccion = "descuento LIKE '%".$direccion."%'";
                $filtros[] = $filtro_direccion;
            }

             if($request->codpostal != null){
                $codpostal = $request->codpostal;
                $filtro_codpostal = "codPostal = '".$codpostal."'";
                $filtros[] = $filtro_codpostal;
            }

            if($request->aptocorreos != null){
                $aptocorreos = $request->aptocorreos;
                $filtro_aptocorreos = "apartadoCorreos = '".$aptocorreos."'";
                $filtros[] = $filtro_aptocorreos;
            }

              if($request->poblacion != null){
                $poblacion = $request->poblacion;
                $filtro_poblacion = "Poblacion = '".$poblacion."'";
                $filtros[] = $filtro_poblacion;
            }

              if($request->telefono != null){
                $telefono = $request->telefono;
                $filtro_telefono = "CodContacto in (select codContacto from europa_contactos_telefonos where codTelefono = '".$telefono."')";
                $filtros[] = $filtro_telefono;
            }

              if($request->email != null){
                $email = $request->email;
                $filtro_aptocorreos = "apartadoCorreos = '".$email."'";
                $filtros[] = $filtro_aptocorreos;
            }

              if($request->pais != null){
                $pais = $request->pais;
                $filtro_pais = "a.codPais = '".$pais."'";
                $filtros[] = $filtro_pais;
            }

             if($request->provincia != null){
                $provincia = $request->provincia;
                $filtro_provincia = "a.codProvincia = '".$provincia."'";
                $filtros[] = $filtro_provincia;
            }

              if($request->tipoContacto != null){
                $tipoContacto = $request->tipoContacto;
                $filtro_tipocontacto = "a.CodTipoFuncion = '".$tipoContacto."'";
                $filtros[] = $filtro_tipocontacto;
            }

             if($request->comercial != null){
                $comercial = $request->comercial;
                $filtro_comercial = "codComercial = '".$comercial."'";
                $filtros[] = $filtro_comercial;
            } 

              if($request->cuentabancaria != null){
                $cuentabancaria = $request->cuentabancaria;
                $filtro_banco = "CC = '".$cuentabancaria."'";
                $filtros[] = $filtro_banco;
            }


             if($request->otrocomercial != null){
                $otrocomercial = $request->otrocomercial;
                $filtro_otrocomercial = "cteDeOtroComercial = '".$otrocomercial."'";
                $filtros[] = $filtro_otrocomercial;
            }  


             if($request->sinprovision != null){
                $sinprovision = $request->sinprovision;
                $filtro_sinprovision = "sinProvisionFondos = '".$sinprovision."'";
                $filtros[] = $filtro_sinprovision;
            } 

            if($request->poremail != null){
                $poremail = $request->poremail;
                $filtro_poremail = "mailing = '".$poremail."'";
                $filtros[] = $filtro_poremail;
            }   

             if($request->conflictivo != null){
                $conflictivo = $request->conflictivo;
                $filtro_conflictivo = "cteConflictivo = '".$conflictivo."'";
                $filtros[] = $filtro_conflictivo;
            } 

              if($request->impagado != null){
                $impagado = $request->impagado;
                $filtro_impagado = "impagado_pdteDev = '".$impagado."'";
                $filtros[] = $filtro_impagado;
            }
         

            for ($i =count($filtros) - 1; $i >= 0 ; $i--) { 
                $consulta .= $filtros[$i];
                $consulta .= " ";

                if($i > 0){
                    $consulta .= "AND ";
                }
            }

            $contactos = DB::table(DB::raw("europa_contactos a, europa_provincias b"))->select(DB::raw("a.*, b.Provincia"))->whereRaw("a.codProvincia = b.codProvincia")->orderby("razonSocial","ASC")->limit(200)->get();



            
            if($consulta !=""){


               $contactos = DB::table(DB::raw("europa_contactos a, europa_provincias b"))->select(DB::raw("a.*, b.Provincia"))->whereRaw("a.codProvincia = b.codProvincia AND ".$consulta)->orderby("razonSocial","ASC")->limit(200)->get();

             }
            
           $objPHPExcel = new PHPExcel();
        // Se asignan las propiedades del libro
        $objPHPExcel->getProperties()->setCreator("ITS") // Nombre del autor
        ->setLastModifiedBy("ITS") //Ultimo usuario que lo modificó
        ->setTitle("Contatos_filtrados") // Titulo
        ->setDescription("Contatos_filtrados"); //Descripción
        $titulosColumnas = array('Cliente'
                                ,'Impago'
                                ,'Poblacion'
                                ,'Direccion'
                                ,'CP'
                                ,'Provincia');
        
        // Se combinan las celdas, para colocar ahí el titulo del reporte
        //$objPHPExcel->setActiveSheetIndex(0)
        //->mergeCells('A1:D1');
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1', $titulosColumnas[0])
        ->setCellValue('B1', $titulosColumnas[1])
        ->setCellValue('C1', $titulosColumnas[2])
        ->setCellValue('D1', $titulosColumnas[3])
        ->setCellValue('E1', $titulosColumnas[4])
        ->setCellValue('F1', $titulosColumnas[5]);



        $i=2;
        foreach ($contactos as $key => $contacto) {
          $impagado = "";
           if($contacto->impagado_pdteDev == 0){
                            $impagado='NO';
                             }else{
                              $impagado='SI';
                             }
              $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.$i, $contacto->razonSocial)
            ->setCellValue('B'.$i, $impagado)
            ->setCellValue('C'.$i, $contacto->direccion)
            ->setCellValue('D'.$i, $contacto->Poblacion)
            ->setCellValue('E'.$i, $contacto->codPostal)
            ->setCellValue('F'.$i, $contacto->Provincia);
            $i++;
        }



    header('Content-Encoding: UTF-8');
    header('Content-type: text/csv; charset=UTF-8');
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="Contactos_filtrados.xlsx"');
    header('Cache-Control: max-age=0');
    header("Pragma: no-cache");
    header("Expires: 0");
    header('Content-Transfer-Encoding: binary');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
    
}

public function exportar_excel_its(Request $request){
   $contactos = DB::table('its_contactos')->where('Empresa','LIKE','%'.$request->search.'%')
                                ->orWhere('CIF','LIKE','%'.$request->search.'%')
                                ->orWhere('Nombre','LIKE','%'.$request->search.'%')
                                ->orWhere('Apellidos','LIKE','%'.$request->search.'%')
                                ->orWhere('Pais','LIKE','%'.$request->search.'%')
                                ->orWhere('Poblacion','LIKE','%'.$request->search.'%')
                                ->take(200)
                                ->orderBy('Empresa')
                                ->get();

        $objPHPExcel = new PHPExcel();
        // Se asignan las propiedades del libro
        $objPHPExcel->getProperties()->setCreator("ITS") // Nombre del autor
        ->setLastModifiedBy("ITS") //Ultimo usuario que lo modificó
        ->setTitle("Contatos_filtrados") // Titulo
        ->setDescription("Contactos_filtrados"); //Descripción
        $titulosColumnas = array('Empresa'
                                ,'Cif'
                                ,'Nombre'
                                ,'Apellidos'
                                ,'Pais'
                                ,'Población');
        
        // Se combinan las celdas, para colocar ahí el titulo del reporte
        //$objPHPExcel->setActiveSheetIndex(0)
        //->mergeCells('A1:D1');
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1', $titulosColumnas[0])
        ->setCellValue('B1', $titulosColumnas[1])
        ->setCellValue('C1', $titulosColumnas[2])
        ->setCellValue('D1', $titulosColumnas[3])
        ->setCellValue('E1', $titulosColumnas[4])
        ->setCellValue('F1', $titulosColumnas[5]);

        $i=2;
        foreach ($contactos as $key => $contacto) {
              $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.$i, $contacto->Empresa)
            ->setCellValue('B'.$i, $contacto->CIF)
            ->setCellValue('C'.$i, $contacto->Nombre)
            ->setCellValue('D'.$i, $contacto->Apellidos)
            ->setCellValue('E'.$i, $contacto->Pais)
            ->setCellValue('F'.$i, $contacto->Poblacion);
            $i++;
        }


    header('Content-Encoding: UTF-8');
    header('Content-type: text/csv; charset=UTF-8');
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="Contactos_filtrados.xlsx"');
    header('Cache-Control: max-age=0');
    header("Pragma: no-cache");
    header("Expires: 0");
    header('Content-Transfer-Encoding: binary');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');


  }

  public function exportar_sobre(Request $request){

    $contacto = DB::table("its_contactos")->where("CIF","=",$request->cif)->first();

    //&$provincia = DB::table("europa_provincias")->where("codProvincia","=",$contacto->codProvincia)->first();

    $filename = '..\public\temp\sobre_DL.docx';

      $templateWord = new TemplateProcessor('..\public\templates\contactos\its\sobre.docx');

                $templateWord->setValue('nombre_empresa',$contacto->Empresa);
                $templateWord->setValue('direccion_empresa',$contacto->Direccion);
                $templateWord->setValue('poblacion',$contacto->Poblacion);
                //$templateWord->setValue('provincia',$provincia->Provincia);
                $templateWord->setValue('cp',$contacto->CP);



    $templateWord->saveAs(storage_path($filename));
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename=sobre_DL.docx; charset=iso-8859-1');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize('..\public\temp\sobre_DL.docx'));
    readfile('..\public\temp\sobre_DL.docx');
}


}