<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\gesanc_sanciones;

use App\gesanc_lista_motivos_denuncia;

use App\gesanc_reduccion;

use App\gesanc_contencioso;

use App\gesanc_clientes;

use DateTime;

use Illuminate\Database\QueryException;

use App\gesanc_contratos;

use PhpOffice\PhpWord\TemplateProcessor;

use PHPExcel; 
use PHPExcel_IOFactory; 

use DocxMerge\DocxMerge;
//include_once('..\public\tbszip.php');

use App\tbszip;

class ContratosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function europa()
    {

    

    /*$contratos = DB::select("SELECT a.codContrato, a.codTipoContrato, a.fechaContrato, b.razonSocial as 'Contratante', c.razonsocial as 'Contratado', d.Nombre, d.Apellidos from europa_contratos a INNER JOIN europa_contactos b ON a.codContratante=b.CodContacto INNER JOIN europa_contactos c ON a.codContratado=c.CodContacto INNER JOIN gesanc_comerciales d ON a.codComercial=d.`Id-comercial` order by a.FechaContrato LIMIT 200");*/

       $contratos = DB::table(DB::raw("europa_contratos a, europa_contactos b"))->select(DB::raw("a.FechaContrato,a.Vencimiento,a.importeCto,a.observaciones,a.codTipoContrato, b.razonSocial,a.codContrato"))->whereRaw("a.codContratado=b.CodContacto")->orderby("FechaContrato","DESC")->get();

       $empresas = DB::table("europa_contactos")->select("razonSocial","CodContacto")->get();

       $tipos_anexo = DB::table("europa_tipo_anexo")->select("tipoAnexo")->distinct()->get();

       $contratantes = DB::table("europa_contactos")->select("razonSocial","CodContacto")->whereRaw("CodContacto in (select codContratante from europa_contratos)")->get();

       $comerciales = DB::table("gesanc_comerciales")->selectRaw("CONCAT(Nombre,' ',Apellidos) as comercial,`Id-comercial` as ID")->whereRaw("`Id-comercial` in (select CodContacto from europa_contactos)")->get();

       $matriculas = DB::table("europa_matriculas")->select("Matricula")->whereRaw("Matricula in (select Matricula from europa_anexos)")->get();
   

        return view('contratos/europa/europa',['contratos'=> $contratos,'empresas'=>$empresas,'tipos_anexo'=>$tipos_anexo,'contratantes'=>$contratantes,'comerciales'=>$comerciales,'matriculas'=>$matriculas]);
    }

     public function its()
    {

    $contratos = DB::table('its_contratos')->selectRaw("IDContrato,TipoContrato,`Fecha Contrato` as FechaContrato,Baja,Contratado,Contratante")->orderBy('FechaContrato','DESC')->take(20)->get();

   	$clientes = DB::table('its_contactos')->select('Empresa')->whereRaw('CIF IN (select Contratado from its_contratos)')->orderBy('Empresa')->get();

    $contratantes = DB::table('its_contratos')->select('Contratante')->distinct()->orderBy('Contratante')->get();

    $matriculas = DB::table('its_anexos')->select("Matricula")->whereRaw('Beneficiario in (select Contratado from its_contratos)')->distinct()->orderBy('Matricula')->get();

    $tipo_contrato = DB::table('its_tipo de contrato')->orderBy('TipoServicio')->get();

    $paises = DB::table('its_paises')->select('Pais')->whereRaw('codais IN (Select Pais from its_contactos)')->orderBy('Pais')->get();

    $coberturas = DB::table('its_cobertura')->select('Descripcion')->whereRaw('COBERTURA in (select Cobertura from its_anexos)')->orderBy('Descripcion')->get();

        return view('contratos/its',['contratos'=> $contratos,'clientes'=>$clientes,'contratantes'=>$contratantes,'matriculas'=>$matriculas,'tipo_contrato'=>$tipo_contrato,'paises'=>$paises,'coberturas'=>$coberturas]);
    }

     public function gesanc()
    {

    $contratos = DB::table('gesanc_contratos')->orderBy('Alta','DESC')->take(100)->get();

    $empresas = DB::table('gesanc_clientes')->whereRaw('cif IN (select cif from gesanc_sanciones)')->orderBy('empresa')->get();

    $comerciales = DB::table("gesanc_comerciales")->selectRaw("CONCAT(Nombre,' ',Apellidos) as comercial,`Id-comercial` as ID")->whereRaw("`Id-comercial` in (select CodContacto from europa_contactos)")->get();

    $contratantes = DB::table("gesanc_empresas")->orderBy("Num_empresa",'ASC')->get();

    $provincias = DB::table("gesanc_provincias")->get();

    $tipos_contrato = DB::table("europa_tipo_anexo")->get();

    $matriculas = DB::table('gesanc_matriculas')->whereRaw('matricula IN (select matricula from gesanc_sanciones)')->orderBy('matricula')->distinct()->get();

        return view('contratos/gesanc/gesanc',['contratos'=> $contratos,'empresas'=>$empresas,'comerciales'=>$comerciales,'contratantes'=>$contratantes,'provincias'=>$provincias,'matriculas'=>$matriculas,'tipos_contrato'=>$tipos_contrato]);
    }

    public function filtra_gesanc(Request $request){

       $output="<p style='text-align:center;'>No se han encontrado resultados.</p>";

            if($request->ajax()){

            $filtros = array();

            $consulta = "";

              if($request->alta_inicio != null && $request->alta_fin !=null){
                $alta_inicio = $request->alta_inicio;
                $alta_fin = $request->alta_fin;

                $FechaContrato = "FechaContrato BETWEEN '".$alta_inicio."' AND '".$alta_fin."'";

                $filtros[] = $FechaContrato;

                //$sanciones=gesanc_sanciones::whereRaw($consulta)->get();
              
            }

             if($request->baja_inicio != null && $request->baja_fin !=null){
                $baja_inicio = $request->baja_inicio;
                $baja_fin = $request->baja_fin;

                $FechaBaja = "baja BETWEEN '".$baja_inicio."' AND '".$baja_fin."'";

                $filtros[] = $FechaBaja;

                //$sanciones=gesanc_sanciones::whereRaw($consulta)->get();
              
            }

             if($request->venc_inicio != null && $request->venc_fin !=null){
                $venc_inicio = $request->venc_inicio;
                $venc_fin = $request->venc_fin;

                $FechaVencimiento = "vencimiento BETWEEN '".$venc_inicio."' AND '".$venc_fin."'";

                $filtros[] = $FechaVencimiento;

                //$sanciones=gesanc_sanciones::whereRaw($consulta)->get();
              
            }

            if($request->codContrato != null){
                $codContrato = $request->codContrato;
                $filtro_codContrato = "idcontrato = '".$codContrato."'";
                $filtros[] = $filtro_codContrato;
            }


             if($request->contratante != null){
                $contratante = $request->contratante;
                $filtro_contratante = "contratante = '".$contratante."'";
                $filtros[] = $filtro_contratante;
            } 

            if($request->empresa != null){
                $empresa = $request->empresa;
                $filtro_contratado = "cif = '".$empresa."'";
                $filtros[] = $filtro_contratado;
            } 

              if($request->tipoContrato != null){
                $tipoContrato = $request->tipoContrato;
                $filtro_tipoContrato = "Tipo = '".$tipoContrato."'";
                $filtros[] = $filtro_tipoContrato;
            }

            if($request->matricula != null){
                $matricula = $request->matricula;
                $filtro_matricula = "Matricula='".$matricula."'";
                $filtros[] = $filtro_matricula;
            }

             if($request->notas != null){
                $notas = $request->notas;
                $filtro_notas = "Notas='".$notas."'";
                $filtros[] = $filtro_notas;
            } 

             if($request->clave != null){
                $clave = $request->clave;
                $filtro_clave = "clavecontrato='".$clave."'";
                $filtros[] = $filtro_clave;
            } 

             if($request->comercial != null){
                $comercial = $request->comercial;
                $filtro_comercial = "Id-comercial = '".$comercial."'";
                $filtros[] = $filtro_comercial;
            }

              if($request->provincia != null){
                $provincia = $request->provincia;
                $filtro_provincia = "cif = (select cif from gesanc_clientes where provincia = '".$provincia."')";
                $filtros[] = $filtro_provincia;
            }  


            for ($i =count($filtros) - 1; $i >= 0 ; $i--) { 
                $consulta .= $filtros[$i];
                $consulta .= " ";

                if($i > 0){
                    $consulta .= "AND ";
                }
            }

           $contratos = DB::table('gesanc_contratos')->orderBy('Alta','DESC')->take(100)->get();

            
            if($consulta !=""){

          $contratos = DB::table('gesanc_contratos')->whereRaw($consulta)->orderBy('Alta','DESC')->take(100)->get();

             }
            
           if($contratos){
                foreach ($contratos as $key => $contrato) {

                     $output.='<tr>'.
                             '<td>'.$contrato->idcontrato .'</td>'.
                             '<td>'.$contrato->Tipo .'</td>'.
                             '<td>'.$contrato->Matricula .'</td>'.
                             '<td>'.$contrato->cif .'</td>'.
                             '<td>'.date('d - m - Y', strtotime($contrato->Alta)).'</td>'.
                             '<td>'.date('d - m - Y', strtotime($contrato->Baja)).'</td>'.
                             '<td><a class="btn btn-sm btn-success" href="/contratos/gesanc/'.$contrato->idcontrato.'"><i class="fa fa-edit"></i> </a></td>'.
                             '</tr>';

                }

                
               
            }

            return Response($output);
             
    }


    }

    public function desactivar_sanciones(Request $request){

         if($request->ajax()){

                try { 
            
             DB::table("gesanc_sanciones")->where("cif","=",$request->cif)->update(['Inactivo'=>1]);

            return  Response("Las sanciones se han desactivado correctamente.");

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }

         }
    }

       public function activar_sanciones(Request $request){

         if($request->ajax()){

                try { 
            
             DB::table("gesanc_sanciones")->where("cif","=",$request->cif)->update(['Inactivo'=>0]);

            return  Response("Las sanciones se han activado correctamente.");

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }

         }
    }

   
     public function eliminar_gesanc(Request $request){
          if($request->ajax()){

                try { 
            
             DB::table("gesanc_contratos")->where("idcontrato","=",$request->idcontrato)->delete();

            return  Response("el contrato se ha eliminado correctamente.");

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }

         }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       //TODO
        if($request->ajax()){

            $datos = array();
            parse_str($request->datos,$datos);

            $empresa = null;
            $contratado = null;
            $cif= null;
            $tipoContrato = null;
            $contratante = null;
            $fechaContrato = null;
            $fecha = null;
            $tipo = null;
            }

            if(array_key_exists("EC",$datos)){
               $EC = 1;
            }

            if($datos["empresa"]!=null){
                $empresa = $datos["empresa"];
            }

            if($datos["contratante"]!=null){
                $contratante = $datos["contratante"];
            }

            if($datos["tipo_contrato"]!=null){
                $tipoContrato = $datos["tipo_contrato"];
            }

    
            if($datos["fechaContrato"] !=null){
                $fechaContrato = DateTime::createFromFormat('d/m/Y', $datos["fechaContrato"]);
                date_time_set($fechaContrato, 00, 00);
                $fecha = date_create($datos["fechaContrato"]);
                date_add($fecha, date_interval_create_from_date_string('1 year'));
                $fecha_renovacion = date_format($fecha, 'Y-m-d');
            }


            $tipo_contrato = DB::table(DB::raw("its_contratos a, `its_tipo de contrato` b"))->select(DB::raw("a.*, b.*"))->whereRaw("a.TipoContrato = b.IdTipoContrato AND TipoServicio='".$tipoContrato."'")->get();
            

              if($datos["tipo_contrato"]!=null){
                foreach ($tipo_contrato as $key => $dato) {
                $tipo = $dato->IdTipoContrato;
            }
            }

            $clientes = DB::table('its_contactos')->select('CIF')->whereRaw('Empresa ="'.$empresa.'" ')->get();

           
                foreach ($clientes as $key => $dato){ 
                $contratado = $dato->CIF;
            }
            
           


            try { 
              DB::table('its_contratos')->insertGetId(
                ['Contratado' => $contratado,'TipoContrato' => $tipo,'Contratante'=>$contratante, 'Fecha Contrato' => $fechaContrato, 'Vencimiento' => $fecha_renovacion]
                );

            return  Response("El contrato se ha añadido correctamente");

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }

    }

 public function borrar (Request $request){
if($request->ajax()){  

$ID = $request->ID;
$tabla = $request->tabla;
$field = $request->field;

DB::table($tabla)->where($field, '=', $ID)->delete();

$output = "<a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a><i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Datos eliminados correctamente.</strong>";

return Response($output);

}

 }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     public function show_europa($id)
    {

      $contrato = DB::table(DB::raw("europa_contratos a, europa_contactos b"))->select(DB::raw("a.*,b.razonSocial"))->whereRaw("a.codContratado=b.CodContacto AND a.CodContrato='".$id."'")->get();

      $tipo_contrato = DB::table("europa_tipo_contrato")->get();

      //$anexos = DB::table('europa_anexos')->where('codContrato',$id)->get();

      $anexos = DB::table(DB::raw("europa_anexos a, europa_tipo_anexo b"))->select(DB::raw("a.*,b.tipoAnexo"))->whereRaw(" a.CodTipoAnexo=b.CodtipoAnexo AND a.CodContrato='".$id."'")->get();


      $empresas = DB::table("europa_contactos")->select("razonSocial","CodContacto","observaciones")->whereRaw("CodContacto =(select codContratado from europa_contratos where CodContrato='".$id."')")->get();

      $contratantes = DB::table("europa_contactos")->select("razonSocial","CodContacto")->whereRaw("CodContacto in (select codContratante from europa_contratos)")->get();

       $comerciales = DB::table("gesanc_comerciales")->selectRaw("CONCAT(Nombre,' ',Apellidos) as comercial,`Id-comercial` as ID")->whereRaw("`Id-comercial` in (select CodContacto from europa_contactos)")->get();

       $suma =DB::table("europa_anexos")->selectRaw("SUM(importeVenta) as suma")->where("CodContrato",$id)->get();

       $tipo_anexo = DB::table("europa_tipo_anexo")->select("tipoAnexo")->whereRaw("CodtipoAnexo = (select CodtipoAnexo from europa_contratos where codContrato=CodContrato='".$id."')");

      $empresa_comercial = "";
      $id_comercial = "";
      $suma_anexos = "";

      foreach ($contrato as $key => $dato) {
        $empresa_comercial = $dato->codContratante;
      }

      foreach ($contrato as $key => $dato) {
        $id_comercial = $dato->codComercial;
      }

      foreach ($suma as $key => $dato) {
        $suma_anexos = $dato->suma;
      }


   

      $otros_contratos = DB::table(DB::raw("europa_contratos a, europa_contactos b"))->select(DB::raw("a.*,b.razonSocial"))->whereRaw("a.codContratado=b.CodContacto AND a.CodContratado=(select CodContratado from europa_contratos where CodContrato ='".$id."')")->get();

        return view('contratos/europa/detail', compact('contrato'),['contrato'=>$contrato,'anexos'=>$anexos,'empresas'=>$empresas,'contratantes'=>$contratantes,'comerciales'=>$comerciales,'otros_contratos'=>$otros_contratos,'empresa_comercial'=>$empresa_comercial,'id_comercial'=>$id_comercial,'suma_anexos'=>$suma_anexos,'tipo_anexo'=>$tipo_anexo]);
    }

    
    public function show_its($id)
    {

    	$contrato = DB::table('its_contratos')->where('IDContrato',$id)->get();

    	$anexos = DB::table('its_anexos')->where('IdContrato',$id)->get();



    	$fechaContrato = DB::table('its_contratos')->selectRaw("`Fecha Contrato` as FechaContrato")->where('IDContrato',$id)->get();

    	$clientes = DB::table('its_contactos')->select('Empresa')->whereRaw('CIF = (select Contratado from its_contratos where IDContrato="'.$id.'")')->get();

    	$tipo_contrato = DB::table(DB::raw("its_contratos a, `its_tipo de contrato` b"))->select(DB::raw("a.*, b.TipoServicio"))->whereRaw("a.TipoContrato = b.IdTipoContrato AND a.IDContrato='".$id."'")->get();

    	$cheques = DB::table("its_cheques")->whereRaw("CIF in (Select Beneficiario from its_anexos where IdContrato = '".$id."')")->get();

    	$fecha_contrato ="";
    	$fecha_renovacion = "";

    	$Contratado ="";

    	foreach ($contrato as $key => $dato) {
                $Contratado = $dato->Contratado;
            }

        foreach ($fechaContrato as $key => $dato) {
                $fecha_contrato = $dato->FechaContrato;
            }


           $fecha = date_create($fecha_contrato);
		   date_add($fecha, date_interval_create_from_date_string('1 year'));
		   $fecha_renovacion = date_format($fecha, 'Y-m-d');

		$otros_contratos = DB::table('its_contratos')->selectRaw("`Fecha Contrato` as FechaContrato,Baja,TipoContrato,IDContrato")->where('Contratado',$Contratado)->get();

        return view('contratos/its_detail', compact('contrato'),['clientes'=>$clientes,'tipo_contrato'=>$tipo_contrato,'fecha_contrato'=>$fecha_contrato,'fecha_renovacion'=>$fecha_renovacion,'otros_contratos'=>$otros_contratos,'cheques'=>$cheques,'anexos'=>$anexos]);
    }

       public function show_gesanc($id)
    {

    $contrato = DB::table('gesanc_contratos')->selectRaw("*,`Id-comercial` as id_comercial")->where("idcontrato","=",$id)->first();

    $empresa = DB::table('gesanc_clientes')->whereRaw('cif IN (select cif from gesanc_contratos where idcontrato = "'.$id.'")')->first();

    $comerciales = DB::table("gesanc_comerciales")->selectRaw("CONCAT(Nombre,' ',Apellidos) as comercial,`Id-comercial` as ID")->get();

    $contratantes = DB::table("gesanc_empresas")->orderBy("Num_empresa",'ASC')->get();

    $tipos_contrato = DB::table("europa_tipo_anexo")->get();

    $remolques = DB::table("europa_anexos_remolques")->whereRaw("Alta='".$contrato->Alta."' and codAnexo in (select codAnexo from europa_anexos where CodContrato in (select CodContrato from europa_contratos where codContratado = (select codContacto from europa_contactos where cif='".$empresa->cif."')))")->get();

    $notas_anexo = DB::table("europa_anexos")->whereRaw("Alta = '".$contrato->Alta."' and CodTipoAnexo = (select CodTipoAnexo from europa_tipo_anexo where tipoAnexo ='".$contrato->Tipo."') and CodContrato in (select CodContrato from europa_contratos where codContratado = (select codContacto from europa_contactos where cif='".$empresa->cif."'))")->first();

    $matriculas = DB::table('gesanc_matriculas')->whereRaw('matricula IN (select matricula from gesanc_sanciones)')->orderBy('matricula')->distinct()->get();


    $otros_contratos = DB::table('gesanc_contratos')->whereRaw("cif = '".$empresa->cif."'")->get();

    $fecha_seleccionada = DB::table('gesanc_contratos')->whereRaw("cif = '".$empresa->cif."' and fechaContrato='".$contrato->fechamodifacion."'")->get();

    $de_alta = DB::table('gesanc_contratos')->whereRaw("cif = '".$empresa->cif."' and Alta <= DATE(CURDATE()) and (Baja > DATE(CURDATE()) OR Baja is null) and Vencimiento > date(curdate())")->get();

    $activo = DB::table('gesanc_contratos')->selectRaw("count(*) as total")->whereRaw("cif = '".$empresa->cif."' and Alta <= DATE(CURDATE()) and (Baja > DATE(CURDATE()) OR Baja is null) and Vencimiento > date(curdate())")->first();

    $no_contratadas = DB::table('gesanc_contratos')->whereRaw("cif = '".$empresa->cif."' AND FechaContrato is null")->get();

    $inactivas = DB::table("gesanc_sanciones")->selectRaw("count(*) as 'total'")->whereRaw("Inactivo = 1 and cif = '".$empresa->cif."'")->first();

       return view('contratos/gesanc/detail',['contrato'=> $contrato,'empresa'=>$empresa,'comerciales'=>$comerciales,'contratantes'=>$contratantes,'matriculas'=>$matriculas,'tipos_contrato'=>$tipos_contrato,'notas_anexo'=>$notas_anexo,'remolques'=>$remolques,'fecha_seleccionada'=>$fecha_seleccionada,'otros_contratos'=>$otros_contratos,'no_contratadas'=>$no_contratadas,'de_alta'=>$de_alta,'inactivas'=>$inactivas,'activo'=>$activo]);
    }

    public function update_gesanc(Request $request)
    {
         //TODO
        if($request->ajax()){

            $datos = array();
            parse_str($request->datos,$datos);

            $idcontrato = null;
            $cif = null;
            $contratante = null;
            $comercial= null;
            $tipo_contrato = null;
            $matricula = null;
            $alta = null;
            $baja = null;
            $vencimiento = null;
            $notas_anexo = null;
            $notas_cliente = null;
            $notas_contrato = null;

            $cod_contrato = null;

            }


            if($datos["idcontrato"]!=null){
                $idcontrato = $datos["idcontrato"];
            }

            if($datos["contratante"]!=null){
                $contratante = $datos["contratante"];
            }

            if($datos["comercial"]!=null){
                $comercial = $datos["comercial"];
            }

             if($datos["matricula"]!=null){
                $matricula = $datos["matricula"];
            }

               if($datos["cif"]!=null){
                $cif = $datos["cif"];
            }

            if($datos["tipo_contrato"]!=null){
                $tipoContrato = $datos["tipo_contrato"];
            }


              if($datos["notas_cliente"]!=null){
                $notas_cliente = $datos["notas_cliente"];
            }

              if($datos["notas_contrato"]!=null){
                $notas_contrato = $datos["notas_contrato"];
            }

    
            if($datos["Alta"] !=null){
                $alta = DateTime::createFromFormat('d/m/Y', $datos["Alta"]);
                date_time_set($alta, 00, 00);
            }

               if($datos["Baja"] !=null){
                $baja = DateTime::createFromFormat('d/m/Y', $datos["Baja"]);
                date_time_set($baja, 00, 00);
            }

               if($datos["Vencimiento"] !=null){
                $vencimiento = DateTime::createFromFormat('d/m/Y', $datos["Vencimiento"]);
                date_time_set($vencimiento, 00, 00);
            }




            try { 

                //Seleccionar las condiciones del contrato según los parametros establecidos

              DB::table('gesanc_contratos')->where("idcontrato","=",$idcontrato)->update(
                ['contratante'=>$contratante,'Id-comercial'=>$comercial,'Tipo'=>$tipo_contrato,'Notas'=>$notas_contrato,'Matricula'=>$matricula,'Alta'=>$alta, 'Baja'=>$baja, 'Vencimiento'=>$vencimiento,'fechamodifacion'=>date('Y-m-d H:i:s')]);

             DB::table("gesanc_clientes")->where("cif",'=',$cif)->update(['Observacion'=>$notas_cliente]);
             
            return  Response("El contrato se ha editado correctamente");

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

      public function nuevo_its(Request $request)
    {


    $clientes = DB::table('its_contactos')->select('Empresa')->get();

    $contratantes = DB::table('its_contratos')->select('Contratante')->distinct()->orderBy('Contratante')->get();

    $tipo_contrato = DB::table('its_tipo de contrato')->orderBy('TipoServicio')->get();


       return view('contratos/create_its',['clientes'=>$clientes,'contratantes'=>$contratantes,'tipo_contrato'=>$tipo_contrato]);
    }

    public function update_its(Request $request, $id)
    {

        //TODO
        if($request->ajax()){

            $datos = array();
            parse_str($request->datos,$datos);

            $empresa = null;
            $contratado = null;
            $tipo = null;
            $contratante = null;
            $fechaContrato = null;
            $Renovacion = null;
            $Baja = null;
            

           
            }

            if(array_key_exists("EC",$datos)){
               $EC = 1;
            }

            if($datos["empresa"]!=null){
                $empresa = $datos["empresa"];
            }

            if($datos["contratado"]!=null){
                $contratado = $datos["contratado"];
            }
            if($datos["contratante"]!=null){
                $contratante = $datos["contratante"];
            }

    
            if($datos["fechaContrato"] !=null){
                $fechaContrato = DateTime::createFromFormat('d/m/Y', $datos["fechaContrato"]);
                date_time_set($fechaContrato, 00, 00);
            }

            if($datos["Renovacion"]!=null){
                $Renovacion = DateTime::createFromFormat('d/m/Y', $datos["Renovacion"]);
                date_time_set($Renovacion, 00, 00);
            }

            if($datos["Baja"]!=null){
                $Baja = DateTime::createFromFormat('d/m/Y', $datos["Baja"]);
                date_time_set($Baja, 00, 00);
            }

            $tipo_contrato = DB::table(DB::raw("its_contratos a, `its_tipo de contrato` b"))->select(DB::raw("a.*, b.*"))->whereRaw("a.TipoContrato = b.IdTipoContrato AND a.IDContrato='".$id."'")->get();
            

              if($datos["tipo_contrato"]!=null){
                foreach ($tipo_contrato as $key => $dato) {
                $tipo = $dato->IdTipoContrato;
            }
            }

            



            try { 
               DB::table('its_contratos')->where('IDContrato', $id)->update(
                    ['Contratado' => $contratado,'TipoContrato' => $tipo, 'Fecha Contrato' => $fechaContrato, 'Vencimiento' => $Renovacion, 'Baja' => $Baja]);

            return  Response("El Contrato se ha editado correctamente.");

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }


    }

    public function update_anexo(Request $request, $id)
    {
        //TODO
        if($request->ajax()){

            $datos = array();
            parse_str($request->datos,$datos);

            $Beneficiario = null;
            $Cobertura = null;
            $Matricula = null;
            $FechaContrato = null;
            $EC = 0;
            $Alta = null;
            $Baja = null;

            $Sustituye = null;
            
            $Inter = null;
            
            $MotivoBaja = null;
           
            //echo $datos["empresa"];
            //print_r($datos);

           
            }

            if(array_key_exists("EC",$datos)){
               $EC = 1;
            }

            if($datos["Beneficiario"]!=null){
                $Beneficiario = $datos["Beneficiario"];
            }

            if($datos["Cobertura"]!=null){
                $Cobertura = $datos["Cobertura"];
            }
            if($datos["Matricula"]!=null){
                $Matricula = $datos["Matricula"];
            }

            if($datos["Sustituye"]!=null){
                $Sustituye = $datos["Sustituye"];
            }

             if($datos["Inter"]!=null){
                $Inter = $datos["Inter"];
            }

             if($datos["MotivoBaja"]!=null){
                $MotivoBaja = $datos["MotivoBaja"];
            }



            if($datos["FechaContrato"] !=null){
                $FechaContrato = DateTime::createFromFormat('d/m/Y', $datos["FechaContrato"]);
                date_time_set($FechaContrato, 00, 00);
            }

            if($datos["Alta"]!=null){
                $Alta = DateTime::createFromFormat('d/m/Y', $datos["Alta"]);
                date_time_set($Alta, 00, 00);
            }

            if($datos["Baja"]!=null){
                $Baja = DateTime::createFromFormat('d/m/Y', $datos["Baja"]);
                date_time_set($Baja, 00, 00);
            }

            $IdMotivo = DB::table('its_motivos baja')->whereRaw('idmotivobaja =(select IdMotivoBaja from its_anexos where IdAnexo="'.$id.'")')->get();
            $IdMotivoBaja = null;

              if($datos["MotivoBaja"]!=null){
                foreach ($IdMotivo as $key => $dato) {
                $IdMotivoBaja = $dato->idmotivobaja;
            }
            }

            



            try { 
               DB::table('its_anexos')->where('IdAnexo', $id)->update(
                    ['Beneficiario' => $datos["Beneficiario"],'Cobertura' => $datos["Cobertura"], 'Matricula' => $datos["Matricula"], 'SUSTITUYE' => $Sustituye, 'cheques' => $EC, 'Alta' => $Alta , 'Baja' => $Baja,'IdMotivoBaja'=>$IdMotivoBaja]);

            return  Response("El Anexo seha editado correctamente.");

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }

        }  
       
    



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

     public function borrar_anexo(Request $request)
    {
        if($request->ajax()){

            $ID = $request->ID;

            DB::table("its_anexos")->where('IdAnexo','=', $ID)->delete();

            $output = "<a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a><i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Datos eliminados correctamente.</strong>";

            return Response($output);

        }
    }


      /**
    * Busca una sanción y muestra en la tabla los resultados.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function search_its(Request $request){

        if($request->ajax()){
            $output="<p style='text-align:center;'>No se han encontrado resultados.</p>";
            $contratos=DB::table('its_contratos')->selectRaw("IDContrato,TipoContrato,`Fecha Contrato` as FechaContrato,Baja,Contratado,Contratante")->where('IDContrato','LIKE','%'.$request->search.'%')
                                ->orWhere('TipoContrato','LIKE','%'.$request->search.'%')
                                ->orWhere('Contratante','LIKE','%'.$request->search.'%')
                                ->orWhere('Contratado','LIKE','%'.$request->search.'%')
                                ->take(200)
                                ->orderBy('FechaContrato','DESC')
                                ->get();

            if($contratos){
                foreach ($contratos as $key => $contrato) {

                    $output.='<tr>'.
                             '<td>'.$contrato->IDContrato.'</td>'.
                             '<td>'.$contrato->TipoContrato.'</td>'.
                             '<td>'.$contrato->Contratante.'</td>'.
                             '<td>'.$contrato->Contratado.'</td>'.
                               '<td>'.date('d - m - Y', strtotime($contrato->FechaContrato)).'</td>'.                 
                               '<td>'.date('d - m - Y', strtotime($contrato->Baja)).'</td>'.
                             '<td><a class="btn btn-sm btn-success" href="sanciones/'.$contrato->IDContrato.'"><i class="fa fa-edit"></i> </a> <a class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a></td>'.
                             '</tr>';

                }
                return Response($output);
            }
        }
    }

     public function search_europa(Request $request){

       DB::enableQueryLog();
         

        if($request->ajax()){
            $output="<p style='text-align:center;'>No se han encontrado resultados.</p>";
            $contratos = DB::table(DB::raw("europa_contratos a, europa_contactos b"))->selectRaw("a.CodContrato,a.FechaContrato,a.Vencimiento,a.importeCto,a.observaciones,a.codTipoContrato, b.razonSocial")->where('codContrato','LIKE','%'.$request->search.'%')
                                ->orWhere('a.FechaContrato','LIKE','%'.$request->search.'%')
                                ->orWhere('a.Vencimiento','LIKE','%'.$request->search.'%')
                                ->orWhere('a.importeCto','LIKE','%'.$request->search.'%')
                                ->orWhere('a.observaciones','LIKE','%'.$request->search.'%')
                                ->Where('a.codContratado','=','b.CodContacto')
                                ->take(200)
                                ->orderBy('FechaContrato','DESC')
                                ->get();

            //dd(DB::getQueryLog());

            if($contratos){
                foreach ($contratos as $key => $contrato) {

                    $output.='<tr>'.
                             '<td>'.$contrato->razonSocial.'</td>'.
                             '<td>'.$contrato->codTipoContrato.'</td>'.
                             '<td>'.date('d - m - Y', strtotime($contrato->FechaContrato)).'</td>'.
                             '<td>'.date('d - m - Y', strtotime($contrato->Vencimiento)).'</td>'.
                             '<td>'.$contrato->importeCto.'</td>'.
                             '<td>'.$contrato->observaciones.'</td>'.
                             '<td><a class="btn btn-sm btn-success" href="contratos/europa/'.$contrato->CodContrato.'"><i class="fa fa-edit"></i> </a> <a class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a></td>'.
                             '</tr>';

                }
                return Response($output);
            }
        }
    }

    public function filtra_europa(Request $request){
            
        $output="<p style='text-align:center;'>No se han encontrado resultados.</p>";

            if($request->ajax()){

            $filtros = array();

            $consulta = "";

              if($request->alta_inicio != null && $request->alta_fin !=null){
                $alta_inicio = $request->alta_inicio;
                $alta_fin = $request->alta_fin;

                $FechaContrato = "FechaContrato BETWEEN '".$alta_inicio."' AND '".$alta_fin."'";

                $filtros[] = $FechaContrato;

                //$sanciones=gesanc_sanciones::whereRaw($consulta)->get();
              
            }

             if($request->baja_inicio != null && $request->baja_fin !=null){
                $baja_inicio = $request->baja_inicio;
                $baja_fin = $request->baja_fin;

                $FechaBaja = "baja BETWEEN '".$baja_inicio."' AND '".$baja_fin."'";

                $filtros[] = $FechaBaja;

                //$sanciones=gesanc_sanciones::whereRaw($consulta)->get();
              
            }

             if($request->venc_inicio != null && $request->venc_fin !=null){
                $venc_inicio = $request->venc_inicio;
                $venc_fin = $request->venc_fin;

                $FechaVencimiento = "vencimiento BETWEEN '".$venc_inicio."' AND '".$venc_fin."'";

                $filtros[] = $FechaVencimiento;

                //$sanciones=gesanc_sanciones::whereRaw($consulta)->get();
              
            }

             if($request->altaanexo_inicio != null && $request->altaanexo_fin !=null){
                $altaanexo_inicio = $request->altaanexo_inicio;
                $altaanexo_fin = $request->altaanexo_fin;

                $Alta_anexo = "(CodContrato in (select CodContrato from europa_anexos where Alta between '".$altaanexo_inicio."' and '".$altaanexo_fin."'))";

                $filtros[] = $Alta_anexo;

                //$sanciones=gesanc_sanciones::whereRaw($consulta)->get();
              
            }

            if($request->codContrato != null){
                $contratante = $request->codContrato;
                $filtro_codContrato = "codContrato = '".$codContrato."'";
                $filtros[] = $filtro_codContrato;
            }

             if($request->importe != null){
                $importe = $request->importe;
                $filtro_importe = "importeCto = '".$importe."'";
                $filtros[] = $filtro_importe;
            } 

             if($request->comision != null){
                $comision = $request->comision;
                $filtro_comision = "porcentComisEspecifica = '".$comision."'";
                $filtros[] = $filtro_comision;
            } 

             if($request->descuento != null){
                $descuento = $request->descuento;
                $filtro_descuento = "descuento = '".$descuento."'";
                $filtros[] = $filtro_descuento;
            }

             if($request->otrocomercial != null){
                $otrocomercial = $request->otrocomercial;
                $filtro_otrocomercial = "esCteDeOtroComercial = '".$otrocomercial."'";
                $filtros[] = $filtro_otrocomercial;
            }
             if($request->pendienteforma != null){
                $pendienteforma = $request->pendienteforma;
                $filtro_pendiente = "pdteFormaPago = '".$pendienteforma."'";
                $filtros[] = $filtro_pendiente;
            }    


             if($request->contratante != null){
                $contratante = $request->contratante;
                $filtro_contratante = "codContratante = '".$contratante."'";
                $filtros[] = $filtro_contratante;
            } 

            if($request->empresa != null){
                $empresa = $request->empresa;
                $filtro_contratado = "codContratado = '".$empresa."'";
                $filtros[] = $filtro_contratado;
            } 

            if($request->matricula != null){
                $matricula = $request->matricula;
                $filtro_matricula = "codContrato in (select codContrato from europa_anexos where Matricula='".$matricula."')";
                $filtros[] = $filtro_matricula;
            } 

             if($request->comercial != null){
                $comercial = $request->comercial;
                $filtro_comercial = "codComercial = '".$comercial."'";
                $filtros[] = $filtro_comercial;
            } 

            if($request->tipo_anexo != null){
                $tipo_anexo = $request->tipo_anexo;
                $filtro_tipoanexo = "codContrato in (select codContrato from europa_anexos where CodTipoAnexo='".$tipo_anexo."')";
                $filtros[] = $filtro_tipoanexo;
            } 

            if($request->tipo_abono != null){
                $tipo_abono = $request->tipo_abono;
                $filtro_tipoabono = "codContrato in (select codContrato from europa_abonos where tipoAbono='".$tipo_abono."')";
                $filtros[] = $filtro_tipoabono;
            }

            for ($i =count($filtros) - 1; $i >= 0 ; $i--) { 
                $consulta .= $filtros[$i];
                $consulta .= " ";

                if($i > 0){
                    $consulta .= "AND ";
                }
            }

            $contratos = DB::table(DB::raw("europa_contratos a, europa_contactos b"))->select(DB::raw("a.CodContrato,a.FechaContrato,a.Vencimiento,a.importeCto,a.observaciones,a.codTipoContrato, b.razonSocial"))->whereRaw("a.codContratado=b.CodContacto")->orderby("FechaContrato","DESC")->limit(200)->get();



            
            if($consulta !=""){

              $contratos = DB::table(DB::raw("europa_contratos a, europa_contactos b"))->select(DB::raw("a.CodContrato,a.FechaContrato,a.Vencimiento,a.importeCto,a.observaciones,a.codTipoContrato, b.razonSocial"))->whereRaw("a.codContratado=b.CodContacto AND ".$consulta)->orderby("FechaContrato","DESC")->limit(200)->get();

             }
            
           if($contratos){
                foreach ($contratos as $key => $contrato) {

                     $output.='<tr>'.
                             '<td>'.$contrato->razonSocial.'</td>'.
                             '<td>'.$contrato->codTipoContrato.'</td>'.
                             '<td>'.date('d - m - Y', strtotime($contrato->FechaContrato)).'</td>'.
                             '<td>'.date('d - m - Y', strtotime($contrato->Vencimiento)).'</td>'.
                             '<td>'.$contrato->importeCto.'</td>'.
                             '<td>'.$contrato->observaciones.'</td>'.
                             '<td><a class="btn btn-sm btn-success" href="/contratos/europa/'.$contrato->CodContrato.'"><i class="fa fa-edit"></i> </a></td>'.
                             '</tr>';

                }

                
               
            }

            return Response($output);
             
    }
}

     public function filtra_its(Request $request){
            
        $output="<p style='text-align:center;'>No se han encontrado resultados.</p>";

            if($request->ajax()){

            $filtros = array();

            $consulta = "";

              if($request->alta_inicio != null && $request->alta_fin !=null){
                $alta_inicio = $request->alta_inicio;
                $alta_fin = $request->alta_fin;

                $FechaContrato = "(`Fecha Contrato` BETWEEN '".$alta_inicio."' AND '".$alta_fin."')";

                $filtros[] = $FechaContrato;

                //$sanciones=gesanc_sanciones::whereRaw($consulta)->get();
              
            }

             if($request->baja_inicio != null && $request->baja_fin !=null){
                $baja_inicio = $request->baja_inicio;
                $baja_fin = $request->baja_fin;

                $FechaBaja = "(Baja BETWEEN '".$baja_inicio."' AND '".$baja_fin."')";

                $filtros[] = $FechaBaja;

                //$sanciones=gesanc_sanciones::whereRaw($consulta)->get();
              
            }

             if($request->venc_inicio != null && $request->venc_fin !=null){
                $venc_inicio = $request->venc_inicio;
                $venc_fin = $request->venc_fin;

                $FechaVencimiento = "(Vencimiento BETWEEN '".$venc_inicio."' AND '".$venc_fin."')";

                $filtros[] = $FechaVencimiento;

                //$sanciones=gesanc_sanciones::whereRaw($consulta)->get();
              
            }

             if($request->altaanexo_inicio != null && $request->altaanexo_fin !=null){
                $altaanexo_inicio = $request->altaanexo_inicio;
                $altaanexo_fin = $request->altaanexo_fin;

                $Alta_anexo = "(IDContrato in (select IdAnexo from its_anexos where Alta between '".$altaanexo_inicio."' and '".$altaanexo_fin."'))";

                $filtros[] = $Alta_anexo;

                //$sanciones=gesanc_sanciones::whereRaw($consulta)->get();
              
            }

             if($request->bajaanexo_inicio != null && $request->bajaanexo_fin !=null){
                $bajaanexo_inicio = $request->bajaanexo_inicio;
                $bajaanexo_fin = $request->bajaanexo_fin;

                $Baja_anexo = "(IDContrato in (select IdAnexo from its_anexos where Baja between '".$bajaanexo_inicio."' and '".$bajaanexo_fin."'))";

                $filtros[] = $Baja_anexo;

                //$sanciones=gesanc_sanciones::whereRaw($consulta)->get();
              
            }

             if($request->contratante != null){
                $contratante = $request->contratante;
                $filtro_contratante = "Contratante = '".$contratante."'";
                $filtros[] = $filtro_contratante;
            } 

            if($request->empresa != null){
                $empresa = $request->empresa;
                $filtro_contratado = "Contratante in (select CIF from its_contactos where Empresa = '".$empresa."')";
                $filtros[] = $filtro_contratado;
            } 

            if($request->matricula != null){
                $matricula = $request->matricula;
                $filtro_matricula = "IDContrato in (select IdContrato from its_anexos where Matricula ='".$matricula."')";
                $filtros[] = $filtro_matricula;
            } 

            if($request->tipo_vehiculo != null){
                $tipo_vehiculo = $request->tipo_vehiculo;
                $filtro_tipovehiculo = "IDContrato IN (SELECT idAnexo from its_anexos where Matricula in (select Matricula from its_matriculas where TipoVehiculo = '".$tipo_vehiculo."'))";
                $filtros[] = $filtro_tipovehiculo;
            } 

            if($request->tipo_contrato != null){
                $tipo_contrato = $request->tipo_contrato;
                $filtro_tipocontrato = "TipoContrato = (select IdTipoContrato from `its_tipo de contrato` where TipoServicio ='".$tipo_contrato."')";
                $filtros[] = $filtro_tipocontrato;
            }

             if($request->cobertura != null){
                $cobertura = $request->cobertura;
                $filtro_cobertura = "IDContrato in (Select IdContrato from its_anexos where Cobertura = (select COBERTURA from its_cobertura where Descripcion='".$cobertura."'))";
                $filtros[] = $filtro_cobertura;
            }  
             if($request->pais != null){
                $pais = $request->pais;
                if($pais=="Espana"){
                    $pais="España";
                }
                $filtro_pais = "Contratante in (Select CIF from its_contactos where Pais = (select Codais from its_paises where Pais='".$pais."'))";
                $filtros[] = $filtro_pais;
            }

            for ($i =count($filtros) - 1; $i >= 0 ; $i--) { 
                $consulta .= $filtros[$i];
                $consulta .= " ";

                if($i > 0){
                    $consulta .= "AND ";
                }
            }

             DB::enableQueryLog();
//


             $contratos = DB::table('its_contratos')->selectRaw("IDContrato,TipoContrato,`Fecha Contrato` as FechaContrato,Baja,Contratado,Contratante")->orderBy('FechaContrato','DESC')->take(20)->get();


            
            if($consulta !=""){

                 $contratos=DB::table('its_contratos')->selectRaw("IDContrato,TipoContrato,`Fecha Contrato` as FechaContrato,Baja,Contratado,Contratante")->whereRaw($consulta)->limit(200)->get();
             }
            
           if($contratos){
                foreach ($contratos as $key => $contrato) {

                     $output.='<tr>'.
                             '<td>'.$contrato->IDContrato.'</td>'.
                             '<td>'.$contrato->TipoContrato.'</td>'.
                             '<td>'.$contrato->Contratante.'</td>'.
                             '<td>'.$contrato->Contratado.'</td>'.
                               '<td>'.date('d - m - Y', strtotime($contrato->FechaContrato)).'</td>'.                 
                               '<td>'.date('d - m - Y', strtotime($contrato->Baja)).'</td>'.
                             '<td><a class="btn btn-sm btn-success" href="contratos/its/'.$contrato->IDContrato.'"><i class="fa fa-edit"></i> </a> <a class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a></td>'.
                             '</tr>';

                }

                
               
            }

            //dd(DB::getQueryLog());

            return Response($output);
             
    }
}



public function show_anexos_its($id)
    {

    $anexos = DB::table('its_anexos')->where('IdAnexo',$id)->get();

	$fechaContrato = DB::table('its_contratos')->selectRaw("`Fecha Contrato` as FechaContrato")->whereRaw('IDContrato = (select IdContrato from its_anexos where IdAnexo="'.$id.'")')->get();

	$Inter = DB::table('its_contratos')->selectRaw("`Fecha Contrato` as FechaContrato")->whereRaw('IDContrato = (select IdContrato from its_anexos where IdAnexo="'.$id.'")')->get();

	$motivo_baja = DB::table('its_motivos baja')->whereRaw('idmotivobaja =(select IdMotivoBaja from its_anexos where IdAnexo="'.$id.'")')->get();

	$fecha_contrato ="";

    

        foreach ($fechaContrato as $key => $dato) {
                $fecha_contrato = $dato->FechaContrato;
            }


    return view('contratos/anexos/anexo_detail',compact('anexos'),['fecha_contrato'=>$fecha_contrato,'motivo_baja'=>$motivo_baja]);

    }

    public function nuevoanexo_its($id)
    {

        $idContrato = $id;

        $contrato = DB::table("its_contratos")->where("IDContrato","=",$id)->first();

        $contacto = DB::table("its_contactos")->whereRaw("CIF = (select Contratado from its_contratos where IDContrato='".$idContrato."')")->first();

        $coberturas = DB::table("its_cobertura")->where("TipoContrato","=",$contrato->TipoContrato)->get();

    return view('contratos/anexos/its_nuevoanexo',['IDContrato'=>$idContrato,'contrato'=>$contrato,'contacto'=>$contacto,'coberturas'=>$coberturas]);

    }

    public function insertar(Request $request)
    {

        if($request->ajax()){

             try { 
                
            //
               DB::table('its_anexos')->insertGetId(
                ['IdContrato'=>$request->IdContrato,'Beneficiario'=>$request->beneficiario,'Cobertura'=>$request->cobertura,'Matricula'=>$request->matricula,'cheques'=>$request->EC,'Alta'=>$request->Alta]
                );

              $matricula_creada = DB::table("its_matriculas")->where("Matricula","=",$request->matricula)->first();

               if($matricula_creada == null){
                DB::table("its_matriculas")->insert(['Matricula'=>$request->matricula,'TipoVehiculo'=>'C']);
               }

            return  Response($request->IdContrato);

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }

        }

         

        }

public function newcobertura_its(){

 $tipo_contrato = DB::table('its_tipo de contrato')->orderBy('TipoServicio')->get();

 return view('contratos/newcobertura',['tipo_contrato'=>$tipo_contrato]);
}

public function store_cobertura(Request $Request){

 //TODO
        if($Request->ajax()){

            $datos = array();
            parse_str($Request->datos,$datos);

            $Cobertura = null;
            $Tipo_Contrato = null;
            $Descripcion = null;
            $Tipo = null;
            $Precio = 0;
            $PrecioR = null;
            $PrecioT = null;

            }


            if($datos["cobertura"]!=null){
                $Cobertura = $datos["cobertura"];
            }

            if($datos["tipo_contrato"]!=null){
                $Tipo_Contrato = $datos["tipo_contrato"];
            }
            if($datos["descripcion"]!=null){
                $Descripcion = $datos["descripcion"];
            }

            if($datos["tipo"]!=null){
                $Tipo = $datos["tipo"];
            }

             if($datos["precio"]!=null){
                $Precio = $datos["precio"];
            }

             if($datos["precioT"]!=null){
                $PrecioT = $datos["precioT"];
            }

             if($datos["precioR"]!=null){
                $PrecioR = $datos["precioR"];
            }

            $Tipos_Contrato = DB::table('its_tipo de contrato')->whereRaw('TipoServicio ="'.$Tipo_Contrato.'"')->get();
            $TipoContrato = null;

              if($datos["tipo_contrato"]!=null){
                foreach ($Tipos_Contrato as $key => $dato) {
                $TipoContrato = $dato->IdTipoContrato;
            }
            }

            



            try { 
               DB::table('its_cobertura')->insertGetId(
                ['COBERTURA' => $Cobertura,'TipoContrato' => $TipoContrato,'Descripcion'=>$Descripcion, 'TIPO' => $Tipo, 'Precio' => $Precio,'PrecioT' => $PrecioT,'PrecioR' => $PrecioR]);

            return  Response("La cobertura se ha añadido correctamente");

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }
}

public function store_tipo(Request $Request){

 //TODO
        if($Request->ajax()){

            $datos = array();
            parse_str($Request->datos,$datos);

            $id = null;
            $servicio = null;
            
            }


            if($datos["id"]!=null){
                $id = $datos["id"];
            }

            if($datos["servicio"]!=null){
                $servicio = $datos["servicio"];
            }

            try { 
               DB::table('its_tipo de contrato')->insertGetId(
                ['IdTipoContrato' => $id,'TipoServicio' => $servicio]);

            return  Response("La cobertura se ha añadido correctamente");

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }
}

public function tarjetas(){

  

    $clientes = DB::table('its_contactos')->whereRaw('CIF IN (select Contratado from its_contratos)')->orderBy('Empresa')->get();

    $contratantes = DB::table('its_tarjetas')->select('empCom')->distinct()->orderBy('empCom')->get();

    $entidades = DB::table('its_tarjetas')->select('entidad')->distinct()->orderBy('entidad')->get();

    $tarjetas = DB::table(DB::raw("its_tarjetas a, `its_tarjetas-matricula` b"))->select(DB::raw("a.*, b.matricula as matricula"))->whereRaw("a.numTarjeta = b.numTarjeta")->get();

        return view('contratos/tarjetas/tarjetas',['clientes'=>$clientes,'tarjetas'=>$tarjetas,'contratantes'=>$contratantes,'entidades'=>$entidades]);
}

 public function search_tarjetas(Request $request){

        if($request->ajax()){
            $output="<p style='text-align:center;'>No se han encontrado resultados.</p>";
            
            DB::enableQueryLog();
            //dd(DB::getQueryLog());

            $tarjetas = DB::table(DB::raw("eurits_db.its_tarjetas a LEFT JOIN eurits_db.`its_tarjetas-matricula` b ON a.numTarjeta=b.numTarjeta"))->select(DB::raw("a.*,a.numTarjeta as 'tarjeta', b.*"))->whereRaw("(b.matricula IS NULL OR b.matricula IS NOT NULL) AND cif LIKE '".$request->cif."'")->get();

       

            if($tarjetas){
                foreach ($tarjetas as $key => $tarjeta) {

                    $output.="<tr>".
                            "<td>".$tarjeta->tarjeta."</td>".
                            "<td>".$tarjeta->pin."</td>".
                            "<td>".$tarjeta->matricula."</td>".
                            "<td>".date('d - m - Y', strtotime($tarjeta->caducidad))."</td>";                           
                            if($tarjeta->docFirmado==1){
                            $output.= "<td>Si</td>";
                            }
                            else{
                            $output.= "<td>No</td>";
                            }
                            if($tarjeta->entidad==1){
                            $output.="<td>CAIXA</td>";
                            }
                            
                            elseif($tarjeta->entidad==2){
                            $output.="<td>C2A</td>";
                            }
                           
                            elseif($tarjeta->entidad==3){
                            $output.="<td>FRANCE TRUCK</td>";
                            }
                            else{
                           $output.= "<td>".$tarjeta->entidad."</td>";
                            }
                            if($tarjeta->empCom==10){
                              $output.= "<td>Europa ITS Soluciones SL</td>";
                            }
                            elseif($tarjeta->empCom==9){
                              $output.= "<td>Soler y Martín Eurogestión, S.L.</td>";
                            }
                            else{
                              $output.= "<td></td>";
                            }
                            $output.= "<td><a class='btn btn-sm btn-success' href='contratos/tarjetas/'.$tarjeta->numTarjeta)'><i class='fa fa-edit'></i> </a></td>";
                            $output.= "</tr>";

                }
                return Response($output);
            }
        }
    }

     public function show_tarjetas($id)
    {

      
      $tarjetas = DB::table(DB::raw("eurits_db.its_tarjetas a LEFT JOIN eurits_db.`its_tarjetas-matricula` b ON a.numTarjeta=b.numTarjeta"))->select(DB::raw("a.*,a.numTarjeta as 'tarjeta',a.`recepcion confirmada` as 'recepcion', b.*"))->whereRaw("(b.matricula IS NULL OR b.matricula IS NOT NULL) AND a.numTarjeta = '".$id."'")->get();

      $tarjetas_matriculas = DB::table('its_tarjetas-matricula')->where('numTarjeta',$id)->get();

       $contratantes = DB::table('its_tarjetas')->select('empCom')->distinct()->orderBy('empCom')->get();

      $entidades = DB::table('its_tarjetas')->select('entidad')->distinct()->orderBy('entidad')->get();

      $matriculas = DB::table('its_matriculas')->select("Matricula")->get();

      $clientes = DB::table('its_contactos')->whereRaw('CIF = (select cif from its_tarjetas where numTarjeta="'.$id.'")')->get();

      $empresas = DB::table('its_contactos')->get();

        return view('contratos/tarjetas/detail',['clientes'=>$clientes,'tarjetas_matriculas'=>$tarjetas_matriculas,'tarjetas'=>$tarjetas,'contratantes'=>$contratantes,'entidades'=>$entidades,'empresas'=>$empresas,'matriculas'=>$matriculas]);
    }


public function update_tarjeta(Request $request, $id)
    {

        //TODO
        if($request->ajax()){

            $datos = array();
            parse_str($request->datos,$datos);

            $empresa = null;
            $matricula = null;
            $contratante = null;
            $entidad = null;
            $entregada = null;
            $fecha_entregada =$datos["entregada"];
            $caducidad = null;
            $baja = null;
            $pin = null;
            $referencia = null;
            $noDeCajero = null;
            $recepcion = null;
            $docFirmado = 0;
            $notas = null;
            

           
            }


            if($datos["empresa"]!=null){
                $empresa = $datos["empresa"];
            }

            if($datos["matricula"]!=null){
                $matricula = $datos["matricula"];
            }

             if($datos["contratante"]!=null){
                $contratante = $datos["contratante"];
              }
            
            if($datos["entidad"]!=null){
                $entidad = $datos["entidad"];
            }


            if($datos["entregada"] !=null){
               $entregada = DateTime::createFromFormat('d/m/Y', $datos["entregada"]);
                date_time_set($entregada, 00, 00);
            }

            if($datos["caducidad"]!=null){
                 $caducidad = DateTime::createFromFormat('d/m/Y', $datos["caducidad"]);
                date_time_set($caducidad, 00, 00);
               
            }

            if($datos["baja"]!=null){
                 $baja = DateTime::createFromFormat('d/m/Y', $datos["baja"]);
                date_time_set($baja, 00, 00);
               
            }

            if($datos["pin"]!=null){
                $pin = $datos["pin"];
            }

             if($datos["referencia"]!=null){
                $referencia = $datos["referencia"];
            }

             if($datos["noDeCajero"]!=null){
                $noDeCajero = $datos["noDeCajero"];
            }
             if($datos["recepcion"]!=null){
                $recepcion = DateTime::createFromFormat('d/m/Y', $datos["recepcion"]);
                date_time_set($recepcion, 00, 00);
            }

               if(array_key_exists("docFirmado",$datos)){
               $docFirmado = 1;
            }




            /*$tipo_contrato = DB::table(DB::raw("its_contratos a, `its_tipo de contrato` b"))->select(DB::raw("a.*, b.*"))->whereRaw("a.TipoContrato = b.IdTipoContrato AND a.IDContrato='".$id."'")->get();
            

              if($datos["tipo_contrato"]!=null){
                foreach ($tipo_contrato as $key => $dato) {
                $tipo = $dato->IdTipoContrato;
            }
            }*/

            



            try { 
               DB::table('its_tarjetas')->where('numTarjeta', $id)->update(
                    ['cif' => $empresa,'empCom' => $contratante, 'entregada' => $entregada, 'caducidad' => $caducidad, 'baja' => $baja, 'pin'=>$pin,'notas'=>$notas,'entidad'=>$entidad,'docFirmado'=>$docFirmado,'referencia'=>$referencia,'noDeCajero'=>$noDeCajero,'recepcion confirmada'=>$recepcion]);

                 DB::table('its_tarjetas-matricula')->where('numTarjeta', $id)->update(
                    ['matricula' => $matricula]);

            return  Response("La tarjeta se ha editado correctamente.");

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }


    }

     public function nueva_tarjeta(){

      $entidades = DB::table('its_tarjetas')->select('entidad')->distinct()->orderBy('entidad')->get();

      $matriculas = DB::table('its_matriculas')->select("Matricula")->get();

      $empresas = DB::table('its_contactos')->get();

      $contratantes = DB::table('its_tarjetas')->select('empCom')->distinct()->orderBy('empCom')->get();


        return view('contratos/tarjetas/nueva',['contratantes'=>$contratantes,'entidades'=>$entidades,'empresas'=>$empresas,'matriculas'=>$matriculas]);

     }

    public function store_tarjeta(Request $request)
    {

        //TODO
        if($request->ajax()){

            $datos = array();
            parse_str($request->datos,$datos);
            $numTarjeta = null;
            $empresa = null;
            $matricula = null;
            $contratante = null;
            $entidad = null;
            $entregada = null;
            $caducidad = null;
            $baja = null;
            $pin = null;
            $referencia = null;
            $noDeCajero = null;
            $recepcion = null;
            $docFirmado = 0;
            $notas = null;
          
           
            }else{
                  return  Response("No he hecho nada");
            }


            if($datos["numTarjeta"]!=null){
                  $numTarjeta = $datos["numTarjeta"];

            if($datos["empresa"]!=null){
                $empresa = $datos["empresa"];
            }

            if($datos["matricula"]!=null){
                $matricula = $datos["matricula"];
            }

             if($datos["contratante"]!=null){
                $contratante = $datos["contratante"];
              }
            
            if($datos["entidad"]!=null){
                $entidad = $datos["entidad"];
            }


            if($datos["entregada"] !=null){
               $entregada = DateTime::createFromFormat('d/m/Y', $datos["entregada"]);
                date_time_set($entregada, 00, 00);
            }

            if($datos["caducidad"]!=null){
                 $caducidad = DateTime::createFromFormat('d/m/Y', $datos["caducidad"]);
                date_time_set($caducidad, 00, 00);
               
            }

            if($datos["baja"]!=null){
                 $baja = DateTime::createFromFormat('d/m/Y', $datos["baja"]);
                date_time_set($baja, 00, 00);
               
            }

            if($datos["pin"]!=null){
                $pin = $datos["pin"];
            }

             if($datos["referencia"]!=null){
                $referencia = $datos["referencia"];
            }

             if($datos["noDeCajero"]!=null){
                $noDeCajero = $datos["noDeCajero"];
            }
             if($datos["recepcion"]!=null){
                $recepcion = DateTime::createFromFormat('d/m/Y', $datos["recepcion"]);
                date_time_set($recepcion, 00, 00);
            }

               if(array_key_exists("docFirmado",$datos)){
               $docFirmado = 1;
            }


            try { 
            
                DB::table('its_tarjetas')->insertGetId(
                ['numTarjeta'=>$numTarjeta,'cif' => $empresa,'empCom' => $contratante, 'entregada' => $entregada, 'caducidad' => $caducidad, 'baja' => $baja, 'pin'=>$pin,'notas'=>$notas,'entidad'=>$entidad,'docFirmado'=>$docFirmado,'referencia'=>$referencia,'noDeCajero'=>$noDeCajero,'recepcion confirmada'=>$recepcion]
                );

                  DB::table('its_tarjetas-matricula')->insertGetId(
                ['numTarjeta'=>$numTarjeta,'matricula' => $matricula]
                );

                    




            return  Response("La tarjeta se ha añadido correctamente.");

            } catch(QueryException $ex){ 

            return  Response($ex->getMessage());
              
            }


    }

   

  }

  public function cheques(){

  

    $clientes = DB::table('its_contactos')->whereRaw('CIF IN (select Contratado from its_contratos)')->orderBy('Empresa')->get();

    $cheques =  DB::table('its_cheques')->selectRaw("*,`Enviado a` as 'enviado',`Banco Emisor` as 'banco'")->limit(200)->get();

      return view('contratos/cheques/cheques',['clientes'=>$clientes,'cheques'=>$cheques]);
}

public function search_cheques(Request $request){

        if($request->ajax()){
            $output="<p style='text-align:center;'>No se han encontrado resultados.</p>";
            
            DB::enableQueryLog();

            $cheques =  DB::table('its_cheques')->selectRaw("*,`Enviado a` as 'enviado',`Banco Emisor` as 'banco'")->whereRaw("CIF='".$request->cif."'")->orderBy("FechaAbono","DESC")->get();

            //dd(DB::getQueryLog());

            if($cheques){
                foreach ($cheques as $key => $cheque) {

                    $output.="<tr>".
                            "<td>".$cheque->enviado."</td>".
                            "<td>".$cheque->banco."</td>";
                            if($cheque->FechaAbono!=null){
                              $output.= "<td>".date('d - m - Y', strtotime($cheque->FechaAbono))."</td>";
                            }
                          else{
                             $output.= "<td></td>";
                          }
                         $output.= "<td>".$cheque->NumAbono."</td>";
                            $output.= "<td><a class='btn btn-sm btn-success' href='contratos/its/cheques/'.$cheque->IdCheque)'><i class='fa fa-edit'></i> </a></td>";
                            $output.= "</tr>";

                }
                return Response($output);
            }
        }
    }

    public function show_cheque($id){

        $estados_cheque = DB::table("its_estados cheques")->get();

        $matriculas = DB::table("its_matriculas")->get();

        $cheque = DB::table("its_cheques")->selectRaw("*,`Banco Emisor` as 'banco',`Enviado a` as 'enviadoa'")->where("IdCheque","=",$id)->first();

        $contactos = DB::table("its_contactos")->get();

          return view('contratos/cheques/detail',['cheque'=>$cheque,'estados_cheque'=>$estados_cheque,'matriculas'=>$matriculas,'contactos'=>$contactos]);
    }

    public function guardar_cheque(Request $request){

        //TODO
        if($request->ajax()){

            $datos = array();
            parse_str($request->datos,$datos);
            $IdCheque = null;
            $EC = null;
            $CIF = null;
            $Cabecera = null;
            $Banco = null;
            $Enviado = null;
            $enviadoa = null;
            $estado = null;
    


            if($datos["IdCheque"]!=null){
                  $IdCheque = $datos["IdCheque"];

            if($datos["EC"]!=null){
                $EC = $datos["EC"];
            }

            if($datos["CIF"]!=null){
                $CIF = $datos["CIF"];
            }

             if($datos["Banco"]!=null){
                $Banco = $datos["Banco"];
              }
            
            if($datos["enviadoa"]!=null){
                $enviadoa = $datos["enviadoa"];
            }

            if($datos["estado"]!=null){
                $estado = $datos["estado"];
            }


            if($datos["Enviado"] !=null){
               $Enviado = DateTime::createFromFormat('d/m/Y', $datos["Enviado"]);
                date_time_set($Enviado, 00, 00);
            }


            try { 

                DB::table('its_cheques')->where("IdCheque","=",$IdCheque)->update(
                ['EC'=>$EC, 'Cabecera'=>$Cabecera, 'CIF'=>$CIF, 'Banco Emisor'=>$Banco, 'Enviado a'=>$enviadoa, 'enviado'=>$Enviado, 'estado'=>$estado]
                );

            return  Response("El cheque se ha editado correctamente.");

            } catch(QueryException $ex){ 

            return  Response($ex->getMessage());
              
            }


    }

    }

}

public function mostrar_renovaciones() {


      


            $comerciales = DB::table("gesanc_comerciales")->selectRaw("*,`Id-comercial` as 'ID'")->get();

            $meses = array();

            $meses[1] = "Enero";
            $meses[2] = "Febrero";
            $meses[3] = "Marzo";
            $meses[4] = "Abril";
            $meses[5] = "Marzo";
            $meses[6] = "Junio";
            $meses[7] = "Julio";
            $meses[8] = "Agosto";
            $meses[9] = "Septiembre";
            $meses[10] = "Octubre";
            $meses[11] = "Noviembre";
            $meses[12] = "Diciembre";

            $mes_actual = date('n') + 1;

            $years = array();
            $years[1] = "2010";
            $years[2] = "2011";
            $years[3] = "2012";
            $years[4] = "2013";
            $years[5] = "2014";
            $years[6] = "2015";
            $years[7] = "2016";
            $years[8] = "2017";
            $years[9] = "2018";
            $years[10] = "2019";

            $year_actual = date('Y');
            
          
               $output = "<br><label>Elegir comercial</label>";

              $output.='<br><select class="selectpicker" data-live-search="true" title="Buscar..." id="comercial_origen" name="comercial_origen">';

                    foreach ($comerciales as $key => $comercial) {

                       $output.='<option value="'.$comercial->ID.'">'.$comercial->Nombre.' '.$comercial->Apellidos.'</option>';
                     }
            
                
              $output .= "</select><br>";

               $output .= "<br><label>Mes</label>";

              $output.='<br><select class="selectpicker" data-live-search="true" title="Buscar..." id="mes_siguiente" name="mes_siguiente">';

                for ($i=1; $i < count($meses) ; $i++) { 
                    if($mes_actual == $i){
                        $output.='<option value="'.$i.'" selected>'.$meses[$i].'</option>';
                    }else{
                        $output.='<option value="'.$i.'">'.$meses[$i].'</option>';
                    }
                }
            
                
              $output .= "</select><br>";

              $output .= "<br><label>Año</label>";

              $output.='<br><select class="selectpicker" data-live-search="true" title="Buscar..." id="year_curso" name="year_curso">';

                for ($i=1; $i < count($years) ; $i++) { 
                    if($year_actual == $years[$i]){
                        $output.='<option value="'.$i.'" selected>'.$years[$i].'</option>';
                    }else{
                        $output.='<option value="'.$i.'">'.$years[$i].'</option>';
                    }
                }
            
                
              $output .= "</select><br>";

            return Response($output);

    
    }

    public function generar_renovaciones (Request $request)
{

    //echo public_path();

    

    $id_comercial = $_GET["comercial"];
    $mes = $_GET["mes"];
    $year = $_GET["year"];
    $fecha_inicio = $year."-".$mes."-01";
    $fecha_fin = $year."-".$mes."-t";
    $documentos = array();

    $clientes = DB::table("gesanc_clientes")->whereRaw("cif in (select cif from eurits_db.gesanc_contratos where `Id-comercial`='".$id_comercial."' and month(Vencimiento)='".$mes."' and year(Vencimiento)='".$year."')")->get();

    //contador para saber el numero de clientes.
    $numItems = count($clientes);
    $i = 0;
    $comercial = DB::table("gesanc_comerciales")->selectRaw("*,CONCAT(Nombre,' ',Apellidos) as comercial,`Id-comercial` as ID")->whereRaw("`Id-comercial` = '".$id_comercial."'")->first();

    
    //$zip = new clsTbsZip();
    
    foreach ($clientes as $key => $cliente) {

    
    $filename = '..\public\temp\Vencimiento-'. $key . '.docx';
    $documentos [] = $filename;
    $lista_vencimiento = "";
    $lista_matricula ="";
    $lista_tipo ="";
    $lista_notas ="";
    //echo "aqui";

    //echo $cliente->empresa;
    $contratos = DB::table('gesanc_contratos')->selectRaw("*,`Id-comercial` as id_comercial")->whereRaw("cif='".$cliente->cif."' and month(Vencimiento)='".$mes."' and year(Vencimiento)='".$year."'")->get();

    $total_contratos_comercial = DB::table('gesanc_contratos')->selectRaw("count(*) as 'total' ")->whereRaw("`Id-comercial`='".$id_comercial."' and month(Vencimiento)='".$mes."' and year(Vencimiento)='".$year."'")->first();

    $total_contratos_cliente = DB::table('gesanc_contratos')->selectRaw("count(*) as 'total' ")->whereRaw("cif='".$cliente->cif."' and month(Vencimiento)='".$mes."' and year(Vencimiento)='".$year."'")->first();
                $templateWord = new TemplateProcessor('..\public\templates\gesanc_sanciones\Renovaciones_ITS.docx');     
                $templateWord->setValue('comercial',$comercial->comercial);
                $templateWord->setValue('provincia',$cliente->provincia);
                $templateWord->setValue('nombre_cliente',$cliente->empresa);
                $templateWord->setValue('cif',$cliente->cif);
                $templateWord->setValue('fecha',$mes."-".$year);
                $templateWord->setValue('total',$total_contratos_comercial->total);
                $templateWord->setValue('cont',$total_contratos_cliente->total);
                $templateWord->setValue('cod_post',$cliente->cp);
                $templateWord->setValue('direccion_cliente',$cliente->direccion);
                $templateWord->setValue('poblacion',$cliente->poblacion);
                $templateWord->setValue('tel_fijo',$cliente->tel1);
                $templateWord->setValue('tel_movil',$cliente->movil1);
                $templateWord->setValue('inicio_mes',date('d/m/Y', strtotime($fecha_inicio)));
                $templateWord->setValue('final_mes',date('d/m/Y', strtotime($fecha_fin)));


                foreach ($contratos as $key => $contrato) {
                    $lista_vencimiento .= date('d - m - Y', strtotime($contrato->Vencimiento))."<w:br/>";
                    $lista_matricula .= $contrato->Matricula."<w:br/>";
                    $lista_tipo .= $contrato->Tipo."<w:br/>";
                    $lista_notas .= $contrato->Notas."<w:br/>";
                }

                $templateWord->setValue('vencimiento',$lista_vencimiento);
                $templateWord->setValue('matricula',$lista_matricula);
                $templateWord->setValue('tipo',$lista_tipo);
                $templateWord->setValue('notas',$lista_notas);
                
            // --- Guardamos el documento pero antes si no es el ultimo documento generamos un salto de línea.
    if(++$i !== $numItems) {
    $salto = '<w:p>
              <w:r>
                <w:br w:type="page" />
              </w:r>
            </w:p>';
  }else{
    $salto = "";
  }
    
    $templateWord->setValue('salto',$salto);           
    //$templateWord->saveAs($filename);
    $templateWord->saveAs(storage_path($filename));
    




//echo file_get_contents('Documento02.docx');

 
    
}
    $dm = new DocxMerge();
    $dm->merge( $documentos, '..\public\temp\result_renovaciones.docx' );
 
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename=result_renovaciones.docx; charset=iso-8859-1');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize('..\public\temp\result_renovaciones.docx'));
    readfile('..\public\temp\result_renovaciones.docx');
  

    $files = glob('..\public\temp\*'); // get all file names
    foreach($files as $file){ // iterate files
      if(is_file($file))
        unlink($file); // delete file
        }
  exit;
}

public function mostrar_codigos(){
    $matriculas = DB::table('gesanc_matriculas')->whereRaw('matricula IN (select matricula from gesanc_sanciones)')->orderBy('matricula')->distinct()->get();
    $output = "<br><label>Buscar matrículas</label>";

              $output.='<br><select class="selectpicker" data-live-search="true" title="Buscar..." id="buscar_matriculas" name="buscar_matriculas">';

                    foreach ($matriculas as $key => $matricula) {

                       $output.='<option value="'.$matricula->IdMatricula.'">'.$matricula->Matricula.'</option>';
                     }
            
                
              $output .= "</select><br>";

              $output .=' <div class="table-responsive" style="margin-top: 20px;">
                    <table class="table" id="cod_matriculas">
                        <thead class="table-header">
                            <th>ID</th>                        
                            <th>Matricula</th>
                            <th>Delegación</th>
                            <th></th>
                        </thead>
                        <tbody ">
                        </tbody>
                    </table>
                </div>';

               return Response($output);

}

public function buscar_matricula(Request $request){
       if($request->ajax()){
        $matricula = DB::table('gesanc_matriculas')->where('IdMatricula',"=",$request->matricula)->first();

        $output = '<tr><td class="idmatricula">'.$matricula->IdMatricula.'</td>'.
                  '<td class="matricula">'.$matricula->Matricula.'</td>'.
                  '<td class="delegacion">'.$matricula->Deleg.'</td>'.
                  '<td class="delegacion"><a class="btn btn-sm btn-danger borrar_matricula"><i class="fa fa-trash"></i></a></td></tr>';

        return Response($output);

       }
}

public function imprimir_codigos_matricula(){
    $matriculas=json_decode($_GET['matriculas']);
    $templateWord = new TemplateProcessor('..\public\templates\gesanc_sanciones\imprimir_codigos.docx');
    $lista_codigos = "";
    $lista_matriculas ="";
    $filename = '..\public\temp\imprimir_codigos_matricula.docx';     
    foreach ($matriculas as $key => $matricula) {
         //Hallar el código de la matricula

        $matricula_contratada = DB::table('gesanc_matriculas')->whereRaw('Matricula = (select Matricula from eurits_db.gesanc_matriculas where idMatricula = '.$matricula.')')->first();

        $contratada = true;

        if($matricula_contratada){
            if($matricula_contratada->FecContr == null){
                    $contratada = false;
            }
        }

        $codigo = "";
        $indice = "";
        $sigla = "";

         if($matricula < 100000){
                    $codigo = sprintf("%05d", $matricula);
                    $indice = 0;
                }else{
                    $codigo = substr($matricula, 1);
                    $indice = round($matricula / 100000);
                    
                }

                 if(!$contratada){
                $codigo .="X";
            }

            switch ($indice) {
            case 0:
                $sigla ="I";
                break;
            case 1:
                $sigla ="H";
                break;
            case 2:
                $sigla ="S";
                break;
            case 3:
                $sigla ="L";
                break;
            case 4:
                $sigla ="P";
                break;
            case 5:
                $sigla ="G";
                break;
            case 6:
                $sigla ="I";
                break;
            case 7:
                $sigla ="H";
                break;
            case 9:
                $sigla ="M";
                break;
            case 10:
                $sigla ="E";
                break;
            default:
                $sigla ="";
                break;
        }


        $codigo_matricula = $sigla.$codigo;
        $num_matricula = $matricula_contratada->Matricula;
        $lista_codigos .= $codigo_matricula."<w:br/><w:br/>";
        $lista_matriculas .= $num_matricula."<w:br/><w:br/>";

    }

    $templateWord->setValue('codigo',$lista_codigos);
    $templateWord->setValue('matricula',$lista_matriculas);
    $templateWord->saveAs(storage_path($filename));

    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename=imprimir_codigos_matricula.docx; charset=iso-8859-1');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize('..\public\temp\imprimir_codigos_matricula.docx'));
    readfile('..\public\temp\imprimir_codigos_matricula.docx');
  

    $files = glob('..\public\temp\*'); // get all file names
    foreach($files as $file){ // iterate files
      if(is_file($file))
        unlink($file); // delete file
        }

}

public function exportar_excel(Request $request){

 $filtros = array();

            $consulta = "";

              if($request->alta_inicio != null && $request->alta_fin !=null){
                $alta_inicio = $request->alta_inicio;
                $alta_fin = $request->alta_fin;

                $FechaContrato = "FechaContrato BETWEEN '".$alta_inicio."' AND '".$alta_fin."'";

                $filtros[] = $FechaContrato;

                //$sanciones=gesanc_sanciones::whereRaw($consulta)->get();
              
            }

             if($request->baja_inicio != null && $request->baja_fin !=null){
                $baja_inicio = $request->baja_inicio;
                $baja_fin = $request->baja_fin;

                $FechaBaja = "baja BETWEEN '".$baja_inicio."' AND '".$baja_fin."'";

                $filtros[] = $FechaBaja;

                //$sanciones=gesanc_sanciones::whereRaw($consulta)->get();
              
            }

             if($request->venc_inicio != null && $request->venc_fin !=null){
                $venc_inicio = $request->venc_inicio;
                $venc_fin = $request->venc_fin;

                $FechaVencimiento = "vencimiento BETWEEN '".$venc_inicio."' AND '".$venc_fin."'";

                $filtros[] = $FechaVencimiento;

                //$sanciones=gesanc_sanciones::whereRaw($consulta)->get();
              
            }

            if($request->codContrato != null){
                $codContrato = $request->codContrato;
                $filtro_codContrato = "idcontrato = '".$codContrato."'";
                $filtros[] = $filtro_codContrato;
            }


             if($request->contratante != null){
                $contratante = $request->contratante;
                $filtro_contratante = "contratante = '".$contratante."'";
                $filtros[] = $filtro_contratante;
            } 

            if($request->empresa != null){
                $empresa = $request->empresa;
                $filtro_contratado = "cif = '".$empresa."'";
                $filtros[] = $filtro_contratado;
            } 

              if($request->tipoContrato != null){
                $tipoContrato = $request->tipoContrato;
                $filtro_tipoContrato = "Tipo = '".$tipoContrato."'";
                $filtros[] = $filtro_tipoContrato;
            }

            if($request->matricula != null){
                $matricula = $request->matricula;
                $filtro_matricula = "Matricula='".$matricula."'";
                $filtros[] = $filtro_matricula;
            }

             if($request->notas != null){
                $notas = $request->notas;
                $filtro_notas = "Notas='".$notas."'";
                $filtros[] = $filtro_notas;
            } 

             if($request->clave != null){
                $clave = $request->clave;
                $filtro_clave = "clavecontrato='".$clave."'";
                $filtros[] = $filtro_clave;
            } 

             if($request->comercial != null){
                $comercial = $request->comercial;
                $filtro_comercial = "Id-comercial = '".$comercial."'";
                $filtros[] = $filtro_comercial;
            }

              if($request->provincia != null){
                $provincia = $request->provincia;
                $filtro_provincia = "cif = (select cif from gesanc_clientes where provincia = '".$provincia."')";
                $filtros[] = $filtro_provincia;
            }  


            for ($i =count($filtros) - 1; $i >= 0 ; $i--) { 
                $consulta .= $filtros[$i];
                $consulta .= " ";

                if($i > 0){
                    $consulta .= "AND ";
                }
            }

           $contratos = DB::table('gesanc_contratos')->orderBy('Alta','DESC')->take(200)->get();

            
            if($consulta !=""){

          $contratos = DB::table('gesanc_contratos')->whereRaw($consulta)->orderBy('Alta','DESC')->take(200)->get();

             }

             $objPHPExcel = new PHPExcel();
        // Se asignan las propiedades del libro
        $objPHPExcel->getProperties()->setCreator("ITS") // Nombre del autor
        ->setLastModifiedBy("ITS") //Ultimo usuario que lo modificó
        ->setTitle("Sanciones filtradas") // Titulo
        ->setDescription("Sanciones filtradas"); //Descripción
        $titulosColumnas = array('Contrato'
                                ,'Tipo'
                                ,'Matricula'
                                ,'CIF'
                                ,'Fecha Alta'
                                ,'Fecha Baja');
        
        // Se combinan las celdas, para colocar ahí el titulo del reporte
        //$objPHPExcel->setActiveSheetIndex(0)
        //->mergeCells('A1:D1');
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1', $titulosColumnas[0])
        ->setCellValue('B1', $titulosColumnas[1])
        ->setCellValue('C1', $titulosColumnas[2])
        ->setCellValue('D1', $titulosColumnas[3])
        ->setCellValue('E1', $titulosColumnas[4])
        ->setCellValue('F1', $titulosColumnas[5]);

        $i=2;
        foreach ($contratos as $key => $contrato) {
              $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.$i, $contrato->idcontrato)
            ->setCellValue('B'.$i, $contrato->Tipo)
            ->setCellValue('C'.$i, $contrato->Matricula)
            ->setCellValue('D'.$i, $contrato->cif)
            ->setCellValue('E'.$i, date('d - m - Y', strtotime($contrato->Alta)))
            ->setCellValue('F'.$i, date('d - m - Y', strtotime($contrato->Baja)));
            $i++;
        }



    header('Content-Encoding: UTF-8');
    header('Content-type: text/csv; charset=UTF-8');
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="contratos_filtrados.xlsx"');
    header('Cache-Control: max-age=0');
    header("Pragma: no-cache");
    header("Expires: 0");
    header('Content-Transfer-Encoding: binary');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');


}

public function exportar_excel_europa(Request $request){

    $filtros = array();

            $consulta = "";

              if($request->alta_inicio != null && $request->alta_fin !=null){
                $alta_inicio = $request->alta_inicio;
                $alta_fin = $request->alta_fin;

                $FechaContrato = "FechaContrato BETWEEN '".$alta_inicio."' AND '".$alta_fin."'";

                $filtros[] = $FechaContrato;

                //$sanciones=gesanc_sanciones::whereRaw($consulta)->get();
              
            }

             if($request->baja_inicio != null && $request->baja_fin !=null){
                $baja_inicio = $request->baja_inicio;
                $baja_fin = $request->baja_fin;

                $FechaBaja = "baja BETWEEN '".$baja_inicio."' AND '".$baja_fin."'";

                $filtros[] = $FechaBaja;

                //$sanciones=gesanc_sanciones::whereRaw($consulta)->get();
              
            }

             if($request->venc_inicio != null && $request->venc_fin !=null){
                $venc_inicio = $request->venc_inicio;
                $venc_fin = $request->venc_fin;

                $FechaVencimiento = "vencimiento BETWEEN '".$venc_inicio."' AND '".$venc_fin."'";

                $filtros[] = $FechaVencimiento;

                //$sanciones=gesanc_sanciones::whereRaw($consulta)->get();
              
            }

             if($request->altaanexo_inicio != null && $request->altaanexo_fin !=null){
                $altaanexo_inicio = $request->altaanexo_inicio;
                $altaanexo_fin = $request->altaanexo_fin;

                $Alta_anexo = "(CodContrato in (select CodContrato from europa_anexos where Alta between '".$altaanexo_inicio."' and '".$altaanexo_fin."'))";

                $filtros[] = $Alta_anexo;

                //$sanciones=gesanc_sanciones::whereRaw($consulta)->get();
              
            }

            if($request->codContrato != null){
                $contratante = $request->codContrato;
                $filtro_codContrato = "codContrato = '".$codContrato."'";
                $filtros[] = $filtro_codContrato;
            }

             if($request->importe != null){
                $importe = $request->importe;
                $filtro_importe = "importeCto = '".$importe."'";
                $filtros[] = $filtro_importe;
            } 

             if($request->comision != null){
                $comision = $request->comision;
                $filtro_comision = "porcentComisEspecifica = '".$comision."'";
                $filtros[] = $filtro_comision;
            } 

             if($request->descuento != null){
                $descuento = $request->descuento;
                $filtro_descuento = "descuento = '".$descuento."'";
                $filtros[] = $filtro_descuento;
            }

             if($request->otrocomercial != null){
                $otrocomercial = $request->otrocomercial;
                $filtro_otrocomercial = "esCteDeOtroComercial = '".$otrocomercial."'";
                $filtros[] = $filtro_otrocomercial;
            }
             if($request->pendienteforma != null){
                $pendienteforma = $request->pendienteforma;
                $filtro_pendiente = "pdteFormaPago = '".$pendienteforma."'";
                $filtros[] = $filtro_pendiente;
            }    


             if($request->contratante != null){
                $contratante = $request->contratante;
                $filtro_contratante = "codContratante = '".$contratante."'";
                $filtros[] = $filtro_contratante;
            } 

            if($request->empresa != null){
                $empresa = $request->empresa;
                $filtro_contratado = "codContratado = '".$empresa."'";
                $filtros[] = $filtro_contratado;
            } 

            if($request->matricula != null){
                $matricula = $request->matricula;
                $filtro_matricula = "codContrato in (select codContrato from europa_anexos where Matricula='".$matricula."')";
                $filtros[] = $filtro_matricula;
            } 

             if($request->comercial != null){
                $comercial = $request->comercial;
                $filtro_comercial = "codComercial = '".$comercial."'";
                $filtros[] = $filtro_comercial;
            } 

            if($request->tipo_anexo != null){
                $tipo_anexo = $request->tipo_anexo;
                $filtro_tipoanexo = "codContrato in (select codContrato from europa_anexos where CodTipoAnexo='".$tipo_anexo."')";
                $filtros[] = $filtro_tipoanexo;
            } 

            if($request->tipo_abono != null){
                $tipo_abono = $request->tipo_abono;
                $filtro_tipoabono = "codContrato in (select codContrato from europa_abonos where tipoAbono='".$tipo_abono."')";
                $filtros[] = $filtro_tipoabono;
            }

            for ($i =count($filtros) - 1; $i >= 0 ; $i--) { 
                $consulta .= $filtros[$i];
                $consulta .= " ";

                if($i > 0){
                    $consulta .= "AND ";
                }
            }

            $contratos = DB::table(DB::raw("europa_contratos a, europa_contactos b"))->select(DB::raw("a.CodContrato,a.FechaContrato,a.Vencimiento,a.importeCto,a.observaciones,a.codTipoContrato, b.razonSocial"))->whereRaw("a.codContratado=b.CodContacto")->orderby("FechaContrato","DESC")->limit(200)->get();



            
            if($consulta !=""){

              $contratos = DB::table(DB::raw("europa_contratos a, europa_contactos b"))->select(DB::raw("a.CodContrato,a.FechaContrato,a.Vencimiento,a.importeCto,a.observaciones,a.codTipoContrato, b.razonSocial"))->whereRaw("a.codContratado=b.CodContacto AND ".$consulta)->orderby("FechaContrato","DESC")->limit(200)->get();

             }

             $objPHPExcel = new PHPExcel();
        // Se asignan las propiedades del libro
        $objPHPExcel->getProperties()->setCreator("ITS") // Nombre del autor
        ->setLastModifiedBy("ITS") //Ultimo usuario que lo modificó
        ->setTitle("Sanciones filtradas") // Titulo
        ->setDescription("Sanciones filtradas"); //Descripción
        $titulosColumnas = array('Cliente'
                                ,'Tipo'
                                ,'Fecha'
                                ,'Vencimiento'
                                ,'Importe'
                                ,'Observaciones');
        
        // Se combinan las celdas, para colocar ahí el titulo del reporte
        //$objPHPExcel->setActiveSheetIndex(0)
        //->mergeCells('A1:D1');
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1', $titulosColumnas[0])
        ->setCellValue('B1', $titulosColumnas[1])
        ->setCellValue('C1', $titulosColumnas[2])
        ->setCellValue('D1', $titulosColumnas[3])
        ->setCellValue('E1', $titulosColumnas[4])
        ->setCellValue('F1', $titulosColumnas[5]);

        $i=2;
        foreach ($contratos as $key => $contrato) {
              $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.$i, $contrato->razonSocial)
            ->setCellValue('B'.$i, $contrato->codTipoContrato)
            ->setCellValue('C'.$i, date('d - m - Y', strtotime($contrato->FechaContrato)))
            ->setCellValue('D'.$i, date('d - m - Y', strtotime($contrato->Vencimiento)))
            ->setCellValue('E'.$i, $contrato->importeCto)
            ->setCellValue('F'.$i, $contrato->observaciones);
            $i++;
        }



    header('Content-Encoding: UTF-8');
    header('Content-type: text/csv; charset=UTF-8');
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="contratos_filtrados.xlsx"');
    header('Cache-Control: max-age=0');
    header("Pragma: no-cache");
    header("Expires: 0");
    header('Content-Transfer-Encoding: binary');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');

    }

    public function exportar_excel_its(Request $request){
         $filtros = array();

            $consulta = "";

              if($request->alta_inicio != null && $request->alta_fin !=null){
                $alta_inicio = $request->alta_inicio;
                $alta_fin = $request->alta_fin;

                $FechaContrato = "(`Fecha Contrato` BETWEEN '".$alta_inicio."' AND '".$alta_fin."')";

                $filtros[] = $FechaContrato;

                //$sanciones=gesanc_sanciones::whereRaw($consulta)->get();
              
            }

             if($request->baja_inicio != null && $request->baja_fin !=null){
                $baja_inicio = $request->baja_inicio;
                $baja_fin = $request->baja_fin;

                $FechaBaja = "(Baja BETWEEN '".$baja_inicio."' AND '".$baja_fin."')";

                $filtros[] = $FechaBaja;

                //$sanciones=gesanc_sanciones::whereRaw($consulta)->get();
              
            }

             if($request->venc_inicio != null && $request->venc_fin !=null){
                $venc_inicio = $request->venc_inicio;
                $venc_fin = $request->venc_fin;

                $FechaVencimiento = "(Vencimiento BETWEEN '".$venc_inicio."' AND '".$venc_fin."')";

                $filtros[] = $FechaVencimiento;

                //$sanciones=gesanc_sanciones::whereRaw($consulta)->get();
              
            }

             if($request->altaanexo_inicio != null && $request->altaanexo_fin !=null){
                $altaanexo_inicio = $request->altaanexo_inicio;
                $altaanexo_fin = $request->altaanexo_fin;

                $Alta_anexo = "(IDContrato in (select IdAnexo from its_anexos where Alta between '".$altaanexo_inicio."' and '".$altaanexo_fin."'))";

                $filtros[] = $Alta_anexo;

                //$sanciones=gesanc_sanciones::whereRaw($consulta)->get();
              
            }

             if($request->bajaanexo_inicio != null && $request->bajaanexo_fin !=null){
                $bajaanexo_inicio = $request->bajaanexo_inicio;
                $bajaanexo_fin = $request->bajaanexo_fin;

                $Baja_anexo = "(IDContrato in (select IdAnexo from its_anexos where Baja between '".$bajaanexo_inicio."' and '".$bajaanexo_fin."'))";

                $filtros[] = $Baja_anexo;

                //$sanciones=gesanc_sanciones::whereRaw($consulta)->get();
              
            }

             if($request->contratante != null){
                $contratante = $request->contratante;
                $filtro_contratante = "Contratante = '".$contratante."'";
                $filtros[] = $filtro_contratante;
            } 

            if($request->empresa != null){
                $empresa = $request->empresa;
                $filtro_contratado = "Contratante in (select CIF from its_contactos where Empresa = '".$empresa."')";
                $filtros[] = $filtro_contratado;
            } 

            if($request->matricula != null){
                $matricula = $request->matricula;
                $filtro_matricula = "IDContrato in (select IdContrato from its_anexos where Matricula ='".$matricula."')";
                $filtros[] = $filtro_matricula;
            } 

            if($request->tipo_vehiculo != null){
                $tipo_vehiculo = $request->tipo_vehiculo;
                $filtro_tipovehiculo = "IDContrato IN (SELECT idAnexo from its_anexos where Matricula in (select Matricula from its_matriculas where TipoVehiculo = '".$tipo_vehiculo."'))";
                $filtros[] = $filtro_tipovehiculo;
            } 

            if($request->tipo_contrato != null){
                $tipo_contrato = $request->tipo_contrato;
                $filtro_tipocontrato = "TipoContrato = (select IdTipoContrato from `its_tipo de contrato` where TipoServicio ='".$tipo_contrato."')";
                $filtros[] = $filtro_tipocontrato;
            }

             if($request->cobertura != null){
                $cobertura = $request->cobertura;
                $filtro_cobertura = "IDContrato in (Select IdContrato from its_anexos where Cobertura = (select COBERTURA from its_cobertura where Descripcion='".$cobertura."'))";
                $filtros[] = $filtro_cobertura;
            }  
             if($request->pais != null){
                $pais = $request->pais;
                if($pais=="Espana"){
                    $pais="España";
                }
                $filtro_pais = "Contratante in (Select CIF from its_contactos where Pais = (select Codais from its_paises where Pais='".$pais."'))";
                $filtros[] = $filtro_pais;
            }

            for ($i =count($filtros) - 1; $i >= 0 ; $i--) { 
                $consulta .= $filtros[$i];
                $consulta .= " ";

                if($i > 0){
                    $consulta .= "AND ";
                }
            }

             $contratos = DB::table('its_contratos')->selectRaw("IDContrato,TipoContrato,`Fecha Contrato` as FechaContrato,Baja,Contratado,Contratante")->orderBy('FechaContrato','DESC')->take(20)->get();


            
            if($consulta !=""){

                 $contratos=DB::table('its_contratos')->selectRaw("IDContrato,TipoContrato,`Fecha Contrato` as FechaContrato,Baja,Contratado,Contratante")->whereRaw($consulta)->limit(200)->get();
             }

             $objPHPExcel = new PHPExcel();
        // Se asignan las propiedades del libro
        $objPHPExcel->getProperties()->setCreator("ITS") // Nombre del autor
        ->setLastModifiedBy("ITS") //Ultimo usuario que lo modificó
        ->setTitle("Sanciones filtradas") // Titulo
        ->setDescription("contratos filtrados"); //Descripción
        $titulosColumnas = array('Contrato'
                                ,'Tipo'
                                ,'Contratante'
                                ,'Cliente'
                                ,'Fecha Alta'
                                ,'Fecha Baja');
        
        // Se combinan las celdas, para colocar ahí el titulo del reporte
        //$objPHPExcel->setActiveSheetIndex(0)
        //->mergeCells('A1:D1');
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1', $titulosColumnas[0])
        ->setCellValue('B1', $titulosColumnas[1])
        ->setCellValue('C1', $titulosColumnas[2])
        ->setCellValue('D1', $titulosColumnas[3])
        ->setCellValue('E1', $titulosColumnas[4])
        ->setCellValue('F1', $titulosColumnas[5]);

        $i=2;
        foreach ($contratos as $key => $contrato) {
              $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.$i, $contrato->IDContrato)
            ->setCellValue('B'.$i, $contrato->TipoContrato)
            ->setCellValue('C'.$i, $contrato->Contratante)
            ->setCellValue('D'.$i, $contrato->Contratado)
            ->setCellValue('E'.$i, date('d - m - Y', strtotime($contrato->FechaContrato)))
            ->setCellValue('F'.$i, date('d - m - Y', strtotime($contrato->Baja)));
            $i++;
        }



    header('Content-Encoding: UTF-8');
    header('Content-type: text/csv; charset=UTF-8');
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="contratos_filtrados.xlsx"');
    header('Cache-Control: max-age=0');
    header("Pragma: no-cache");
    header("Expires: 0");
    header('Content-Transfer-Encoding: binary');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');


    }

    public function exportar_excel_tarjetas(Request $request){

        if($request->cif !=null){
             $tarjetas = DB::table(DB::raw("eurits_db.its_tarjetas a LEFT JOIN eurits_db.`its_tarjetas-matricula` b ON a.numTarjeta=b.numTarjeta"))->select(DB::raw("a.*,a.numTarjeta as 'tarjeta', b.*"))->whereRaw("(b.matricula IS NULL OR b.matricula IS NOT NULL) AND cif LIKE '".$request->cif."'")->get();
        }else{
             $tarjetas = DB::table(DB::raw("eurits_db.its_tarjetas a LEFT JOIN eurits_db.`its_tarjetas-matricula` b ON a.numTarjeta=b.numTarjeta"))->select(DB::raw("a.*,a.numTarjeta as 'tarjeta', b.*"))->whereRaw("(b.matricula IS NULL OR b.matricula IS NOT NULL) LIMIT 200")->get();
        }
       

        $objPHPExcel = new PHPExcel();
        // Se asignan las propiedades del libro
        $objPHPExcel->getProperties()->setCreator("ITS") // Nombre del autor
        ->setLastModifiedBy("ITS") //Ultimo usuario que lo modificó
        ->setTitle("Tarjetas filtradas") // Titulo
        ->setDescription("Tarjetas filtradas"); //Descripción
        $titulosColumnas = array('Tarjeta'
                                ,'Pin'
                                ,'Matricula'
                                ,'Caducidad'
                                ,'Doc Firmado'
                                ,'Entidad'
                                ,'Contratante');
        
        // Se combinan las celdas, para colocar ahí el titulo del reporte
        //$objPHPExcel->setActiveSheetIndex(0)
        //->mergeCells('A1:D1');
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1', $titulosColumnas[0])
        ->setCellValue('B1', $titulosColumnas[1])
        ->setCellValue('C1', $titulosColumnas[2])
        ->setCellValue('D1', $titulosColumnas[3])
        ->setCellValue('E1', $titulosColumnas[4])
        ->setCellValue('F1', $titulosColumnas[5])
        ->setCellValue('G1', $titulosColumnas[6]);
       
         $i=2;     
        foreach ($tarjetas as $key => $tarjeta) {

            $docfirmado = "";
            $entidad = "";
            $contratante = "";

            if($tarjeta->docFirmado==1){
                             $docfirmado = "si";
                            }
                            else{
                             $docfirmado = "no";
                            }
                            if($tarjeta->entidad==1){
                             $entidad = "CAIXA";
                            }
                            
                            elseif($tarjeta->entidad==2){
                             $entidad = "C2A";
                            }
                           
                            elseif($tarjeta->entidad==3){
                             $entidad = "FRANCE TRUCK";
                            }
                            else{
                           $entidad = $tarjeta->entidad;
                            }
                            if($tarjeta->empCom==10){
                              $contratante = "Europa ITS Soluciones SL";
                            }
                            elseif($tarjeta->empCom==9){
                              $contratante = "Soler y Martín Eurogestión, S.L";
                            }
                            else{
                              $contratante = "";
                            }

              $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.$i, $tarjeta->tarjeta)
            ->setCellValue('B'.$i, $tarjeta->pin)
            ->setCellValue('C'.$i, $tarjeta->matricula)
            ->setCellValue('D'.$i, date('d - m - Y', strtotime($tarjeta->caducidad)))
            ->setCellValue('E'.$i, $docfirmado)
            ->setCellValue('F'.$i, $entidad)
            ->setCellValue('G'.$i, $contratante);
            $i++;
        }


     header('Content-Encoding: UTF-8');
    header('Content-type: text/csv; charset=UTF-8');
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="tarjetas_filtradas.xlsx"');
    header('Cache-Control: max-age=0');
    header("Pragma: no-cache");
    header("Expires: 0");
    header('Content-Transfer-Encoding: binary');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');




}

}