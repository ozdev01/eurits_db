<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\gesanc_sanciones;

use App\gesanc_lista_motivos_denuncia;

use App\gesanc_reduccion;

use App\gesanc_contencioso;

use App\gesanc_clientes;

use DateTime;

use Illuminate\Database\QueryException;

use App\gesanc_contratos;

use PhpOffice\PhpWord\TemplateProcessor;

use PHPExcel; 
use PHPExcel_IOFactory; 

use DocxMerge\DocxMerge;

use Auth;

class EuropaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function europa()
    {

    

    /*$contratos = DB::select("SELECT a.codContrato, a.codTipoContrato, a.fechaContrato, b.razonSocial as 'Contratante', c.razonsocial as 'Contratado', d.Nombre, d.Apellidos from europa_contratos a INNER JOIN europa_contactos b ON a.codContratante=b.CodContacto INNER JOIN europa_contactos c ON a.codContratado=c.CodContacto INNER JOIN gesanc_comerciales d ON a.codComercial=d.`Id-comercial` order by a.FechaContrato LIMIT 200");*/

       $contratos = DB::table(DB::raw("europa_contratos a, europa_contactos b"))->select(DB::raw("a.FechaContrato,a.Vencimiento,a.importeCto,a.observaciones,a.codTipoContrato, b.razonSocial,a.codContrato"))->whereRaw("a.codContratado=b.CodContacto")->orderby("FechaContrato","DESC")->limit(200)->get();

       $empresas = DB::table("europa_contactos")->select("razonSocial","CodContacto")->get();

       $tipos_anexo = DB::table("europa_tipo_anexo")->select("tipoAnexo")->distinct()->get();

       $contratantes = DB::table("europa_contactos")->select("razonSocial","CodContacto")->whereRaw("CodContacto in (select codContratante from europa_contratos)")->get();

       $comerciales = DB::table("gesanc_comerciales")->selectRaw("CONCAT(Nombre,' ',Apellidos) as comercial,`Id-comercial` as ID")->whereRaw("`Id-comercial` in (select CodContacto from europa_contactos)")->get();

       $matriculas = DB::table("europa_matriculas")->select("Matricula")->whereRaw("Matricula in (select Matricula from europa_anexos)")->get();
   

        return view('contratos/europa/europa',['contratos'=> $contratos,'empresas'=>$empresas,'tipos_anexo'=>$tipos_anexo,'contratantes'=>$contratantes,'comerciales'=>$comerciales,'matriculas'=>$matriculas]);
    }

    public function ver_anexos()
    {

    

    /*$contratos = DB::select("SELECT a.codContrato, a.codTipoContrato, a.fechaContrato, b.razonSocial as 'Contratante', c.razonsocial as 'Contratado', d.Nombre, d.Apellidos from europa_contratos a INNER JOIN europa_contactos b ON a.codContratante=b.CodContacto INNER JOIN europa_contactos c ON a.codContratado=c.CodContacto INNER JOIN gesanc_comerciales d ON a.codComercial=d.`Id-comercial` order by a.FechaContrato LIMIT 200");*/

       //$contratos = DB::table(DB::raw("europa_contratos a, europa_contactos b"))->select(DB::raw("a.FechaContrato,a.Vencimiento,a.importeCto,a.observaciones,a.codTipoContrato, b.razonSocial,a.codContrato"))->whereRaw("a.codContratado=b.CodContacto")->orderby("FechaContrato","DESC")->get();

       $anexos = DB::table(DB::raw("europa_anexos a, europa_contratos b, europa_contactos c, europa_tipo_anexo d "))->select(DB::raw("a.CodAnexo,c.razonSocial as 'cliente' ,d.tipoAnexo as 'tipo',a.Matricula as 'matricula',b.fechaContrato as'fechadeContrato',a.Alta as 'fechaAlta',a.Baja as 'fechaBaja'"))->whereRaw("a.CodContrato=b.codContrato and b.codContratado=c.CodContacto and a.CodTipoAnexo=d.CodtipoAnexo")->orderby("fechaAlta","DESC")->limit(200)->get();

       $empresas = DB::table("europa_contactos")->select("razonSocial","CodContacto")->get();

       $tipos_anexo = DB::table("europa_tipo_anexo")->select("tipoAnexo")->distinct()->get();

       $contratantes = DB::table("europa_contactos")->select("razonSocial","CodContacto")->whereRaw("CodContacto in (select codContratante from europa_contratos)")->get();

       $comerciales = DB::table("gesanc_comerciales")->selectRaw("CONCAT(Nombre,' ',Apellidos) as comercial,`Id-comercial` as ID")->whereRaw("`Id-comercial` in (select CodContacto from europa_contactos)")->get();

       $matriculas = DB::table("europa_matriculas")->select("Matricula")->whereRaw("Matricula in (select Matricula from europa_anexos) and TipoVehiculo='C'")->get();

       $remolques = DB::table("europa_matriculas")->select("Matricula")->whereRaw("Matricula in (select Matricula from europa_anexos) and TipoVehiculo='R'")->get();
   

        return view('contratos/europa/anexos/anexos',['anexos'=> $anexos,'empresas'=>$empresas,'tipos_anexo'=>$tipos_anexo,'contratantes'=>$contratantes,'comerciales'=>$comerciales,'matriculas'=>$matriculas,'remolques'=>$remolques]);
    }

     

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $clientes = DB::table("europa_contactos")->select("razonSocial","CodContacto")->get();

       $tipos_anexo = DB::table("europa_tipo_anexo")->select("tipoAnexo")->distinct()->get();

       $contratantes = DB::table("europa_contactos")->select("razonSocial","CodContacto")->whereRaw("CodContacto in (select codContratante from europa_contratos)")->get();

       $comerciales = DB::table("gesanc_comerciales")->selectRaw("CONCAT(Nombre,' ',Apellidos) as comercial,`Id-comercial` as ID")->whereRaw("`Id-comercial` in (select CodContacto from europa_contactos)")->get();

        $tipo_contrato = DB::table("europa_tipo_contrato")->get();

        return view('contratos/europa/nuevo',['clientes'=>$clientes,'contratantes'=>$contratantes,'comerciales'=>$comerciales,'tipo_contrato'=>$tipo_contrato]);
        
    }

    public function create_anexo($id)
    {

       $contrato = DB::table("europa_contratos")->where("codContrato","=",$id)->first();

       $cliente = DB::table("europa_contactos")->whereRaw("codContacto =(select codContratado from europa_contratos where codContrato='".$id."')")->first();

        $matriculas = DB::table("europa_matriculas")->select("Matricula")->whereRaw("Matricula in (select Matricula from europa_anexos)")->get();

        $tipos_anexo = DB::table("europa_tipo_anexo")->select("tipoAnexo","CodTipoAnexo")->whereRaw("codTipoContrato = (select codTipoContrato from europa_contratos where codContrato='".$id."')")->get();

        return view('contratos/europa/anexos/nuevo',['tipos_anexo'=>$tipos_anexo,'contrato'=>$contrato,'cliente'=>$cliente,'matriculas'=>$matriculas]);
        
    }

    public function precio_anexo(Request $request){
         if($request->ajax()){

              //DB::enableQueryLog();
      
           $precio_anexo = DB::table("europa_precios_venta")->select("precioMinimoVenta")->whereRaw("codCondiciones='".$request->codCondiciones."' and CodTipoAnexo = '".$request->CodTipoAnexo."'")->get();

             //dd(DB::getQueryLog());

           foreach ($precio_anexo as $key => $precio) {

            if($precio->precioMinimoVenta!=null){

                 return  Response($precio->precioMinimoVenta);

           }else{

                return  Response("0");
           }

            }
    }
}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_contrato(Request $request)
    {
         //TODO
        if($request->ajax()){

            $datos = array();
            parse_str($request->datos,$datos);

            $empresa = null;
            $contratante = null;
            $comercial= null;
            $tipoContrato = null;
            $contratante = null;
            $fechaContrato = null;
            $fechavencimiento = null;

            $cod_contrato = null;

            }

            if(array_key_exists("EC",$datos)){
               $EC = 1;
            }

            if($datos["empresa"]!=null){
                $empresa = $datos["empresa"];
            }

            if($datos["contratante"]!=null){
                $contratante = $datos["contratante"];
            }

            if($datos["comercial"]!=null){
                $comercial = $datos["comercial"];
            }

            if($datos["tipo_contrato"]!=null){
                $tipoContrato = $datos["tipo_contrato"];
            }

    
            if($datos["fechaContrato"] !=null){
                $fechaContrato = DateTime::createFromFormat('d/m/Y', $datos["fechaContrato"]);
                date_time_set($fechaContrato, 00, 00);
            }

             if($datos["fechavencimiento"] !=null){
                $fechavencimiento = DateTime::createFromFormat('d/m/Y', $datos["fechavencimiento"]);
                date_time_set($fechavencimiento, 00, 00);        
            }



            try { 

                //Seleccionar las condiciones del contrato según los parametros establecidos
            $codCondiciones = DB::table("europa_condiciones_contrato")->select("codCondiciones")->whereRaw("CodTarifa = (select CodTarifa from europa_tarifas where CodTipoContrato='".$tipoContrato."' and codEmpresa='".$contratante."' and CodTarifa in (select CodTarifa from europa_tarifas_comercial where CodComercial='".$comercial."'))")->first();

              DB::table('europa_contratos')->insertGetId(
                ['codContratado' => $empresa,'codTipoContrato' => $tipoContrato,'codContratante'=>$contratante,'codComercial'=>$comercial, 'fechaContrato' => $fechaContrato, 'vencimiento' => $fechavencimiento,'codCondiciones'=>$codCondiciones->codCondiciones]);

              $contrato_creado = DB::table('europa_contratos')->orderby('fechaContrato', 'desc')->first();
             
              $cod_contrato = $contrato_creado->codContrato;
          

            return  Response($cod_contrato);

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }
    }

    public function store_franquicia(Request $request)
    {
         //TODO
        if($request->ajax()){

            $datos = array();
            parse_str($request->datos,$datos);

            $Tarifa = null;
            $contratante = null;
            $tipo_contrato = null;
            $fechaEfecto = null;
            $porcentajeComision = null;
            $pctjeComisionExcep = null;
            $porcentajeDeduccion = null;



            if($datos["Tarifa"]!=null){
                $Tarifa = $datos["Tarifa"];
            }

            if($datos["contratante"]!=null){
                $contratante = $datos["contratante"];
            }

            if($datos["tipo_contrato"]!=null){
                $tipo_contrato = $datos["tipo_contrato"];
            }

    
            if($datos["fechaEfecto"] !=null){
                $fechaEfecto = DateTime::createFromFormat('d/m/Y', $datos["fechaEfecto"]);
                date_time_set($fechaEfecto, 00, 00);
            }

             if($datos["porcentajeComision"]!=null){
                $porcentajeComision = $datos["porcentajeComision"];
            }
             if($datos["pctjeComisionExcep"]!=null){
                $pctjeComisionExcep = $datos["pctjeComisionExcep"];
            }
             if($datos["porcentajeDeduccion"]!=null){
                $porcentajeDeduccion = $datos["porcentajeDeduccion"];
            }
        

            try { 

                //Seleccionar las condiciones del contrato según los parametros establecidos
    

              DB::table('europa_tarifas')->insertGetId(
                ['Tarifa'=>$Tarifa, 'CodTipoContrato'=>$tipo_contrato, 'codEmpresa'=>$contratante]);

               $Tarifa_creada = DB::table('europa_tarifas')->orderby('CodTarifa', 'DESC')->first();

               DB::table('europa_condiciones_contrato')->insertGetId(
                ['FechaEfecto'=>$fechaEfecto, 'CodTarifa'=>$Tarifa_creada->CodTarifa, 'porcentajeComision'=>$porcentajeComision, 'pctjeComisionExcep'=>$pctjeComisionExcep, 'fechaModif'=>date('Y-m-d H:i:s'), 'porcentajeDeduccion'=>$porcentajeDeduccion]);

             
              $Tarifa = $Tarifa_creada->CodTarifa;
          

            return  Response($Tarifa);

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }
    }

  }

    public function store_anexo(Request $request)
    {
         //TODO
        if($request->ajax()){

            $datos = array();
            parse_str($request->datos,$datos);

            $codContratado = null;
            $codContratante = null;
            $codComercial= null;
            $CodTipoAnexo = null;
            $codContrato = null;
            $importe = null;
            $matricula = null;
            $alta = null;
            $vencimiento = null;
            $cod_contrato = null;
            $notas = null;

            $esrenovacion = 0;
            $norenovar = 0;

            }

            if(array_key_exists("esrenovacion",$datos)){
               $esrenovacion = 1;
            }

            if(array_key_exists("norenovar",$datos)){
               $norenovar = 1;
            }

            if($datos["codContratado"]!=null){
                $codContratado = $datos["codContratado"];
            }

            if($datos["codContratante"]!=null){
                $codContratante = $datos["codContratante"];
            }

            if($datos["codComercial"]!=null){
                $codComercial = $datos["codComercial"];
            }

             if($datos["codContrato"]!=null){
                $codContrato = $datos["codContrato"];
            }

            if($datos["tipo_anexo"]!=null){
                $CodTipoAnexo = $datos["tipo_anexo"];
            }


            if($datos["matricula"]!=null){
                $matricula = $datos["matricula"];
            }

            if($datos["importe"]!=null){
                $importe = $datos["importe"];
            }

            if($datos["notas"]!=null){
                $notas = $datos["notas"];
            }

    
            if($datos["alta"] !=null){
                $alta = DateTime::createFromFormat('d/m/Y', $datos["alta"]);
                date_time_set($alta, 00, 00);
            }

        


            try { 

                //Seleccionar las condiciones del contrato según los parametros establecidos



              DB::table('europa_anexos')->insertGetId(
                ['CodContrato'=>$codContrato, 'Matricula'=>$matricula, 'CodTipoAnexo'=>$CodTipoAnexo, 'Alta'=>$alta, 'importeVenta'=>$importe,'FechaIntroDat'=>date('Y-m-d H:i:s'), 'FechaModif'=>date('Y-m-d H:i:s'), 'Notas'=>$notas,'esRenovacion'=>$esrenovacion, 'sinPrecioMinimo'=>0, 'NOrenovar'=>$norenovar]);

              

                 $contrato = DB::table("europa_contratos")->select("descuento")->where("codContrato","=",$codContrato)->first();

              $anexos_contrato = DB::table("europa_anexos")->selectRaw("sum(importeVenta) as 'totalventa'")->whereRaw("CodAnexo not in (select CodSustituido from europa_anexos_sustituciones) and codContrato='".$codContrato."'")->first();

              $suma_anexos = $anexos_contrato->totalventa;

              $descuento_comercial = 0;
              if($contrato->descuento !=null){
                $descuento_comercial = $contrato->descuento;
            }
              
              $abonos_contrato = DB::table("europa_abonos")->whereRaw("codContrato ='".$codContrato."' and (tipoAbono =-2 OR tipoAbono=-1)")->get();

            $dcto_abonos = 0;

              if($abonos_contrato !=null){
                foreach ($abonos_contrato as $key => $abono) {
                    $dcto_abonos = $dcto_abonos + $abono->importe;
                }
              }else{
                $dcto_abonos = 0;
              }

              $importe_calculado = $suma_anexos - $descuento_comercial - $dcto_abonos;



            DB::table('europa_contratos')->where("codContrato","=",$codContrato)->update(
                ['importeCto'=>$importe_calculado]);

              $anexo_creado = DB::table('europa_anexos')->orderby('alta', 'desc')->first();
             
              $cod_anexo = $anexo_creado->CodAnexo;
          

            return  Response($cod_anexo);

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }
    }



    public function asignarCondicionesContrato ($codContratante, $CodComercial, $CodTipoContrato, $FechaContrato){

         try { 
              DB::table('europa_contratos')->insertGetId(
                ['codContratado' => $empresa,'codTipoContrato' => $tipoContrato,'codContratante'=>$contratante,'codComercial'=>$comercial, 'fechaContrato' => $fechaContrato, 'vencimiento' => $fechavencimiento]
                );

              $contrato_creado = DB::table('europa_contratos')->orderby('fechaContrato', 'desc')->first();

             
              $cod_contrato = $contrato_creado->codContrato;
          

            return  Response($cod_contrato);

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

     public function show_europa($id)
    {

      $contrato = DB::table(DB::raw("europa_contratos a, europa_contactos b"))->select(DB::raw("a.*,b.razonSocial"))->whereRaw("a.codContratado=b.CodContacto AND a.CodContrato='".$id."'")->get();

      $tipo_contrato = DB::table("europa_tipo_contrato")->get();

      //$anexos = DB::table('europa_anexos')->where('codContrato',$id)->get();

      $anexos = DB::table(DB::raw("europa_anexos a, europa_tipo_anexo b"))->select(DB::raw("a.*,b.tipoAnexo"))->whereRaw(" a.CodTipoAnexo=b.CodtipoAnexo AND a.CodContrato='".$id."'")->get();


      $empresas = DB::table("europa_contactos")->select("razonSocial","CodContacto","observaciones")->whereRaw("CodContacto =(select codContratado from europa_contratos where CodContrato='".$id."')")->get();

      $contratantes = DB::table("europa_contactos")->select("razonSocial","CodContacto")->whereRaw("CodContacto in (select codContratante from europa_contratos)")->get();

       $comerciales = DB::table("gesanc_comerciales")->selectRaw("CONCAT(Nombre,' ',Apellidos) as comercial,`Id-comercial` as ID")->whereRaw("`Id-comercial` in (select CodContacto from europa_contactos)")->get();

       $suma =DB::table("europa_anexos")->selectRaw("SUM(importeVenta) as suma")->where("CodContrato",$id)->get();

       $tipo_anexo = DB::table("europa_tipo_anexo")->select("tipoAnexo")->whereRaw("CodtipoAnexo = (select CodtipoAnexo from europa_contratos where codContrato=CodContrato='".$id."')");





      $empresa_comercial = "";
      $id_comercial = "";
      $suma_anexos = 0;

      foreach ($contrato as $key => $dato) {
        $empresa_comercial = $dato->codContratante;
      }

      foreach ($contrato as $key => $dato) {
        $id_comercial = $dato->codComercial;
      }

      foreach ($suma as $key => $dato) {
        $suma_anexos = $dato->suma;
      }



   

      $otros_contratos = DB::table(DB::raw("europa_contratos a, europa_contactos b"))->select(DB::raw("a.*,b.razonSocial"))->whereRaw("a.codContratado=b.CodContacto AND a.CodContratado=(select CodContratado from europa_contratos where CodContrato ='".$id."')")->get();

        return view('contratos/europa/detail', compact('contrato'),['contrato'=>$contrato,'anexos'=>$anexos,'empresas'=>$empresas,'contratantes'=>$contratantes,'comerciales'=>$comerciales,'otros_contratos'=>$otros_contratos,'empresa_comercial'=>$empresa_comercial,'id_comercial'=>$id_comercial,'suma_anexos'=>$suma_anexos,'tipo_anexo'=>$tipo_anexo]);
    }

    public function show_anexos($id){
        $contrato = DB::table("europa_contratos")->whereRaw("codContrato = (select CodContrato from europa_anexos where CodAnexo ='".$id."')")->first();

        $anexo = DB::table(DB::raw("europa_anexos a,europa_tipo_anexo b"))->select(DB::raw("a.*, CASE when CodMotivoBaja is null then 'ninguno' else (select motivobaja from `europa_motivos baja` where CodMotivoBaja=a.CodMotivoBaja) end as 'Motivo',b.tipoAnexo as 'tipo',a.CodMotivoBaja as 'CodMotivoBaja'"))->whereRaw("a.CodTipoAnexo=b.CodTipoAnexo and a.CodAnexo='".$id."'")->first();

        $cliente = DB::table("europa_contactos")->whereRaw("codContacto =(select codContratado from europa_contratos where codContrato='".$contrato->codContrato."')")->first();

        $matriculas = DB::table("europa_matriculas")->select("Matricula")->whereRaw("TipoVehiculo='C'")->get();

        $matriculas_remolque = DB::table("europa_matriculas")->select("Matricula")->whereRaw("TipoVehiculo='R'")->get();

        $tipos_anexo = DB::table("europa_tipo_anexo")->select("tipoAnexo","CodTipoAnexo")->whereRaw("codTipoContrato = (select codTipoContrato from europa_contratos where codContrato='".$contrato->codContrato."')")->get();

        $motivos_baja = DB::table(DB::raw("`europa_motivos baja`"))->get();

        //$remolques = DB::table("europa_anexos_remolques")->where("codAnexo","=",$id)->get();

        $remolques = DB::table(DB::raw("europa_anexos_remolques a"))->select(DB::raw("a.*, CASE when codMotivoBaja is null then 'ninguno' else (select motivobaja from `europa_motivos baja` where CodMotivoBaja=a.codMotivoBaja) end as 'Motivo'"))->whereRaw("a.CodAnexo='".$id."'")->get();

        $sustituido_por = DB::table("europa_anexos")->select("Matricula")->whereRaw("CodAnexo =(select CodSustituyente from europa_anexos_sustituciones where CodSustituido ='".$id."' )")->first();

        $sustituido = null;

        if($sustituido_por!=null){
            $sustituido = $sustituido_por->Matricula;
        }

        $sustituye = null;

         $sustituye_a = DB::table("europa_anexos")->select("Matricula")->whereRaw("CodAnexo =(select CodSustituido from europa_anexos_sustituciones where CodSustituyente ='".$id."' )")->first();

         if($sustituye_a!=null){
            $sustituye = $sustituye_a->Matricula;
        }

       
        return view('contratos/europa/anexos/detail',['tipos_anexo'=>$tipos_anexo,'contrato'=>$contrato,'cliente'=>$cliente,'matriculas'=>$matriculas,'motivos_baja'=>$motivos_baja,'remolques'=>$remolques,'sustituido'=>$sustituido,'sustituye'=>$sustituye,'anexo'=>$anexo,'matriculas_remolque'=>$matriculas_remolque]);
        

    }

    public function update_anexo(Request $request){
         //TODO
        if($request->ajax()){

            $CodAnexo = $request->CodAnexo;

            $datos = array();
            parse_str($request->datos,$datos);

            $codContratado = null;
            $codContratante = null;
            $codComercial= null;
            $CodTipoAnexo = null;
            $codContrato = null;
            $importe = null;
            $matricula = null;
            $alta = null;
            $vencimiento = null;
            $cod_contrato = null;
            $notas = null;
            $baja = null;
            $motivo_baja = null;
            $esrenovacion = 0;
            $norenovar = 0;

            }

            if(array_key_exists("esrenovacion",$datos)){
               $esrenovacion = 1;
            }

            if(array_key_exists("norenovar",$datos)){
               $norenovar = 1;
            }

            if($datos["codContratado"]!=null){
                $codContratado = $datos["codContratado"];
            }

            if($datos["codContratante"]!=null){
                $codContratante = $datos["codContratante"];
            }

            if($datos["codComercial"]!=null){
                $codComercial = $datos["codComercial"];
            }

             if($datos["codContrato"]!=null){
                $codContrato = $datos["codContrato"];
            }

            if($datos["tipo_anexo"]!=null){
                $CodTipoAnexo = $datos["tipo_anexo"];
            }

            if($datos["motivo_baja"]!=null){
                $motivo_baja = $datos["motivo_baja"];
            }

            if($datos["matricula"]!=null){
                $matricula = $datos["matricula"];
            }

            if($datos["importe"]!=null){
                $importe = $datos["importe"];
            }

            if($datos["notas"]!=null){
                $notas = $datos["notas"];
            }

    
            if($datos["alta"] !=null){
                $alta = DateTime::createFromFormat('d/m/Y', $datos["alta"]);
                date_time_set($alta, 00, 00);
            }

             if($datos["baja"] !=null){
                $baja = DateTime::createFromFormat('d/m/Y', $datos["baja"]);
                date_time_set($baja, 00, 00);
            }

        


            try { 

                //Seleccionar las condiciones del contrato según los parametros establecidos



              DB::table('europa_anexos')->where("CodAnexo","=",$CodAnexo)->update(
                ['CodContrato'=>$codContrato, 'Matricula'=>$matricula, 'CodTipoAnexo'=>$CodTipoAnexo, 'Alta'=>$alta, 'importeVenta'=>$importe,'FechaIntroDat'=>date('Y-m-d H:i:s'), 'FechaModif'=>date('Y-m-d H:i:s'), 'Notas'=>$notas,'esRenovacion'=>$esrenovacion, 'sinPrecioMinimo'=>0, 'NOrenovar'=>$norenovar]);

              //$importe_anterior = DB::table("europa_contratos")->select("importeCto")->where("codContrato","=",$codContrato)->first();

              //$importeCto = $importe_anterior->importeCto+$importe;

              $contrato = DB::table("europa_contratos")->select("descuento")->where("codContrato","=",$codContrato)->first();

              $anexos_contrato = DB::table("europa_anexos")->selectRaw("sum(importeVenta) as 'totalventa'")->whereRaw("CodAnexo not in (select CodSustituido from europa_anexos_sustituciones) and codContrato='".$codContrato."'")->first();

              $suma_anexos = $anexos_contrato->totalventa;

              $descuento_comercial = 0;
              if($contrato->descuento !=null){
                $descuento_comercial = $contrato->descuento;
            }
              
              $abonos_contrato = DB::table("europa_abonos")->whereRaw("codContrato ='".$codContrato."' and (tipoAbono =-2 OR tipoAbono=-1)")->get();

            $dcto_abonos = 0;

              if($abonos_contrato !=null){
                foreach ($abonos_contrato as $key => $abono) {
                    $dcto_abonos = $dcto_abonos + $abono->importe;
                }
              }else{
                $dcto_abonos = 0;
              }

              $importe_calculado = $suma_anexos - $descuento_comercial - $dcto_abonos;



            DB::table('europa_contratos')->where("codContrato","=",$codContrato)->update(
                ['importeCto'=>$importe_calculado]);

              $anexo_creado = DB::table('europa_anexos')->orderby('alta', 'desc')->first();
             
              $cod_anexo = $anexo_creado->CodAnexo;
          

            return  Response($cod_anexo);

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }
    }

    public function create_anexo_remolque (Request $request){

         //TODO
        if($request->ajax()){

            $numOrdenGesanc = 1;

            $orden = DB::table('europa_anexos_remolques')->select("numOrdenGesanc")->where("codAnexo","=",$request->CodAnexo)->first();

            if($orden!=null){

            $numOrdenGesanc = $orden->numOrdenGesanc;
            $numOrdenGesanc++;
            }

            $alta = DateTime::createFromFormat('d/m/Y', $request->alta_remolque);
                date_time_set($alta, 00, 00);

        
            try { 

                //Seleccionar las condiciones del contrato según los parametros establecidos
              DB::table('europa_anexos_remolques')->insertGetId(
                ['codAnexo'=>$request->CodAnexo, 'numOrdenGesanc'=>$numOrdenGesanc, 'matricula'=>$request->matricula, 'idContrato'=>$request->codContrato, 'Alta'=>$alta,'fechaintrodatos'=>date('Y-m-d H:i:s'), 'fechamodifacion'=>date('Y-m-d H:i:s')]);

            return  Response("El remolque se ha añadido al anexo");

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }

            }
    }

    public function update_anexo_remolque(Request $request){
         if($request->ajax()){

           
            $alta = DateTime::createFromFormat('d/m/Y', $request->alta);
                date_time_set($alta, 00, 00);

            $baja = null;
            if($request->baja!=null){
                  $baja = DateTime::createFromFormat('d/m/Y', $request->baja);
                date_time_set($baja, 00, 00);
            }
          
            try { 

                //Seleccionar las condiciones del contrato según los parametros establecidos
              DB::table('europa_anexos_remolques')->where("codAnexoRemolque","=",$request->codAnexoRemolque)->update(
                ['matricula'=>$request->matricula_remolque,'Alta'=>$alta,'Baja'=>$baja,'codMotivoBaja'=>$request->motivobaja,'fechamodifacion'=>date('Y-m-d H:i:s')]);

            return  Response("El remolque se ha modificado correctamente");

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }

            }

    }

    public function eliminar_anexo_remolque(Request $request){

            if($request->ajax()){
          
            try { 

                //Seleccionar las condiciones del contrato según los parametros establecidos
              DB::table('europa_anexos_remolques')->where("codAnexoRemolque","=",$request->codAnexoRemolque)->delete();

            return  Response("el remolque se ha eliminado correctamente del anexo");

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }

            }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_contrato(Request $request, $id)
    {
        //TODO
        if($request->ajax()){

            $datos = array();
            parse_str($request->datos,$datos);

           
            $tipo_contrato = null;
            $tipo = null;
            $contratante = null;
            $comercial = null;
            $fechaContrato = null;
            $vencimiento = null;
            $baja = null;
            $observaciones_contrato = null;
            $observaciones_cliente = null;
            $importe_anexos = null;
            $importeCto = null;
            $descuento_comercial = null;
            $comision = null;
            $comisionespec = null;
            $facturar = null;
            $provisionfondos = 0;
            $pendienteforma = 0;
            $otrocomercial = 0;
            $obviar = 0;

            

           
            }

            if(array_key_exists("provisionfondos",$datos)){
               $provisionfondos = 1;
            }
             if(array_key_exists("otrocomercial",$datos)){
               $otrocomercial = 1;
            }
             if(array_key_exists("obviar",$datos)){
               $obviar = 1;
            }

             if(array_key_exists("pendienteforma",$datos)){
               $pendienteforma = 1;
            }

            if($datos["tipo_contrato"]!=null){
                $tipo_contrato = $datos["tipo_contrato"];
            }
            if($datos["contratante"]!=null){
                $contratante = $datos["contratante"];
            }

            if($datos["comercial"]!=null){
                $comercial = $datos["comercial"];
            }
            if($datos["observaciones_contrato"]!=null){
                $observaciones_contrato = $datos["observaciones_contrato"];
            }
            if($datos["observaciones_cliente"]!=null){
                $observaciones_cliente = $datos["observaciones_cliente"];
            }
            if($datos["importe_anexos"]!=null){
                $importe_anexos = $datos["importe_anexos"];
            }
            if($datos["importeCto"]!=null){
                $importeCto = $datos["importeCto"];
            }
            if($datos["descuento_comercial"]!=null){
                $descuento_comercial = $datos["descuento_comercial"];
            }
            if($datos["comision"]!=null){
                $comision = $datos["comision"];
            }
            if($datos["comisionespec"]!=null){
                $comisionespec = $datos["comisionespec"];
            }

            if($datos["facturar"]=="Indefinido"){
                $facturar = 0;
            }
            elseif($datos["facturar"]=="Facturar"){
                $facturar = 1;
            }
            elseif($datos["facturar"]=="no Facturar"){
                $facturar = 2;
            }

    
            if($datos["fechaContrato"] !=null){
                $fechaContrato = DateTime::createFromFormat('d/m/Y', $datos["fechaContrato"]);
                date_time_set($fechaContrato, 00, 00);
            }

            if($datos["vencimiento"]!=null){
                $vencimiento = DateTime::createFromFormat('d/m/Y', $datos["vencimiento"]);
                date_time_set($vencimiento, 00, 00);
            }

            if($datos["baja"]!=null){
                $baja = DateTime::createFromFormat('d/m/Y', $datos["baja"]);
                date_time_set($baja, 00, 00);
            }

           
            

              if($datos["tipo_contrato"]!=null){
              $tipo_contrato = $datos["tipo_contrato"];
            }

            try { 

               
       

               DB::table('europa_contratos')->where('CodContrato', $id)->update(
                    ['codContratante' => $contratante, 'codComercial'=>$comercial, 'codTipoContrato'=>$tipo_contrato, 'fechaContrato'=>$fechaContrato, 'baja'=>$baja, 'vencimiento'=>$vencimiento,'totalLiquidacion'=>$comision,'importeCto'=>$importeCto, 'descuento'=>$descuento_comercial, 'conProvisionFondos'=>$provisionfondos, 'observaciones'=>$observaciones_contrato,'pdteFormaPago'=>$pendienteforma, 'facturar'=>$facturar, 'porcentComisEspecifica'=>$comisionespec, 'esCteDeOtroComercial'=>$otrocomercial, 'Obviar'=>$obviar]);

                

            return  Response("El Contrato se ha editado correctamente.");

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function filtra_europa(Request $request){
            
        $output="<p style='text-align:center;'>No se han encontrado resultados.</p>";

            if($request->ajax()){

            $filtros = array();

            $consulta = "";

              if($request->alta_inicio != null && $request->alta_fin !=null){
                $alta_inicio = $request->alta_inicio;
                $alta_fin = $request->alta_fin;

                $FechaContrato = "FechaContrato BETWEEN '".$alta_inicio."' AND '".$alta_fin."'";

                $filtros[] = $FechaContrato;

                //$sanciones=gesanc_sanciones::whereRaw($consulta)->get();
              
            }

             if($request->baja_inicio != null && $request->baja_fin !=null){
                $baja_inicio = $request->baja_inicio;
                $baja_fin = $request->baja_fin;

                $FechaBaja = "baja BETWEEN '".$baja_inicio."' AND '".$baja_fin."'";

                $filtros[] = $FechaBaja;

                //$sanciones=gesanc_sanciones::whereRaw($consulta)->get();
              
            }

             if($request->venc_inicio != null && $request->venc_fin !=null){
                $venc_inicio = $request->venc_inicio;
                $venc_fin = $request->venc_fin;

                $FechaVencimiento = "vencimiento BETWEEN '".$venc_inicio."' AND '".$venc_fin."'";

                $filtros[] = $FechaVencimiento;

                //$sanciones=gesanc_sanciones::whereRaw($consulta)->get();
              
            }

             if($request->altaanexo_inicio != null && $request->altaanexo_fin !=null){
                $altaanexo_inicio = $request->altaanexo_inicio;
                $altaanexo_fin = $request->altaanexo_fin;

                $Alta_anexo = "(CodContrato in (select CodContrato from europa_anexos where Alta between '".$altaanexo_inicio."' and '".$altaanexo_fin."'))";

                $filtros[] = $Alta_anexo;

                //$sanciones=gesanc_sanciones::whereRaw($consulta)->get();
              
            }

            if($request->codContrato != null){
                $contratante = $request->codContrato;
                $filtro_codContrato = "codContrato = '".$codContrato."'";
                $filtros[] = $filtro_codContrato;
            }

             if($request->importe != null){
                $importe = $request->importe;
                $filtro_importe = "importeCto = '".$importe."'";
                $filtros[] = $filtro_importe;
            } 

             if($request->comision != null){
                $comision = $request->comision;
                $filtro_comision = "porcentComisEspecifica = '".$comision."'";
                $filtros[] = $filtro_comision;
            } 

             if($request->descuento != null){
                $descuento = $request->descuento;
                $filtro_descuento = "descuento = '".$descuento."'";
                $filtros[] = $filtro_descuento;
            }

             if($request->otrocomercial != null){
                $otrocomercial = $request->otrocomercial;
                $filtro_otrocomercial = "esCteDeOtroComercial = '".$otrocomercial."'";
                $filtros[] = $filtro_otrocomercial;
            }
             if($request->pendienteforma != null){
                $pendienteforma = $request->pendienteforma;
                $filtro_pendiente = "pdteFormaPago = '".$pendienteforma."'";
                $filtros[] = $filtro_pendiente;
            }    


             if($request->contratante != null){
                $contratante = $request->contratante;
                $filtro_contratante = "codContratante = '".$contratante."'";
                $filtros[] = $filtro_contratante;
            } 

            if($request->empresa != null){
                $empresa = $request->empresa;
                $filtro_contratado = "codContratado = '".$empresa."'";
                $filtros[] = $filtro_contratado;
            } 

            if($request->matricula != null){
                $matricula = $request->matricula;
                $filtro_matricula = "codContrato in (select codContrato from europa_anexos where Matricula='".$matricula."')";
                $filtros[] = $filtro_matricula;
            } 

             if($request->comercial != null){
                $comercial = $request->comercial;
                $filtro_comercial = "codComercial = '".$comercial."'";
                $filtros[] = $filtro_comercial;
            } 

            if($request->tipo_anexo != null){
                $tipo_anexo = $request->tipo_anexo;
                $filtro_tipoanexo = "codContrato in (select codContrato from europa_anexos where CodTipoAnexo='".$tipo_anexo."')";
                $filtros[] = $filtro_tipoanexo;
            } 

            if($request->tipo_abono != null){
                $tipo_abono = $request->tipo_abono;
                $filtro_tipoabono = "codContrato in (select codContrato from europa_abonos where tipoAbono='".$tipo_abono."')";
                $filtros[] = $filtro_tipoabono;
            }

            for ($i =count($filtros) - 1; $i >= 0 ; $i--) { 
                $consulta .= $filtros[$i];
                $consulta .= " ";

                if($i > 0){
                    $consulta .= "AND ";
                }
            }

            $contratos = DB::table(DB::raw("europa_contratos a, europa_contactos b"))->select(DB::raw("a.CodContrato,a.FechaContrato,a.Vencimiento,a.importeCto,a.observaciones,a.codTipoContrato, b.razonSocial"))->whereRaw("a.codContratado=b.CodContacto")->orderby("FechaContrato","DESC")->limit(200)->get();



            
            if($consulta !=""){

              $contratos = DB::table(DB::raw("europa_contratos a, europa_contactos b"))->select(DB::raw("a.CodContrato,a.FechaContrato,a.Vencimiento,a.importeCto,a.observaciones,a.codTipoContrato, b.razonSocial"))->whereRaw("a.codContratado=b.CodContacto AND ".$consulta)->orderby("FechaContrato","DESC")->limit(200)->get();

             }
            
           if($contratos){
                foreach ($contratos as $key => $contrato) {

                     $output.='<tr>'.
                             '<td>'.$contrato->razonSocial.'</td>'.
                             '<td>'.$contrato->codTipoContrato.'</td>'.
                             '<td>'.date('d - m - Y', strtotime($contrato->FechaContrato)).'</td>'.
                             '<td>'.date('d - m - Y', strtotime($contrato->Vencimiento)).'</td>'.
                             '<td>'.$contrato->importeCto.'</td>'.
                             '<td>'.$contrato->observaciones.'</td>'.
                             '<td><a class="btn btn-sm btn-success" href="/contratos/europa/'.$contrato->CodContrato.'"><i class="fa fa-edit"></i> </a></td>'.
                             '</tr>';

                }

                
               
            }

            return Response($output);
             
    }
}


    public function filtra_anexo(Request $request){
            
        $output="<p style='text-align:center;'>No se han encontrado resultados.</p>";

            if($request->ajax()){

            $filtros = array();

            $consulta = "";

              if($request->alta_contrato_inicio != null && $request->alta_contrato_fin !=null){
                $alta_contrato_inicio = $request->alta_contrato_inicio;
                $alta_contrato_fin = $request->alta_contrato_fin;

                $FechaContrato = "b.FechaContrato BETWEEN '".$alta_contrato_inicio."' AND '".$alta_contrato_fin."'";

                $filtros[] = $FechaContrato;

                //$sanciones=gesanc_sanciones::whereRaw($consulta)->get();
              
            }

             if($request->alta_inicio != null && $request->alta_fin !=null){
                $alta_inicio = $request->alta_inicio;
                $alta_fin = $request->alta_fin;

                $FechaAlta = "a.Alta BETWEEN '".$baja_inicio."' AND '".$baja_fin."'";

                $filtros[] = $FechaAlta;

                //$sanciones=gesanc_sanciones::whereRaw($consulta)->get();
              
            }

             if($request->baja_inicio != null && $request->baja_fin !=null){
                $baja_inicio = $request->baja_inicio;
                $baja_fin = $request->baja_fin;

                $fechaBaja = "a.Baja BETWEEN '".$baja_inicio."' AND '".$baja_fin."'";

                $filtros[] = $FechaVencimiento;

                //$sanciones=gesanc_sanciones::whereRaw($consulta)->get();
              
            }

             if($request->vencimiento_inicio != null && $request->vencimiento_fin !=null){
                $vencimiento_inicio = $request->vencimiento_inicio;
                $vencimiento_fin = $request->vencimiento_fin;

                $FechaVencimiento = "a.vencimiento BETWEEN '".$vencimiento_inicio."' AND '".$vencimiento_fin."'";

                $filtros[] = $FechaVencimiento;

                //$sanciones=gesanc_sanciones::whereRaw($consulta)->get();
              
            }

            if($request->codContrato != null){
                $codContrato = $request->codContrato;
                $filtro_codContrato = "a.codContrato = '".$codContrato."'";
                $filtros[] = $filtro_codContrato;
            }

             if($request->codAnexo != null){
                $codAnexo = $request->codAnexo;
                $filtro_codAnexo = "a.CodAnexo = '".$codAnexo."'";
                $filtros[] = $filtro_codAnexo;
            }

             if($request->importe != null){
                $importe = $request->importe;
                $filtro_importe = "a.importeVenta = '".$importe."'";
                $filtros[] = $filtro_importe;
            } 

             if($request->comision != null){
                $comision = $request->comision;
                $filtro_comision = "b.porcentComisEspecifica = '".$comision."'";
                $filtros[] = $filtro_comision;
            } 

             if($request->descuento != null){
                $descuento = $request->descuento;
                $filtro_descuento = "b.descuento = '".$descuento."'";
                $filtros[] = $filtro_descuento;
            }

             if($request->otrocomercial != null){
                $otrocomercial = $request->otrocomercial;
                $filtro_otrocomercial = "b.esCteDeOtroComercial = '".$otrocomercial."'";
                $filtros[] = $filtro_otrocomercial;
            }

            if($request->esRenovacion != null){
                $esRenovacion = $request->esRenovacion;
                $filtro_esRenovacion = "a.esRenovacion = '".$esRenovacion."'";
                $filtros[] = $filtro_esRenovacion;
            }

            if($request->norenovar != null){
                $norenovar = $request->norenovar;
                $filtro_norenovar = "a.NOrenovar = '".$norenovar."'";
                $filtros[] = $filtro_esRenovacion;
            }

             if($request->sustituido != 0){
                $sustituido = $request->sustituido;
                $filtro_sustituido = "a.CodAnexo in (select CodSustituido from europa_anexos_sustituciones)";
                $filtros[] = $filtro_sustituido;
            }

              if($request->sustituyente != 0){
                $sustituyente = $request->sustituyente;
                $filtro_sustituyente = "a.CodAnexo in (select CodSustituyente from europa_anexos_sustituciones)";
                $filtros[] = $filtro_sustituyente;
            }

            if($request->otrocomercial != null){
                $otrocomercial = $request->otrocomercial;
                $filtro_otrocomercial = "b.esCteDeOtroComercial = '".$otrocomercial."'";
                $filtros[] = $filtro_otrocomercial;
            }
            

             if($request->contratante != null){
                $contratante = $request->contratante;
                $filtro_contratante = "b.codContratante = '".$contratante."'";
                $filtros[] = $filtro_contratante;
            } 

            if($request->empresa != null){
                $empresa = $request->empresa;
                $filtro_contratado = "b.codContratado = '".$empresa."'";
                $filtros[] = $filtro_contratado;
            } 

            if($request->matricula != null){
                $matricula = $request->matricula;
                $filtro_matricula = "a.Matricula ='".$matricula."'";
                $filtros[] = $filtro_matricula;
            } 

             if($request->remolque != null){
                $remolque = $request->remolque;
                $filtro_remolque = "a.CodAnexo in (select codAnexo from europa_anexos_remolques where matricula='".$remolque."')";
                $filtros[] = $filtro_remolque;
            } 

             if($request->comercial != null){
                $comercial = $request->comercial;
                $filtro_comercial = "codComercial = '".$comercial."'";
                $filtros[] = $filtro_comercial;
            } 

            if($request->tipo_anexo != null){
                $tipo_anexo = $request->tipo_anexo;
                $filtro_tipoanexo = "a.CodTipoAnexo in (select CodTipoAnexo from europa_tipo_anexo where tipoAnexo='".$tipo_anexo."')";
                $filtros[] = $filtro_tipoanexo;
            } 

            if($request->tipo_abono != null){
                $tipo_abono = $request->tipo_abono;
                $filtro_tipoabono = "a.codContrato in (select codContrato from europa_abonos where tipoAbono='".$tipo_abono."')";
                $filtros[] = $filtro_tipoabono;
            }

            for ($i =count($filtros) - 1; $i >= 0 ; $i--) { 
                $consulta .= $filtros[$i];
                $consulta .= " ";

                if($i > 0){
                    $consulta .= "AND ";
                }
            }

            $anexos = DB::table(DB::raw("europa_anexos a, europa_contratos b, europa_contactos c, europa_tipo_anexo d "))->select(DB::raw("a.CodAnexo,c.razonSocial as 'cliente' ,d.tipoAnexo as 'tipo',a.Matricula as 'matricula',b.fechaContrato as'fechadeContrato',a.Alta as 'fechaAlta',a.Baja as 'fechaBaja'"))->whereRaw("a.CodContrato=b.codContrato and b.codContratado=c.CodContacto and a.CodTipoAnexo=d.CodtipoAnexo")->orderby("fechaAlta","DESC")->limit(200)->get();


            
            if($consulta !=""){

              $anexos = DB::table(DB::raw("europa_anexos a, europa_contratos b, europa_contactos c, europa_tipo_anexo d "))->select(DB::raw("a.CodAnexo,c.razonSocial as 'cliente' ,d.tipoAnexo as 'tipo',a.Matricula as 'matricula',b.fechaContrato as'fechadeContrato',a.Alta as 'fechaAlta',a.Baja as 'fechaBaja'"))->whereRaw("a.CodContrato=b.codContrato and b.codContratado=c.CodContacto and a.CodTipoAnexo=d.CodtipoAnexo AND ".$consulta."")->orderby("fechaAlta","DESC")->limit(200)->get();

             }
            
           if($anexos){
                foreach ($anexos as $key => $anexo) {

                     $output.='<tr>'.
                             '<td>'.$anexo->cliente.'</td>'.
                             '<td>'.$anexo->tipo.'</td>'.
                             '<td>'.$anexo->matricula.'</td>'.
                             '<td>'.date('d - m - Y', strtotime($anexo->fechadeContrato)).'</td>'.
                             '<td>'.date('d - m - Y', strtotime($anexo->fechaAlta)).'</td>'.
                             '<td>'.date('d - m - Y', strtotime($anexo->fechaBaja)).'</td>'.
                             '<td><a class="btn btn-sm btn-success" href="/contratos/europa/anexos/'.$anexo->CodAnexo.'"><i class="fa fa-edit"></i> </a></td>'.
                             '</tr>';

                }

                
               
            }

            return Response($output);
             
    }
}

public function search_europa(Request $request){

       DB::enableQueryLog();
        //dd(DB::getQueryLog());
         

        if($request->ajax()){
            $output="<p style='text-align:center;'>No se han encontrado resultados.</p>";
            $contratos = DB::table(DB::raw("europa_contratos a, europa_contactos b"))->selectRaw("a.CodContrato,a.FechaContrato,a.Vencimiento,a.importeCto,a.observaciones,a.codTipoContrato, b.razonSocial")->where('codContrato','LIKE','%'.$request->search.'%')
                                ->orWhere('a.FechaContrato','LIKE','%'.$request->search.'%')
                                ->orWhere('a.Vencimiento','LIKE','%'.$request->search.'%')
                                ->orWhere('a.importeCto','LIKE','%'.$request->search.'%')
                                ->orWhere('a.observaciones','LIKE','%'.$request->search.'%')
                                ->Where('a.codContratado','=','b.CodContacto')
                                ->take(200)
                                ->orderBy('FechaContrato','DESC')
                                ->get();

            //dd(DB::getQueryLog());

            if($contratos){
                foreach ($contratos as $key => $contrato) {

                    $output.='<tr>'.
                             '<td>'.$contrato->razonSocial.'</td>'.
                             '<td>'.$contrato->codTipoContrato.'</td>'.
                             '<td>'.date('d - m - Y', strtotime($contrato->FechaContrato)).'</td>'.
                             '<td>'.date('d - m - Y', strtotime($contrato->Vencimiento)).'</td>'.
                             '<td>'.$contrato->importeCto.'</td>'.
                             '<td>'.$contrato->observaciones.'</td>'.
                             '<td><a class="btn btn-sm btn-success" href="contratos/europa/'.$contrato->CodContrato.'"><i class="fa fa-edit"></i> </a> <a class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a></td>'.
                             '</tr>';

                }
                return Response($output);
            }
        }
    }

    public function recibos(Request $request){
         if($request->ajax()){

            $id = $request->codContrato; 

            $recibos = DB::table(DB::raw("europa_recibos a,europa_tipo_forma_pago c"))->selectRaw("a.*,if(isnull(a.codEstado),null,(select estado from `europa_estados recibo` b where a.codEstado=b.codEstado)) as estado,c.FormaPago")->whereRaw("a.codFormaPago=c.CodFormaPago and a.CodContrato='".$id."'")->get();

            $formas_pago = DB::table("europa_tipo_forma_pago")->get();

            $estados_recibo = DB::table("europa_estados recibo")->get();

            $output = "<table class='table'>
                        <thead class='table-header'>
                            <th>Nº</th>                        
                            <th>Estado</th>
                            <th>FechaVencimiento</th>
                            <th>Importe</th>
                            <th>Forma de pago</th>
                            <th>fecha devol.</th>
                            <th>fecha sust.</th>
                            <th>Gastos devol.</th>
                            <th>Origen</th>
                            <th></th>
                        </thead>
                        <tbody id='myTable'>";

            if($recibos->count()){

                $cnt = 0;
                //$num = [];


                foreach ($recibos as $key => $recibo) {
                     $num = [];
                     $output.='<tr>';

                     if($recibo->origen==0){
                        $output.='<td>'.$recibo->numRecibo.'<input type="hidden" name="codRecibo" class="codRecibo" value="'.$recibo->codRecibo.'"></td>';
                     }else{
                        $codRecibo = $recibo->codRecibo;
                        do {
                        $numRecibo = DB::table("europa_recibos")->select("numRecibo")->where("codRecibo","=",$codRecibo)->first();
                        
                        $origen = DB::table("europa_recibos")->where("codRecibo","=",$codRecibo)->first();
                        $codRecibo =$origen->origen;
                        $num[] = $numRecibo->numRecibo;
                        } while($origen->origen!=0);

                        $numero_recibo= "";
                     $inverso = array_reverse($num);
                     $ultimo = end($inverso);
                     foreach ($inverso as $key => $val) {
                           $numero_recibo .=  $val.".";
                          
                        }
                    $numero_recibo = rtrim($numero_recibo, '.');
                     $output.='<td>'.$numero_recibo.'</td>';
                     }

                        $output.='<td><select class="selectpicker estados_recibo" data-live-search="true" title="Buscar..." id="estados_recibo" name="estados_recibo">';
                                           foreach ($estados_recibo as $key => $estado) {
                                    if($recibo->codEstado == $estado->codEstado){
                                       $output.='<option value="'.$estado->codEstado.'" selected>'.$estado->estado.'</option>'; 
                                    }else{
                                        $output.='<option value="'.$estado->codEstado.'">'.$estado->estado.'</option>'; 
                                    }
                                }
                            
                    $output .= "</select></td>";
                             if($recibo->vencimiento!=null){
                                $output.='<td><input type="text" class="form-control datepicker FechaVencimiento" name="FechaVencimiento" value="'.date('d/m/Y', strtotime($recibo->vencimiento)).'"></td>';
                             }else{ $output.='<td><input type="text" class="form-control datepicker FechaVencimiento" name="FechaVencimiento" value=""></td>';

                             }
                             
                    $output.='<td><input type="text" class="form-control importe_recibo" name="importe_recibo" id="importe_recibo" value="'.$recibo->importe.'"></td>';
                    $output.='<td><select class="selectpicker forma_pago_recibo" data-live-search="true" title="Buscar..." id="forma_pago_recibo" name="forma_pago_recibo">';
                                           foreach ($formas_pago as $key => $forma_pago) {
                                    if($recibo->codFormaPago == $forma_pago->CodFormaPago){
                                       $output.='<option value="'.$forma_pago->CodFormaPago.'" selected>'.$forma_pago->FormaPago.'</option>'; 
                                    }else{
                                        $output.='<option value="'.$forma_pago->CodFormaPago.'">'.$forma_pago->FormaPago.'</option>'; 
                                    }

                                     
                                }
                            
                    $output .= "</select></td>";


                              if($recibo->fechaDevolucion!=null){
                                $output.='<td><input type="text" class="form-control datepicker fechaDevolucion" name="fechaDevolucion" value="'.date('d/m/Y', strtotime($recibo->fechaDevolucion)).'"></td>';
                             }else{ $output.='<td><input type="text" class="form-control datepicker fechaDevolucion" name="fechaDevolucion" value=""></td>';
                             }
                              if($recibo->fechaSustitucion!=null){
                                $output.='<td><input type="text" class="form-control datepicker fechaSustitucion" name="fechaSustitucion" value="'.date('d/m/Y', strtotime($recibo->fechaSustitucion)).'"></td>';
                             }else{ $output.='<td><input type="text" class="form-control datepicker fechaSustitucion" name="fechaSustitucion" value=""></td>';
                             }
                    $output.='<td><input type="text" class="form-control gastosDevolucion" name="gastosDevolucion" id="gastosDevolucion" value="'.$recibo->gastosDevolucion.'"></td>'.
                             '<td>'.$recibo->origen.'</td>'.
                             '<td><a class="btn btn-sm btn-success update_recibo"><i class="fa fa-edit"></i> </a> <a class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a></td>'.
                             '</tr>';

                            

                }
            }

            $output .= "</tbody></table>";

             return Response($output);
        }
    }

    public function update_recibo(Request $request){

        if($request->ajax()){

            $codRecibo = null;
            $estados_recibo = null;
            $forma_pago_recibo = null;
            $importe  = null;
            $FechaVencimiento = null;
            $fechaDevolucion = null;
            $fechaSustitucion = null;
            $gastosDevolucion = null;

            if($request->FechaVencimiento !=null){
                $FechaVencimiento = DateTime::createFromFormat('d/m/Y', $request->FechaVencimiento);
                 date_time_set($FechaVencimiento, 00, 00);
            }

             if($request->fechaDevolucion !=null){
                $fechaDevolucion = DateTime::createFromFormat('d/m/Y', $request->fechaDevolucion);
                 date_time_set($fechaDevolucion, 00, 00);
            }

             if($request->fechaSustitucion !=null){
                $fechaSustitucion = DateTime::createFromFormat('d/m/Y', $request->fechaSustitucion);
                 date_time_set($fechaSustitucion, 00, 00);
            }

            if($request->codRecibo !=null){
                $codRecibo = $request->codRecibo;
            }
            if($request->estados_recibo !=null){
                $estados_recibo = $request->estados_recibo;
            }
            if($request->forma_pago_recibo !=null){
                $forma_pago_recibo = $request->forma_pago_recibo;
            }
             if($request->importe !=null){
                $importe = $request->importe;
            }
             if($request->gastosDevolucion !=null){
                $gastosDevolucion = $request->gastosDevolucion;
            }

        }

            try { 

                //Seleccionar las condiciones del contrato según los parametros establecidos


              DB::table('europa_recibos')->where("codRecibo","=",$codRecibo)->update(
                ['codFormaPago'=>$forma_pago_recibo, 'importe'=>$importe, 'codEstado'=>$estados_recibo, 'vencimiento'=>$FechaVencimiento,'fechaDevolucion'=>$fechaDevolucion, 'fechaSustitucion'=>$fechaSustitucion,'gastosDevolucion'=>$gastosDevolucion]);

            return  Response("El recibo se ha modificado correctamente");

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }


    }


    public function liquidaciones(Request $request)
    {

          if($request->ajax()){

            $id = $request->codContrato;

            $recibos = DB::table("europa_recibos")->where("codContrato","=",$id)->get();
            $formas_pago = DB::table("europa_tipo_forma_pago")->get(); 

            
             $output = "<table class='table' id='table_motivos'>
                        <thead class='table-header'>
                            <th>Recibo</th>                        
                            <th>Fecha Liquidación</th>
                            <th>Forma</th>
                            <th>Importe</th>
                            <th></th>
                        </thead>
                        <tbody id='myTable'>";
            $liquidaciones = DB::table("europa_liquidacion_comisiones")->whereRaw("codRecibo in(select codRecibo from europa_recibos where CodContrato='".$id."')")->get();

            if($liquidaciones->count()){

                foreach ($liquidaciones as $key => $liquidacion) {

                     $output.='<tr>';
                      $output.= '<td><select class="selectpicker recibo" data-container="body" data-live-search="true" title="Buscar..."  name="estado">';
                        foreach ($recibos as $key => $recibo) {
                            if($recibo->codRecibo == $liquidacion->codRecibo){
                                $output.= '<option value="'.$recibo->codRecibo.'" selected>'.$recibo->codRecibo.'</option>';
                            }else{
                                $output.= '<option value="'.$recibo->codRecibo.'">'.$recibo->codRecibo.'</option>';
                            }
                        }
                        $output.= '</select></td>'.
                              '<td><input type="text" class="form-control datepicker fecha_de_liquidacion" value="'.date('d/m/Y', strtotime($liquidacion->fechaLiquidacion)).'"></td>';
                              $output.= '<td><select class="selectpicker forma_pago" data-container="body" data-live-search="true" title="Buscar...">';
                        foreach ($formas_pago as $key => $forma) {
                            if($forma->CodFormaPago == $liquidacion->codFormaLquidacion){
                                $output.= '<option value="'.$forma->CodFormaPago.'" selected>'.$forma->FormaPago.'</option>';
                            }else{
                                $output.= '<option value="'.$forma->CodFormaPago.'">'.$forma->FormaPago.'</option>';
                            }
                        }
                        $output.= '</select></td>'.
                              '<td><input type="number" class="form-control importe" value="'.$liquidacion->importe.'"></td>'.
                             '<td><a class="btn btn-sm btn-success editar_liquidacion" href="#"><i class="fa fa-edit"></i></a><a class="btn btn-sm btn-danger eliminar_liquidacion"><i class="fa fa-trash"></i></a></td>'.
                             '</tr>';

                }
                }

                $output.='<tr>';
                      $output.= '<td><select class="selectpicker recibo" data-container="body" data-live-search="true" title="Buscar..."  name="estado">';
                        foreach ($recibos as $key => $recibo) {
                                $output.= '<option value="'.$recibo->codRecibo.'">'.$recibo->codRecibo.'</option>';
                        }
                        $output.= '</select></td>'.
                              '<td><input type="text" class="form-control datepicker fecha" value=""></td>';
                              $output.= '<td><select class="selectpicker forma_pago" data-container="body" data-live-search="true" title="Buscar..."  name="estado">';
                        foreach ($formas_pago as $key => $forma) {
                                $output.= '<option value="'.$forma->CodFormaPago.'">'.$forma->FormaPago.'</option>';
                        }
                        $output.= '</select></td>'.
                              '<td><input type="number" class="form-control importe" value=""></td>'.
                             '<td><a class="btn btn-success add_liquidacion" id="add_liquidacion"><i class="fa fa-plus"></i></a></td>'.
                             '</tr>';
            
            $output .= "</tbody></table><input type='hidden' id='tabla_tipo' value='motivo'>";
            return Response($output);
        }
    }

    public function editar_liquidacion(Request $request){
          if($request->ajax()){ 

        $recibo = null;
        $fecha = null;
        $forma_pago = null;
        $importe = null;


         if($request->fecha !=null){
                $fecha = DateTime::createFromFormat('d/m/Y', $request->fecha);
                 date_time_set($fecha, 00, 00);
            }

        if($request->recibo !=null){
                $recibo = $request->recibo;
            }
            if($request->forma_pago !=null){
                $forma_pago = $request->forma_pago;
            }
         if($request->importe !=null){
                $importe = $request->importe;
            }


          }

               try { 

                //Seleccionar las condiciones del contrato según los parametros establecidos



              DB::table('europa_liquidacion_comisiones')->where("codRecibo","=",$recibo)->update(
                ['codRecibo'=>$recibo, 'codFormaLquidacion'=>$forma_pago, 'fechaLiquidacion'=>$fecha, 'importe'=>$importe]);

            return  Response("La liquidacion se ha modificado correctamente");

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }
    
    }

    public function add_liquidacion(Request $request){
          if($request->ajax()){ 

        $recibo = null;
        $fecha = null;
        $forma_pago = null;
        $importe = null;


         if($request->fecha !=null){
                $fecha = DateTime::createFromFormat('d/m/Y', $request->fecha);
                 date_time_set($fecha, 00, 00);
            }

        if($request->recibo !=null){
                $recibo = $request->recibo;
            }
            if($request->forma_pago !=null){
                $forma_pago = $request->forma_pago;
            }
         if($request->importe !=null){
                $importe = $request->importe;
            }


          }

               try { 

                //Seleccionar las condiciones del contrato según los parametros establecidos


              DB::table('europa_liquidacion_comisiones')->insert(
                ['codRecibo'=>$recibo, 'codFormaLquidacion'=>$forma_pago, 'fechaLiquidacion'=>$fecha, 'importe'=>$importe]);


            return  Response("La liquidacion se ha insertado correctamente");

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }
    
    }

     public function eliminar_liquidacion(Request $request){
          if($request->ajax()){ 

             $recibo = null;
            if($request->recibo !=null){
                $recibo = $request->recibo;
            }

               try { 

                //Seleccionar las condiciones del contrato según los parametros establecidos


              DB::table('europa_liquidacion_comisiones')->where("codRecibo","=",$recibo)->delete();
                


            return  Response("La liquidacion se ha eliminado correctamente");

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }
    
    }
}

     public function abonos(Request $request)
    {

          if($request->ajax()){

            $id = $request->codContrato; 

            
             $output = "<table class='table' id='table_motivos'>
                        <thead class='table-header'>
                            <th>Abono</th>                        
                            <th>Expediente</th>
                            <th>Importe</th>
                            <th>Notas</th>
                            <th></th>
                        </thead>
                        <tbody id='myTable'>";
            $abonos = DB::table("europa_abonos")->selectRaw('*,(case
 when tipoAbono=0 then "Abono Multa, MODIFICA factura (NO reduce contrato/comi.)"
 when tipoAbono=-1 then "Abono Multa, REDUCE contrato y comisión"
 when tipoAbono=-2 then "Abono Normal (reduce cto/comis)"
 when tipoAbono=-3 then "Abono Multa, POST FACTURA (NO reduce contrato/comi.)"
 else "ninguno" end) as tipo')->whereRaw("codContrato='".$id."'")->get();

            if($abonos->count()){

                foreach ($abonos as $key => $abono) {

                     $output.='<tr>';
                     $output.= '<td><input type="hidden" class="idabono" value="'.$abono->idabono.'"><select class="selectpicker tipo_abono" data-container="body" data-live-search="true" title="Buscar...">';
                            if($abono->tipoAbono == 0){
                                $output.= '<option value="0" selected>Abono Multa, MODIFICA factura (NO reduce contrato/comi.)</option>';
                                $output.= '<option value="-1">Abono Multa, REDUCE contrato y comisión</option>';
                                $output.= '<option value="-2">Abono Normal (reduce cto/comis)</option>';
                                $output.= '<option value="-3">Abono Multa, POST FACTURA (NO reduce contrato/comi.)</option>';
                            }
                            elseif($abono->tipoAbono == -1){
                                $output.= '<option value="0" >Abono Multa, MODIFICA factura (NO reduce contrato/comi.)</option>';
                                $output.= '<option value="-1" selected>Abono Multa, REDUCE contrato y comisión</option>';
                                $output.= '<option value="-2">Abono Normal (reduce cto/comis)</option>';
                                $output.= '<option value="-3">Abono Multa, POST FACTURA (NO reduce contrato/comi.)</option>';
                            }
                            elseif($abono->tipoAbono == -2){
                                $output.= '<option value="0" >Abono Multa, MODIFICA factura (NO reduce contrato/comi.)</option>';
                                $output.= '<option value="-1">Abono Multa, REDUCE contrato y comisión</option>';
                                $output.= '<option value="-2" selected>Abono Normal (reduce cto/comis)</option>';
                                $output.= '<option value="-3">Abono Multa, POST FACTURA (NO reduce contrato/comi.)</option>';
                            }
                            elseif($abono->tipoAbono == -3){
                                $output.= '<option value="0" >Abono Multa, MODIFICA factura (NO reduce contrato/comi.)</option>';
                                $output.= '<option value="-1">Abono Multa, REDUCE contrato y comisión</option>';
                                $output.= '<option value="-2">Abono Normal (reduce cto/comis)</option>';
                                $output.= '<option value="-3" selected>Abono Multa, POST FACTURA (NO reduce contrato/comi.)</option>';
                            }
                        
                        $output.= '</select></td>';

                    $output.= '<td><input type="text" class="form-control expediente" value="'.$abono->expediente.'"></td>'.
                             '<td><input type="number" class="form-control importe" value="'.$abono->importe.'"></td>'.
                             '<td><input type="text" class="form-control notas" value="'.$abono->notas.'"></td>'.
                             '<td><a class="btn btn-sm btn-success btn-edit editar_abono" href="#"><i class="fa fa-edit"></i></a><a class="btn btn-sm btn-danger eliminar_liquidacion"><i class="fa fa-trash"></i></a></td>'.
                             '</tr>';

                }
 
            }

             $output.='<tr>';
                     $output.= '<td><select class="selectpicker tipo_abono" data-container="body" data-live-search="true" title="Buscar...">';
                           
                                $output.= '<option value="0" >Abono Multa, MODIFICA factura (NO reduce contrato/comi.)</option>';
                                $output.= '<option value="-1">Abono Multa, REDUCE contrato y comisión</option>';
                                $output.= '<option value="-2">Abono Normal (reduce cto/comis)</option>';
                                $output.= '<option value="-3">Abono Multa, POST FACTURA (NO reduce contrato/comi.)</option>';
                            
                        
                        $output.= '</select></td>';

                    $output.= '<td><input type="text" class="form-control expediente" value=""></td>'.
                             '<td><input type="number" class="form-control importe" value=""></td>'.
                             '<td><input type="text" class="form-control notas" value=""></td>'.
                             '<td><a class="btn btn-success add_motivo" id="add_abono"><i class="fa fa-plus"></i></a></td>'.

                             '</tr>';
            
            $output .= "</tbody></table><input type='hidden' id='tabla_tipo' value='motivo'>";
            return Response($output);
        }
    }

     public function editar_abono(Request $request){
          if($request->ajax()){ 

        $abono = null;
        $tipo = null;
        $expediente = null;
        $importe = null;
        $notas = null;



        if($request->abono !=null){
                $abono = $request->abono;
            }
        if($request->tipo !=null){
                $tipo = $request->tipo;
            }
        if($request->expediente !=null){
                $expediente = $request->expediente;
            }
        if($request->importe !=null){
                $importe = $request->importe;
            }
        if($request->notas !=null){
                $notas = $request->notas;
            }


          }

               try { 

                //Seleccionar las condiciones del contrato según los parametros establecidos


DB::enableQueryLog();

              DB::table('europa_abonos')->where("idabono","=",$abono)->update(
                ['expediente'=>$expediente, 'importe'=>$importe, 'tipoAbono'=>$tipo, 'notas'=>$notas]);
dd(DB::getQueryLog());
            return  Response("El abono se ha modificado correctamente");

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }
    
    }

    public function add_abono(Request $request){
          if($request->ajax()){ 

        $idcontrato = null;
        $abono = null;
        $tipo = null;
        $expediente = null;
        $importe = null;
        $notas = null;



        if($request->abono !=null){
                $abono = $request->abono;
            }
        if($request->tipo !=null){
                $tipo = $request->tipo;
            }
        if($request->expediente !=null){
                $expediente = $request->expediente;
            }
        if($request->importe !=null){
                $importe = $request->importe;
            }
        if($request->notas !=null){
                $notas = $request->notas;
            }
        if($request->idcontrato !=null){
                $idcontrato = $request->idcontrato;
            }


          }

               try { 

                //Seleccionar las condiciones del contrato según los parametros establecidos



              DB::table('europa_abonos')->insertGetId(
                ['codContrato'=>$idcontrato,'expediente'=>$expediente, 'importe'=>$importe, 'tipoAbono'=>$tipo, 'notas'=>$notas]);

            return  Response("El abono se ha insertado correctamente");

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }
    
    }

    public function anexos(Request $request){
         if($request->ajax()){

            $id = $request->codContrato; 

            $anexos = DB::table(DB::raw("europa_anexos a, europa_tipo_anexo b"))->select(DB::raw("a.*,b.tipoAnexo"))->whereRaw(" a.CodTipoAnexo=b.CodtipoAnexo AND a.CodContrato='".$id."'")->get();

            $output = "<table class='table'>
                        <thead class='table-header'>                       
                            <th>Matricula</th>
                            <th>Tipo</th>
                             <th>Alta</th>
                             <th>Baja</th>
                             <th>Importe</th>
                            <th></th>
                        </thead>
                        <tbody id='myTable'>";

            if($anexos->count()){

                foreach ($anexos as $key => $anexo) {

                     $output.='<tr>'.
                             '<td>'.$anexo->Matricula.'</td>'.
                             '<td>'.$anexo->tipoAnexo.'</td>'.
                             '<td>'.date('d/m/Y', strtotime($anexo->Alta)).'</td>'.
                             '<td>'.date('d/m/Y', strtotime($anexo->Baja)).'</td>'.
                             '<td>'.$anexo->importeVenta.' €</td>'.
                             '<td><a class="btn btn-sm btn-success" href="/contratos/europa/anexos/'.$anexo->CodAnexo.'"><i class="fa fa-edit"></i></a></td>'.
                             '</tr>';

                }
            }

            $output .= "</tbody></table>";

             return Response($output);
        }
    }

    public function eliminar_abono(Request $request){
          if($request->ajax()){ 

             $abono = null;
            if($request->abono !=null){
                $abono = $request->abono;
            }

               try { 

                //Seleccionar las condiciones del contrato según los parametros establecidos


              DB::table('europa_abonos')->where("idabono","=",$abono)->delete();
                


            return  Response("el abono se ha eliminado correctamente");

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }
    
    }
}

    public function mostrar_sustitucion(Request $request){

        if($request->ajax()){

            $codContrato = $request->codContrato;
            
            $output = "<label>Matricula</label>";

            $anexos = DB::table(DB::raw("europa_anexos a, europa_tipo_anexo b"))->select(DB::raw("a.*,b.tipoAnexo"))->whereRaw(" a.CodTipoAnexo=b.CodtipoAnexo AND a.CodContrato='".$codContrato."'")->get();

            $output.='<select class="selectpicker" data-live-search="true" title="Buscar..." id="lista_anexo" name="lista_anexo">';

             if($anexos->count()){

                foreach ($anexos as $key => $anexo) {

                     $output.='<option value="'.$anexo->CodAnexo.'">'.$anexo->Matricula.'</option>';
                }
            }
              $output .= "</select>";

              $output.='<br><label>Fecha sustitución</label><input type="text" class="form-control datepicker" name="fecha_sustitucion" id="fecha_sustitucion" value="">';

              $output.='<label>Notas</label><textarea class="form-control" name="notas_sustitucion" id="notas_sustitucion"></textarea>';

            return Response($output);

    }

}

public function mostrar_sustitucion_remolques(Request $request){

        if($request->ajax()){

            $codAnexo = $request->codAnexo;

            $remolques = DB::table(DB::raw("europa_anexos_remolques a"))->select(DB::raw("a.*, CASE when codMotivoBaja is null then 'ninguno' else (select motivobaja from `europa_motivos baja` where CodMotivoBaja=a.codMotivoBaja) end as 'Motivo'"))->whereRaw("a.CodAnexo='".$codAnexo."'")->get();
            
            $output = "<label>sustituyente</label>";

            $output.='<select class="selectpicker" data-live-search="true" title="Buscar..." id="remolque_sustituyente" name="remolque_sustituyente">';

             if($remolques->count()){

                foreach ($remolques as $key => $remolque) {

                     $output.='<option value="'.$remolque->codAnexoRemolque.'">'.$remolque->matricula.'</option>';
                }
            }
              $output .= "</select>";

               $output .= "<label>Sustituido</label>";

              $output.='<select class="selectpicker" data-live-search="true" title="Buscar..." id="remolque_sustituido" name="remolque_sustituido">';

             if($remolques->count()){

                foreach ($remolques as $key => $remolque) {

                     $output.='<option value="'.$remolque->codAnexoRemolque.'">'.$remolque->matricula.'</option>';
                }
            }
              $output .= "</select>";

              $output.='<br><label>Fecha sustitución</label><input type="text" class="form-control datepicker" name="fecha_sustitucion_remolque" id="fecha_sustitucion_remolque" value="">';

              $output.='<label>Notas</label><textarea class="form-control" name="notas_sustitucion_remolque" id="notas_sustitucion_remolque"></textarea>';

            return Response($output);

    }

}

public function sustitucion(Request $request){

    if($request->ajax()){

            $AnexoSustituido = $request->AnexoSustituido;
            $AnexoSustituyente = $request->AnexoSustituyente;

            $fechaSustitucion = null;

             if($request->fechaSustitucion !=null){
                $fechaSustitucion = DateTime::createFromFormat('d/m/Y', $request->fechaSustitucion);
                date_time_set($fechaSustitucion, 00, 00);
            }
            
               try { 

               DB::table('europa_anexos_sustituciones')->insertGetId(
                ['CodSustituido' => $AnexoSustituido,'CodSustituyente' => $AnexoSustituyente,'fechaSutitucion'=>$fechaSustitucion,'notas'=>$request->notas]);

               DB::table('europa_anexos')->where('CodAnexo','=',$AnexoSustituido)->update(
                ['Baja'=>$fechaSustitucion]);

               $contrato = DB::table("europa_contratos")->whereRaw("codContrato = 'select codContrato from europa_anexos where codAnexo ='".$AnexoSustituyente."' ")->first();

              $anexos_contrato = DB::table("europa_anexos")->selectRaw("sum(importeVenta) as 'totalventa'")->whereRaw("CodAnexo not in (select CodSustituido from europa_anexos_sustituciones) and codContrato='".$contrato->codContrato."'")->first();

              $suma_anexos = $anexos_contrato->totalventa;

              $descuento_comercial = 0;
              if($contrato->descuento !=null){
                $descuento_comercial = $contrato->descuento;
            }
              
              $abonos_contrato = DB::table("europa_abonos")->whereRaw("codContrato ='".$contrato->codContrato."' and (tipoAbono =-2 OR tipoAbono=-1)")->get();

            $dcto_abonos = 0;

              if($abonos_contrato !=null){
                foreach ($abonos_contrato as $key => $abono) {
                    $dcto_abonos = $dcto_abonos + $abono->importe;
                }
              }else{
                $dcto_abonos = 0;
              }

              $importe_calculado = $suma_anexos - $descuento_comercial - $dcto_abonos;



            DB::table('europa_contratos')->where("codContrato","=",$contrato->codContrato)->update(
                ['importeCto'=>$importe_calculado]);



            return  Response("La sustitución se ha llevado a cabo correctamente.");

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }

    }


    }

    public function sustitucion_remolques(Request $request){

    if($request->ajax()){

            $AnexoSustituido = $request->AnexoSustituido;
            $AnexoSustituyente = $request->AnexoSustituyente;

            $fechaSustitucion = null;

             if($request->fechaSustitucion !=null){
                $fechaSustitucion = DateTime::createFromFormat('d/m/Y', $request->fechaSustitucion);
                date_time_set($fechaSustitucion, 00, 00);
            }
            
               try { 

               DB::table('europa_anexos_remolques_sustituciones')->insertGetId(
                ['CodSustituido' => $AnexoSustituido,'CodSustituyente' => $AnexoSustituyente,'fechaSutitucion'=>$fechaSustitucion,'notas'=>$request->notas]);

               DB::table('europa_anexos_remolques')->where('codAnexoRemolque','=',$AnexoSustituido)->update(
                ['Baja'=>$fechaSustitucion]);

            return  Response("La sustitución se ha llevado a cabo correctamente.");

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }
    }

    }

   public function mostrar_generar_recibos(Request $request){

        if($request->ajax()){

            $codContrato = $request->codContrato;

            $importeCto = $request->importeCto;

            $formas_pago = DB::table("europa_tipo_forma_pago")->get();
            
            $output = "<label>Precio</label>";
            $output.='<ul class="list-inline">
                           <li><input type="radio" name="precio" value="automatico" placeholder="Indefinido" checked> <span>Automático</span></li>
                          <li><input type="radio" name="precio" value="manual" placeholder="Facturar"> <span>Manual</span></li>
                           </ul>';


              $output.='<br><label>Número de recibos</label><input type="number" class="form-control" name="num_recibo" id="num_recibo" value="1">';

              $output.='<br><label>Importe</label><input type="number" class="form-control" name="importe_recibo" id="importe_recibo" value="'.$importeCto.'">';

               $output .= "<br><label>Forma de pago</label>";

              $output.='<br><select class="selectpicker" data-live-search="true" title="Buscar..." id="forma_pago_recibo" name="forma_pago_recibo">';

             if($formas_pago->count()){

                foreach ($formas_pago as $key => $forma_pago) {

                     $output.='<option value="'.$forma_pago->CodFormaPago.'">'.$forma_pago->FormaPago.'</option>';
                }


            }
              $output .= "</select><br>";

              $output.='<br><label>Vencimiento</label><input type="text" class="form-control datepicker" name="venc_recibo" id="venc_recibo" value="">';

              $output.='<br><label>Notas</label><textarea class="form-control" name="notas_recibo" id="notas_recibo"></textarea>';

            return Response($output);

    }

}

public function mostrar_desinmovilizacion(Request $request){

            $codContrato = $request->codContrato;

            $output='<br><label>Codigo Contrato: '.$codContrato.'</label>';

            $output.='<br><label>Fecha Informe</label><input type="text" class="form-control datepicker" name="fecha_informe" id="fecha_informe" value="'.date('d/m/Y').'">';

            $output.='<div id="fechas" style="display:none"><br><label>Fecha Baja/Cambio</label><input type="text" class="form-control datepicker" name="fecha_baja_cambio" id="fecha_baja_cambio" value="'.date('d/m/Y').'"></div>';

            $output .= "<br><label>Concepto</label>";

              $output.='<br><select class="selectpicker" data-live-search="true" title="Buscar..." id="concepto_informe" name="concepto_informe">';
                 $output.='<option value="1">ALTA DESINMOVILIZACION Y PEDIR TARJETAS</option>';
                 $output.='<option value="2">RENOVACION DE INTERNACIONAL</option>';
                 $output.='<option value="3">CAMBIO DE MATRICULA</option>';
                 $output.='<option value="4">BAJA DE MATRICULA</option>';
              $output .= "</select><br>";

              $output .= "<div id='coberturas'>'<br><label>Cobertura</label>";

              $output.='<br><select class="selectpicker" data-live-search="true" title="Buscar..." id="cobertura_informe" name="cobertura_informe">';
                 $output.='<option value="1">SOLO FRANCIA</option>';
                 $output.='<option value="2">TODA LA UE</option>';
                 $output.='<option value="3">CAUCION EN ESPAÑA</option>';
              $output .= "</select></div><br>";

             $output.='<br><label>Comentario</label><textarea class="form-control" name="comentario_informe" id="comentario_informe"></textarea>';

             $output.='<br><label>Advertencia</label><textarea class="form-control" name="advertencia_informe" id="advertencia_informe"></textarea>';

            return Response($output);

    }

    public function generar_desinmovilizacion(){
            
        $cliente = DB::table("europa_contactos")->whereRaw("codContacto=(select codContratado from europa_contratos where codContrato='".$_GET['codContrato']."')")->first();
        $provincia = DB::table("europa_provincias")->whereRaw("codProvincia = '".$cliente->codProvincia."'")->first();
        $contrato = DB::table("europa_contratos")->whereRaw("codContrato='".$_GET['codContrato']."'")->first();
        $contratante = DB::table("europa_contactos")->whereRaw("codContacto='".$contrato->codContratante."'")->first();
        $telefono_contratante =  DB::table("europa_contactos_telefonos")->whereRaw("codContacto='".$contrato->codContratante."' and codTipoTelefono='1'")->first();
        $fax_contratante =  DB::table("europa_contactos_telefonos")->whereRaw("codContacto='".$contrato->codContratante."' and codTipoTelefono='3'")->first();
        $anexos =  DB::table("europa_anexos")->whereRaw("codContrato='".$_GET['codContrato']."'")->get();
        $filename = '..\public\temp\informe_desinmovilizacion.docx';
        $lista_matriculas = "";
        $templateWord = new TemplateProcessor('..\public\templates\europa_contratos\Desinmovilizacion.docx');
                    
                
                $templateWord->setValue('contratante',$contratante->razonSocial);
                $templateWord->setValue('usuario',Auth::user()->name);
                $templateWord->setValue('fax',$fax_contratante->CodTelefono);
                $templateWord->setValue('fecha_hoy',$_GET['fecha_informe']);
                $templateWord->setValue('telefono',$telefono_contratante->CodTelefono);
                $templateWord->setValue('contrato',$_GET['codContrato']);
                $templateWord->setValue('referencia','PETICIÓN DESINMOVILIZACIÓN');

               if($_GET['concepto']==1){
                  $templateWord->setValue('mensaje','ALTA DESINMOVILIZACIÓN Y PEDIR TARJETAS');  
                }else{
                  $templateWord->setValue('mensaje','RENOVACION DE INTERNACIONAL');  
                }
                $templateWord->setValue('empresa',$cliente->razonSocial);
                $templateWord->setValue('cif',$cliente->cif);
                $templateWord->setValue('direccion',$cliente->direccion);
                $templateWord->setValue('cp',$cliente->codPostal);
                $templateWord->setValue('poblacion',$cliente->Poblacion);
                $templateWord->setValue('provincia',$provincia->Provincia);
                $templateWord->setValue('fecha_contrato',date('d - m - Y', strtotime($contrato->fechaContrato)));
                foreach ($anexos as $key => $anexo) {
                      $lista_matriculas.= $anexo->Matricula."<w:br/>";
                }
                $templateWord->setValue('lista_matriculas',$lista_matriculas);

               if($_GET['cobertura']==1){
                    $templateWord->setValue('a',"X");
                    $templateWord->setValue('b',"");
                    $templateWord->setValue('c',"");
                }
                elseif($_GET['cobertura']==2){
                    $templateWord->setValue('a',"");
                    $templateWord->setValue('b',"X");
                    $templateWord->setValue('c',"");
                }
                else{
                    $templateWord->setValue('a',"");
                    $templateWord->setValue('b',"");
                    $templateWord->setValue('c',"X");
                }
                $templateWord->setValue('comentario',$_GET['comentario']);
                $templateWord->setValue('advertencia',$_GET['advertencia']);

                $templateWord->saveAs(storage_path($filename));

    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename=informe_desinmovilizacion.docx; charset=iso-8859-1');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize('..\public\temp\informe_desinmovilizacion.docx'));
    readfile('..\public\temp\informe_desinmovilizacion.docx');
  

    $files = glob('..\public\temp\*'); // get all file names
    foreach($files as $file){ // iterate files
      if(is_file($file))
        unlink($file); // delete file
        }
  exit;

    }

    public function informe_cambio_matriculas(){
            
        $cliente = DB::table("europa_contactos")->whereRaw("codContacto=(select codContratado from europa_contratos where codContrato='".$_GET['codContrato']."')")->first();
        $provincia = DB::table("europa_provincias")->whereRaw("codProvincia = '".$cliente->codProvincia."'")->first();
        $contrato = DB::table("europa_contratos")->whereRaw("codContrato='".$_GET['codContrato']."'")->first();
        $contratante = DB::table("europa_contactos")->whereRaw("codContacto='".$contrato->codContratante."'")->first();
        $telefono_contratante =  DB::table("europa_contactos_telefonos")->whereRaw("codContacto='".$contrato->codContratante."' and codTipoTelefono='1'")->first();
        $fax_contratante =  DB::table("europa_contactos_telefonos")->whereRaw("codContacto='".$contrato->codContratante."' and codTipoTelefono='3'")->first();
        $anexos =  DB::table("europa_anexos")->whereRaw("codContrato='".$_GET['codContrato']."'")->get();
        $filename = '..\public\temp\informe_cambio_matriculas.docx';
        $lista_matriculas_baja = "";
        $lista_matriculas_alta = "";
        $fecha_cambio = "";
        if($_GET["fecha_baja_cambio"] !=null){
                $fecha_cambio = DateTime::createFromFormat('d/m/Y', $_GET["fecha_baja_cambio"]);
                date_time_set($fecha_cambio, 00, 00);
            }
        $sustituciones = DB::table("europa_anexos_sustituciones")->whereRaw("date(fechaSutitucion)='".$fecha_cambio->format('Y-m-d')."' and CodSustituyente in (select CodAnexo from europa_anexos where CodContrato='".$_GET['codContrato']."')")->get();

        $templateWord = new TemplateProcessor('..\public\templates\europa_contratos\CambioMatricula.docx');
                    
                
                $templateWord->setValue('contratante',$contratante->razonSocial);
                $templateWord->setValue('usuario',Auth::user()->name);
                $templateWord->setValue('fax',$fax_contratante->CodTelefono);
                $templateWord->setValue('fecha_hoy',$_GET['fecha_informe']);
                $templateWord->setValue('telefono',$telefono_contratante->CodTelefono);
                $templateWord->setValue('contrato',$_GET['codContrato']);
                $templateWord->setValue('referencia','CAMBIO MATRICULA');

          
                $templateWord->setValue('mensaje','CAMBIO MATRICULA');  
                
                $templateWord->setValue('empresa',$cliente->razonSocial);
                $templateWord->setValue('cif',$cliente->cif);
                $templateWord->setValue('direccion',$cliente->direccion);
                $templateWord->setValue('cp',$cliente->codPostal);
                $templateWord->setValue('poblacion',$cliente->Poblacion);
                $templateWord->setValue('provincia',$provincia->Provincia);
                $templateWord->setValue('fecha_contrato',date('d - m - Y', strtotime($contrato->fechaContrato)));
                $templateWord->setValue('fecha_cambio',$_GET["fecha_baja_cambio"]);

                foreach ($sustituciones as $key => $sustitucion) {

                    $sustituyente =  DB::table("europa_anexos")->whereRaw("CodAnexo='".$sustitucion->CodSustituyente."'")->first();
                    $sustituido =  DB::table("europa_anexos")->whereRaw("CodAnexo='".$sustitucion->CodSustituido."'")->first();
                      $lista_matriculas_baja.= $sustituido->Matricula."<w:br/>";
                      $lista_matriculas_alta.= $sustituyente->Matricula."<w:br/>";
                }
                $templateWord->setValue('lista_matriculas_alta',$lista_matriculas_baja);
                $templateWord->setValue('lista_matriculas_baja',$lista_matriculas_alta);

                $templateWord->setValue('comentario',$_GET['comentario']);
                $templateWord->setValue('advertencia',$_GET['advertencia']);

                $templateWord->saveAs(storage_path($filename));

    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename=informe_cambio_matriculas.docx; charset=iso-8859-1');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize('..\public\temp\informe_cambio_matriculas.docx'));
    readfile('..\public\temp\informe_cambio_matriculas.docx');
  

    $files = glob('..\public\temp\*'); // get all file names
    foreach($files as $file){ // iterate files
      if(is_file($file))
        unlink($file); // delete file
        }
  exit;

    }

    public function informe_baja_matriculas(){
            
        $cliente = DB::table("europa_contactos")->whereRaw("codContacto=(select codContratado from europa_contratos where codContrato='".$_GET['codContrato']."')")->first();
        $provincia = DB::table("europa_provincias")->whereRaw("codProvincia = '".$cliente->codProvincia."'")->first();
        $contrato = DB::table("europa_contratos")->whereRaw("codContrato='".$_GET['codContrato']."'")->first();
        $contratante = DB::table("europa_contactos")->whereRaw("codContacto='".$contrato->codContratante."'")->first();
        $telefono_contratante =  DB::table("europa_contactos_telefonos")->whereRaw("codContacto='".$contrato->codContratante."' and codTipoTelefono='1'")->first();
        $fax_contratante =  DB::table("europa_contactos_telefonos")->whereRaw("codContacto='".$contrato->codContratante."' and codTipoTelefono='3'")->first();
        $fecha_baja = "";
             if($_GET["fecha_baja_cambio"] !=null){
                $fecha_baja = DateTime::createFromFormat('d/m/Y', $_GET["fecha_baja_cambio"]);
                date_time_set($fecha_baja, 00, 00);
            }
        $anexos =  DB::table("europa_anexos")->whereRaw("codContrato='".$_GET['codContrato']."' and date(Baja)='".$fecha_baja->format('Y-m-d')."'")->get();
        $filename = '..\public\temp\informe_baja_matriculas.docx';
        $lista_matriculas_baja = "";
   

        $templateWord = new TemplateProcessor('..\public\templates\europa_contratos\BajaMatricula.docx');
                    
                
                $templateWord->setValue('contratante',$contratante->razonSocial);
                $templateWord->setValue('usuario',Auth::user()->name);
                $templateWord->setValue('fax',$fax_contratante->CodTelefono);
                $templateWord->setValue('fecha_hoy',$_GET['fecha_informe']);
                $templateWord->setValue('telefono',$telefono_contratante->CodTelefono);
                $templateWord->setValue('contrato',$_GET['codContrato']);
                $templateWord->setValue('referencia','BAJA MATRICULA');

                $templateWord->setValue('mensaje','BAJA MARTRICULA');  
                
                $templateWord->setValue('empresa',$cliente->razonSocial);
                $templateWord->setValue('cif',$cliente->cif);
                $templateWord->setValue('direccion',$cliente->direccion);
                $templateWord->setValue('cp',$cliente->codPostal);
                $templateWord->setValue('poblacion',$cliente->Poblacion);
                $templateWord->setValue('provincia',$provincia->Provincia);
                $templateWord->setValue('fecha_contrato',date('d - m - Y', strtotime($contrato->fechaContrato)));
                $templateWord->setValue('fecha_baja',$_GET["fecha_baja_cambio"]);

                foreach ($anexos as $key => $anexo) {

                      $lista_matriculas_baja.= $anexo->Matricula."<w:br/>";
                 
                }
                $templateWord->setValue('lista_matriculas_baja',$lista_matriculas_baja);
                
                $templateWord->setValue('comentario',$_GET['comentario']);
                $templateWord->setValue('advertencia',$_GET['advertencia']);

                $templateWord->saveAs(storage_path($filename));

    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename=informe_baja_matriculas.docx; charset=iso-8859-1');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize('..\public\temp\informe_baja_matriculas.docx'));
    readfile('..\public\temp\informe_baja_matriculas.docx');
  

    $files = glob('..\public\temp\*'); // get all file names
    foreach($files as $file){ // iterate files
      if(is_file($file))
        unlink($file); // delete file
        }
  exit;

    }
    

public function mostrar_sustitucion_recibos(Request $request){

        if($request->ajax()){

            $codContrato = $request->codContrato;

            $importeCto = $request->importeCto;

            $formas_pago = DB::table("europa_tipo_forma_pago")->get();

            $recibos = DB::table("europa_recibos")->where("codContrato","=",$codContrato)->orderBy("numRecibo",'ASC')->get();

            $output = "<br><label>Recibo de origen</label>";

              $output.='<br><select class="selectpicker" data-live-search="true" title="Buscar..." id="recibo_origen" name="recibo_origen">';

             if($recibos->count()){

                foreach ($recibos as $key => $recibo) {

                     $output.='<option value="'.$recibo->codRecibo.'">'.$recibo->numRecibo.'</option>';
                }

            }
              $output .= "</select><br>";
            
            $output .= "<br><label>Precio</label>";
            $output.='<ul class="list-inline">
                           <li><input type="radio" name="precio" value="automatico" placeholder="Indefinido" checked> <span>Automático</span></li>
                          <li><input type="radio" name="precio" value="manual" placeholder="Facturar"> <span>Manual</span></li>
                           </ul>';


              $output.='<br><label>Número de recibos</label><input type="number" class="form-control" name="num_recibo" id="num_recibo" value="1">';

              $output.='<br><label>Importe</label><input type="number" class="form-control" name="importe_recibo" id="importe_recibo" value="" disabled>';

              $output.='<input type="hidden" class="form-control" name="importe_fijo" id="importe_fijo" value="">';

               $output .= "<br><label>Forma de pago</label>";

              $output.='<br><select class="selectpicker" data-live-search="true" title="Buscar..." id="forma_pago_recibo" name="forma_pago_recibo">';

             if($formas_pago->count()){

                foreach ($formas_pago as $key => $forma_pago) {

                     $output.='<option value="'.$forma_pago->CodFormaPago.'">'.$forma_pago->FormaPago.'</option>';
                }


            }
              $output .= "</select><br>";

              $output.='<br><label>Vencimiento</label><input type="text" class="form-control datepicker" name="venc_recibo" id="venc_recibo" value="">';

              $output.='<br><label>Notas</label><textarea class="form-control" name="notas_recibo" id="notas_recibo"></textarea>';

            return Response($output);

    }

}

public function mostrar_devolucion_recibo(Request $request){

        if($request->ajax()){

            $codContrato = $request->codContrato;

            $recibos = DB::table("europa_recibos")->where("codContrato","=",$codContrato)->orderBy("numRecibo",'ASC')->get();

            $output = "<br><label>Recibo a devolver</label>";

              $output.='<br><select class="selectpicker" data-live-search="true" title="Buscar..." id="recibo_origen" name="recibo_origen">';

             if($recibos->count()){

                foreach ($recibos as $key => $recibo) {

                     $output.='<option value="'.$recibo->codRecibo.'">'.$recibo->numRecibo.'</option>';
                }

            }
              $output .= "</select><br>";

             $output.='<br><label>Gastos devolución</label><input type="number" class="form-control" name="gastosDevolucion" id="gastosDevolucion" value="">';

              $output.='<br><label>Fecha Devolución</label><input type="text" class="form-control datepicker" name="devolucion" id="devolucion" value="">';

              $output.='<br><label>Notas</label><textarea class="form-control" name="notas_recibo" id="notas_recibo"></textarea>';

            return Response($output);

    }

}

public function mostrar_anular_recibo(Request $request){

        if($request->ajax()){

            $codContrato = $request->codContrato;

            $recibos = DB::table("europa_recibos")->where("codContrato","=",$codContrato)->orderBy("numRecibo",'ASC')->get();

            $output = "<br><label>Recibo para anulación</label>";

              $output.='<br><select class="selectpicker" data-live-search="true" title="Buscar..." id="recibo_origen" name="recibo_origen">';

             if($recibos->count()){

                foreach ($recibos as $key => $recibo) {

                     $output.='<option value="'.$recibo->codRecibo.'">'.$recibo->numRecibo.'</option>';
                }

            }
              $output .= "</select><br>";

              $output.='<br><label>Notas</label><textarea class="form-control" name="notas_recibo" id="notas_recibo"></textarea>';

            return Response($output);

    }

}

public function get_importe_recibo(Request $request){

        if($request->ajax()){

           $recibo = DB::table("europa_recibos")->where("codRecibo",'=',$request->origen)->first();

            return Response($recibo->importe);
        }

}

    public function generar_recibos (Request $request){

     if($request->ajax()){

            $recibo = DB::table("europa_recibos")->selectRaw("CASE WHEN max(numRecibo) is not null then max(numRecibo) else '0' end as maximo")->where("codContrato","=",$request->codContrato)->first();

            $maximo = $recibo->maximo;

            $numRecibo = $request->num_recibo;

            $vencimiento = null;

             if($request->vencimiento !=null){
                $vencimiento = DateTime::createFromFormat('d/m/Y', $request->vencimiento);
                date_time_set($vencimiento, 00, 00);
            }

            try {

                for ($i=1; $i <= $numRecibo ; $i++) { 

                    DB::table('europa_recibos')->insertGetId(
                ['CodContrato'=>$request->codContrato, 'numRecibo'=>$maximo+$i,'codFormaPago'=>$request->forma_pago, 'importe'=>$request->importe_recibo, 'codEstado'=>1, 'vencimiento'=>$vencimiento,'notas'=>$request->notas_recibo]);
                     
                 } 

            return  Response("Los recibos se han generado correctamente.");

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }

    
        }

    }

    public function sustituir_recibos (Request $request){

     if($request->ajax()){


            $numRecibo = $request->num_recibo;

            $vencimiento = null;

             if($request->vencimiento !=null){
                $vencimiento = DateTime::createFromFormat('d/m/Y', $request->vencimiento);
                date_time_set($vencimiento, 00, 00);
            }

            try {

                for ($i=1; $i <= $numRecibo ; $i++) { 

                    DB::table('europa_recibos')->insertGetId(
                ['CodContrato'=>$request->codContrato, 'numRecibo'=>$i,'codFormaPago'=>$request->forma_pago, 'importe'=>$request->importe_recibo, 'codEstado'=>1, 'vencimiento'=>$vencimiento,'notas'=>$request->notas_recibo,'origen'=>$request->recibo_origen]);
                     
                 }

                 DB::table('europa_recibos')->where('codRecibo','=',$request->recibo_origen)->update(
                    ['fechaSustitucion'=>date('Y-m-d H:i:s'),'codEstado'=>2]); 

            return  Response("El recibo se ha sustituido correctamente por los nuevos generados.");

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }

    
        }

    }

       public function devolver_recibo (Request $request){

     if($request->ajax()){


            

            $devolucion = null;

             if($request->devolucion !=null){
                $devolucion = DateTime::createFromFormat('d/m/Y', $request->devolucion);
                date_time_set($devolucion, 00, 00);
            }

            try {


                 DB::table('europa_recibos')->where('codRecibo','=',$request->recibo_origen)->update(
                    ['fechaDevolucion'=>$devolucion,'codEstado'=>4,'gastosDevolucion'=>$request->gastos,'notas'=>$request->notas_recibo]); 

            return  Response("El recibo se ha marcado como devuelto correctamente.");

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }

    
        }

    }

    public function anular_recibo (Request $request){

     if($request->ajax()){



            try {


                 DB::table('europa_recibos')->where('codRecibo','=',$request->recibo_origen)->update(
                    ['codEstado'=>3,'notas'=>$request->notas_recibo]); 

            return  Response("El recibo se ha marcado como anulado correctamente.");

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }

    
        }

    }

    public function mostrar_tipos_anexos(){

        $tipos_anexo = DB::table("europa_tipo_anexo")->get();

         return view('contratos/europa/anexos/tipos',['tipos_anexo'=>$tipos_anexo]);

    }

    public function guardar_tipo_anexo(Request $request){
         if($request->ajax()){


            try {


                 DB::table('europa_tipo_anexo')->where('CodtipoAnexo','=',$request->CodtipoAnexo)->update(
                    ['tipoAnexo'=>$request->tipoAnexo,'codTipoContrato'=>$request->tipoContrato,'provisionFondos'=>$request->provisionfondos]); 

            return  Response("El tipo de anexo se ha modificado correctamente.");

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }

    
        }
    }

      public function store_tipo_anexo(Request $request){
         if($request->ajax()){


            try {


                 DB::table('europa_tipo_anexo')->insertGetId(
                    ['tipoAnexo'=>$request->tipoAnexo,'codTipoContrato'=>$request->tipoContrato,'provisionFondos'=>$request->provisionfondos]); 

            return  Response("El tipo de anexo se ha registrado correctamente.");

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }

    
        }
    }

    public function franquicias(){

        $franquicias = DB::table(DB::raw("europa_tarifas a, europa_contactos b, europa_tipo_contrato c, europa_tarifas_comercial d"))->select(DB::raw("a.CodTarifa,a.Tarifa, b.razonSocial, c.tipoServicio, (select razonSocial from europa_contactos where CodContacto=d.CodComercial) as comercial, d.fechaAplicacion"))->whereRaw(" a.codEmpresa=b.CodContacto and 
a.CodTipoContrato=c.codTipoContrato and
a.CodTarifa=d.CodTarifa")->orderby("Tarifa","ASC")->limit(200)->get();

       
        return view('contratos/europa/franquicias/franquicias',['franquicias'=>$franquicias]);    
      }

  public function show_franquicias($id){

        $franquicia = DB::table(DB::raw("europa_tarifas a, europa_contactos b, europa_tipo_contrato c"))->select(DB::raw("a.codEmpresa,a.CodTipoContrato,a.CodTarifa,a.Tarifa, b.razonSocial, c.tipoServicio"))->whereRaw(" a.codEmpresa=b.CodContacto and 
          a.CodTipoContrato=c.codTipoContrato AND a.CodTarifa='".$id."'")->first();

        $condiciones = DB::table("europa_condiciones_contrato")->where("CodTarifa","=",$id)->first();

        //$comerciales = DB::table(DB::raw("europa_tarifas_comercial a, europa_contactos b"))->select(DB::raw("a.*,b.razonSocial"))->whereRaw("a.CodComercial=b.CodContacto AND a.CodTarifa='".$id."'")->get();

        //$precios = DB::table(DB::raw("europa_precios_venta a, europa_tipo_anexo b"))->select(DB::raw(" a.*,b.tipoAnexo "))->whereRaw("a.CodTipoAnexo = b.CodtipoAnexo AND a.codCondiciones='".$condiciones->codCondiciones."'")->get();

       
        return view('contratos/europa/franquicias/detail',['franquicia'=>$franquicia,'condiciones'=>$condiciones]);    
      }


       public function precios(Request $request)
    {

          if($request->ajax()){

            $condiciones = $request->codCondiciones;

            $tipoContrato =$request->tipoContrato;


            
             $output = "<table class='table' id='table_motivos'>
                        <thead class='table-header'>
                            <th>Tipo Anexo</th>                        
                            <th>Precio Min.</th>
                            <th>% Renovacion</th>
                            <th>Precio Renovacion</th>
                            <th></th>
                        </thead>
                        <tbody id='myTable'>";
            $precios = DB::table(DB::raw("europa_precios_venta a, europa_tipo_anexo b"))->select(DB::raw(" a.*,b.tipoAnexo "))->whereRaw("a.CodTipoAnexo = b.CodtipoAnexo AND a.codCondiciones='".$condiciones."'")->get();

            $tipos_anexo = DB::table("europa_tipo_anexo")->where("codTipoContrato","=",$tipoContrato)->get();

            if($precios->count()){

                foreach ($precios as $key => $precio) {

                     $output.='<tr>'.
                              '<td><input type="hidden" class="codCondiciones" value="'.$precio->codCondiciones.'"><select class="selectpicker tipoAnexo" data-container="body" data-live-search="true" title="buscar..." name="tipoAnexo">';
                      foreach ($tipos_anexo as $key => $tipo) {
                            if($precio->CodTipoAnexo == $tipo->CodtipoAnexo){
                              $output.='<option value="'.$tipo->CodtipoAnexo.'" selected>'.$tipo->tipoAnexo.'</option>';
                            }else{
                               $output.='<option value="'.$tipo->CodtipoAnexo.'">'.$tipo->tipoAnexo.'</option>';
                            }
                        }
            $output.='</select></td>'.
                              '<td><input type="text" class="form-control precioMinimoVenta" name="tipoAnexo" value="'.$precio->precioMinimoVenta.'"></td>'.
                             '<td><input type="text" class="form-control comisionRenovacion" name="tipoAnexo" value="'.$precio->comisionRenovacion.'"></td>'.
                             '<td><input type="text" class="form-control precioMinimoRenovacion" name="tipoAnexo" value="'.$precio->precioMinimoRenovacion.'"></td>'.
                             '<td><a class="btn btn-sm btn-success editar_precio" href="#"><i class="fa fa-edit"></i></a> <a class="btn btn-sm btn-danger eliminar_precio"><i class="fa fa-trash"></i></a></td>'.
                             '</tr>';

                }
                
            }

             $output.='<tr>'.
                     '<td><select class="selectpicker tipoAnexo" data-container="body" data-live-search="true" title="buscar..." id="addpais" name="addpais">';
                      foreach ($tipos_anexo as $key => $tipo) {
                               $output.='<option value="'.$tipo->CodtipoAnexo.'">'.$tipo->tipoAnexo.'</option>';     
                        }
            $output.='</select></td>'.
                     '<td><input type="number" class="form-control precioMinimoVenta" value=""></td>'.
                     '<td><input type="number" class="form-control comisionRenovacion" value=""></td>'.  
                     '<td><input type="number" class="form-control precioMinimoRenovacion" value=""></td>';  
                     

            $output.='<td><a class="btn btn-success" id="add_precio"><i class="fa fa-plus"></i></a></td>'.
                     '</tr>';


            $output .= "</tbody></table><input type='hidden' id='tabla_tipo' value='direcciones_base'>";
            
            $output .= "</tbody></table><input type='hidden' id='tabla_tipo' value='motivo'>";
            return Response($output);
        }
    }

     public function comerciales(Request $request)
    {

          if($request->ajax()){

            $codTarifa = $request->CodTarifa;
            $tipoContrato = $request->tipoContrato;


            
             $output = "<table class='table' id='table_motivos'>
                        <thead class='table-header'>
                            <th>Nombre comercial</th>                        
                            <th>Fecha de aplicación</th>
                            <th></th>
                        </thead>
                        <tbody id='myTable'>";
           $comerciales_tarifa = DB::table(DB::raw("europa_tarifas_comercial a, europa_contactos b"))->select(DB::raw("a.*,b.razonSocial"))->whereRaw("a.CodComercial=b.CodContacto AND a.CodTarifa='".$codTarifa."'")->get();

           $comerciales_general = DB::table("europa_contactos")->whereRaw("CodContacto in (select codContacto from europa_contactos_funcion where codTipoFuncion='1')")->orderBy("razonSocial","ASC")->get();

            $tipos_anexo = DB::table("europa_tipo_anexo")->where("codTipoContrato","=",$tipoContrato)->get();

            if($comerciales_tarifa->count()){

                foreach ($comerciales_tarifa as $key => $comercial) {

                     $output.='<tr>'.
                              '<td><input type="hidden" class="CodComercial" value="'.$comercial->CodComercial.'">'.$comercial->razonSocial.'</td>'.
                             '<td>'.date('d/m/Y', strtotime($comercial->fechaAplicacion)).'</td>'.
                             '<td><a class="btn btn-sm btn-danger eliminar_comercial"><i class="fa fa-trash"></i></a></td>'.
                             '</tr>';

                }

            }

             $output.='<tr>'.
                     '<td><select class="selectpicker CodComercial" data-container="body" data-live-search="true" title="buscar..." name="CodComercial">';
                      foreach ($comerciales_general as $key => $comercial) {
                               $output.='<option value="'.$comercial->CodContacto.'">'.$comercial->razonSocial.'</option>';     
                        }
            $output.='</select></td>'.
                     '<td><input type="text" class="form-control datepicker fechaAplicacion" value=""></td>';  
            $output.='<td><a class="btn btn-success" id="add_comercial"><i class="fa fa-plus"></i></a></td>'.
                     '</tr>';
            $output .= "</tbody></table><input type='hidden' id='tabla_tipo' value='comerciales'>";
            
            return Response($output);
        }
    }

    public function update_franquicia(Request $request){

         //TODO
        if($request->ajax()){

            $datos = array();
            parse_str($request->datos,$datos);

            $Tarifa = null;
            $codTarifa = null;
            $contratante = null;
            $tipo_contrato= null;
         
            $fechaEfecto = null;
            $porcentajeComision = null;
            $pctjeComisionExcep = null;
            $porcentajeDeduccion = null;


            }

            if($datos["codTarifa"]!=null){
                $codTarifa = $datos["codTarifa"];
            }

            if($datos["Tarifa"]!=null){
                $Tarifa = $datos["Tarifa"];
            }


            if($datos["contratante"]!=null){
                $contratante = $datos["contratante"];
            }

            if($datos["tipo_contrato"]!=null){
                $tipo_contrato = $datos["tipo_contrato"];
            }

            if($datos["porcentajeComision"]!=null){
                $porcentajeComision = $datos["porcentajeComision"];
            }
              if($datos["pctjeComisionExcep"]!=null){
                $pctjeComisionExcep = $datos["pctjeComisionExcep"];
            }
              if($datos["porcentajeDeduccion"]!=null){
                $porcentajeDeduccion = $datos["porcentajeDeduccion"];
            }

    
            if($datos["fechaEfecto"] !=null){
                $fechaEfecto = DateTime::createFromFormat('d/m/Y', $datos["fechaEfecto"]);
                date_time_set($fechaEfecto, 00, 00);
            }



            try { 

                //Seleccionar las condiciones del contrato según los parametros establecidos
            
              DB::table("europa_tarifas")->where("codTarifa",'=',$codTarifa)->update([
              'Tarifa'=>$Tarifa, 'CodTipoContrato'=>$tipo_contrato, 'codEmpresa'=>$contratante]);


              DB::table('europa_condiciones_contrato')->where("CodTarifa","=",$codTarifa)->update([
              'FechaEfecto'=>$fechaEfecto,'porcentajeComision'=>$porcentajeComision, 'pctjeComisionExcep'=>$pctjeComisionExcep, 'fechaModif'=>date('Y-m-d H:i:s'), 'porcentajeDeduccion'=>$porcentajeDeduccion]);  
            return  Response("La franquicia se ha actualizado correctamente");

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }

    }

    public function eliminar_franquicia(Request $request){

         if($request->ajax()){  

    $action = $request->action;
    $codTarifa = $request->codTarifa;
   
    $output = "<i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Se han encontrado errores.</strong> No se ha podido llevar a cabo la acción.Si el problema persiste, póngase en contacto con el administrador.";

    switch ($action) {
        case 1:

            $CodTipoAnexo = $request->CodTipoAnexo;
            $codCondiciones = $request->codCondiciones;

                  try { 

    
                DB::table('europa_precios_venta')->whereRaw("CodTipoAnexo = '".$CodTipoAnexo."' AND codCondiciones='".$codCondiciones."'")->delete();       

            
             $output = "<a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a><i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Precio eliminado correctamente.</strong>";

            return  Response($output);

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }

            break;

             case 2:

            $CodComercial = $request->CodComercial;


       
                  try { 


            //DB::enableQueryLog();
                DB::table("europa_tarifas_comercial")->whereRaw("CodComercial = '".$CodComercial."' AND codTarifa='".$codTarifa."'")->delete();

                 


            //dd(DB::getQueryLog());

            $output = "<a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a><i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Comercial desvinculado correctamente.</strong>";
            
             return Response($output);

               return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }

      
            break;

        default:
            # code...
            break;
            
    }

}

    }

     public function insertar_franquicia(Request $request)
    {

          if($request->ajax()){  

    $action = $request->action;
    
   
    $output = "<i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Se han encontrado errores.</strong> No se ha podido llevar a cabo la inserción de datos.Si el problema persiste, póngase en contacto con el administrador.";

    switch ($action) {
        case 1:
  
            $codCondiciones = $request->codCondiciones;
            $CodTipoAnexo = $request->CodTipoAnexo;
            $precioMinimoVenta = $request->precioMinimoVenta;
            $comisionRenovacion = $request->comisionRenovacion;
            $precioMinimoRenovacion = $request->precioMinimoRenovacion;

                  try { 


            //DB::enableQueryLog();
    
                DB::table('europa_precios_venta')->insert(
                array('codCondiciones'=>$codCondiciones,
                    'CodTipoAnexo' => $CodTipoAnexo,
                      'precioMinimoVenta'=>$precioMinimoVenta,
                      'comisionRenovacion'=>$comisionRenovacion,
                      'precioMinimoRenovacion'=>$precioMinimoRenovacion));

                //dd(DB::getQueryLog());

             $output = "<a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a><i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Precio Añadido correctamente.</strong>";

            return  Response($output);

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }

            break;

             case 2:

            $CodComercial = $request->CodComercial;
            $fechaAplicacion = $request->fechaAplicacion;
            $fecha = DateTime::createFromFormat('d/m/Y', $fechaAplicacion);
             date_time_set($fecha, 00, 00);
            


            

                  try { 


            //DB::enableQueryLog();
                DB::table("europa_tarifas_comercial")->insert(
                array('CodComercial'=>$CodComercial, 'CodTarifa'=>$codTarifa, 'fechaAplicacion'=>$fecha));

            //dd(DB::getQueryLog());

            $output = "<a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a><i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> El comercial se ha asociado a la franquicia correctamente.</strong>";
            
             return Response($output);

               return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }

        

            break;

            case 3:

            $categoria = $request->categoria;
            $vinculacion = $request->vinculacion;

            try { 

             DB::table("europa_contactos_funcion")->insert(
                array('CodContacto' => $CodContacto,
                      'CodSupJerarquico' => $vinculacion,
                      'codTipoFuncion'=>$categoria));

            $output = "<a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a><i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Datos insertados correctamente.</strong>";
            
             return Response($output);

               return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }
      

                

            break;

        default:
            # code...
            break;
            
    }

}

    }

    public function update_precio(Request $request){

           $codTarifa = $request->codTarifa;
            $codCondiciones = $request->codCondiciones;
            $CodTipoAnexo = $request->CodTipoAnexo;

            if($request->precioMinimoVenta!="")
            $precioMinimoVenta = $request->precioMinimoVenta;
            else
            $precioMinimoVenta = null;

            if($request->comisionRenovacion!="")
            $comisionRenovacion = $request->comisionRenovacion;
            else
            $comisionRenovacion = null;

            if($request->precioMinimoRenovacion!="")
            $precioMinimoRenovacion = $request->precioMinimoRenovacion;
            else
            $precioMinimoRenovacion = null;

       
                  try { 


            //DB::enableQueryLog();

                DB::table("europa_precios_venta")->whereRaw("codCondiciones = '".$codCondiciones."' AND CodTipoAnexo='".$CodTipoAnexo."'")->update(
                array('CodTipoAnexo' => $CodTipoAnexo,
                      'precioMinimoVenta'=>$precioMinimoVenta,
                      'comisionRenovacion'=>$comisionRenovacion,
                      'precioMinimoRenovacion'=>$precioMinimoRenovacion));

            //dd(DB::getQueryLog());

            $output = "<a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a><i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Datos modificados correctamente.</strong>";
            
             return Response($output);

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }

    }

    public function nueva_franquicia(){

       return view('contratos/europa/franquicias/nuevo');

    }

    public function clonar_gesanc(Request $request){

        if($request->ajax()){

            $CodAnexo = $request->CodAnexo;

            $datos = array();
            parse_str($request->datos,$datos);

            $codContratado = null;
            $codContratante = null;
            $codComercial= null;
            $CodTipoAnexo = null;
            $codContrato = null;
            $importe = null;
            $matricula = null;
            $alta = null;
            $vencimiento = null;
            $cod_contrato = null;
            $notas = null;
            $baja = null;
            $motivo_baja = null;
            $esrenovacion = 0;
            $norenovar = 0;

            }

            if(array_key_exists("esrenovacion",$datos)){
               $esrenovacion = 1;
            }

            if(array_key_exists("norenovar",$datos)){
               $norenovar = 1;
            }

            if($datos["codContratado"]!=null){
                $codContratado = $datos["codContratado"];
            }

            if($datos["codContratante"]!=null){
                $codContratante = $datos["codContratante"];
            }

            if($datos["codComercial"]!=null){
                $codComercial = $datos["codComercial"];
            }

             if($datos["codContrato"]!=null){
                $codContrato = $datos["codContrato"];
            }

            if($datos["tipo_anexo"]!=null){
                $CodTipoAnexo = $datos["tipo_anexo"];
            }

            if($datos["motivo_baja"]!=null){
                $motivo_baja = $datos["motivo_baja"];
            }

            if($datos["matricula"]!=null){
                $matricula = $datos["matricula"];
            }

            if($datos["importe"]!=null){
                $importe = $datos["importe"];
            }

            if($datos["notas"]!=null){
                $notas = $datos["notas"];
            }

    
            if($datos["alta"] !=null){
                $alta = DateTime::createFromFormat('d/m/Y', $datos["alta"]);
                date_time_set($alta, 00, 00);
            }

             if($datos["baja"] !=null){
                $baja = DateTime::createFromFormat('d/m/Y', $datos["baja"]);
                date_time_set($baja, 00, 00);
            }

        


            try { 

                //Seleccionar las condiciones del contrato según los parametros establecidos

           $contacto = DB::table("europa_contactos")->whereRaw("CodContacto =(select codContratado from europa_contratos where codContrato =(select codContrato from europa_anexos where codAnexo='".$CodAnexo."'))")->first();

           $provincia = DB::table("europa_provincias")->where("CodProvincia","=",$contacto->codProvincia)->first();

           $pais = DB::table("europa_paises")->where("CodPais","=",$contacto->codPais)->first();

           $delegacion = DB::table("europa_contactos_funcion")->where("CodContacto","=",$contacto->CodContacto)->first();

           $comercial = DB::table("europa_contactos_funcion")->whereRaw("CodContacto='".$contacto->CodContacto."' AND codTipoFuncion=5")->first();
           $sucomercial = null;

           $existente = DB::table("gesanc_clientes")->where("cif","=",$contacto->cif)->first();

            if($existente!=null){
                if($sucomercial !=null){
            $sucomercial = $comercial->CodSupJerarquico;
           }

              DB::table('gesanc_clientes')->insertGetId(
                ['cif'=>$contacto->cif, 'empresa'=>$contacto->razonSocial, 'direccion'=>$contacto->direccion, 'cp'=>$contacto->codPostal, 'poblacion'=>$contacto->Poblacion, 'provincia'=>$provincia->Provincia, 'pais'=>$pais->Pais,'delegacion'=>$delegacion->CodSupJerarquico, 'Mailing'=>$contacto->mailing, 'Observacion'=>$contacto->observaciones, 'email'=>$contacto->email, 'url'=>$contacto->web, 'Id-comercial'=>$sucomercial,'fechaintrodatos'=>date('Y-m-d H:i:s'), 'fechamodifacion'=>date('Y-m-d H:i:s'),'impagado_pdteDev'=>$contacto->impagado_pdteDev, 'CC'=>$contacto->CC, 'cteConflictivo'=>$contacto->cteConflictivo, 'apartadoCorreos'=>$contacto->apartadoCorreos]);

              $contacto_creado = DB::table("gesanc_clientes")->orderby('fechaintrodatos',"DESC")->first();

              //añadir los telefonos
              $telefonos = DB::table("europa_contactos_telefonos")->where("CodContacto","=",$contacto->CodContacto)->get();

              foreach ($telefonos as $key => $telefono) {
             
                if($telefono->numOrdenGesanc > 2 && $telefono->codTipoTelefono == 1){
                    DB::table("gesanc_clientes")->where("CODCLI","=",$contacto_creado->CODCLI)->update(['tel'.$key+1=>$telefono->CodTelefono]);
                }
                if($telefono->numOrdenGesanc > 2 && $telefono->codTipoTelefono == 2){
                    DB::table("gesanc_clientes")->where("CODCLI","=",$contacto_creado->CODCLI)->update(['movil'.$key+1=>$telefono->CodTelefono]);
                }
                if($telefono->numOrdenGesanc > 2 && $telefono->codTipoTelefono == 3){
                    DB::table("gesanc_clientes")->where("CODCLI","=",$contacto_creado->CODCLI)->update(['fax'.$key+1=>$telefono->CodTelefono]);
                }
              }
            }

            //crear el contrato

              $clavecontrato = $CodAnexo + $codContratante * 1000000;


              $tipo = DB::table("europa_tipo_anexo")->where("CodTipoAnexo","=",$CodTipoAnexo)->first();


              DB::table("gesanc_contratos")->insertGetId(['clavecontrato'=>$clavecontrato, 'cif'=>$contacto->cif,'contratante'=>$codContratante, 'FechaContrato'=>$alta, 'Id-comercial'=>$codComercial,'Tipo'=>$tipo->tipoAnexo,'Reduccion'=>0, 'Notas'=>$notas,'Matricula'=>$matricula,'Alta'=>$alta, 'Baja'=>$baja, 'Vencimiento'=>$vencimiento, 'contratado'=>$contacto_creado->CODCLI,'fechaintrodatos'=>date('Y-m-d H:i:s'), 'fechamodifacion'=>date('Y-m-d H:i:s'), 'precio'=>$importe]);


              //crear los remolques
              $contrato_creado = DB::table("gesanc_contratos")->orderby("fechaintrodatos","DESC")->first();

              $anexosremolques = DB::table("europa_anexos_remolques")->whereRaw("codAnexoRemolque not in (select CodSustituido from europa_anexos_remolques_sustituciones) and codAnexo='".$codContrato."'")->get();

              if($anexosremolques!=null){
                  foreach ($anexosremolques as $key => $remolque) {
                  DB::table("gesanc_remolques")->insertGetId(['idContrato'=>$contrato_creado->idcontrato, 'matricula'=>$remolque->matricula, 'Alta'=>$remolque->Alta, 'Baja'=>$remolque->Baja, 'fechaintrodatos'=>date('Y-m-d H:i:s'), 'fechamodifacion'=>date('Y-m-d H:i:s')]);
                    }
              }
            
          
            return  Response($contrato_creado->idcontrato);

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }
    }

    public function clonar_its(Request $request){

        if($request->ajax()){

            $CodAnexo = $request->CodAnexo;

            $datos = array();
            parse_str($request->datos,$datos);

            $codContratado = null;
            $codContratante = null;
            $codComercial= null;
            $CodTipoAnexo = null;
            $codContrato = null;
            $importe = null;
            $matricula = null;
            $alta = null;
            $vencimiento = null;
            $cod_contrato = null;
            $notas = null;
            $baja = null;
            $motivo_baja = null;
            $esrenovacion = 0;
            $norenovar = 0;

            }

            if(array_key_exists("esrenovacion",$datos)){
               $esrenovacion = 1;
            }

            if(array_key_exists("norenovar",$datos)){
               $norenovar = 1;
            }

            if($datos["codContratado"]!=null){
                $codContratado = $datos["codContratado"];
            }

            if($datos["codContratante"]!=null){
                $codContratante = $datos["codContratante"];
            }

            if($datos["codComercial"]!=null){
                $codComercial = $datos["codComercial"];
            }

             if($datos["codContrato"]!=null){
                $codContrato = $datos["codContrato"];
            }

            if($datos["tipo_anexo"]!=null){
                $CodTipoAnexo = $datos["tipo_anexo"];
            }

            if($datos["motivo_baja"]!=null){
                $motivo_baja = $datos["motivo_baja"];
            }

            if($datos["matricula"]!=null){
                $matricula = $datos["matricula"];
            }

            if($datos["importe"]!=null){
                $importe = $datos["importe"];
            }

            if($datos["notas"]!=null){
                $notas = $datos["notas"];
            }

    
            if($datos["alta"] !=null){
                $alta = DateTime::createFromFormat('d/m/Y', $datos["alta"]);
                date_time_set($alta, 00, 00);
            }

             if($datos["baja"] !=null){
                $baja = DateTime::createFromFormat('d/m/Y', $datos["baja"]);
                date_time_set($baja, 00, 00);
            }

        


            try { 

                //Seleccionar las condiciones del contrato según los parametros establecidos

           $contacto = DB::table("europa_contactos")->whereRaw("CodContacto =(select codContratado from europa_contratos where codContrato =(select codContrato from europa_anexos where codAnexo='".$CodAnexo."'))")->first();

           $provincia = DB::table("europa_provincias")->where("CodProvincia","=",$contacto->codProvincia)->first();

           $pais = DB::table("europa_paises")->where("CodPais","=",$contacto->codPais)->first();

           $delegacion = DB::table("europa_contactos_funcion")->where("CodContacto","=",$contacto->CodContacto)->first();

           $comercial = DB::table("europa_contactos_funcion")->whereRaw("CodContacto='".$contacto->CodContacto."' AND codTipoFuncion=5")->first();
           $sucomercial = null;

           $existente = DB::table("gesanc_clientes")->where("cif","=",$contacto->cif)->first();

            if($existente!=null){
                if($sucomercial !=null){
            $sucomercial = $comercial->CodSupJerarquico;
           }

              DB::table('gesanc_clientes')->insertGetId(
                ['cif'=>$contacto->cif, 'empresa'=>$contacto->razonSocial, 'direccion'=>$contacto->direccion, 'cp'=>$contacto->codPostal, 'poblacion'=>$contacto->Poblacion, 'provincia'=>$provincia->Provincia, 'pais'=>$pais->Pais,'delegacion'=>$delegacion->CodSupJerarquico, 'Mailing'=>$contacto->mailing, 'Observacion'=>$contacto->observaciones, 'email'=>$contacto->email, 'url'=>$contacto->web, 'Id-comercial'=>$sucomercial,'fechaintrodatos'=>date('Y-m-d H:i:s'), 'fechamodifacion'=>date('Y-m-d H:i:s'),'impagado_pdteDev'=>$contacto->impagado_pdteDev, 'CC'=>$contacto->CC, 'cteConflictivo'=>$contacto->cteConflictivo, 'apartadoCorreos'=>$contacto->apartadoCorreos]);

              $contacto_creado = DB::table("gesanc_clientes")->orderby('fechaintrodatos',"DESC")->first();

              //añadir los telefonos
              $telefonos = DB::table("europa_contactos_telefonos")->where("CodContacto","=",$contacto->CodContacto)->get();

              foreach ($telefonos as $key => $telefono) {
             
                if($telefono->numOrdenGesanc > 2 && $telefono->codTipoTelefono == 1){
                    DB::table("gesanc_clientes")->where("CODCLI","=",$contacto_creado->CODCLI)->update(['tel'.$key+1=>$telefono->CodTelefono]);
                }
                if($telefono->numOrdenGesanc > 2 && $telefono->codTipoTelefono == 2){
                    DB::table("gesanc_clientes")->where("CODCLI","=",$contacto_creado->CODCLI)->update(['movil'.$key+1=>$telefono->CodTelefono]);
                }
                if($telefono->numOrdenGesanc > 2 && $telefono->codTipoTelefono == 3){
                    DB::table("gesanc_clientes")->where("CODCLI","=",$contacto_creado->CODCLI)->update(['fax'.$key+1=>$telefono->CodTelefono]);
                }
              }
            }

            //crear el contrato

              $clavecontrato = $CodAnexo + $codContratante * 1000000;


              $tipo = DB::table("europa_tipo_anexo")->where("CodTipoAnexo","=",$CodTipoAnexo)->first();


              DB::table("gesanc_contratos")->insertGetId(['clavecontrato'=>$clavecontrato, 'cif'=>$contacto->cif,'contratante'=>$codContratante, 'FechaContrato'=>$alta, 'Id-comercial'=>$codComercial,'Tipo'=>$tipo->tipoAnexo,'Reduccion'=>0, 'Notas'=>$notas,'Matricula'=>$matricula,'Alta'=>$alta, 'Baja'=>$baja, 'Vencimiento'=>$vencimiento, 'contratado'=>$contacto_creado->CODCLI,'fechaintrodatos'=>date('Y-m-d H:i:s'), 'fechamodifacion'=>date('Y-m-d H:i:s'), 'precio'=>$importe]);


              //crear los remolques
              $contrato_creado = DB::table("gesanc_contratos")->orderby("fechaintrodatos","DESC")->first();

              $anexosremolques = DB::table("europa_anexos_remolques")->whereRaw("codAnexoRemolque not in (select CodSustituido from europa_anexos_remolques_sustituciones) and codAnexo='".$codContrato."'")->get();

              if($anexosremolques!=null){
                  foreach ($anexosremolques as $key => $remolque) {
                  DB::table("gesanc_remolques")->insertGetId(['idContrato'=>$contrato_creado->idcontrato, 'matricula'=>$remolque->matricula, 'Alta'=>$remolque->Alta, 'Baja'=>$remolque->Baja, 'fechaintrodatos'=>date('Y-m-d H:i:s'), 'fechamodifacion'=>date('Y-m-d H:i:s')]);
                    }
              }
            
          
            return  Response($contrato_creado->idcontrato);

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }
    }

    public function ver_renovaciones (Request $request){

        if($request->ajax()){

            $year = date("Y");
            $year2 = $year -1;
            $year3 = $year -2;
            $year4 = $year -3;
            $year5 = $year -4;

            $contratos = DB::table(DB::raw("europa_contratos a, europa_contactos b"))->selectRaw("a.*,b.razonSocial")->whereRaw("a.codContratado=b.CodContacto and year(Vencimiento) = '".$year."'")->get();
            $clientes = DB::table("europa_contactos")->whereRaw("CodContacto in (select CodContratado from europa_contratos)")->get();
          
          

            

             $output = '<div class="row"><div class="col-md-12"> <div class="form-group col-md-3">
                            <label for="empresa">Año</label>
                             <select class="selectpicker" data-live-search="true" title="Buscar..." id="fechaactual" name="fechaactual">
                             <option selected value='.$year.'>'.$year.'</option>
                             <option value='.$year2.'>'.$year2.'</option>
                             <option value='.$year3.'>'.$year3.'</option>
                             <option value='.$year4.'>'.$year4.'</option>
                             <option value='.$year5.'>'.$year5.'</option>
                             </select>
                        </div>';
            $output.='<div class="form-group col-md-3">
                            <label for="empresa">Mes</label>
                             <select class="selectpicker" data-live-search="true" title="Buscar..." id="fechaactual" name="fechaactual">
                             <option selected value="1">Enero</option>
                            <option  value="1">Enero</option>
                            <option  value="1">Febrero</option>
                            <option  value="1">Marzo</option>
                            <option  value="1">Abril</option>
                            <option  value="1">Mayo</option>
                            <option  value="1">Junio</option>
                            <option  value="1">Agosto</option>
                            <option  value="1">Septiembre</option>
                            <option  value="1">Octubre</option>
                            <option  value="1">Noviembre</option>
                            <option  value="1">Diciembre</option>
                             </select>
                        </div>';



                        



    $output.='<div class="form-group col-md-4"><label for="empresa">Empresa</label><select class="selectpicker" data-live-search="true" title="Buscar..." id="contrato_origen" name="contrato_origen">';
                foreach ($clientes as $key => $cliente) {
                    $output.= '<optgroup label="'.$cliente->razonSocial.'">';
                    foreach ($contratos as $key => $contrato) {
                     if($contrato->codContratado == $cliente->CodContacto){
                       $output.='<option value="'.$contrato->codContrato.'">'.$contrato->codContrato.' - '.date('d/m/Y', strtotime($contrato->fechaContrato)).'</option>';
                     }
                    }
                    $output.='</optgroup>';
                }
            
              $output .= "</select>";            
              $output.='</div><div class="form-group col-md-4"><button type="button" class="btn btn-primary imprimir">Imprimir Contrato</button></div><div class="form-group col-md-4"><button type="button" class="btn btn-primary imprimir">Imprimir Condiciones</button></div></div>';

               return Response($output);
    }

}

public function exportar_excel_europa(Request $request){

    $filtros = array();

            $consulta = "";

              if($request->alta_inicio != null && $request->alta_fin !=null){
                $alta_inicio = $request->alta_inicio;
                $alta_fin = $request->alta_fin;

                $FechaContrato = "FechaContrato BETWEEN '".$alta_inicio."' AND '".$alta_fin."'";

                $filtros[] = $FechaContrato;

                //$sanciones=gesanc_sanciones::whereRaw($consulta)->get();
              
            }

             if($request->baja_inicio != null && $request->baja_fin !=null){
                $baja_inicio = $request->baja_inicio;
                $baja_fin = $request->baja_fin;

                $FechaBaja = "baja BETWEEN '".$baja_inicio."' AND '".$baja_fin."'";

                $filtros[] = $FechaBaja;

                //$sanciones=gesanc_sanciones::whereRaw($consulta)->get();
              
            }

             if($request->venc_inicio != null && $request->venc_fin !=null){
                $venc_inicio = $request->venc_inicio;
                $venc_fin = $request->venc_fin;

                $FechaVencimiento = "vencimiento BETWEEN '".$venc_inicio."' AND '".$venc_fin."'";

                $filtros[] = $FechaVencimiento;

                //$sanciones=gesanc_sanciones::whereRaw($consulta)->get();
              
            }

             if($request->altaanexo_inicio != null && $request->altaanexo_fin !=null){
                $altaanexo_inicio = $request->altaanexo_inicio;
                $altaanexo_fin = $request->altaanexo_fin;

                $Alta_anexo = "(CodContrato in (select CodContrato from europa_anexos where Alta between '".$altaanexo_inicio."' and '".$altaanexo_fin."'))";

                $filtros[] = $Alta_anexo;

                //$sanciones=gesanc_sanciones::whereRaw($consulta)->get();
              
            }

            if($request->codContrato != null){
                $contratante = $request->codContrato;
                $filtro_codContrato = "codContrato = '".$codContrato."'";
                $filtros[] = $filtro_codContrato;
            }

             if($request->importe != null){
                $importe = $request->importe;
                $filtro_importe = "importeCto = '".$importe."'";
                $filtros[] = $filtro_importe;
            } 

             if($request->comision != null){
                $comision = $request->comision;
                $filtro_comision = "porcentComisEspecifica = '".$comision."'";
                $filtros[] = $filtro_comision;
            } 

             if($request->descuento != null){
                $descuento = $request->descuento;
                $filtro_descuento = "descuento = '".$descuento."'";
                $filtros[] = $filtro_descuento;
            }

             if($request->otrocomercial != null){
                $otrocomercial = $request->otrocomercial;
                $filtro_otrocomercial = "esCteDeOtroComercial = '".$otrocomercial."'";
                $filtros[] = $filtro_otrocomercial;
            }
             if($request->pendienteforma != null){
                $pendienteforma = $request->pendienteforma;
                $filtro_pendiente = "pdteFormaPago = '".$pendienteforma."'";
                $filtros[] = $filtro_pendiente;
            }    


             if($request->contratante != null){
                $contratante = $request->contratante;
                $filtro_contratante = "codContratante = '".$contratante."'";
                $filtros[] = $filtro_contratante;
            } 

            if($request->empresa != null){
                $empresa = $request->empresa;
                $filtro_contratado = "codContratado = '".$empresa."'";
                $filtros[] = $filtro_contratado;
            } 

            if($request->matricula != null){
                $matricula = $request->matricula;
                $filtro_matricula = "codContrato in (select codContrato from europa_anexos where Matricula='".$matricula."')";
                $filtros[] = $filtro_matricula;
            } 

             if($request->comercial != null){
                $comercial = $request->comercial;
                $filtro_comercial = "codComercial = '".$comercial."'";
                $filtros[] = $filtro_comercial;
            } 

            if($request->tipo_anexo != null){
                $tipo_anexo = $request->tipo_anexo;
                $filtro_tipoanexo = "codContrato in (select codContrato from europa_anexos where CodTipoAnexo='".$tipo_anexo."')";
                $filtros[] = $filtro_tipoanexo;
            } 

            if($request->tipo_abono != null){
                $tipo_abono = $request->tipo_abono;
                $filtro_tipoabono = "codContrato in (select codContrato from europa_abonos where tipoAbono='".$tipo_abono."')";
                $filtros[] = $filtro_tipoabono;
            }

            for ($i =count($filtros) - 1; $i >= 0 ; $i--) { 
                $consulta .= $filtros[$i];
                $consulta .= " ";

                if($i > 0){
                    $consulta .= "AND ";
                }
            }

            $contratos = DB::table(DB::raw("europa_contratos a, europa_contactos b"))->select(DB::raw("a.CodContrato,a.FechaContrato,a.Vencimiento,a.importeCto,a.observaciones,a.codTipoContrato, b.razonSocial"))->whereRaw("a.codContratado=b.CodContacto")->orderby("FechaContrato","DESC")->limit(200)->get();



            
            if($consulta !=""){

              $contratos = DB::table(DB::raw("europa_contratos a, europa_contactos b"))->select(DB::raw("a.CodContrato,a.FechaContrato,a.Vencimiento,a.importeCto,a.observaciones,a.codTipoContrato, b.razonSocial"))->whereRaw("a.codContratado=b.CodContacto AND ".$consulta)->orderby("FechaContrato","DESC")->limit(200)->get();

             }

             $objPHPExcel = new PHPExcel();
        // Se asignan las propiedades del libro
        $objPHPExcel->getProperties()->setCreator("ITS") // Nombre del autor
        ->setLastModifiedBy("ITS") //Ultimo usuario que lo modificó
        ->setTitle("contratos_filtrados") // Titulo
        ->setDescription("contratos_filtrados"); //Descripción
        $titulosColumnas = array('Cliente'
                                ,'Tipo'
                                ,'Fecha'
                                ,'Vencimiento'
                                ,'Importe'
                                ,'Observaciones');
        
        // Se combinan las celdas, para colocar ahí el titulo del reporte
        //$objPHPExcel->setActiveSheetIndex(0)
        //->mergeCells('A1:D1');
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1', $titulosColumnas[0])
        ->setCellValue('B1', $titulosColumnas[1])
        ->setCellValue('C1', $titulosColumnas[2])
        ->setCellValue('D1', $titulosColumnas[3])
        ->setCellValue('E1', $titulosColumnas[4])
        ->setCellValue('F1', $titulosColumnas[5]);

        $i=2;
        foreach ($contratos as $key => $contrato) {
              $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.$i, $contrato->razonSocial)
            ->setCellValue('B'.$i, $contrato->codTipoContrato)
            ->setCellValue('C'.$i, date('d - m - Y', strtotime($contrato->FechaContrato)))
            ->setCellValue('D'.$i, date('d - m - Y', strtotime($contrato->Vencimiento)))
            ->setCellValue('E'.$i, $contrato->importeCto)
            ->setCellValue('F'.$i, $contrato->observaciones);
            $i++;
        }



    header('Content-Encoding: UTF-8');
    header('Content-type: text/csv; charset=UTF-8');
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="contratos_filtrados.xlsx"');
    header('Cache-Control: max-age=0');
    header("Pragma: no-cache");
    header("Expires: 0");
    header('Content-Transfer-Encoding: binary');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');

    }

    public function generar_excel_anexos (Request $request){
        $filtros = array();

            $consulta = "";

              if($request->alta_contrato_inicio != null && $request->alta_contrato_fin !=null){
                $alta_contrato_inicio = $request->alta_contrato_inicio;
                $alta_contrato_fin = $request->alta_contrato_fin;

                $FechaContrato = "b.FechaContrato BETWEEN '".$alta_contrato_inicio."' AND '".$alta_contrato_fin."'";

                $filtros[] = $FechaContrato;

                //$sanciones=gesanc_sanciones::whereRaw($consulta)->get();
              
            }

             if($request->alta_inicio != null && $request->alta_fin !=null){
                $alta_inicio = $request->alta_inicio;
                $alta_fin = $request->alta_fin;

                $FechaAlta = "a.Alta BETWEEN '".$baja_inicio."' AND '".$baja_fin."'";

                $filtros[] = $FechaAlta;

                //$sanciones=gesanc_sanciones::whereRaw($consulta)->get();
              
            }

             if($request->baja_inicio != null && $request->baja_fin !=null){
                $baja_inicio = $request->baja_inicio;
                $baja_fin = $request->baja_fin;

                $fechaBaja = "a.Baja BETWEEN '".$baja_inicio."' AND '".$baja_fin."'";

                $filtros[] = $FechaVencimiento;

                //$sanciones=gesanc_sanciones::whereRaw($consulta)->get();
              
            }

             if($request->vencimiento_inicio != null && $request->vencimiento_fin !=null){
                $vencimiento_inicio = $request->vencimiento_inicio;
                $vencimiento_fin = $request->vencimiento_fin;

                $FechaVencimiento = "a.vencimiento BETWEEN '".$vencimiento_inicio."' AND '".$vencimiento_fin."'";

                $filtros[] = $FechaVencimiento;

                //$sanciones=gesanc_sanciones::whereRaw($consulta)->get();
              
            }

            if($request->codContrato != null){
                $codContrato = $request->codContrato;
                $filtro_codContrato = "a.codContrato = '".$codContrato."'";
                $filtros[] = $filtro_codContrato;
            }

             if($request->codAnexo != null){
                $codAnexo = $request->codAnexo;
                $filtro_codAnexo = "a.CodAnexo = '".$codAnexo."'";
                $filtros[] = $filtro_codAnexo;
            }

             if($request->importe != null){
                $importe = $request->importe;
                $filtro_importe = "a.importeVenta = '".$importe."'";
                $filtros[] = $filtro_importe;
            } 

             if($request->comision != null){
                $comision = $request->comision;
                $filtro_comision = "b.porcentComisEspecifica = '".$comision."'";
                $filtros[] = $filtro_comision;
            } 

             if($request->descuento != null){
                $descuento = $request->descuento;
                $filtro_descuento = "b.descuento = '".$descuento."'";
                $filtros[] = $filtro_descuento;
            }

             if($request->otrocomercial != null){
                $otrocomercial = $request->otrocomercial;
                $filtro_otrocomercial = "b.esCteDeOtroComercial = '".$otrocomercial."'";
                $filtros[] = $filtro_otrocomercial;
            }

            if($request->esRenovacion != null){
                $esRenovacion = $request->esRenovacion;
                $filtro_esRenovacion = "a.esRenovacion = '".$esRenovacion."'";
                $filtros[] = $filtro_esRenovacion;
            }

            if($request->norenovar != null){
                $norenovar = $request->norenovar;
                $filtro_norenovar = "a.NOrenovar = '".$norenovar."'";
                $filtros[] = $filtro_esRenovacion;
            }

             if($request->sustituido != 0){
                $sustituido = $request->sustituido;
                $filtro_sustituido = "a.CodAnexo in (select CodSustituido from europa_anexos_sustituciones)";
                $filtros[] = $filtro_sustituido;
            }

              if($request->sustituyente != 0){
                $sustituyente = $request->sustituyente;
                $filtro_sustituyente = "a.CodAnexo in (select CodSustituyente from europa_anexos_sustituciones)";
                $filtros[] = $filtro_sustituyente;
            }

            if($request->otrocomercial != null){
                $otrocomercial = $request->otrocomercial;
                $filtro_otrocomercial = "b.esCteDeOtroComercial = '".$otrocomercial."'";
                $filtros[] = $filtro_otrocomercial;
            }
            

             if($request->contratante != null){
                $contratante = $request->contratante;
                $filtro_contratante = "b.codContratante = '".$contratante."'";
                $filtros[] = $filtro_contratante;
            } 

            if($request->empresa != null){
                $empresa = $request->empresa;
                $filtro_contratado = "b.codContratado = '".$empresa."'";
                $filtros[] = $filtro_contratado;
            } 

            if($request->matricula != null){
                $matricula = $request->matricula;
                $filtro_matricula = "a.Matricula ='".$matricula."'";
                $filtros[] = $filtro_matricula;
            } 

             if($request->remolque != null){
                $remolque = $request->remolque;
                $filtro_remolque = "a.CodAnexo in (select codAnexo from europa_anexos_remolques where matricula='".$remolque."')";
                $filtros[] = $filtro_remolque;
            } 

             if($request->comercial != null){
                $comercial = $request->comercial;
                $filtro_comercial = "codComercial = '".$comercial."'";
                $filtros[] = $filtro_comercial;
            } 

            if($request->tipo_anexo != null){
                $tipo_anexo = $request->tipo_anexo;
                $filtro_tipoanexo = "a.CodTipoAnexo in (select CodTipoAnexo from europa_tipo_anexo where tipoAnexo='".$tipo_anexo."')";
                $filtros[] = $filtro_tipoanexo;
            } 

            if($request->tipo_abono != null){
                $tipo_abono = $request->tipo_abono;
                $filtro_tipoabono = "a.codContrato in (select codContrato from europa_abonos where tipoAbono='".$tipo_abono."')";
                $filtros[] = $filtro_tipoabono;
            }

            for ($i =count($filtros) - 1; $i >= 0 ; $i--) { 
                $consulta .= $filtros[$i];
                $consulta .= " ";

                if($i > 0){
                    $consulta .= "AND ";
                }
            }

            $anexos = DB::table(DB::raw("europa_anexos a, europa_contratos b, europa_contactos c, europa_tipo_anexo d "))->select(DB::raw("a.CodAnexo,c.razonSocial as 'cliente' ,d.tipoAnexo as 'tipo',a.Matricula as 'matricula',b.fechaContrato as'fechadeContrato',a.Alta as 'fechaAlta',a.Baja as 'fechaBaja'"))->whereRaw("a.CodContrato=b.codContrato and b.codContratado=c.CodContacto and a.CodTipoAnexo=d.CodtipoAnexo")->orderby("fechaAlta","DESC")->limit(200)->get();


            
            if($consulta !=""){

              $anexos = DB::table(DB::raw("europa_anexos a, europa_contratos b, europa_contactos c, europa_tipo_anexo d "))->select(DB::raw("a.CodAnexo,c.razonSocial as 'cliente' ,d.tipoAnexo as 'tipo',a.Matricula as 'matricula',b.fechaContrato as'fechadeContrato',a.Alta as 'fechaAlta',a.Baja as 'fechaBaja'"))->whereRaw("a.CodContrato=b.codContrato and b.codContratado=c.CodContacto and a.CodTipoAnexo=d.CodtipoAnexo AND ".$consulta."")->orderby("fechaAlta","DESC")->limit(200)->get();

             }

              $objPHPExcel = new PHPExcel();
        // Se asignan las propiedades del libro
        $objPHPExcel->getProperties()->setCreator("ITS") // Nombre del autor
        ->setLastModifiedBy("ITS") //Ultimo usuario que lo modificó
        ->setTitle("anexos_filtrados") // Titulo
        ->setDescription("Sanciones filtradas"); //Descripción
        $titulosColumnas = array('Cliente'
                                ,'Tipo'
                                ,'Matricula'
                                ,'Fecha Contrato'
                                ,'Alta'
                                ,'Baja');
        
        // Se combinan las celdas, para colocar ahí el titulo del reporte
        //$objPHPExcel->setActiveSheetIndex(0)
        //->mergeCells('A1:D1');
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1', $titulosColumnas[0])
        ->setCellValue('B1', $titulosColumnas[1])
        ->setCellValue('C1', $titulosColumnas[2])
        ->setCellValue('D1', $titulosColumnas[3])
        ->setCellValue('E1', $titulosColumnas[4])
        ->setCellValue('F1', $titulosColumnas[5]);

        $i=2;
        foreach ($anexos as $key => $anexo) {
              $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.$i, $anexo->cliente)
            ->setCellValue('B'.$i, $anexo->tipo)
            ->setCellValue('C'.$i, $anexo->matricula)
            ->setCellValue('D'.$i, date('d - m - Y', strtotime($anexo->fechadeContrato)))
            ->setCellValue('E'.$i, date('d - m - Y', strtotime($anexo->fechaAlta)))
            ->setCellValue('F'.$i, date('d - m - Y', strtotime($anexo->fechaBaja)));
            $i++;
        }



    header('Content-Encoding: UTF-8');
    header('Content-type: text/csv; charset=UTF-8');
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="anexos_filtrados.xlsx"');
    header('Cache-Control: max-age=0');
    header("Pragma: no-cache");
    header("Expires: 0");
    header('Content-Transfer-Encoding: binary');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
    }


public function imprimir_recibos(Request $request){

    $recibos = DB::table("europa_recibos")->whereRaw("codContrato='".$request->codContrato."' and codEstado='1'")->get();

    if(count($recibos)==0){
        Response(1);
    }else{

       Response(0);
    }
}

public function impresion_recibos(){

    $recibos = DB::table("europa_recibos")->whereRaw("codContrato='".$_GET['codContrato']."' and codEstado='1'")->get();

    foreach ($recibos as $key => $recibo) {
        $cliente = DB::table("europa_contactos")->whereRaw("codContacto=(select codContratado from europa_contratos where codContrato='".$_GET['codContrato']."')")->first();
        $provincia = DB::table("europa_provincias")->whereRaw("codProvincia = '".$cliente->codProvincia."'")->first();;
        $contrato = DB::table("europa_contratos")->whereRaw("codContrato='".$_GET['codContrato']."'")->first();;
        $filename = '..\public\temp\recibo_'. $key . '.docx';
        $documentos [] = $filename;
        $templateWord = new TemplateProcessor('..\public\templates\recibos\Recibos.docx');
                    $numero_recibo = "";
                    $num = [];
                     if($recibo->origen==0){
                        $numero_recibo=$recibo->numRecibo;
                     }else{
                        $codRecibo = $recibo->codRecibo;
                        do {
                        $numRecibo = DB::table("europa_recibos")->select("numRecibo")->where("codRecibo","=",$codRecibo)->first();
                        
                        $origen = DB::table("europa_recibos")->where("codRecibo","=",$codRecibo)->first();
                        $codRecibo =$origen->origen;
                        $num[] = $numRecibo->numRecibo;
                        } while($origen->origen!=0);

                     $inverso = array_reverse($num);
                     $ultimo = end($inverso);
                     foreach ($inverso as $key => $val) {
                           $numero_recibo .=  $val.".";
                        }
                    $numero_recibo = rtrim($numero_recibo, '.');
                     }

                $templateWord->setValue('num',$numero_recibo);
                $templateWord->setValue('importe',$recibo->importe." €");
                $templateWord->setValue('fecha',date('d - m - Y'));
                $templateWord->setValue('vencimiento',date('d - m - Y', strtotime($recibo->vencimiento)));
                //$templateWord->setValue('importe_letras',ucfirst(convertir($recibo->importe)));
                $templateWord->setValue('empresa',$cliente->razonSocial);
                $templateWord->setValue('direccion',$cliente->direccion);
                $templateWord->setValue('CP',$cliente->codPostal);
                $templateWord->setValue('poblacion',$cliente->Poblacion);
                $templateWord->setValue('provincia',$provincia->Provincia);
                $templateWord->setValue('CIF',$cliente->cif);
                $templateWord->setValue('fecha_c',date('d - m - Y', strtotime($contrato->fechaContrato)));
                $templateWord->saveAs(storage_path($filename));
    }

    $dm = new DocxMerge();
    $dm->merge( $documentos, '..\public\temp\recibos.docx' );
 
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename=recibos.docx; charset=iso-8859-1');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize('..\public\temp\recibos.docx'));
    readfile('..\public\temp\recibos.docx');
  

    $files = glob('..\public\temp\*'); // get all file names
    foreach($files as $file){ // iterate files
      if(is_file($file))
        unlink($file); // delete file
        }
  exit;

}

public function mostrar_recibo_suelto(){

            
            $output = '<label>Importe</label><br><input type="text" class="form-control" name="importe_suelto" id="importe_suelto" value="">';


            $output.='<br><label>Fecha vencimiento</label><br><input type="text" class="form-control datepicker" name="vencimiento_suelto" id="vencimiento_suelto" value="">';

        
            return Response($output);
}

public function generar_recibo_suelto(){

        $cliente = DB::table("europa_contactos")->whereRaw("codContacto=(select codContratado from europa_contratos where codContrato='".$_GET['codContrato']."')")->first();
        $provincia = DB::table("europa_provincias")->whereRaw("codProvincia = '".$cliente->codProvincia."'")->first();
        $contrato = DB::table("europa_contratos")->whereRaw("codContrato='".$_GET['codContrato']."'")->first();
        $filename = '..\public\temp\recibo_suelto.docx';
        $templateWord = new TemplateProcessor('..\public\templates\recibos\recibo_suelto.docx');
                    
                //$templateWord->setValue('num',$numero_recibo);
                $templateWord->setValue('importe',$_GET['importe']." €");
                $templateWord->setValue('fecha',date('d - m - Y'));
                $templateWord->setValue('vencimiento',$_GET['vencimiento']);
                //$templateWord->setValue('importe_letras',ucfirst(convertir($recibo->importe)));
                $templateWord->setValue('empresa',$cliente->razonSocial);
                $templateWord->setValue('direccion',$cliente->direccion);
                $templateWord->setValue('CP',$cliente->codPostal);
                $templateWord->setValue('poblacion',$cliente->Poblacion);
                $templateWord->setValue('provincia',$provincia->Provincia);
                $templateWord->setValue('CIF',$cliente->cif);
                $templateWord->setValue('fecha_c',date('d - m - Y', strtotime($contrato->fechaContrato)));
                $templateWord->saveAs(storage_path($filename));

                header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename=recibo_suelto.docx; charset=iso-8859-1');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize('..\public\temp\recibo_suelto.docx'));
    readfile('..\public\temp\recibo_suelto.docx');
  

    $files = glob('..\public\temp\*'); // get all file names
    foreach($files as $file){ // iterate files
      if(is_file($file))
        unlink($file); // delete file
        }
  exit;
}

public function mostrar_imprimir_contrato(){


     $output ="<label>Ejemplar para:</label><br>";
     $output.="<input type='radio' name='ejemplar'id='ejemplar' value='interesado' checked>Interesado  ";
     $output.="<input type='radio' name='ejemplar'id='ejemplar' value='its'>ITS<br>";

     /*$output .="<label>Condiciones:</label><br>";
     $output.="<input type='radio' name='condiciones'id='condiciones' value='normales' checked>Normales  ";
     $output.="<input type='radio' name='condiciones'id='condiciones' value='especiales'>Especiales  ";
     $output.="<input type='radio' name='condiciones'id='condiciones' value='otra'>100%N 50%I<br>";*/

     $output .="<label>Borrar:</label><br>";
     $output.="<input type='radio' name='borrar'id='borrar' value='todo'>Todo  ";
     $output.="<input type='radio' name='borrar'id='borrar' value='importes'>Importes<br>";

     $output .="<label>Contrato en blanco:</label><br>";
     $output.="<input type='radio' name='en_blanco'id='en_blanco' value='con'>Con cliente  ";
     $output.="<input type='radio' name='en_blanco'id='en_blanco' value='sin'>Sin cliente<br>";
     

     return Response($output);
}

public function imprimir_contrato(){

        $cliente = DB::table("europa_contactos")->whereRaw("codContacto=(select codContratado from europa_contratos where codContrato='".$_GET['codContrato']."')")->first();
        $provincia = DB::table("europa_provincias")->whereRaw("codProvincia = '".$cliente->codProvincia."'")->first();
        $contrato = DB::table("europa_contratos")->whereRaw("codContrato='".$_GET['codContrato']."'")->first();
        $telefono = DB::table("europa_contactos_telefonos")->whereRaw("CodContacto='".$cliente->CodContacto."' and codTipoTelefono='1'")->first();
        $fax = DB::table("europa_contactos_telefonos")->whereRaw("CodContacto='".$cliente->CodContacto."' and codTipoTelefono='3'")->first();
        $anexos = DB::table(DB::raw("europa_anexos a, europa_tipo_anexo b"))->select(DB::raw("a.*,b.tipoAnexo"))->whereRaw(" a.CodTipoAnexo=b.CodtipoAnexo AND a.CodContrato='".$_GET['codContrato']."'")->get();

        $ejemplar = "";
        if($_GET["ejemplar"]=='interesado'){   
            $ejemplar = "El interesado";
        }else{
            $ejemplar = "ITS";
        }

        if($_GET["en_blanco"]!=null && $_GET["en_blanco"]!='undefined' ){
            if($_GET["en_blanco"]=="con"){

                    $filename = '..\public\temp\contrato_cliente.docx';
                    $templateWord = new TemplateProcessor('..\public\templates\europa_contratos\contrato.docx');
                                
                            $templateWord->setValue('razonsocial',$cliente->razonSocial);
                            $templateWord->setValue('direccion',$cliente->direccion);
                            $templateWord->setValue('poblacion',$cliente->Poblacion);
                            $templateWord->setValue('cp',$cliente->codPostal);
                            $templateWord->setValue('provincia',$provincia->Provincia);
                            $templateWord->setValue('cif',$cliente->cif);

                            if($telefono!=null)
                            $templateWord->setValue('telefono',$telefono->CodTelefono);
                            else
                            $templateWord->setValue('telefono','');
    
                            $templateWord->setValue('email',$cliente->email);

                            if($fax!=null)
                            $templateWord->setValue('fax',$fax->CodTelefono);
                            else
                            $templateWord->setValue('fax','');

                    
                            $templateWord->setValue('matricula','');
                            $templateWord->setValue('remolque1','');
                            $templateWord->setValue('remolque2','');
                            $templateWord->setValue('remolque3','');
                            $templateWord->setValue('tipo_anexo','');
                            $templateWord->setValue('importe','');
                            $templateWord->setValue('fecha_contrato',date('d  M  Y', strtotime($contrato->fechaContrato)));
                            $templateWord->setValue('ejemplar',$ejemplar);
                            $templateWord->saveAs(storage_path($filename));

                        header('Content-Description: File Transfer');
                        header('Content-Type: application/octet-stream');
                        header('Content-Disposition: attachment; filename=contrato_cliente.docx; charset=iso-8859-1');
                        header('Expires: 0');
                        header('Cache-Control: must-revalidate');
                        header('Pragma: public');
                        header('Content-Length: ' . filesize('..\public\temp\contrato_cliente.docx'));
                        readfile('..\public\temp\contrato_cliente.docx');
                      

                        $files = glob('..\public\temp\*'); // get all file names
                        foreach($files as $file){ // iterate files
                          if(is_file($file))
                            unlink($file); // delete file
                            }
                      exit;
            }else{


        $filename = '..\public\temp\contrato_cliente.docx';
        $templateWord = new TemplateProcessor('..\public\templates\europa_contratos\contrato.docx');
                    
                $templateWord->setValue('razonsocial','');
                $templateWord->setValue('direccion','');
                $templateWord->setValue('poblacion','');
                $templateWord->setValue('cp','');
                $templateWord->setValue('provincia','');
                $templateWord->setValue('cif','');
                $templateWord->setValue('telefono','');
                $templateWord->setValue('email','');
                $templateWord->setValue('fax','');
                $templateWord->setValue('matricula','');
                $templateWord->setValue('remolque1','');
                $templateWord->setValue('remolque2','');
                $templateWord->setValue('remolque3','');
                $templateWord->setValue('anexo','');
                $templateWord->setValue('importe','');
                $templateWord->setValue('fecha_contrato','');
                $templateWord->setValue('ejemplar',$ejemplar);
                $templateWord->saveAs(storage_path($filename));

                header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename=contrato_cliente.docx; charset=iso-8859-1');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize('..\public\temp\contrato_cliente.docx'));
    readfile('..\public\temp\contrato_cliente.docx');
  

    $files = glob('..\public\temp\*'); // get all file names
    foreach($files as $file){ // iterate files
      if(is_file($file))
        unlink($file); // delete file
        }
  exit;
            }

        }

                    $filename = '..\public\temp\contrato_cliente.docx';
                    $templateWord = new TemplateProcessor('..\public\templates\europa_contratos\contrato.docx');
                                
                            $templateWord->setValue('razonsocial',$cliente->razonSocial);
                            $templateWord->setValue('direccion',$cliente->direccion);
                            $templateWord->setValue('poblacion',$cliente->Poblacion);
                            $templateWord->setValue('cp',$cliente->codPostal);
                            $templateWord->setValue('provincia',$provincia->Provincia);
                            $templateWord->setValue('cif',$cliente->cif);

                            if($telefono!=null)
                            $templateWord->setValue('telefono',$telefono->CodTelefono);
                            else
                            $templateWord->setValue('telefono','');
    
                            $templateWord->setValue('email',$cliente->email);

                            if($fax!=null)
                            $templateWord->setValue('fax',$fax->CodTelefono);
                            else
                            $templateWord->setValue('fax','');

                            $lista_matriculas = "";
                            $remolques1 = "";
                            $remolques2 = "";
                            $remolques3 = "";
                            $lista_anexos = "";
                            $lista_importes = "";
                            $tabla = '<w:tbl>
<w:tblPr>
<w:tblStyle w:val="TableGrid"/>
<w:tblW w:w="5000" w:type="pct"/>
</w:tblPr>
<w:tblGrid>
<w:gridCol w:w="2880"/>
<w:gridCol w:w="2880"/>
<w:gridCol w:w="2880"/>
</w:tblGrid>
<w:tr>
<w:tc>
<w:tcPr>
<w:tcW w:w="2880" w:type="dxa"/>
</w:tcPr>
<w:p>
<w:r>
<w:t>AAA</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="2880" w:type="dxa"/>
</w:tcPr>
<w:p>
<w:r>
<w:t>BBB</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="2880" w:type="dxa"/>
</w:tcPr>
<w:p>
<w:r>
<w:t>CCC</w:t>
</w:r>
</w:p>
</w:tc>
</w:tr>
<w:tr>
<w:tc>
<w:tcPr>
<w:tcW w:w="2880" w:type="dxa"/>
</w:tcPr>
<w:p>
<w:r>
<w:t>AAA</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="2880" w:type="dxa"/>
</w:tcPr>
<w:p>
<w:r>
<w:t>BBB</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="2880" w:type="dxa"/>
</w:tcPr>
<w:p>
<w:r>
<w:t>CCC</w:t>
</w:r>
</w:p>
</w:tc>
</w:tr>
</w:tbl>';

                            foreach ($anexos as $key => $anexo) {
                                //$lista_matriculas .='<w:p>'.$anexo->Matricula."</w:p>";

                            }

                            if($_GET["borrar"]!=null && $_GET["borrar"]!='undefined' ){
                                if($_GET["borrar"]=="todo"){

                                    $templateWord->setValue('matricula','');
                                    $templateWord->setValue('remolque1','');
                                    $templateWord->setValue('remolque2','');
                                    $templateWord->setValue('remolque3','');
                                    $templateWord->setValue('tipo_anexo','');
                                    $templateWord->setValue('importe','');
                                }else{
                                    $templateWord->setValue('matricula','');
                                    $templateWord->setValue('remolque1','');
                                    $templateWord->setValue('remolque2','');
                                    $templateWord->setValue('remolque3','');
                                    $templateWord->setValue('tipo_anexo','');
                                    $templateWord->setValue('importe','');

                             }

                         }else{

                            $templateWord->setValue('table',$tabla);
                            $templateWord->setValue('remolque1','');
                            $templateWord->setValue('remolque2','');
                            $templateWord->setValue('remolque3','');
                            $templateWord->setValue('tipo_anexo','');
                            $templateWord->setValue('importe','');


                            $templateWord->setValue('fecha_contrato',date('d  M  Y', strtotime($contrato->fechaContrato)));
                            $templateWord->setValue('ejemplar',$ejemplar);
                            $templateWord->saveAs(storage_path($filename));

                                    header('Content-Description: File Transfer');
                        header('Content-Type: application/octet-stream');
                        header('Content-Disposition: attachment; filename=contrato_cliente.docx; charset=iso-8859-1');
                        header('Expires: 0');
                        header('Cache-Control: must-revalidate');
                        header('Pragma: public');
                        header('Content-Length: ' . filesize('..\public\temp\contrato_cliente.docx'));
                        readfile('..\public\temp\contrato_cliente.docx');
                      

                        $files = glob('..\public\temp\*'); // get all file names
                        foreach($files as $file){ // iterate files
                          if(is_file($file))
                            unlink($file); // delete file
                            }
                      exit;
                             }
                        
        }

}



