<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\gesanc_sanciones;

use App\gesanc_lista_motivos_denuncia;

use App\gesanc_reduccion;

use App\gesanc_contencioso;

use App\gesanc_clientes;

use DateTime;

use Illuminate\Database\QueryException;

use App\gesanc_contratos;

use PhpOffice\PhpWord\TemplateProcessor;

use PHPExcel; 
use PHPExcel_IOFactory;

class FacturasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_europa()
    {
        
        $facturas = DB::table(DB::raw("europa_facturacion a, europa_contactos b"))->selectRaw("a.idfactura,b.razonSocial as 'cliente', a.NumLiq as 'numero',CASE a.TipoLiq
        WHEN 'F' THEN 'Factura'
        ELSE 'Abono'
        END as 'tipo',
        CASE a.ClaseLiq
        WHEN 'F' THEN 'Provisión de fondo'
        WHEN 'C' THEN 'Corriente'
        WHEN 'T' THEN 'De contrato'
        END as 'clase',
        a.impFacturaCto as 'importe',
        a.Notas")->whereRaw("a.codContacto=b.codContacto")->orderby("FEcha Emis","DESC")->limit(200)->get();

        $empresas = DB::table("europa_contactos")->select("razonSocial","CodContacto","cif")->get();

        $contratantes = DB::table("europa_contactos")->select("razonSocial","CodContacto")->whereRaw("CodContacto in (select codContratante from europa_contratos)")->get();

        $matriculas = DB::table("europa_matriculas")->select("Matricula")->whereRaw("Matricula in (select Matricula from europa_anexos)")->get();

        return view('facturas/europa/facturas',['facturas'=> $facturas,'empresas'=>$empresas,'contratantes'=>$contratantes,'matriculas'=>$matriculas]);
    }

    public function index_its()
    {
        
     
        $facturas = DB::table(DB::raw("its_facturacion a, its_contactos b"))->selectRaw("a.idfactura,b.Empresa as 'cliente', a.NumLiq as 'numero',CASE a.TipoLiq
        WHEN 'F' THEN 'Factura'
        ELSE 'Abono'
        END as 'tipo',
        CASE a.ClaseLiq
        WHEN 'H' THEN 'Desinmov H'
        WHEN 'G' THEN 'Elec cheque G'
        WHEN 'A' THEN 'Elec cheque A'
        WHEN 'C' THEN 'Corriente'
        WHEN 'T' THEN 'Contrato'
        WHEN 'E' THEN 'asis mec E'
        WHEN 'F' THEN 'asis mec F'
        WHEN 'D' THEN 'asis mec D'
        WHEN 'B' THEN 'asis mec B'
        END as 'clase',
        a.Precio as 'importe',
        a.Notas")->whereRaw("a.Cte_Fac_Abonr=b.CIF")->orderby("FEcha Emis","DESC")->limit(200)->get();
  

        $empresas = DB::table('its_contactos')->whereRaw('CIF IN (select Contratado from its_contratos)')->orderBy('Empresa')->get();

        $contratantes = DB::table('its_contactos')->whereRaw('CIF in (select Contratante from its_contratos)')->distinct()->orderBy('Empresa')->get();

        $matriculas = DB::table('its_anexos')->select("Matricula")->whereRaw('Beneficiario in (select Contratado from its_contratos)')->distinct()->orderBy('Matricula')->get();


        return view('facturas/its/facturas',['facturas'=> $facturas,'empresas'=>$empresas,'contratantes'=>$contratantes,'matriculas'=>$matriculas]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_europa(Request $request)
    {
        //
        //TODO
        if($request->ajax()){

            $datos = array();
            parse_str($request->datos,$datos);

            $contratante = null;
            $tipo = null;
            $clase = null;
            $origen = null;
            $empresa = null;
            $num_factura = null;
            $fechaemision = null;
            $cc = null;
            $notas = null;
            $provisionfondos = 0;
            $iva = null;
            $importe = null;

             if(array_key_exists("provisionfondos",$datos)){
               $provisionfondos = 1;
            }

            }

             if($datos["contratante"]!=null){
                $contratante = $datos["contratante"];
            }
             if($datos["tipo"]!=null){
                $tipo = $datos["tipo"];
            }
             if($datos["clase"]!=null){
                $clase = $datos["clase"];
            }
             if($datos["origen"]!=null){
                $origen = $datos["origen"];
            }
             if($datos["empresa"]!=null){
                $empresa = $datos["empresa"];
            }
          

            if($datos["cc"]!=null){
                $cc = $datos["cc"];
            }

            if($datos["notas"]!=null){
                $notas = $datos["notas"];
            }

            if($datos["iva"]!=null){
                $iva = $datos["iva"];
            }

    
            if($datos["fechaemision"] !=null){
                $fechaemision = DateTime::createFromFormat('d/m/Y', $datos["fechaemision"]);
                date_time_set($fechaemision, 00, 00);
            }

            if($contratante == 10){
              
              $max_num_actual = DB::table("europa_facturacion")->selectRaw("CONVERT(substr(NumLiq, 1, (length(NumLiq)- 
              length(SUBSTRING_INDEX((NumLiq), '/', -1))-1)),UNSIGNED INTEGER) as 'numero',RIGHT(NumLiq,2) as 'fecha'")->whereRaw("Entidad ='10' and RIGHT(NumLiq,2) = '".date('y')."'")->orderBy("numero","DESC")->first();

              if($max_num_actual != null){
                $nuevo_numero = $max_num_actual->numero + 1;
              }else{
                $nuevo_numero = 1;
              }

              $num_factura = $nuevo_numero.'/'.date('y');

            }else{

              $max_num_actual = DB::table("europa_facturacion")->selectRaw("CONVERT(substr(NumLiq, 1, (length(NumLiq)- 
              length(SUBSTRING_INDEX((NumLiq), '/', -1))-1)),UNSIGNED INTEGER) as 'numero',RIGHT(NumLiq,2)")->whereRaw("Entidad ='9' and RIGHT(NumLiq,4) = '".date('Y')."'")->orderBy("numero","DESC")->first();

              if($max_num_actual != null){
                $nuevo_numero = $max_num_actual->numero + 1;
              }else{
                $nuevo_numero = 1;
              }

              $num_factura = $nuevo_numero.'/'.date('Y');

            }

             $clave = $contratante.$tipo.$clase.$num_factura;


            try { 

                //Seleccionar las condiciones del contrato según los parametros establecidos
            
             $factura =  DB::table("europa_facturacion")->insertGetId(['ClaveLiq'=>$clave,
              'CuentaIngBanc'=>$cc, 'Notas'=>$notas, 'FEcha Emis'=>$fechaemision,'IVA'=>$iva,'conProvisionFondos'=>$provisionfondos,'Entidad'=>$contratante,'TipoLiq'=>$tipo,'ClaseLiq'=>$clase,'NumLiq'=>$num_factura,'codContacto'=>$empresa,'FacturaOrigen'=>$origen]);

            return  Response($factura);

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }
    }

    public function store_its(Request $request)
    {
        //
        //TODO
        if($request->ajax()){

            $datos = array();
            parse_str($request->datos,$datos);

            $contratante = null;
            $tipo = null;
            $precio = null;
            $precioT = null;
            $precioR = null;
            $CriterioSelec = null;
            $clase = null;
            $empresa = null;
            $num_factura = null;
            $fechaemision = null;
            $fechavencimiento = null;
            $fechainicio = null;
            $fechafin = null;
            $cc = null;
            $notas = null;
            $iva = null;



            }

             if($datos["contratante"]!=null){
                $contratante = $datos["contratante"];
            }
             if($datos["tipo"]!=null){
                $tipo = $datos["tipo"];
            }
             if($datos["clase"]!=null){
                $clase = $datos["clase"];
            }
             if($datos["empresa"]!=null){
                $empresa = $datos["empresa"];
            }

            if(array_key_exists("precio",$datos)){
               $precio = $datos["precio"];
            }
            if(array_key_exists("precioT",$datos)){
               $precioT = $datos["precioT"];
            }
            if(array_key_exists("precioR",$datos)){
               $precioR = $datos["precioR"];
            }

            if(array_key_exists("CriterioSelec",$datos)){
               $CriterioSelec = $datos["CriterioSelec"];
            }
          
            if($datos["cc"]!=null){
                $cc = $datos["cc"];
            }

            if($datos["notas"]!=null){
                $notas = $datos["notas"];
            }

            if($datos["iva"]!=null){
                $iva = $datos["iva"];
            }

    
            if($datos["fechainicial"] !=null){
                $fechainicio = DateTime::createFromFormat('d/m/Y', $datos["fechainicial"]);
                date_time_set($fechainicio, 00, 00);
            }
             if($datos["fechafinal"] !=null){
                $fechafin = DateTime::createFromFormat('d/m/Y', $datos["fechafinal"]);
                date_time_set($fechafin, 00, 00);
            }

              if($datos["fechaemision"] !=null){
                $fechaemision = DateTime::createFromFormat('d/m/Y', $datos["fechaemision"]);
                date_time_set($fechaemision, 00, 00);
            }
            if($datos["vencimiento"] !=null){
                $fechavencimiento = DateTime::createFromFormat('d/m/Y', $datos["vencimiento"]);
                date_time_set($fechavencimiento, 00, 00);
            }

            if($contratante == 10){


            if($tipo=='F'){
                     $max_num_actual = DB::table("its_facturacion")->selectRaw("CONVERT(substr(NumLiq, 1, (length(NumLiq)- 
              length(SUBSTRING_INDEX((NumLiq), '/', -1))-1)),UNSIGNED INTEGER) as 'numero',RIGHT(NumLiq,2) as 'fecha'")->whereRaw("Entidad ='10' and TipoLiq='F' and RIGHT(NumLiq,2) = '".date('y')."'")->orderBy("numero","DESC")->first();
                 }else{
                      $max_num_actual = DB::table("its_facturacion")->selectRaw("CONVERT(substr(NumLiq, 1, (length(NumLiq)- 
              length(SUBSTRING_INDEX((NumLiq), '/', -1))-1)),UNSIGNED INTEGER) as 'numero',RIGHT(NumLiq,2) as 'fecha'")->whereRaw("Entidad ='10' and TipoLiq='A' and RIGHT(NumLiq,2) = '".date('y')."'")->orderBy("numero","DESC")->first();
                 }
              
         

              if($max_num_actual != null){
                $nuevo_numero = $max_num_actual->numero + 1;
              }else{
                $nuevo_numero = 1;
              }

              $num_factura = $nuevo_numero.'/'.date('y');

            }
             elseif($contratante == 5){
              
              if($tipo=='F'){
                     $max_num_actual = DB::table("its_facturacion")->selectRaw("CONVERT(substr(NumLiq, 1, (length(NumLiq)- 
              length(SUBSTRING_INDEX((NumLiq), '/', -1))-1)),UNSIGNED INTEGER) as 'numero',RIGHT(NumLiq,2) as 'fecha'")->whereRaw("Entidad ='5' and TipoLiq='F' and RIGHT(NumLiq,2) = '".date('y')."'")->orderBy("numero","DESC")->first();
                 }else{
                      $max_num_actual = DB::table("its_facturacion")->selectRaw("CONVERT(substr(NumLiq, 1, (length(NumLiq)- 
              length(SUBSTRING_INDEX((NumLiq), '/', -1))-1)),UNSIGNED INTEGER) as 'numero',RIGHT(NumLiq,2) as 'fecha'")->whereRaw("Entidad ='5' and TipoLiq='A' and RIGHT(NumLiq,2) = '".date('y')."'")->orderBy("numero","DESC")->first();
                 }

              if($max_num_actual != null){
                $nuevo_numero = $max_num_actual->numero + 1;
              }else{
                $nuevo_numero = 1;
              }

              $num_factura = $nuevo_numero.'/'.date('y');

            }
            else{

                 if($tipo=='F'){
                     $max_num_actual = DB::table("its_facturacion")->selectRaw("CONVERT(substr(NumLiq, 1, (length(NumLiq)- 
              length(SUBSTRING_INDEX((NumLiq), '/', -1))-1)),UNSIGNED INTEGER) as 'numero',RIGHT(NumLiq,2)")->whereRaw("Entidad ='9' and TipoLiq='F' and RIGHT(NumLiq,4) = '".date('Y')."'")->orderBy("numero","DESC")->first();
                 }else{
                       $max_num_actual = DB::table("its_facturacion")->selectRaw("CONVERT(substr(NumLiq, 1, (length(NumLiq)- 
              length(SUBSTRING_INDEX((NumLiq), '/', -1))-1)),UNSIGNED INTEGER) as 'numero',RIGHT(NumLiq,2)")->whereRaw("Entidad ='9' and TipoLiq='A' and RIGHT(NumLiq,4) = '".date('Y')."'")->orderBy("numero","DESC")->first();
                 }

              

              if($max_num_actual != null){
                $nuevo_numero = $max_num_actual->numero + 1;
              }else{
                $nuevo_numero = 1;
              }

              $num_factura = $nuevo_numero.'/'.date('Y');

            }

             $clave = $contratante.$tipo.$clase.$num_factura.'/'.date('Y');


            try { 

                //Seleccionar las condiciones del contrato según los parametros establecidos
         
             $factura =  DB::table("its_facturacion")->insertGetId(['ClaveLiq'=>$clave, 'Entidad'=>$contratante, 'NumLiq'=>$num_factura, 'TipoLiq'=>$tipo, 'CriterioSelec'=>$CriterioSelec, 'ClaseLiq'=>$clase, 'Cte_Fac_Abonr'=>$empresa, 'FEcha Emis'=>$fechaemision, 'PeriodoIni'=>$fechainicio, 'PeriodoFin'=>$fechafin, 'Precio'=>$precio, 'PrecioT'=>$precioT, 'PrecioR'=>$precioR, 'IVA'=>$iva, 'Notas'=>$notas, 'CuentaIngBanc'=>$cc, 'VenctoRecBanc'=>$fechavencimiento]);
           
             if($tipo=='F'){

                if($clase =='D' || $clase=='E' || $clase=='F'){

                if($CriterioSelec == 1){
                  //DB::enableQueryLog();
                    $anexos = DB::table(DB::raw("its_anexos a,its_contactos b,its_matriculas c,its_contratos d"))->selectRaw(" a.Beneficiario,b.Empresa,CASE when (a.Vinculacion is null and c.TipoVehiculo='C') 
                    then a.Matricula when (a.Vinculacion is not null and c.TipoVehiculo='R') then (select Matricula from its_anexos where IdAnexo=a.Vinculacion) else null END 
                    as 'Matricula',
                    CASE when (c.TipoVehiculo='R') 
                    then a.Matricula else null END as 'Remolque',a.FechaIntroDat,d.`Fecha Contrato` as 'FechaContrato',
                    CASE when (a.Vinculacion is null and c.TipoVehiculo='C') then (select PrecioT from its_cobertura where COBERTURA = a.Cobertura)
                    when (c.TipoVehiculo='R' and a.Vinculacion is null) then (select PrecioR from its_cobertura where COBERTURA = a.Cobertura)
                    when (a.Vinculacion is not null and c.TipoVehiculo='R') then (select Precio from its_cobertura where COBERTURA = a.Cobertura) end as 'Precio'")->whereRaw("a.IdContrato=d.IDContrato and a.Beneficiario=b.CIF and a.Matricula=c.Matricula and d.Contratante ='".$empresa."' and a.Cobertura='".$clase."' and d.`Fecha Contrato` between '".date_format($fechainicio, 'Y-m-d')."' and '".date_format($fechafin, 'Y-m-d')."' and a.SUSTITUYE is null")->get();

                }else{
                   
                    $anexos = DB::table(DB::raw("its_anexos a,its_contactos b,its_matriculas c,its_contratos d"))->selectRaw(" a.Beneficiario,b.Empresa,CASE when (a.Vinculacion is null and c.TipoVehiculo='C') 
                    then a.Matricula when (a.Vinculacion is not null and c.TipoVehiculo='R') then (select Matricula from its_anexos where IdAnexo=a.Vinculacion) else null END 
                    as 'Matricula',
                    CASE when (c.TipoVehiculo='R') 
                    then a.Matricula else null END as 'Remolque',a.FechaIntroDat,d.`Fecha Contrato` as 'FechaContrato',
                    CASE when (a.Vinculacion is null and c.TipoVehiculo='C') then (select PrecioT from its_cobertura where COBERTURA = a.Cobertura)
                    when (c.TipoVehiculo='R' and a.Vinculacion is null) then (select PrecioR from its_cobertura where COBERTURA = a.Cobertura)
                    when (a.Vinculacion is not null and c.TipoVehiculo='R') then (select Precio from its_cobertura where COBERTURA = a.Cobertura) end as 'Precio'")->whereRaw("a.IdContrato=d.IDContrato and a.Beneficiario=b.CIF and a.Matricula=c.Matricula and d.Contratante ='".$empresa."' and a.Cobertura='".$clase."' and a.FechaIntroDat between '".date_format($fechainicio, 'Y-m-d')."' and '".date_format($fechafin, 'Y-m-d')."' and a.SUSTITUYE is null")->get();
                    
                }
                 
                    foreach ($anexos as $key => $anexo) {
                    
                    DB::table("its_facturacion detalle")->insert(['ClaveLiq'=>$clave, 'MATRICULA'=>$anexo->Matricula,'REMOLQUE'=>$anexo->Remolque, 'CIF'=>$anexo->Beneficiario,'fechaContrato'=>$anexo->FechaContrato,'PRECIO'=>$anexo->Precio, 'Estado'=>0]);
                
                 }

             }
             if($clase =='H' || $clase=='G'){

                $anexos = DB::table(DB::raw("its_anexos a,its_contactos b,its_matriculas c,its_contratos d"))->selectRaw("a.Beneficiario,b.Empresa,a.Matricula,(select PRECIO from its_cobertura where COBERTURA = a.Cobertura) as 'Precio'")->whereRaw("a.IdContrato=d.IDContrato and a.Beneficiario=b.CIF and a.Matricula=c.Matricula and d.Contratante ='".$empresa."' and a.Cobertura='".$clase."' and a.FechaIntroDat between '".date_format($fechainicio, 'Y-m-d')."' and '".date_format($fechafin, 'Y-m-d')."' and a.SUSTITUYE is null")->get();

                 foreach ($anexos as $key => $anexo) {
                    
                    DB::table("its_facturacion detalle")->insert(['ClaveLiq'=>$clave, 'MATRICULA'=>$anexo->Matricula, 'CIF'=>$anexo->Beneficiario,'PRECIO'=>$anexo->Precio, 'Estado'=>0]);
                
                 }
             }

             }
            

            return  Response($factura);

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_europa($id)
    {
        $factura = DB::table(DB::raw("europa_facturacion a, europa_contactos b"))->selectRaw("*,a.idfactura,b.razonSocial as 'cliente', a.NumLiq as 'numero',CASE a.TipoLiq
        WHEN 'F' THEN 'Factura'
        ELSE 'Abono'
        END as 'tipo',
        CASE a.ClaseLiq
        WHEN 'F' THEN 'Provisión de fondo'
        WHEN 'C' THEN 'Corriente'
        WHEN 'T' THEN 'De contrato'
        END as 'clase',
        a.impFacturaCto as 'importe',
        `FEcha Emis` as 'fecha',
        a.Notas,b.cif")->whereRaw("a.codContacto=b.codContacto and a.idfactura='".$id."'")->orderby("FEcha Emis","DESC")->limit(200)->first();


        $contratante = DB::table("europa_contactos")->select("razonSocial","CodContacto")->whereRaw("CodContacto ='".$factura->Entidad."'")->first();

        $detalles = DB::table("europa_facturacion detalle")->where("ClaveLiq","=",$factura->ClaveLiq)->get();

        $importes_factura = DB::table("europa_facturacion detalle")->selectRaw("SUM(PRECIO*UNIDADES) as 'cantidad'")->where("ClaveLiq","=",$factura->ClaveLiq)->first();

        //Si es una factura de contrato
        if($importes_factura->cantidad == 0){

            if($factura->conProvisionFondos == 1){
            $baseimponible = $factura->impFacturaCto * (100 - 30) / (100 + $factura->IVA);     
            $iva = ($baseimponible * $factura->IVA) / 100;
            $provision = $factura->impFacturaCto - $baseimponible - $iva;
            $total = $factura->impFacturaCto;
        }else{
            $baseimponible = $factura->impFacturaCto / ( 1 + ( $factura->IVA / 100 ) );
            $iva = $factura->impFacturaCto - $baseimponible;
            $provision = null;
            $total = $factura->impFacturaCto;
        }
        //Si no es una factura de contrato
        }else{
            $baseimponible = $importes_factura->cantidad;
            $iva =($baseimponible * $factura->IVA) /100;
            $provision = $factura->provisionFondos;
            $total =$baseimponible + $iva + $provision;
        }


      
        

        return view('facturas/europa/detail',['factura'=> $factura,'contratante'=>$contratante,'detalles'=>$detalles,'baseimponible'=>round($baseimponible,2),'iva'=>round($iva,2),'provision'=>round($provision,2),'total'=>round($total,2)]);
    }

    public function show_its($id)
    {


       $factura = DB::table(DB::raw("its_facturacion a, its_contactos b"))->selectRaw("a.idfactura,b.Empresa as 'cliente', a.NumLiq as 'numero',CASE a.TipoLiq
        WHEN 'F' THEN 'Factura'
        ELSE 'Abono'
        END as 'tipo',
        CASE a.ClaseLiq
        WHEN 'H' THEN 'Desinmov H'
        WHEN 'G' THEN 'Elec cheque G'
        WHEN 'A' THEN 'Elec cheque A'
        WHEN 'C' THEN 'Corriente'
        WHEN 'T' THEN 'Contrato'
        WHEN 'E' THEN 'asis mec E'
        WHEN 'F' THEN 'asis mec F'
        WHEN 'D' THEN 'asis mec D'
        WHEN 'B' THEN 'asis mec B'
        END as 'clase',
        a.Precio as 'importe',a.*,
        a.Notas,a.`Fecha Emis` as 'fecha'")->whereRaw("a.Cte_Fac_Abonr=b.CIF and idfactura='".$id."'")->first();

        $empresa = DB::table('its_contactos')->whereRaw('CIF = (select Cte_Fac_Abonr from its_facturacion where idfactura="'.$id.'")')->first();

        $contratante = DB::table("gesanc_empresas")->select("Num_empresa","Empresa")->whereRaw("Num_empresa ='".$factura->Entidad."'")->first();

        $precios =DB::table("its_cobertura")->where("COBERTURA","=",$factura->ClaseLiq)->first();

        $cuentas = DB::table("its_cuentas bancarias")->get();


        if($factura->TipoLiq =='F'){

          if($factura->ClaseLiq=='C' || $factura->ClaseLiq=='A'){
            $detalles = DB::table("its_facturacion detalle")->where("ClaveLiq","=",$factura->ClaveLiq)->get();
            $importes_factura = DB::table("its_facturacion detalle")->selectRaw("SUM(PRECIO*UNIDADES) as 'cantidad'")->where("ClaveLiq","=",$factura->ClaveLiq)->first();

          }
          if($factura->ClaseLiq=='H' || $factura->ClaseLiq=='G'){
            $detalles = DB::table(DB::raw("`its_facturacion detalle` a, its_contactos b"))->selectRaw("a.CIF,b.Empresa,a.MATRICULA,a.PRECIO,a.iddetalle ")->whereRaw("a.CIF=b.CIF and ClaveLiq='".$factura->ClaveLiq."'")->get();
             $importes_factura = DB::table("its_facturacion detalle")->selectRaw("SUM(PRECIO) as 'cantidad'")->where("ClaveLiq","=",$factura->ClaveLiq)->first();
          }
          if($factura->ClaseLiq=='F' || $factura->ClaseLiq=='D' || $factura->ClaseLiq=='E'){
            $detalles = DB::table(DB::raw("`its_facturacion detalle` a, its_contactos b"))->selectRaw("a.CIF,b.Empresa,a.MATRICULA,a.REMOLQUE,a.fechaContrato,a.PRECIO,a.iddetalle")->whereRaw("a.CIF=b.CIF and ClaveLiq='".$factura->ClaveLiq."'")->get();
             $importes_factura = DB::table("its_facturacion detalle")->selectRaw("SUM(PRECIO) as 'cantidad'")->where("ClaveLiq","=",$factura->ClaveLiq)->first();
          }
        }else{
          $detalles = DB::table("its_facturacion detalle")->where("ClaveLiq","=",$factura->ClaveLiq)->get();
          $importes_factura = DB::table("its_facturacion detalle")->selectRaw("SUM(PRECIO*UNIDADES) as 'cantidad'")->where("ClaveLiq","=",$factura->ClaveLiq)->first();
        }
        
          if($importes_factura != null){
            $baseimponible = $importes_factura->cantidad;
            $iva =($baseimponible * $factura->IVA) /100;
            $provision = $factura->provisionFondos;
            $total =$baseimponible + $iva + $provision;
          }else{
            $baseimponible = 0;
            $iva = 0;
            $provision = 0;
            $total = 0;
          }
            
          
            return view('facturas/its/detail',['empresa'=>$empresa,'factura'=> $factura,'contratante'=>$contratante,'detalles'=>$detalles,'baseimponible'=>round($baseimponible,2),'iva'=>round($iva,2),'total'=>round($total,2),'precios'=>$precios,'cuentas'=>$cuentas,'provision'=>round($provision,2)]);

        
    }

    public function refrescar_detalles(Request $request){

      if($request->ajax()){

        $clave = $request->clave;
        $eliminar = $request->eliminar;

        if($eliminar == 1){
          DB::table("its_facturacion detalle")->where("ClaveLiq","=",$clave)->delete();
        }

        $factura = DB::table("its_facturacion")->where("ClaveLiq","=",$clave)->first();
        $tipo = $factura->TipoLiq;
        $clase = $factura->ClaseLiq;
        $CriterioSelec = $factura->CriterioSelec;
        $fechainicio = $factura->PeriodoIni;
        $fechafin = $factura->PeriodoFin;
        $Cte_Fac_Abonr = $factura->Cte_Fac_Abonr;

          try { 

               if($tipo=='F'){

                if($clase =='D' || $clase=='E' || $clase=='F'){

                if($CriterioSelec == 1){
                  //DB::enableQueryLog();
                    $anexos = DB::table(DB::raw("its_anexos a,its_contactos b,its_matriculas c,its_contratos d"))->selectRaw(" a.Beneficiario,b.Empresa,CASE when (a.Vinculacion is null and c.TipoVehiculo='C') 
                    then a.Matricula when (a.Vinculacion is not null and c.TipoVehiculo='R') then (select Matricula from its_anexos where IdAnexo=a.Vinculacion) else null END 
                    as 'Matricula',
                    CASE when (c.TipoVehiculo='R') 
                    then a.Matricula else null END as 'Remolque',a.FechaIntroDat,d.`Fecha Contrato` as 'FechaContrato',
                    CASE when (a.Vinculacion is null and c.TipoVehiculo='C') then (select PrecioT from its_cobertura where COBERTURA = a.Cobertura)
                    when (c.TipoVehiculo='R' and a.Vinculacion is null) then (select PrecioR from its_cobertura where COBERTURA = a.Cobertura)
                    when (a.Vinculacion is not null and c.TipoVehiculo='R') then (select Precio from its_cobertura where COBERTURA = a.Cobertura) end as 'Precio'")->whereRaw("a.IdContrato=d.IDContrato and a.Beneficiario=b.CIF and a.Matricula=c.Matricula and d.Contratante ='".$Cte_Fac_Abonr."' and a.Cobertura='".$clase."' and d.`Fecha Contrato` between '".$fechainicio."' and '".$fechafin."' and a.SUSTITUYE is null")->get();
                 //sdd(DB::getQueryLog());
                }else{
                   
                    $anexos = DB::table(DB::raw("its_anexos a,its_contactos b,its_matriculas c,its_contratos d"))->selectRaw(" a.Beneficiario,b.Empresa,CASE when (a.Vinculacion is null and c.TipoVehiculo='C') 
                    then a.Matricula when (a.Vinculacion is not null and c.TipoVehiculo='R') then (select Matricula from its_anexos where IdAnexo=a.Vinculacion) else null END 
                    as 'Matricula',
                    CASE when (c.TipoVehiculo='R') 
                    then a.Matricula else null END as 'Remolque',a.FechaIntroDat,d.`Fecha Contrato` as 'FechaContrato',
                    CASE when (a.Vinculacion is null and c.TipoVehiculo='C') then (select PrecioT from its_cobertura where COBERTURA = a.Cobertura)
                    when (c.TipoVehiculo='R' and a.Vinculacion is null) then (select PrecioR from its_cobertura where COBERTURA = a.Cobertura)
                    when (a.Vinculacion is not null and c.TipoVehiculo='R') then (select Precio from its_cobertura where COBERTURA = a.Cobertura) end as 'Precio'")->whereRaw("a.IdContrato=d.IDContrato and a.Beneficiario=b.CIF and a.Matricula=c.Matricula and d.Contratante ='".$Cte_Fac_Abonr."' and a.Cobertura='".$clase."' and a.FechaIntroDat between '".$fechainicio."' and '".$fechafin."' and a.SUSTITUYE is null")->get();
                    
                }
                 
                    foreach ($anexos as $key => $anexo) {
                    
                    DB::table("its_facturacion detalle")->insert(['ClaveLiq'=>$clave, 'MATRICULA'=>$anexo->Matricula,'REMOLQUE'=>$anexo->Remolque, 'CIF'=>$anexo->Beneficiario,'fechaContrato'=>$anexo->FechaContrato,'PRECIO'=>$anexo->Precio, 'Estado'=>0]);
                
                 }

             }
             if($clase =='H' || $clase=='G'){

                $anexos = DB::table(DB::raw("its_anexos a,its_contactos b,its_matriculas c,its_contratos d"))->selectRaw("a.Beneficiario,b.Empresa,a.Matricula,(select PRECIO from its_cobertura where COBERTURA = a.Cobertura) as 'Precio'")->whereRaw("a.IdContrato=d.IDContrato and a.Beneficiario=b.CIF and a.Matricula=c.Matricula and d.Contratante ='".$Cte_Fac_Abonr."' and a.Cobertura='".$clase."' and a.FechaIntroDat between '".$fechainicio."' and '".$fechafin."' and a.SUSTITUYE is null")->get();

                 foreach ($anexos as $key => $anexo) {
                    
                    DB::table("its_facturacion detalle")->insert(['ClaveLiq'=>$clave, 'MATRICULA'=>$anexo->Matricula, 'CIF'=>$anexo->Beneficiario,'PRECIO'=>$anexo->Precio, 'Estado'=>0]);
                
                 }
             }

             }

            return  Response("Los detalles se han actualizado correctamente.");

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }

          }
    }

     public function editar_europa(Request $request)
    {   
        //TODO
        if($request->ajax()){

            $datos = array();
            parse_str($request->datos,$datos);

            $clave = $request->clave;
            $fechaemision = null;
            $cc = null;
            $notas = null;
            $provisionfondos = 0;
            $iva = null;
            $importe = null;

            $total = null;


             if(array_key_exists("provisionfondos",$datos)){
               $provisionfondos = 1;
            }

            }

          

            if($datos["cc"]!=null){
                $cc = $datos["cc"];
            }

            if($datos["notas"]!=null){
                $notas = $datos["notas"];
            }

            if($datos["iva"]!=null){
                $iva = $datos["iva"];
            }

             if(array_key_exists("importe",$datos)){
                $importe = $datos["importe"];
            }

            if($request->total!=null){
                $total = $request->total;
            }

    
            if($datos["fechaemision"] !=null){
                $fechaemision = DateTime::createFromFormat('d/m/Y', $datos["fechaemision"]);
                date_time_set($fechaemision, 00, 00);
            }


            try { 

                //Seleccionar las condiciones del contrato según los parametros establecidos
              DB::table("europa_facturacion")->where("ClaveLiq",'=',$clave)->update([
              'CuentaIngBanc'=>$cc, 'Notas'=>$notas, 'FEcha Emis'=>$fechaemision,'impFacturaCto'=>$total,'IVA'=>$iva,'conProvisionFondos'=>$provisionfondos]);

            return  Response("La factura se ha actualizado correctamente");

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }
    }

    public function editar_its(Request $request)
    {   
        //TODO
        if($request->ajax()){

            $datos = array();
            parse_str($request->datos,$datos);
            $clave = $request->clave;
            $precio = null;
            $precioT = null;
            $precioR = null;
            $CriterioSelec = null;
            $fechaemision = null;
            $fechavencimiento = null;
            $fechainicio = null;
            $fechafin = null;
            $cc = null;
            $notas = null;
            $iva = null;



            }


            if(array_key_exists("CriterioSelec",$datos)){
               $CriterioSelec = $datos["CriterioSelec"];
            }
          
            if($datos["cc"]!=null){
                $cc = $datos["cc"];
            }

            if($datos["notas"]!=null){
                $notas = $datos["notas"];
            }

            if($datos["iva"]!=null){
                $iva = $datos["iva"];
            }

    
            if($datos["fechainicial"] !=null){
                $fechainicio = DateTime::createFromFormat('d/m/Y', $datos["fechainicial"]);
                date_time_set($fechainicio, 00, 00);
            }
             if($datos["fechafinal"] !=null){
                $fechafin = DateTime::createFromFormat('d/m/Y', $datos["fechafinal"]);
                date_time_set($fechafin, 00, 00);
            }

              if($datos["fechaemision"] !=null){
                $fechaemision = DateTime::createFromFormat('d/m/Y', $datos["fechaemision"]);
                date_time_set($fechaemision, 00, 00);
            }
            if($datos["vencimiento"] !=null){
                $fechavencimiento = DateTime::createFromFormat('d/m/Y', $datos["vencimiento"]);
                date_time_set($fechavencimiento, 00, 00);
            }


            try { 

                //Seleccionar las condiciones del contrato según los parametros establecidos
            
             $factura =  DB::table("its_facturacion")->where("ClaveLiq","=",$clave)->update(['CriterioSelec'=>$CriterioSelec, 'FEcha Emis'=>$fechaemision, 'PeriodoIni'=>$fechainicio, 'PeriodoFin'=>$fechafin,'IVA'=>$iva,'Notas'=>$notas, 'CuentaIngBanc'=>$cc, 'VenctoRecBanc'=>$fechavencimiento]);

            return  Response("La factura se ha actualizado correctamente");

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }
    }

    public function nuevo_europa(){

          $facturas = DB::table(DB::raw("europa_facturacion a, europa_contactos b"))->selectRaw("a.idfactura,b.razonSocial as 'cliente', a.NumLiq as 'numero',CASE a.TipoLiq
        WHEN 'F' THEN 'Factura'
        ELSE 'Abono'
        END as 'tipo',
        CASE a.ClaseLiq
        WHEN 'F' THEN 'Provisión de fondo'
        WHEN 'C' THEN 'Corriente'
        WHEN 'T' THEN 'De contrato'
        END as 'clase',
        a.impFacturaCto as 'importe',ClaveLiq as 'clave',
        a.Notas")->whereRaw("a.codContacto=b.codContacto")->orderby("FEcha Emis","DESC")->limit(200)->get();

        $empresas = DB::table("europa_contactos")->select("razonSocial","CodContacto","cif")->get();

        $contratantes = DB::table("europa_contactos")->select("razonSocial","CodContacto")->whereRaw("CodContacto in (select codContratante from europa_contratos)")->get();

        $matriculas = DB::table("europa_matriculas")->select("Matricula")->whereRaw("Matricula in (select Matricula from europa_anexos)")->get();

        return view('facturas/europa/nuevo',['facturas'=> $facturas,'empresas'=>$empresas,'contratantes'=>$contratantes,'matriculas'=>$matriculas]);
    }

    public function nuevo_its($clase){


        
        if($clase =='H' || $clase=='A' || $clase=='G'){
            $empresas = DB::table("gesanc_empresas")->select("CIF","Empresa")->get();
            $contratantes = DB::table("gesanc_empresas")->select("Num_empresa","Empresa")->whereRaw("Num_empresa ='5' AND Num_empresa ='9' ")->get();
        }
        elseif($clase =='D' || $clase=='E' || $clase=='F'){
        $empresas = DB::table("gesanc_empresas")->select("CIF","Empresa")->get();
        $contratantes = DB::table("gesanc_empresas")->select("Num_empresa","Empresa")->whereRaw("Num_empresa ='5'")->get();
        }else{
           $empresas = DB::table("its_contactos")->select("Empresa","CIF")->get();
           $contratantes = DB::table("gesanc_empresas")->select("Num_empresa","Empresa")->whereRaw("Num_empresa ='5' AND Num_empresa ='9'  AND Num_empresa ='10' ")->get();
        }

        $matriculas = DB::table("europa_matriculas")->select("Matricula")->whereRaw("Matricula in (select Matricula from europa_anexos)")->get();

        $cuentas = DB::table("its_cuentas bancarias")->get();

        $precios =DB::table("its_cobertura")->where("COBERTURA","=",$clase)->first();

        return view('facturas/its/nuevo',['empresas'=>$empresas,'contratantes'=>$contratantes,'matriculas'=>$matriculas,'cuentas'=>$cuentas,'clase'=>$clase,'precios'=>$precios]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy_europa(Request $request)
    {
        //
         if($request->ajax()){

            $clave = $request->clave;

              try { 

                //Seleccionar las condiciones del contrato según los parametros establecidos
            
              DB::table("europa_facturacion")->where("ClaveLiq",'=',$clave)->delete();

            return  Response("La factura se ha eliminado correctamente");

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }

         }
    }

    public function eliminar_detalle_europa(Request $request)
    {
        //
         if($request->ajax()){

            $iddetalle = $request->iddetalle;
            $precio = $request->precio;
            $tipo = $request->tipo;

              try { 

                //Seleccionar las condiciones del contrato según los parametros establecidos
               $factura = DB::table("europa_facturacion")->whereRaw("ClaveLiq = (select ClaveLiq from `europa_facturacion detalle` where iddetalle='".$iddetalle."')")->first();

                if($tipo == "Abono"){
                  DB::table("europa_facturacion")->whereRaw("ClaveLiq = (select ClaveLiq from `europa_facturacion detalle` where iddetalle='".$iddetalle."')")->update(['impFacturaCto'=>$factura->impFacturaCto + $precio]);
                }else{
                     DB::table("europa_facturacion")->whereRaw("ClaveLiq = (select ClaveLiq from `europa_facturacion detalle` where iddetalle='".$iddetalle."')")->update(['impFacturaCto'=>$factura->impFacturaCto - $precio]);
                }
            
              DB::table("europa_facturacion detalle")->where("iddetalle",'=',$iddetalle)->delete();


            return  Response("El detalle se ha eliminado correctamente");

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }

         }
    }

    public function eliminar_detalle_its(Request $request)
    {
        //
         if($request->ajax()){

            $iddetalle = $request->iddetalle;
            $precio = $request->precio;
            $tipo = $request->tipo;

              try { 

                //Seleccionar las condiciones del contrato según los parametros establecidos
               $factura = DB::table("its_facturacion")->whereRaw("ClaveLiq = (select ClaveLiq from `its_facturacion detalle` where iddetalle='".$iddetalle."')")->first();

   
                DB::table("its_facturacion")->whereRaw("ClaveLiq = (select ClaveLiq from `its_facturacion detalle` where iddetalle='".$iddetalle."')")->update(['Precio'=>$factura->Precio - $precio]);
                
            
              DB::table("its_facturacion detalle")->where("iddetalle",'=',$iddetalle)->delete();


            return  Response("El detalle se ha eliminado correctamente");

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }

         }
    }

     public function editar_detalle_europa(Request $request)
    {
        //
         if($request->ajax()){

            $clave = $request->clave;
            $iddetalle = $request->iddetalle;
            $unidades = $request->unidades;
            $precio = $request->precio;
            $concepto = $request->concepto;

              try { 

                //Seleccionar las condiciones del contrato según los parametros establecidos

            
              DB::table("europa_facturacion detalle")->where("iddetalle",'=',$iddetalle)->update([
                'UNIDADES'=>$unidades,'PRECIO'=>$precio,'CONCEPTO'=>$concepto]);


            return  Response("El detalle se ha modificado correctamente");

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }

         }
    }

    public function editar_detalle_its(Request $request)
    {
        //
         if($request->ajax()){

            $clave = $request->clave;
            $iddetalle = null;
            $unidades =  null;
            $precio =  null;
            $concepto =  null;
            $fecha = null;
            $iva = $request->iva;
            $matricula =  null;
            $remolque =  null;

            if($iva ==1){
              $factura = DB::table("its_facturacion")->where("ClaveLiq","=",$clave)->first();
              $iva_calculado = ($precio*$factura->IVA) / 100;
              $precio = $precio - $iva_calculado; 
            }

            if($request->clave!=null){
                $clave = $request->clave;
            }
            if($request->iddetalle!=null){
                $iddetalle = $request->iddetalle;
            }
            if($request->unidades!=null){
                $unidades = $request->unidades;
            }
            if($request->precio!=null){
                $precio = $request->precio;
            }
            if($request->concepto!=null){
                $concepto = $request->concepto;
            }
            if($request->matricula!=null){
                $matricula = $request->matricula;
            }
            if($request->remolque!=null){
                $remolque = $request->remolque;
            }

    
              if($request->fecha !=null){
                $fecha = DateTime::createFromFormat('d/m/Y', $request->fecha);
                date_time_set($fecha, 00, 00);
            }

              try { 

                //Seleccionar las condiciones del contrato según los parametros establecidos

            
              DB::table("its_facturacion detalle")->where("iddetalle",'=',$iddetalle)->update([
                'MATRICULA'=>$matricula, 'REMOLQUE'=>$remolque, 'FechaContrato'=>$fecha, 'CONCEPTO'=>$concepto, 'UNIDADES'=>$unidades, 'PRECIO'=>$precio]);


            return  Response("El detalle se ha modificado correctamente");

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }

         }
    }

    public function insertar_detalle_europa(Request $request)
    {
        //
         if($request->ajax()){

            $clave = $request->clave;
            $codigo = null;
            $iddetalle = $request->iddetalle;
            $unidades = $request->unidades;
            $precio = $request->precio;
            $concepto = $request->concepto;

            if($request->codigo != ""){
                $codigo = $request->codigo;
            }

              try { 

                //Seleccionar las condiciones del contrato según los parametros establecidos

              $factura = DB::table("europa_facturacion")->where("ClaveLiq","=",$clave)->first();

              if($factura->TipoLiq == "A" && $precio < 0){
                $precio = $precio * -1;
              }

            
              DB::table("europa_facturacion detalle")->insert(['CODIGO'=>$codigo,'ClaveLiq'=>$clave,
                'UNIDADES'=>$unidades,'PRECIO'=>$precio,'CONCEPTO'=>$concepto]);


            return  Response("El detalle se ha añadido correctamente");

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }

         }
    }

     public function insertar_detalle_its(Request $request)
    {
        //
         //
         if($request->ajax()){

            $clave = $request->clave;
            $iddetalle = null;
            $unidades =  null;
            $precio =  null;
            $concepto =  null;
            $fecha = null;
            $iva = $request->iva;
            $matricula =  null;
            $remolque =  null;

            if($iva ==1){
              $factura = DB::table("its_facturacion")->where("ClaveLiq","=",$clave)->first();
              $iva_calculado = ($precio*$factura->IVA) / 100;
              $precio = $precio - $iva_calculado; 
            }

            if($request->clave!=null){
                $clave = $request->clave;
            }
            if($request->iddetalle!=null){
                $iddetalle = $request->iddetalle;
            }
            if($request->unidades!=null){
                $unidades = $request->unidades;
            }
            if($request->precio!=null){
                $precio = $request->precio;
            }
            if($request->concepto!=null){
                $concepto = $request->concepto;
            }
            if($request->matricula!=null){
                $matricula = $request->matricula;
            }
            if($request->remolque!=null){
                $remolque = $request->remolque;
            }

    
              if($request->fecha !=null){
                $fecha = DateTime::createFromFormat('d/m/Y', $request->fecha);
                date_time_set($fecha, 00, 00);
            }

              try { 

                //Seleccionar las condiciones del contrato según los parametros establecidos

            DB::enableQueryLog();
              DB::table("its_facturacion detalle")->insertGetId([
                'MATRICULA'=>$matricula, 'REMOLQUE'=>$remolque, 'FechaContrato'=>$fecha, 'CONCEPTO'=>$concepto, 'UNIDADES'=>$unidades, 'PRECIO'=>$precio,'ClaveLiq'=>$clave]);
              dd(DB::getQueryLog());

            return  Response("El detalle se ha modificado correctamente");

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }

         }
    }



    public function filtra_europa(Request $request){
            
        $output="<p style='text-align:center;'>No se han encontrado resultados.</p>";

            if($request->ajax()){

            $filtros = array();

            $consulta = "";

              if($request->alta_inicio != null && $request->alta_fin !=null){
                $alta_inicio = $request->alta_inicio;
                $alta_fin = $request->alta_fin;

                $FechaContrato = "`FEcha Emis` BETWEEN '".$alta_inicio."' AND '".$alta_fin."'";

                $filtros[] = $FechaContrato;
              
            }


            if($request->numfactura != null){
                $numfactura = $request->numfactura;
                $filtro_numfactura = "NumLiq = '".$numfactura."'";
                $filtros[] = $filtro_numfactura;
            }

             if($request->codContrato != null){
                $codContrato = $request->codContrato;
                $filtro_codContrato = "codContrato = '".$codContrato."'";
                $filtros[] = $filtro_codContrato;
            } 

             if($request->clavefactura != null){
                $clavefactura = $request->clavefactura;
                $filtro_clavefactura = "ClaveLiq = '".$clavefactura."'";
                $filtros[] = $filtro_clavefactura;
            }

             if($request->fac != null){
                $fac = $request->fac;
                $filtro_fac = "TipoLiq = '".$fac."'";
                $filtros[] = $filtro_fac;
            }

              if($request->empresa != null){
                $empresa = $request->empresa;
                $filtro_empresa = "Cte_Fac_Abonr = '".$empresa."'";
                $filtros[] = $filtro_empresa;
            }
            if($request->cif != null){
                $cif = $request->cif;
                $filtro_cif = "Cte_Fac_Abonr = '".$cif."'";
                $filtros[] = $filtro_cif;
            }          

             if($request->contratante != null){
                $contratante = $request->contratante;
                $filtro_contratante = "Entidad = '".$contratante."'";
                $filtros[] = $filtro_contratante;
            } 

            if($request->tipo != null){
                $tipo = $request->tipo;
                $filtro_tipo = "ClaseLiq = '".$tipo."'";
                $filtros[] = $filtro_tipo;
            } 

            if($request->matricula != null){
                $matricula = $request->matricula;
                $filtro_matricula = "ClaveLiq in (select ClaveLiq from `europa_facturacion detalle` where MATRICULA='".$matricula."')";
                $filtros[] = $filtro_matricula;
            } 

             if($request->precio != null){
                $precio = $request->precio;
                $filtro_precio = "ClaveLiq in (select ClaveLiq from `europa_facturacion detalle` where PRECIO='".$precio."')";
                $filtros[] = $filtro_precio;
            } 

              if($request->coddetalle != null){
                $coddetalle = $request->coddetalle;
                $filtro_coddetalle = "ClaveLiq in (select ClaveLiq from `europa_facturacion detalle` where CODIGO='".$coddetalle."')";
                $filtros[] = $filtro_coddetalle;
            } 

              if($request->unidades != null){
                $unidades = $request->unidades;
                $filtro_unidades = "ClaveLiq in (select ClaveLiq from `europa_facturacion detalle` where UNIDADES='".$unidades."')";
                $filtros[] = $filtro_unidades;
            }

            if($request->provisionfondos != null){
                $filtro_provision = "conProvisionFondos = 1";
                $filtros[] = $filtro_provision;
            } 

            for ($i =count($filtros) - 1; $i >= 0 ; $i--) { 
                $consulta .= $filtros[$i];
                $consulta .= " ";

                if($i > 0){
                    $consulta .= "AND ";
                }
            }

            $facturas = DB::table(DB::raw("europa_facturacion a, europa_contactos b"))->selectRaw("a.idfactura,b.razonSocial as 'cliente', a.NumLiq as 'numero',CASE a.TipoLiq
        WHEN 'F' THEN 'Factura'
        ELSE 'Abono'
        END as 'tipo',
        CASE a.ClaseLiq
        WHEN 'F' THEN 'Provisión de fondo'
        WHEN 'C' THEN 'Corriente'
        WHEN 'T' THEN 'De contrato'
        END as 'clase',
        a.impFacturaCto as 'importe',
        a.Notas")->whereRaw("a.Cte_Fac_Abonr=b.cif")->orderby("FEcha Emis","DESC")->limit(200)->get();



            
            if($consulta !=""){

              $facturas = DB::table(DB::raw("europa_facturacion a, europa_contactos b"))->selectRaw("a.idfactura,b.razonSocial as 'cliente', a.NumLiq as 'numero',CASE a.TipoLiq
        WHEN 'F' THEN 'Factura'
        ELSE 'Abono'
        END as 'tipo',
        CASE a.ClaseLiq
        WHEN 'F' THEN 'Provisión de fondo'
        WHEN 'C' THEN 'Corriente'
        WHEN 'T' THEN 'De contrato'
        END as 'clase',
        a.impFacturaCto as 'importe',
        a.Notas")->whereRaw("a.Cte_Fac_Abonr=b.cif AND ".$consulta)->orderby("FEcha Emis","DESC")->limit(200)->get();

             }
            
           if($facturas){
                foreach ($facturas as $key => $factura) {

                     $output.='<tr>'.
                            '<td>'.$factura->cliente.'</td>'.
                            '<td>'.$factura->numero.'</td>'.
                            '<td>'.$factura->tipo.'</td>'.
                            '<td>'.$factura->clase.'</td>'.
                            '<td>'.$factura->importe.' €</td>'.                            
                            '<td>'.$factura->Notas.'</td>'.
                            '<td><a class="btn btn-sm btn-success" href="/facturas/europa/'.$factura->idfactura.'"><i class="fa fa-edit"></i></a></td>'.
                            '</tr>';

                }

                
               
            }

            return Response($output);
             
    }
}

public function filtra_its(Request $request){
            
        $output="<p style='text-align:center;'>No se han encontrado resultados.</p>";

            if($request->ajax()){

            $filtros = array();

            $consulta = "";

              if($request->alta_inicio != null && $request->alta_fin !=null){
                $alta_inicio = $request->alta_inicio;
                $alta_fin = $request->alta_fin;

                $FechaContrato = "`FEcha Emis` BETWEEN '".$alta_inicio."' AND '".$alta_fin."'";

                $filtros[] = $FechaContrato;
              
            }


            if($request->numfactura != null){
                $numfactura = $request->numfactura;
                $filtro_numfactura = "NumLiq = '".$numfactura."'";
                $filtros[] = $filtro_numfactura;
            }

             if($request->codContrato != null){
                $codContrato = $request->codContrato;
                $filtro_codContrato = "codContrato = '".$codContrato."'";
                $filtros[] = $filtro_codContrato;
            } 

             if($request->clavefactura != null){
                $clavefactura = $request->clavefactura;
                $filtro_clavefactura = "ClaveLiq = '".$clavefactura."'";
                $filtros[] = $filtro_clavefactura;
            }

             if($request->fac != null){
                $fac = $request->fac;
                $filtro_fac = "TipoLiq = '".$fac."'";
                $filtros[] = $filtro_fac;
            }

              if($request->empresa != null){
                $empresa = $request->empresa;
                $filtro_empresa = "Cte_Fac_Abonr = '".$empresa."'";
                $filtros[] = $filtro_empresa;
            }

            if($request->cif != null){
                $cif = $request->cif;
                $filtro_cif = "Cte_Fac_Abonr = '".$cif."'";
                $filtros[] = $filtro_cif;
            }         


             if($request->contratante != null){
                $contratante = $request->contratante;
                $filtro_contratante = "Entidad = '".$contratante."'";
                $filtros[] = $filtro_contratante;
            } 

            if($request->tipo != null){
                $tipo = $request->tipo;
                $filtro_tipo = "ClaseLiq = '".$tipo."'";
                $filtros[] = $filtro_tipo;
            } 

            if($request->matricula != null){
                $matricula = $request->matricula;
                $filtro_matricula = "ClaveLiq in (select ClaveLiq from `its_facturacion detalle` where MATRICULA='".$matricula."')";
                $filtros[] = $filtro_matricula;
            } 

             if($request->precio != null){
                $precio = $request->precio;
                $filtro_precio = "ClaveLiq in (select ClaveLiq from `its_facturacion detalle` where PRECIO='".$precio."')";
                $filtros[] = $filtro_precio;
            } 

              if($request->coddetalle != null){
                $coddetalle = $request->coddetalle;
                $filtro_coddetalle = "ClaveLiq in (select ClaveLiq from `its_facturacion detalle` where CODIGO='".$coddetalle."')";
                $filtros[] = $filtro_coddetalle;
            } 

              if($request->unidades != null){
                $unidades = $request->unidades;
                $filtro_unidades = "ClaveLiq in (select ClaveLiq from `its_facturacion detalle` where UNIDADES='".$unidades."')";
                $filtros[] = $filtro_unidades;
            } 

            for ($i =count($filtros) - 1; $i >= 0 ; $i--) { 
                $consulta .= $filtros[$i];
                $consulta .= " ";

                if($i > 0){
                    $consulta .= "AND ";
                }
            }

            $facturas = DB::table(DB::raw("its_facturacion a, its_contactos b"))->selectRaw("a.idfactura,b.Empresa as 'cliente', a.NumLiq as 'numero',CASE a.TipoLiq
        WHEN 'F' THEN 'Factura'
        ELSE 'Abono'
        END as 'tipo',
        CASE a.ClaseLiq
        WHEN 'H' THEN 'Desinmov H'
        WHEN 'G' THEN 'Elec cheque G'
        WHEN 'A' THEN 'Elec cheque A'
        WHEN 'C' THEN 'Corriente'
        WHEN 'T' THEN 'Contrato'
        WHEN 'E' THEN 'asis mec E'
        WHEN 'F' THEN 'asis mec F'
        WHEN 'D' THEN 'asis mec D'
        WHEN 'B' THEN 'asis mec B'
        END as 'clase',
        a.Precio as 'importe',
        a.Notas")->whereRaw("a.Cte_Fac_Abonr=b.CIF")->orderby("FEcha Emis","DESC")->limit(200)->get();



            
            if($consulta !=""){

              $facturas = DB::table(DB::raw("its_facturacion a, its_contactos b"))->selectRaw("a.idfactura,b.Empresa as 'cliente', a.NumLiq as 'numero',CASE a.TipoLiq
        WHEN 'F' THEN 'Factura'
        ELSE 'Abono'
        END as 'tipo',
        CASE a.ClaseLiq
        WHEN 'H' THEN 'Desinmov H'
        WHEN 'G' THEN 'Elec cheque G'
        WHEN 'A' THEN 'Elec cheque A'
        WHEN 'C' THEN 'Corriente'
        WHEN 'T' THEN 'Contrato'
        WHEN 'E' THEN 'asis mec E'
        WHEN 'F' THEN 'asis mec F'
        WHEN 'D' THEN 'asis mec D'
        WHEN 'B' THEN 'asis mec B'
        END as 'clase',
        a.Precio as 'importe',
        a.Notas")->whereRaw("a.Cte_Fac_Abonr=b.CIF AND ".$consulta)->orderby("FEcha Emis","DESC")->limit(200)->get();

             }
            
           if($facturas){
                foreach ($facturas as $key => $factura) {

                     $output.='<tr>'.
                            '<td>'.$factura->cliente.'</td>'.
                            '<td>'.$factura->numero.'</td>'.
                            '<td>'.$factura->tipo.'</td>'.
                            '<td>'.$factura->clase.'</td>'.                            
                            '<td>'.$factura->Notas.'</td>'.
                            '<td><a class="btn btn-sm btn-success" href="/facturas/its/'.$factura->idfactura.'"><i class="fa fa-edit"></i></a></td>'.
                            '</tr>';

                }

                
               
            }

            return Response($output);
             
    }
}

  public function mostrar_decontrato_europa(Request $request){

      if($request->ajax()){


            $contratos = DB::table(DB::raw("europa_contratos a, europa_contactos b"))->selectRaw("a.*,b.razonSocial")->whereRaw("a.codContratado=b.CodContacto")->get();
            $clientes = DB::table("europa_contactos")->whereRaw("CodContacto in (select CodContratado from europa_contratos)")->get();
          
               $output = "<br><label>Contrato</label>";

              $output.='<br><select class="selectpicker" data-live-search="true" title="Buscar..." id="contrato_origen" name="contrato_origen">';

                foreach ($clientes as $key => $cliente) {
                    $output.= '<optgroup label="'.$cliente->razonSocial.'">';
                    foreach ($contratos as $key => $contrato) {
                     if($contrato->codContratado == $cliente->CodContacto){
                       $output.='<option value="'.$contrato->codContrato.'">'.$contrato->codContrato.' - '.date('d/m/Y', strtotime($contrato->fechaContrato)).'</option>';
                     }
                    }
                    $output.='</optgroup>';
                }
            
              $output .= "</select><br>";

              $output .= "<label>Tipo</label>";
            $output.='<ul class="list-inline">
                           <li><input type="radio" name="tipofac" value="factura" placeholder="Factura" checked> <span>Factura</span></li>
                          <li><input type="radio" name="tipofac" value="abono" placeholder="Abono"> <span>Abono</span></li>
                           </ul>';

              $output .="<br><div id='div_origen'></div><br>";

              $output.='<br><label>Fecha de emisión</label><input type="text" class="form-control datepicker" name="fechaemision" id="fechaemision" value="">';

              $output.='<br><label>IVA %</label><input type="number" class="form-control" name="ivaporcentaje" id="ivaporcentaje" value="21">';


              $output.='<br><label>Notas</label><textarea class="form-control" name="notas_factura" id="notas_factura"></textarea>';

            return Response($output);

    }

  }

   public function mostrar_deprovision_europa(Request $request){

      if($request->ajax()){

            $year = date("Y");
            $year2 = $year -1;
            $year3 = $year -2;
            $year4 = $year -3;
            $year5 = $year -4;

            $facturas = DB::table(DB::raw("europa_facturacion a, europa_contactos b"))->selectRaw("a.*,b.razonSocial")->whereRaw("a.codContacto=b.CodContacto and a.conProvisionFondos=1")->orderBy("FEcha Emis","DESC")->get();
             $clientes = DB::table("europa_contactos")->orderBy("razonSocial","ASC")->get();

             $output = '
    <div class="row"><div class="col-md-12"> <div class="form-group col-md-2">
                            <label for="empresa">Año</label>
                             <select class="selectpicker" data-live-search="true" title="Buscar..." id="fechaactual" name="fechaactual">
                             <option selected value='.$year.'>'.$year.'</option>
                             <option value='.$year2.'>'.$year2.'</option>
                             <option value='.$year3.'>'.$year3.'</option>
                             <option value='.$year4.'>'.$year4.'</option>
                             <option value='.$year5.'>'.$year5.'</option>
                             </select>
                        </div>';

             $output .= '<div class="form-group col-md-8"><label for="empresa">Empresa</label><select class="selectpicker form-control" data-live-search="true" title="Buscar..." id="cliente_provision" name="cliente_provision">';
               foreach ($clientes as $key => $cliente) {
                $output.= '<option value="'.$cliente->CodContacto.'">'.$cliente->razonSocial.'</option>';
                }
            
              $output .= "</select>";
            
              $output.='</div></div>';



            $output .= '<div class="col-md-12"><div class="form-group col-md-12" id="tablafacturas"><label>Pendientes de liquidar</label';
            $output .= '<div class="table-responsive" id="tabla_contenido">
                    <table class="table"><thead class="table-header">
                        <th></th>
                        <th>Num</th>
                        <th>Fecha Emis.</th>
                        <th>SUBTOTAL</th>
                        <th>IVA</th>
                        <th>PROV_FONDO</th>
                        <th>APLIC. FONDO</th>
                        <th>HONORARIO</th>
                        <th>TOTAL_FR</th>
                        <th></th>
                        </thead>
                        <tbody id="pteliquidar">
                        </tbody>
                        </table></div>';

            $output .= '<div class="col-md-12"><div class="form-group col-md-12" id="tablafacturas"><label>Liquidadas</label';
            $output .= '<div class="table-responsive" id="tabla_contenido">
                    <table class="table"><thead class="table-header" style="background: #449D44;">
                        <th>Num</th>
                        <th>Fecha Emis.</th>
                        <th>SUBTOTAL</th>
                        <th>IVA</th>
                        <th>PROV_FONDO</th>
                        <th>APLIC. FONDO</th>
                        <th>HONORARIO</th>
                        <th>TOTAL_FR</th>
                        </thead>
                        <tbody id="liquidadas">
                        </tbody>
                        </table></div></div></div>';

            return Response($output);

    }

  }

  public function mostrar_pte_liquidar(Request $request){
    if($request->ajax()){
      $fecha = $request->fecha;
      $empresa = $request->empresa;

      $facturas = DB::table("europa_facturacion")->selectRaw("*,`FEcha Emis` as 'fechaemision'")->whereRaw("ClaseLiq='T' and codContacto='".$empresa."' and ClaveLiq not in (select FacturaOrigen from europa_facturacion where FacturaOrigen IS NOT NULL) and year(`FEcha Emis`)='".$fecha."' and conProvisionFondos=1")->get();

      $liquidadas = DB::table("europa_facturacion")->selectRaw("*,`FEcha Emis` as 'fechaemision'")->whereRaw("ClaseLiq='T' and codContacto='".$empresa."' and ClaveLiq  in (select FacturaOrigen from europa_facturacion where FacturaOrigen IS NOT NULL) and year(`FEcha Emis`)='".$fecha."' and conProvisionFondos=1")->get();

       $output= "No se han encontrado facturas que coincidan con los parámetros enviados";

       foreach ($facturas as $key => $factura) {
        $output = '<tr">';
        $importes_factura = DB::table("europa_facturacion detalle")->selectRaw("SUM(PRECIO*UNIDADES) as 'cantidad'")->where("ClaveLiq","=",$factura->ClaveLiq)->first();
         $aplicacion = DB::table("europa_aplicaciones provision fondos")->selectRaw("SUM(importe) as 'importes'")->where("ClaveLiq","=",$factura->ClaveLiq)->first();

            $baseimponible = $factura->impFacturaCto * (100 - 30) / (100 + $factura->IVA);     
            $iva = ($baseimponible * $factura->IVA) / 100;
            $provision = $factura->impFacturaCto - $baseimponible - $iva;
            $total = $factura->impFacturaCto;
        

         $output.='<td><input type="radio" name="selec" class="selec"><input type="hidden" class="clavefactura" value="'.$factura->ClaveLiq.'"></td>';
         $output.='<td><input type="hidden" class="idfactura" value="'.$factura->idfactura.'">'.$factura->NumLiq.'</td>';
         $output.='<td>'.date('d/m/Y', strtotime($factura->fechaemision)).'</td>';
         $output.='<td>'.round($baseimponible,2).' €</td>';
         $output.='<td>'.round($iva,2).'€</td>';
         $output.='<td>'.round($provision,2).' €</td>';
         if($aplicacion !=null){
          $aplicado = $aplicacion->importes;
          $honorarios = $provision - $aplicado;
          $output.='<td>'.round($aplicado,2).' €</td>';
          $output.='<td class ="honorarios">'.round($honorarios,2).' €</td>';
         }else{
          $output.='<td>0 €</td>';
          $output.='<td>'.round($provision,2).' €</td>';
         }
         $output.='<td>'.round($total,2).' €</td>';
         $output.='<td><button type="button" class="btn btn-primary aplicacion">Aplicar prov.</button></td>';
        $output .= '</tr">';
       }

            return Response($output);

    }
  }

  public function mostrar_liquidadas(Request $request){
    if($request->ajax()){
      $fecha = $request->fecha;
      $empresa = $request->empresa;

      $facturas = DB::table("europa_facturacion")->selectRaw("*,`FEcha Emis` as 'fechaemision'")->whereRaw("ClaseLiq='T' and codContacto='".$empresa."' and ClaveLiq in (select FacturaOrigen from europa_facturacion where FacturaOrigen IS NOT NULL) and year(`FEcha Emis`)='".$fecha."' and conProvisionFondos=1")->get();
   
    $output= "No se han encontrado facturas que coincidan con los parámetros enviados";
       foreach ($facturas as $key => $factura) {
        $output = '<tr">';
        $importes_factura = DB::table("europa_facturacion detalle")->selectRaw("SUM(PRECIO*UNIDADES) as 'cantidad'")->where("ClaveLiq","=",$factura->ClaveLiq)->first();
         $aplicacion = DB::table("europa_aplicaciones provision fondos")->selectRaw("SUM(importe) as 'importes'")->where("ClaveLiq","=",$factura->ClaveLiq)->first();

            $baseimponible = $factura->impFacturaCto * (100 - 30) / (100 + $factura->IVA);     
            $iva = ($baseimponible * $factura->IVA) / 100;
            $provision = $factura->impFacturaCto - $baseimponible - $iva;
            $total = $factura->impFacturaCto;
      
         $output.='<td>'.$factura->NumLiq.'</td>';
         $output.='<td>'.date('d/m/Y', strtotime($factura->fechaemision)).'</td>';
         $output.='<td>'.round($baseimponible,2).' €</td>';
         $output.='<td>'.round($iva,2).'€</td>';
         $output.='<td>'.round($provision,2).' €</td>';
         if($aplicacion !=null){
          $aplicado = $aplicacion->importes;
          $output.='<td>'.round($aplicado,2).' €</td>';
          $output.='<td>'.round($provision,2) - round($aplicado,2).' €</td>';
         }else{
          $output.='<td>0 €</td>';
          $output.='<td>'.round($provision,2).' €</td>';
         }
         $output.='<td>'.round($total,2).' €</td>';
        $output .= '</tr">';
       }

      

            return Response($output);

    }
  }

  public function mostrar_aplicacion(Request $request){

    if($request->ajax()){

      $idfactura = $request->idfactura;

      $factura = DB::table("europa_facturacion")->where("idfactura","=",$idfactura)->first();

      $aplicaciones = DB::table("europa_aplicaciones provision fondos")->whereRaw("ClaveLiq = (select ClaveLiq from europa_facturacion where idfactura='".$idfactura."')")->get();

       $output = '<div class="row"><input type="hidden" class="claveliq_aplicacion" value="'.$factura->ClaveLiq.'"><div class="col-md-12"> <div class="form-group col-md-6">
                            <label for="empresa">Acuerdo/Expediente</label>
                            <input type="text" class="form-control expediente" id="expediente" value="" placeholder="">
                        </div>';

             $output .= '<div class="form-group col-md-3"><label for="empresa">Importe</label> <input type="number" class="form-control importe_aplicacion" value="" placeholder="">';
            
              $output.='</div>';

              $output.='<div class="form-group col-md-6"><button type="button" class="btn btn-primary agregar_aplicacion">Agregar</button></div></div>';

            $output .= '<div class="col-md-12"><div class="form-group col-md-12" id="tablafacturas"><label>Otras Aplicaciones</label';
            $output .= '<div class="table-responsive" id="tabla_contenido">
                    <table class="table"><thead class="table-header">
                        <th>Expediente</th>
                        <th>Importe</th>
                        </thead>
                        <tbody>';

                        foreach ($aplicaciones as $key => $aplicacion) {
                        $output.='<tr>';
                        $output.= '<td>'.$aplicacion->identSancion.'</td>';
                        $output.= '<td>'.$aplicacion->importe.'</td>';
                        $output.= '</tr>';
                        }

                   $output .='</tbody></table></div>';
                   return Response($output);


    }
  }

  public function insertar_aplicacion (Request $request){

    if($request->ajax()){
      $clavefactura = $request->clavefactura;
      $expediente = $request->expendiente;
      $importe = $request->importe;

       try { 


    
              DB::table("europa_aplicaciones provision fondos")->insert(['ClaveLiq'=>$clavefactura, 'identSancion'=>$expediente, 'importe'=>$importe]);


            return  Response("El detalle se ha añadido correctamente");

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }

         }


    }
  

  public function mostrar_origen(Request $request){

            $facturas = DB::table("europa_facturacion")->where("codContrato","=",$request->contrato)->get();
          
               $output = "<br><label>Factura Origen</label>";

              $output.='<br><select class="selectpicker" data-live-search="true" title="Buscar..." id="factura_origen" name="factura_origen">';

                foreach ($facturas as $key => $factura) {

                     $output.='<option value="'.$factura->ClaveLiq.'">'.$factura->NumLiq.'</option>';
                }
            
              $output .= "</select><br>";

            return Response($output);
  }

   public function generar_factura_decontrato(Request $request)
    {
        //
         if($request->ajax()){

            $codContrato = $request->codContrato;
            $tipo = null;
            $origen = $request->origen;
            $fechaemision = null;
            $notas = $request->notas;
            $iva = $request->iva;
            $reduccion = false;
            $importe_reduccion = 0;

            if($request->fechaemision !=null){
                $fechaemision = DateTime::createFromFormat('d/m/Y', $request->fechaemision);
                date_time_set($fechaemision, 00, 00);
            }

            if($request->tipo == 'factura'){
              $tipo = 'F';
            }else{
              $tipo = 'A';
            }

            $contacto = DB::table("europa_contactos")->whereRaw("CodContacto =(select codContratado from europa_contratos where codContrato='".$codContrato."')")->first();

            $contrato = DB::table("europa_contratos")->where("CodContrato","=",$codContrato)->first();


            $anexos = DB::table(DB::raw("europa_anexos a, europa_tipo_anexo b"))->selectRaw("a.Matricula,b.tipoAnexo")->whereRaw("a.CodTipoAnexo=b.CodtipoAnexo and a.CodContrato ='".$codContrato."'")->get();

            $abonos = DB::table("europa_abonos")->where("codContrato","=",$codContrato)->get();

            $provisionfondos = $contrato->conProvisionFondos;

            $clase = 'T';

            $contratante = $contrato->codContratante;

            if($tipo == 'F'){

              if($contratante == 10){
              
              $max_num_actual = DB::table("europa_facturacion")->selectRaw("CONVERT(substr(NumLiq, 1, (length(NumLiq)- 
              length(SUBSTRING_INDEX((NumLiq), '/', -1))-1)),UNSIGNED INTEGER) as 'numero',RIGHT(NumLiq,2) as 'fecha'")->whereRaw("Entidad ='10' and TipoLiq='F' and RIGHT(NumLiq,2) = '".date('y')."'")->orderBy("numero","DESC")->first();

              if($max_num_actual != null){
                $nuevo_numero = $max_num_actual->numero + 1;
              }else{
                $nuevo_numero = 1;
              }

              $num_factura = $nuevo_numero.'/'.date('y');

            }elseif($contratante == 3){
              
              $max_num_actual = DB::table("europa_facturacion")->selectRaw("CONVERT(substr(NumLiq, 1, (length(NumLiq)- 
              length(SUBSTRING_INDEX((NumLiq), '/', -1))-1)),UNSIGNED INTEGER) as 'numero',RIGHT(NumLiq,2) as 'fecha'")->whereRaw("Entidad ='10' and TipoLiq='F' and RIGHT(NumLiq,2) = '".date('y')."'")->orderBy("numero","DESC")->first();

              if($max_num_actual != null){
                $nuevo_numero = $max_num_actual->numero + 1;
              }else{
                $nuevo_numero = 1;
              }

              $num_factura = $nuevo_numero.'/'.date('y');

            }else{

              $max_num_actual = DB::table("europa_facturacion")->selectRaw("CONVERT(substr(NumLiq, 1, (length(NumLiq)- 
              length(SUBSTRING_INDEX((NumLiq), '/', -1))-1)),UNSIGNED INTEGER) as 'numero',RIGHT(NumLiq,2)")->whereRaw("Entidad ='9' and TipoLiq='F' and RIGHT(NumLiq,4) = '".date('Y')."'")->orderBy("numero","DESC")->first();

              if($max_num_actual != null){
                $nuevo_numero = $max_num_actual->numero + 1;
              }else{
                $nuevo_numero = 1;
              }

              $num_factura = $nuevo_numero.'/'.date('Y');

            }

            }else{

              if($contratante == 10){
              
              $max_num_actual = DB::table("europa_facturacion")->selectRaw("CONVERT(substr(NumLiq, 1, (length(NumLiq)- 
              length(SUBSTRING_INDEX((NumLiq), '/', -1))-1)),UNSIGNED INTEGER) as 'numero',RIGHT(NumLiq,2) as 'fecha'")->whereRaw("Entidad ='10' and TipoLiq='A' and RIGHT(NumLiq,2) = '".date('y')."'")->orderBy("numero","DESC")->first();

              if($max_num_actual != null){
                $nuevo_numero = $max_num_actual->numero + 1;
              }else{
                $nuevo_numero = 1;
              }

              $num_factura = $nuevo_numero.'/'.date('y');

            }elseif($contratante == 3){
              
              $max_num_actual = DB::table("europa_facturacion")->selectRaw("CONVERT(substr(NumLiq, 1, (length(NumLiq)- 
              length(SUBSTRING_INDEX((NumLiq), '/', -1))-1)),UNSIGNED INTEGER) as 'numero',RIGHT(NumLiq,2) as 'fecha'")->whereRaw("Entidad ='10' and TipoLiq='A' and RIGHT(NumLiq,2) = '".date('y')."'")->orderBy("numero","DESC")->first();

              if($max_num_actual != null){
                $nuevo_numero = $max_num_actual->numero + 1;
              }else{
                $nuevo_numero = 1;
              }

              $num_factura = $nuevo_numero.'/'.date('y');

            }else{

              $max_num_actual = DB::table("europa_facturacion")->selectRaw("CONVERT(substr(NumLiq, 1, (length(NumLiq)- 
              length(SUBSTRING_INDEX((NumLiq), '/', -1))-1)),UNSIGNED INTEGER) as 'numero',RIGHT(NumLiq,2)")->whereRaw("Entidad ='9' and TipoLiq='A' and RIGHT(NumLiq,4) = '".date('Y')."'")->orderBy("numero","DESC")->first();

              if($max_num_actual != null){
                $nuevo_numero = $max_num_actual->numero + 1;
              }else{
                $nuevo_numero = 1;
              }

              $num_factura = $nuevo_numero.'/'.date('Y');

            }

            }

            

             $clave = $contratante.$tipo.$clase.$num_factura;


              try { 

                //Seleccionar las condiciones del contrato según los parametros establecidos

             if($tipo =='A'){

          
              $factura_origen =  DB::table("europa_facturacion")->where("ClaveLiq",'=',$origen)->first();
       
                //si es abono lo volvemos negativo
              $factura =  DB::table("europa_facturacion")->insertGetId(['ClaveLiq'=>$clave,
             'Notas'=>$notas, 'FEcha Emis'=>$fechaemision,'IVA'=>$iva,'conProvisionFondos'=>$provisionfondos,'Entidad'=>$contratante,'TipoLiq'=>$tipo,'ClaseLiq'=>$clase,'NumLiq'=>$num_factura,'codContacto'=>$contacto->CodContacto,'FacturaOrigen'=>$origen,'impFacturaCto'=>$factura_origen->impFacturaCto * -1,'codContrato'=>$codContrato]);

            //crear detalle de cabecera
            DB::table("europa_facturacion detalle")->insert(['ClaveLiq'=>$clave,
                'CONCEPTO'=>"CONTRATO FECHA ".$request->fechaemision]);

            //generar un detalle por cada matricula del anexo
            foreach ($anexos as $key => $anexo) {
              
                DB::table("europa_facturacion detalle")->insert(['ClaveLiq'=>$clave,
                'CONCEPTO'=>$anexo->tipoAnexo,'MATRICULA'=>$anexo->Matricula]);
            }

            if($contrato->descuento !=null){
              DB::table("europa_facturacion detalle")->insert(['ClaveLiq'=>$clave,
                'CONCEPTO'=>'Descuento comercial ('.$contrato->descuento.' €)']);
            }

              return  Response($factura);

            }else{
               $factura =  DB::table("europa_facturacion")->insertGetId(['ClaveLiq'=>$clave,
             'Notas'=>$notas, 'FEcha Emis'=>$fechaemision,'IVA'=>$iva,'conProvisionFondos'=>$provisionfondos,'Entidad'=>$contratante,'TipoLiq'=>$tipo,'ClaseLiq'=>$clase,'NumLiq'=>$num_factura,'codContacto'=>$contacto->CodContacto,'FacturaOrigen'=>$origen,'impFacturaCto'=>$contrato->importeCto,'codContrato'=>$codContrato]);


            foreach ($abonos as $key => $abono) {
              if($abono->tipoAbono == "0"){
              $factura_creada =  DB::table("europa_facturacion")->where("idfactura",'=',$factura)->first();
                DB::table("europa_facturacion")->where("idfactura",'=',$factura)->update([
              'impFacturaCto'=>$factura_creada->impFacturaCto - $abono->importe]);
              }
            }

            //crear detalle de cabecera
            DB::table("europa_facturacion detalle")->insert(['ClaveLiq'=>$clave,
                'CONCEPTO'=>"CONTRATO FECHA ".$request->fechaemision]);

            //generar un detalle por cada matricula del anexo
            foreach ($anexos as $key => $anexo) {
              
                DB::table("europa_facturacion detalle")->insert(['ClaveLiq'=>$clave,
                'CONCEPTO'=>$anexo->tipoAnexo,'MATRICULA'=>$anexo->Matricula]);
            }

            if($contrato->descuento !=null){
              DB::table("europa_facturacion detalle")->insert(['ClaveLiq'=>$clave,
                'CONCEPTO'=>'Descuento comercial ('.$contrato->descuento.' €)']);
            }

              return  Response($factura);
            }
            
            

          
           

          
               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }

         }
    }

     public function generar_factura_deprovision(Request $request)
    {
        //
         if($request->ajax()){

            $factura = $request->factura;
            $importe = $request->honorarios;
            $tipo = 'factura';

            $symbols = array('$', '€', '£');
            $honorarios = str_replace($symbols, '', $importe);
            $honorarios = str_replace(' ', '', $honorarios);


            if($request->tipo == 'factura'){
              $tipo = 'F';
            }else{
              $tipo = 'A';
            }

            $factura_origen = DB::table("europa_facturacion")->where("ClaveLiq","=",$factura)->first();

            $contacto = DB::table("europa_contactos")->whereRaw("CodContacto =(select codContacto from europa_facturacion where ClaveLiq='".$factura."')")->first();

            $contrato = DB::table("europa_contratos")->whereRaw("CodContrato = (select codContrato from europa_facturacion where ClaveLiq = '".$factura."') ")->first();

            $anexos = DB::table(DB::raw("europa_anexos a, europa_tipo_anexo b"))->selectRaw("a.Matricula,b.tipoAnexo")->whereRaw("a.CodTipoAnexo=b.CodtipoAnexo and a.CodContrato ='".$factura_origen->codContrato."'")->get();

            $abonos = DB::table("europa_abonos")->where("codContrato","=",$factura_origen->codContrato)->get();

            $provisionfondos = $contrato->conProvisionFondos;

            $clase = 'T';

            $contratante = $contrato->codContratante;

            if($contratante == 10){
              
              $max_num_actual = DB::table("europa_facturacion")->selectRaw("CONVERT(substr(NumLiq, 1, (length(NumLiq)- 
              length(SUBSTRING_INDEX((NumLiq), '/', -1))-1)),UNSIGNED INTEGER) as 'numero',RIGHT(NumLiq,2) as 'fecha'")->whereRaw("Entidad ='10' and RIGHT(NumLiq,2) = '".date('y')."'")->orderBy("numero","DESC")->first();

              if($max_num_actual != null){
                $nuevo_numero = $max_num_actual->numero + 1;
              }else{
                $nuevo_numero = 1;
              }

              $num_factura = $nuevo_numero.'/'.date('y');

            }elseif($contratante == 3){
              
              $max_num_actual = DB::table("europa_facturacion")->selectRaw("CONVERT(substr(NumLiq, 1, (length(NumLiq)- 
              length(SUBSTRING_INDEX((NumLiq), '/', -1))-1)),UNSIGNED INTEGER) as 'numero',RIGHT(NumLiq,2) as 'fecha'")->whereRaw("Entidad ='10' and RIGHT(NumLiq,2) = '".date('y')."'")->orderBy("numero","DESC")->first();

              if($max_num_actual != null){
                $nuevo_numero = $max_num_actual->numero + 1;
              }else{
                $nuevo_numero = 1;
              }

              $num_factura = $nuevo_numero.'/'.date('y');

            }else{

              $max_num_actual = DB::table("europa_facturacion")->selectRaw("CONVERT(substr(NumLiq, 1, (length(NumLiq)- 
              length(SUBSTRING_INDEX((NumLiq), '/', -1))-1)),UNSIGNED INTEGER) as 'numero',RIGHT(NumLiq,2)")->whereRaw("Entidad ='9' and RIGHT(NumLiq,4) = '".date('Y')."'")->orderBy("numero","DESC")->first();

              if($max_num_actual != null){
                $nuevo_numero = $max_num_actual->numero + 1;
              }else{
                $nuevo_numero = 1;
              }

              $num_factura = $nuevo_numero.'/'.date('Y');

            }

             $clave = $contratante.$tipo.$clase.$num_factura;


              try { 

                //Seleccionar las condiciones del contrato según los parametros establecidos

              $factura =  DB::table("europa_facturacion")->insertGetId(['ClaveLiq'=>$clave,
             'Notas'=>$factura_origen->Notas, 'FEcha Emis'=>date('Y-m-d H:i:s'),'IVA'=>$factura_origen->IVA,'conProvisionFondos'=>0,'Entidad'=>$contratante,'TipoLiq'=>'F','ClaseLiq'=>'F','NumLiq'=>$num_factura,'codContacto'=>$contacto->CodContacto,'FacturaOrigen'=>$factura,'impFacturaCto'=>$honorarios,'codContrato'=>$factura_origen->codContrato]);


            //crear detalle de cabecera
            DB::table("europa_facturacion detalle")->insert(['ClaveLiq'=>$clave,
                'CONCEPTO'=>"Honorarios en concepto de asesoramiento y estudio de sanciones "]);

            //generar un detalle por cada matricula del anexo
            foreach ($anexos as $key => $anexo) {
              
                DB::table("europa_facturacion detalle")->insert(['ClaveLiq'=>$clave,
                'CONCEPTO'=>$anexo->tipoAnexo,'MATRICULA'=>$anexo->Matricula]);
            }

            if($contrato->descuento !=null){
              DB::table("europa_facturacion detalle")->insert(['ClaveLiq'=>$clave,
                'CONCEPTO'=>'Descuento comercial ('.$contrato->descuento.' €)']);
            }

              return  Response($factura);
            

          
               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }

         }
    }

     public function exportar_factura()
{

    //echo public_path();

    
    $clave = $_GET["clave"];
    $baseimponible = $_GET["baseimponible"];
    $iva = $_GET["iva"];
    $provision = $_GET["provision"];
    $total = $_GET["total"];
    $numfactura = $_GET["numfactura"];

    $factura = DB::table("europa_facturacion")->selectRaw("*,`FEcha Emis` as 'fechaemision'")->where("ClaveLiq","=",$clave)->first();

    $contacto = DB::table("europa_contactos")->where("codContacto","=",$factura->codContacto)->first();

    $provincia = DB::table("europa_provincias")->where("codProvincia","=",$contacto->codProvincia)->first();

    $detalles_factura = DB::table("europa_facturacion detalle")->where("ClaveLiq","=",$clave)->get();



    
        $importes_factura = DB::table("europa_facturacion detalle")->selectRaw("SUM(PRECIO*UNIDADES) as 'cantidad'")->where("ClaveLiq","=",$factura->ClaveLiq)->first();


        if($factura->TipoLiq == 'F'){
          //Si es una factura de contrato
        if($importes_factura->cantidad == 0){

            if($factura->conProvisionFondos == 1){
            $baseimponible = $factura->impFacturaCto * (100 - 30) / (100 + $factura->IVA);     
            $iva = ($baseimponible * $factura->IVA) / 100;
            $provision = $factura->impFacturaCto - $baseimponible - $iva;
            $total = $factura->impFacturaCto;
            $templateWord = new TemplateProcessor('..\public\templates\facturas\europa\factura_contrato_P.docx');
             $templateWord->setValue('provision',$provision);

        }else{
            $baseimponible = $factura->impFacturaCto / ( 1 + ( $factura->IVA / 100 ) );
            $iva = $factura->impFacturaCto - $baseimponible;
            $provision = null;
            $total = $factura->impFacturaCto;
            $templateWord = new TemplateProcessor('..\public\templates\facturas\europa\factura_contrato.docx');
        }
        //Si no es una factura de contrato
        }else{
            $baseimponible = $importes_factura->cantidad;
            $iva =($baseimponible * $factura->IVA) /100;
            $provision = $factura->provisionFondos;
            $total =$baseimponible + $iva + $provision;
            $templateWord = new TemplateProcessor('..\public\templates\facturas\europa\factura_corriente.docx');
        }

        //si es factura de provisión
        if($factura->ClaseLiq=='F'){
            $baseimponible = $importes_factura->cantidad;
            $iva =($baseimponible * $factura->IVA) /100;
            $provision = $factura->provisionFondos;
            $total =$baseimponible + $iva + $provision;
            $templateWord = new TemplateProcessor('..\public\templates\facturas\europa\factura_contrato.docx');
        }
      }elseif($factura->TipoLiq == 'A'){
           //Si es una factura de contrato
        if($importes_factura->cantidad == 0){

            if($factura->conProvisionFondos == 1){
            $baseimponible = $_GET["baseimponible"];
            $iva = $_GET["iva"];
            $provision = $_GET["provision"];
            $total = $_GET["total"];
            $templateWord = new TemplateProcessor('..\public\templates\facturas\europa\abono_contrato_P.docx');
             $templateWord->setValue('provision',$provision);

        }else{
            $baseimponible = $_GET["baseimponible"];
            $iva = $_GET["iva"];
            $provision = $_GET["provision"];
            $total = $_GET["total"];
            $templateWord = new TemplateProcessor('..\public\templates\facturas\europa\abono_contrato.docx');
        }
        //Si no es una factura de contrato
        }else{
            $baseimponible = $importes_factura->cantidad;
            $iva =($baseimponible * $factura->IVA) /100;
            $provision = $factura->provisionFondos;
            $total =$baseimponible + $iva + $provision;
            $templateWord = new TemplateProcessor('..\public\templates\facturas\europa\abono_corriente.docx');
        }

        //si es factura de provisión
        if($factura->ClaseLiq=='F'){
            $baseimponible = $importes_factura->cantidad;
            $iva =($baseimponible * $factura->IVA) /100;
            $provision = $factura->provisionFondos;
            $total =$baseimponible + $iva + $provision;
            $templateWord = new TemplateProcessor('..\public\templates\facturas\europa\abono_contrato.docx');
                   }
      }


        
    
   if($factura->ClaseLiq=='T' || $factura->ClaseLiq=='F'){
    if($factura != null && $contacto!=null){
                $templateWord->setValue('nombre_empresa',$contacto->razonSocial);
                $templateWord->setValue('direccion_empresa',$contacto->direccion);
                $templateWord->setValue('poblacion',$contacto->Poblacion);
                $templateWord->setValue('provincia',$provincia->Provincia);
                $templateWord->setValue('cif',$contacto->cif);
                $templateWord->setValue('cp',$contacto->codPostal);
                $templateWord->setValue('num_fac',$numfactura);
                $templateWord->setValue('fecha',date('d/m/Y', strtotime($factura->fechaemision)));
                $templateWord->setValue('contrato',$factura->codContrato);
                $templateWord->setValue('subtotal',round($baseimponible,2));
                $templateWord->setValue('iva',round($iva,2));
                $templateWord->setValue('total',round($total,2));


                $lista_matriculas = "";
                $lista_conceptos = "";
                $lista_importes = "";
                $lista_totales = "";

                foreach ($detalles_factura as $key => $detalle) {
                    $lista_conceptos.= $detalle->CONCEPTO."<w:br/>";
                    $lista_importes.= $detalle->PRECIO."<w:br/>";
                    $lista_matriculas.= $detalle->MATRICULA."<w:br/>";
              }

                $templateWord->setValue('concepto',$lista_conceptos);
                $templateWord->setValue('importe',$lista_importes);
                $templateWord->setValue('matricula',$lista_matriculas);
                $templateWord->setValue('importe','holaaa');

            // --- Guardamos el documento

                $nombredoc = $factura->TipoLiq.$factura->ClaseLiq.date("d").date("m").date("Y").'.docx';

    $templateWord->saveAs($nombredoc);
     header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename='.$nombredoc.'');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($nombredoc));
    readfile($nombredoc);
    exit;
    }
    }
    elseif($factura->ClaseLiq =='C'){
      if($factura != null && $contacto!=null){
                $templateWord->setValue('nombre_empresa',$contacto->razonSocial);
                $templateWord->setValue('direccion_empresa',$contacto->direccion);
                $templateWord->setValue('poblacion',$contacto->Poblacion);
                $templateWord->setValue('provincia',$provincia->Provincia);
                $templateWord->setValue('cif',$contacto->cif);
                $templateWord->setValue('cp',$contacto->codPostal);
                $templateWord->setValue('num_fac',$factura->NumLiq);
                $templateWord->setValue('fecha',date('d/m/Y', strtotime($factura->fechaemision)));
                $templateWord->setValue('subtotal',$baseimponible);
                $templateWord->setValue('iva',$iva);
                $templateWord->setValue('total',$total);
                $templateWord->setValue('notas',$factura->Notas);


                $lista_conceptos = "";
                $lista_importes = "";
                $lista_totales = "";
                $lista_unidades = "";

                foreach ($detalles_factura as $key => $detalle) {
                    $lista_conceptos.= $detalle->CONCEPTO."<w:br/>";
                    $lista_importes.= $detalle->PRECIO."<w:br/>";
                    $unidades = $detalle->UNIDADES;
                    $lista_unidades .= $detalle->UNIDADES."<w:br/>";
                    $precio = $detalle->PRECIO;
                    if($precio!=null){
                    $precio = $detalle->PRECIO;
                    }else{
                    $precio = 0;
                    }
                     if($unidades!=null){
                    $unidades = $detalle->UNIDADES;
                    }else{
                    $unidades = 1;
                    }
                    $total_precio = $unidades * $precio;
                    $lista_totales .= $total_precio."<w:br/>";
                    }
                }

                $templateWord->setValue('concepto',$lista_conceptos);
                $templateWord->setValue('unidades',$lista_unidades);
                $templateWord->setValue('precio',$lista_importes);
                $templateWord->setValue('preciot',$lista_totales);
                
    }

            // --- Guardamos el documento
    $nombredoc = $factura->TipoLiq.$factura->ClaseLiq.date("d").date("m").date("Y").'.docx';
    $templateWord->saveAs($nombredoc);
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename='.$nombredoc.'');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($nombredoc));
    readfile($nombredoc);
    exit;
    }

    public function exportar_factura_its()
{

    //echo public_path();

    
    $clave = $_GET["clave"];
    $baseimponible = $_GET["baseimponible"];
    $iva = $_GET["iva"];
    $provision = $_GET["provision"];
    $total = $_GET["total"];
    $numfactura = $_GET["numfactura"];

    $factura = DB::table("its_facturacion")->selectRaw("*,`FEcha Emis` as 'fechaemision'")->where("ClaveLiq","=",$clave)->first();

    $contacto = DB::table("its_contactos")->where("CIF","=",$factura->Cte_Fac_Abonr)->first();

    $contratante = DB::table("its_contactos")->whereRaw("CIF = (Select CIF from gesanc_empresas where num_empresa='".$factura->Entidad."')")->first();

    //$provincia = DB::table("europa_provincias")->where("codProvincia","=",$contacto->codProvincia)->first();

    $detalles_factura = DB::table("its_facturacion detalle")->where("ClaveLiq","=",$clave)->get();

    $importes_factura = DB::table("its_facturacion detalle")->selectRaw("SUM(PRECIO*UNIDADES) as 'cantidad'")->where("ClaveLiq","=",$factura->ClaveLiq)->first();


    if($factura->TipoLiq == 'F'){
      if($factura->ClaseLiq == 'C' || $factura->ClaseLiq == 'T'){

        if($factura->provisionFondos!=null){
          $templateWord = new TemplateProcessor('..\public\templates\facturas\its\Factura Corriente_P.docx');
          $templateWord->setValue('provision',round($factura->provisionFondos,2)." €");
        }else{
          $templateWord = new TemplateProcessor('..\public\templates\facturas\its\Factura Corriente.docx');
        }
                
                
        
                $templateWord->setValue('fecha',date('d/m/Y', strtotime($factura->fechaemision)));

                $templateWord->setValue('empresa_con',$contratante->Empresa);
                $templateWord->setValue('direccion_con',$contratante->Direccion);
                $templateWord->setValue('cp_con',$contratante->CP);
                $templateWord->setValue('poblacion_con',$contratante->Poblacion);
                //$templateWord->setValue('provincia_con',$contratante->cif);
                $templateWord->setValue('cif_con',$contratante->CIF);

                $templateWord->setValue('cliente',$contacto->Empresa);
                $templateWord->setValue('direccion',$contacto->Direccion);
                $templateWord->setValue('cp',$contacto->CP);
                $templateWord->setValue('poblacion',$contacto->Poblacion);
                //$templateWord->setValue('provincia',$contacto->CIF);
                $templateWord->setValue('cif',$contacto->CIF);


                $templateWord->setValue('num',$numfactura);
                $templateWord->setValue('subtotal',round($baseimponible,2)." €");
                $templateWord->setValue('iva',round($iva,2)." €");
                $templateWord->setValue('total',round($total,2)." €");


                $lista_conceptos = "";
                $lista_unidades = "";
                $lista_importes = "";
                $lista_totales = "";

                 foreach ($detalles_factura as $key => $detalle) {
                    $lista_conceptos.= $detalle->CONCEPTO."<w:br/>";
                    if($detalle->PRECIO!=null){
                      $lista_importes.= $detalle->PRECIO." € <w:br/>";
                       $unidades = $detalle->UNIDADES;
                    $lista_unidades .= $detalle->UNIDADES."<w:br/>";
                    $precio = $detalle->PRECIO;
                    if($precio!=null){
                    $precio = $detalle->PRECIO;
                    }else{
                    $precio = 0;
                    }
                     if($unidades!=null){
                    $unidades = $detalle->UNIDADES;
                    }else{
                    $unidades = 1;
                    }
                    $total_precio = $unidades * $precio;
                    $lista_totales .= $total_precio." € <w:br/>";
                    }else{
                      $lista_importes.="<w:br/>";
                      $lista_totales .= "<w:br/>";

                    }
                    
                   
                    }

                $templateWord->setValue('concepto',$lista_conceptos);
                $templateWord->setValue('unidades',$lista_unidades);
                $templateWord->setValue('precio_u',$lista_importes);
                $templateWord->setValue('total_detalle',$lista_totales);

            // --- Guardamos el documento

    $nombredoc = $factura->TipoLiq.$factura->ClaseLiq.date("d").date("m").date("Y").'.docx';

    $templateWord->saveAs($nombredoc);
     header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename='.$nombredoc.'');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($nombredoc));
    readfile($nombredoc);
    exit;
      }
      if($factura->ClaseLiq == 'H' || $factura->ClaseLiq == 'G' || $factura->ClaseLiq == 'A'){
        $templateWord = new TemplateProcessor('..\public\templates\facturas\its\Factura EC.docx');
      
                
                $templateWord->setValue('fecha',date('d M Y', strtotime($factura->fechaemision)));

                $templateWord->setValue('empresa_con',$contratante->Empresa);
                $templateWord->setValue('direccion_con',$contratante->Direccion);
                $templateWord->setValue('cp_con',$contratante->CP);
                $templateWord->setValue('poblacion_con',$contratante->Poblacion);
                //$templateWord->setValue('provincia_con',$contratante->cif);
                $templateWord->setValue('cif_con',$contratante->CIF);

                $templateWord->setValue('cliente',$contacto->Empresa);
                $templateWord->setValue('direccion',$contacto->Direccion);
                $templateWord->setValue('cp',$contacto->CP);
                $templateWord->setValue('poblacion',$contacto->Poblacion);
                //$templateWord->setValue('provincia',$contacto->CIF);
                $templateWord->setValue('cif',$contacto->CIF);
                $templateWord->setValue('inicial',date('d M Y',strtotime($factura->PeriodoIni)));
                $templateWord->setValue('final', date('d M Y',strtotime($factura->PeriodoFin)));
                $templateWord->setValue('vencimiento', date('d M Y',strtotime($factura->VenctoRecBanc)));

                $templateWord->setValue('num',$numfactura);
                $templateWord->setValue('subtotal',round($baseimponible,2)." €");
                $templateWord->setValue('iva_p',$factura->IVA." %");
                $templateWord->setValue('iva',round($iva,2)." €");
                $templateWord->setValue('total',round($total,2)." €");


                $lista_cif = "";
                $lista_beneficiario = "";
                $lista_matriculas = "";
                $lista_precio = "";


                 foreach ($detalles_factura as $key => $detalle) {
                   $beneficiario = DB::table("its_contactos")->where("CIF","=",$detalle->CIF)->first();
                $lista_cif .= $detalle->CIF."<w:br/>";
                $lista_beneficiario .= $beneficiario->Empresa."<w:br/>";
                $lista_matriculas .= $detalle->MATRICULA."<w:br/>";
                $lista_precio .= $detalle->PRECIO." € <w:br/>";
                    }
                $templateWord->setValue('cif',$lista_cif);
                $templateWord->setValue('beneficiario',$lista_beneficiario);
                $templateWord->setValue('matricula',$lista_matriculas);
                $templateWord->setValue('precio',$lista_precio);

            // --- Guardamos el documento

    $nombredoc = $factura->TipoLiq.$factura->ClaseLiq.date("d").date("m").date("Y").'.docx';

    $templateWord->saveAs($nombredoc);
     header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename='.$nombredoc.'');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($nombredoc));
    readfile($nombredoc);
    exit;

      }
    }else{
      if($factura->ClaseLiq == 'C' || $factura->ClaseLiq == 'T'){
         $templateWord = new TemplateProcessor('..\public\templates\facturas\its\Abono Corriente.docx');
              
               
                $templateWord->setValue('fecha',date('d/m/Y', strtotime($factura->fechaemision)));
                $templateWord->setValue('empresa_con',$contratante->Empresa);
                $templateWord->setValue('direccion_con',$contratante->Direccion);
                $templateWord->setValue('cp_con',$contratante->CP);
                $templateWord->setValue('poblacion_con',$contratante->Poblacion);
                //$templateWord->setValue('provincia_con',$contratante->cif);
                $templateWord->setValue('cif_con',$contratante->CIF);

                $templateWord->setValue('cliente',$contacto->Empresa);
                $templateWord->setValue('direccion',$contacto->Direccion);
                $templateWord->setValue('cp',$contacto->CP);
                $templateWord->setValue('poblacion',$contacto->Poblacion);
                //$templateWord->setValue('provincia',$contacto->CIF);
                $templateWord->setValue('cif',$contacto->CIF);


                $templateWord->setValue('num',$numfactura);
                $templateWord->setValue('subtotal',round($baseimponible,2)." €");
                $templateWord->setValue('provision',round($factura->provisionFondos,2)." €");
                $templateWord->setValue('iva',round($iva,2)." €");
                $templateWord->setValue('iva_p',$factura->IVA." %");
                $templateWord->setValue('total',round($total,2)." €");
                $templateWord->setValue('notas',$factura->Notas);


                $lista_conceptos = "";
                $lista_unidades = "";
                $lista_importes = "";
                $lista_totales = "";

                 foreach ($detalles_factura as $key => $detalle) {
                    $lista_conceptos.= $detalle->CONCEPTO."<w:br/>";
                    if($detalle->PRECIO!=null){
                      $lista_importes.= $detalle->PRECIO." € <w:br/>";
                       $unidades = $detalle->UNIDADES;
                    $lista_unidades .= $detalle->UNIDADES."<w:br/>";
                    $precio = $detalle->PRECIO;
                    if($precio!=null){
                    $precio = $detalle->PRECIO;
                    }else{
                    $precio = 0;
                    }
                     if($unidades!=null){
                    $unidades = $detalle->UNIDADES;
                    }else{
                    $unidades = 1;
                    }
                    $total_precio = $unidades * $precio;
                    $lista_totales .= $total_precio." € <w:br/>";
                    }else{
                      $lista_importes.="<w:br/>";
                      $lista_totales .= "<w:br/>";

                    }
                    
                   
                    }

                $templateWord->setValue('concepto',$lista_conceptos);
                $templateWord->setValue('unidades',$lista_unidades);
                $templateWord->setValue('precio_u',$lista_importes);
                $templateWord->setValue('total_detalle',$lista_totales);

            // --- Guardamos el documento

    $nombredoc = $factura->TipoLiq.$factura->ClaseLiq.date("d").date("m").date("Y").'.docx';

    $templateWord->saveAs($nombredoc);
     header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename='.$nombredoc.'');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($nombredoc));
    readfile($nombredoc);
    exit;
      }
      if($factura->ClaseLiq == 'H' || $factura->ClaseLiq == 'G' || $factura->ClaseLiq == 'A'){
        $templateWord = new TemplateProcessor('..\public\templates\facturas\its\Abono EC.docx');
       
               
                $templateWord->setValue('fecha',date('d M Y', strtotime($factura->fechaemision)));

                $templateWord->setValue('empresa_con',$contratante->Empresa);
                $templateWord->setValue('direccion_con',$contratante->Direccion);
                $templateWord->setValue('cp_con',$contratante->CP);
                $templateWord->setValue('poblacion_con',$contratante->Poblacion);
                //$templateWord->setValue('provincia_con',$contratante->cif);
                $templateWord->setValue('cif_con',$contratante->CIF);

                $templateWord->setValue('cliente',$contacto->Empresa);
                $templateWord->setValue('direccion',$contacto->Direccion);
                $templateWord->setValue('cp',$contacto->CP);
                $templateWord->setValue('poblacion',$contacto->Poblacion);
                //$templateWord->setValue('provincia',$contacto->CIF);
                $templateWord->setValue('cif',$contacto->CIF);
                $templateWord->setValue('inicial',date('d M Y',strtotime($factura->PeriodoIni)));
                $templateWord->setValue('final', date('d M Y',strtotime($factura->PeriodoFinn)));
                $templateWord->setValue('vencimiento', date('d M Y',strtotime($factura->VenctoRecBanc)));

                $templateWord->setValue('num',$numfactura);
                $templateWord->setValue('subtotal',round($baseimponible,2)." €");
                $templateWord->setValue('iva',round($iva,2)." €");
                $templateWord->setValue('total',round($total,2)." €");


                $lista_cif = "";
                $lista_beneficiario = "";
                $lista_matriculas = "";
                $lista_precio = "";


                 foreach ($detalles_factura as $key => $detalle) {
                   $beneficiario = DB::table("its_contactos")->where("CIF","=",$detalle->CIF)->first();
                $lista_cif .= $detalle->CIF."<w:br/>";
                $lista_beneficiario .= $beneficiario->Empresa."<w:br/>";
                $lista_matriculas .= $detalle->MATRICULA."<w:br/>";
                $lista_precio .= $detalle->PRECIO."<w:br/>";
                    }
                $templateWord->setValue('cif',$lista_cif);
                $templateWord->setValue('beneficiario',$lista_beneficiario);
                $templateWord->setValue('matricula',$lista_matriculas);
                $templateWord->setValue('precio',$lista_precio);

            // --- Guardamos el documento

    $nombredoc = $factura->TipoLiq.$factura->ClaseLiq.date("d").date("m").date("Y").'.docx';

    $templateWord->saveAs($nombredoc);
     header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename='.$nombredoc.'');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($nombredoc));
    readfile($nombredoc);
    exit;
      }
    }

    }

     public function exportar_sobre(){

    $clave = $_GET["clave"];

    $factura = DB::table("europa_facturacion")->selectRaw("*,`FEcha Emis` as 'fechaemision'")->where("ClaveLiq","=",$clave)->first();

    $contacto = DB::table("europa_contactos")->where("codContacto","=",$factura->codContacto)->first();

    $provincia = DB::table("europa_provincias")->where("codProvincia","=",$contacto->codProvincia)->first();

      $templateWord = new TemplateProcessor('..\public\templates\facturas\europa\sobre.docx');

                $templateWord->setValue('nombre_empresa',$contacto->razonSocial);
                $templateWord->setValue('direccion_empresa',$contacto->direccion);
                $templateWord->setValue('poblacion',$contacto->Poblacion);
                $templateWord->setValue('provincia',$provincia->Provincia);
                $templateWord->setValue('cp',$contacto->codPostal);


      $nombredoc = 'SOBRE_'.$factura->TipoLiq.$factura->ClaseLiq.date("d").date("m").date("Y").'.docx';
      $templateWord->saveAs($nombredoc);
      header('Content-Description: File Transfer');
      header('Content-Type: application/octet-stream');
      header('Content-Disposition: attachment; filename='.$nombredoc.'');
      header('Expires: 0');
      header('Cache-Control: must-revalidate');
      header('Pragma: public');
      header('Content-Length: ' . filesize($nombredoc));
      readfile($nombredoc);
}

 public function exportar_excel_europa(Request $request){
            

            $filtros = array();

            $consulta = "";

              if($request->alta_inicio != null && $request->alta_fin !=null){
                $alta_inicio = $request->alta_inicio;
                $alta_fin = $request->alta_fin;

                $FechaContrato = "`FEcha Emis` BETWEEN '".$alta_inicio."' AND '".$alta_fin."'";

                $filtros[] = $FechaContrato;
              
            }


            if($request->numfactura != null){
                $numfactura = $request->numfactura;
                $filtro_numfactura = "NumLiq = '".$numfactura."'";
                $filtros[] = $filtro_numfactura;
            }

             if($request->codContrato != null){
                $codContrato = $request->codContrato;
                $filtro_codContrato = "codContrato = '".$codContrato."'";
                $filtros[] = $filtro_codContrato;
            } 

             if($request->clavefactura != null){
                $clavefactura = $request->clavefactura;
                $filtro_clavefactura = "ClaveLiq = '".$clavefactura."'";
                $filtros[] = $filtro_clavefactura;
            }

             if($request->fac != null){
                $fac = $request->fac;
                $filtro_fac = "TipoLiq = '".$fac."'";
                $filtros[] = $filtro_fac;
            }

              if($request->empresa != null){
                $empresa = $request->empresa;
                $filtro_empresa = "Cte_Fac_Abonr = '".$empresa."'";
                $filtros[] = $filtro_empresa;
            }
            if($request->cif != null){
                $cif = $request->cif;
                $filtro_cif = "Cte_Fac_Abonr = '".$cif."'";
                $filtros[] = $filtro_cif;
            }          

             if($request->contratante != null){
                $contratante = $request->contratante;
                $filtro_contratante = "Entidad = '".$contratante."'";
                $filtros[] = $filtro_contratante;
            } 

            if($request->tipo != null){
                $tipo = $request->tipo;
                $filtro_tipo = "ClaseLiq = '".$tipo."'";
                $filtros[] = $filtro_tipo;
            } 

            if($request->matricula != null){
                $matricula = $request->matricula;
                $filtro_matricula = "ClaveLiq in (select ClaveLiq from `europa_facturacion detalle` where MATRICULA='".$matricula."')";
                $filtros[] = $filtro_matricula;
            } 

             if($request->precio != null){
                $precio = $request->precio;
                $filtro_precio = "ClaveLiq in (select ClaveLiq from `europa_facturacion detalle` where PRECIO='".$precio."')";
                $filtros[] = $filtro_precio;
            } 

              if($request->coddetalle != null){
                $coddetalle = $request->coddetalle;
                $filtro_coddetalle = "ClaveLiq in (select ClaveLiq from `europa_facturacion detalle` where CODIGO='".$coddetalle."')";
                $filtros[] = $filtro_coddetalle;
            } 

              if($request->unidades != null){
                $unidades = $request->unidades;
                $filtro_unidades = "ClaveLiq in (select ClaveLiq from `europa_facturacion detalle` where UNIDADES='".$unidades."')";
                $filtros[] = $filtro_unidades;
            }

            if($request->provisionfondos != null){
                $filtro_provision = "conProvisionFondos = 1";
                $filtros[] = $filtro_provision;
            } 

            for ($i =count($filtros) - 1; $i >= 0 ; $i--) { 
                $consulta .= $filtros[$i];
                $consulta .= " ";

                if($i > 0){
                    $consulta .= "AND ";
                }
            }

            $facturas = DB::table(DB::raw("europa_facturacion a, europa_contactos b"))->selectRaw("a.idfactura,b.razonSocial as 'cliente', a.NumLiq as 'numero',CASE a.TipoLiq
        WHEN 'F' THEN 'Factura'
        ELSE 'Abono'
        END as 'tipo',
        CASE a.ClaseLiq
        WHEN 'F' THEN 'Provisión de fondo'
        WHEN 'C' THEN 'Corriente'
        WHEN 'T' THEN 'De contrato'
        END as 'clase',
        a.impFacturaCto as 'importe',
        a.Notas")->whereRaw("a.Cte_Fac_Abonr=b.cif")->orderby("FEcha Emis","DESC")->limit(200)->get();



            
            if($consulta !=""){

              $facturas = DB::table(DB::raw("europa_facturacion a, europa_contactos b"))->selectRaw("a.idfactura,b.razonSocial as 'cliente', a.NumLiq as 'numero',CASE a.TipoLiq
        WHEN 'F' THEN 'Factura'
        ELSE 'Abono'
        END as 'tipo',
        CASE a.ClaseLiq
        WHEN 'F' THEN 'Provisión de fondo'
        WHEN 'C' THEN 'Corriente'
        WHEN 'T' THEN 'De contrato'
        END as 'clase',
        a.impFacturaCto as 'importe',
        a.Notas")->whereRaw("a.Cte_Fac_Abonr=b.cif AND ".$consulta)->orderby("FEcha Emis","DESC")->limit(200)->get();

             }
            
        $objPHPExcel = new PHPExcel();
        // Se asignan las propiedades del libro
        $objPHPExcel->getProperties()->setCreator("ITS") // Nombre del autor
        ->setLastModifiedBy("ITS") //Ultimo usuario que lo modificó
        ->setTitle("facturas_filtradas") // Titulo
        ->setDescription("facturas_filtradas"); //Descripción
        $titulosColumnas = array('Cliente'
                                ,'Nº Fac'
                                ,'Tipo'
                                ,'Clase'
                                ,'Importe'
                                ,'Notas');
        
        // Se combinan las celdas, para colocar ahí el titulo del reporte
        //$objPHPExcel->setActiveSheetIndex(0)
        //->mergeCells('A1:D1');
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1', $titulosColumnas[0])
        ->setCellValue('B1', $titulosColumnas[1])
        ->setCellValue('C1', $titulosColumnas[2])
        ->setCellValue('D1', $titulosColumnas[3])
        ->setCellValue('E1', $titulosColumnas[4])
        ->setCellValue('F1', $titulosColumnas[5]);



        $i=2;
        foreach ($facturas as $key => $factura) {
              $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.$i, $factura->cliente)
            ->setCellValue('B'.$i, $factura->numero)
            ->setCellValue('C'.$i, $factura->tipo)
            ->setCellValue('D'.$i, $factura->clase)
            ->setCellValue('E'.$i, $factura->importe)
            ->setCellValue('F'.$i, $factura->Notas);
            $i++;
        }



    header('Content-Encoding: UTF-8');
    header('Content-type: text/csv; charset=UTF-8');
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="facturas_filtradas.xlsx"');
    header('Cache-Control: max-age=0');
    header("Pragma: no-cache");
    header("Expires: 0");
    header('Content-Transfer-Encoding: binary');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
             
}

}
  







