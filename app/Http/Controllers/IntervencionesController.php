<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\gesanc_sanciones;

use App\gesanc_lista_motivos_denuncia;

use App\gesanc_reduccion;

use App\gesanc_contencioso;

use App\gesanc_clientes;

use DateTime;

use Illuminate\Database\QueryException;

use App\gesanc_contratos;

use PhpOffice\PhpWord\TemplateProcessor;

use PHPExcel; 
use PHPExcel_IOFactory;

use DocxMerge\DocxMerge;

use Auth;

class IntervencionesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $intervenciones = DB::table(DB::raw("inter24_interventions a, gesanc_paises b"))->selectRaw("a.ACUERDO,a.MATRICULA,a.FECHA,a.NumDossier,a.CLIENTE,b.Pais,a.IMPORTE,a.*")->whereRaw("a.PAIS=b.`Codigo Pais`")->orderby("FECHA","DESC")->limit(200)->get();

        $clientes = DB::table("gesanc_clientes")->get();

        $contratantes = DB::table("gesanc_empresas")->get();

        $matriculas = DB::table("gesanc_matriculas")->get();

        $comerciales = DB::table("gesanc_comerciales")->selectRaw("*,`Id-comercial` as 'idcomercial'")->get();

        $conductores = DB::table("gesanc_choferes")->get();

        $paises = DB::table("gesanc_paises")->selectRaw("*,`Codigo Pais` as 'codpais'")->get();

        $autoridades = DB::table("gesanc_autoridades")->get();

        $motivos = DB::table("inter24_motivos")->get();

        $actores = DB::table("inter24_actores")->get();

        $entidades = DB::table("inter24_nombre actor")->get();

        $operaciones = DB::table("inter24_operaciones")->get();


        return view('intervenciones/index',['intervenciones'=>$intervenciones,'clientes'=> $clientes,'comerciales'=>$comerciales,'contratantes'=>$contratantes,'matriculas'=>$matriculas,'conductores'=>$conductores,'paises'=>$paises,'autoridades'=>$autoridades,'motivos'=>$motivos,'actores'=>$actores,'entidades'=>$entidades,'operaciones'=>$operaciones]);

        /**/
    }

    public function filtrar(Request $request){
            
        $output="<p style='text-align:center;'>No se han encontrado resultados.</p>";

            if($request->ajax()){

            $filtros = array();

            $consulta = "";

              if($request->matricula != null){
                $matricula = $request->matricula;
                $filtro_matricula = "MATRICULA = '".$matricula."'";
                $filtros[] = $filtro_matricula;
            }
            if($request->acuerdo != null){
                $acuerdo = $request->acuerdo;
                $filtro_acuerdo = "ACUERDO = '".$acuerdo."'";
                $filtros[] = $filtro_acuerdo;
            }
            if($request->importe != null){
                $importe = $request->importe;
                $filtro_importe = "IMPORTE = '".$importe."'";
                $filtros[] = $filtro_importe;
            }
            if($request->apertura != null){
                $apertura = $request->apertura;
                $filtro_apertura = "F_APERTURA = '".$apertura."'";
                $filtros[] = $filtro_apertura;
            }
            if($request->cif != null){
                $cif = $request->cif;
                $filtro_cif = "CIF = '".$cif."'";
                $filtros[] = $filtro_cif;
            }
            if($request->empresa != null){
                $empresa = $request->empresa;
                $filtro_empresa = "CLIENTE = '".$empresa."'";
                $filtros[] = $filtro_empresa;
            }
            if($request->contratante != null){
                $contratante = $request->contratante;
                $filtro_contratante = "EMPRESA = '".$contratante."'";
                $filtros[] = $filtro_contratante;
            }
            if($request->ordenante != null){
                $ordenante = $request->ordenante;
                $filtro_ordenante = "IdComercial = '".$ordenante."'";
                $filtros[] = $filtro_ordenante;
            }
            if($request->conductor != null){
                $conductor = $request->conductor;
                $filtro_conductor = "CHOFER = '".$conductor."'";
                $filtros[] = $filtro_conductor;
            }
            if($request->lugar != null){
                $lugar = $request->lugar;
                $filtro_lugar = "LUGAR = '".$lugar."'";
                $filtros[] = $filtro_lugar;
            }
            if($request->ciudad != null){
                $ciudad = $request->ciudad;
                $filtro_ciudad = "CIUDAD = '".$ciudad."'";
                $filtros[] = $filtro_ciudad;
            }
            if($request->pais != null){
                $pais = $request->pais;
                $filtro_pais = "a.PAIS = '".$pais."'";
                $filtros[] = $filtro_pais;
            }
            if($request->autoridad != null){
                $autoridad = $request->autoridad;
                $filtro_autoridad = "Autoridad = '".$autoridad."'";
                $filtros[] = $filtro_autoridad;
            }
            if($request->telefono != null){
                $telefono = $request->telefono;
                $filtro_telefono = "TelefAutoridad = '".$telefono."'";
                $filtros[] = $filtro_telefono;
            }
            if($request->fax != null){
                $fax = $request->fax;
                $filtro_fax = "FaxAutoridad = '".$fax."'";
                $filtros[] = $filtro_fax;
            }
            if($request->facprov != null){
                $facprov = $request->facprov;
                $filtro_facprov = "FacturadoTAI = '".$facprov."'";
                $filtros[] = $filtro_facprov;
            }
            if($request->facits != null){
                $facits = $request->facits;
                $filtro_facits = "FacturaITS = '".$facits."'";
                $filtros[] = $filtro_facits;
            }
            if($request->importeprov != null){
                $importeprov = $request->importeprov;
                $filtro_importeprov = "ImporteFProv = '".$importeprov."'";
                $filtros[] = $filtro_importeprov;
            }
            if($request->importeits != null){
                $importeits = $request->importeits;
                $filtro_importeits = "ImporteFITS = '".$importeits."'";
                $filtros[] = $filtro_importeits;
            }
            if($request->gastos != null){
                $gastos = $request->gastos;
                $filtro_gastos = "Gastos = '".$gastos."'";
                $filtros[] = $filtro_numfactura;
            }
            if($request->remision != null){
                $remision = $request->remision;
                $filtro_remision = "fecharemision = '".$remision."'";
                $filtros[] = $filtro_remision;
            }
            if($request->dossier != null){
                $dossier = $request->dossier;
                $filtro_dossier = "NumDossier = '".$dossier."'";
                $filtros[] = $filtro_dossier;
            }
            if($request->notas != null){
                $notas = $request->notas;
                $filtro_notas = "Notas = '".$notas."'";
                $filtros[] = $filtro_notas;
            }
            if($request->anulada != null){
                $anulada = $request->anulada;
                $filtro_anulada = "anuladaIntervencion = '".$anulada."'";
                $filtros[] = $filtro_anulada;
            }
            if($request->fc != null){
                $fc = $request->fc;
                $filtro_fc = "FaxConf = '".$fc."'";
                $filtros[] = $filtro_fc;
            }

            //MOTIVOS
            if($request->motivo != null){
                $motivo = $request->motivo;
                $filtro_motivo = "ID in (select ID from `inter24_motivos por multa` where IdMotivo='".$motivo."')";
                $filtros[] = $filtro_motivo;
            }
            if($request->estado != null){
                $estado = $request->estado;
                $filtro_estado = "ID in (select ID from `inter24_motivos por multa` where estado_caucion='".$estado."')";
                $filtros[] = $filtro_estado;
            }
            if($request->importe_motivo != null){
                $importe_motivo = $request->importe_motivo;
                $filtro_importe_motivo = "ID in (select ID from `inter24_motivos por multa` where Importe='".$importe."')";
                $filtros[] = $filtro_importe_motivo;
            }
            if($request->cheque != null){
                $cheque = $request->cheque;
                $filtro_cheque = "ID in (select ID from `inter24_motivos por multa` where Cheque='".$cheque."')";
                $filtros[] = $filtro_cheque;
            }
            if($request->detalles != null){
                $detalles = $request->detalles;
                $filtro_detalles = "ID in (select ID from `inter24_motivos por multa` where Detalles='".$detalle."')";
                $filtros[] = $filtro_detalles;
            }
            if($request->tarjeta != null){
                $tarjeta = $request->tarjeta;
                $filtro_tarjeta = "ID in (select ID from `inter24_motivos por multa` where numTarjeta='".$tarjeta."')";
                $filtros[] = $filtro_tarjeta;
            }
            if($request->caucionpagada != null){
                $caucionpagada = $request->caucionpagada;
                $filtro_caucionpagada = "ID in (select ID from `inter24_motivos por multa` where caucionPagada='".$caucionpagada."')";
                $filtros[] = $filtro_caucionpagada;
            }

            //tramites
               if($request->fecha_t != null){
                $fecha_t = $request->fecha_t;
                $filtro_fecha_t = "ID in (select IDMULTA from inter24_tramites where FECHA='".$fecha_t."')";
                $filtros[] = $filtro_fecha_t;
            }
            if($request->hora_tramite != null){
                $hora_tramite = $request->hora_tramite;
                $filtro_hora_tramite = "ID in (select IDMULTA from inter24_tramites where HORA='".$hora_tramite."')";
                $filtros[] = $filtro_hora_tramite;
            }
            if($request->persona1 != null){
                $persona1 = $request->persona1;
                $filtro_persona1 = "ID in (select IDMULTA from inter24_tramites where COD_PERSONA1='".$persona1."')";
                $filtros[] = $filtro_persona1;
            }
            if($request->persona2 != null){
                $persona2 = $request->persona2;
                $filtro_persona2 = "ID in (select IDMULTA from inter24_tramites where COD_PERSONA2='".$persona2."')";
                $filtros[] = $filtro_persona2;
            }
            if($request->entidad1 != null){
                $entidad1 = $request->entidad1;
                $filtro_entidad1 = "ID in (select IDMULTA from inter24_tramites where COD_ACTOR1='".$entidad1."')";
                $filtros[] = $filtro_entidad1;
            }
            if($request->entidad2 != null){
                $entidad2 = $request->entidad2;
                $filtro_entidad2 = "ID in (select IDMULTA from inter24_tramites where COD_ACTOR2='".$entidad2."')";
                $filtros[] = $filtro_entidad2;
            }
            if($request->operacion != null){
                $operacion = $request->operacion;
                $filtro_operacion = "ID in (select IDMULTA from inter24_tramites where COD_OPER1='".$operacion."')";
                $filtros[] = $filtro_operacion;
            }
             if($request->texto_tramite != null){
                $texto_tramite = $request->texto_tramite;
                $filtro_texto_tramite = "ID in (select IDMULTA from inter24_tramites where TRAMITE='".$texto_tramite."')";
                $filtros[] = $filtro_texto_tramite;
            }

            for ($i =count($filtros) - 1; $i >= 0 ; $i--) { 
                $consulta .= $filtros[$i];
                $consulta .= " ";

                if($i > 0){
                    $consulta .= "AND ";
                }
            }
            //DB::enableQueryLog();

            $intervenciones = DB::table(DB::raw("inter24_interventions a, gesanc_paises b"))->selectRaw("a.ACUERDO,a.MATRICULA,a.FECHA,a.NumDossier,a.CLIENTE,b.Pais,a.IMPORTE,a.*")->whereRaw("a.PAIS=b.`Codigo Pais`")->orderby("FECHA","DESC")->limit(200)->get();



            
            if($consulta !=""){

              $intervenciones = DB::table(DB::raw("inter24_interventions a, gesanc_paises b"))->selectRaw("a.ACUERDO,a.MATRICULA,a.FECHA,a.NumDossier,a.CLIENTE,b.Pais,a.IMPORTE,a.*")->whereRaw("a.PAIS=b.`Codigo Pais` AND ".$consulta)->orderby("FECHA","DESC")->limit(200)->get();

             }
             //DB::enableQueryLog();
             //dd(DB::getQueryLog());

            
           if($intervenciones){
                foreach ($intervenciones as $key => $intervencion) {

                     $output.='<tr>'.
                            '<td>'.$intervencion->ACUERDO.'</td>'.
                            '<td>'.$intervencion->MATRICULA.'</td>'.
                            '<td>'.date('d - m - Y', strtotime($intervencion->FECHA)).'</td>'.
                            '<td>'.$intervencion->NumDossier.'</td>'.                           
                            '<td>'.$intervencion->CLIENTE.'</td>'.
                            '<td>'.$intervencion->Pais.'</td>'.
                            '<td><a class="btn btn-sm btn-success" href="'. url('intervenciones/'.$intervencion->ID).'"><i class="fa fa-edit"></i></a></td>'.
                            '</tr>';

                }

                
               
            }

            return Response($output);
             
    }
}


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $contratantes = DB::table("gesanc_empresas")->get();
        $comerciales = DB::table("gesanc_comerciales")->selectRaw("*,`Id-comercial` as 'idcomercial'")->get();
        $autoridades = DB::table("gesanc_autoridades")->get();
        $clientes = DB::table("its_contactos")->get();
        $actores = DB::table("inter24_actores")->get();
        $entidades = DB::table("inter24_nombre actor")->get();
        $operaciones = DB::table("inter24_operaciones")->get();
        $paises = DB::table("gesanc_paises")->selectRaw("*,`Codigo Pais` as 'codpais'")->get();


        return view('intervenciones/nuevo',['contratantes'=>$contratantes,'comerciales'=>$comerciales,'autoridades'=>$autoridades,'actores'=>$actores,'entidades'=>$entidades,'operaciones'=>$operaciones,'paises'=>$paises,'clientes'=>$clientes]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //TODO
        if($request->ajax()){

            $datos = array();
            parse_str($request->datos,$datos);

            $fecha_apertura=null;
            $hora_apertura=null;
            $fecha=null;
            $hora=null;
            $acuerdo=null;
            $dossier=null;
            $matricula=null;
            $cif=null;
            $cliente=null;
            $contratante=null;
            $comercial=null;
            $ordenante=null;
            $conductor=null;
            $lugar=null;
            $ciudad=null;
            $pais=null;
            $autoridad=null;
            $telefono=null;
            $asistido=null;
            $boletin=null;
            $fax=null;
            $fecharemision=null;
            $facturadoTAI=null;
            $ImporteFProv=null;
            $FacturaITS=null;
            $ImporteFITS=null;
            $importe=null;
            $gastos=null;
            $notas = null;
            $fc=0;
            $pq=0;
            $no_contratada=0;
            $comercialautoriz=0;



            }

            if(array_key_exists("fc",$datos)){
               $fc = 1;
            }

            if(array_key_exists("pq",$datos)){
               $pq = 1;
            }
            if(array_key_exists("no_contratada",$datos)){
               $no_contratada = 1;
            }

            if(array_key_exists("comercialautoriz",$datos)){
               $comercialautoriz = 1;
            }

            /*  if($datos["fecha_apertura"] !=null){
                $fecha_apertura = DateTime::createFromFormat('d/m/Y', $datos["fecha_apertura"]);
                date_time_set($fecha_apertura, 00, 00);
            }

             if($datos["hora_apertura"] !=null){
                $hora_apertura = DateTime::createFromFormat('H:i', $datos["hora_apertura"]);
                date_time_set($fecha_apertura, $hora_apertura, 00);
            }

            if($datos["fecha"] !=null){
                $fecha = DateTime::createFromFormat('d/m/Y', $datos["fecha"]);
                date_time_set($fecha, 00, 00);
            }

             if($datos["hora"] !=null){
                $hora = DateTime::createFromFormat('H:i', $datos["hora"]);
                date_time_set($fecha, $hora, 00);
            }

            if($datos["fecharemision"] !=null){
                $hora = DateTime::createFromFormat('d/m/Y', $datos["fecharemision"]);
                date_time_set($fecharemision, $hora, 00);
            }*/

            if($datos["hora_apertura"]!=null){
                $hora_apertura = $datos["hora_apertura"];
            }
             if($datos["hora"]!=null){
                $hora = $datos["hora"];
            }

            if($datos["fecha_apertura"] !=null){
                $fecha_apertura = DateTime::createFromFormat('d/m/Y', $datos["fecha_apertura"]);
                $horas = substr($hora_apertura,0,2);
                $minutos = substr($hora_apertura,-2);
                date_time_set($fecha_apertura, $horas, $minutos);
            }


            if($datos["fecha"] !=null){
                $fecha = DateTime::createFromFormat('d/m/Y', $datos["fecha"]);
                $horas = substr($hora,0,2);
                $minutos = substr($hora,-2);
                date_time_set($fecha, $horas, $minutos);
            }


            if($datos["acuerdo"]!=null){
                $acuerdo = $datos["acuerdo"];
            }

            if($datos["dossier"]!=null){
                $dossier = $datos["dossier"];
            }

            if($datos["matricula"]!=null){
                $matricula = $datos["matricula"];
            }

             if($datos["cif"]!=null){
                $cif = $datos["cif"];
            }

            if($datos["cliente"]!=null){
                $cliente = $datos["cliente"];
            }

            if($datos["contratante"]!=null){
                $contratante = $datos["contratante"];
            }

            if($datos["comercial"]!=null){
                $comercial = $datos["comercial"];
            }

            if($datos["ordenante"]!=null){
                $ordenante = $datos["ordenante"];
            }

            if($datos["conductor"]!=null){
                $conductor = $datos["conductor"];
            }
            if($datos["lugar"]!=null){
                $lugar = $datos["lugar"];
            }
            if($datos["ciudad"]!=null){
                $ciudad = $datos["ciudad"];
            }
            if($datos["pais"]!=null){
                $pais = $datos["pais"];
            }
            if($datos["autoridad"]!=null){
                $autoridad = $datos["autoridad"];
            }
            if($datos["telefono"]!=null){
                $telefono = $datos["telefono"];
            }
            if($datos["asistido"]!=null){
                $asistido = $datos["asistido"];
            }
            if($datos["boletin"]!=null){
                $boletin = $datos["boletin"];
            }
            if($datos["fax"]!=null){
                $fax = $datos["fax"];
            }
            if($datos["facturadoTAI"]!=null){
                $facturadoTAI = $datos["facturadoTAI"];
            }
            if($datos["ImporteFProv"]!=null){
                $ImporteFProv = $datos["ImporteFProv"];
            }
            if($datos["FacturaITS"]!=null){
                $FacturaITS = $datos["FacturaITS"];
            }
            if($datos["ImporteFITS"]!=null){
                $ImporteFITS = $datos["ImporteFITS"];
            }
            if($datos["importe"]!=null){
                $importe = $datos["importe"];
            }
            if($datos["gastos"]!=null){
                $gastos = $datos["gastos"];
            }
            if($datos["notas"]!=null){
                $notas = $datos["notas"];
            }


            try { 

                //Seleccionar las condiciones del contrato según los parametros establecidos



              $intervencion = DB::table('inter24_interventions')->insertGetId(
                ['ACUERDO'=>$acuerdo, 'matriculaNOcontrat_GESANC_ITS'=>$no_contratada, 'anuladaIntervencion'=>0, 'F_APERTURA'=>$fecha_apertura,'FECHA'=>$fecha_apertura, 'HORA'=>$fecha_apertura, 'NumDossier'=>$dossier, 'CLIENTE'=>$cliente, 'CIF'=>$cif, 'IdComercial'=>$comercial, 'EMPRESA'=>$contratante, 'DEMANDANTE'=>$ordenante, 'CHOFER'=>$conductor, 'CIUDAD'=>$ciudad, 'LUGAR'=>$lugar, 'PAIS'=>$pais, 'MATRICULA'=>$matricula, 'IMPORTE'=>$importe,'ENTRADAEC'=>$fecha_apertura, 'pendienteQuittance'=>$pq, 'comprobado_MOTIVO'=>0, 'comprobado_IMPORTE'=>0, 'impago_multas'=>0, 'FacturadoTAI'=>$facturadoTAI, 'ImporteFProv'=>$ImporteFProv, 'Autoridad'=>$autoridad, 'NumBoletin'=>$boletin, 'TelefAutoridad'=>$telefono, 'FaxAutoridad'=>$fax, 'Notas'=>$notas, 'Cargado'=>0,'Gastos'=>$gastos, 'fecharemision'=>$fecharemision, 'emitido'=>1, 'FacturaITS'=>$FacturaITS, 'ImporteFITS'=>$ImporteFITS, 'FaxConf'=>$fc, 'marca_modif'=>0, 'Fecha_actualizacion'=>$fecha]);

            return  Response($intervencion);

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {


        $intervencion = DB::table("inter24_interventions")->where("ID","=",$id)->first();
        $tramites = DB::table("inter24_tramites")->where("IDMULTA","=",$id)->get();
        $contratantes = DB::table("gesanc_empresas")->get();
        $comerciales = DB::table("gesanc_comerciales")->selectRaw("*,`Id-comercial` as 'idcomercial'")->get();
        $autoridades = DB::table("gesanc_autoridades")->get();
        $actores = DB::table("inter24_actores")->get();
        $entidades = DB::table("inter24_nombre actor")->get();
        $operaciones = DB::table("inter24_operaciones")->get();
        $paises = DB::table("gesanc_paises")->selectRaw("*,`Codigo Pais` as 'codpais'")->get();
        $contrato = DB::table("its_contratos")->selectRaw("*,`Fecha Contrato` as 'FechaContrato'")->whereRaw("IDContrato in (select IdContrato from its_anexos where Matricula='".$intervencion->MATRICULA."')")->first();

        $hora_apertura = DB::table("inter24_interventions")->selectRaw("HOUR(HORA)")->where("ID","=",$id)->first();


        $matricula = DB::table("its_matriculas")->where("Matricula","=",$intervencion->MATRICULA)->first();

        $cliente = DB::table("its_contactos")->where("CIF",'=',$intervencion->CIF)->first();

        $observaciones_cliente = DB::table("gesanc_clientes")->where("cif",'=',$intervencion->CIF)->first();

        $contratada_its =  DB::table("its_contratos")->whereRaw("IDContrato in (select IdContrato from its_anexos where Matricula='".$intervencion->MATRICULA."' and Cobertura='H')")->get();
        $contratada_gesanc =  DB::table("gesanc_contratos")->whereRaw("Matricula='".$intervencion->MATRICULA."'")->get();

        $de_alta = DB::table('its_contratos')->selectRaw("count(*) as 'cantidad'")->whereRaw("Contratado = '".$intervencion->CIF."' and `Fecha Contrato` <= DATE(CURDATE()) and (Baja > DATE(CURDATE()) OR Baja is null) and Vencimiento > date(curdate())")->first();

        $no_contratada = 0;
        $pedir_autorizacion = 0;
        $cliente_importante = 0;
        $avisar_comercial = 0;
        $tarjetas_activas = DB::table("its_tarjetas")->where("cif","=",$intervencion->CIF)->get();
        $avisar_empresa = 0;
        $cobertura_francia = DB::table("its_contratos")->whereRaw("IDContrato in (select IdContrato from its_anexos where Matricula='".$intervencion->MATRICULA."' and idAnexo in(select IdAnexo from `its_ambito territorial` where Pais='F'))")->first();

        $en_renovacion = 0;
        $renovacion = $fecha = date_create($contrato->Vencimiento);
        date_add($renovacion, date_interval_create_from_date_string('30 days'));

        if( $contrato!=null && $contrato->Vencimiento >= $renovacion){
            $en_renovacion = 1;
        }

        if($contratada_gesanc == null && $contratada_its == null){
            $no_contratada = 1;
        }

        if($matricula!=null){
                 if($intervencion->PAIS =='E' || $matricula->Pais == $intervencion->PAIS){
            $pedir_autorizacion = 1;
        }
        }
   
        if($de_alta->cantidad > 4){
            $cliente_importante = 1;
        }

        if($intervencion->CIF == 'B04213831' || $intervencion->CLIENTE =='DOLORES AGUILA PARRA SL'){
            $avisar_comercial = 1;
        }
        if($intervencion->CLIENTE=='TERESA_TRANSPAEZ_CORBALAN' || $intervencion->CIF=='74337132M' || $intervencion->CIF == 'B73488173'){
            $avisar_empresa = 1;
        }


        return view('intervenciones/detail',['intervencion'=>$intervencion,'tramites'=> $tramites,'contratantes'=>$contratantes,'comerciales'=>$comerciales,'paises'=>$paises,'autoridades'=>$autoridades,'no_contratada'=>$no_contratada,'pedir_autorizacion'=>$pedir_autorizacion,'cliente_importante'=>$cliente_importante,'avisar_comercial'=>$avisar_comercial,'tarjetas_activas'=>$tarjetas_activas,'avisar_empresa'=>$avisar_empresa,'cobertura_francia'=>$cobertura_francia,'cliente'=>$cliente,'contrato'=>$contrato,'en_renovacion'=>$en_renovacion,'observaciones_cliente'=>$observaciones_cliente]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         //TODO
        if($request->ajax()){

            $datos = array();
            parse_str($request->datos,$datos);

            $fecha_apertura=null;
            $hora_apertura=null;
            $fecha=null;
            $hora=null;
            $acuerdo=null;
            $dossier=null;
            $matricula=null;
            $cif=null;
            $cliente=null;
            $contratante=null;
            $comercial=null;
            $ordenante=null;
            $conductor=null;
            $lugar=null;
            $ciudad=null;
            $pais=null;
            $autoridad=null;
            $telefono=null;
            $asistido=null;
            $boletin=null;
            $fax=null;
            $fecharemision=null;
            $facturadoTAI=null;
            $ImporteFProv=null;
            $FacturaITS=null;
            $ImporteFITS=null;
            $importe=null;
            $gastos=null;
            $notas = null;
            $fc=0;
            $pq=0;
            $no_contratada=0;
            $comercialautoriz=0;



            }

            if(array_key_exists("fc",$datos)){
               $fc = 1;
            }

            if(array_key_exists("pq",$datos)){
               $pq = 1;
            }
            if(array_key_exists("no_contratada",$datos)){
               $no_contratada = 1;
            }

            if(array_key_exists("comercialautoriz",$datos)){
               $comercialautoriz = 1;
            }

            /*  if($datos["fecha_apertura"] !=null){
                $fecha_apertura = DateTime::createFromFormat('d/m/Y', $datos["fecha_apertura"]);
                date_time_set($fecha_apertura, 00, 00);
            }

             if($datos["hora_apertura"] !=null){
                $hora_apertura = DateTime::createFromFormat('H:i', $datos["hora_apertura"]);
                date_time_set($fecha_apertura, $hora_apertura, 00);
            }

            if($datos["fecha"] !=null){
                $fecha = DateTime::createFromFormat('d/m/Y', $datos["fecha"]);
                date_time_set($fecha, 00, 00);
            }

             if($datos["hora"] !=null){
                $hora = DateTime::createFromFormat('H:i', $datos["hora"]);
                date_time_set($fecha, $hora, 00);
            }

            if($datos["fecharemision"] !=null){
                $hora = DateTime::createFromFormat('d/m/Y', $datos["fecharemision"]);
                date_time_set($fecharemision, $hora, 00);
            }*/

            if($datos["hora_apertura"]!=null){
                $hora_apertura = $datos["hora_apertura"];
            }
             if($datos["hora"]!=null){
                $hora = $datos["hora"];
            }

            if($datos["fecha_apertura"] !=null){
                $fecha_apertura = DateTime::createFromFormat('d/m/Y', $datos["fecha_apertura"]);
                $horas = substr($hora_apertura,0,2);
                $minutos = substr($hora_apertura,-2);
                date_time_set($fecha_apertura, $horas, $minutos);
            }


            if($datos["fecha"] !=null){
                $fecha = DateTime::createFromFormat('d/m/Y', $datos["fecha"]);
                $horas = substr($hora,0,2);
                $minutos = substr($hora,-2);
                date_time_set($fecha, $horas, $minutos);
            }


            if($datos["acuerdo"]!=null){
                $acuerdo = $datos["acuerdo"];
            }

            if($datos["dossier"]!=null){
                $dossier = $datos["dossier"];
            }

            if($datos["matricula"]!=null){
                $matricula = $datos["matricula"];
            }

             if($datos["cif"]!=null){
                $cif = $datos["cif"];
            }

            if($datos["cliente"]!=null){
                $cliente = $datos["cliente"];
            }

            if($datos["contratante"]!=null){
                $contratante = $datos["contratante"];
            }

            if($datos["comercial"]!=null){
                $comercial = $datos["comercial"];
            }

            if($datos["ordenante"]!=null){
                $ordenante = $datos["ordenante"];
            }

            if($datos["conductor"]!=null){
                $conductor = $datos["conductor"];
            }
            if($datos["lugar"]!=null){
                $lugar = $datos["lugar"];
            }
            if($datos["ciudad"]!=null){
                $ciudad = $datos["ciudad"];
            }
            if($datos["pais"]!=null){
                $pais = $datos["pais"];
            }
            if($datos["autoridad"]!=null){
                $autoridad = $datos["autoridad"];
            }
            if($datos["telefono"]!=null){
                $telefono = $datos["telefono"];
            }
            if($datos["asistido"]!=null){
                $asistido = $datos["asistido"];
            }
            if($datos["boletin"]!=null){
                $boletin = $datos["boletin"];
            }
            if($datos["fax"]!=null){
                $fax = $datos["fax"];
            }
            if($datos["facturadoTAI"]!=null){
                $facturadoTAI = $datos["facturadoTAI"];
            }
            if($datos["ImporteFProv"]!=null){
                $ImporteFProv = $datos["ImporteFProv"];
            }
            if($datos["FacturaITS"]!=null){
                $FacturaITS = $datos["FacturaITS"];
            }
            if($datos["ImporteFITS"]!=null){
                $ImporteFITS = $datos["ImporteFITS"];
            }
            if($datos["importe"]!=null){
                $importe = $datos["importe"];
            }
            if($datos["gastos"]!=null){
                $gastos = $datos["gastos"];
            }
            if($datos["notas"]!=null){
                $notas = $datos["notas"];
            }


            try { 

                //Seleccionar las condiciones del contrato según los parametros establecidos



              DB::table('inter24_interventions')->where("ID","=",$id)->update(
                ['ACUERDO'=>$acuerdo, 'matriculaNOcontrat_GESANC_ITS'=>$no_contratada, 'anuladaIntervencion'=>0, 'F_APERTURA'=>$fecha_apertura,'FECHA'=>$fecha_apertura, 'HORA'=>$fecha_apertura, 'NumDossier'=>$dossier, 'CLIENTE'=>$cliente, 'CIF'=>$cif, 'IdComercial'=>$comercial, 'EMPRESA'=>$contratante, 'DEMANDANTE'=>$ordenante, 'CHOFER'=>$conductor, 'CIUDAD'=>$ciudad, 'LUGAR'=>$lugar, 'PAIS'=>$pais, 'MATRICULA'=>$matricula, 'IMPORTE'=>$importe,'ENTRADAEC'=>$fecha_apertura, 'pendienteQuittance'=>$pq, 'comprobado_MOTIVO'=>0, 'comprobado_IMPORTE'=>0, 'impago_multas'=>0, 'FacturadoTAI'=>$facturadoTAI, 'ImporteFProv'=>$ImporteFProv, 'Autoridad'=>$autoridad, 'NumBoletin'=>$boletin, 'TelefAutoridad'=>$telefono, 'FaxAutoridad'=>$fax, 'Notas'=>$notas, 'Cargado'=>0,'Gastos'=>$gastos, 'fecharemision'=>$fecharemision, 'emitido'=>1, 'FacturaITS'=>$FacturaITS, 'ImporteFITS'=>$ImporteFITS, 'FaxConf'=>$fc, 'marca_modif'=>0, 'Fecha_actualizacion'=>$fecha]);

            return  Response("La intervención se ha modificado correctamente");

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function motivos(Request $request)
    {

          if($request->ajax()){

            $id = $request->id; 

            $intervencion = DB::table("inter24_interventions")->where("ID",'=',$id)->first();

            
             $output = "<table class='table' id='table_motivos'>
                        <thead class='table-header'>
                            <th>Caución</th>                        
                            <th>Estado</th>
                            <th>Motivo</th>
                            <th>Importe</th>
                            <th>Divisa</th>
                            <th>Cant</th>
                            <th>NumTarjeta</th>
                            <th>Asistido</th>
                            <th>Detalles</th>
                            <th></th>
                            <th style='display:none'></th>
                        </thead>
                        <tbody id='myTable'>";
            $motivos = DB::table(DB::raw("`inter24_motivos por multa` a, inter24_motivos b"))->select(DB::raw("a.*"))->whereRaw("a.IdMotivo = b.COD_MOTIVO and a.ACUERDO ='".$intervencion->ACUERDO."'")->get();

            $lista_motivos = DB::table("inter24_motivos")->get();

            if($motivos->count()){

               

                foreach ($motivos as $key => $motivo) {

                     $output.='<tr>';
                     if($motivo->caucionPagada==1){
                        $output.= '<td><input type="checkbox" class="caucion" checked></td>';
                    }else{
                        $output.= '<td><input type="checkbox" class="caucion"></td>';
                    }

                        $output.= '<td><input type="hidden" class="idmotivo" value="'.$motivo->idmotivomulta.'"><select class="selectpicker estado" data-container="body" data-live-search="true" title="Buscar..."  name="estado">';
                        $output.= '<option value="'.$motivo->estado_caucion.'" selected>'.$motivo->estado_caucion.'</option>';
                        $output.= '<option value="">En Blanco</option>
                         <option value="S">Sobreseida</option>
                         <option value="R">Reducida</option>
                         <option value="A">Anulada</option>
                         <option value="P">Pagada</option>
                         <option value="C">Caducada</option>
                        </select></td>';
                        $output.= '<td><select class="selectpicker motivo" data-container="body" data-live-search="true" title="Buscar..." name="estado">';
                        foreach ($lista_motivos as $key => $dato) {
                            if($motivo->IdMotivo == $dato->COD_MOTIVO){
                        $output.= '<option value="'.$dato->COD_MOTIVO.'" selected>'.$dato->COD_MOTIVO.'-'.$dato->MOTIVO.'</option>';
                            }else{
                                $output.= '<option value="'.$dato->COD_MOTIVO.'">'.$dato->COD_MOTIVO.'-'.$dato->MOTIVO.'</option>';
                            }
                        }
                        $output.= '</select></td>';
                         $output.= '<td><input type="number" class="form-control importe" value="'.$motivo->Importe.'"></td>'.
                         '<td><input type="text" class="form-control divisa" value="'.$motivo->Divisa.'"></td>'.
                         '<td><input type="number" class="form-control cantidad" value="'.$motivo->Cantidad.'"></td>'.
                         '<td><input type="number" class="form-control numTarjeta" value="'.$motivo->numTarjeta.'"></td>'.
                         '<td><input type="number" class="form-control asistido" value="'.$motivo->Asistido.'"></td>'.
                         '<td><input type="number" class="form-control detalles" value="'.$motivo->Detalles.'"></td>'.
                                  '<td><a class="btn btn-sm btn-success btn-edit editar_motivo" href="#"><i class="fa fa-edit"></i></a><a class="btn btn-sm btn-danger eliminar_motivo"><i class="fa fa-trash"></i></a></td>'.
                        '</tr>';

                }


                
            }
            $output.='<tr>';
            $output.= '<td><input type="checkbox" class="caucion"></td>';
            $output.= '<td><select class="selectpicker estado" data-container="body" data-live-search="true" title="Buscar..."  name="estado">';
            $output.= '<option value="">En Blanco</option>
                         <option value="S">Sobreseida</option>
                         <option value="R">Reducida</option>
                         <option value="A">Anulada</option>
                         <option value="P">Pagada</option>
                         <option value="C">Caducada</option>
                        </select></td>';
                        $output.= '<td><select class="selectpicker motivo" data-container="body" data-live-search="true" title="Buscar..."  name="estado">';
                        foreach ($lista_motivos as $key => $dato) {
                        $output.= '<option value="'.$dato->COD_MOTIVO.'">'.$dato->COD_MOTIVO.'-'.$dato->MOTIVO.'</option>';
                        }
                        $output.= '</select></td>';
                         $output.= '<td><input type="number" class="form-control importe" value=""></td>'.
                         '<td><input type="text" class="form-control divisa" value=""></td>'.
                         '<td><input type="number" class="form-control cantidad" value=""></td>'.
                         '<td><input type="text" class="form-control numTarjeta" value=""></td>'.
                         '<td><input type="text" class="form-control asistido" value=""></td>'.
                         '<td><input type="text" class="form-control detalles" value=""></td>'.
                                  '<td><a class="btn btn-success add_motivo" id="add_motivo"><i class="fa fa-plus"></i></a></td>'.
                                  '</tr>';


            $output .= "</tbody></table><input type='hidden' id='tabla_tipo' value='motivo'>";
            return Response($output);
        }
    }

     public function tramites(Request $request)
    {

          if($request->ajax()){

            $id = $request->id; 

            $intervencion = DB::table("inter24_interventions")->where("ID",'=',$id)->first();

            
             $output = "<table class='table'>
                        <thead class='table-header'>
                            <th>Fecha</th>                        
                            <th>Hora</th>
                            <th>Trámite</th>
                            <th></th>
                        </thead>
                        <tbody id='myTable'>";


             $tramites =  DB::table("inter24_tramites")->whereRaw("ACUERDO='".$intervencion->ACUERDO."'")->get();

             $select_tramite = DB::table('gesanc_tramites')->get();

            if($tramites->count()){

                foreach ($tramites as $key => $tramite) {

                     $output.='<tr>'.
                              '<td>'.date('d/m/Y', strtotime($tramite->FECHA)).'</td>';
            
                    $output.='<td>'.date('H:i', strtotime($tramite->HORA)).'</td>';

                    $output.='<td>'.$tramite->TRAMITE.'</td>';

                    $output.='</tr>';

                }

            }

            $output .= "</tbody></table><input type='hidden' id='tabla_tipo' value='tramites'>";
            return Response($output);
        }
    }

    public function add_tramite(Request $request){
         if($request->ajax()){

            $personas = DB::table("inter24_nombre actor")->get();
            $entidades = DB::table("inter24_actores")->get();
            $operaciones = DB::table("inter24_operaciones")->get();

             $output = ' <div class="row"><div class="col-md-12">
             <div class="form-group col-md-4"><label>Fecha</label>
             <input type="text" id="fecha_del_tramite" class="form-control datepicker" value=""></div>';

             $output.=' <div class="form-group col-md-4"><label>Hora</label>
             <input type="text" id="hora_tramite" class="form-control" value=""></div>';

             $output.=' <div class="form-group col-md-2"><label>Ocultar</label>
             <input type="checkbox" id="ocultar"></div>';

             $output.='</div>';

            $output .= '<div class="col-md-12">';
            $output.= '<div class="form-group col-md-4"><label>Persona 1</label><select class="selectpicker" data-container="body" data-live-search="true" title="Buscar..." id="persona1" name="estado">';
            foreach ($personas as $key => $persona) {
                $output.= '<option value="'.$persona->IdNombre.'">'.$persona->Nombre.'</option>';
            }
                        $output.= '</select></div>';
            $output.= '<div class="form-group col-md-4"><label>Entidad 1</label><select class="selectpicker" data-container="body" data-live-search="true" title="Buscar..." id="entidad1" name="estado">';
            foreach ($entidades as $key => $entidad) {
                $output.= '<option value="'.$entidad->IdActor.'">'.$entidad->Actor.'</option>';
            }
                        $output.= '</select></div>';
            $output.= '<div class="form-group col-md-4"><label>Operacion</label><select class="selectpicker" data-container="body" data-live-search="true" title="Buscar..." id="operacion" name="estado">';
            foreach ($operaciones as $key => $operacion) {
                $output.= '<option value="'.$operacion->IdOperacion.'">'.$operacion->Operacion.'</option>';
            }
                        $output.= '</select></div>';
             $output.= '<div class="form-group col-md-4"><label>Persona 2</label><select class="selectpicker" data-container="body" data-live-search="true" title="Buscar..." id="persona2" name="estado">';
            foreach ($personas as $key => $persona) {
                $output.= '<option value="'.$persona->IdNombre.'">'.$persona->Nombre.'</option>';
            }
                        $output.= '</select></div>';
            $output.= '<div class="form-group col-md-4"><label>Entidad 2</label><select class="selectpicker" data-container="body" data-live-search="true" title="Buscar..." id="entidad2" name="estado">';
            foreach ($entidades as $key => $entidad) {
                $output.= '<option value="'.$entidad->IdActor.'">'.$entidad->Actor.'</option>';
            }
                        $output.= '</select></div>';

            $output.=' <div class="form-group col-md-5"><label>Trámite</label>
             <textarea class="form-control" rows="4" id="notas_tramite"></textarea></div>';

            $output.='</div>';

            return Response($output);

         }
    }

    public function insertar_tramite(Request $request){

        if($request->ajax()){
            $fecha = null;
            $hora = null;
            $persona1 = null;
            $entidad1 = null;
            $operacion = null;
            $persona2 = null;
            $entidad2 = null;
            $acuerdo = null;
            $id = null;
            $ocultar = null;
            $notas_tramite = null;


             if($request->hora !=null){
                $hora = $request->hora;
            }

            if($request->fecha !=null){
                $fecha = DateTime::createFromFormat('d/m/Y', $request->fecha);
                $horas = substr($hora,0,2);
                $minutos = substr($hora,-2);
                date_time_set($fecha, $horas, $minutos);
            }


            if($request->persona1 !=null){
                $persona1 = $request->persona1;
            }

            if($request->entidad1 !=null){
                $entidad1 = $request->entidad1;
            }

            if($request->operacion !=null){
                $operacion = $request->operacion;
            }

             if($request->persona2 !=null){
                $persona2 = $request->persona2;
            }

            if($request->entidad2!=null){
                $entidad2 = $request->entidad2;
            }

            if($request->acuerdo !=null){
                $acuerdo = $request->acuerdo;
            }

            if($request->id !=null){
                $id = $request->id;
            }

            if($request->ocultar !=null){
                $ocultar = $request->ocultar;
            }

            if($request->notas_tramite!=null){
                $notas_tramite = $request->notas_tramite;
            }

        }

        try { 

                //Seleccionar las condiciones del contrato según los parametros establecidos


              DB::table('inter24_tramites')->insert(
                ['ACUERDO'=>$acuerdo, 'IDMULTA'=>$id,'FECHA'=>$fecha,'HORA'=>$fecha, 'COD_ACTOR1'=>$entidad1, 'COD_PERSONA1'=>$persona1, 'COD_OPER1'=>$operacion, 'COD_ACTOR2'=>$entidad2, 'COD_PERSONA2'=>$persona2, 'TRAMITE'=>$notas_tramite, 'Ocultar'=>$ocultar, 'marca_Modif'=>0, 'Fecha_actualizacion'=>date('Y-m-d H:i:s')]);


            return  Response("El trámite se ha registrado correctamente");

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }



    }

    public function recibos(Request $request)
    {

          if($request->ajax()){

            $id = $request->id; 

            $intervencion = DB::table("inter24_interventions")->where("ID",'=',$id)->first();

            
             $output = "<table class='table' id='table_motivos'>
                        <thead class='table-header'>
                            <th>Acuerdo</th>                        
                            <th>Vencimiento</th>
                            <th>Corr. Pago</th>
                            <th>Importe</th>
                            <th>Forma Pago</th>
                            <th>Estado</th>
                            <th>Notas</th>
                            <th></th>
                            <th style='display:none'></th>
                        </thead>
                        <tbody id='myTable'>";
           
           $recibos = DB::table("inter24_recibos")->where("acuerdo","=",$intervencion->ACUERDO)->get();

           $estados_recibo = DB::table("inter24_tipo_estadorecibo")->get();

           $formas_pago = DB::table("inter24_tipo_formapago")->get();

            if($recibos->count()){


                foreach ($recibos as $key => $recibo) {

                     $output.='<tr>';
                        $output.= '<td><input type="hidden" class="codrecibo" value="'.$recibo->codRecibo.'"><input type="text" class="form-control acuerdo" value="'.$recibo->acuerdo.'"></td>';
                        $output.= '<td><input type="text" class="form-control datepicker vencimiento" value="'.date('d/m/Y', strtotime($recibo->vencimiento)).'"></td>';
                        $output.= '<td><input type="number" class="form-control correspondePagoCte" value="'.$recibo->correspondePagoCte.'"></td>';
                        $output.= '<td><input type="number" class="form-control importe" value="'.$recibo->importe.'"></td>';
                        $output.= '<td><select class="selectpicker forma_pago" data-container="body" data-live-search="true" title="Buscar..."  name="forma_pago">';
                                    foreach ($formas_pago as $key => $forma) {
                                        if($recibo->codFormaPago == $forma->CodFormaPago ){
                                        $output.= '<option value="'.$forma->CodFormaPago.'" selected>'.$forma->FormaPago.'</option>';
                                        }else{
                                        $output.= '<option value="'.$forma->CodFormaPago.'">'.$forma->FormaPago.'</option>';
                                        }
                                    }
                        $output.= '</select></td>';
                        $output.= '<td><select class="selectpicker estados_recibo" data-container="body" data-live-search="true" title="Buscar..." name="estados_recibo">';
                                    foreach ($estados_recibo as $key => $estado) {
                                        if($recibo->codEstado == $estado->codEstado ){
                                        $output.= '<option value="'.$estado->codEstado.'" selected>'.$estado->estado.'</option>';
                                        }else{
                                        $output.= '<option value="'.$estado->codEstado.'">'.$estado->estado.'</option>';
                                        }
                                    }
                        $output.= '</select></td>';
                         $output.= '<td><input type="text" class="form-control notas" value="'.$recibo->notas.'"></td>'.
                                  '<td><a class="btn btn-sm btn-success btn-edit editar_recibo" href="#"><i class="fa fa-edit"></i></a><a class="btn btn-sm btn-danger eliminar_recibo"><i class="fa fa-trash"></i></a></td>'.
                                  '</tr>';

                }


                
            }
           $output.='<tr>';
                        $output.= '<td><input type="text" class="form-control acuerdo" value"></td>';
                        $output.= '<td><input type="text" class="form-control datepicker vencimiento" value=""></td>';
                        $output.= '<td><input type="number" class="form-control correspondePagoCte" value=""></td>';
                        $output.= '<td><input type="number" class="form-control importe" value=""></td>';
                        $output.= '<td><select class="selectpicker" data-container="body" data-live-search="true" title="Buscar..." class="forma_pago" name="forma_pago">';
                                    foreach ($formas_pago as $key => $forma) {
                                        $output.= '<option value="'.$forma->CodFormaPago.'">'.$forma->FormaPago.'</option>';
                                    }
                        $output.= '</select></td>';
                        $output.= '<td><select class="selectpicker" data-container="body" data-live-search="true" title="Buscar..." class="estados_recibo" name="estados_recibo">';
                                    foreach ($estados_recibo as $key => $estado) {
                                        $output.= '<option value="'.$estado->codEstado.'">'.$estado->estado.'</option>';
                                    }
                        $output.= '</select></td>';
                         $output.= '<td><input type="text" class="form-control notas" value=""></td>'.
                                  '<td><a class="btn btn-success" id="add_recibo"><i class="fa fa-plus"></i></a></td>'.
                                  '</tr>';


            $output .= "</tbody></table><input type='hidden' id='tabla_tipo' value='motivo'>";
            return Response($output);
        }
    }

    public function editar_motivo(Request $request){
          if($request->ajax()){ 

        $id = null;
        $caucion = null;
        $estado = null;
        $motivo = null;
        $importe = null;
        $divisa = null;
        $cantidad = null;
        $numTarjeta = null;
        $asistido =null;
        $detalles = null;
        $acuerdo = null; 

         if($request->estado !=null){
                $estado = $request->estado;
            }
              if($request->id !=null){
                $id = $request->id;
            }
            if($request->caucion !=null){
                $caucion = $request->caucion;
            }
         if($request->motivo !=null){
                $motivo = $request->motivo;
            }
         if($request->importe !=null){
                $importe = $request->importe;
            }
         if($request->divisa !=null){
                $divisa = $request->divisa;
            }
         if($request->cantidad !=null){
                $cantidad = $request->cantidad;
            }
         if($request->numTarjeta !=null){
                $numTarjeta = $request->numTarjeta;
            }
         if($request->asistido !=null){
                $asistido = $request->asistido;
            }
         if($request->detalles !=null){
                $detalles = $request->detalles;
            }
        if($request->acuerdo !=null){
                $acuerdo = $request->acuerdo;
            }


          }

               try { 

                //Seleccionar las condiciones del contrato según los parametros establecidos


              DB::table('inter24_motivos por multa')->where("idmotivomulta","=",$id)->update(
                ['ACUERDO'=>$acuerdo, 'ID'=>$id, 'IdMotivo'=>$motivo, 'Importe'=>$importe, 'Cantidad'=>$cantidad, 'Divisa'=>$divisa, 'Detalles'=>$detalles, 'Asistido'=>$asistido, 'marca_modif'=>0, 'Fecha_actualizacion'=>date('Y-m-d H:i:s'), 'numTarjeta'=>$numTarjeta, 'caucionPagada'=>$caucion, 'estado_caucion'=>$caucion]);


            return  Response("El motivo se ha modificado correctamente");

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }
    
    }

    public function add_motivo(Request $request){
          if($request->ajax()){ 

        $caucion = null;
        $estado = null;
        $motivo = null;
        $importe = null;
        $divisa = null;
        $cantidad = null;
        $numTarjeta = null;
        $asistido =null;
        $detalles = null;
        $acuerdo = null; 

         if($request->estado !=null){
                $estado = $request->estado;
            }
        
            if($request->caucion !=null){
                $caucion = $request->caucion;
            }
         if($request->motivo !=null){
                $motivo = $request->motivo;
            }
         if($request->importe !=null){
                $importe = $request->importe;
            }
         if($request->divisa !=null){
                $divisa = $request->divisa;
            }
         if($request->cantidad !=null){
                $cantidad = $request->cantidad;
            }
         if($request->numTarjeta !=null){
                $numTarjeta = $request->numTarjeta;
            }
         if($request->asistido !=null){
                $asistido = $request->asistido;
            }
         if($request->detalles !=null){
                $detalles = $request->detalles;
            }
        if($request->acuerdo !=null){
                $acuerdo = $request->acuerdo;
            }


          }

               try { 

                //Seleccionar las condiciones del contrato según los parametros establecidos


              DB::table('inter24_motivos por multa')->insertGetId(
                ['ACUERDO'=>$acuerdo,'IdMotivo'=>$motivo, 'Importe'=>$importe, 'Cantidad'=>$cantidad, 'Divisa'=>$divisa, 'Detalles'=>$detalles, 'Asistido'=>$asistido, 'marca_modif'=>0, 'Fecha_actualizacion'=>date('Y-m-d H:i:s'), 'numTarjeta'=>$numTarjeta, 'caucionPagada'=>$caucion, 'estado_caucion'=>$caucion]);


            return  Response("El motivo se ha añadido correctamente");

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }



    
    }

    public function eliminar_motivo(Request $request){
          if($request->ajax()){ 

               try { 

                //Seleccionar las condiciones del contrato según los parametros establecidos


              DB::table('inter24_motivos por multa')->where("idmotivomulta","=",$request->id)->delete();


            return  Response("El motivo se ha eliminado correctamente");

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }



    
    }
}

 public function editar_recibo(Request $request){
          if($request->ajax()){ 

        $vencimiento = null;
        $correspondePagoCte = null;
        $importe = null;
        $forma_pago = null;
        $estados_recibo = null;
        $notas = null;
        $codrecibo = null;
        $acuerdo = null;


         if($request->vencimiento !=null){
                $vencimiento = DateTime::createFromFormat('d/m/Y', $request->vencimiento);
                 date_time_set($vencimiento, 00, 00);
            }

        if($request->id !=null){
                $id = $request->id;
            }
            if($request->correspondePagoCte !=null){
                $correspondePagoCte = $request->correspondePagoCte;
            }
         if($request->importe !=null){
                $importe = $request->importe;
            }
         if($request->forma_pago !=null){
                $forma_pago = $request->forma_pago;
            }
         if($request->estados_recibo !=null){
                $estados_recibo = $request->estados_recibo;
            }
         if($request->notas !=null){
                $notas = $request->notas;
            }
         if($request->codrecibo !=null){
                $codrecibo = $request->codrecibo;
            }
        if($request->acuerdo !=null){
                $acuerdo = $request->acuerdo;
            }


          }

               try { 

                //Seleccionar las condiciones del contrato según los parametros establecidos


              DB::table('inter24_recibos')->where("codRecibo","=",$codrecibo)->update(
                ['acuerdo'=>$acuerdo, 'vencimiento'=>$vencimiento, 'correspondePagoCte'=>$correspondePagoCte, 'codFormaPago'=>$forma_pago, 'codEstado'=>$estados_recibo, 'notas'=>$notas, 'importe'=>$importe]);


            return  Response("El recibo se ha modificado correctamente");

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }
    
    }

    public function add_recibo(Request $request){
          if($request->ajax()){ 

        $vencimiento = null;
        $correspondePagoCte = null;
        $importe = null;
        $forma_pago = null;
        $estados_recibo = null;
        $notas = null;
        $acuerdo = null;


         if($request->vencimiento !=null){
                $vencimiento = DateTime::createFromFormat('d/m/Y', $request->vencimiento);
                 date_time_set($vencimiento, 00, 00);
            }

        if($request->id !=null){
                $id = $request->id;
            }
            if($request->correspondePagoCte !=null){
                $correspondePagoCte = $request->correspondePagoCte;
            }
         if($request->importe !=null){
                $importe = $request->importe;
            }
         if($request->forma_pago !=null){
                $forma_pago = $request->forma_pago;
            }
         if($request->estados_recibo !=null){
                $estados_recibo = $request->estados_recibo;
            }
         if($request->notas !=null){
                $notas = $request->notas;
            }
        if($request->acuerdo !=null){
                $acuerdo = $request->acuerdo;
            }


          }

               try { 

                //Seleccionar las condiciones del contrato según los parametros establecidos


              DB::table('inter24_recibos')->insertGetId(
                ['acuerdo'=>$acuerdo, 'vencimiento'=>$vencimiento, 'correspondePagoCte'=>$correspondePagoCte, 'codFormaPago'=>$forma_pago, 'codEstado'=>$estados_recibo, 'notas'=>$notas, 'importe'=>$importe]);


            return  Response("El recibo se ha agregado correctamente");

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }
    
    }

    public function eliminar_recibo(Request $request){
          if($request->ajax()){ 

               try { 

                //Seleccionar las condiciones del contrato según los parametros establecidos


              DB::table('inter24_recibos')->where("codRecibo","=",$request->codrecibo)->delete();


            return  Response("El recibo se ha eliminado correctamente");

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }



    
    }
}

public function check_contrato(Request $request){

    if($request->ajax()){ 

        $res = [];
        $contrato = null;

               try { 

                //Seleccionar las condiciones del contrato según los parametros establecidos


              $contrato = DB::table("its_contratos")->selectRaw("*,`Fecha Contrato` as 'FechaContrato'")->whereRaw("IDContrato in (select IdContrato from its_anexos where Matricula='".$request->matricula."')")->first();

              $matricula = DB::table("its_matriculas")->where("Matricula","=",$request->matricula)->first();
              $cobertura_francia = DB::table("its_contratos")->whereRaw("IDContrato in (select IdContrato from its_anexos where Matricula='".$request->matricula."' and idAnexo in(select IdAnexo from `its_ambito territorial` where Pais='F'))")->first();


            $no_contratada = 0;


              $contratada_its =  DB::table("its_contratos")->whereRaw("IDContrato in (select IdContrato from its_anexos where Matricula='".$request->matricula."' and Cobertura='H')")->get();
              $contratada_gesanc =  DB::table("gesanc_contratos")->whereRaw("Matricula='".$request->matricula."'")->get();
     

             if($contratada_gesanc == null && $contratada_its == null){
            $no_contratada = 1;
            }
            if($contrato !=null){
                if($no_contratada == 0 && $contrato->Baja == null && $contrato->TipoContrato == 'AJT'){
                    $res[0] = ["Contrato de Multas, Alta ".date('d/m/Y', strtotime($contrato->FechaContrato)).", Baja ".date('d/m/Y', strtotime($contrato->Vencimiento)).""];
                }     
            elseif($no_contratada == 0 && $contrato->Baja !=null && $contrato->TipoContrato == 'AJT'){
                    $res[1] = ["Contrato de Multas, Alta ".date('d/m/Y', strtotime($contrato->FechaContrato)).", Baja ".date('d/m/Y', strtotime($contrato->Vencimiento)).""];
                }
            elseif($no_contratada == 1){
                    $res[2] = ["No existe ningún contrato asociado a esta matrícula"];
                }
                            
            }

            if($cobertura_francia!=null){
                $res[3]=1;
            }

             if($matricula!=null){
                 if($matricula->Pais == 'E'){
            $res[4] = 1;
                }
            }

        $renovacion = $fecha = date_create($contrato->Vencimiento);
        date_add($renovacion, date_interval_create_from_date_string('30 days'));

        if( $contrato!=null && $contrato->Vencimiento >= $renovacion){
            $res[5]= 1;
        }

            return  Response(json_encode($res));

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }
        }
    }

    public function check_cliente(Request $request){
        if($request->ajax()){

            $res = [];
         try { 
            
            $cliente = DB::table("its_contactos")->where("CIF",'=',$request->CIF)->first();

            $observaciones_cliente = DB::table("gesanc_clientes")->where("cif",'=',$request->CIF)->first();

            $tarjetas_activas = DB::table("its_tarjetas")->where("cif","=",$request->CIF)->get();



               if($cliente!=null){
                        if($cliente->devolucion ==1){
                        $res[0]=1;
                            }else{
                            $res[0]=0;
                            }
                         if($cliente->cteConflictivo ==1){
                        $res[1]=1;
                            }else{
                            $res[1]=0;
                            }
                         if($cliente->impago_multas ==1){
                       $res[2]=1;
                            }else{
                            $res[2]=0;
                            }

                        $res[4]=$cliente->Empresa;
                        }
                        if($observaciones_cliente!=null){
                        $res[3]=$observaciones_cliente->Observacion;
                        }

        if($cliente->CIF == 'B04213831' || $cliente->Empresa =='DOLORES AGUILA PARRA SL'){
            $res[5] = 1;
        }
        if($cliente->Empresa=='TERESA_TRANSPAEZ_CORBALAN' || $cliente->CIF=='74337132M' || $cliente->CIF == 'B73488173'){
            $res[6] = 1;
        }

         $de_alta = DB::table('its_contratos')->selectRaw("count(*) as 'cantidad'")->whereRaw("Contratado = '".$cliente->CIF."' and `Fecha Contrato` <= DATE(CURDATE()) and (Baja > DATE(CURDATE()) OR Baja is null) and Vencimiento > date(curdate())")->first();
        if($de_alta->cantidad > 4){
            $res[7] = 1;
        }
        if($tarjetas_activas!=null){
            $res[8]=1;
        }


                     
            return  Response(json_encode($res));        
            
            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            } 

           
        }

    }

    public function exportar_excel(Request $request){

            $filtros = array();

            $consulta = "";

              if($request->matricula != null){
                $matricula = $request->matricula;
                $filtro_matricula = "MATRICULA = '".$matricula."'";
                $filtros[] = $filtro_matricula;
            }
            if($request->acuerdo != null){
                $acuerdo = $request->acuerdo;
                $filtro_acuerdo = "ACUERDO = '".$acuerdo."'";
                $filtros[] = $filtro_acuerdo;
            }
            if($request->importe != null){
                $importe = $request->importe;
                $filtro_importe = "IMPORTE = '".$importe."'";
                $filtros[] = $filtro_importe;
            }
            if($request->apertura != null){
                $apertura = $request->apertura;
                $filtro_apertura = "F_APERTURA = '".$apertura."'";
                $filtros[] = $filtro_apertura;
            }
            if($request->cif != null){
                $cif = $request->cif;
                $filtro_cif = "CIF = '".$cif."'";
                $filtros[] = $filtro_cif;
            }
            if($request->empresa != null){
                $empresa = $request->empresa;
                $filtro_empresa = "CLIENTE = '".$empresa."'";
                $filtros[] = $filtro_empresa;
            }
            if($request->contratante != null){
                $contratante = $request->contratante;
                $filtro_contratante = "EMPRESA = '".$contratante."'";
                $filtros[] = $filtro_contratante;
            }
            if($request->ordenante != null){
                $ordenante = $request->ordenante;
                $filtro_ordenante = "IdComercial = '".$ordenante."'";
                $filtros[] = $filtro_ordenante;
            }
            if($request->conductor != null){
                $conductor = $request->conductor;
                $filtro_conductor = "CHOFER = '".$conductor."'";
                $filtros[] = $filtro_conductor;
            }
            if($request->lugar != null){
                $lugar = $request->lugar;
                $filtro_lugar = "LUGAR = '".$lugar."'";
                $filtros[] = $filtro_lugar;
            }
            if($request->ciudad != null){
                $ciudad = $request->ciudad;
                $filtro_ciudad = "CIUDAD = '".$ciudad."'";
                $filtros[] = $filtro_ciudad;
            }
            if($request->pais != null){
                $pais = $request->pais;
                $filtro_pais = "a.PAIS = '".$pais."'";
                $filtros[] = $filtro_pais;
            }
            if($request->autoridad != null){
                $autoridad = $request->autoridad;
                $filtro_autoridad = "Autoridad = '".$autoridad."'";
                $filtros[] = $filtro_autoridad;
            }
            if($request->telefono != null){
                $telefono = $request->telefono;
                $filtro_telefono = "TelefAutoridad = '".$telefono."'";
                $filtros[] = $filtro_telefono;
            }
            if($request->fax != null){
                $fax = $request->fax;
                $filtro_fax = "FaxAutoridad = '".$fax."'";
                $filtros[] = $filtro_fax;
            }
            if($request->facprov != null){
                $facprov = $request->facprov;
                $filtro_facprov = "FacturadoTAI = '".$facprov."'";
                $filtros[] = $filtro_facprov;
            }
            if($request->facits != null){
                $facits = $request->facits;
                $filtro_facits = "FacturaITS = '".$facits."'";
                $filtros[] = $filtro_facits;
            }
            if($request->importeprov != null){
                $importeprov = $request->importeprov;
                $filtro_importeprov = "ImporteFProv = '".$importeprov."'";
                $filtros[] = $filtro_importeprov;
            }
            if($request->importeits != null){
                $importeits = $request->importeits;
                $filtro_importeits = "ImporteFITS = '".$importeits."'";
                $filtros[] = $filtro_importeits;
            }
            if($request->gastos != null){
                $gastos = $request->gastos;
                $filtro_gastos = "Gastos = '".$gastos."'";
                $filtros[] = $filtro_numfactura;
            }
            if($request->remision != null){
                $remision = $request->remision;
                $filtro_remision = "fecharemision = '".$remision."'";
                $filtros[] = $filtro_remision;
            }
            if($request->dossier != null){
                $dossier = $request->dossier;
                $filtro_dossier = "NumDossier = '".$dossier."'";
                $filtros[] = $filtro_dossier;
            }
            if($request->notas != null){
                $notas = $request->notas;
                $filtro_notas = "Notas = '".$notas."'";
                $filtros[] = $filtro_notas;
            }
            if($request->anulada != null){
                $anulada = $request->anulada;
                $filtro_anulada = "anuladaIntervencion = '".$anulada."'";
                $filtros[] = $filtro_anulada;
            }
            if($request->fc != null){
                $fc = $request->fc;
                $filtro_fc = "FaxConf = '".$fc."'";
                $filtros[] = $filtro_fc;
            }

            //MOTIVOS
            if($request->motivo != null){
                $motivo = $request->motivo;
                $filtro_motivo = "ID in (select ID from `inter24_motivos por multa` where IdMotivo='".$motivo."')";
                $filtros[] = $filtro_motivo;
            }
            if($request->estado != null){
                $estado = $request->estado;
                $filtro_estado = "ID in (select ID from `inter24_motivos por multa` where estado_caucion='".$estado."')";
                $filtros[] = $filtro_estado;
            }
            if($request->importe_motivo != null){
                $importe_motivo = $request->importe_motivo;
                $filtro_importe_motivo = "ID in (select ID from `inter24_motivos por multa` where Importe='".$importe."')";
                $filtros[] = $filtro_importe_motivo;
            }
            if($request->cheque != null){
                $cheque = $request->cheque;
                $filtro_cheque = "ID in (select ID from `inter24_motivos por multa` where Cheque='".$cheque."')";
                $filtros[] = $filtro_cheque;
            }
            if($request->detalles != null){
                $detalles = $request->detalles;
                $filtro_detalles = "ID in (select ID from `inter24_motivos por multa` where Detalles='".$detalle."')";
                $filtros[] = $filtro_detalles;
            }
            if($request->tarjeta != null){
                $tarjeta = $request->tarjeta;
                $filtro_tarjeta = "ID in (select ID from `inter24_motivos por multa` where numTarjeta='".$tarjeta."')";
                $filtros[] = $filtro_tarjeta;
            }
            if($request->caucionpagada != null){
                $caucionpagada = $request->caucionpagada;
                $filtro_caucionpagada = "ID in (select ID from `inter24_motivos por multa` where caucionPagada='".$caucionpagada."')";
                $filtros[] = $filtro_caucionpagada;
            }

            //tramites
               if($request->fecha_t != null){
                $fecha_t = $request->fecha_t;
                $filtro_fecha_t = "ID in (select IDMULTA from inter24_tramites where FECHA='".$fecha_t."')";
                $filtros[] = $filtro_fecha_t;
            }
            if($request->hora_tramite != null){
                $hora_tramite = $request->hora_tramite;
                $filtro_hora_tramite = "ID in (select IDMULTA from inter24_tramites where HORA='".$hora_tramite."')";
                $filtros[] = $filtro_hora_tramite;
            }
            if($request->persona1 != null){
                $persona1 = $request->persona1;
                $filtro_persona1 = "ID in (select IDMULTA from inter24_tramites where COD_PERSONA1='".$persona1."')";
                $filtros[] = $filtro_persona1;
            }
            if($request->persona2 != null){
                $persona2 = $request->persona2;
                $filtro_persona2 = "ID in (select IDMULTA from inter24_tramites where COD_PERSONA2='".$persona2."')";
                $filtros[] = $filtro_persona2;
            }
            if($request->entidad1 != null){
                $entidad1 = $request->entidad1;
                $filtro_entidad1 = "ID in (select IDMULTA from inter24_tramites where COD_ACTOR1='".$entidad1."')";
                $filtros[] = $filtro_entidad1;
            }
            if($request->entidad2 != null){
                $entidad2 = $request->entidad2;
                $filtro_entidad2 = "ID in (select IDMULTA from inter24_tramites where COD_ACTOR2='".$entidad2."')";
                $filtros[] = $filtro_entidad2;
            }
            if($request->operacion != null){
                $operacion = $request->operacion;
                $filtro_operacion = "ID in (select IDMULTA from inter24_tramites where COD_OPER1='".$operacion."')";
                $filtros[] = $filtro_operacion;
            }
             if($request->texto_tramite != null){
                $texto_tramite = $request->texto_tramite;
                $filtro_texto_tramite = "ID in (select IDMULTA from inter24_tramites where TRAMITE='".$texto_tramite."')";
                $filtros[] = $filtro_texto_tramite;
            }

            for ($i =count($filtros) - 1; $i >= 0 ; $i--) { 
                $consulta .= $filtros[$i];
                $consulta .= " ";

                if($i > 0){
                    $consulta .= "AND ";
                }
            }
            //DB::enableQueryLog();

            $intervenciones = DB::table(DB::raw("inter24_interventions a, gesanc_paises b"))->selectRaw("a.ACUERDO,a.MATRICULA,a.FECHA,a.NumDossier,a.CLIENTE,b.Pais,a.IMPORTE,a.*")->whereRaw("a.PAIS=b.`Codigo Pais`")->orderby("FECHA","DESC")->limit(200)->get();



            
            if($consulta !=""){

              $intervenciones = DB::table(DB::raw("inter24_interventions a, gesanc_paises b"))->selectRaw("a.ACUERDO,a.MATRICULA,a.FECHA,a.NumDossier,a.CLIENTE,b.Pais,a.IMPORTE,a.*")->whereRaw("a.PAIS=b.`Codigo Pais` AND ".$consulta)->orderby("FECHA","DESC")->limit(200)->get();

         }

          $objPHPExcel = new PHPExcel();
        // Se asignan las propiedades del libro
        $objPHPExcel->getProperties()->setCreator("ITS") // Nombre del autor
        ->setLastModifiedBy("ITS") //Ultimo usuario que lo modificó
        ->setTitle("intervenciones_filtradas") // Titulo
        ->setDescription("intervenciones_filtradas"); //Descripción
        $titulosColumnas = array('Acuerdo'
                                ,'Matricula'
                                ,'Fecha'
                                ,'Dossier'
                                ,'Cliente'
                                ,'Pais');
        
        // Se combinan las celdas, para colocar ahí el titulo del reporte
        //$objPHPExcel->setActiveSheetIndex(0)
        //->mergeCells('A1:D1');
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1', $titulosColumnas[0])
        ->setCellValue('B1', $titulosColumnas[1])
        ->setCellValue('C1', $titulosColumnas[2])
        ->setCellValue('D1', $titulosColumnas[3])
        ->setCellValue('E1', $titulosColumnas[4])
        ->setCellValue('F1', $titulosColumnas[5]);
        
        $i=2;
        foreach ($intervenciones as $key => $intervencion) {
              $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.$i, $intervencion->ACUERDO)
            ->setCellValue('B'.$i, $intervencion->MATRICULA)
            ->setCellValue('C'.$i, date('d - m - Y', strtotime($intervencion->FECHA)))
            ->setCellValue('D'.$i, $intervencion->NumDossier)
            ->setCellValue('E'.$i, $intervencion->CLIENTE)
            ->setCellValue('F'.$i, $intervencion->Pais);
            $i++;
        }



    header('Content-Encoding: UTF-8');
    header('Content-type: text/csv; charset=UTF-8');
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="intervenciones_filtradas.xlsx"');
    header('Cache-Control: max-age=0');
    header("Pragma: no-cache");
    header("Expires: 0");
    header('Content-Transfer-Encoding: binary');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
    
    }

    public function imprimir_recibos(Request $request){

    $recibos = DB::table("inter24_recibos")->whereRaw("acuerdo='".$request->acuerdo."'")->get();

    if(count($recibos)==0){
        Response(1);
    }else{

       Response(0);
    }
}

public function impresion_recibos(){

    $recibos = DB::table("inter24_recibos")->whereRaw("acuerdo='".$_GET["acuerdo"]."'")->get();
    $intervencion = DB::table("inter24_interventions")->where("acuerdo","=",$_GET["acuerdo"])->first();
    $contrato = DB::table("its_contratos")->selectRaw("*,`Fecha Contrato` as 'FechaContrato'")->whereRaw("IDContrato in (select IdContrato from its_anexos where Matricula='".$intervencion->MATRICULA."')")->first();
    $cliente = DB::table("its_contactos")->where("CIF",'=',$intervencion->CIF)->first();

    foreach ($recibos as $key => $recibo) {
        $filename = '..\public\temp\recibo_'. $key . '.docx';
        $documentos [] = $filename;
        $templateWord = new TemplateProcessor('..\public\templates\recibos\Recibos.docx');
                    $numero_recibo = "";
                $templateWord->setValue('num',$numero_recibo);
                $templateWord->setValue('importe',$recibo->importe." €");
                $templateWord->setValue('fecha',date('d - m - Y'));
                $templateWord->setValue('vencimiento',date('d - m - Y', strtotime($recibo->vencimiento)));
                //$templateWord->setValue('importe_letras',ucfirst(convertir($recibo->importe)));
                $templateWord->setValue('empresa',$cliente->Empresa);
                $templateWord->setValue('direccion',$cliente->Direccion);
                $templateWord->setValue('CP',$cliente->CP);
                $templateWord->setValue('poblacion',$cliente->Poblacion);
                //$templateWord->setValue('provincia',$provincia->Provincia);
                $templateWord->setValue('CIF',$cliente->CIF);
                $templateWord->setValue('fecha_c',date('d - m - Y', strtotime($contrato->FechaContrato)));
                $templateWord->saveAs(storage_path($filename));
    }

    $dm = new DocxMerge();
    $dm->merge( $documentos, '..\public\temp\recibos.docx' );
 
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename=recibos.docx; charset=iso-8859-1');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize('..\public\temp\recibos.docx'));
    readfile('..\public\temp\recibos.docx');
  

    $files = glob('..\public\temp\*'); // get all file names
    foreach($files as $file){ // iterate files
      if(is_file($file))
        unlink($file); // delete file
        }
  exit;

}

public function mostrar_parte_multa(){


     $output ="<label>Ejemplar para:</label><br>";
     $output.="<input type='radio' name='ejemplar'id='ejemplar' value='comercial' checked>Empresa Comercial  ";
     $output.="<input type='radio' name='ejemplar'id='ejemplar' value='cliente'>Cliente<br>";

     $output .="<label>Texto Indicativo:</label><br>";
     $output.="<input type='radio' name='texto'id='texto' value='remision' checked>Remisión giro bancario  ";
     $output.="<input type='radio' name='texto'id='texto' value='ingreso'>Ingreso en cuenta ";
     $output.="<input type='radio' name='texto'id='texto' value='nada'>Nada<br>";

     $output .="<label>Es caución:</label><br>";
     $output.="<input type='radio' name='caucion'id='caucion' value='si' checked>Si  ";
     $output.="<input type='radio' name='caucion'id='caucion' value='no'>No<br>";
     

     return Response($output);
}

public function imprimir_parte_multa(){

    $intervencion = DB::table("inter24_interventions")->where("ID","=",$_GET["intervencion"])->first();
    $matricula = DB::table("its_matriculas")->where("Matricula","=",$intervencion->MATRICULA)->first();
    $cliente = DB::table("its_contactos")->where("CIF",'=',$intervencion->CIF)->first();
    $contratantes = DB::table("gesanc_empresas")->where("num_empresa",'=',$intervencion->EMPRESA)->first();
    $pais = DB::table("gesanc_paises")->whereRaw("`Codigo Pais` = '".$intervencion->PAIS."'")->first();

    

        $filename = '..\public\temp\parte_multa.docx';
        $documentos [] = $filename;
        $templateWord = new TemplateProcessor('..\public\templates\intervenciones\parte_multa.docx');

        if($_GET["ejemplar"]=="comercial"){
            $templateWord->setValue('destinatario',$contratantes->Empresa);
        }else{
            $templateWord->setValue('destinatario',$cliente->Empresa);
        }

        if($_GET["texto"]=="remision"){
            $templateWord->setValue('texto_indicativo',"Con fecha de hoy giramos un recibo bancario a la vista contra su cuenta corriente, por lo que le rogamos que la misma se encuentre suficientemente  aprovisionada.");
        }elseif($_GET["texto"]=="ingreso"){
            $templateWord->setValue('texto_indicativo',"DEBE REALIZAR INGRESO EN CUENTA:<w:br/>ES7921002534830210086691");
        }else{  
            $templateWord->setValue('texto_indicativo',"");
        }

        if($_GET["caucion"]=="si"){
            $templateWord->setValue('caucion',"MEDIANTE CAUCION");
        }else{
            $templateWord->setValue('caucion',"");
        }
             
                $templateWord->setValue('dossier',$intervencion->ACUERDO);
                $templateWord->setValue('fecha',date('d - m - Y', strtotime($intervencion->F_APERTURA)));
                $templateWord->setValue('pais',$pais->Pais);
                $templateWord->setValue('cliente',$cliente->Empresa);
                $templateWord->setValue('matri',$intervencion->MATRICULA);
                $templateWord->saveAs(storage_path($filename));

    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename=parte_multa.docx; charset=iso-8859-1');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize('..\public\temp\parte_multa.docx'));
    readfile('..\public\temp\parte_multa.docx');
  

    $files = glob('..\public\temp\*'); // get all file names
    foreach($files as $file){ // iterate files
      if(is_file($file))
        unlink($file); // delete file
        }
  exit;



}

public function mostrar_parte_cliente(){


     $output ="<label>Gastos:</label><br>";
     $output.="<input type='text' class='form-control' name='gastos_dossier' id='gastos_dossier' value=''><br>";

     $output .="<label>Nota adicional:</label><br>";
     $output.="<input type='text' class='form-control' name='nota_adicional' id='nota_adicional' value=''><br>";
     return Response($output);
}

public function imprimir_parte_cliente(){


    $intervencion = DB::table("inter24_interventions")->where("ID","=",$_GET["intervencion"])->first();
    $matricula = DB::table("its_matriculas")->where("Matricula","=",$intervencion->MATRICULA)->first();
    $cliente = DB::table("its_contactos")->where("CIF",'=',$intervencion->CIF)->first();
    $contratantes = DB::table("gesanc_empresas")->where("num_empresa",'=',$intervencion->EMPRESA)->first();
    $pais = DB::table("gesanc_paises")->whereRaw("`Codigo Pais` = '".$intervencion->PAIS."'")->first();

    

        $filename = '..\public\temp\parte_cliente.docx';
        $documentos [] = $filename;
        $templateWord = new TemplateProcessor('..\public\templates\intervenciones\parte_multa_cliente.docx');

        $templateWord->setValue('cliente',$cliente->Empresa);
        $templateWord->setValue('acuerdo',$intervencion->ACUERDO);
        $templateWord->setValue('email',$cliente->email);
        $templateWord->setValue('fecha',date('d - m - Y', strtotime($intervencion->F_APERTURA)));
        $templateWord->setValue('importe',$intervencion->IMPORTE." €");
        $templateWord->setValue('matricula',$intervencion->MATRICULA);
        $templateWord->setValue('chofer',$intervencion->CHOFER);
        $templateWord->setValue('autori',$intervencion->Autoridad);
        $templateWord->setValue('lugar',$intervencion->LUGAR);
        $templateWord->setValue('ciudad',$intervencion->CIUDAD);
        $templateWord->setValue('Pais',$pais->Pais);
        $templateWord->setValue('gastos',$_GET["gastos"]." €");
        $templateWord->setValue('nota',$_GET["texto"]);

                $templateWord->saveAs(storage_path($filename));

    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename=parte_cliente.docx; charset=iso-8859-1');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize('..\public\temp\parte_cliente.docx'));
    readfile('..\public\temp\parte_cliente.docx');
  

    $files = glob('..\public\temp\*'); // get all file names
    foreach($files as $file){ // iterate files
      if(is_file($file))
        unlink($file); // delete file
        }
  exit;
}

public function mostrar_france_truck(){

     $output ="<label>Texto Indicativo:</label><br>";
     $output.="<input type='radio' name='texto'id='texto' value='avance' checked>Avance de fondos  ";
     $output.="<input type='radio' name='texto'id='texto' value='pago'>Pago de sanción<br>";

     $output .="<label>Gastos:</label><br>";
     $output.="<input type='text' class='form-control' name='gastos_dossier' id='gastos_dossier' value=''><br>";
     return Response($output);
}

public function imprimir_france_truck(){


    $intervencion = DB::table("inter24_interventions")->where("ID","=",$_GET["intervencion"])->first();
    $matricula = DB::table("its_matriculas")->where("Matricula","=",$intervencion->MATRICULA)->first();
    $cliente = DB::table("its_contactos")->where("CIF",'=',$intervencion->CIF)->first();
    $contratantes = DB::table("gesanc_empresas")->where("num_empresa",'=',$intervencion->EMPRESA)->first();
    $pais = DB::table("gesanc_paises")->whereRaw("`Codigo Pais` = '".$intervencion->PAIS."'")->first();

    

        $filename = '..\public\temp\france_truck.docx';
        $documentos [] = $filename;
        $templateWord = new TemplateProcessor('..\public\templates\intervenciones\france_truck.docx');

        if($_GET["texto"]=="avance"){
            $templateWord->setValue('asunto',"AVANCE DE FONDOS");
        }else{
            $templateWord->setValue('asunto',"PAGO DE SANCIÓN");
        }

        $templateWord->setValue('cliente',$cliente->Empresa);
        $templateWord->setValue('acuerdo',$intervencion->ACUERDO);
        $templateWord->setValue('fecha',date('d - m - Y', strtotime($intervencion->F_APERTURA)));
        $templateWord->setValue('importe',$intervencion->IMPORTE." €");
        $templateWord->setValue('matricula',$intervencion->MATRICULA);
        $templateWord->setValue('chofer',$intervencion->CHOFER);
        $templateWord->setValue('autori',$intervencion->Autoridad);
        $templateWord->setValue('Pais',$pais->Pais);
        $templateWord->setValue('gastos',$_GET["gastos"]." €");

                $templateWord->saveAs(storage_path($filename));

    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename=france_truck.docx; charset=iso-8859-1');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize('..\public\temp\france_truck.docx'));
    readfile('..\public\temp\france_truck.docx');
  

    $files = glob('..\public\temp\*'); // get all file names
    foreach($files as $file){ // iterate files
      if(is_file($file))
        unlink($file); // delete file
        }
  exit;
}

public function mostrar_parte_tramites(){

     $output ="<label>Tramites ocultos:</label><br>";
     $output.="<input type='radio' name='oculto'id='oculto' value='si' checked>Si  ";
     $output.="<input type='radio' name='oculto'id='oculto' value='no'>No<br>";
     return Response($output);
}

public function imprimir_parte_tramites(){


    $intervencion = DB::table("inter24_interventions")->where("ID","=",$_GET["intervencion"])->first();
    $motivos = DB::table(DB::raw("`inter24_motivos por multa` a, inter24_motivos b"))->select(DB::raw("a.*,b.Motivo"))->whereRaw("a.IdMotivo = b.COD_MOTIVO and a.ID ='".$_GET["intervencion"]."'")->get();

     if($_GET["oculto"]=="si"){
           $tramites = DB::table("inter24_tramites")->whereRaw("IDMULTA = '".$_GET["intervencion"]."'")->get();
        }else{
            $tramites = DB::table("inter24_tramites")->whereRaw("IDMULTA = '".$_GET["intervencion"]."' and Ocultar='0'")->get();
        }
    

    $matricula = DB::table("its_matriculas")->where("Matricula","=",$intervencion->MATRICULA)->first();
    $cliente = DB::table("its_contactos")->where("CIF",'=',$intervencion->CIF)->first();
    $contratantes = DB::table("gesanc_empresas")->where("num_empresa",'=',$intervencion->EMPRESA)->first();
    $pais = DB::table("gesanc_paises")->whereRaw("`Codigo Pais` = '".$intervencion->PAIS."'")->first();

    

        $filename = '..\public\temp\tramites.docx';
        $documentos [] = $filename;
        $templateWord = new TemplateProcessor('..\public\templates\intervenciones\tramites.docx');

       


        $templateWord->setValue('contratante',$contratantes->Empresa);
        $templateWord->setValue('acuerdo',$intervencion->ACUERDO);
        $templateWord->setValue('dossier',$intervencion->NumDossier);
        $templateWord->setValue('cliente',$cliente->Empresa);
        $templateWord->setValue('fecha',date('d - m - Y', strtotime($intervencion->F_APERTURA)));
        $templateWord->setValue('importe',$intervencion->IMPORTE);
        $templateWord->setValue('matricula',$intervencion->MATRICULA);
        $templateWord->setValue('demantande',$intervencion->DEMANDANTE);
        $templateWord->setValue('chofer',$intervencion->CHOFER);
        $templateWord->setValue('lugar',$intervencion->LUGAR);
        $templateWord->setValue('ciudad',$intervencion->CIUDAD);
        $templateWord->setValue('Pais',$pais->Pais);
        $templateWord->setValue('autoridad',$intervencion->Autoridad);
        $templateWord->setValue('telefono',$cliente->Tel);
        $templateWord->setValue('fax',$cliente->fax);
        $templateWord->setValue('boletin',$intervencion->NumBoletin);
        $templateWord->setValue('asistido',$intervencion->Asistido);

        $lista_motivos = "";
        $lista_importes = "";
        $lista_cantidad = "";
        $lista_tarjetas = "";
        $lista_detalles = "";

        foreach ($motivos as $key => $motivo) {
        $lista_motivos  .= $motivo->MOTIVO."<w:br/>";
        $lista_importes .= $motivo->Importe." €<w:br/>";
        $lista_cantidad .= $motivo->Cantidad."<w:br/>";
        $lista_tarjetas .= $motivo->numTarjeta."<w:br/>";
        $lista_detalles .= $motivo->Detalles."<w:br/>";
        }

        $templateWord->setValue('motivos',$lista_motivos);
        $templateWord->setValue('importe',$lista_importes);
        $templateWord->setValue('cant',$lista_cantidad);
        $templateWord->setValue('tarjeta',$lista_tarjetas);
        $templateWord->setValue('detalles',$lista_detalles);

        $lista_fecha = "";
        $lista_hora = "";
        $lista_tramite = "";

        foreach ($tramites as $key => $tramite) {
        $lista_fecha  .= date('d - m - Y', strtotime($tramite->FECHA))."<w:br/>";
        $lista_hora .= date('H:i', strtotime($tramite->HORA))." €<w:br/>";
        $lista_tramite .= $tramite->TRAMITE."<w:br/>";
    }

        $templateWord->setValue('fecha_tramite',$lista_motivos);
        $templateWord->setValue('hora',$lista_importes);
        $templateWord->setValue('tramite',$lista_cantidad);

        $templateWord->saveAs(storage_path($filename));

    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename=tramites.docx; charset=iso-8859-1');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize('..\public\temp\tramites.docx'));
    readfile('..\public\temp\tramites.docx');
  

    $files = glob('..\public\temp\*'); // get all file names
    foreach($files as $file){ // iterate files
      if(is_file($file))
        unlink($file); // delete file
        }
  exit;
}


}
