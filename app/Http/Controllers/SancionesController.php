<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\gesanc_sanciones;

use App\gesanc_lista_motivos_denuncia;

use App\gesanc_reduccion;

use App\gesanc_contencioso;

use App\gesanc_clientes;

use App\gesanc_choferesclientes;

use DateTime;

use Illuminate\Database\QueryException;

use App\gesanc_contratos;

use PhpOffice\PhpWord\TemplateProcessor;

use PHPExcel; 
use PHPExcel_IOFactory; 
use DocxMerge\DocxMerge;


class SancionesController extends Controller
{




        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sanciones = DB::table('gesanc_sanciones')->orderBy('FECHALLEGADA','DESC')->take(20)->get();

        $clientes = DB::table('gesanc_clientes')->select('empresa')->whereRaw('cif IN (select cif from gesanc_sanciones)')->orderBy('empresa')->get();

        $conductores = DB::table('gesanc_sanciones')->select('conductor')->whereRaw('conductor IN (select chofer from gesanc_choferes)')->distinct()->orderBy('conductor')->get();

        $matriculas = DB::table('gesanc_matriculas')->select('matricula')->whereRaw('matricula IN (select matricula from gesanc_sanciones)')->orderBy('matricula')->distinct()->get();

        $paises = DB::table('gesanc_paises')->select('Pais')->whereRaw('`Codigo Pais` IN (Select pais from gesanc_sanciones)')->orderBy('Pais')->get();

        $autoridades = DB::table('gesanc_sanciones')->select('Autoridad')->distinct()->orderBy('Autoridad')->get();

        //$comerciales = DB::table('gesanc_comerciales')->select(DB::Raw("CONCAT(Nombre,' ',Apellidos) AS comercial"))->get();


        //calcular el código de la matricula



        return view('sanciones/index', ['sanciones'=> $sanciones,'clientes' =>$clientes,'conductores'=>$conductores,'matriculas'=>$matriculas,'paises'=>$paises,'autoridades'=>$autoridades,]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $clientes = DB::table('gesanc_clientes')->select('empresa')->whereRaw('cif IN (select cif from gesanc_sanciones)')->orderBy('empresa')->get();

        $conductores = DB::table('gesanc_sanciones')->select('conductor')->whereRaw('conductor IN (select chofer from gesanc_choferes)')->distinct()->orderBy('conductor')->get();

        $matriculas = DB::table('gesanc_matriculas')->select('matricula')->whereRaw('matricula IN (select matricula from gesanc_sanciones)')->orderBy('matricula')->distinct()->get();

        $paises = DB::table('gesanc_paises')->select('Pais')->whereRaw('`Codigo Pais` IN (Select pais from gesanc_sanciones)')->orderBy('Pais')->get();

        $autoridades = DB::table('gesanc_sanciones')->select('Autoridad')->distinct()->orderBy('Autoridad')->get();

        //$comerciales = DB::table('gesanc_comerciales')->select(DB::Raw("CONCAT(Nombre,' ',Apellidos) AS comercial"))->get();

        $divisas = $divisa = DB::table('gesanc_divisas')->get();

        //calcular el código de la matricula
        return view('sanciones/create', ['clientes' =>$clientes,'conductores'=>$conductores,'matriculas'=>$matriculas,'paises'=>$paises,'autoridades'=>$autoridades,'divisas'=>$divisas]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->ajax()){

            $datos = array();
            parse_str($request->datos,$datos);

            $gestionadogure = 0;
            $estado = 0;
            $justif = 0;
            $fecha_justif = "";
            $codigopais = 0;
            $importe = null;
            $divisa = "";
            $pagado = null;
            $autoridad = "";
            $codigoautoridad ="";
            $empresa ="";
            $cif = "";
            $idcontrato = "";
            $matricula = "";
            $boletin = "";
            $expediente = "";
            $acuerdo = "";
            $ft = "";
            $conductor = "";

            $importepagado = null;


            $fecha_multa = null;
            
            $fecha_entrada = null;
            
            $fecha_archivado = null;

            $fecha_justif = null;
           
            //echo $datos["empresa"];
            //print_r($datos);

            if(array_key_exists("gestionadogure",$datos)){
               $gestionadogure = 1;
            }
            if(array_key_exists("estado",$datos)){
               $estado = 1;
            }
            if(array_key_exists("justif",$datos)){
               $justif = 1;
            }
            if($datos["pagado"]!=null){
                $pagado = $datos["pagado"];
            }

            if($datos["importe"]!=null){
                $importe = $datos["importe"];
            }

            if($datos["fecha_multa"] !=null){
                $fecha_multa = DateTime::createFromFormat('d/m/Y', $datos["fecha_multa"]);
                date_time_set($fecha_multa, 00, 00);
            }

            if($datos["fecha_entrada"]!=null){
                $fecha_entrada = DateTime::createFromFormat('d/m/Y', $datos["fecha_entrada"]);
                date_time_set($fecha_entrada, 00, 00);
            }

            if($datos["fecha_archivado"]!=null){
                $fecha_archivado = DateTime::createFromFormat('d/m/Y', $datos["fecha_archivado"]);
                date_time_set($fecha_archivado, 00, 00);
            }

            if($datos["fecha_justif"]!=null){
                $fecha_justif = DateTime::createFromFormat('d/m/Y', $datos["fecha_justif"]);
                date_time_set($fecha_justif, 00, 00);
            }

            if($datos["empresa"]!=null){
                $empresa = $datos["empresa"];
            }

             if($datos["autoridad"]!=null){
                $autoridad = $datos["autoridad"];
            }
             if($datos["matricula"]!=null){
                $matricula = $datos["matricula"];
            }
             if($datos["boletin"]!=null){
                $boletin = $datos["boletin"];
            }
             if($datos["expediente"]!=null){
                $expediente = $datos["expediente"];
            }

             if($datos["acuerdo"]!=null){
                $acuerdo = $datos["acuerdo"];
            }

             if($datos["ft"]!=null){
                $ft = $datos["ft"];
            }

            if($datos["conductor"]!=null){
                $conductor = $datos["conductor"];
            }

            if($datos["divisas"]!=null){
                $divisa = $datos["divisas"];
            }

             if($datos["pagado"]!=null){
                $importepagado = $datos["pagado"];
            }

            $paises = DB::table('gesanc_paises')->select(DB::raw("`Codigo Pais` as Codigo"))->whereRaw('Pais ="'.$datos["pais"].'"')->get();

            $clientes = DB::table('gesanc_clientes')->select('cif')->whereRaw('empresa="'.$empresa.'"')->get();

            $autoridades = DB::table('gesanc_autoridades')->select('IDautoridad')->whereRaw('Autoridad="'.$autoridad.'"')->get();

            $contratos = gesanc_contratos::select('IDCONTRATO')->whereRaw('Matricula="'.$datos["matricula"].'"')->get();

            foreach ($paises as $key => $dato) {
                $codigopais = $dato->Codigo;
            }

            foreach ($clientes as $key => $dato) {
                $cif = $dato->cif;
            }

            foreach ($autoridades as $key => $dato) {
                $codigoautoridad = $dato->IDautoridad;
            }

              foreach ($contratos as $key => $dato) {
                $idcontrato = $dato->IDCONTRATO;
            }


            try { 
               
               gesanc_sanciones::insertGetId(
                ['IDCONTRATO'=>$idcontrato,'cif'=>$cif,'Matricula'=>$matricula,'Boletin'=>$boletin,'expediente'=>$expediente,'Acuerdo'=>$acuerdo,'FechaMulta'=>$fecha_multa,'Conductor'=>$conductor,'Importe'=>$importe,'Moneda'=>$divisa,'Notas'=>$datos['notas'],'Lugar'=>$datos["lugar"],'Pais'=>$codigopais,'Autoridad'=>$autoridad,'FECHALLEGADA'=>$fecha_entrada,'Importepagado'=>$importepagado,'FECHACIERRE'=>$fecha_archivado,'fechaJustificante'=>$fecha_justif,'CodigoFT'=>$ft]
                );

            return  Response("La sanción se ha editado correctamente");

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }

        }  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        /*$sancion = gesanc_sanciones::with('gesanc_contratos','gesanc_clientes','gesanc_empresas.Empresa')->get();*/
        $sancion = gesanc_sanciones::where('IDSancion',$id)->firstOrFail();
        //$motivos = DB::table('gesanc_lista_motivos_denuncia')->select(DB::raw("a.*, b.MOTIVO from gesanc_lista_motivos_denuncia a, gesanc_motivos b"))->whereRaw("a.Idmotivo = b.COD_MOTIVO and a.IDSancion ='".$id."'")->get();

        $motivos = DB::table(DB::raw("gesanc_lista_motivos_denuncia a, gesanc_motivos b"))->select(DB::raw("a.*, b.MOTIVO"))->whereRaw("a.Idmotivo = b.COD_MOTIVO and a.IDSancion ='".$id."'")->get();

        $cliente = gesanc_clientes::whereRaw('cif = (select cif from gesanc_sanciones where IDSancion = '.$id.')')->firstOrFail();

        /*raw("SELECT a.*, b.MOTIVO from gesanc_lista_motivos_denuncia a, gesanc_motivos b where a.Idmotivo = b.COD_MOTIVO and a.IDSancion ='".$id."'")->get();*/
        /*$empresa = gesanc_sanciones::
                   ->join('gesanc_contratos','gesanc_sanciones.IDCONTRATO','=','gesanc_contratos.idcontrato')
                   ->join('gesanc_clientes','gesanc_contratos.cif','=','gesanc_clientes.cif')
                   ->join('gesanc_empresas','gesanc_clientes.delegacion','=','gesanc_empresas.Num_empresa')
                   ->where('IDSancion',$id)
                   ->select('empresa');*/
                   //dd($sancion);
                   //dd($empresa);

        $conductores = DB::table('gesanc_sanciones')->select('conductor')->whereRaw('conductor IN (select chofer from gesanc_choferes)')->distinct()->orderBy('conductor')->get(); 

        $autoridades = DB::table('gesanc_sanciones')->select('Autoridad')->distinct()->orderBy('Autoridad')->get();

        $boletines = DB::table('gesanc_sanciones')->select('Boletin')->distinct()->orderBy('Boletin')->get();

        $matriculas = DB::table('gesanc_matriculas')->select('matricula')->whereRaw('matricula IN (select matricula from gesanc_sanciones)')->orderBy('matricula')->distinct()->get();

        $paises = DB::table('gesanc_paises')->select('Pais')->orderBy('Pais')->get();

        $pais = DB::table('gesanc_paises')->select('Pais')->whereRaw('`Codigo Pais` IN (Select pais from gesanc_sanciones where IDSancion='.$id.')')->orderBy('Pais')->get();

        $autoridades = DB::table('gesanc_sanciones')->select('Autoridad')->distinct()->orderBy('Autoridad')->get();

        $lugares = DB::table('gesanc_sanciones')->select('lugar')->distinct()->orderBy('Lugar')->get();

        $divisas = $divisa = DB::table('gesanc_divisas')->get();


        //calcular el estado

        $estado = gesanc_sanciones::select("Inactivo")->where('IDSancion',$id)->firstOrFail();


        //calcular cambio a euros

        $divisa = DB::table('gesanc_divisas')->select('cambio')->whereRaw('Divisa = (select Moneda from gesanc_sanciones where IDSancion = '.$id.')')->get();
        $cambio =""; 

        if($divisa){
             foreach ($divisa as $key => $datos) {
                 $cambio = $datos->cambio;
            }
        }  

        //Hallar el código de la matricula

        $matricula_contratada = DB::table('gesanc_matriculas')->select('FecContr')->whereRaw('Matricula = (select Matricula from eurits_db.gesanc_sanciones where IDSancion = '.$id.')')->get();

        $contratada = true;

        if($matricula_contratada){
             foreach ($matricula_contratada as $key => $datos) {
                 if($datos->FecContr == null){
                    $contratada = false;
                }
            } 
        }

        $IdMatricula = DB::table('gesanc_matriculas')->select('idMatricula')->whereRaw('Matricula = (select Matricula from eurits_db.gesanc_sanciones where IDSancion = '.$id.')')->get();

        $codigo = "";
        $indice = "";
        $sigla = "";

        if($IdMatricula){

            foreach ($IdMatricula as $key => $datos) {
                 if($datos->idMatricula < 100000){
                    $codigo = sprintf("%05d", $datos->idMatricula);
                    $indice = 0;
                }else{
                    $codigo = substr($datos->idMatricula, 1);
                    $indice = round($datos->idMatricula / 100000);
                    
                }
            }
            if(!$contratada){
                $codigo .="X";
            }
        }

        switch ($indice) {
            case 0:
                $sigla ="I";
                break;
            case 1:
                $sigla ="H";
                break;
            case 2:
                $sigla ="S";
                break;
            case 3:
                $sigla ="L";
                break;
            case 4:
                $sigla ="P";
                break;
            case 5:
                $sigla ="G";
                break;
            case 6:
                $sigla ="I";
                break;
            case 7:
                $sigla ="H";
                break;
            case 9:
                $sigla ="M";
                break;
            case 10:
                $sigla ="E";
                break;
            default:
                $sigla ="";
                break;
        }

        $codigo_matricula = $sigla.$codigo;


        return view('sanciones/detail', compact('sancion'),['motivos'=>$motivos,'cliente'=>$cliente,'codigo'=>$codigo_matricula,'cambio'=>$cambio,'conductores'=>$conductores,'boletines'=>$boletines,'autoridades'=>$autoridades,'matriculas'=>$matriculas,'paises'=>$paises,'pais'=>$pais,'lugares'=>$lugares,'divisas'=>$divisas,'estado'=>$estado]);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //TODO
        if($request->ajax()){

            $datos = array();
            parse_str($request->datos,$datos);

            $gestionadogure = 0;
            $estado = 0;
            $justif = 0;
            $fecha_justif = "";
            $codigopais = 0;
            $importe = null;
            $pagado = null;

            $fecha_multa = null;
            
            $fecha_entrada = null;
            
            $fecha_archivado = null;

            $fecha_justif = null;
           
            //echo $datos["empresa"];
            //print_r($datos);

            if(array_key_exists("gestionadogure",$datos)){
               $gestionadogure = 1;
            }
            if(array_key_exists("estado",$datos)){
               $estado = 1;
            }
            if(array_key_exists("justif",$datos)){
               $justif = 1;
            }
            if($datos["pagado"]!=null){
                $pagado = $datos["pagado"];
            }

            if($datos["importe"]!=null){
                $importe = $datos["importe"];
            }

            if($datos["fecha_multa"] !=null){
                $fecha_multa = DateTime::createFromFormat('d/m/Y', $datos["fecha_multa"]);
                date_time_set($fecha_multa, 00, 00);
            }

            if($datos["fecha_entrada"]!=null){
                $fecha_entrada = DateTime::createFromFormat('d/m/Y', $datos["fecha_entrada"]);
                date_time_set($fecha_entrada, 00, 00);
            }

            if($datos["fecha_archivado"]!=null){
                $fecha_archivado = DateTime::createFromFormat('d/m/Y', $datos["fecha_archivado"]);
                date_time_set($fecha_archivado, 00, 00);
            }

            if($datos["fecha_justif"]!=null){
                $fecha_justif = DateTime::createFromFormat('d/m/Y', $datos["fecha_justif"]);
                date_time_set($fecha_justif, 00, 00);
            }


            $paises = DB::table('gesanc_paises')->select(DB::raw("`Codigo Pais` as Codigo"))->whereRaw('Pais ="'.$datos["pais"].'"')->get();

            foreach ($paises as $key => $dato) {
                $codigopais = $dato->Codigo;
            }

            try { 
               gesanc_sanciones::where('IDSancion', $id)->update(
                    ['Matricula' => $datos["matricula"],'cif' => $datos["cif"], 'expediente' => $datos["expediente"], 'Boletin' => $datos["boletin"], 'Conductor' => $datos["conductor"], 'Acuerdo' => $datos["acuerdo"], 'CodigoFT' => $datos["ft"], 'Lugar' => $datos["lugar"], 'Autoridad' => $datos["autoridad"], 'Pais' => $codigopais, 'Importe' => $datos["importe"], 'Moneda' => $datos["divisas"], "Importepagado" => $pagado, 'Notas' => $datos["notas"], 'FechaMulta' => $fecha_multa, 'FECHALLEGADA' => $fecha_entrada, 'FECHACIERRE' => $fecha_archivado, 'GestionadoGURE' => $gestionadogure, 'Inactivo' => $estado, 'RecibidoJP' => $justif, 'fechaJustificante' => $fecha_justif]);

            return  Response("La sanción se ha editado correctamente.");

               //return Response("hecho");

            } catch(QueryException $ex){ 
              
            //dd($ex->getMessage());

            return  Response($ex->getMessage());
              
            }

        }  
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       
    }

    /**
    * Busca una sanción y muestra en la tabla los resultados.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function search(Request $request){

        if($request->ajax()){
            $output="<p style='text-align:center;'>No se han encontrado resultados.</p>";
            $sanciones=gesanc_sanciones::where('IDCONTRATO','LIKE','%'.$request->search.'%')
                                ->orWhere('cif','LIKE','%'.$request->search.'%')
                                ->orWhere('Matricula','LIKE','%'.$request->search.'%')
                                ->orWhere('Autoridad','LIKE','%'.$request->search.'%')
                                ->take(200)
                                ->orderBy('FECHALLEGADA','DESC')
                                ->get();

            if($sanciones){
                foreach ($sanciones as $key => $sancion) {

                    $output.='<tr>'.
                             '<td>'.$sancion->IDCONTRATO.'</td>'.
                             '<td>'.$sancion->expediente.'</td>'.
                             '<td>'.$sancion->Matricula.'</td>'.
                             '<td>'.$sancion->cif.'</td>'.
                             '<td>'.$sancion->Autoridad.'</td>'.
                             '<td>'.date('d - m - Y', strtotime($sancion->FECHALLEGADA)).'</td>'.
                             '<td><a class="btn btn-sm btn-success" href="sanciones/'.$sancion->IDSancion.'"><i class="fa fa-edit"></i> </a> <a class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a></td>'.
                             '</tr>';

                }
                return Response($output);
            }
        }
    }

     /**
    * Filtra la sanción según parámetros
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */

    public function filtrar(Request $request){
            $output="<p style='text-align:center;'>No se han encontrado resultados.</p>";
        

            if($request->ajax()){

            $filtros = array();

            $consulta = "";

              if($request->fecha_inicio != null && $request->fecha_fin !=null){
                $fecha_inicio = $request->fecha_inicio;
                $fecha_fin = $request->fecha_fin;

                $fechallegada = "(FECHALLEGADA BETWEEN '".$fecha_inicio."' AND '".$fecha_fin."')";

                $filtros[] = $fechallegada;

                //$sanciones=gesanc_sanciones::whereRaw($consulta)->get();
              
            }

            if($request->multa_inicio != null && $request->multa_fin !=null){
                $multa_inicio = $request->multa_inicio;
                $multa_fin = $request->multa_fin;

                $fechamulta = "(FECHALLEGADA BETWEEN '".$multa_inicio."' AND '".$multa_fin."')";

                $filtros[] = $fechamulta;

                //$sanciones=gesanc_sanciones::whereRaw($consulta)->get();
              
            }

             if($request->empresa != null){
                $empresa = $request->empresa;
                $filtro_empresa = "cif = (select cif from gesanc_clientes where empresa ='".$empresa."')";
                $filtros[] = $filtro_empresa;
            } 

            if($request->importe != null){
                $importe = $request->importe;
                $filtro_importe = "importe = '".$importe."'";
                $filtros[] = $filtro_importe;
            } 

            if($request->matricula != null){
                $matricula = $request->matricula;
                $filtro_matricula = "matricula = '".$matricula."'";
                $filtros[] = $filtro_matricula;
            } 

            if($request->lugar != null){
                $lugar = $request->lugar;
                $filtro_lugar = "lugar = '".$lugar."'";
                $filtros[] = $filtro_lugar;
            }

             if($request->conductor != null){
                $conductor = $request->conductor;
                $filtro_conductor = "conductor = '".$conductor."'";
                $filtros[] = $filtro_conductor;
            }  

             if($request->autoridad != null){
                $autoridad = $request->autoridad;
                $filtro_autoridad = "Autoridad = '".$autoridad."'";
                $filtros[] = $filtro_autoridad;
            } 

            if($request->pagadoITS != null){
                $pagadoITS = $request->pagadoITS;
                $filtro_pagadoITS = "importepagado = '".$pagadoITS."'";
                $filtros[] = $filtro_pagadoITS;
            }

             if($request->pais != null){
                $pais = $request->pais;
                if($pais=="Espana"){
                    $pais="España";
                }
                $filtro_pais = "pais = (select `codigo pais` from gesanc_paises where pais ='".$pais."')";
                $filtros[] = $filtro_pais;
            }

            for ($i =count($filtros) - 1; $i >= 0 ; $i--) { 
                $consulta .= $filtros[$i];
                $consulta .= " ";

                if($i > 0){
                    $consulta .= "AND ";
                }
            }

            //DB::enableQueryLog();
            
             $sanciones = DB::table('gesanc_sanciones')->orderBy('FECHALLEGADA','DESC')->take(100)->get();

              //dd(DB::getQueryLog());
            
            if($consulta !=""){

                 $sanciones=gesanc_sanciones::whereRaw($consulta)->limit(200)->get();
             }
            
           if($sanciones){
                foreach ($sanciones as $key => $sancion) {

                    $output.='<tr>'.
                             '<td>'.$sancion->IDCONTRATO.'</td>'.
                             '<td>'.$sancion->expediente.'</td>'.
                             '<td>'.$sancion->Matricula.'</td>'.
                             '<td>'.$sancion->cif.'</td>'.
                             '<td>'.$sancion->Autoridad.'</td>'.
                             '<td>'.date('d - m - Y', strtotime($sancion->FECHALLEGADA)).'</td>'.
                             '<td><a class="btn btn-sm btn-success" href="sanciones/'.$sancion->IDSancion.'"><i class="fa fa-edit"></i> </a> <a class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a></td>'.
                             '</tr>';

                }
                return Response($output);
            }
    }
}


 public function choferes()
    {

        $clientes = DB::table('gesanc_clientes')->orderBy('empresa')->get();

        $its = DB::table('gesanc_choferes')->orderBy('Chofer')->get();

        $indufisa = DB::table('gesanc_indufisa_choferes')->orderBy('Chofer')->get();


        //calcular el código de la matricula
        return view('sanciones/choferes', ['clientes' =>$clientes,'its'=>$its,'indufisa'=>$indufisa]);
    }

     public function buscar_choferes(Request $request)
    {


        //$conductores = DB::table('gesanc_choferes')->whereRaw()->orderBy('Chofer')->get();

         if($request->ajax()){

            $nombre_empresa = "";

            $nombre_chofer = $request->nombre_chofer;

            $origen = $request->origen;

            $codcli = "";


            //$clientes = DB::table('gesanc_clientes')->whereRaw("empresa = '".$nombre_empresa."'")->get();

            $clientes = DB::table('gesanc_clientes')->whereRaw("CODCLI =(select CODCLI from eurits_db.gesanc_choferesclientes where CODCHOFER = (select CODCHOFER from eurits_db.gesanc_choferes where Chofer='".$nombre_chofer."'))")->get();

            $cif_empresa =  "";

            foreach ($clientes as $key => $dato) {
                $cif_empresa = $dato->cif;
                $nombre_empresa = $dato->empresa; 
                $codcli = $dato->CODCLI;
            }

            
            
        $output="";

         /*$choferes = DB::select(DB::raw("SELECT a.*, b.mandato FROM eurits_db.gesanc_choferes a,eurits_db.gesanc_choferesclientes b")->whereRaw("a.CODCHOFER=b.CODCHOFER and a.CODCHOFER in select CODCHOFER from eurits_db.gesanc_choferesclientes where codcli = (select CODCLI from eurits_db.gesanc_clientes where cif='"..$cif_empresa"'))")->get();*/
          
          if($origen=="its"){


            $output = '<div class="form-group col-md-6">
                            <label for="matricula">Empresa</label>
                            <input type="text" class="form-control" name="codigo" value="'.$nombre_empresa.'" disabled>                       
                            </div>';

            $output .= '<div class="form-group col-md-6">
                            <label for="matricula">CIF</label>
                            <input type="text" class="form-control" name="codigo" value="'.$cif_empresa.'" disabled> 
                            <input type="hidden" class="form-control" name="codigo" id="codigo_cliente" value="'.$codcli.'">                          
                            </div>';
            $output .= "<table class='table' id='table_motivos'>
                        <thead class='table-header'>
                            <th>Chófer</th>                        
                            <th>Mandato</th>
                            <th>DNI</th>
                            <th></th>
                        </thead>
                        <tbody id='myTable'>";

             $choferes = DB::table(DB::raw("gesanc_choferes a, gesanc_choferesclientes b"))->select(DB::raw("a.*, b.mandato"))->whereRaw("a.CODCHOFER=b.CODCHOFER and a.CODCHOFER in( select CODCHOFER from gesanc_choferesclientes where codcli = (select CODCLI from gesanc_clientes where cif='".$cif_empresa."'))")->get();

             if($choferes->count()){


                foreach ($choferes as $key => $chofer) {

                     $output.='<tr>'.
                            '<td><input type="text" class="form-control nombre_chofer" name="nombre_chofer" value="'.$chofer->Chofer.'">
                            <input type="hidden" id="codchofer" name="nombre_chofer" value="'.$chofer->CODCHOFER.'"></td>'.
                             '<td>';
                    if($chofer->mandato == 1){
                        $output.= '<input type="checkbox" name="mandato" class="mandato" checked>';
                    }else{
                        $output.= '<input type="checkbox" name="mandato" class="mandato">';
                    }
                    $output.='</td>'.
                             '<td><input type="text" class="form-control dni" name="dni" value="'.$chofer->DNI.'"></td>'.
                             '<td><a class="btn btn-sm btn-success btn-edit" href="#"><i class="fa fa-edit"></i></a> <a class="btn btn-sm btn-danger btn-delete"><i class="fa fa-trash"></i></a></td>'.
                             '</tr>';
                }

                $its = DB::table('gesanc_choferes')->orderBy('Chofer')->get();

                 $output.='<tr>';
                 $output.='<td><select class="selectpicker form-control" data-container="body" data-live-search="true" title="buscar..." id="chofer" name="chofer">';
                      foreach ($its as $key => $chofer) {
                            $output.='<option value="'.$chofer->CODCHOFER.'">'.$chofer->Chofer.'</option>';
                        }
                $output.='</select></td>';
                $output.='<td><input type="checkbox" name="mandato" class="mandato" id="mandato"></td>'. 
                     '<td></td>'.
                     '<td><a class="btn btn-success" id="add_chofer"><i class="fa fa-plus"></i></a></td>'.    
                     '</tr>';
            }

            $output .= "</tbody></table>";

         }else{

             $output .= "<table class='table' id='table_motivos'>
                        <thead class='table-header'>
                            <th>CodChofer</th>                        
                            <th>Nombre</th>
                            <th></th>
                        </thead>
                        <tbody id='myTable'>";

            $choferes = DB::table('gesanc_indufisa_choferes')->whereRaw("Chofer='".$nombre_chofer."'")->get();

              foreach ($choferes as $key => $chofer) {

                     $output.='<tr>'.
                            '<td><input type="text" class="form-control" name="expediente" value="'.$chofer->CodChofer.'" disabled></td>';
                    $output.='</td>'.
                             '<td><input type="text" class="form-control dni" name="dni" value="'.$chofer->Chofer.'"></td>'.
                             '<td><a class="btn btn-sm btn-success" href="#"><i class="fa fa-edit"></i></a> <a class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a></td>'.
                             '</tr>';
                }


         }

        
    
            
            return Response($output);
        }

        //calcular el código de la matricula
        //return view('sanciones/choferes', ['clientes' =>$clientes,'conductores'=>$conductores]);
    }

     public function gestion_chofer(Request $request)
    {

         if($request->ajax()){

            $dni = $request->dni;
            $chofer = $request->nombre_chofer;
            $mandato = $request->mandato;
            $gestion = $request->gestion;
            $codchofer= $request->codchofer;
            $codcli = $request->codigo_cliente;

         }

         if($gestion=="editar"){

            DB::table("gesanc_choferes")->where('CODCHOFER', $codchofer)->update(['DNI'=>$dni,'Chofer'=>$chofer]);
            DB::table("gesanc_choferesclientes")->where('CODCHOFER', $codchofer)->update(['mandato'=>$mandato]);
            $output="Chófer editado correctamente";
         }
          if($gestion=="borrar"){

            DB::table("gesanc_choferesclientes")->where('CODCHOFER', $codchofer)->delete();
            $output="Chófer eliminado del cliente correctamente";
         }
         if($gestion=="insertar"){
            DB::enableQueryLog();
            gesanc_choferesclientes::updateOrCreate(
                    ['CODCHOFER' => $codchofer],
                    ['CODCLI'=>$codcli,'CODCHOFER'=>$codchofer,'mandato'=>$mandato]);
            $output=DB::getQueryLog();
         }

          return Response($output);

    }


    public function cesce()
    {

        $clientes = DB::table('gesanc_clientes')->orderBy('empresa')->get();

        //calcular el código de la matricula
        return view('sanciones/cesce', ['clientes' =>$clientes]);
    }

      public function buscar_contratos(Request $request)
    {

        if($request->ajax()){

            $cif= $request->cif;

            $nombre_empresa = $request->nombre_empresa;

            $output="";

            $contratos = DB::table('gesanc_contratos')->select('FechaContrato')->whereRaw("cif='".$cif."'")->distinct()->orderBy('FechaContrato')->get();

            $output.='<div class="form-group col-md-4"><label for="matricula">Contratos</label><select class="selectpicker form-control" data-container="body" data-live-search="true" title="buscar..." id="contratos" name="contratos">';
                      foreach ($contratos as $key => $contrato) {
                            $output.='<option>'.date('d/m/Y', strtotime($contrato->FechaContrato)).'</option>';
                        }
            $output.='</select></div>';

        }
        //calcular el código de la matricula
        return Response($output);
    }

     public function cargar_contratos(Request $request)
    {

        if($request->ajax()){

            $FechaContrato= $request->FechaContrato;
            $cif= $request->cif;

            $output="";

            $contratos = DB::table('gesanc_contratos')->whereRaw("FechaContrato='".$FechaContrato."' and cif='".$cif."'")->orderBy('FechaContrato')->get();

            $output .= "<table class='table' id='tabla_contratos'>
                        <thead class='table-header'>
                            <th>cesce</th>                        
                            <th>Matricula</th>
                            <th>Tipo</th>
                            <th>Alta</th>
                            <th>Baja</th>
                            <th>R1</th>
                            <th>R2</th>
                            <th>R3</th>
                            <th></th>
                        </thead>
                        <tbody id='myTable'>";
                    
                    foreach ($contratos as $key => $contrato) {
                     $output.='<tr>';
                    if($contrato->CESCE == 1){
                        $output.= '<td><input type="checkbox" name="CESCE" class="CESCE" checked></td>';
                    }else{
                        $output.= '<td><input type="checkbox" name="CESCE" class="CESCE"></td>';
                    }
                    $output.='<td>'.$contrato->Matricula.'</td>'.
                             '<td>'.$contrato->Tipo.'</td>'.
                             '<td>'.date('d - m - Y', strtotime($contrato->Alta)).'</td>'.
                             '<td>'.date('d - m - Y', strtotime($contrato->Baja)).'</td>'.
                             '<td>'.$contrato->R1.'</td>'.
                             '<td>'.$contrato->R2.'</td>'.
                             '<td>'.$contrato->R3.'</td>'.
                             '<td><a class="btn btn-sm btn-success" href="#"><i class="fa fa-edit"></i></a> <a class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a></td>'.
                             '</tr>';
                }
           $output .= "</tbody></table>";

        }
        //calcular el código de la matricula
        return Response($output);
    }



/**
     * Exporta a documento Word el registro.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

public function exportar (Request $request, $id)
{

    //echo public_path();

    $templateWord = new TemplateProcessor('..\public\templates\gesanc_sanciones\Portada_sancion.docx');

    $codigosancion = $_GET["codigosancion"];
    $empresa = $_GET["empresa"];
    $cif = $_GET["cif"];
    $lista_motivos ="";

    $sancion = gesanc_sanciones::where('IDSancion',$id)->get();

    $contratos = gesanc_contratos::whereRaw('IDCONTRATO = (select idcontrato from gesanc_sanciones where IDSancion ='.$id.')')->get();

    
    $motivos = DB::table(DB::raw("gesanc_lista_motivos_denuncia a, gesanc_motivos b"))->select(DB::raw("a.*, b.MOTIVO"))->whereRaw("a.Idmotivo = b.COD_MOTIVO and a.IDSancion ='".$id."'")->get();

    $clientes = DB::table('gesanc_clientes')->whereRaw('cif ="'.$cif.'"')->get();



            if($sancion){
                foreach ($sancion as $key => $datos) {
                $templateWord->setValue('nombre_empresa',$empresa);
                $templateWord->setValue('boletin',$datos->Boletin);
                $templateWord->setValue('expediente',$datos->expediente);
                $templateWord->setValue('matricula',$datos->Matricula);
                $templateWord->setValue('fecha_multa',date('d - m - Y', strtotime($datos->FechaMulta)));
                $templateWord->setValue('importe',$datos->Importe);
                $templateWord->setValue('divisa',$datos->Moneda);
                $templateWord->setValue('codigo',$codigosancion);
                $templateWord->setValue('notas',$datos->Notas);

                

                if($datos->RecibidoJP==1){
                $templateWord->setValue('justif',"Si");
                }else{
                $templateWord->setValue('justif',"No");
                }

            }

            if($clientes){
                foreach ($clientes as $key => $cliente) {
                $templateWord->setValue('telefono',$cliente->tel1);
                $templateWord->setValue('fax',$cliente->fax1);
                $templateWord->setValue('movil',$cliente->movil1);
                $templateWord->setValue('fax_its',$cliente->Dato_ITS);
                }

            }

             if($contratos){
                foreach ($contratos as $key => $contrato) {
                $templateWord->setValue('fecha_contrato',date('d - m - Y', strtotime($contrato->FechaContrato)));
                $templateWord->setValue('fecha_alta',date('d - m - Y', strtotime($contrato->Alta)));
                $templateWord->setValue('tipo',$contrato->Tipo);
                }

            }

            if($motivos){
                foreach ($motivos as $key => $motivo) {
                $lista_motivos.= $motivo->MOTIVO."<w:br/>";
                }

            }

            $templateWord->setValue('motivos_multa',$lista_motivos);


            // --- Guardamos el documento

    $templateWord->saveAs('Documento02.docx');

//echo file_get_contents('Documento02.docx');

 header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename=Documento02.docx; charset=iso-8859-1');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize('Documento02.docx'));
    readfile('Documento02.docx');
    exit;
}

}

  /**
    * Devuelve las reducciones de una sanción
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */

public function reducciones(Request $request)
    {

          if($request->ajax()){

            $id = $request->IDSancion; 

            $reducciones=gesanc_reduccion::select(DB::raw("*, `ID-REDUCCION` as REDUCCION"))->where('idsancion',"=",$id)->get();

            $divisas = DB::table("gesanc_divisas")->get();


            $output = "<table class='table'>
                        <thead class='table-header'>
                            <th>Fecha</th>                        
                            <th>Importe</th>
                            <th>Moneda</th>
                            <th></th>
                        </thead>
                        <tbody id='myTable'>";

            if($reducciones->count()){

                foreach ($reducciones as $key => $reduccion) {

                     $output.='<tr>'.
                             '<td>'.date('d - m - Y', strtotime($reduccion->fecha)).'</td>'.
                             '<td>'.$reduccion->importe.'</td>'.
                             '<td>'.$reduccion->Moneda.'</td>'.
                             '<td><a class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a><input type="hidden" class="ID" value="'.$reduccion->REDUCCION.'"></td>'.
                             '</tr>';

                }
            }

                     $output.='<tr>'.
                              '<td><input type="text" class="form-control datepicker" name="fecha_reduccion" id="fecha_reduccion" value="" placeholder="fecha..."></td>'.
                              '<td><input type="text" class="form-control" name="importe" id="importe" value=""></td>';

                    $output.='<td><select class="selectpicker" data-container="body" data-live-search="true" title="buscar..." id="id_moneda" name="id_moneda">';
                      foreach ($divisas as $key => $divisa) {
                            $output.='<option>'.$divisa->divisa.'</option>';
                        }
                    $output.='</select></td>';
                              
                    $output.='<td><a class="btn btn-success" id="add_reduccion"><i class="fa fa-plus"></i></a></td></tr>';

            $output .= "</tbody></table>";

             return Response($output);
        }
    }

    public function motivos(Request $request)
    {

          if($request->ajax()){

            $id = $request->IDSancion; 

            
             $output = "<table class='table' id='table_motivos'>
                        <thead class='table-header'>
                            <th>ID</th>                        
                            <th>Motivo</th>
                            <th></th>
                            <th style='display:none'></th>
                        </thead>
                        <tbody id='myTable'>";
            $motivos = DB::table(DB::raw("gesanc_lista_motivos_denuncia a, gesanc_motivos b"))->select(DB::raw("a.*, b.MOTIVO, b.COD_MOTIVO"))->whereRaw("a.Idmotivo = b.COD_MOTIVO and a.IDSancion ='".$id."'")->get();

            $lista_motivos = DB::table("gesanc_motivos")->get();

            if($motivos->count()){

               

                foreach ($motivos as $key => $motivo) {

                     $output.='<tr>'.
                              '<td class="ID" style="display:none">'.$motivo->IdListaMotivos.'</td>'.
                             '<td>'.$motivo->COD_MOTIVO.'</td>'.
                             '<td>'.$motivo->MOTIVO.'</td>'.
                             '<td><a class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a></td>'.
                             '</tr>';

                }


                
            }
            $output.='<tr>'.
                     '<td></td>'.   
                     '<td><select class="selectpicker" data-container="body" data-live-search="true" title="buscar..." id="id_motivo" name="id_motivo">';
                      foreach ($lista_motivos as $key => $motivo) {
                            $output.='<option>'.$motivo->MOTIVO.'</option>';
                        }
            $output.='</select></td>';

            $output.='<td><a class="btn btn-success" id="add_motivo"><i class="fa fa-plus"></i></a></td>'.
                     '</tr>';


            $output .= "</tbody></table><input type='hidden' id='tabla_tipo' value='motivo'>";
            return Response($output);
        }
    }

     public function tramites(Request $request)
    {

          if($request->ajax()){

            $id = $request->IDSancion; 

            
             $output = "<table class='table'>
                        <thead class='table-header'>
                            <th>PRIV</th>                        
                            <th>Fecha</th>
                            <th>Código</th>
                            <th>Trámite</th>
                            <th>Carta</th>
                            <th></th>
                        </thead>
                        <tbody id='myTable'>";


             $tramites =  DB::table(DB::raw("gesanc_recursos a, gesanc_tramites b"))->select(DB::raw("a.*, b.TRAMITE"))->whereRaw("a.cod_tramite = b.CODIGO and a.id_sancion ='".$id."'")->get();

             $select_tramite = DB::table('gesanc_tramites')->get();

            if($tramites->count()){

               

                foreach ($tramites as $key => $tramite) {

                     $output.='<tr>'.
                              '<td>';
                     if($tramite->privado == 0){

                    $output.= "<input type='checkbox' name='privado'>";

                    } else{

                    $output.= "<input type='checkbox' name='privado' checked>";

                    }
                    $output.='</td>';
            
                    $output.='<td>'.date('d - m - Y', strtotime($tramite->fecha)).'</td>';

                    $output.='<td class="ID">'.$tramite->cod_tramite.'</td>';

                    $output.='<td>'.$tramite->TRAMITE.'</td>';

                    $output.= "<td><input type='checkbox' name='carta'></td>";

                    $output.='<td><a class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a><input type="hidden" class="ID" value="'.$tramite->id_recurso.'"></td>'.'</tr>';

                }

            }

            $output.= "<tr><td><input type='checkbox' name='privado'></td>";

            $output.='<td><input type="text" class="form-control datepicker" name="fecha_tramite" id="fecha_tramite" value="" placeholder="fecha..."></td>';

            $output.='<td></td>';

            $output.='<td><select class="selectpicker form-control" data-container="body" data-live-search="true" title="buscar..." id="id_tramite" name="id_tramite">';
                      foreach ($select_tramite as $key => $tramite) {
                            $output.='<option>'.$tramite->TRAMITE.'</option>';
                        }
            $output.='</select></td>';

            $output.= "<td><input type='checkbox' name='carta'></td>";

            $output.='<td><a class="btn btn-success" id="add_tramite"><i class="fa fa-plus"></i></a></td>'.
                     '</tr>';
          

            $output .= "</tbody></table><input type='hidden' id='tabla_tipo' value='tramites'>";
            return Response($output);
        }
    }

    public function contencioso(Request $request)
    {

          if($request->ajax()){

            $id = $request->IDSancion; 

            
            $output="<p style='text-align:center;'>No se han encontrado resultados.</p>";
            $contencioso = gesanc_contencioso::where('IDSancion',$id)->get();

            if($contencioso->count()){
              foreach ($contencioso as $key =>$datos) {
              $output = "<div class='panel-body' style='border-top:2px solid #335599;'><form class='inline-form'>";

              $output.= "<div class='form-group col-md-3'>".
                        "<label for='Entregado'>Entregado</label>".
                        "<input type='text' class='form-control datepicker' placeholder='fecha...' name='fechaEntrega' id='fechaEntrega' value='".date('d/m/Y', strtotime($datos->FechaEntrega))."'>".
                        "</div>";

              $output.= "<div class='form-group col-md-3'>".
                        "<label for='Letrado'>Letrado</label>".
                        "<input type='text' class='form-control' name='Letrado' id='Letrado' value='".$datos->Letrado."'>".
                        "</div>";

              $output.= "<div class='form-group col-md-3'>".
                        "<label for='Recurso'>Recurso</label>".
                        "<input type='text' class='form-control' name='Recurso' id='Recurso' value='".$datos->Recurso."'>".
                        "</div>";

              $output.= "<div class='form-group col-md-3'>".
                        "<label for='Honorarios'>Honorarios pagados</label>";

              if($datos->HonorariosPagados == 0){

                    $output.= "<input type='checkbox' id='honorarios' name='honorarios'></div>";

                    } else{

                    $output.= "<input type='checkbox' name='Honorarios' checked></div>";

                    }

                $output.= "<div class='form-group col-md-3'>".
                          "<label for='Viable'>Viable";

             if($datos->Viable == 0){
                
                    $output.= "<input type='checkbox' name='Viable'></label></div>";

                    } else{

                    $output.= "<input type='checkbox' id='Viable' name='Viable' checked></label></div>";

                    }

                    $output.="<div class='form-group col-md-3'>".
                             " <a href='#'' id='save_contencioso'><i class='fa fa-save'> </i> Guardar</a>".
                             "</div>";

                    $output.= " </form></div>";
              }  


            }else{

              $output = "<div class='panel-body' style='border-top:2px solid #335599;'><form class='inline-form'>";

              $output.= "<div class='form-group col-md-3'>".
                        "<label for='Entregado'>Entregado</label>".
                        "<input type='text' class='form-control datepicker' placeholder='fecha...' name='fechaEntrega' id='fechaEntrega' value=''>".
                        "</div>";

              $output.= "<div class='form-group col-md-3'>".
                        "<label for='Letrado'>Letrado</label>".
                        "<input type='text' class='form-control' name='Letrado' id='Letrado' value=''>".
                        "</div>";


              $output.= "<div class='form-group col-md-3'>".
                        "<label for='Recurso'>Recurso</label>".
                        "<input type='text' class='form-control' name='Recurso' id='Recurso'  value=''>".
                        "</div>";

              $output.= "<div class='form-group col-md-3'>".
                        "<label for='Honorarios'>Honorarios pagados </label>".
                        "<input type='checkbox' id='honorarios' name='honorarios'>".
                        "</div>";

              $output.= "<div class='form-group col-md-3'>".
                        "<label for='Viable'>Viable </label>".
                        "<input type='checkbox' id='Viable' name='Viable'>".
                        "</div>";

             $output.="<div class='form-group col-md-3'>".
                             "<a href='#'' id='save_contencioso'><i class='fa fa-save'> </i> Guardar</a>".
                             "</div>";

             $output.= " </form></div>";




            }
            
            //$output="<p style='text-align:center;'>VEVES.</p>";
            
            return Response($output);
        }
    }

    public function pendiente(Request $request){

        if($request->ajax()){

            $id = $request->IDSancion; 

            $output="<p style='text-align:center;'>No se han encontrado resultados.</p>";

            $pendiente = gesanc_sanciones::where('IDSancion',$id)->get();

            if($pendiente){
                foreach ($pendiente as $key => $datos) {

                $output = "<div class='panel-body' style='border-top:2px solid #335599;'><form class='inline-form'>";

                $output.= "<div class='form-group col-md-3'>".
                        "<label for='Notaspendiente'>Notas Pendiente</label>".
                        "<textarea type='textarea' class='form-control' name='Notaspendiente' id='Notaspendiente'>".$datos->Nota_pdte."</textarea>".
                        "</div>";

                $output.="<div class='form-group col-md-3'>";

                if($datos->marca_pdte == null){
                $output.="<input type='radio' name='Estado'id='sinDatos' value='Sindatos' checked>Sin datos<br>".
                         "<input type='radio' name='Estado' id='Pendiente'  value='Pendiente'>Pendiente<br>".
                         "<input type='radio' name='Estado' id='Pagada'  value='Pagada'>Pagada<br>".
                         "<input type='radio' name='Estado' id='noPagada'  value='NoPagada'>No Pagada<br>".
                         "</div>";
                }

                if($datos->marca_pdte == 1){
                $output.="<input type='radio' name='Estado' id='sinDatos'  value='Sindatos'>Sin datos<br>".
                         "<input type='radio' name='Estado' id='Pendiente'  value='Pendiente' checked>Pendiente<br>".
                         "<input type='radio' name='Estado' id='Pagada'  value='Pagada'>Pagada<br>".
                         "<input type='radio' name='Estado' id='noPagada'  value='NoPagada'>No Pagada<br>".
                         "</div>";
                }

                if($datos->marca_pdte == 2){
                $output.="<input type='radio' name='Estado' id='sinDatos'  value='Sindatos'>Sin datos<br>".
                         "<input type='radio' name='Estado' id='Pendiente'  value='Pendiente'>Pendiente<br>".
                         "<input type='radio' name='Estado' id='Pagada'  value='Pagada' checked>Pagada<br>".
                         "<input type='radio' name='Estado' id='noPagada'  value='NoPagada'>No Pagada<br>".
                         "</div>";
                }

                if($datos->marca_pdte == 3){
                $output.="<input type='radio' name='Estado' id='sinDatos'  value='Sindatos'>Sin datos<br>".
                         "<input type='radio' name='Estado' id='Pendiente'  value='Pendiente'>Pendiente<br>".
                         "<input type='radio' name='Estado' id='Pagada'  value='Pagada'>Pagada<br>".
                         "<input type='radio' name='Estado' id='noPagada'  value='NoPagada' checked>No Pagada<br>".
                         "</div>";
                }

                 $output.="<div class='form-group col-md-3'>".
                             " <a href='#'' id='save_pendiente'><i class='fa fa-save'> </i> Guardar</a>".
                             "</div>";

                 $output.= " </form></div>";




            }

                
            }

            return Response($output);
        }
    }


 public function insertar(Request $request){

if($request->ajax()){  

    $action = $request->action;
    $IDSancion = $request->IDSancion;
   
    $output = "<i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Se han encontrado errores.</strong> No se ha podido llevar a cabo la inserción de datos.Si el problema persiste, póngase en contacto con el administrador.";

    switch ($action) {
        case 1:

            $motivo = $request->motivo;
            $cod_motivo = 0;
            $consulta = DB::table("gesanc_motivos")->select("COD_MOTIVO")->whereRaw('MOTIVO ="'.$motivo.'"')->get();

            if($consulta){
                foreach ($consulta as $key => $datos) {
                     $cod_motivo = $datos->COD_MOTIVO;
                }

                 DB::table('gesanc_lista_motivos_denuncia')->insert(
                array('IDSancion' => $IDSancion,
                        'Idmotivo' => $cod_motivo));

            $output = "<a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a><i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Datos insertados correctamente.</strong>";
            }

             return Response($output);

            break;

            case 2:

            $tramite = $request->tramite;
            $fecha = $request->fecha_tramite;
            $cod_tramite = 0;
            $consulta = DB::table("gesanc_tramites")->select("CODIGO")->whereRaw('TRAMITE ="'.$tramite.'"')->get();

            if($consulta){
                foreach ($consulta as $key => $datos) {
                     $cod_tramite = $datos->CODIGO;
                }

                 DB::table('gesanc_recursos')->insert(
                array('id_sancion' => $IDSancion,
                        'fecha' => $fecha,
                        'cod_tramite' => $cod_tramite));

            $output = "<a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a><i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Datos insertados correctamente.</strong>";
            }

            return Response($output);

           
            break;

            case 3:

            $moneda = $request->moneda;
            $fecha = $request->fecha_reduccion;
            $importe = $request->importe;
            $cif = 0;
            $matricula = 0;

            $consulta = DB::table("gesanc_sanciones")->whereRaw('IDSancion ="'.$IDSancion.'"')->get();

            if($consulta){
                foreach ($consulta as $key => $datos) {
                     $cif = $datos->cif;
                     $matricula = $datos->Matricula;
                }

                 DB::table('gesanc_reduccion')->insert(
                array('idsancion' => $IDSancion,
                        'fecha' => $fecha,
                        'cif' => $cif,
                        'Matricula' => $matricula,
                        'importe' => $importe,
                        'Moneda' => $moneda));

            $output = "<a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a><i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Datos insertados correctamente.</strong>";
            }

            return Response($output);

            break;

            case 4:

            $letrado = $request->letrado;
            $recurso = $request->recurso;
            $fechaEntrega = $request->fechaEntrega;
            $honorarios = $request->honorarios;
            $viable = $request->viable;

            $IdContencioso = 0;

            $consulta = DB::table("gesanc_contencioso")->select("IdContencioso")->whereRaw('IDSancion ="'.$IDSancion.'"')->get();

            if($consulta){
                foreach ($consulta as $key => $datos) {
                     $IdContencioso = $datos->IdContencioso;
                    
                }

            }

             gesanc_contencioso::updateOrCreate(
                    ['IdContencioso' => $IdContencioso],
                    ['IDSancion' => $IDSancion, 'FechaEntrega' => $fechaEntrega, 'Letrado' => $letrado, 'Recurso' => $recurso, 'HonorariosPagados' => $honorarios, 'Viable' => $viable]);

            $output = "<a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a><i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Datos insertados correctamente.</strong>";

            return Response($output);


            break;

            case 5:

            $notasPendiente = $request->Notaspendiente;

            if($request->marca_pdte == 'sinDatos'){
                gesanc_sanciones::where('IDSancion', $IDSancion)->update(
                    ['nota_pdte' => $notasPendiente]);
            }

            if($request->marca_pdte == 'Pendiente'){
                gesanc_sanciones::where('IDSancion', $IDSancion)->update(
                    ['nota_pdte' => $notasPendiente, 'marca_pdte' => 1]);
            }

             if($request->marca_pdte == 'Pagada'){
                gesanc_sanciones::where('IDSancion', $IDSancion)->update(
                    ['nota_pdte' => $notasPendiente, 'marca_pdte' => 2]);
            }

             if($request->marca_pdte == 'No Pagada'){
                gesanc_sanciones::where('IDSancion', $IDSancion)->update(
                    ['nota_pdte' => $notasPendiente, 'marca_pdte' => 3]);
            }


            $output = "<a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a><i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Datos insertados correctamente.</strong>";

            return Response($output);

            break;
        
        default:
            # code...
            break;

            
    }

    

}

 }

 public function borrar (Request $request){
if($request->ajax()){  

$ID = $request->ID;
$tabla = $request->tabla;
$field = $request->field;

DB::table($tabla)->where($field, '=', $ID)->delete();

$output = "<a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a><i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Datos eliminados correctamente.</strong>";

return Response($output);

}

 }

public function new_motivo (Request $request){
if($request->ajax()){  


$motivo = $request->motivo;


$id = DB::table('gesanc_motivos')->insertGetId(
    ['MOTIVO' => $motivo]);

$output = "<a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a><i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Se ha añadido correctamente.</strong>";

return Response($output);

}

 }

public function new_tramite (Request $request){
if($request->ajax()){  


$tramite = $request->tramite;


$id = DB::table('gesanc_tramites')->insertGetId(
    ['TRAMITE' => $tramite]);

$output = "<a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a><i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Se ha añadido correctamente.</strong>";

return Response($output);

}

 }


 public function exportar_excel(Request $request){



            $filtros = array();

            $consulta = "";

              if($request->fecha_inicio != null && $request->fecha_fin !=null){
                $fecha_inicio = $request->fecha_inicio;
                $fecha_fin = $request->fecha_fin;

                $fechallegada = "(FECHALLEGADA BETWEEN '".$fecha_inicio."' AND '".$fecha_fin."')";

                $filtros[] = $fechallegada;

                //$sanciones=gesanc_sanciones::whereRaw($consulta)->get();
              
            }

            if($request->multa_inicio != null && $request->multa_fin !=null){
                $multa_inicio = $request->multa_inicio;
                $multa_fin = $request->multa_fin;

                $fechamulta = "(FECHALLEGADA BETWEEN '".$multa_inicio."' AND '".$multa_fin."')";

                $filtros[] = $fechamulta;

                //$sanciones=gesanc_sanciones::whereRaw($consulta)->get();
              
            }

             if($request->empresa != null){
                $empresa = $request->empresa;
                $filtro_empresa = "cif = (select cif from gesanc_clientes where empresa ='".$empresa."')";
                $filtros[] = $filtro_empresa;
            } 

            if($request->importe != null){
                $importe = $request->importe;
                $filtro_importe = "importe = '".$importe."'";
                $filtros[] = $filtro_importe;
            } 

            if($request->matricula != null){
                $matricula = $request->matricula;
                $filtro_matricula = "matricula = '".$matricula."'";
                $filtros[] = $filtro_matricula;
            } 

            if($request->lugar != null){
                $lugar = $request->lugar;
                $filtro_lugar = "lugar = '".$lugar."'";
                $filtros[] = $filtro_lugar;
            }

             if($request->conductor != null){
                $conductor = $request->conductor;
                $filtro_conductor = "conductor = '".$conductor."'";
                $filtros[] = $filtro_conductor;
            }  

             if($request->autoridad != null){
                $autoridad = $request->autoridad;
                $filtro_autoridad = "Autoridad = '".$autoridad."'";
                $filtros[] = $filtro_autoridad;
            } 

            if($request->pagadoITS != null){
                $pagadoITS = $request->pagadoITS;
                $filtro_pagadoITS = "importepagado = '".$pagadoITS."'";
                $filtros[] = $filtro_pagadoITS;
            }

             if($request->pais != null){
                $pais = $request->pais;
                if($pais=="Espana"){
                    $pais="España";
                }
                $filtro_pais = "pais = (select `codigo pais` from gesanc_paises where pais ='".$pais."')";
                $filtros[] = $filtro_pais;
            }

            for ($i =count($filtros) - 1; $i >= 0 ; $i--) { 
                $consulta .= $filtros[$i];
                $consulta .= " ";

                if($i > 0){
                    $consulta .= "AND ";
                }
            }

            //DB::enableQueryLog();
            
             $sanciones = DB::table('gesanc_sanciones')->orderBy('FECHALLEGADA','DESC')->take(200)->get();

              //dd(DB::getQueryLog());
            
            if($consulta !=""){

                 $sanciones=gesanc_sanciones::whereRaw($consulta)->limit(200)->get();
             }

        $objPHPExcel = new PHPExcel();
        // Se asignan las propiedades del libro
        $objPHPExcel->getProperties()->setCreator("ITS") // Nombre del autor
        ->setLastModifiedBy("ITS") //Ultimo usuario que lo modificó
        ->setTitle("Sanciones filtradas") // Titulo
        ->setDescription("Sanciones filtradas"); //Descripción
        $titulosColumnas = array('Contrato'
                                ,'Expediente'
                                ,'Matricula'
                                ,'CIF'
                                ,'Autoridad'
                                ,'Fecha LLegada');
        
        // Se combinan las celdas, para colocar ahí el titulo del reporte
        //$objPHPExcel->setActiveSheetIndex(0)
        //->mergeCells('A1:D1');
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1', $titulosColumnas[0])
        ->setCellValue('B1', $titulosColumnas[1])
        ->setCellValue('C1', $titulosColumnas[2])
        ->setCellValue('D1', $titulosColumnas[3])
        ->setCellValue('E1', $titulosColumnas[4])
        ->setCellValue('F1', $titulosColumnas[5]);

        $i=2;
        foreach ($sanciones as $key => $sancion) {
              $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.$i, $sancion->IDCONTRATO)
            ->setCellValue('B'.$i, $sancion->expediente)
            ->setCellValue('C'.$i, $sancion->Matricula)
            ->setCellValue('D'.$i, $sancion->cif)
            ->setCellValue('E'.$i, $sancion->Autoridad)
            ->setCellValue('F'.$i, date('d - m - Y', strtotime($sancion->FECHALLEGADA)));
            $i++;
        }



    header('Content-Encoding: UTF-8');
    header('Content-type: text/csv; charset=UTF-8');
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="sanciones_filtradas.xlsx"');
    header('Cache-Control: max-age=0');
    header("Pragma: no-cache");
    header("Expires: 0");
    header('Content-Transfer-Encoding: binary');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');

    }

public function mostrar_informe_sanciones(){
     $output = "<label>Agrupar por: </label>";
            $output.='<ul class="list-inline">
                         <li><input type="radio" name="tipofac" value="year" placeholder="year" checked> <span>Año</span></li>
                          <li><input type="radio" name="tipofac" value="motivo" placeholder="Motivo"> <span>Motivo</span></li>
                          <li><input type="radio" name="tipofac" value="motivo" placeholder="Motivo"> <span>Matricula</span></li>
                             </ul>';
              $output .= "<br><label>¿Presentar trámites?</label>";

              $output.= '<br><input type="checkbox" name="presentar_tramites" class="presentar_tramites" checked>';

               $output .= "<br><label>Importante: El informe tomará los valores de busqueda actuales</label>";

            return Response($output);

}

public function generar_informe(Request $request){



            $filtros = array();

            $consulta = "";

              if($request->fecha_inicio != null && $request->fecha_fin !=null){
                $fecha_inicio = $request->fecha_inicio;
                $fecha_fin = $request->fecha_fin;

                $fechallegada = "(FECHALLEGADA BETWEEN '".$fecha_inicio."' AND '".$fecha_fin."')";

                $filtros[] = $fechallegada;

                //$sanciones=gesanc_sanciones::whereRaw($consulta)->get();
              
            }

            if($request->multa_inicio != null && $request->multa_fin !=null){
                $multa_inicio = $request->multa_inicio;
                $multa_fin = $request->multa_fin;

                $fechamulta = "(FECHALLEGADA BETWEEN '".$multa_inicio."' AND '".$multa_fin."')";

                $filtros[] = $fechamulta;

                //$sanciones=gesanc_sanciones::whereRaw($consulta)->get();
              
            }

             if($request->empresa != null){
                $empresa = $request->empresa;
                $filtro_empresa = "cif = (select cif from gesanc_clientes where empresa ='".$empresa."')";
                $filtros[] = $filtro_empresa;
            } 

            if($request->importe != null){
                $importe = $request->importe;
                $filtro_importe = "importe = '".$importe."'";
                $filtros[] = $filtro_importe;
            } 

            if($request->matricula != null){
                $matricula = $request->matricula;
                $filtro_matricula = "matricula = '".$matricula."'";
                $filtros[] = $filtro_matricula;
            } 

            if($request->lugar != null){
                $lugar = $request->lugar;
                $filtro_lugar = "lugar = '".$lugar."'";
                $filtros[] = $filtro_lugar;
            }

             if($request->conductor != null){
                $conductor = $request->conductor;
                $filtro_conductor = "conductor = '".$conductor."'";
                $filtros[] = $filtro_conductor;
            }  

             if($request->autoridad != null){
                $autoridad = $request->autoridad;
                $filtro_autoridad = "Autoridad = '".$autoridad."'";
                $filtros[] = $filtro_autoridad;
            } 

            if($request->pagadoITS != null){
                $pagadoITS = $request->pagadoITS;
                $filtro_pagadoITS = "importepagado = '".$pagadoITS."'";
                $filtros[] = $filtro_pagadoITS;
            }

             if($request->pais != null){
                $pais = $request->pais;
                if($pais=="Espana"){
                    $pais="España";
                }
                $filtro_pais = "pais = (select `codigo pais` from gesanc_paises where pais ='".$pais."')";
                $filtros[] = $filtro_pais;
            }

            for ($i =count($filtros) - 1; $i >= 0 ; $i--) { 
                $consulta .= $filtros[$i];
                $consulta .= " ";

                if($i > 0){
                    $consulta .= "AND ";
                }
            }

            //DB::enableQueryLog();
            
             //$sanciones = DB::table('gesanc_sanciones')->orderBy('FECHALLEGADA','DESC')->take(200)->get();

            $clientes = DB::table('gesanc_clientes')->whereRaw("cif in (select cif from gesanc_sanciones)")->orderBy('empresa')->take(200)->get();

              //dd(DB::getQueryLog());
            
            if($consulta !=""){

                 //$sanciones=gesanc_sanciones::whereRaw($consulta)->limit(200)->get();

            $clientes = DB::table('gesanc_clientes')->whereRaw("cif in (select cif from gesanc_sanciones where ".$consulta.")")->orderBy('empresa')->take(200)->get();
             }


$documentos = array();

foreach ($clientes as $key => $cliente) {
    
    $filename = '..\public\temp\informe_sanciones_year_'. $key . '.docx';
    $documentos [] = $filename;
    $templateWord = new TemplateProcessor('..\public\templates\gesanc_sanciones\informe_sanciones_year.docx');
    $templateWord->setValue('empresa',$cliente->empresa);
    $templateWord->saveAs(storage_path($filename));

    $years = DB::table('gesanc_sanciones')->selectRaw("distinct(year(FechaMulta)) as 'year'")->whereRaw("cif = '".$cliente->cif."'")->get();
    foreach ($years as $key => $year) {
    $sanciones = DB::table('gesanc_sanciones')->whereRaw("cif = '".$cliente->cif."' and year(FechaMulta)='".$year->year."'")->get();
    $nombre_doc_year = '..\public\temp\sanciones_year'. $key . '.docx';
    $documentos [] = $nombre_doc_year;
    $template_year = new TemplateProcessor('..\public\templates\gesanc_sanciones\sanciones_year.docx');
    $template_year->setValue('year',$year->year);
    $lista_matricula ="";
    $lista_fecha ="";
    $lista_expediente ="";
    $lista_motivos ="";
    $lista_tramites ="";
    $lista_importe ="";
    $lista_reduccion ="";
    foreach ($sanciones as $key => $sancion) {
    $lista_matricula .= $sancion->Matricula."<w:br/>";
    $lista_fecha .= date('d - m - Y', strtotime($sancion->FechaMulta))."<w:br/>";
    $lista_expediente .= $sancion->expediente."<w:br/>";
    
    if($sancion->codigosancion!=null){
    $motivo = DB::table("gesanc_motivos")->whereRaw("COD_MOTIVO = (select codigosancion from gesanc_sanciones where codigosancion='".$sancion->codigosancion."')")->first();
    $lista_motivos .=$motivo->MOTIVO."<w:br/>";
    }else{
        $lista_motivos .="<w:br/>";
        }
    }
    $template_year->setValue('matricula',$lista_matricula);
    $template_year->setValue('fecha',$lista_fecha);
    $template_year->setValue('expediente',$lista_expediente);
    $template_year->setValue('motivo_sancion',$lista_motivos);
    $template_year->saveAs(storage_path($nombre_doc_year));
    }
}

    $dm = new DocxMerge();
    $dm->merge( $documentos, '..\public\temp\informe_sanciones.docx' );
 
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename=informe_sanciones.docx; charset=iso-8859-1');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize('..\public\temp\informe_sanciones.docx'));
    readfile('..\public\temp\informe_sanciones.docx');

    $files = glob('..\public\temp\*'); // get all file names
    foreach($files as $file){ // iterate files
      if(is_file($file))
        unlink($file); // delete file
        }
  exit;


}

}