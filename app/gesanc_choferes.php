<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $CODCHOFER
 * @property string $DNI
 * @property string $Chofer
 * @property GesancChoferescliente[] $gesancChoferesclientes
 */
class gesanc_choferes extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['DNI', 'Chofer'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function gesancChoferesclientes()
    {
        return $this->hasMany('App\GesancChoferescliente', 'CODCHOFER', 'CODCHOFER');
    }
}
