<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $CODCHOFER
 * @property integer $CODCLI
 * @property integer $mandato
 * @property GesancChoferes $gesancChoferes
 * @property GesancClientes $gesancClientes
 */
class gesanc_choferesclientes extends Model
{

    public $timestamps = false;
    protected $primaryKey = 'CODCHOFER';
    /**
     * @var array
     */
    protected $fillable = ['CODCHOFER', 'CODCLI', 'mandato'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function gesancChoferes()
    {
        return $this->belongsTo('App\GesancChoferes', 'CODCHOFER', 'CODCHOFER');
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function gesancClientes()
    {
        return $this->belongsTo('App\GesancClientes', 'CODCLI', 'CODCLI');
    }
}
