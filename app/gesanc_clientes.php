<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property boolean $delegacion
 * @property integer $CODCLI
 * @property string $cif
 * @property string $empresa
 * @property string $direccion
 * @property string $cp
 * @property string $poblacion
 * @property string $provincia
 * @property string $pais
 * @property string $tel1
 * @property string $tel2
 * @property string $movil1
 * @property string $movil2
 * @property string $fax1
 * @property string $fax2
 * @property integer $Mailing
 * @property string $Observacion
 * @property string $email
 * @property string $url
 * @property string $Id-comercial
 * @property integer $PN
 * @property integer $cliente
 * @property string $fechaintrodatos
 * @property string $fechamodifacion
 * @property integer $mandato
 * @property integer $choferes
 * @property string $PWD
 * @property string $Dato_ITS
 * @property integer $impagado_pdteDev
 * @property string $CC
 * @property integer $cteConflictivo
 * @property string $apartadoCorreos
 * @property GesancEmpresa $gesancEmpresa
 * @property GesancChoferescliente[] $gesancChoferesclientes
 * @property GesancContrato[] $gesancContratos
 */
class gesanc_clientes extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['delegacion', 'cif', 'empresa', 'direccion', 'cp', 'poblacion', 'provincia', 'pais', 'tel1', 'tel2', 'movil1', 'movil2', 'fax1', 'fax2', 'Mailing', 'Observacion', 'email', 'url', 'Id-comercial', 'PN', 'cliente', 'fechaintrodatos', 'fechamodifacion', 'mandato', 'choferes', 'PWD', 'Dato_ITS', 'impagado_pdteDev', 'CC', 'cteConflictivo', 'apartadoCorreos'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function gesancEmpresa()
    {
        return $this->belongsTo('App\GesancEmpresa', 'delegacion', 'Num_empresa');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function gesancChoferesclientes()
    {
        return $this->hasMany('App\GesancChoferescliente', 'CODCLI', 'CODCLI');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function gesancContratos()
    {
        return $this->hasMany('App\GesancContrato', 'cif', 'cif');
    }
}
