<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $IdContencioso
 * @property integer $IDSancion
 * @property string $ClaveContencioso
 * @property string $FechaEntrega
 * @property string $Letrado
 * @property integer $Viable
 * @property string $Recurso
 * @property string $HonorariosPagados
 * @property string $FechaModif
 * @property GesancSancione $GesancSancione
 */
class gesanc_contencioso extends Model
{

     public $table = "gesanc_contencioso";
     public $timestamps = false;
     protected $primaryKey = 'IdContencioso';
    /**
     * @var array
     */
    protected $fillable = ['IdContencioso', 'ClaveContencioso', 'IDSancion', 'FechaEntrega','Letrado','Viable','Recurso','HonorariosPagados','FechaModif'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
      public function gesancSanciones()
    {
        return $this->belongsTo('App\GesancSancione', 'IDSancion', 'IDSancion');
    }
}