<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $idcontrato
 * @property string $cif
 * @property integer $clavecontrato
 * @property string $beneficiario
 * @property boolean $contratante
 * @property string $FechaContrato
 * @property string $Id-comercial
 * @property integer $semestral
 * @property string $Tipo
 * @property string $Pago
 * @property boolean $Cobertura
 * @property integer $Reduccion
 * @property string $Notas
 * @property integer $ANULADO
 * @property string $fechaAnulacion
 * @property string $Matricula
 * @property string $R1
 * @property string $R2
 * @property string $R3
 * @property string $Alta
 * @property string $Baja
 * @property string $Vencimiento
 * @property integer $contratado
 * @property integer $CESCE
 * @property string $fechaintrodatos
 * @property string $fechamodifacion
 * @property float $precio
 * @property GesancCliente $gesancCliente
 * @property GesancSancione[] $gesancSanciones
 */
class gesanc_contratos extends Model
{

    public $table = "gesanc_contratos";
    public $timestamps = false;
    protected $primaryKey = 'IDCONTRATO';

    /**
     * @var array
     */
    protected $fillable = ['cif', 'clavecontrato', 'beneficiario', 'contratante', 'FechaContrato', 'Id-comercial', 'semestral', 'Tipo', 'Pago', 'Cobertura', 'Reduccion', 'Notas', 'ANULADO', 'fechaAnulacion', 'Matricula', 'R1', 'R2', 'R3', 'Alta', 'Baja', 'Vencimiento', 'contratado', 'CESCE', 'fechaintrodatos', 'fechamodifacion', 'precio'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function gesancCliente()
    {
        return $this->belongsTo('App\GesancCliente', 'cif', 'cif');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function gesancSanciones()
    {
        return $this->hasMany('App\GesancSancione', 'IDCONTRATO', 'idcontrato');
    }
}
