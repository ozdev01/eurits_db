<?php

namespace App;
/**NOTA IMPORTANTE:

Para generar el modelo, hay que usar la estructura de creación de modelos completa, es decir, incluyendo el nombre de la tabla como se ve en el siguiente ejemplo:

php artisan krlove:generate:model gesanc_delegacion --table-name=gesanc_delegacion

*/
use Illuminate\Database\Eloquent\Model;

/**
 * @property boolean $Num-empresa
 * @property string $Delegacion
 * @property string $Direccion
 * @property string $CP
 * @property string $Poblacion
 * @property string $Telefono
 * @property string $Fax
 * @property string $Movil
 * @property string $email
 * @property GesancEmpresa $gesancEmpresa
 */
class gesanc_delegacion extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'gesanc_delegacion';

    /**
     * @var array
     */
    protected $fillable = ['Num-empresa', 'Direccion', 'CP', 'Poblacion', 'Telefono', 'Fax', 'Movil', 'email'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function gesancEmpresa()
    {
        return $this->belongsTo('App\GesancEmpresa', 'Num-empresa', 'Num_empresa');
    }
}
