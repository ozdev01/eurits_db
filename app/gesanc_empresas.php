<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $Empresa
 * @property boolean $Num_empresa
 * @property string $Siglas
 * @property string $CIF
 * @property string $Delegado
 * @property string $Dirección
 * @property string $CP
 * @property string $Población
 * @property string $Telefono
 * @property string $Fax
 * @property string $Movil
 * @property string $email
 * @property string $RutaClientes
 * @property string $RutaContratos
 * @property GesancCliente[] $gesancClientes
 * @property GesancDelegacion[] $gesancDelegacions
 */
class gesanc_empresas extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['Empresa', 'Siglas', 'CIF', 'Delegado', 'Dirección', 'CP', 'Población', 'Telefono', 'Fax', 'Movil', 'email', 'RutaClientes', 'RutaContratos'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function gesancClientes()
    {
        return $this->hasMany('App\GesancCliente', 'delegacion', 'Num_empresa');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function gesancDelegacions()
    {
        return $this->hasMany('App\GesancDelegacion', 'Num-empresa', 'Num_empresa');
    }
}
