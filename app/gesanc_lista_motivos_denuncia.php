<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $IdListaMotivos
 * @property integer $IDSancion
 * @property integer $Idmotivo
 * @property string $FechaModif
 * @property GesancSancione $GesancSancione
 */
class gesanc_lista_motivos_denuncia extends Model
{

     public $table = "gesanc_lista_motivos_denuncia";
    /**
     * @var array
     */
    protected $fillable = ['IdListaMotivos', 'IDSancion', 'Idmotivo', 'FechaModif'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
      public function gesancSanciones()
    {
        return $this->belongsTo('App\GesancSancione', 'IDSancion', 'IDSancion');
    }
}