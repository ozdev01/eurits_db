<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $COD_MOTIVO
 * @property string $MOTIVO
 * @property string $PAGO
 * @property GesancListaMotivosDenuncium[] $gesancListaMotivosDenuncias
 */
class gesanc_motivos extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['MOTIVO', 'PAGO'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function gesancListaMotivosDenuncias()
    {
        return $this->hasMany('App\GesancListaMotivosDenuncium', 'Idmotivo', 'COD_MOTIVO');
    }
}
