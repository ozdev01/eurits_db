<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $ID-REDUCCION
 * @property string $ClaveReduccion
 * @property integer $idsancion
 * @property string $matricula
 * @property string $cif
 * @property integer $importe
 * @property string $fecha
 * @property string $moneda
 * @property string $usuario
 * @property string $FechaModif
 * @property GesancSancione $GesancSancione
 */
class gesanc_reduccion extends Model
{

 public $table = "gesanc_reduccion";

    /**
     * @var array
     */
   
    protected $fillable = ['ID-REDUCCION', 'ClaveReduccion', 'idsancion', 'matricula', 'cif', 'importe', 'fecha', 'IDRECURSOS', 'moneda', 'usuario', 'FechaModif', ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
      public function gesancSanciones()
    {
        return $this->belongsTo('App\GesancSancione', 'idsancion', 'IDSancion');
    }
}