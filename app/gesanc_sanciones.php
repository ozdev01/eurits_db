<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $IDSancion
 * @property integer $ClaveSancion
 * @property integer $IDCONTRATO
 * @property string $cif
 * @property string $Matricula
 * @property string $Boletin
 * @property string $expediente
 * @property string $Acuerdo
 * @property string $FechaMulta
 * @property string $HoraMulta
 * @property string $Conductor
 * @property float $Importe
 * @property string $Sancion propuesta
 * @property string $Moneda
 * @property string $Notas
 * @property string $Lugar
 * @property string $Pais
 * @property string $Autoridad
 * @property integer $Inactivo
 * @property string $Pago
 * @property integer $codigosancion
 * @property integer $RecibidoJP
 * @property integer $NCP
 * @property integer $AP
 * @property integer $marca_pdte
 * @property integer $AC
 * @property integer $CP
 * @property integer $GestionadoGURE
 * @property integer $SG
 * @property integer $IR
 * @property string $FECHALLEGADA
 * @property float $Importepagado
 * @property string $FECHACIERRE
 * @property string $CodigoFT
 * @property integer $CodigoAutoridad
 * @property string $Nota_pdte
 * @property integer $Privado
 * @property string $FechaModif
 * @property string $fechaJustificante
 * @property GesancContrato $gesancContrato
 */

class gesanc_sanciones extends Model
{
    public $timestamps = false;
    /**
     * @var array
     */
    protected $fillable = ['ClaveSancion', 'IDCONTRATO', 'cif', 'Matricula', 'Boletin', 'expediente', 'Acuerdo', 'FechaMulta', 'HoraMulta', 'Conductor', 'Importe', 'Sancion propuesta', 'Moneda', 'Notas', 'Lugar', 'Pais', 'Autoridad', 'Inactivo', 'Pago', 'codigosancion', 'RecibidoJP', 'NCP', 'AP', 'marca_pdte', 'AC', 'CP', 'GestionadoGURE', 'SG', 'IR', 'FECHALLEGADA', 'Importepagado', 'FECHACIERRE', 'CodigoFT', 'CodigoAutoridad', 'Nota_pdte', 'Privado', 'FechaModif', 'fechaJustificante'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function gesancContrato()
    {
        return $this->belongsTo('App\GesancContrato', 'IDCONTRATO', 'idcontrato');
    }
}
