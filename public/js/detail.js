$(document).on('focusin','.datepicker',function(){
         $(this).datepicker({
        format: "dd/mm/yyyy",
        dateFormat: 'yy-mm-dd',
        language: "es",
        autoclose: true
    });

         $(this).selectpicker("data-live-search","true");

    });


  $(document).on('click','.btn-danger',function(){

    $ID = $(this).closest("tr").find('.ID').text();

    $IDSancion = $("#IDSancion").val();

bootbox.confirm({
     title: "Eliminar Registro",
    message: "<i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> ¿Está seguro que desea eliminar el registro?</strong> Tenga en cuenta que el proceso es irreversible.",
    buttons: {
        cancel: {
            label: '<i class="fa fa-times"></i> Cancelar'
        },
        confirm: {
            label: '<i class="fa fa-check"></i> Aceptar'
        }
    },
    callback: function (result) {
            if(result == true){
            if($("#tabla_tipo").val() == 'motivo'){
                $tabla = 'gesanc_lista_motivos_denuncia';
                $field = "IdListaMotivos";

        setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('sanciones/borrar')}}',
                data : {'ID':$ID,'tabla': $tabla,'field':$field},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $("#success").append(data);
                    $("#success").show();
                    //bootbox.alert(data);
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);



        setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('sanciones/motivos')}}',
                data : {'IDSancion':$IDSancion},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('#tabla_contenido').html(data);
                    $('.selectpicker').selectpicker('refresh');
                    $('.selectpicker').selectpicker('render');
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);


                 
            }

            if($("#tabla_tipo").val() == 'reducciones'){
                $tabla = 'gesanc_reduccion';
                $field= '`ID-REDUCCION`';

                setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('sanciones/borrar')}}',
                data : {'ID':$ID,'tabla': $tabla,'field':$field},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $("#success").append(data);
                    $("#success").show();
                    //bootbox.alert(data);
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);



        setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('sanciones/reducciones')}}',
                data : {'IDSancion':$IDSancion},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('#tabla_contenido').html(data);
                    $('.selectpicker').selectpicker('refresh');
                    $('.selectpicker').selectpicker('render');
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);
            }

             if($("#tabla_tipo").val() == 'tramites'){
                $tabla = 'gesanc_recursos';
                $field = "id_recurso";

                alert($tabla);
                alert($field);

                setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('sanciones/borrar')}}',
                data : {'ID':$ID,'tabla': $tabla,'field':$field},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $("#success").append(data);
                    $("#success").show();
                    //bootbox.alert(data);
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);



        setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('sanciones/tramites')}}',
                data : {'IDSancion':$IDSancion},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('#tabla_contenido').html(data);
                    $('.selectpicker').selectpicker('refresh');
                    $('.selectpicker').selectpicker('render');
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);
               
            }

    
    }
    }
});

});





    $("#volver").click(function(){
        window.history.go(-1); return false;
    });

  $(document).ready(function() {

  $ID = $("#IDSancion").val();
  setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('sanciones/motivos')}}',
                data : {'IDSancion':$ID},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('#tabla_contenido').html(data);
                    $('.selectpicker').selectpicker('refresh');
                    $('.selectpicker').selectpicker('render');
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

            

});

    $('#reducciones').click(function(){
        $("#success").hide();
        $("#error").hide();
        //alert("reducciones");
        $ID = $("#IDSancion").val();
        setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('sanciones/reducciones')}}',
                data : {'IDSancion':$ID},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('#tabla_contenido').html(data);
                    $('.selectpicker').selectpicker('refresh');
                    $('.selectpicker').selectpicker('render');
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

    });

     $('#motivos').click(function(){
        $("#success").hide();
        $("#error").hide();
        //alert("reducciones");
        $ID = $("#IDSancion").val();
        setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('sanciones/motivos')}}',
                data : {'IDSancion':$ID},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('#tabla_contenido').html(data);
                    $('.selectpicker').selectpicker('refresh');
                    $('.selectpicker').selectpicker('render');
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

    });

     $('#contencioso').click(function(){
         $("#success").hide();
        $("#error").hide();
        //alert("reducciones");
        $ID = $("#IDSancion").val();
        setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('sanciones/contencioso')}}',
                data : {'IDSancion':$ID},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('#tabla_contenido').html(data);
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

    });

     $('#tramites').click(function(){
         $("#success").hide();
        $("#error").hide();
        //alert("reducciones");
        $ID = $("#IDSancion").val();
        setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('sanciones/tramites')}}',
                data : {'IDSancion':$ID},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('#tabla_contenido').html(data);
                    $('.selectpicker').selectpicker('refresh');
                    $('.selectpicker').selectpicker('render');
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

    });

     $('#pendiente').click(function(){
         $("#success").hide();
        $("#error").hide();
        //alert("reducciones");
        $ID = $("#IDSancion").val();
        setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('sanciones/pendiente')}}',
                data : {'IDSancion':$ID},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('#tabla_contenido').html(data);
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

    });

      

$('.detalles').on('click',function(){
    $('.detalles.active').removeClass('active');
    $(this).addClass('active');
}); 


$(document).on('click', '#add_motivo' ,function(){

$action = 1;
$motivo = $("#id_motivo option:selected").text();
$ID = $("#IDSancion").val();

if($motivo == "buscar..." || $motivo == ""){
    $("#error").empty();
    $("#error").append("<i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Se han encontrado errores.</strong> El campo del motivo no puede estar vacío.");
    $("#error").show();
}else{
    $("#error").hide();
    $("#success").hide();
    $("#success").empty();

     setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('sanciones/insertar')}}',
                data : {'action':$action,'IDSancion':$ID,'motivo':$motivo},
                success : function(data){
                    console.log(JSON.stringify(data));
                    $("#success").append(data);
                    $("#success").show();
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

     $("#motivos")[0].click();
}
});

$(document).on('click', '#add_tramite' ,function(){
$("#error").hide();
$("#error").empty();
$action = 2;
$tramite = $("#id_tramite option:selected").text();
$fecha = $("#fecha_tramite").val();
$fecha_tramite= $fecha.split("/").reverse().join("-");
$ID = $("#IDSancion").val();

if($tramite == "buscar..." || $tramite == "" || $fecha_tramite == ""){

    if($tramite == "buscar..." || $tramite == ""){
         $("#error").append("<i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Se han encontrado errores.</strong> El campo de trámite no puede estar vacío.<br>");
    }

    if($fecha_tramite == ""){
         $("#error").append("<i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Se han encontrado errores.</strong> El campo de fecha no puede estar vacío.<br>");
    }
    $("#error").show();
}else{
    $("#error").hide();
    $("#success").hide();
    $("#success").empty();

     setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('sanciones/insertar')}}',
                data : {'action':$action,'IDSancion':$ID,'tramite':$tramite,'fecha_tramite': $fecha_tramite},
                success : function(data){
                    console.log(JSON.stringify(data));
                    $("#success").append(data);
                    $("#success").show();
                    //bootbox.alert(data);
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

       setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('sanciones/tramites')}}',
                data : {'IDSancion':$ID},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('#tabla_contenido').html(data);
                    $('.selectpicker').selectpicker('refresh');
                    $('.selectpicker').selectpicker('render');
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);
}
});

$(document).on('click', '#add_reduccion' ,function(){
$("#error").hide();
$("#error").empty();
$action = 3;
$moneda = $("#id_moneda option:selected").text();
$fecha = $("#fecha_reduccion").val();
$fecha_reduccion= $fecha.split("/").reverse().join("-");
$importe = $("#importe").val();

$ID = $("#IDSancion").val();

if($moneda == "buscar..." || $moneda == "" || $fecha_reduccion == "" || $importe == ""){

    if($moneda == "buscar..." || $moneda == ""){
         $("#error").append("<i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Se han encontrado errores.</strong> El campo de la divisa no puede estar vacío.<br>");
    }

    if($fecha_reduccion == ""){
         $("#error").append("<i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Se han encontrado errores.</strong> El campo de fecha no puede estar vacío.<br>");
    }

    if($importe == ""){
         $("#error").append("<i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Se han encontrado errores.</strong> El campo de importe no puede estar vacío.<br>");
    }

    $("#error").show();
}else{
    $("#error").hide();
    $("#success").hide();
    $("#success").empty();

     setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('sanciones/insertar')}}',
                data : {'action':$action,'IDSancion':$ID,'importe':$importe,'fecha_reduccion': $fecha_reduccion, 'moneda': $moneda},
                success : function(data){
                    console.log(JSON.stringify(data));
                    $("#success").append(data);
                    $("#success").show();
                    //bootbox.alert(data);
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

       setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('sanciones/reducciones')}}',
                data : {'IDSancion':$ID},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('#tabla_contenido').html(data);
                    $('.selectpicker').selectpicker('refresh');
                    $('.selectpicker').selectpicker('render');
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);
}
});

$(document).on('click', '#save_contencioso' ,function(){
$("#error").hide();
$("#error").empty();
$action = 4;
$letrado = $("#Letrado").val();
$recurso = 0
$honorarios = 0;
$Viable = 0;
$fecha = $("#fechaEntrega").val();
$fechaEntrega= $fecha.split("/").reverse().join("-");
$importe = $("#importe").val();
$ID = $("#IDSancion").val();


if($('#honorarios').prop("checked") == true){
    $honorarios = 1;
}

if($('#Viable').prop("checked") == true){
    $Viable = 1;
}

if($('#Recurso').val()!= ""){
    $recurso = $('#Recurso').val();
}



if($letrado == "" || $fechaEntrega == ""){

    if($letrado == ""){
         $("#error").append("<i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Se han encontrado errores.</strong> El campo de letrado no puede estar vacío.<br>");
    }

    if($fechaEntrega == ""){
         $("#error").append("<i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Se han encontrado errores.</strong> El campo de fecha no puede estar vacío.<br>");
    }

    $("#error").show();
}else{
    $("#error").hide();
    $("#success").hide();
    $("#success").empty();

     setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('sanciones/insertar')}}',
                data : {'action':$action,'IDSancion':$ID,'letrado':$letrado,'recurso': $recurso, 'fechaEntrega': $fechaEntrega, 'honorarios': $honorarios, 'viable': $Viable},
                success : function(data){
                    console.log(JSON.stringify(data));
                    $("#success").append(data);
                    $("#success").show();
                    //bootbox.alert(data);
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

       setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('sanciones/contencioso')}}',
                data : {'IDSancion':$ID},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('#tabla_contenido').html(data);
                    $('.selectpicker').selectpicker('refresh');
                    $('.selectpicker').selectpicker('render');
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);
}
});

$(document).on('click', '#save_pendiente' ,function(){
$("#error").hide();
$("#error").empty();
$action = 5;
$Notaspendiente = $("#Notaspendiente").val();
$marca_pdte = 0
$ID = $("#IDSancion").val();



$marca_pdte = $("input[name=Estado]:checked").val();

$("#success").hide();
$("#success").empty();

     setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('sanciones/insertar')}}',
                data : {'action':$action,'IDSancion':$ID,'Notaspendiente': $Notaspendiente, 'marca_pdte': $marca_pdte},
                success : function(data){
                    console.log(JSON.stringify(data));
                    $("#success").append(data);
                    $("#success").show();
                    //bootbox.alert(data);
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

       setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('sanciones/pendiente')}}',
                data : {'IDSancion':$ID},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('#tabla_contenido').html(data);
                    $('.selectpicker').selectpicker('refresh');
                    $('.selectpicker').selectpicker('render');
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

});