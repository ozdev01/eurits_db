 $(document).on('focusin','.datepicker',function(){
         $(this).datepicker({
        format: "dd/mm/yyyy",
        dateFormat: 'yy-mm-dd',
        language: "es",
        autoclose: true
    });

         $(this).selectpicker("data-live-search","true");

    });


    $("#volver").click(function(){
        window.history.go(-1); return false;
    });

  $(document).ready(function() {


  $ID = $("#IDSancion").val();
  setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('sanciones/motivos')}}',
                data : {'IDSancion':$ID},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('#tabla_contenido').html(data);
                    $('.selectpicker').selectpicker('refresh');
                    $('.selectpicker').selectpicker('render');
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

            

});

    $('#reducciones').click(function(){
        $("#success").hide();
        $("#error").hide();
        //alert("reducciones");
        $ID = $("#IDSancion").val();
        setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('sanciones/reducciones')}}',
                data : {'IDSancion':$ID},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('#tabla_contenido').html(data);
                    $('.selectpicker').selectpicker('refresh');
                    $('.selectpicker').selectpicker('render');
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

    });

     $('#motivos').click(function(){
        $("#success").hide();
        $("#error").hide();
        //alert("reducciones");
        $ID = $("#IDSancion").val();
        setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('sanciones/motivos')}}',
                data : {'IDSancion':$ID},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('#tabla_contenido').html(data);
                    $('.selectpicker').selectpicker('refresh');
                    $('.selectpicker').selectpicker('render');
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

    });

     $('#contencioso').click(function(){
         $("#success").hide();
        $("#error").hide();
        //alert("reducciones");
        $ID = $("#IDSancion").val();
        setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('sanciones/contencioso')}}',
                data : {'IDSancion':$ID},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('#tabla_contenido').html(data);
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

    });

     $('#tramites').click(function(){
         $("#success").hide();
        $("#error").hide();
        //alert("reducciones");
        $ID = $("#IDSancion").val();
        setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('sanciones/tramites')}}',
                data : {'IDSancion':$ID},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('#tabla_contenido').html(data);
                    $('.selectpicker').selectpicker('refresh');
                    $('.selectpicker').selectpicker('render');
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

    });

     $('#pendiente').click(function(){
         $("#success").hide();
        $("#error").hide();
        //alert("reducciones");
        $ID = $("#IDSancion").val();
        setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('sanciones/pendiente')}}',
                data : {'IDSancion':$ID},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('#tabla_contenido').html(data);
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

    });

      

$('.detalles').on('click',function(){
    $('.detalles.active').removeClass('active');
    $(this).addClass('active');
}); 


$(document).on('click', '#add_motivo' ,function(){

$action = 1;
$motivo = $("#id_motivo option:selected").text();
$ID = $("#IDSancion").val();

if($motivo == "buscar..." || $motivo == ""){
    $("#error").empty();
    $("#error").append("<i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Se han encontrado errores.</strong> El campo del motivo no puede estar vacío.");
    $("#error").show();
}else{
    $("#error").hide();
    $("#success").hide();
    $("#success").empty();

     setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('sanciones/insertar')}}',
                data : {'action':$action,'IDSancion':$ID,'motivo':$motivo},
                success : function(data){
                    console.log(JSON.stringify(data));
                    $("#success").append(data);
                    $("#success").show();
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

     $("#motivos")[0].click();
}
});