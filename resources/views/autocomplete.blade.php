@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xl-12 col-md-offset-0">
			<form>
				<input type="text" name="text" id="q">
			</form>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		$("#q").autocomplete({
			source:"search/autocomplete",
			minLength: 3,
			select: function(event, ui){
				$('#q').val(ui.item.value);
			}
		});
		$('#q').data("ui-autocomplete")._renderItem = function(ul, item){
			var $li = $("<li style='width:800px;margin-left:10px;margin-bottom:5px'>");			
		}
		$li.attr('data-value', item.value);
		$li.append("");
		$li.append("**"+item.value);
		return $li.appendTo(ul);
	});

</script>
@endsection