@extends('layouts.app')
@section('content')

   
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="row"> 
                    <div class="panel-heading" id="sanciones-header">                    
                        <div class="col-md-12">
                            <h3 class="module-title">Anexo @foreach ($anexos as $dato){!! $dato->IdAnexo !!} @endforeach</h3>
                        </div>
                        <div class="col-md-12">
                            <div>
                                <ul class="nav navbar-nav">
                                    <li><a href="#" class="bt-edit"><i class="fa fa-save"> </i> Guardar</a></li>
                                    <li><a href="#" class="bt-export"><i class="fa fa-print"> </i> Imprimir</a></li>
                                    <li><a href="#" id="eliminar"><i class="fa fa-trash"> </i> Eliminar</a></li>
                                    <li><a href="#" id="volver"><i class="fa fa-arrow-left"> </i> Volver</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="panel-body" style="border-top:2px solid #335599;">                        
                    <form class="inline-form" id="form_anexo">
                        <div class="form-group col-md-4">
                            <label for="Beneficiario">Beneficiario</label>
                            @foreach ($anexos as $dato)
                            <input type="text" class="form-control" name="Beneficiario" value="{!! $dato->Beneficiario !!}">
                            <input type="hidden" class="form-control" name="id" id="id" value="{!! $dato->IdAnexo !!}">
                            @endforeach                    
                        </div>
                        <div class="form-group col-md-2">
                            <label for="codigo">Cobertura</label>
                            @foreach ($anexos as $dato)
                            <input type="text" class="form-control" name="Cobertura" value="{!! $dato->Cobertura !!}">
                            @endforeach  
                        </div>
                        <div class="form-group col-md-4">
                            <label for="empresa">Matricula</label>
                            @foreach ($anexos as $dato)
                            <input type="text" id="empresa" class="form-control" name="Matricula" value="{!! $dato->Matricula !!}">
                            @endforeach                                 
                        </div>
                        <div class="form-group col-md-2">
                            <label for="cif">Sin EC</label>
                            @foreach ($anexos as $dato)
                            @if($dato->cheques > 0)
                     	<input type="checkbox" name="EC" checked>
                       @else
                       <input type="checkbox" name="EC">
                       @endif
                            @endforeach                       
                        </div>
                     
                        <div class="form-group col-md-3">
                            <label for="matricula">Fecha Contrato</label>
                            
                            <input type="text" class="form-control datepicker" name="FechaContrato" value="{!! date('d/m/Y', strtotime($fecha_contrato)) !!}">
                                                    
                        </div>
                        <div class="form-group col-md-3">
                            <label for="matricula">Alta</label>
                             @foreach ($anexos as $dato)
                            <input type="text" class="form-control datepicker" name="Alta" value="{!! date('d/m/Y', strtotime($dato->Alta)) !!}">
                            @endforeach                            
                        </div>
                         <div class="form-group col-md-3">
                            <label for="matricula">Baja</label>
                             @foreach ($anexos as $dato)
                            <input type="text" class="form-control datepicker" name="Baja" value="{!! date('d/m/Y', strtotime($dato->Baja)) !!}">
                            @endforeach                            
                        </div>  
                       <div class="form-group col-md-3">
                            <label for="matricula">Sustituye A</label>
                             @foreach ($anexos as $dato)
                            <input type="text" class="form-control" name="Sustituye" value="{!! $dato->SUSTITUYE!!}">
                            @endforeach                            
                        </div>
                           <div class="form-group col-md-3">
                            <label for="matricula">Inter</label>
                          
                            <input type="text" class="form-control" name="Inter" value="">
                                                 
                        </div>
                           <div class="form-group col-md-6">
                            <label for="matricula">Motivo Baja</label>
                             <select class="selectpicker" data-live-search="true" title="Buscar..." id="MotivoBaja" name="MotivoBaja">
                            @foreach ($motivo_baja as $dato)
                             <option selected>{{$dato->motivobaja}}</option>
                            @endforeach
                              
                            </select>                       
                        </div>            
                    </form>
                    </div>

                </div>
            </div>
</div>

<script type="text/javascript">

$("#volver").click(function(){
        window.history.go(-1); return true;
    });


 $(document).on('focusin','.datepicker',function(){
         $(this).datepicker({
        format: "dd/mm/yyyy",
        dateFormat: 'yy-mm-dd',
        language: "es",
        autoclose: true
    });

         $(this).selectpicker("data-live-search","true");

    });

 $(".bt-edit").click(function(){

    
    $ID = $("#id").val();

    $form = $("#form_anexo").serialize();
  
     setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '/contratos/its/anexos/'+$ID+'/guardar',
                data : {'datos' : $("#form_anexo").serialize()},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    bootbox.alert({
                            message: data,
                            callback: function () {
                                location.reload();
                            }
                        });
                    $("#loading").hide();

                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

});



</script>

@endsection