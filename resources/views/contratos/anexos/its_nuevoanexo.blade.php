@extends('layouts.app')
@section('content')

   
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="row"> 
                    <div class="panel-heading" id="sanciones-header">                    
                        <div class="col-md-12">
                            <h3 class="module-title">Añadir nuevo anexo</h3>
                        </div>
                        <div class="col-md-12">
                            <div>
                                <ul class="nav navbar-nav">
                                    <li><a href="#" class="bt-edit"><i class="fa fa-save"> </i> Guardar</a></li>
                                    <li><a href="#" id="volver"><i class="fa fa-arrow-left"> </i> Volver</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="panel-body" style="border-top:2px solid #335599;">                        
                    <form class="inline-form" id="form_sancion">
                        <div class="form-group col-md-4">
                            <label for="Beneficiario">Beneficiario</label>
                          
                            <input type="text" class="form-control" name="empresa" id="empresa" value="{{$contacto->Empresa}}" readonly>
                            <input type="hidden" class="form-control" name="beneficiario" id="beneficiario" value="{{!! $contacto->CIF !!}}">
                            <input type="hidden" class="form-control" name="idContrato" id="idContrato" value="{{$IDContrato}}">
                                           
                        </div>
                        <div class="form-group col-md-2">
                            <label for="codigo">Cobertura</label>
                            <select class="selectpicker" data-live-search="true" title="Buscar..." id="cobertura" name="cobertura">
                                @foreach ($coberturas as $cobertura)
                                <option value="{{$cobertura->COBERTURA}}">{{ $cobertura->Descripcion}}</option>
                                @endforeach
                            </select> 
                         
                        </div>
                        <div class="form-group col-md-4">
                            <label for="empresa">Matricula</label>
                        
                            <input type="text" class="form-control" name="matricula" id="matricula" value="">
                                                 
                        </div>
                        <div class="form-group col-md-2">
                            <label for="cif">Sin EC</label>
                       <input type="checkbox" name="EC" id="EC">                         
                        </div>
                     
                        <div class="form-group col-md-3">
                            <label for="matricula">Fecha Contrato</label>
                            <input type="text" class="form-control datepicker" name="fecha_contrato" id="fecha_contrato" value="">                       
                        </div>
                        <div class="form-group col-md-3">
                            <label for="matricula">Alta</label>
                           
                            <input type="text" class="form-control datepicker" name="alta" id="alta" value="">
                                               
                        </div>
                                 
                    </form>
                    </div>

                </div>
            </div>
</div>

<script type="text/javascript">

$("#volver").click(function(){
        window.history.go(-1); return true;
    });


 $(document).on('focusin','.datepicker',function(){
         $(this).datepicker({
        format: "dd/mm/yyyy",
        dateFormat: 'yy-mm-dd',
        language: "es",
        autoclose: true
    });

         $(this).selectpicker("data-live-search","true");

    });

 $(".bt-edit").click(function(){

$beneficiario = $("#beneficiario").val();
$cobertura = $("#cobertura option:selected").val();
$matricula = $("#matricula").val();
$EC = 0;

$idContrato = $("#idContrato").val();

if( $("#EC").prop("checked")){
    $EC = 1;
}

$fecha_contrato = $("#fecha_contrato").val();
$fechaContrato= $fecha_contrato.split("/").reverse().join("-");

$fecha_alta = $("#alta").val();
$alta= $fecha_alta.split("/").reverse().join("-");

 setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/insertaranexo')}}',
                data : {'IdContrato':$idContrato,'beneficiario':$beneficiario,'cobertura': $cobertura, 'matricula': $matricula,'EC': $EC,'fechaContrato': $fechaContrato,'alta': $alta},
                success : function(data){
                    console.log(JSON.stringify(data));
                    bootbox.alert({
                        message: "El anexo se ha añadido correctamente",
                        callback: function () {
                            window.location = "/contratos/its/"+data;
                        }
                    })
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);


 });


</script>

@endsection