@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xl-12 col-md-offset-0">
            <div class="panel panel-default">
                <div class="row"> 
                    <div class="panel-heading" id="sanciones-header">                    
                        <div class="col-md-8">
                            <h3 class="module-title">Cheques ITS</h3>
                        </div>
                        <div class="col-md-4">
                            <div>
                                <ul class="nav navbar-nav">                            
                                    <li><a href="#" id="volver"><i class="fa fa-arrow-left"> </i> Volver</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="panel-body" style="border-top:2px solid #335599;">                        
                    <form class="inline-form" id="form_sancion">
                        <div class="form-group col-md-4">
                            <label for="matricula">Buscar por Cliente</label>
                             <select class="selectpicker" data-live-search="true" title="Buscar..." id="cliente" name="clientes">
                                 <option selected></option>
                                    @foreach ($clientes as $cliente)
                                    <option value="{{$cliente->CIF}}">{{ $cliente->Empresa}}</option>
                                    @endforeach
                            </select>                            
                            </div>                               
                        </div>  
                    </form>
                       <div class="table-responsive" id="tabla_contenido" style="border-top: none;">
                              <table class="table">
                        <thead class="table-header">
                            <th>Enviado a</th>                        
                            <th>Banco emisor</th>
                            <th>Fecha abono</th>
                            <th>Num Abono</th>
                            <th></th>
                        </thead>
                        <tbody id="myTable">
                        @foreach ($cheques as $cheque)
                        <tr>
                            <td>{{ $cheque->enviado }} </td>
                            <td>{{ $cheque->banco }} </td>
                            @if($cheque->FechaAbono!=null)
                            <td>{{ date('d - m - Y', strtotime($cheque->FechaAbono))}}</td> 
                            @else
                            <td></td>
                            @endif                           
                            <td>{{ $cheque->NumAbono }} </td>
                            <td><a class="btn btn-sm btn-success" href={!! url('/contratos/its/cheques/'.$cheque->IdCheque) !!}#"><i class="fa fa-edit"></i> </a></td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                    </div>                        
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $("#volver").click(function(){
        window.history.go(-1); return false;
    });
     $(document).on('focusin','.datepicker',function(){
         $(this).datepicker({
        format: "dd/mm/yyyy",
        dateFormat: 'yy-mm-dd',
        language: "es",
        autoclose: true
    });

         $(this).selectpicker("data-live-search","true");

    });

     $( ".selectpicker" ).change(function(){

    $cif = $("#cliente").find("option:selected").val();
    setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/its/cheques/buscar')}}',
                data : {'cif': $cif},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('#myTable').html(data);
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

});
</script>
@endsection