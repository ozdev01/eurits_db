@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xl-12 col-md-offset-0">
            <div class="panel panel-default">
                <div class="row"> 
                    <div class="panel-heading" id="sanciones-header">                    
                        <div class="col-md-8">

                            <h3 class="module-title">{{$cheque->IdCheque}}</h3>

                        </div>
                        <div class="col-md-4">
                            <div>
                                <ul class="nav navbar-nav">
                                    <li><a href="#" id="guardar"><i class="fa fa-save"> </i> Guardar</a></li>                                 

                                    <li><a href="#" id="volver"><i class="fa fa-arrow-left"> </i> Volver</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="panel-body" style="border-top:2px solid #335599;">                        
                    <form class="inline-form" id="form_sancion">
                        <div class="form-group col-md-3">
                            <label for="empresa">Cabecera</label>
                            <select class="selectpicker" data-live-search="true" title="Buscar..." id="Cabecera" name="Cabecera">
                            @foreach ($matriculas as $matricula)
                            @if($matricula->Matricula == $cheque->Cabecera)
                               <option value="{{$matricula->Matricula}}" selected>{{$matricula->Matricula}}</option>
                               @else
                            <option value="{{$matricula->Matricula}}">{{$matricula->Matricula}}</option>
                            @endif
                             @endforeach
                            </select>                 
                        </div>
                       <div class="form-group col-md-3">
                            <label for="matricula">EC</label>
                           <input type="text" class="form-control" name="EC" value="{{$cheque->EC}}">  
                        </div>
                        <div class="form-group col-md-3">
                            <label for="matricula">CIF</label>
                            <select class="selectpicker" data-live-search="true" title="Buscar..." id="CIF" name="CIF">
                            @foreach ($contactos as $contacto)
                            @if($contacto->CIF == $cheque->Cabecera)
                               <option value="{{$matricula->Matricula}}" selected>{{$matricula->Matricula}}</option>
                               @else
                            <option value="{{$matricula->Matricula}}">{{$matricula->Matricula}}</option>
                            @endif
                             @endforeach
                            </select>                     
                        </div>
                          <div class="form-group col-md-3">
                            <label for="matricula">ESTADO</label>
                            <select class="selectpicker" data-live-search="true" title="Buscar..." id="estado" name="estado">
                            @foreach ($estados_cheque as $estado)
                            @if($estado->Estado == $cheque->estado)
                               <option value="{{$estado->Estado}}" selected>{{$estado->Concepto}}</option>
                               @else
                            <option value="{{$estado->Estado}}">{{$estado->Concepto}}</option>
                            @endif
                             @endforeach
                            </select>                     
                        </div>
                         <div class="form-group col-md-3">
                            <label for="matricula">Banco</label>
                            <input type="text" class="form-control" name="Banco" value="{{$cheque->banco}}">     
                              <input type="hidden" class="form-control" name="IdCheque" value="{{$cheque->IdCheque}}">                        
                        </div>
                      <div class="form-group col-md-2">
                            <label for="matricula">Enviado el</label>
                            @if($cheque->enviado != null)
                           <input type="text" class="form-control datepicker" name="Enviado" value="{{date('d/m/Y', strtotime($cheque->enviado))}}">
                           @else
                            <input type="text" class="form-control datepicker" name="Enviado" value="">
                            @endif  
                        </div>
                          <div class="form-group col-md-2">
                            <label for="matricula">Enviado a</label>
                           <input type="text" class="form-control" name="enviadoa" value="{{$cheque->enviadoa}}">   
                        </div>       
                    </form>
                    </div>                       
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

$("#volver").click(function(){
        window.history.go(-1); return true;
    });



     $(document).on('focusin','.datepicker',function(){
         $(this).datepicker({
        format: "dd/mm/yyyy",
        dateFormat: 'yy-mm-dd',
        language: "es",
        autoclose: true
    });

         $(this).selectpicker("data-live-search","true");

    });

         $("#volver").click(function(){

        window.history.go(-1); return false;
    });


 $("#guardar").click(function(){

     setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '/contratos/its/cheques/guardar',
                data : {'datos' : $("#form_sancion").serialize()},            
                success : function(data){
                     if(data.includes("SQLSTATE")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                location.reload();
                            }
                        });
                    }else{
                         bootbox.alert({
                            message: data,
                            callback: function () {
                                 location.reload();
                            }
                        });
                    }
                   
                    $("#loading").hide();

                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);
});

 
</script>
@endsection