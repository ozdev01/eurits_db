@extends('layouts.app')
@section('content')

<div class="container">
    <div class="row">
        <div class="col-sm-2">
         
            <h4>Búsqueda</h4>           
            <!--<label for="search" id="loading" style="display:none;">
                <i class="fa fa-refresh fa-spin fa-1x fa-fw"></i>
            </label>-->
            <input type="text" class="form-control" id="search" name="search" placeholder="Buscar...">
            </div>
        <div class="col-sm-10">
            <div class="panel panel-default">
                <div class="row"> 
                    <div class="panel-heading" id="sanciones-header">                    
                        <div class="col-md-2" style="width: 100%">
                            <h3 class="module-title">Contactos ITS</h3>
                        </div>
                        <div class="col-md-10">
                            <div>
                                <ul class="list-inline">
                                    <li><a href="{{url('contratos/its/contactos/nuevo')}}" id=""><i class="fa fa-plus"> </i> Añadir</a></li>
                                    <li><a href="#" id="excel"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Excel</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table-responsive"">
                    <table class="table">
                        <thead class="table-header">
                            <th>Empresa</th>                        
                            <th>CIF</th>
                            <th>Nombre</th>
                            <th>Apellidos</th>
                            <th>Pais</th>
                            <th>Población</th>
                            <th></th>
                        </thead>
                        <tbody id="myTable">
                        @foreach ($contactos as $contacto)
                        <tr>
                            <td>{{ $contacto->Empresa }} </td>
                            <td>{{ $contacto->CIF }} </td>
                            <td>{{ $contacto->Nombre }} </td>                            
                            <td>{{ $contacto->Apellidos }} </td>
                            <td>{{ $contacto->Pais }} </td>
                            <td>{{ $contacto->Poblacion }}</td>
                            <td><a class="btn btn-sm btn-success" href={!! url('contratos/its/contactos/'.$contacto->CIF) !!}#"><i class="fa fa-edit"></i></a> 
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    
                </div>

                 <div class="col-md-12 text-center">
      <ul class="pagination pagination-lg pager" id="myPager"></ul>
      </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

 $('.datepicker').datepicker({
        format: "dd/mm/yyyy",
        dateFormat: 'yy-mm-dd',
        language: "es",
        autoclose: true
    });
    
    $('#search').on('keyup',function(){
        $value=$(this).val();
        //alert($value);
        $("#loading").show();
        setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/its/contratos/search_its')}}',
                data : {'search':$value},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('tbody').html(data);
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);
});

$("#contactos").click(function(){
$container = $('#collapseempresa').clone();
$container.find('select').attr('id', 'selectempresa');

 setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/contactos')}}',            
                success : function(data){
                    $('.selectpicker').selectpicker('refresh');
                    $('.selectpicker').selectpicker('render');
                    $('.table-responsive').html(data);

                    // do something in the background
                    $dialog.modal('hide');
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);


});

$("#excel").click(function(){
$value=$(this).val();

 $dialog = bootbox.dialog({
                        message: '<p class="text-center">Generando su Excel, espere por favor...</p>',
                        closeButton: false
                    });

    setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/its/contactos/exportar_excel_its')}}',
                data : {'search':$value},            
                success : function(data){
                    window.location = this.url;
                    $dialog.modal('hide');
                },
                error : function(data){
                    //console.log(JSON.stringify(data));
                    bootbox.alert("Ha ocurrido un error generando su Excel.");
                    $dialog.modal('hide');
                }
            });
        }, 500);

});
</script>
@endsection