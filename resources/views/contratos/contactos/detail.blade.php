@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xl-12 col-md-offset-0">
            <div class="panel panel-default">
                <div class="row"> 
                    <div class="panel-heading" id="sanciones-header">                    
                        <div class="col-md-8">
                         @foreach ($contactos as $contacto)
                            <h3 class="module-title">{{$contacto->Empresa}}</h3>
                              @endforeach
                              @foreach ($contactos as $contacto)
                         @if($contacto->autorizar == 1)
                          - Autorizar 
                           @endif
                             @if($contacto->devolucion == 1)
                          - Devolución 
                           @endif
                             @if($contacto->impago_multas == 1)
                          - Impago Multas 
                           @endif
                           @if($contacto->cteConflictivo == 1)
                          - Conflictivo
                           @endif
                        @endforeach 
                        </div>
                        <div class="col-md-4">
                            <div>
                                <ul class="nav navbar-nav">
                                    <li><a href="#" id="btn-save"><i class="fa fa-save"> </i> Guardar</a></li>                                 

                                    <li><a href="#" id="volver"><i class="fa fa-arrow-left"> </i> Volver</a></li>

                                    <li><a href="#" id="sobre_dl"><i class="fa fa-file-word-o" aria-hidden="true"></i></i> Sobre DL</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="panel-body" style="border-top:2px solid #335599;">                        
                    <form class="inline-form" id="form_sancion">
                         <div class="form-group col-md-2">
                            <label for="matricula">CIF</label>
                             @foreach ($contactos as $contacto)
                           <input type="text" class="form-control" name="cif" value="{{$contacto->CIF}}" disabled>
                           @endforeach                     
                        </div>
                        <div class="form-group col-md-3">
                           @foreach ($contactos as $contacto)
                            <label for="empresa">Empresa</label>
                            <input type="text" class="form-control" name="empresa" value="{{$contacto->Empresa}}" disabled>
                            @endforeach                   
                        </div>
                        <div class="form-group col-md-2">
                        @foreach ($contactos as $contacto)
                            <label for="expediente">Nombre</label>
                            <input type="text" class="form-control" name="nombre" value="{{$contacto->Nombre}}">
                              @endforeach                          
                        </div>
                        <div class="form-group col-md-4">
                        @foreach ($contactos as $contacto)
                            <label for="matricula">Apellidos</label>
                            <input type="text" class="form-control" name="apellidos" value="{{$contacto->Apellidos}}">
                              @endforeach                             
                        </div>
                        <div class="form-group col-md-2">
                        @foreach ($contactos as $contacto)
                            <label for="matricula">Siglas</label>
                             <input type="text" class="form-control" name="siglas" value="{{$contacto->Siglas}}">
                               @endforeach                                
                        </div>
                         <div class="form-group col-md-3">
                         @foreach ($contactos as $contacto)
                            <label for="acuerdo">Dirección</label>
                            <input type="text" class="form-control" name="direccion" value="{{$contacto->Direccion}}">   
                              @endforeach                          
                        </div>
                         <div class="form-group col-md-2">
                         @foreach ($contactos as $contacto)
                            <label for="ft">CP</label>
                            <input type="text" class="form-control" name="cp" value="{{$contacto->CP}}">      
                              @endforeach                       
                        </div>
                      <div class="form-group col-md-2">
                      @foreach ($contactos as $contacto)
                            <label for="matricula">Población</label>
                           <input type="text" class="form-control" name="poblacion" value="{{$contacto->Poblacion}}">  
                             @endforeach 
                        </div>
                          <div class="form-group col-md-2">
                           @foreach ($contactos as $contacto)
                                <label for="matricula">Password</label>
                                <input type="text" class="form-control" name="password" value="{{$contacto->password}}">  
                                   @endforeach                 
                        </div>  
                         <div class="form-group col-md-2">
                            <label for="matricula">País</label>
                            <select class="selectpicker" data-live-search="true" title="Buscar..." id="pais" name="pais">
                             <option selected></option>
                                @foreach ($paises as $dato)
                                <option value="{{$dato->Pais}}">{{ $dato->Pais}}</option>
                                @endforeach
                            </select> 
                        </div>
                        <div class="form-group col-md-3">
                        @foreach ($contactos as $contacto)
                            <label for="matricula">Web</label>
                            <input type="text" class="form-control" name="web" value="{{$contacto->web}}">  
                               @endforeach                          
                        </div>
                        
                        <div class="form-group col-md-3">
                        @foreach ($contactos as $contacto)
                                <label for="matricula">Banco</label>
                                <input type="text" class="form-control" name="banco" value="{{$contacto->Banco}}"> 
                                   @endforeach                  
                        </div>
                           <div class="form-group col-md-2">
                           @foreach ($contactos as $contacto)
                                <label for="matricula">Teléfono</label>
                                <input type="text" class="form-control" name="telefono" value="{{$contacto->Tel}}">  
                                   @endforeach                 
                        </div>
                           <div class="form-group col-md-2">
                           @foreach ($contactos as $contacto)
                                <label for="matricula">Fax</label>
                                <input type="text" class="form-control" name="fax" value="{{$contacto->fax}}">   
                                   @endforeach                
                        </div>
                           <div class="form-group col-md-2">
                           @foreach ($contactos as $contacto)
                                <label for="matricula">Móvil</label>
                                <input type="text" class="form-control" name="movil" value="{{$contacto->movil}}">  
                                   @endforeach                 
                        </div>
                           <div class="form-group col-md-3">
                           @foreach ($contactos as $contacto)
                                <label for="matricula">Email</label>
                                <input type="text" class="form-control" name="email" value="{{$contacto->email}}">  
                                   @endforeach                 
                        </div>
                           <div class="form-group col-md-6">
                           @foreach ($contactos as $contacto)
                                <label for="matricula">CC</label>
                                <input type="text" class="form-control" name="cc" value="{{$contacto->CC}}">        
                                   @endforeach           
                        </div>
                            <div class="form-group col-md-6">
                            @foreach ($contactos as $contacto)
                            <label for="matricula">Observaciones</label>
                            <textarea type="textarea" class="form-control" name="observaciones" style="" >{{$contacto->Observaciones}}</textarea> 
                               @endforeach
                            </div> 
                             <div class="form-group col-md-6">
                           <ul class="list-inline">
                         @foreach ($contactos as $contacto)
                         @if($contacto->autorizar == 1)
                          <li><label for="matricula">Autorizar</label> <input type="checkbox" name="autorizar" value="" checked></li>
                          @else
                           <li><label for="matricula">Autorizar</label> <input type="checkbox" name="autorizar" value=""></li>
                           @endif
                             @if($contacto->devolucion == 1)
                          <li><label for="matricula">Impagado/pendiente</label> <input type="checkbox" name="devolucion" value="" checked></li>
                          @else
                           <li><label for="matricula">Impagado/pendiente</label> <input type="checkbox" name="devolucion" value=""></li>
                           @endif
                             @if($contacto->impago_multas == 1)
                          <li><label for="matricula">Impago multas</label> <input type="checkbox" name="impago_multas" value="" checked></li>
                          @else
                           <li><label for="matricula">Impago multas</label> <input type="checkbox" name="impago_multas" value=""></li>
                           @endif
                           @if($contacto->cteConflictivo == 1)
                          <li><label for="matricula">Conflictivo</label> <input type="checkbox" name="Conflictivo" value="" checked></li>
                          @else
                           <li><label for="matricula">Conflictivo</label> <input type="checkbox" name="Conflictivo" value=""></li>
                           @endif
                        @endforeach
                        </ul>
                            </div>
                                                 
                        </div>     
                    </form>
                    </div>
                       <div class="panel panel-default">
              <div class="row"> 
                    <div class="panel-heading" id="detalles-header">                    
                        <div class="col-md-6">
                            <h3 class="module-title">Detalles</h3>
                        </div>
                        <div class="col-md-6">
                            <div>
                                <ul class="nav navbar-nav">
                                    <li class="detalles"><a id="telefono"><i class="fa fa-phone" aria-hidden="true"></i> Telefonos</a></li>
                                    <li class="detalles"><a id= "direcciones"><i class="fa fa-car" aria-hidden="true"></i> Direcciones </a></li>
                                    <li class="detalles"><a id="funciones"><i class="fa fa-info-circle" aria-hidden="true"></i> Funciones</a></li>
                                    <li class="detalles"><a id="tarjetas"><i class="fa fa-credit-card" aria-hidden="true"></i> Tarjetas </a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div id="error" style="display:none" class="alert alert-danger alert-dismissible fade in" role="alert"></div>
                    <div id="success" style="display:none" class="alert alert-success alert-dismissible"></div>
                     <div class="table-responsive" id="tabla_contenido">
                </div>
                    </div>                        
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

$(function() {

 $cif =  $("input[name=cif]").val();
  setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/its/contactos/telefono')}}',
                data : {'cif':$cif},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('#tabla_contenido').html(data);
                    $('.selectpicker').selectpicker('refresh');
                    $('.selectpicker').selectpicker('render');
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);
    });

$("#volver").click(function(){
        window.history.go(-1); return true;
    });


$("#telefono").click(function(){

        $("#error").hide();
    $("#success").hide();
    $("#success").empty();
 
    $cif =  $("input[name=cif]").val();
  setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/its/contactos/telefono')}}',
                data : {'cif':$cif},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('#tabla_contenido').html(data);
                    $('.selectpicker').selectpicker('refresh');
                    $('.selectpicker').selectpicker('render');
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);
    });

$("#direcciones").click(function(){

        $("#error").hide();
    $("#success").hide();
    $("#success").empty();
 
    $cif =  $("input[name=cif]").val();
  setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/its/contactos/direccion')}}',
                data : {'cif':$cif},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('#tabla_contenido').html(data);
                    $('.selectpicker').selectpicker('refresh');
                    $('.selectpicker').selectpicker('render');
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);
    });

$("#funciones").click(function(){

        $("#error").hide();
    $("#success").hide();
    $("#success").empty();
 
    $cif =  $("input[name=cif]").val();
  setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/its/contactos/funciones')}}',
                data : {'cif':$cif},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('#tabla_contenido').html(data);
                    $('.selectpicker').selectpicker('refresh');
                    $('.selectpicker').selectpicker('render');
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);
    });

$("#tarjetas").click(function(){

        $("#error").hide();
    $("#success").hide();
    $("#success").empty();
 
    $cif =  $("input[name=cif]").val();
  setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/its/contactos/tarjetas')}}',
                data : {'cif':$cif},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('#tabla_contenido').html(data);
                    $('.selectpicker').selectpicker('refresh');
                    $('.selectpicker').selectpicker('render');
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);
    });


    $("#volver").click(function(){
        window.history.go(-1); return false;
    });
     $(document).on('focusin','.datepicker',function(){
         $(this).datepicker({
        format: "dd/mm/yyyy",
        dateFormat: 'yy-mm-dd',
        language: "es",
        autoclose: true
    });

         $(this).selectpicker("data-live-search","true");

    });


 $("#btn-save").click(function(){
    
    $form = $("#form_sancion").serialize();

    $cif =  $("input[name=cif]").val();
  
     setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '/contratos/its/contactos/'+$cif+'/guardar',
                data : {'datos' : $("#form_sancion").serialize()},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    if(data.includes("error")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                //location.reload();
                            }
                        });
                    }else{
                         bootbox.alert({
                            message: data,
                            callback: function () {
                                 window.location = '/contratos/its/contactos';
                            }
                        });
                    }
                   
                    $("#loading").hide();

                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);



});


 $(document).on('click', '#add_telefono' ,function(){

$action = 1;
$cif =  $("input[name=cif]").val();
$telefono = $("#addtelefono").val();
$tipo = $("#addtipo :selected").text();

$contacto = $("#addcontacto").val();

if($tipo == "buscar..." || $telefono == ""){
    $("#error").empty();
    $("#error").append("<i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Se han encontrado errores.</strong> El campo del tipo o del teléfono no puede estar vacío.");
    $("#error").show();
}else{
    $("#error").hide();
    $("#success").hide();
    $("#success").empty();

     setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/its/contactos/insertar_its')}}',
                data : {'action':$action,'cif':$cif,'telefono':$telefono,'tipo':$tipo,'contacto':$contacto},
                success : function(data){
                    console.log(JSON.stringify(data));
                    $("#success").append(data);
                    $("#success").show();
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

     $("#telefono")[0].click();
}
});

  $(document).on('click', '#add_direccion' ,function(){

$action = 2;
$cif =  $("input[name=cif]").val();
$direccion = $("#adddireccion").val();
$poblacion = $("#addpoblacion").val();
$cp = $("#addcp").val();
$pais = $("#addpais :selected").text();


if($pais == "buscar..." || $cp == ""){
    $("#error").empty();
    $("#error").append("<i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Se han encontrado errores.</strong> El campo del pais o del Código Postal no puede estar vacío.");
    $("#error").show();
}else{
    $("#error").hide();
    $("#success").hide();
    $("#success").empty();

     setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/its/contactos/insertar_its')}}',
                data : {'action':$action,'cif':$cif,'direccion':$direccion,'poblacion':$poblacion,'cp':$cp,'pais':$pais},
                success : function(data){
                    console.log(JSON.stringify(data));
                    $("#success").append(data);
                    $("#success").show();
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

     $("#direcciones")[0].click();
}
});

  $(document).on('click', '#add_direccion' ,function(){

$action = 2;
$cif =  $("input[name=cif]").val();
$direccion = $("#adddireccion").val();
$poblacion = $("#addpoblacion").val();
$cp = $("#addcp").val();
$pais = $("#addpais :selected").text();


if($pais == "buscar..." || $cp == ""){
    $("#error").empty();
    $("#error").append("<i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Se han encontrado errores.</strong> El campo del pais o del Código Postal no puede estar vacío.");
    $("#error").show();
}else{
    $("#error").hide();
    $("#success").hide();
    $("#success").empty();

     setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/its/contactos/insertar_its')}}',
                data : {'action':$action,'cif':$cif,'direccion':$direccion,'poblacion':$poblacion,'cp':$cp,'pais':$pais},
                success : function(data){
                    console.log(JSON.stringify(data));
                    $("#success").append(data);
                    $("#success").show();
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

     $("#direcciones")[0].click();
}
});

  $(document).on('click', '#add_vinculacion' ,function(){

$action = 3;
$cif =  $("input[name=cif]").val();
$categoria = $("#addcategoria :selected").text();
$vinculacion = $("#addvinculacion :selected").text();

if($categoria == "buscar..." || $vinculacion == "buscar..."){
    $("#error").empty();
    $("#error").append("<i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Se han encontrado errores.</strong> El campo de categoria o vinculación no pueden estar vacíos.");
    $("#error").show();
}else{
    $("#error").hide();
    $("#success").hide();
    $("#success").empty();

     setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/its/contactos/insertar_its')}}',
                data : {'action':$action,'cif':$cif,'categoria':$categoria,'vinculacion':$vinculacion},
                success : function(data){
                    console.log(JSON.stringify(data));
                    $("#success").append(data);
                    $("#success").show();
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

     $("#funciones")[0].click();
}
});

  $("#sobre_dl").click(function(){
    $cif =  $("input[name=cif]").val();

    $dialog = bootbox.dialog({
                        message: '<p class="text-center">Generando el sobre, espere por favor...</p>',
                        closeButton: false
                    });

    setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/its/contactos/exportar_sobre')}}',
                data : {'cif':$cif},            
                success : function(data){
                    window.location = this.url;
                    $dialog.modal('hide');
                },
                error : function(data){
                    //console.log(JSON.stringify(data));
                    bootbox.alert("Ha ocurrido un error generando su informe.");
                    $dialog.modal('hide');
                }
            });
        }, 500);
  });

</script>
@endsection