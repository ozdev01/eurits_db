@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xl-12 col-md-offset-0">
            <div class="panel panel-default">
                <div class="row"> 
                    <div class="panel-heading" id="sanciones-header">                    
                        <div class="col-md-8">
                            <h3 class="module-title">Nuevo contacto</h3>
                        </div>
                        <div class="col-md-4">
                            <div>
                                <ul class="nav navbar-nav">
                                    <li><a href="#" id="btn-save"><i class="fa fa-save"> </i> Guardar</a></li>                                 

                                    <li><a href="#" id="volver"><i class="fa fa-arrow-left"> </i> Volver</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="panel-body" style="border-top:2px solid #335599;">                        
                    <form class="inline-form" id="form_sancion">
                         <div class="form-group col-md-2">
                            <label for="matricula">CIF</label>
                           <input type="text" class="form-control" name="cif" value="">                     
                        </div>
                        <div class="form-group col-md-3">
                            <label for="empresa">Empresa</label>
                            <input type="text" class="form-control" name="empresa" value="">                   
                        </div>
                        <div class="form-group col-md-2">
                            <label for="expediente">Nombre</label>
                            <input type="text" class="form-control" name="nombre" value="">                         
                        </div>
                        <div class="form-group col-md-4">
                            <label for="matricula">Apellidos</label>
                            <input type="text" class="form-control" name="apellidos" value="">                            
                        </div>
                        <div class="form-group col-md-2">
                            <label for="matricula">Siglas</label>
                             <input type="text" class="form-control" name="siglas" value="">                               
                        </div>
                         <div class="form-group col-md-4">
                            <label for="acuerdo">Dirección</label>
                            <input type="text" class="form-control" name="direccion" value="">                            
                        </div>
                         <div class="form-group col-md-2">
                            <label for="ft">CP</label>
                            <input type="text" class="form-control" name="cp" value="">                            
                        </div>
                      <div class="form-group col-md-3">
                            <label for="matricula">Población</label>
                           <input type="text" class="form-control" name="poblacion" value="">  
                        </div>
                        <!--<div class="form-group col-md-2">
                            <label for="matricula">Provincia</label>
                            <select class="selectpicker" data-live-search="true" title="Buscar..." id="pais" name="pais">
                            <option selected></option>
                            @foreach ($provincias as $provincia)
                             <option value="{{$provincia->IdProv}}">{{$provincia->Provincia}}</option>
                            @endforeach
                            </select> 
                        </div>-->
                         <div class="form-group col-md-2">
                            <label for="matricula">País</label>
                            <!--<input type="text" class="form-control" name="matricula" value="ESPAÑA">-->
                            <select class="selectpicker" data-live-search="true" title="Buscar..." id="pais" name="pais">
                             <option selected></option>
                                @foreach ($paises as $dato)
                                <option value="{{$dato->Pais}}">{{ $dato->Pais}}</option>
                                @endforeach
                            </select> 
                        </div>
                        <div class="form-group col-md-3">
                            <label for="matricula">Web</label>
                            <input type="text" class="form-control" name="web" value="">                            
                        </div>
                        
                        <div class="form-group col-md-3">
                                <label for="matricula">Banco</label>
                                <input type="text" class="form-control" name="banco" value="">                   
                        </div>
                           <div class="form-group col-md-2">
                                <label for="matricula">Teléfono</label>
                                <input type="text" class="form-control" name="telefono" value="">                   
                        </div>
                           <div class="form-group col-md-2">
                                <label for="matricula">Fax</label>
                                <input type="text" class="form-control" name="fax" value="">                   
                        </div>
                           <div class="form-group col-md-2">
                                <label for="matricula">Móvil</label>
                                <input type="text" class="form-control" name="movil" value="">                   
                        </div>
                           <div class="form-group col-md-3">
                                <label for="matricula">Email</label>
                                <input type="text" class="form-control" name="email" value="">                   
                        </div>
                           <div class="form-group col-md-6">
                                <label for="matricula">CC</label>
                                <input type="text" class="form-control" name="cc" value="">                   
                        </div>
                            <div class="form-group col-md-6">
                            <label for="matricula">Observaciones</label>
                            <textarea type="textarea" class="form-control" name="observaciones" style="" ></textarea> 
                            </div>                        
                        </div>     
                    </form>
                    </div>                        
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $("#volver").click(function(){
        window.history.go(-1); return false;
    });
     $(document).on('focusin','.datepicker',function(){
         $(this).datepicker({
        format: "dd/mm/yyyy",
        dateFormat: 'yy-mm-dd',
        language: "es",
        autoclose: true
    });

         $(this).selectpicker("data-live-search","true");

    });


 $("#btn-save").click(function(){
    
    $form = $("#form_sancion").serialize();
  
     setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/its/contactos/store')}}',
                data : {'datos' : $("#form_sancion").serialize()},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    if(data.includes("error")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                //location.reload();
                            }
                        });
                    }else{
                         bootbox.alert({
                            message: data,
                            callback: function () {
                                 window.location = '/contratos/its/contactos';
                            }
                        });
                    }
                   
                    $("#loading").hide();

                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);



});
</script>
@endsection