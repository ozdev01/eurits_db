@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xl-12 col-md-offset-0">
            <div class="panel panel-default">
                <div class="row"> 
                    <div class="panel-heading" id="sanciones-header">                    
                        <div class="col-md-8">
                            <h3 class="module-title">Nuevo contrato ITS</h3>
                        </div>
                        <div class="col-md-4">
                            <div>
                                <ul class="nav navbar-nav">
                                    <li><a href="#" id="btn-save"><i class="fa fa-save"> </i> Guardar</a></li>                                 

                                    <li><a href="#" id="volver"><i class="fa fa-arrow-left"> </i> Volver</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="panel-body" style="border-top:2px solid #335599;">                        
                     <form class="inline-form" id="form_contrato">
                        <div class="form-group col-md-4">
                            <label for="matricula">Empresa</label>
                             <select class="selectpicker" data-live-search="true" title="Buscar..." id="empresa" name="empresa">
                             <option selected></option>
                                @foreach ($clientes as $cliente)
                                <option>{{ $cliente->Empresa}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="empresa">Tipo Contrato</label>
                            <select class="selectpicker" data-live-search="true" title="Buscar..." id="tipo_contrato" name="tipo_contrato">
                 <option selected></option>
                   @foreach ($tipo_contrato as $tipo)
                    <option>{{ $tipo->TipoServicio}}</option>
                    @endforeach
                </select>                                
                        </div>
                        <div class="form-group col-md-3">
                            <label for="cif">Contratante</label>
                          <select class="selectpicker" data-live-search="true" title="Buscar..." id="contratante" name="contratante">
                 <option selected></option>
                    @foreach ($contratantes as $contratante)
                    <option>{{ $contratante->Contratante}}</option>
                    @endforeach
                </select>
                          </div>
                        <div class="form-group col-md-3">
                            <label for="matricula">Fecha Contrato</label>
                            <input type="text" class="form-control datepicker" name="fechaContrato" id="fechaContrato" value="">                            
                        </div>           
                    </form>
                    </div>                        
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $("#volver").click(function(){
        window.history.go(-1); return false;
    });
     $(document).on('focusin','.datepicker',function(){
         $(this).datepicker({
        format: "dd/mm/yyyy",
        dateFormat: 'yy-mm-dd',
        language: "es",
        autoclose: true
    });

         $(this).selectpicker("data-live-search","true");

    });

    $("#justif").click(function(){

if($("#justif").is(':checked')){
    $("#labeljustif").css("display","");
    $("#fecha_justif").css("display","");
}else{
    $("#labeljustif").css("display","none");
    $("#fecha_justif").css("display","none");
}

});

 $("#estadosancion").click(function(){


if($("#estadosancion").is(':checked')){
    $("#inactivo").css("display","");
}else{
    $("#inactivo").css("display","none");
}

});


 $("#btn-save").click(function(){
    
    $form = $("#form_contrato").serialize();
  
     setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/store')}}',
                data : {'datos' : $("#form_contrato").serialize()},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    if(data.includes("error")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                //location.reload();
                            }
                        });
                    }else{
                         bootbox.alert({
                            message: data,
                            callback: function () {
                                 window.location = '/contratos/its';
                            }
                        });
                    }
                   
                    $("#loading").hide();

                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);



});
</script>
@endsection