@extends('layouts.app')
@section('content')

<div class="container">
    <div class="row">
        <div class="col-sm-2">
         
         <!--   <h4>Búsqueda</h4>           
            <!--<label for="search" id="loading" style="display:none;">
                <i class="fa fa-refresh fa-spin fa-1x fa-fw"></i>
            </label>-->
                <!--  <input type="text" class="form-control" id="search" name="search" placeholder="Buscar...">-->
            <h4>Filtros</h4>
            <div class =filtros>
                 <a data-toggle="collapse" href="#fechallegada" aria-expanded="false" aria-controls="fechallegada"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> fecha de Contrato</h5></a>
                <div class="collapse" id="fechallegada">
                <input type="text" class="form-control datepicker" name="fecha_alta_inicio" id="fecha_alta_inicio" value="" placeholder="fecha de inicio...">
                <br>
                <input type="text" class="form-control datepicker" id="fecha_alta_fin" value="" placeholder="fecha de fin...">
                </div>
            </div>
              <div class="filtros">
                <a data-toggle="collapse" href="#fechaanexo" aria-expanded="false" aria-controls="fechaanexo"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Alta Anexo</h5></a>
                <div class="collapse" id="fechaanexo">
                <input type="text" class="form-control datepicker" name="alta_anexo_inicio" id="alta_anexo_inicio" value="" placeholder="fecha de inicio...">
                <br>
                <input type="text" class="form-control datepicker" id="alta_anexo_fin" name="alta_anexo_fin" value="" placeholder="fecha de fin...">
                </div>
            </div>
             <div class="filtros">
                <a data-toggle="collapse" href="#fechabaja" aria-expanded="false" aria-controls="fechabaja"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Baja Anexo</h5></a>
                <div class="collapse" id="fechabaja">
                <input type="text" class="form-control datepicker" name="baja_anexo_inicio" id="baja_anexo_inicio" value="" placeholder="fecha de inicio...">
                <br>
                <input type="text" class="form-control datepicker" id="baja_anexo_fin" name="baja_anexo_fin" value="" placeholder="fecha de fin...">
                </div>
            </div>
             <div class="filtros">
           
                <a data-toggle="collapse" href="#fechavenc" aria-expanded="false" aria-controls="fechavenc"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Vencimiento</h5></a>
                <div class="collapse" id="fechavenc">
                <input type="text" class="form-control datepicker" name="venc_anexo_inicio" id="venc_anexo_inicio" value="" placeholder="fecha de inicio...">
                <br>
                <input type="text" class="form-control datepicker" id="venc_anexo_fin" name="venc_anexo_fin" value="" placeholder="fecha de fin...">
                </div>
            </div>
                <div class =filtros>
                 <a data-toggle="collapse" href="#colcodcontrato" aria-expanded="false" aria-controls="colcodcontrato"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Código Contrato</h5></a>
                <div class="collapse" id="colcodcontrato">
                <input type="text" class="form-control" name="codContrato" id="codContrato" value="" placeholder="escriba el código...">
                </div>
            </div>
              <div class =filtros>
                 <a data-toggle="collapse" href="#colcodanexo" aria-expanded="false" aria-controls="colcodanexo"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Código Anexo</h5></a>
                <div class="collapse" id="colcodanexo">
                <input type="text" class="form-control" name="colcodanexo" id="colcodanexo" value="" placeholder="escriba el código...">
                </div>
            </div>
             <div class =filtros>
                 <a data-toggle="collapse" href="#colimporte" aria-expanded="false" aria-controls="colimporte"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Importe</h5></a>
                <div class="collapse" id="colimporte">
                <input type="number" class="form-control" name="importe" id="importe" value="" placeholder="escriba el importe...">
                </div>
            </div>
                <div class =filtros>
                 <a data-toggle="collapse" href="#coldescuento" aria-expanded="false" aria-controls="coldescuento"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Descuento</h5></a>
                <div class="collapse" id="coldescuento">
                <input type="number" class="form-control" name="descuento" id="descuento" value="" placeholder="">
                </div>
            </div>
              <div class =filtros>
                 <a data-toggle="collapse" href="#colcomision" aria-expanded="false" aria-controls="colcomision"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Comisión</h5></a>
                <div class="collapse" id="colcomision">
                <input type="numer" class="form-control" name="comision" id="comision" value="" placeholder="">
                </div>
            </div>
              <div class =filtros>
                 <a data-toggle="collapse" href="#colempresa" aria-expanded="false" aria-controls="colempresa"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Empresa</h5></a>
                <div class="collapse" id="colempresa">
               <select class="selectpicker" data-live-search="true" title="Buscar..." id="empresa" name="empresa">
                 <option selected></option>
                    @foreach ($empresas as $empresa)
                    <option value="{{$empresa->CodContacto}}">{{ $empresa->razonSocial}}</option>
                    @endforeach
                </select>
                </div>
            </div>
            <div class =filtros>
                 <a data-toggle="collapse" href="#colcliotro" aria-expanded="false" aria-controls="colcliotro"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Cliente Otro comercial</h5></a>
                <div class="collapse" id="colcliotro">
                <input type="checkbox" name="otrocomercial" value="" id="otrocomercial">
                </div>
            </div>
              <div class =filtros>
                 <a data-toggle="collapse" href="#colsustituido" aria-expanded="false" aria-controls="colsustituido"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Fue sustituido?</h5></a>
                <div class="collapse" id="colsustituido">
                <input type="checkbox" name="sustituido" value="" id="sustituido">
                </div>
            </div>
               <div class =filtros>
                 <a data-toggle="collapse" href="#colsustituyente" aria-expanded="false" aria-controls="colsustituyente"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> ¿Sustituyó?</h5></a>
                <div class="collapse" id="colsustituyente">
                <input type="checkbox" name="sustituyente" value="" id="sustituyente">
                </div>
            </div>
              <div class =filtros>
                 <a data-toggle="collapse" href="#colrenovacion" aria-expanded="false" aria-controls="colrenovacion"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> ¿Es una renovación?</h5></a>
                <div class="collapse" id="colrenovacion">
                <input type="checkbox" name="esRenovacion" value="" id="esRenovacion">
                </div>
            </div>
              <div class =filtros>
                 <a data-toggle="collapse" href="#colnorenovar" aria-expanded="false" aria-controls="colnorenovar"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Marcado NO RENOVAR</h5></a>
                <div class="collapse" id="colnorenovar">
                <input type="checkbox" name="norenovar" value="" id="norenovar">
                </div>
            </div>
              <div class =filtros>
                 <a data-toggle="collapse" href="#coltipoanexo" aria-expanded="false" aria-controls="coltipoanexo"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Tipo de Anexo</h5></a>
                <div class="collapse" id="coltipoanexo">
                <select class="selectpicker" data-live-search="true" title="Buscar..." id="tipo_anexo" name="tipo_anexo">
                 <option selected></option>
                    @foreach ($tipos_anexo as $tipo)
                    <option>{{ $tipo->tipoAnexo}}</option>
                    @endforeach
                </select>
                </div>
            </div>
            <div class="filtros">
                <a data-toggle="collapse" href="#colcontratante" aria-expanded="false" aria-controls="colcontratante"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Empresa Comercial</h5></a>
                 <div class="collapse" id="colcontratante">
                 <select class="selectpicker" data-live-search="true" title="Buscar..." id="contratante" name="contratante">
                 <option selected></option>
                    @foreach ($contratantes as $contratante)
                    <option value="{{$contratante->CodContacto}}">{{ $contratante->razonSocial}}</option>
                    @endforeach
                </select>
                </div>
            </div>
            <div class="filtros">
                <a data-toggle="collapse" href="#colcomercial" aria-expanded="false" aria-controls="colcomercial"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Nombre Comercial</h5></a>
                 <div class="collapse" id="colcomercial">
                  <select class="selectpicker" data-live-search="true" title="Buscar..." id="comercial" name="comercial">
                 <option selected></option>
                    @foreach ($comerciales as $comercial)
                    <option value="{{$comercial->ID}}">{{ $comercial->comercial}}</option>
                    @endforeach
                </select>
                </div>
            </div>
              <div class="filtros">
                 <a data-toggle="collapse" href="#colabono" aria-expanded="false" aria-controls="colabono"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Tipo de Abono</h5></a>
                <div class="collapse" id="colabono">
                  <select class="selectpicker" data-live-search="true" title="Buscar..." id="tipo_abono" name="tipo_abono">
                 <option selected></option>
                    <option value="-2">Abono Normal (reduce cto/comis)</option>
                    <option value="-3">Abono Multa, POST FACTURA (NO reduce contrato/comi.)</option>
                    <option value="0">Abono Multa, MODIFICA factura (NO reduce contrato/comi.)</option>
                    <option value="-1">Abono Multa, REDUCE contrato y comisión</option>
                </select>
                </div>
            </div>
            <div class="filtros">
            <a data-toggle="collapse" href="#colmatricula" aria-expanded="false" aria-controls="colmatricula"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Matricula</h5></a>
            <div class="collapse" id="colmatricula">
             <select class="selectpicker" data-live-search="true" title="Buscar..." id="matricula" name="matricula">
                 <option selected></option>
                    @foreach ($matriculas as $matricula)
                    <option>{{ $matricula->Matricula}}</option>
                    @endforeach
                </select>
                </div>
                </div>
                <div class="filtros">
            <a data-toggle="collapse" href="#colremolque" aria-expanded="false" aria-controls="colremolque"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Remolque</h5></a>
            <div class="collapse" id="colremolque">
             <select class="selectpicker" data-live-search="true" title="Buscar..." id="remolque" name="remolque">
                 <option selected></option>
                    @foreach ($remolques as $remolque)
                    <option>{{ $remolque->Matricula}}</option>
                    @endforeach
                </select>
                </div>
                </div>
            <div class="filtros">
            <button type="button" id="filtrar" class="btn btn-default btn-primary">filtrar</button>
            </div>
        </div>
        <div class="col-sm-10">
            <div class="panel panel-default">
                <div class="row"> 
                    <div class="panel-heading" id="sanciones-header">                    
                        <div class="col-md-2" style="width: 100%">
                            <h3 class="module-title">Anexos Europa</h3>
                        </div>
                        <div class="col-md-10">
                            <div>
                                <ul class="list-inline">
                                   <li><a href="{{url('contratos/europa')}}" id="volver"><i class="fa fa-arrow-left"> </i> Volver a Contratos</a></li>
                                   <li><a href="#" id="excel"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Excel</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table-responsive"">
                    <table class="table">
                        <thead class="table-header">
                            <th>Cliente</th>                        
                            <th>Tipo</th>
                            <th>Matricula</th>
                            <th>Fecha de Contrato</th>
                            <th>Alta</th>
                            <th>Baja</th>
                            <th></th>
                        </thead>
                        <tbody id="myTable">
                        @foreach ($anexos as $anexo)
                        <tr>
                            <td>{{ $anexo->cliente }} </td>
                            <td>{{ $anexo->tipo }} </td>
                            <td>{{ $anexo->matricula }} </td>
                            <td>{{ date('d - m - Y', strtotime($anexo->fechadeContrato)) }} </td>
                            <td>{{ date('d - m - Y', strtotime($anexo->fechaAlta)) }} </td>
                            <td>{{ date('d - m - Y', strtotime($anexo->fechaBaja)) }} </td>
                             <td><a class="btn btn-sm btn-success" href="{{URL::to('contratos/europa/anexos/'.$anexo->CodAnexo)}}"><i class="fa fa-edit"></i> </a></td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    
                </div>

                 <div class="col-md-12 text-center">
      <ul class="pagination pagination-lg pager" id="myPager"></ul>
      </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

 $('.datepicker').datepicker({
        format: "dd/mm/yyyy",
        dateFormat: 'yy-mm-dd',
        language: "es",
        autoclose: true
    });
    
    $('#search').on('keyup',function(){
        $value=$(this).val();
        //alert($value);
        $("#loading").show();
        setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/search_europa')}}',
                data : {'search':$value},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('tbody').html(data);
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);
});

$( "#filtrar" ).click(function() {
$fecha_alta_inicio = $("#fecha_alta_inicio").val();
$fecha_alta_fin = $("#fecha_alta_fin").val();

$alta_anexo_inicio = $("#alta_anexo_inicio").val();
$alta_anexo_fin = $("#alta_anexo_fin").val();

$baja_anexo_inicio = $("#baja_anexo_inicio").val();
$baja_anexo_fin = $("#baja_anexo_fin").val();

$venc_anexo_inicio = $("#venc_anexo_inicio").val();
$venc_anexo_fin = $("#venc_anexo_fin").val();

$codContrato = $("#codContrato").val();
$codAnexo = $("#codAnexo").val();
$importe = $("#importe").val();
$descuento = $("#descuento").val();
$comision = $("#comision").val();

$otrocomercial = 0;
$sustituido = 0;
$sustituyente = 0;
$esRenovacion = 0;
$norenovar = 0;

if($("#otrocomercial").prop("checked")){
$otrocomercial = 1;
}

if($("#sustituido").prop("checked")){
$sustituido = 1;
}
if($("#sustituyente").prop("checked")){
$sustituyente = 1;
}
if($("#esRenovacion").prop("checked")){
$esRenovacion = 1;
}
if($("#norenovar").prop("checked")){
$norenovar = 1;
}


$contratante = $("#contratante option:selected").val();

$empresa =  $("#empresa option:selected").val();

$comercial =  $("#comercial option:selected").val();

$matricula = $("#matricula option:selected").text();

$remolque = $("#remolque option:selected").text();

$tipo_anexo = $("#tipo_anexo option:selected").text();

$tipo_abono = $("#tipo_abono option:selected").val();



$alta_contrato_inicio = $fecha_alta_inicio.split("/").reverse().join("-");
$alta_contrato_fin = $fecha_alta_fin.split("/").reverse().join("-");

$alta_inicio = $alta_anexo_inicio.split("/").reverse().join("-");
$alta_fin = $alta_anexo_fin.split("/").reverse().join("-");

$baja_inicio = $baja_anexo_inicio.split("/").reverse().join("-");
$baja_fin = $baja_anexo_fin.split("/").reverse().join("-");

$vencimiento_inicio = $venc_anexo_inicio.split("/").reverse().join("-");
$vencimiento_fin = $venc_anexo_fin.split("/").reverse().join("-");


//alert($inicio);

        $dialog = bootbox.dialog({
                        message: '<p class="text-center">Cargando datos, espere por favor...</p>',
                        closeButton: false
                    });

    setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/europa/filtra_anexo')}}',
                data : {'alta_contrato_inicio':$alta_contrato_inicio,'alta_contrato_fin':$alta_contrato_fin,'alta_inicio':$alta_inicio,'alta_fin':$alta_fin,'baja_inicio':$baja_inicio,'baja_fin':$baja_fin,'vencimiento_inicio':$vencimiento_inicio,'vencimiento_fin':$vencimiento_fin,'contratante':$contratante,'empresa':$empresa,'matricula':$matricula,'tipo_anexo':$tipo_anexo,'tipo_abono':$tipo_abono,'comercial':$comercial,'otrocomercial':$otrocomercial,'codContrato':$codContrato,'importe':$importe,'descuento':$descuento,'comision':$comision,'remolque':$remolque,'sustituido':$sustituido,'sustituyente':$sustituyente,'esRenovacion':$esRenovacion,'norenovar':$norenovar,'codAnexo':$codAnexo},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('tbody').html(data);

                    // do something in the background
                    $dialog.modal('hide');
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

});

$( "#excel" ).click(function() {
$fecha_alta_inicio = $("#fecha_alta_inicio").val();
$fecha_alta_fin = $("#fecha_alta_fin").val();

$alta_anexo_inicio = $("#alta_anexo_inicio").val();
$alta_anexo_fin = $("#alta_anexo_fin").val();

$baja_anexo_inicio = $("#baja_anexo_inicio").val();
$baja_anexo_fin = $("#baja_anexo_fin").val();

$venc_anexo_inicio = $("#venc_anexo_inicio").val();
$venc_anexo_fin = $("#venc_anexo_fin").val();

$codContrato = $("#codContrato").val();
$codAnexo = $("#codAnexo").val();
$importe = $("#importe").val();
$descuento = $("#descuento").val();
$comision = $("#comision").val();

$otrocomercial = 0;
$sustituido = 0;
$sustituyente = 0;
$esRenovacion = 0;
$norenovar = 0;

if($("#otrocomercial").prop("checked")){
$otrocomercial = 1;
}

if($("#sustituido").prop("checked")){
$sustituido = 1;
}
if($("#sustituyente").prop("checked")){
$sustituyente = 1;
}
if($("#esRenovacion").prop("checked")){
$esRenovacion = 1;
}
if($("#norenovar").prop("checked")){
$norenovar = 1;
}


$contratante = $("#contratante option:selected").val();

$empresa =  $("#empresa option:selected").val();

$comercial =  $("#comercial option:selected").val();

$matricula = $("#matricula option:selected").text();

$remolque = $("#remolque option:selected").text();

$tipo_anexo = $("#tipo_anexo option:selected").text();

$tipo_abono = $("#tipo_abono option:selected").val();



$alta_contrato_inicio = $fecha_alta_inicio.split("/").reverse().join("-");
$alta_contrato_fin = $fecha_alta_fin.split("/").reverse().join("-");

$alta_inicio = $alta_anexo_inicio.split("/").reverse().join("-");
$alta_fin = $alta_anexo_fin.split("/").reverse().join("-");

$baja_inicio = $baja_anexo_inicio.split("/").reverse().join("-");
$baja_fin = $baja_anexo_fin.split("/").reverse().join("-");

$vencimiento_inicio = $venc_anexo_inicio.split("/").reverse().join("-");
$vencimiento_fin = $venc_anexo_fin.split("/").reverse().join("-");


//alert($inicio);

        $dialog = bootbox.dialog({
                        message: '<p class="text-center">Generando su Excel, espere por favor...</p>',
                        closeButton: false
                    });

    setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/europa/generar_excel_anexos')}}',
                data : {'alta_contrato_inicio':$alta_contrato_inicio,'alta_contrato_fin':$alta_contrato_fin,'alta_inicio':$alta_inicio,'alta_fin':$alta_fin,'baja_inicio':$baja_inicio,'baja_fin':$baja_fin,'vencimiento_inicio':$vencimiento_inicio,'vencimiento_fin':$vencimiento_fin,'contratante':$contratante,'empresa':$empresa,'matricula':$matricula,'tipo_anexo':$tipo_anexo,'tipo_abono':$tipo_abono,'comercial':$comercial,'otrocomercial':$otrocomercial,'codContrato':$codContrato,'importe':$importe,'descuento':$descuento,'comision':$comision,'remolque':$remolque,'sustituido':$sustituido,'sustituyente':$sustituyente,'esRenovacion':$esRenovacion,'norenovar':$norenovar,'codAnexo':$codAnexo},            
                 success : function(data){
                    window.location = this.url;
                    $dialog.modal('hide');
                },
                error : function(data){
                    bootbox.alert("Ha ocurrido un error generando su Excel.");
                    $dialog.modal('hide');
                }
            });
        }, 500);

});
</script>
@endsection