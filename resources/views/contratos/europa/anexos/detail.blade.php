@extends('layouts.app')
@section('content')

   
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="row"> 
                    <div class="panel-heading" id="sanciones-header">                    
                        <div class="col-md-12">
                            <h3 class="module-title">Anexo {{$anexo->CodAnexo}}</h3>
                         <input type="hidden" class="form-control" name="CodAnexo" id="CodAnexo" value="{{$anexo->CodAnexo}}" >   
                        </div>
                        <div class="col-md-12">
                            <div>
                                <ul class="nav navbar-nav">
                                    <li><a href="#" class="bt-edit"><i class="fa fa-save"> </i> Guardar</a></li>
                                    <li><a href="#" class="bt-sustituir" id="sustituir"><i class="fa fa-sticky-note" aria-hidden="true"></i> Sustituir anexo</a></li>
                                     <li><a href="#" class="bt-sustituir_remolque" id="sustituir_remolque"><i class="fa fa-sticky-note" aria-hidden="true"></i> Sustituir remolque</a></li>
                                      <a class="dropdown-toggle" href="#" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <li> <i class="fa fa-clone" aria-hidden="true"></i> Clonar</a>
                                    <ul class="dropdown-menu" role="menu">
                                         <li><a class="dropdown-item" id="clonar_gesanc" href="#">A GESANC</a></li>
                                        <li><a class="dropdown-item" id="clonar_its" href="#" id="newtipo">A ITS</a></li>
                                    </ul></li>
                                    <li><a href="#" class="bt-erase"><i class="fa fa-trash" aria-hidden="true"></i> Eliminar</a></li>
                                    <li><a href="#" id="volver"><i class="fa fa-arrow-left"> </i> Volver</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="panel-body" style="border-top:2px solid #335599;">                        
                    <form class="inline-form" id="form_contrato">
                        <div class="form-group col-md-4">
                            <label for="Beneficiario">Cliente</label>
                            <input type="text" class="form-control" name="cliente" id="cliente" value="{{$cliente->razonSocial}}" disabled>
                             <input type="hidden" class="form-control" name="codContratado" id="codContratado" value="{{$contrato->codContratado}}" >
                             <input type="hidden" class="form-control" name="condiciones" id="condiciones" value="{{$contrato->codCondiciones}}" >   
                              <input type="hidden" class="form-control" name="codComercial" id="codComercial" value="{{$contrato->codComercial}}" >           
                        </div>
                         <div class="form-group col-md-2">
                            <label for="Beneficiario">Contrato</label>
                            <input type="text" class="form-control" name="codContrato" id="codContrato" value="{{$contrato->codContrato}}" disabled>
                            <input type="hidden" class="form-control" name="codContrato" id="codContrato" value="{{$contrato->codContrato}}" >
                            <input type="hidden" class="form-control" name="codContratante" id="codContratante" value="{{$contrato->codContratante}}" >                
                        </div>
                         <div class="form-group col-md-2">
                            <label for="Beneficiario">Fecha Alta</label>
                            <input type="text" class="form-control" name="cliente" id="cliente" value="{{ date('d/m/Y', strtotime($contrato->fechaContrato)) }}" disabled>           
                        </div>
                        <div class="form-group col-md-3">
                            <label for="empresa">Matricula</label>
                            <select class="selectpicker" data-live-search="true" title="Buscar..." id="matricula" name="matricula">
                             <option selected value="{{$anexo->Matricula}}">{{$anexo->Matricula}}</option>
                                @foreach ($matriculas as $matricula)
                                <option>{{ $matricula->Matricula}}</option>
                                @endforeach
                            </select>
                        </div>
                         <div class="form-group col-md-2">
                            <label for="Beneficiario">Sustituye a</label>
                            <input type="text" class="form-control" name="cliente" id="cliente" value="{{ $sustituye}}" disabled>           
                        </div>
                         <div class="form-group col-md-2">
                            <label for="Beneficiario">Sustituido Por</label>
                            <input type="text" class="form-control" name="cliente" id="cliente" value="{{ $sustituido}}" disabled>           
                        </div>
                        <div class="form-group col-md-4">
                            <label for="codigo">Tipo de Anexo</label>
                            <select class="selectpicker" data-live-search="true" title="Buscar..." id="tipo_anexo" name="tipo_anexo">
                            <option selected value="{{$anexo->CodTipoAnexo}}">{{$anexo->tipo}}</option>
                                @foreach ($tipos_anexo as $tipo)
                                <option value="{{ $tipo->CodTipoAnexo}}">{{ $tipo->tipoAnexo}}</option>
                                @endforeach
                            </select>
                        </div>
                         <div class="form-group col-md-2">
                            <label for="Beneficiario">Importe</label>
                            <input type="text" class="form-control" name="importe" id="importe" value="{{$anexo->importeVenta}}">
                        </div>
                            <div class="form-group col-md-2">
                            <label for="codigo">No aplicar precio</label>
                     
                            <a class="btn btn-danger" href="#" id="aplicarprecio" aria-label="Delete">
                              <i class="fa fa-times" aria-hidden="true"></i>
                            </a>
                         
                        </div>
                        <div class="form-group col-md-3">
                            <label for="matricula">Fecha Alta</label>
                            <input type="text" class="form-control datepicker" name="alta" id="alta" value="{{ date('d/m/Y', strtotime($anexo->Alta)) }}">                       
                        </div>
                            <div class="form-group col-md-3">
                            <label for="matricula">Fecha Baja</label>
                            @if($anexo->Baja!=null)
                            <input type="text" class="form-control datepicker" name="baja" id="baja" value="{{ date('d/m/Y', strtotime($anexo->Baja)) }}">
                            @else
                            <input type="text" class="form-control datepicker" name="baja" id="baja" value="">
                            @endif                       
                        </div>
                         <div class="form-group col-md-4">
                            <label for="Beneficiario">Motivo Baja</label>
                            <select class="selectpicker" data-live-search="true" title="Buscar..." id="motivo_baja" name="motivo_baja">
                            @if($anexo->Motivo!=null)
                            <option selected value="{{$anexo->CodMotivoBaja}}">{{$anexo->Motivo}}</option>
                            @else
                             <option selected></option>
                             @endif
                                @foreach ($motivos_baja as $motivo)
                                <option value="{{ $motivo->CodMotivobaja}}">{{ $motivo->motivobaja}}</option>
                                @endforeach
                            </select>        
                        </div>
                         <div class="form-group col-md-6">
                            <label for="codigo">Notas</label>
                     
                            <input type="text" class="form-control" name="notas" id="notas" value="{{$anexo->Notas}}">
                     
                        </div>
                         <div class="form-group col-md-2">
                            <label for="codigo">¿Renovación?</label>
                            @if($anexo->esRenovacion==1)
                            <input type="checkbox"  name="esrenovacion" id="esrenovacion" value="" checked>
                            @else
                             <input type="checkbox"  name="esrenovacion" id="esrenovacion" value="">
                             @endif
                         
                        </div>
                         <div class="form-group col-md-2">
                            <label for="codigo">¿No renovar?</label>
                            @if($anexo->NOrenovar==1)
                            <input type="checkbox" name="norenovar" id="norenovar" value="" checked>
                            @else
                            <input type="checkbox" name="norenovar" id="norenovar" value="">
                            @endif
                        </div>
                                 
                    </form>
                    </div>
                     <div class="panel panel-default">
              <div class="row"> 
                    <div class="panel-heading" id="detalles-header">                    
                        <div class="col-md-9">
                            <h3 class="module-title"> Anexos remolques</h3>
                                <div class="col-md-12">
                        </div>
                        </div>
                    </div>
                    </div>
                    <div id="error" style="display:none" class="alert alert-danger alert-dismissible fade in" role="alert"></div>
                    <div id="success" style="display:none" class="alert alert-success alert-dismissible"></div>
                     <div class="table-responsive" id="tabla_contenido">
                    <table class="table">
                        <thead class="table-header">        
                            <th>Matricula</th>
                            <th>Alta</th>
                             <th>Baja</th>
                             <th>Motivo</th>
                            <th></th>
                        </thead>
                        <tbody id="myTable">
                        @foreach ($remolques as $remolque)
                       <tr>
                       <td><select class="selectpicker matricula_remolque"  data-container="body" data-live-search="true" title="Buscar..."  name="matricula_remolque">
                             <option selected value="{{$remolque->matricula}}">{{$remolque->matricula}}</option>
                                @foreach ($matriculas_remolque as $matricula)
                                <option>{{ $matricula->Matricula}}</option>
                                @endforeach
                            </select><input type="hidden" class="codAnexoRemolque" name="idAnexo" value="{!!$remolque->codAnexoRemolque !!}"></td>
                       <td><input type="text" class="form-control datepicker alta_remolque" name="alta_remolque" value="{{ date('d/m/Y', strtotime($remolque->Alta))}}"></td>
                       @if($remolque->Baja!=null)
                       <td><input type="text" class="form-control datepicker baja_remolque" name="baja_remolque" value="{{ date('d/m/Y', strtotime($remolque->Baja))}}"></td>
                       @else
                       <td><input type="text" class="form-control datepicker baja_remolque" name="baja_remolque" value=""></td>
                       @endif
                       <td><select class="selectpicker motivo_baja" data-container="body"  data-live-search="true" title="Buscar..."  name="motivo_baja">
                            @if($remolque->codMotivoBaja!=null)
                            <option selected value="{{$remolque->codMotivoBaja}}">{{$remolque->Motivo}}</option>
                            @else
                             <option selected></option>
                             @endif
                                @foreach ($motivos_baja as $motivo)
                                <option value="{{ $motivo->CodMotivobaja}}">{{ $motivo->motivobaja}}</option>
                                @endforeach
                            </select> </td>
                       <td><a class="btn btn-sm btn-success editar_remolque" href="#"><i class="fa fa-edit"></i> </a> <a class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a></td>
                       </tr>
                            @endforeach 

                            <tr>
                            <td><select class="selectpicker"  data-container="body" data-live-search="true" title="Buscar..." id="matricula_remolque" name="matricula_remolque">
                             <option selected"></option>
                                @foreach ($matriculas_remolque as $matricula)
                                <option>{{ $matricula->Matricula}}</option>
                                @endforeach
                            </select></td>
                       <td> <input type="text" class="form-control datepicker" name="alta_remolque" id="alta_remolque" value=""></td>
                       <td></td>
                       <td></td>
                       <td><a class="btn btn-success" id="add_remolque"><i class="fa fa-plus"></i></a></td>
                            </tr>
                        </tbody>
                    </table>
                     
                </div>
                    </div>

                </div>
            </div>
</div>

<script type="text/javascript">

$("#volver").click(function(){
        window.history.go(-1); return true;
    });


 $("#clonar_gesanc").click(function(){

    $form = $("#form_contrato").serialize();

    $CodAnexo = $("#CodAnexo").val();

    bootbox.confirm({
        title: "Clonar en Gesanc",
        message: "<i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> ¿Está seguro que desea ejecutar esta acción?</strong> Se creará en GESANC un contrato con los datos de este anexo y tantos remolques asociados al nuevo contrato como anexos de remolque existan. Además una ficha de contacto en GESANC con los datos del cliente.",
        buttons: {
            cancel: {
            label: '<i class="fa fa-times"></i> Cancelar'
        },
        confirm: {
            label: '<i class="fa fa-check"></i> Aceptar'
        }
        },
        callback: function (result) {
            if(result == true){
               

                setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('contratos/europa/clonar_gesanc')}}',
                        data : {'datos' : $("#form_contrato").serialize(),'CodAnexo':$CodAnexo},            
                        success : function(data){
                                    if(data.includes("SQLSTATE")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                //location.reload();
                            }
                        });
                    }else{
                           bootbox.alert({
                            message: "el anexo se ha creado correctamente",
                            callback: function () {
                                 window.location = '/contratos/gesanc/'+data;
                            }
                        });
                    }
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);
            }
        }
    });

});

 $("#clonar_its").click(function(){

    $form = $("#form_contrato").serialize();

    $CodAnexo = $("#CodAnexo").val();

    bootbox.confirm({
        title: "Clonar en ITS",
        message: "<i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> ¿Está seguro que desea ejecutar esta acción?</strong> Se creará en ITS un contrato con los datos del contrato actual y tantos anexos como incluya el contrato actual. Además una ficha de contacto en ITS con los datos del cliente, si aún no existe.",
        buttons: {
            cancel: {
            label: '<i class="fa fa-times"></i> Cancelar'
        },
        confirm: {
            label: '<i class="fa fa-check"></i> Aceptar'
        }
        },
        callback: function (result) {
            if(result == true){
               

                setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('contratos/europa/clonar_gesanc')}}',
                        data : {'datos' : $("#form_contrato").serialize(),'CodAnexo':$CodAnexo},            
                        success : function(data){
                                    if(data.includes("SQLSTATE")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                //location.reload();
                            }
                        });
                    }else{
                           bootbox.alert({
                            message: "el anexo se ha creado correctamente",
                            callback: function () {
                                 window.location = '/contratos/gesanc/'+data;
                            }
                        });
                    }
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);
            }
        }
    });

});

 $(document).on('focusin','.datepicker',function(){
         $(this).datepicker({
        format: "dd/mm/yyyy",
        dateFormat: 'yy-mm-dd',
        language: "es",
        autoclose: true
    });

         $(this).selectpicker("data-live-search","true");

    });

 $( "#esrenovacion" ).on( "click", function() {

    if($("#matricula option:selected").text()==""){
        bootbox.alert("Falta introducir la matricula");
        $(this).prop("checked",false);
    }
});


 $( "#aplicarprecio" ).on( "click", function() {

    $("#importe").prop("disabled",false);
    $("#importe").val("0");

});

  $("#matricula").change(function(){
    if($(this).find("option:selected").text()==""){
        $("#esrenovacion").prop("checked",false);
    }
  });

   $( "#add_remolque" ).on( "click", function() {

    $codAnexo = $("#CodAnexo").val();
    $matricula= $("#matricula_remolque option:selected").val();
    $alta_remolque = $("#alta_remolque").val();
    $contrato = $("#codContrato").val();

    setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('contratos/europa/create_anexo_remolque')}}',
                        data : {'matricula':$matricula,'alta_remolque':$alta_remolque,'CodAnexo':$codAnexo,'codContrato':$contrato},
                        success : function(data){

                                console.log(JSON.stringify(data));
                    if(data.includes("SQLSTATE")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                //location.reload();
                            }
                        });
                    }else{
                         bootbox.alert({
                            message: data,
                            callback: function () {
                                 location.reload();
                            }
                        });
                    }
                   
                    $("#loading").hide();

                         
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);
    

});

   $("#sustituir").click(function(){

     $codContrato = $("#codContrato").val();

      setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('contratos/europa/mostrar_sustitucion')}}',
                        data : {'codContrato':$codContrato},
                        success : function(data){
                           var dialog = bootbox.dialog({
                                    message: data,
                                    title: "Sustituir anexo",
                                    buttons: {
                                        success: {
                                            label: 'aceptar',
                                             callback: function() {
                                                $sustituido = $("#lista_anexo option:selected").val();
                                                $fechaSustitucion = $("#fecha_sustitucion").val();
                                                $notas = $("#notas_sustitucion").val();
                                                sustituir($sustituido,$fechaSustitucion,$notas);
                                                dialog.modal('hide');
                                                 }
                                        }
                                    }
                                });
                             $('.selectpicker').selectpicker('refresh');
                             $('.selectpicker').selectpicker('render');
                             
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);

});

  $("#sustituir_remolque").click(function(){

     $codAnexo = $("#CodAnexo").val();

      setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('contratos/europa/mostrar_sustitucion_remolque')}}',
                        data : {'CodAnexo':$codAnexo},
                        success : function(data){
                           var dialog = bootbox.dialog({
                                    message: data,
                                    title: "Sustituir remolque",
                                    buttons: {
                                        success: {
                                            label: 'aceptar',
                                             callback: function() {
                                                $sustituido = $("#remolque_sustituyente option:selected").val();
                                                $sustituyente = $("#remolque_sustituido option:selected").val();
                                                $fechaSustitucion = $("#fecha_sustitucion_remolque").val();
                                                $notas = $("#notas_sustitucion_remolte").val();
                                                sustituir_remolque($sustituido,$sustituyente,$fechaSustitucion,$notas);
                                                dialog.modal('hide');
                                                 }
                                        }
                                    }
                                });
                             $('.selectpicker').selectpicker('refresh');
                             $('.selectpicker').selectpicker('render');
                             
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);

});



 $("#tipo_anexo").change(function(){

    if($(this).find("option:selected").val()!=null){

         $CodTipoAnexo = $(this).find("option:selected").val();

        $codCondiciones = $("#condiciones").val();

         setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('contratos/europa/precio_anexo')}}',
                        data : {'CodTipoAnexo':$CodTipoAnexo,'codCondiciones':$codCondiciones},
                        success : function(data){
                            var dialog = bootbox.dialog({
                                message: '<p class="text-center">buscando importe, espere por favor</p>',
                                closeButton: false
                            });
                            // do something in the background
                            $("#importe").prop("disabled",false);
                            $("#importe").val(data);
                            dialog.modal('hide');
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);

    }else{
        $("#importe").prop("disabled");
         $("#importe").val("");
    }

    });

 $(".bt-edit").click(function(){
    
    $form = $("#form_contrato").serialize();

    $CodAnexo = $("#CodAnexo").val();
  
     setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/europa/update_anexo')}}',
                data : {'datos' : $("#form_contrato").serialize(),'CodAnexo':$CodAnexo},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    if(data.includes("SQLSTATE")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                //location.reload();
                            }
                        });
                    }else{
                         bootbox.alert({
                            message: "el anexo se ha modificado correctamente",
                            callback: function () {
                                 location.reload();
                            }
                        });
                    }
                   
                    $("#loading").hide();

                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);



});

  $(".editar_remolque").click(function(){


    
   $codAnexoRemolque = $(this).closest("tr").find('.codAnexoRemolque').val();
   $matricula = $(this).closest("tr").find('.matricula_remolque option:selected').val();
   $alta = $(this).closest("tr").find('.alta_remolque').val();
   $baja = $(this).closest("tr").find('.baja_remolque').val();
   $motivobaja = $(this).closest("tr").find('.motivo_baja option:selected').val();
  
     setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/europa/update_anexo_remolque')}}',
                data : {'codAnexoRemolque':$codAnexoRemolque,'matricula_remolque':$matricula,'alta':$alta,'baja':$baja,'motivobaja':$motivobaja},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    if(data.includes("SQLSTATE")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                //location.reload();
                            }
                        });
                    }else{
                         bootbox.alert({
                            message: "el remolque se ha modificado correctamente",
                            callback: function () {
                                 location.reload();
                            }
                        });
                    }
                   
                    $("#loading").hide();

                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

});

  //eliminar el registro
  $(document).on('click','.btn-danger',function(){

$codAnexoRemolque = $(this).closest("tr").find('.codAnexoRemolque').val();
    

bootbox.confirm({
     title: "Eliminar Registro",
    message: "<i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> ¿Está seguro que desea eliminar el registro?</strong> Tenga en cuenta que esta acción es irreversible.",
    buttons: {
        cancel: {
            label: '<i class="fa fa-times"></i> Cancelar'
        },
        confirm: {
            label: '<i class="fa fa-check"></i> Aceptar'
        }
    },
    callback: function (result) {

            if(result == true){
                
                setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('contratos/europa/eliminar_anexo_remolque')}}',
                        data : {'codAnexoRemolque':$codAnexoRemolque},            
                        success : function(data){
                            console.log(JSON.stringify(data));
                    if(data.includes("SQLSTATE")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                //location.reload();
                            }
                        });
                    }else{
                         bootbox.alert({
                            message: data,
                            callback: function () {
                                 location.reload();
                            }
                        });
                    }
                   
                    $("#loading").hide();
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);

    }

    }
});

});

  function sustituir(anexo,fecha,notas){

    $sustituyente = $("#CodAnexo").val();



      setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('contratos/europa/sustitucion')}}',
                        data : {'AnexoSustituido':anexo,'AnexoSustituyente':$sustituyente,'fechaSustitucion':fecha,'notas':notas},            
                        success : function(data){
                            console.log(JSON.stringify(data));
                    if(data.includes("SQLSTATE")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                //location.reload();
                            }
                        });
                    }else{
                         bootbox.alert({
                            message: data,
                            callback: function () {
                                 location.reload();
                            }
                        });
                    }
                   
                    $("#loading").hide();
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);

  }

  function sustituir_remolque(sustituido,sustituyente,fecha,notas){


      setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('contratos/europa/sustitucion')}}',
                        data : {'AnexoSustituido':sustituido,'AnexoSustituyente':sustituyente,'fechaSustitucion':fecha,'notas':notas},            
                        success : function(data){
                            console.log(JSON.stringify(data));
                    if(data.includes("SQLSTATE")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                //location.reload();
                            }
                        });
                    }else{
                         bootbox.alert({
                            message: data,
                            callback: function () {
                                 location.reload();
                            }
                        });
                    }
                   
                    $("#loading").hide();
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);

  }


</script>

@endsection