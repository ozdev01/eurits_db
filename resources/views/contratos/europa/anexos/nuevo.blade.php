@extends('layouts.app')
@section('content')

   
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="row"> 
                    <div class="panel-heading" id="sanciones-header">                    
                        <div class="col-md-12">
                            <h3 class="module-title">Añadir anexo</h3>
                        </div>
                        <div class="col-md-12">
                            <div>
                                <ul class="nav navbar-nav">
                                    <li><a href="#" class="bt-edit"><i class="fa fa-save"> </i> Guardar</a></li>
                                    <li><a href="#" id="volver"><i class="fa fa-arrow-left"> </i> Volver</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="panel-body" style="border-top:2px solid #335599;">                        
                    <form class="inline-form" id="form_contrato">
                        <div class="form-group col-md-4">
                            <label for="Beneficiario">Cliente</label>
                            <input type="text" class="form-control" name="cliente" id="cliente" value="{{$cliente->razonSocial}}" disabled>
                             <input type="hidden" class="form-control" name="codContratado" id="codContratado" value="{{$contrato->codContratado}}" >
                             <input type="hidden" class="form-control" name="condiciones" id="condiciones" value="{{$contrato->codCondiciones}}" >   
                              <input type="hidden" class="form-control" name="codComercial" id="codComercial" value="{{$contrato->codComercial}}" >           
                        </div>
                         <div class="form-group col-md-2">
                            <label for="Beneficiario">Contrato</label>
                            <input type="text" class="form-control" name="cliente" id="cliente" value="{{$contrato->codContrato}}" disabled>
                            <input type="hidden" class="form-control" name="codContrato" id="codContrato" value="{{$contrato->codContrato}}" >
                            <input type="hidden" class="form-control" name="codContratante" id="codContratante" value="{{$contrato->codContratante}}" >                
                        </div>
                         <div class="form-group col-md-2">
                            <label for="Beneficiario">Fecha Contrato</label>
                            <input type="text" class="form-control" name="cliente" id="cliente" value="{{ date('d/m/Y', strtotime($contrato->fechaContrato)) }}" disabled>           
                        </div>
                        <div class="form-group col-md-3">
                            <label for="empresa">Matricula</label>
                            <select class="selectpicker" data-live-search="true" title="Buscar..." id="matricula" name="matricula">
                             <option selected></option>
                                @foreach ($matriculas as $matricula)
                                <option>{{ $matricula->Matricula}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="codigo">Tipo de Anexo</label>
                            <select class="selectpicker" data-live-search="true" title="Buscar..." id="tipo_anexo" name="tipo_anexo">
                            <option selected></option>
                                @foreach ($tipos_anexo as $tipo)
                                <option value="{{ $tipo->CodTipoAnexo}}">{{ $tipo->tipoAnexo}}</option>
                                @endforeach
                            </select>
                        </div>
                         <div class="form-group col-md-2">
                            <label for="Beneficiario">Importe</label>
                            <input type="text" class="form-control" name="importe" id="importe" value="" disabled>
                        </div>
                            <div class="form-group col-md-2">
                            <label for="codigo">No aplicar precio</label>
                     
                            <a class="btn btn-danger" href="#" id="aplicarprecio" aria-label="Delete">
                              <i class="fa fa-times" aria-hidden="true"></i>
                            </a>
                         
                        </div>
                        <div class="form-group col-md-3">
                            <label for="matricula">Fecha Alta</label>
                            <input type="text" class="form-control datepicker" name="alta" id="alta" value="">                       
                        </div>
                         <div class="form-group col-md-6">
                            <label for="codigo">Notas</label>
                     
                            <input type="text" class="form-control" name="notas" id="notas" value="">
                         
                        </div>
                         <div class="form-group col-md-2">
                            <label for="codigo">¿Renovación?</label>
                     
                            <input type="checkbox"  name="esrenovacion" id="esrenovacion" value="">
                         
                        </div>
                         <div class="form-group col-md-2">
                            <label for="codigo">¿No renovar?</label>

                            <input type="checkbox" name="norenovar" id="norenovar" value="">
                         
                        </div>
                                 
                    </form>
                    </div>

                </div>
            </div>
</div>

<script type="text/javascript">

$("#volver").click(function(){
        window.history.go(-1); return true;
    });


 $(document).on('focusin','.datepicker',function(){
         $(this).datepicker({
        format: "dd/mm/yyyy",
        dateFormat: 'yy-mm-dd',
        language: "es",
        autoclose: true
    });

         $(this).selectpicker("data-live-search","true");

    });

 $( "#esrenovacion" ).on( "click", function() {

    if($("#matricula option:selected").text()==""){
        bootbox.alert("Falta introducir la matricula");
        $(this).prop("checked",false);
    }
});


 $( "#aplicarprecio" ).on( "click", function() {

    $("#importe").prop("disabled",false);
    $("#importe").val("0");

});

  $("#matricula").change(function(){
    if($(this).find("option:selected").text()==""){
        $("#esrenovacion").prop("checked",false);
    }
  });



 $("#tipo_anexo").change(function(){

    if($(this).find("option:selected").val()!=null){

         $CodTipoAnexo = $(this).find("option:selected").val();

        $codCondiciones = $("#condiciones").val();

         setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('contratos/europa/precio_anexo')}}',
                        data : {'CodTipoAnexo':$CodTipoAnexo,'codCondiciones':$codCondiciones},
                        success : function(data){
                            var dialog = bootbox.dialog({
                                message: '<p class="text-center">buscando importe, espere por favor</p>',
                                closeButton: false
                            });
                            // do something in the background
                            $("#importe").prop("disabled",false);
                            $("#importe").val(data);
                            dialog.modal('hide');
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);

    }else{
        $("#importe").prop("disabled");
         $("#importe").val("");
    }

    });

 $(".bt-edit").click(function(){
    
    $form = $("#form_contrato").serialize();
  
     setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/europa/store_anexo')}}',
                data : {'datos' : $("#form_contrato").serialize()},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    if(data.includes("SQLSTATE")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                //location.reload();
                            }
                        });
                    }else{
                         bootbox.alert({
                            message: "el anexo se ha creado correctamente",
                            callback: function () {
                                 window.location = '/contratos/europa/anexos/'+data;
                            }
                        });
                    }
                   
                    $("#loading").hide();

                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);



});


</script>

@endsection