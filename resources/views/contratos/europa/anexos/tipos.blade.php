@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="row"> 
                    <div class="panel-heading" id="sanciones-header">                    
                        <div class="col-md-12">
                            <h3 class="module-title">Editar los tipos de Anexo</h3>
                        </div>
                        <div class="col-md-12">
                            <div>
                                <ul class="nav navbar-nav">
                                        <li><a href="{{url('contratos/europa')}}" id="volver"><i class="fa fa-arrow-left"> </i> Volver a Contratos</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    </div>
                <div class="panel panel-default">
                     <div class="table-responsive" id="tabla_contenido">
                    <table class="table">
                        <thead class="table-header">                       
                            <th>Tipo de Anexo</th>
                            <th>Tipo de Contrato</th>
                             <th>Provision de Fondos</th>
                            <th></th>
                        </thead>
                        <tbody id="myTable">
                        @foreach ($tipos_anexo as $tipo)
                       <tr>
                       <td><input type="text" name="tipoAnexo" class="form-control tipoAnexo" value="{!!$tipo->tipoAnexo !!}" placeholder=""><input type="hidden" class="codtipoanexo" name="codtipoanexo" value="{!!$tipo->CodtipoAnexo !!}"></td>
                       <td><select name="tipoContrato" class="form-control selectpicker tipoContrato" data-container="body" data-live-search="true" title="Buscar...">
                       @if($tipo->codTipoContrato == "AJ")
                         <option value="AJ" selected>MULTAS</option>
                         <option value="AM">ASISTENCIA</option>
                        @else
                         <option value="AJ" >MULTAS</option>
                         <option value="AM" selected>ASISTENCIA</option>
                         @endif
                       </select></td>
                       @if($tipo->provisionFondos == 1)
                       <td><input type="checkbox" name="provisionFondos" value="" class="provisionFondos" checked></td>
                       @else
                       <td><input type="checkbox" name="provisionFondos" value="" class="provisionFondos"></td>
                       @endif
                       <td><a class="btn btn-sm btn-success editar" href="#"><i class="fa fa-edit"></i></a></td>
                       </tr>
                            @endforeach
                        <tr>
                        <td><input type="text" name="tipoAnexo" class="form-control tipoAnexo" value=""></td>
                        <td><select name="tipoContrato" class="form-control selectpicker tipoContrato" data-container="body" data-live-search="true" title="Buscar...">
                        
                         <option value="AJ" >MULTAS</option>
                         <option value="AM" >ASISTENCIA</option>
                       </select></td>
                         <td><input type="checkbox" name="provisionFondos" value="" class="provisionFondos"></td>
                          <td><a class="btn btn-success" id="guardar"><i class="fa fa-plus"></i></a></td>
                        </tr>
                        </tbody>
                    </table>
                     
                </div>
                    </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

 $(document).on('focusin','.datepicker',function(){
         $(this).datepicker({
        format: "dd/mm/yyyy",
        dateFormat: 'yy-mm-dd',
        language: "es",
        autoclose: true
    });

         $(this).selectpicker("data-live-search","true");

    });



  $(".editar").click(function(){

    $CodtipoAnexo = $(this).closest("tr").find('.codtipoanexo').val();

    $tipoAnexo = $(this).closest("tr").find('.tipoAnexo').val();

    $tipoContrato = $(this).closest("tr").find('.tipoContrato option:selected').val();

    $provisionfondos = 0;

    if($(this).closest("tr").find('.provisionFondos').prop("checked")==true){
      $provisionfondos = 1;
    }

    bootbox.confirm({
    title: "Editar el Tipo",
    message: "¿Desea cambiar el tipo de anexo? Tenga en cuenta que otros registros pueden verse afectados.",
    buttons: {
        cancel: {
            label: '<i class="fa fa-times"></i> Cancelar'
        },
        confirm: {
            label: '<i class="fa fa-check"></i> Aceptar'
        }
    },
    callback: function (result) {

      if(result){

         setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('contratos/europa/guardar_tipo_anexo')}}',
                        data : {'CodtipoAnexo':$CodtipoAnexo,'tipoAnexo':$tipoAnexo,'tipoContrato':$tipoContrato,'provisionfondos':$provisionfondos},
                        success : function(data){
                            if(data.includes("SQLSTATE")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                //location.reload();
                            }
                        });
                    }else{
                         bootbox.alert({
                            message: data,
                            callback: function () {
                                 location.reload();
                            }
                        });
                    }
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);
      }

    
        
    }
});






  });


 $("#guardar").click(function(){

    $tipoAnexo = $(this).closest("tr").find('.tipoAnexo').val();

    $tipoContrato = $(this).closest("tr").find('.tipoContrato option:selected').val();

    $provisionfondos = 0;

    if($(this).closest("tr").find('.provisionFondos').prop("checked")==true){
      $provisionfondos = 1;
    }

         setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('contratos/europa/store_tipo_anexo')}}',
                        data : {'tipoAnexo':$tipoAnexo,'tipoContrato':$tipoContrato,'provisionfondos':$provisionfondos},
                        success : function(data){
                            if(data.includes("SQLSTATE")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                //location.reload();
                            }
                        });
                    }else{
                         bootbox.alert({
                            message: data,
                            callback: function () {
                                 location.reload();
                            }
                        });
                    }
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);
      



  });


</script>

@endsection