@extends('layouts.app')
@section('content')

<div class="container">
    <div class="row">
        <div class="col-sm-2">
         
            <h4>Búsqueda</h4>           
            <!--<label for="search" id="loading" style="display:none;">
                <i class="fa fa-refresh fa-spin fa-1x fa-fw"></i>
            </label>-->
            <input type="text" class="form-control" id="search" name="search" placeholder="Buscar...">
             <h4>Filtros</h4>
                <div class =filtros>
                 <a data-toggle="collapse" href="#colcodcontacto" aria-expanded="false" aria-controls="colcodcontacto"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Codigo Contacto</h5></a>
                <div class="collapse" id="colcodcontacto">
                <input type="text" class="form-control" name="codContacto" id="codContacto" value="" placeholder=".">
                </div>
            </div>
               <div class =filtros>
                 <a data-toggle="collapse" href="#colfic" aria-expanded="false" aria-controls="colfic"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> CIF</h5></a>
                <div class="collapse" id="colfic">
               <select class="selectpicker" data-live-search="true" title="Buscar..." id="cif" name="cif">
                 <option selected></option>
                    @foreach ($empresas as $empresa)
                    <option value="{{$empresa->CodContacto}}">{{ $empresa->razonSocial}}</option>
                    @endforeach
                </select>
                </div>
            </div>
                 <div class =filtros>
                 <a data-toggle="collapse" href="#colempresa" aria-expanded="false" aria-controls="colempresa"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Empresa</h5></a>
                <div class="collapse" id="colempresa">
               <select class="selectpicker" data-live-search="true" title="Buscar..." id="empresa" name="empresa">
                 <option selected></option>
                    @foreach ($empresas as $empresa)
                    <option value="{{$empresa->CodContacto}}">{{ $empresa->razonSocial}}</option>
                    @endforeach
                </select>
                </div>
            </div>
              <div class =filtros>
                 <a data-toggle="collapse" href="#coldireccion" aria-expanded="false" aria-controls="coldireccion"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Dirección</h5></a>
                <div class="collapse" id="coldireccion">
                <input type="text" class="form-control" name="direccion" id="direccion" value="" placeholder="">
                </div>
            </div>
                <div class =filtros>
                 <a data-toggle="collapse" href="#colcodpostal" aria-expanded="false" aria-controls="colcodpostal"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Código Postal</h5></a>
                <div class="collapse" id="colcodpostal">
                <input type="text" class="form-control" name="codpostal" id="codpostal" value="" placeholder="">
                </div>
            </div>
                <div class =filtros>
                 <a data-toggle="collapse" href="#colapartado" aria-expanded="false" aria-controls="colapartado"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Apdo. Correos</h5></a>
                <div class="collapse" id="colapartado">
                <input type="text" class="form-control" name="aptocorreos" id="aptocorreos" value="" placeholder="">
                </div>
            </div>
            <div class =filtros>
                 <a data-toggle="collapse" href="#colpoblacion" aria-expanded="false" aria-controls="colpoblacion"><h5 class="titulo_filtro"><i class="fa fa-plus"></i>Poblacion</h5></a>
                <div class="collapse" id="colpoblacion">
                <input type="text" class="form-control" name="poblacion" id="poblacion" value="" placeholder="">
                </div>
            </div>

              <div class =filtros>
                 <a data-toggle="collapse" href="#colprovincia" aria-expanded="false" aria-controls="colprovincia"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Provincia</h5></a>
                <div class="collapse" id="colprovincia">
               <select class="selectpicker" data-live-search="true" title="Buscar..." id="provincia" name="provincia">
                 <option selected></option>
                    @foreach ($provincias as $provincia)
                    <option value="{{$provincia->codProvincia}}">{{ $provincia->Provincia}}</option>
                    @endforeach
                </select>
                </div>
            </div>
                <div class =filtros>
                 <a data-toggle="collapse" href="#coltelefono" aria-expanded="false" aria-controls="coltelefono"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Teléfono</h5></a>
                <div class="collapse" id="coltelefono">
                <input type="text" class="form-control" name="telefono" id="telefono" value="" placeholder="">
                </div>
            </div>
                <div class =filtros>
                 <a data-toggle="collapse" href="#colemail" aria-expanded="false" aria-controls="colemail"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Email</h5></a>
                <div class="collapse" id="colemail">
                <input type="text" class="form-control" name="email" id="email" value="" placeholder="">
                </div>
            </div>
               <div class =filtros>
                 <a data-toggle="collapse" href="#colpais" aria-expanded="false" aria-controls="colpais"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> País</h5></a>
                <div class="collapse" id="colpais">
               <select class="selectpicker" data-live-search="true" title="Buscar..." id="pais" name="pais">
                 <option selected></option>
                    @foreach ($paises as $pais)
                    <option value="{{$pais->codPais}}">{{ $pais->Pais}}</option>
                    @endforeach
                </select>
                </div>
            </div>
                <div class =filtros>
                 <a data-toggle="collapse" href="#coltipo" aria-expanded="false" aria-controls="coltipo"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Tipo Contacto</h5></a>
                <div class="collapse" id="coltipo">
               <select class="selectpicker" data-live-search="true" title="Buscar..." id="tipoContacto" name="tipoContacto">
                 <option selected></option>
                    @foreach ($funciones as $funcion)
                    <option value="{{$funcion->CodTipoFuncion}}">{{ $funcion->Funcion}}</option>
                    @endforeach
                </select>
                </div>
            </div>
              <div class="filtros">
                <a data-toggle="collapse" href="#colcomercial" aria-expanded="false" aria-controls="colcomercial"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Nombre Comercial</h5></a>
                 <div class="collapse" id="colcomercial">
                  <select class="selectpicker" data-live-search="true" title="Buscar..." id="comercial" name="comercial">
                 <option selected></option>
                    @foreach ($comerciales as $comercial)
                    <option value="{{$comercial->ID}}">{{ $comercial->comercial}}</option>
                    @endforeach
                </select>
                </div>
            </div>
                <div class =filtros>
                 <a data-toggle="collapse" href="#colcuenta" aria-expanded="false" aria-controls="colcuenta"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Cuenta bancaria</h5></a>
                <div class="collapse" id="colcuenta">
                <input type="text" class="form-control" name="cuenctabancaria" id="cuentabancaria" value="" placeholder="">
                </div>
            </div>
            <div class =filtros>
                 <a data-toggle="collapse" href="#colcliotro" aria-expanded="false" aria-controls="colcliotro"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Cliente Otro comercial</h5></a>
                <div class="collapse" id="colcliotro">
                <input type="checkbox" name="otrocomercial" value="" id="otrocomercial">
                </div>
            </div>
              <div class =filtros>
                 <a data-toggle="collapse" href="#colprovision" aria-expanded="false" aria-controls="colprovision"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Sin prov. de fondos</h5></a>
                <div class="collapse" id="colprovision">
                <input type="checkbox" name="sinprovision" value="" id="sinprovision">
                </div>
            </div>
            <div class =filtros>
                 <a data-toggle="collapse" href="#colporemail" aria-expanded="false" aria-controls="colporemail"><h5 class="titulo_filtro"><i class="fa fa-plus"></i>Envíos por email</h5></a>
                <div class="collapse" id="colporemail">
                <input type="checkbox" name="poremail" value="" id="poremail">
                </div>
            </div>
            <div class =filtros>
                 <a data-toggle="collapse" href="#colconflictivo" aria-expanded="false" aria-controls="colconflictivo"><h5 class="titulo_filtro"><i class="fa fa-plus"></i>Cliente Conflictivo</h5></a>
                <div class="collapse" id="colconflictivo">
                <input type="checkbox" name="conflictivo" value="" id="conflictivo">
                </div>
            </div>
            <div class =filtros>
                 <a data-toggle="collapse" href="#colimpagado" aria-expanded="false" aria-controls="colimpagado"><h5 class="titulo_filtro"><i class="fa fa-plus"></i>Impagado</h5></a>
                <div class="collapse" id="colimpagado">
                <input type="checkbox" name="impagado" value="" id="impagado">
                </div>
            </div>
            <div class="filtros">
            <button type="button" id="filtrar" class="btn btn-default btn-primary">filtrar</button>
            </div>
            </div>
        <div class="col-sm-10">
            <div class="panel panel-default">
                <div class="row"> 
                    <div class="panel-heading" id="sanciones-header">                    
                        <div class="col-md-2" style="width: 100%">
                            <h3 class="module-title">Contactos EUROPA</h3>
                        </div>
                        <div class="col-md-10">
                            <div>
                                <ul class="list-inline nav navbar-nav">
                                    <li><a href="{{url('contratos/europa/contactos/nuevo')}}" id=""><i class="fa fa-plus"> </i> Añadir</a></li>
                                    <li><a href="#" id="excel"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Excel</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table-responsive"">
                    <table class="table">
                        <thead class="table-header">
                            <th>Cliente</th>                        
                            <th>Impago</th>
                            <th>Dirección</th>
                            <th>Población</th>
                            <th>CP</th>
                            <th>Provincia</th>
                            <th></th>
                        </thead>
                        <tbody id="myTable">
                        @foreach ($contactos as $contacto)
                        <tr>
                            <td>{{ $contacto->razonSocial }} </td>
                            @if($contacto->impagado_pdteDev == 0)
                            <td>NO</td>
                            @else
                            <td>SI</td>
                            @endif
                            <td>{{ $contacto->direccion }} </td>                            
                            <td>{{ $contacto->Poblacion }} </td>
                            <td>{{ $contacto->codPostal }} </td>
                            <td>{{ $contacto->Provincia }}</td>
                            <td><a class="btn btn-sm btn-success" href={!! url('contratos/europa/contactos/'.$contacto->CodContacto) !!}#"><i class="fa fa-edit"></i></a></td> 
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    
                </div>

                 <div class="col-md-12 text-center">
      <ul class="pagination pagination-lg pager" id="myPager"></ul>
      </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

 $('.datepicker').datepicker({
        format: "dd/mm/yyyy",
        dateFormat: 'yy-mm-dd',
        language: "es",
        autoclose: true
    });
    
    $('#search').on('keyup',function(){
        $value=$(this).val();
        //alert($value);
        $("#loading").show();
        setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/its/contratos/search_its')}}',
                data : {'search':$value},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('tbody').html(data);
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);
});

$( "#filtrar" ).click(function() {



$codContacto = $("#codContacto").val();
$cif = $("#cif").val();
$empresa =  $("#empresa option:selected").val();
$pais =  $("#pais option:selected").val();
$provincia =  $("#provincia option:selected").val();
$direccion = $("#direccion").val();
$codpostal = $("#codpostal").val();
$aptocorreos = $("#aptocorreos").val();
$poblacion = $("#poblacion").val();
$telefono = $("#telefono").val();
$email = $("#email").val();
$tipoContacto = $("#tipoContacto option:selected").val();
$comercial = $("#comercial option:selected").val();
$cuentabancaria = $("#cuentabancaria").val();

$otrocomercial = 0;
$sinprovision = 0;
$poremail = 0;
$conflictivo = 0;
$impagado = 0;

if($("#otrocomercial").prop("checked")){
$otrocomercial = 1;
}
if($("#sinprovision").prop("checked")){
$sinprovision = 1;
}
if($("#poremail").prop("checked")){
$poremail = 1;
}
if($("#conflictivo").prop("checked")){
$conflictivo = 1;
}
if($("#impagado").prop("checked")){
$impagado = 1;
}


//alert($inicio);

        $dialog = bootbox.dialog({
                        message: '<p class="text-center">Cargando datos, espere por favor...</p>',
                        closeButton: false
                    });

    setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/europa/contactos/filtra_europa')}}',
                data : {'codContacto':$codContacto,'cif':$cif,'empresa':$empresa,'direccion':$direccion,'codpostal':$codpostal,'aptocorreos':$aptocorreos,'poblacion':$poblacion,'telefono':$telefono,'email':$email,'tipoContacto':$tipoContacto,'comercial':$comercial,'cuentabancaria':$cuentabancaria,'otrocomercial':$otrocomercial,'sinprovision':$sinprovision,'poremail':$poremail,'pais':$pais,'conflictivo':$conflictivo,'impagado':$impagado,'provincia':$provincia},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('tbody').html(data);

                    // do something in the background
                    $dialog.modal('hide');
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

});

$( "#excel" ).click(function() {



$codContacto = $("#codContacto").val();
$cif = $("#cif").val();
$empresa =  $("#empresa option:selected").val();
$pais =  $("#pais option:selected").val();
$provincia =  $("#provincia option:selected").val();
$direccion = $("#direccion").val();
$codpostal = $("#codpostal").val();
$aptocorreos = $("#aptocorreos").val();
$poblacion = $("#poblacion").val();
$telefono = $("#telefono").val();
$email = $("#email").val();
$tipoContacto = $("#tipoContacto option:selected").val();
$comercial = $("#comercial option:selected").val();
$cuentabancaria = $("#cuentabancaria").val();

$otrocomercial = 0;
$sinprovision = 0;
$poremail = 0;
$conflictivo = 0;
$impagado = 0;

if($("#otrocomercial").prop("checked")){
$otrocomercial = 1;
}
if($("#sinprovision").prop("checked")){
$sinprovision = 1;
}
if($("#poremail").prop("checked")){
$poremail = 1;
}
if($("#conflictivo").prop("checked")){
$conflictivo = 1;
}
if($("#impagado").prop("checked")){
$impagado = 1;
}


//alert($inicio);

        $dialog = bootbox.dialog({
                        message: '<p class="text-center">Generando su Excel, espere por favor...</p>',
                        closeButton: false
                    });

    setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/europa/contactos/generar_excel_contactos')}}',
                data : {'codContacto':$codContacto,'cif':$cif,'empresa':$empresa,'direccion':$direccion,'codpostal':$codpostal,'aptocorreos':$aptocorreos,'poblacion':$poblacion,'telefono':$telefono,'email':$email,'tipoContacto':$tipoContacto,'comercial':$comercial,'cuentabancaria':$cuentabancaria,'otrocomercial':$otrocomercial,'sinprovision':$sinprovision,'poremail':$poremail,'pais':$pais,'conflictivo':$conflictivo,'impagado':$impagado,'provincia':$provincia},            
                success : function(data){
                    window.location = this.url;
                    $dialog.modal('hide');
                },
                error : function(data){
                    bootbox.alert("Ha ocurrido un error generando su Excel.");
                    $dialog.modal('hide');
                }
            });
        }, 500);

});

$("#contactos").click(function(){
$container = $('#collapseempresa').clone();
$container.find('select').attr('id', 'selectempresa');

 setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/contactos')}}',            
                success : function(data){
                    $('.selectpicker').selectpicker('refresh');
                    $('.selectpicker').selectpicker('render');
                    $('.table-responsive').html(data);

                    // do something in the background
                    $dialog.modal('hide');
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);


});
</script>
@endsection