@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-xl-12 col-md-offset-0">
            <div class="panel panel-default">
                <div class="row"> 
                    <div class="panel-heading" id="sanciones-header">                    
                        <div class="col-md-8">
                        <h3 class="module-title">{{$contacto->razonSocial}}</h3>
                         @if($contacto->sinProvisionFondos == 1)
                          - Sin Provisión 
                           @endif
                             @if($contacto->impagado_pdteDev == 1)
                          - Impago Multas 
                           @endif
                           @if($contacto->cteConflictivo == 1)
                          - Conflictivo
                           @endif
                        </div>
                        <div class="col-md-4">
                            <div>
                                <ul class="nav navbar-nav">
                                    <li><a href="#" id="btn-save"><i class="fa fa-save"> </i> Guardar</a></li>                                 
                                    <li><a href="#" id="volver"><i class="fa fa-arrow-left"> </i> Volver</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="panel-body" style="border-top:2px solid #335599;">                        
                    <form class="inline-form" id="form_sancion">
                      <div class="form-group col-md-2">
                           <label for="matricula">Cod. Contacto</label>
                            <input type="text" class="form-control" name="codigo" value="{{$contacto->CodContacto}}" disabled>
                            <input type="hidden" class="form-control" name="codContacto" value="{{$contacto->CodContacto}}">
                                     
                        </div>
                         <div class="form-group col-md-2">
                            <label for="matricula">CIF</label>
                    
                           <input type="text" class="form-control" name="cif" value="{{$contacto->cif}}">
                                    
                        </div>
                        <div class="form-group col-md-3">
                           <label for="matricula">Empresa</label>
                            <input type="text" class="form-control" name="empresa" value="{{$contacto->razonSocial}}">
                                     
                        </div>
                           <div class="form-group col-md-3">
                           <label for="matricula">Apellidos</label>
                            <input type="text" class="form-control" name="apellidos" value="{{$contacto->apellidos}}">
                                     
                        </div>
                          <div class="form-group col-md-2">
                           <label for="matricula">Siglas</label>
                            <input type="text" class="form-control" name="siglas" value="{{$contacto->siglas}}">
                                     
                        </div>
                         <div class="form-group col-md-4">
                  
                            <label for="acuerdo">Dirección</label>
                            <input type="text" class="form-control" name="direccion" value="{{$contacto->direccion}}">   
                                               
                        </div>
                         <div class="form-group col-md-2">
                         
                            <label for="ft">CP</label>
                            <input type="text" class="form-control" name="cp" value="{{$contacto->codPostal}}">      
                                               
                        </div>
                          <div class="form-group col-md-2">
                         
                            <label for="ft">apdo. Correo</label>
                            <input type="text" class="form-control" name="apartadoCorreos" value="{{$contacto->apartadoCorreos}}">      
                                               
                        </div>


                      <div class="form-group col-md-2">
                     
                            <label for="matricula">Población</label>
                           <input type="text" class="form-control" name="poblacion" value="{{$contacto->Poblacion}}">  
                         
                        </div>
                         <div class="form-group col-md-2">
                            <label for="matricula">País</label>
                            <select class="selectpicker" data-live-search="true" title="Buscar..." id="pais" name="pais">
                                @foreach ($paises as $dato)
                                @if($contacto->codPais==$dato->codPais)
                                <option value="{{$dato->codPais}}" selected>{{ $dato->Pais}}</option>
                                @else
                                <option value="{{$dato->codPais}}">{{ $dato->Pais}}</option>
                                @endif
                                @endforeach
                            </select> 
                        </div>
                            <div class="form-group col-md-2">
                            <label for="matricula">Provincia</label>
                            <select class="selectpicker" data-live-search="true" title="Buscar..." id="provincia" name="provincia">
                                @foreach ($provincias as $dato)
                                @if($contacto->codProvincia==$dato->codProvincia)
                                <option value="{{$dato->codProvincia}}" selected>{{ $dato->Provincia}}</option>
                                @else
                                <option value="{{$dato->codProvincia}}" >{{ $dato->Provincia}}</option>
                                @endif
                                @endforeach
                            </select> 
                        </div>
                        <div class="form-group col-md-3">
                     
                            <label for="matricula">Web</label>
                            <input type="text" class="form-control" name="web" value="{{$contacto->web}}">  
                                                     
                        </div>
                           <div class="form-group col-md-3">
                      
                                <label for="matricula">Email</label>
                                <input type="text" class="form-control" name="email" value="{{$contacto->email}}">  
                                          
                        </div>
                           <div class="form-group col-md-4">
                      
                                <label for="matricula">CC</label>
                                <input type="text" class="form-control" name="cc" value="{{$contacto->CC}}">        
                                          
                        </div>
                            <div class="form-group col-md-6">
                            
                            <label for="matricula">Observaciones</label>
                            <textarea type="textarea" class="form-control" name="observaciones" style="" >{{$contacto->observaciones}}</textarea> 
                               
                            </div> 
                             <div class="form-group col-md-6">
                           <ul class="list-inline">
                         @if($contacto->cteDeOtroComercial == 1)
                          <li><label for="matricula">Otro comercial</label> <input type="checkbox" name="cteDeOtroComercial" value="" checked></li>
                          @else
                           <li><label for="matricula">Otro comercial</label> <input type="checkbox" name="cteDeOtroComercial" value=""></li>
                           @endif
                             @if($contacto->sinProvisionFondos == 1)
                          <li><label for="matricula">Sin provisión</label> <input type="checkbox" name="sinProvisionFondos" value="" checked></li>
                          @else
                           <li><label for="matricula">Sin provisión</label> <input type="checkbox" name="sinProvisionFondos" value=""></li>
                           @endif
                             @if($contacto->impagado_pdteDev == 1)
                          <li><label for="matricula">Impago</label> <input type="checkbox" name="impagado_pdteDev" value="" checked></li>
                          @else
                           <li><label for="matricula">Impago</label> <input type="checkbox" name="impagado_pdteDev" value=""></li>
                           @endif
                           @if($contacto->cteConflictivo == 1)
                          <li><label for="matricula">Conflictivo</label> <input type="checkbox" name="cteConflictivo" value="" checked></li>
                          @else
                           <li><label for="matricula">Conflictivo</label> <input type="checkbox" name="cteConflictivo" value=""></li>
                           @endif
                               @if($contacto->mailing == 1)
                          <li><label for="matricula">Mailing</label> <input type="checkbox" name="mailing" value="" checked></li>
                          @else
                           <li><label for="matricula">Mailing</label> <input type="checkbox" name="mailing" value=""></li>
                           @endif
                        </ul>
                            </div>
                                                 
                        </div>     
                    </form>
                    </div>
                       <div class="panel panel-default">
              <div class="row"> 
                    <div class="panel-heading" id="detalles-header">                    
                        <div class="col-md-6">
                            <h3 class="module-title">Detalles</h3>
                        </div>
                        <div class="col-md-6">
                            <div>
                                <ul class="nav navbar-nav">
                                    <li class="detalles"><a id="telefono"><i class="fa fa-phone" aria-hidden="true"></i> Telefonos</a></li>
                                    <li class="detalles"><a id= "direcciones"><i class="fa fa-car" aria-hidden="true"></i> Direcciones </a></li>
                                    <li class="detalles"><a id="funciones"><i class="fa fa-info-circle" aria-hidden="true"></i> Funciones</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div id="error" style="display:none" class="alert alert-danger alert-dismissible fade in" role="alert"></div>
                    <div id="success" style="display:none" class="alert alert-success alert-dismissible"></div>
                     <div class="table-responsive" id="tabla_contenido">
                </div>
                    </div>                        
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

$(function() {

  $codContacto =  $("input[name=codContacto]").val();
  setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/europa/contactos/telefono')}}',
                data : {'codContacto':$codContacto},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('#tabla_contenido').html(data);
                    $('.selectpicker').selectpicker('refresh');
                    $('.selectpicker').selectpicker('render');
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);
    });

$("#telefono").click(function(){

         var dialog = bootbox.dialog({
    message: '<p class="text-center">Cargando datos...</p>',
    closeButton: false
});

        $("#error").hide();
    $("#success").hide();
    $("#success").empty();
 
  $codContacto =  $("input[name=codContacto]").val();
  setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/europa/contactos/telefono')}}',
                data : {'codContacto':$codContacto},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('#tabla_contenido').html(data);
                    $('.selectpicker').selectpicker('refresh');
                    $('.selectpicker').selectpicker('render');
                    $("#loading").hide();
                     dialog.modal('hide');
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                     dialog.modal('hide');
                }
            });
        }, 500);
    });

$("#direcciones").click(function(){

         var dialog = bootbox.dialog({
    message: '<p class="text-center">Cargando datos...</p>',
    closeButton: false
});

        $("#error").hide();
    $("#success").hide();
    $("#success").empty();
 
    $codContacto =  $("input[name=codContacto]").val();
  setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/europa/contactos/direccion')}}',
                data : {'codContacto':$codContacto},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('#tabla_contenido').html(data);
                    $('.selectpicker').selectpicker('refresh');
                    $('.selectpicker').selectpicker('render');
                    $("#loading").hide();
                     dialog.modal('hide');
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                     dialog.modal('hide');
                }
            });
        }, 500);
    });

$("#funciones").click(function(){

         var dialog = bootbox.dialog({
    message: '<p class="text-center">Cargando datos...</p>',
    closeButton: false
});

        $("#error").hide();
    $("#success").hide();
    $("#success").empty();
 
    $codContacto =  $("input[name=codContacto]").val();
  setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/europa/contactos/funciones')}}',
                data : {'codContacto':$codContacto},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('#tabla_contenido').html(data);
                    $('.selectpicker').selectpicker('refresh');
                    $('.selectpicker').selectpicker('render');
                    $("#loading").hide();
                     dialog.modal('hide');
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                     dialog.modal('hide');
                }
            });
        }, 500);
    });



    $("#volver").click(function(){
        window.history.go(-1); return false;
    });
     $(document).on('focusin','.datepicker',function(){
         $(this).datepicker({
        format: "dd/mm/yyyy",
        dateFormat: 'yy-mm-dd',
        language: "es",
        autoclose: true
    });

         $(this).selectpicker("data-live-search","true");

    });


 $("#btn-save").click(function(){
    
    $form = $("#form_sancion").serialize();

    $id =  $("input[name=codContacto]").val();
  
     setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '/contratos/europa/contactos/'+$id+'/guardar',
                data : {'datos' : $("#form_sancion").serialize()},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    if(data.includes("error")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                //location.reload();
                            }
                        });
                    }else{
                         bootbox.alert({
                            message: data,
                            callback: function () {
                                 location.reload();
                            }
                        });
                    }
                   
                    $("#loading").hide();

                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);



});

  $(document).on('click', '.editar_telefono' ,function(){
    $action = 1;
$CodContacto =  $("input[name=codContacto]").val();
$telefono = $(this).closest("tr").find('.telefono').val();
$antiguo = $(this).closest("tr").find('.antiguo').val();
$tipo = $(this).closest("tr").find('.tipo option:selected').val();

if($tipo == "buscar..." || $telefono == ""){
    $("#error").empty();
    $("#error").append("<i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Se han encontrado errores.</strong> El campo del tipo o del teléfono no puede estar vacío.");
    $("#error").show();
}else{
    $("#error").hide();
    $("#success").hide();
    $("#success").empty();

     setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/europa/contactos/guardar_europa')}}',
                data : {'action':$action,'CodContacto':$CodContacto,'telefono':$telefono,'tipo':$tipo,'antiguo':$antiguo},
                success : function(data){
                    console.log(JSON.stringify(data));
                    $("#success").append(data);
                    $("#success").show();
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

     $("#telefono")[0].click();
}
  });


$(document).on('click', '.eliminar_telefono' ,function(){
    $action = 1;
$CodContacto =  $("input[name=codContacto]").val();
$telefono = $(this).closest("tr").find('.telefono').val();
$tipo = $(this).closest("tr").find('.tipo option:selected').val();

bootbox.confirm({
    title: "Eliminar registro",
    message: "¿Está seguro que desea eliminar el registro?. Tenga en cuenta que el proceso es irreversible",
    buttons: {
        cancel: {
            label: '<i class="fa fa-times"></i> Cancelar'
        },
        confirm: {
            label: '<i class="fa fa-check"></i> Aceptar'
        }
    },
    callback: function (result) {
        
        if(result){

           var dialog = bootbox.dialog({
    message: '<p class="text-center">Eliminando...</p>',
    closeButton: false
});

           setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/europa/contactos/eliminar_europa')}}',
                data : {'action':$action,'CodContacto':$CodContacto,'telefono':$telefono,'tipo':$tipo},
                success : function(data){
                    console.log(JSON.stringify(data));
                    $("#success").append(data);
                    $("#success").show();
                    $("#loading").hide();
                    dialog.modal('hide');
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                    dialog.modal('hide');
                }
            });
        }, 500);

     $("#telefono")[0].click();

        }
    }
});


 


  });

$(document).on('click', '.eliminar_direccion' ,function(){

$action = 2;
$CodContacto =  $("input[name=codContacto]").val();
$orden = $(this).closest("tr").find('.orden').val();

bootbox.confirm({
    title: "Eliminar registro",
    message: "¿Está seguro que desea eliminar el registro?. Tenga en cuenta que el proceso es irreversible.",
    buttons: {
        cancel: {
            label: '<i class="fa fa-times"></i> Cancelar'
        },
        confirm: {
            label: '<i class="fa fa-check"></i> Aceptar'
        }
    },
    callback: function (result) {
        
        if(result){

           var dialog = bootbox.dialog({
    message: '<p class="text-center">Eliminando...</p>',
    closeButton: false
});
         setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/europa/contactos/eliminar_europa')}}',
                data : {'action':$action,'CodContacto':$CodContacto,'orden':$orden},
                success : function(data){
                    console.log(JSON.stringify(data));
                    $("#success").append(data);
                    $("#success").show();
                    $("#loading").hide();
                    dialog.modal('hide');
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                    dialog.modal('hide');
                }
            });
        }, 500);

     $("#direcciones")[0].click();

        }
    }
  });

});


 $(document).on('click', '.editar_direccion' ,function(){

$action = 2;
$CodContacto =  $("input[name=codContacto]").val();
$direccion = $(this).closest("tr").find('.direccion').val();
$poblacion = $(this).closest("tr").find('.poblacion').val();
$orden = $(this).closest("tr").find('.orden').val();
$cp = $(this).closest("tr").find('.codPostal').val();
$pais = $(this).closest("tr").find('.pais option:selected').val();
$provincia = $(this).closest("tr").find('.provincia option:selected').val();
$apartadoCorreos =$(this).closest("tr").find('.apartadoCorreos').val();
$facturarA = $(this).closest("tr").find('.contacto option:selected').val();
$activar = 0;

if($(this).closest("tr").find('.facturar').prop("checked")){
$activar = 1;
}


if($pais == "buscar..." || $cp == ""){
    $("#error").empty();
    $("#error").append("<i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Se han encontrado errores.</strong> El campo del pais o del Código Postal no puede estar vacío.");
    $("#error").show();
}else{
    $("#error").hide();
    $("#success").hide();
    $("#success").empty();
     var dialog = bootbox.dialog({
    message: '<p class="text-center">Editando...</p>',
    closeButton: false
});

     setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/europa/contactos/guardar_europa')}}',
                data : {'action':$action,'CodContacto':$CodContacto,'direccion':$direccion,'poblacion':$poblacion,'cp':$cp,'pais':$pais,'provincia':$provincia,'apartadoCorreos':$apartadoCorreos,'facturarA':$facturarA,'activar':$activar,'orden':$orden},
                success : function(data){
                    console.log(JSON.stringify(data));
                    $("#success").append(data);
                    $("#success").show();
                    $("#loading").hide();
                    dialog.modal('hide');
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                    dialog.modal('hide');
                }
            });
        }, 500);

     $("#direcciones")[0].click();
}
});




 $(document).on('click', '#add_telefono' ,function(){

$action = 1;
$CodContacto =  $("input[name=codContacto]").val();
$telefono = $(this).closest("tr").find('.telefono').val();
$tipo = $(this).closest("tr").find('.tipo option:selected').val();

if($tipo == "buscar..." || $telefono == ""){
    $("#error").empty();
    $("#error").append("<i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Se han encontrado errores.</strong> El campo del tipo o del teléfono no puede estar vacío.");
    $("#error").show();
}else{
    $("#error").hide();
    $("#success").hide();
    $("#success").empty();

    var dialog = bootbox.dialog({
    message: '<p class="text-center">Añadiendo teléfono</p>',
    closeButton: false
});

     setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/europa/contactos/insertar_europa')}}',
                data : {'action':$action,'CodContacto':$CodContacto,'telefono':$telefono,'tipo':$tipo},
                success : function(data){
                    console.log(JSON.stringify(data));
                    $("#success").append(data);
                    $("#success").show();
                    $("#loading").hide();
                    dialog.modal('hide');
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                     $("#error").append("<i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong>Error de base de datos</strong> Intentelo dentro de unos minutos.");
                    dialog.modal('hide');
                }
            });
        }, 500);

     $("#telefono")[0].click();
}
});

  $(document).on('click', '#guardar_direccion' ,function(){

$action = 2;
$CodContacto =  $("input[name=codContacto]").val();
$direccion = $(this).closest("tr").find('.direccion').val();
$poblacion = $(this).closest("tr").find('.poblacion').val();
$cp = $(this).closest("tr").find('.codPostal').val();
$pais = $(this).closest("tr").find('.pais option:selected').val();
$provincia = $(this).closest("tr").find('.provincia option:selected').val();
$apartadoCorreos =$(this).closest("tr").find('.apartadoCorreos').val();
$facturarA = $(this).closest("tr").find('.contacto option:selected').val();
$activar = 0;

if($(this).closest("tr").find('.facturar').prop("checked")){
$activar = 1;
}


if($pais == "buscar..." || $cp == ""){
    $("#error").empty();
    $("#error").append("<i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Se han encontrado errores.</strong> El campo del pais o del Código Postal no puede estar vacío.");
    $("#error").show();
}else{
    $("#error").hide();
    $("#success").hide();
    $("#success").empty();

    var dialog = bootbox.dialog({
    message: '<p class="text-center">Añadiendo dirección</p>',
    closeButton: false
});

     setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/europa/contactos/insertar_europa')}}',
                data : {'action':$action,'CodContacto':$CodContacto,'direccion':$direccion,'poblacion':$poblacion,'cp':$cp,'pais':$pais,'provincia':$provincia,'apartadoCorreos':$apartadoCorreos,'facturarA':$facturarA,'activar':$activar},
                success : function(data){
                    console.log(JSON.stringify(data));
                    $("#success").append(data);
                    $("#success").show();
                    $("#loading").hide();
                      dialog.modal('hide');
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                      dialog.modal('hide');
                }
            });
        }, 500);

     $("#direcciones")[0].click();
}
});


  $(document).on('click', '#add_vinculacion' ,function(){

$action = 3;
$CodContacto =  $("input[name=codContacto]").val();
$categoria = $(this).closest("tr").find('.categoria option:selected').val();
$vinculacion = $(this).closest("tr").find('.vinculacion option:selected').val();

if($categoria == "buscar..." || $vinculacion == "buscar..."){
    $("#error").empty();
    $("#error").append("<i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Se han encontrado errores.</strong> El campo de categoria o vinculación no pueden estar vacíos.");
    $("#error").show();
}else{
    $("#error").hide();
    $("#success").hide();
    $("#success").empty();

     var dialog = bootbox.dialog({
    message: '<p class="text-center">Añadiendo función</p>',
    closeButton: false
});

     setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/europa/contactos/insertar_europa')}}',
                data : {'action':$action,'CodContacto':$CodContacto,'categoria':$categoria,'vinculacion':$vinculacion},
                success : function(data){
                    console.log(JSON.stringify(data));
                    $("#success").append(data);
                    $("#success").show();
                    $("#loading").hide();
                    dialog.modal('hide');
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                    dialog.modal('hide');
                }
            });
        }, 500);

     $("#funciones")[0].click();
}
});
</script>
@endsection