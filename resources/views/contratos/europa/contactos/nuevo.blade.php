@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xl-12 col-md-offset-0">
            <div class="panel panel-default">
                <div class="row"> 
                    <div class="panel-heading" id="sanciones-header">                    
                        <div class="col-md-8">
                        <h3 class="module-title">Nuevo Contacto en Europa</h3>
                        </div>
                        <div class="col-md-4">
                            <div>
                                <ul class="nav navbar-nav">
                                    <li><a href="#" id="btn-save"><i class="fa fa-save"> </i> Guardar</a></li>                                 
                                    <li><a href="#" id="volver"><i class="fa fa-arrow-left"> </i> Volver</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="panel-body" style="border-top:2px solid #335599;">                        
                    <form class="inline-form" id="form_sancion">
                         <div class="form-group col-md-2">
                            <label for="matricula">CIF</label>
                    
                           <input type="text" class="form-control" name="cif" value="">
                                    
                        </div>
                        <div class="form-group col-md-3">
                           <label for="matricula">Empresa</label>
                            <input type="text" class="form-control" name="empresa" value="">
                                     
                        </div>
                           <div class="form-group col-md-3">
                           <label for="matricula">Apellidos</label>
                            <input type="text" class="form-control" name="apellidos" value="">
                                     
                        </div>
                          <div class="form-group col-md-2">
                           <label for="matricula">Siglas</label>
                            <input type="text" class="form-control" name="siglas" value="">
                                     
                        </div>
                         <div class="form-group col-md-4">
                  
                            <label for="acuerdo">Dirección</label>
                            <input type="text" class="form-control" name="direccion" value="">   
                                               
                        </div>
                         <div class="form-group col-md-2">
                         
                            <label for="ft">CP</label>
                            <input type="text" class="form-control" name="cp" value="">      
                                               
                        </div>
                          <div class="form-group col-md-2">
                         
                            <label for="ft">apdo. Correo</label>
                            <input type="text" class="form-control" name="apartadoCorreos" value="">      
                                               
                        </div>


                      <div class="form-group col-md-2">
                     
                            <label for="matricula">Población</label>
                           <input type="text" class="form-control" name="poblacion" value="">  
                         
                        </div>
                         <div class="form-group col-md-2">
                            <label for="matricula">País</label>
                            <select class="selectpicker" data-live-search="true" title="Buscar..." id="pais" name="pais">
                                @foreach ($paises as $dato)
                                <option value="{{$dato->codPais}}">{{ $dato->Pais}}</option>
                                @endforeach
                            </select> 
                        </div>
                            <div class="form-group col-md-2">
                            <label for="matricula">Provincia</label>
                            <select class="selectpicker" data-live-search="true" title="Buscar..." id="provincia" name="provincia">
                                @foreach ($provincias as $dato)
                                <option value="{{$dato->codProvincia}}" >{{ $dato->Provincia}}</option>
                                @endforeach
                            </select> 
                        </div>
                        <div class="form-group col-md-3">
                     
                            <label for="matricula">Web</label>
                            <input type="text" class="form-control" name="web" value="">  
                                                     
                        </div>
                           <div class="form-group col-md-3">
                      
                                <label for="matricula">Email</label>
                                <input type="text" class="form-control" name="email" value="">  
                                          
                        </div>
                           <div class="form-group col-md-4">
                      
                                <label for="matricula">CC</label>
                                <input type="text" class="form-control" name="cc" value="">        
                                          
                        </div>
                            <div class="form-group col-md-6">
                            
                            <label for="matricula">Observaciones</label>
                            <textarea type="textarea" class="form-control" name="observaciones" style="" ></textarea> 
                               
                            </div> 
                             <div class="form-group col-md-6">
                           <ul class="list-inline">

                           <li><label for="matricula">Otro comercial</label> <input type="checkbox" name="cteDeOtroComercial" value=""></li>
                          
                           <li><label for="matricula">Sin provisión</label> <input type="checkbox" name="sinProvisionFondos" value=""></li>
                          
                           <li><label for="matricula">Impago</label> <input type="checkbox" name="impagado_pdteDev" value=""></li>
                         
                           <li><label for="matricula">Conflictivo</label> <input type="checkbox" name="cteConflictivo" value=""></li>
                          
                           <li><label for="matricula">Mailing</label> <input type="checkbox" name="mailing" value=""></li>
                        </ul>
                            </div>
                                                 
                        </div>     
                    </form>
                    </div>                        
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

$("#volver").click(function(){
        window.history.go(-1); return true;
    });


 $(document).on('focusin','.datepicker',function(){
         $(this).datepicker({
        format: "dd/mm/yyyy",
        dateFormat: 'yy-mm-dd',
        language: "es",
        autoclose: true
    });

         $(this).selectpicker("data-live-search","true");

    });

 $("#btn-save").click(function(){
  
     setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/europa/contactos/store')}}',
                data : {'datos' : $("#form_sancion").serialize()},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    if(data.includes("SQLSTATE")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                //location.reload();
                            }
                        });
                    }else{
                         bootbox.alert({
                            message: "el contacto se ha creado correctamente",
                            callback: function () {
                                 window.location = '/contratos/europa/contactos/'+data;
                            }
                        });
                    }
                   
                    $("#loading").hide();

                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);



});
</script>
@endsection