@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="row"> 
                    <div class="panel-heading" id="sanciones-header">                    
                        <div class="col-md-12">
                            <h3 class="module-title">Contrato @foreach ($contrato as $dato){!! $dato->codContrato !!} @endforeach</h3>
                            @foreach ($contrato as $dato)<input type="hidden" class="id" name="codContrato" id="codContrato" value="{!!$dato->codContrato !!}"> @endforeach
                        </div>
                        <div class="col-md-12">
                            <div>
                                <ul class="nav navbar-nav">
                                    <li><a href="#" class="bt-edit"><i class="fa fa-save"> </i> Guardar</a></li>
                                    <li><a href="#" class="bt-anexo"><i class="fa fa-plus" aria-hidden="true"></i> Nuevo anexo</a></li>
                                    <a class="dropdown-toggle" href="#" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                       <li> <i class="fa fa-file-text-o" aria-hidden="true"></i> Recibos</a>
                                    <ul class="dropdown-menu" role="menu">
                                         <li><a class="dropdown-item" id="generar_recibos" href="#">Generar</a></li>
                                        <li><a class="dropdown-item" id="sustituir_recibos" href="#" id="newtipo">Sustituir</a></li>
                                        <li><a class="dropdown-item" id="devolver_recibos" href="#" id="newtipo">Devolución</a></li>
                                        <li><a class="dropdown-item" id="anular_recibos" href="#" id="newtipo">Anular</a></li>
                                        <li><a class="dropdown-item" id="imprimir_recibos" href="#" id="newtipo">Imprimir recibos</a></li>
                                        <li><a class="dropdown-item" id="recibo_suelto" href="#" id="newtipo">Recibo suelto</a></li>
                                    </ul></li>
                    
                                    <li><a href="#" class="bt-export" id="desinmovilizacion"><i class="fa fa-print"> </i> Desinmovilización</a></li>
                                    <li><a href="#" class="bt-export" id="imprimir"><i class="fa fa-print"> </i> Imprimir contrato</a></li>
                                    <li><a href="#" id="eliminar"><i class="fa fa-trash"> </i> Eliminar</a></li>
                                    <li><a href="#" id="volver"><i class="fa fa-arrow-left"> </i> Volver</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="panel-body" style="border-top:2px solid #335599;">                        
                    <form class="inline-form" id="form_contrato">
                        <div class="form-group col-md-4">
                            <label for="matricula">Empresa</label>
                            @foreach ($empresas as $dato)
                            <input type="text" class="form-control" name="empresa" value="{!! $dato->razonSocial !!}" disabled>
                            @endforeach                    
                        </div>
                        <div class="form-group col-md-2">
                            <label for="codigo">Tipo Contrato</label>
                          <select class="selectpicker" data-live-search="true" title="Buscar..." id="tipo_contrato" name="tipo_contrato">
                                @foreach ($contrato as $dato)
                              @if($dato->codTipoContrato=="AM")
                              <option value="AM" selected>ASISTENCIA</option>
                              <option value="AJ">MULTA</option>
                              @else
                                <option value="AM" >ASISTENCIA</option>
                              <option value="AJ" selected>MULTA</option>
                              @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="empresa">Contratante</label>
                            @foreach ($contrato as $dato)
                            <select class="selectpicker" data-live-search="true" title="Buscar..." id="contratante" name="contratante">
                                @foreach ($contratantes as $dato)
                                @if($dato->CodContacto==$empresa_comercial)
                                <option value="{{$dato->CodContacto}}" selected>{{ $dato->razonSocial}}</option>
                                @else
                                 <option value="{{$dato->CodContacto}}">{{ $dato->razonSocial}}</option>
                                 @endif
                                @endforeach
                            </select>
                            @endforeach                                 
                        </div>
                        <div class="form-group col-md-4">
                            <label for="cif">Comercial</label>
                            <select class="selectpicker" data-live-search="true" title="Buscar..." id="comercial" name="comercial">
                                @foreach ($comerciales as $dato)
                                @if($dato->ID==$id_comercial)
                                <option value="{{$dato->ID}}" selected>{{ $dato->comercial}}</option>
                                @else
                                 <option value="{{$dato->ID}}">{{ $dato->comercial}}</option>
                                 @endif
                                @endforeach
                            </select>      
                        </div>
                        <div class="form-group col-md-2">
                            <label for="matricula">Fecha Contrato</label>
                            @foreach($contrato as $dato)
                            <input type="text" class="form-control" name="fechaContrato" value="{{ date('d/m/Y', strtotime($dato->fechaContrato)) }}">
                            @endforeach                            
                        </div>
                         <div class="form-group col-md-2">
                            <label for="matricula">Vencimiento</label>
                             @foreach($contrato as $dato)
                            <input type="text" class="form-control" name="vencimiento" value="{{ date('d/m/Y', strtotime($dato->vencimiento)) }}">
                            @endforeach                              
                        </div>  
                         <div class="form-group col-md-2">
                            <label for="matricula">Baja</label>
                           @foreach($contrato as $dato)
                            @if($dato->baja != null && $dato->baja != "")
                            <input type="text" class="form-control" name="baja" value="{{ date('d/m/Y', strtotime($dato->baja)) }}">
                            <input type="hidden" class="form-control" name="baja_check" value="{{ date('d/m/Y', strtotime($dato->baja)) }}">
                            @else
                            <input type="text" class="form-control" name="baja" value="">
                            <input type="hidden" class="form-control" name="baja_check" value="">
                            @endif
                            @endforeach                             
                        </div>
                        <div class="form-group col-md-6">
                            @foreach ($contrato as $dato)
                            <label for="matricula">Observaciones Contrato</label>
                            <textarea type="textarea" class="form-control" name="observaciones_contrato" style="" >{{$dato->observaciones}}</textarea> 
                               @endforeach
                            </div>
                            <div class="form-group col-md-6">
                            @foreach ($empresas as $dato)
                            <label for="matricula">Observaciones Cliente</label>
                            <textarea type="textarea" class="form-control" name="observaciones_cliente" style="" >{{$dato->observaciones}}</textarea> 
                               @endforeach
                            </div>
                            <div class="form-group col-md-3">
                            <label for="matricula">Importe Anexos</label>
                            <input type="text" class="form-control" name="importe_anexos" value="{!! $suma_anexos !!}">     
                        </div>
                        <div class="form-group col-md-3">
                            <label for="matricula">Importe Contrato</label>
                            @foreach ($contrato as $dato)
                            <input type="text" class="form-control" name="importeCto" id="importeCto" value="{!! $dato->importeCto !!}">
                            @endforeach                    
                        </div>
                        <div class="form-group col-md-3">
                            <label for="matricula">Descuento Comercial</label>
                            <input type="text" class="form-control" name="descuento_comercial" value="{!! $dato->descuento !!}">     
                        </div>
                         <div class="form-group col-md-3">
                            <label for="matricula">Comisión</label>
                            <label for="matricula"></label>
                              @foreach ($contrato as $dato)
                            <input type="text" class="form-control" name="comision" value="{!! $dato->totalLiquidacion !!}">
                            @endforeach     
                        </div>
                          <div class="form-group col-md-2">
                            <label for="matricula">Comisión Esp.</label>
                              @foreach ($contrato as $dato)
                            <input type="text" class="form-control" name="comisionespec" value="{!! $dato->porcentComisEspecifica !!}">
                            @endforeach     
                        </div>
                         <div class="form-group col-md-3">
                           @foreach($contrato as $dato)
                           @if($dato->facturar==0)
                           <ul>
                           <li><input type="radio" name="facturar" value="Indefinido" placeholder="Indefinido" checked> <span>Indefinido</span></li>
                           <li><input type="radio" name="facturar" value="Facturar" placeholder="Facturar"> <span>Facturar</span></li>
                           <li><input type="radio" name="facturar" value="no Facturar" placeholder="no Facturar"> <span>No facturar</span></li>
                           </ul>
                           @elseif($dato->facturar==1)
                           <ul>
                           <li><input type="radio" name="facturar" value="Indefinido" placeholder="Indefinido"> <span>Indefinido</span></li>
                          <li><input type="radio" name="facturar" value="Facturar" placeholder="Facturar" checked> <span>Facturar</span></li>
                          <li> <input type="radio" name="facturar" value="no Facturar" placeholder="no Facturar"> <span>No facturar</span></li>
                           </ul>
                            @elseif($dato->facturar==2)
                            <ul>
                           <li><input type="radio" name="facturar" value="Indefinido" placeholder="Indefinido"> <span>Indefinido</span></li>
                           <li><input type="radio" name="facturar" value="Facturar" placeholder="Facturar"> <span>Facturar</span></li>
                           <li><input type="radio" name="facturar" value="no Facturar" placeholder="no Facturar" checked> <span>No facturar</span></li>
                            </ul>
                           @else
                           <ul>
                           <li><input type="radio" name="facturar" value="Indefinido" placeholder="Indefinido" > <span>Indefinido</span></li>
                           <li><input type="radio" name="facturar" value="Facturar" placeholder="Facturar"> <span>Facturar</span></li>
                           <li><input type="radio" name="facturar" value="no Facturar" placeholder="no Facturar"> <span>No facturar</span></li>
                           </ul>
                           @endif
                       @endforeach                        
                        </div>
                        <div class="form-group col-md-2">
                            <label for="matricula">¿Provisión de Fondos?</label>
                           @foreach($contrato as $dato)
                           @if($dato->conProvisionFondos==0)
                            <input type="checkbox" name="provisionfondos" id="provisionfondos" value="">
                            @else
                            <input type="checkbox" name="provisionfondos" id="provisionfondos" value="" checked>
                            @endif
                            @endforeach                             
                        </div>
                        <div class="form-group col-md-2">
                            <label for="matricula">¿De otro comercial?</label>
                           @foreach($contrato as $dato)
                           @if($dato->esCteDeOtroComercial==0)
                            <input type="checkbox" name="otrocomercial" id="otrocomercial" value="">
                            @else
                            <input type="checkbox" name="otrocomercial" id="otrocomercial" value="" checked>
                            @endif
                            @endforeach                             
                        </div>
                         <div class="form-group col-md-2">
                            <label for="matricula">¿Obviar Contrato?</label>
                           @foreach($contrato as $dato)
                           @if($dato->Obviar==0)
                            <input type="checkbox" name="obviar" id="obviar" value="">
                            @else
                            <input type="checkbox" name="obviar" id="obviar" value="" checked>
                            @endif
                            @endforeach                             
                        </div>
                        <div class="form-group col-md-2">
                            <label for="matricula">¿Pendiente F.Pago?</label>
                           @foreach($contrato as $dato)
                           @if($dato->Obviar==0)
                            <input type="checkbox" name="pendienteforma" id="pendienteforma" value="">
                            @else
                            <input type="checkbox" name="pendienteforma" id="pendienteforma" value="" checked>
                            @endif
                            @endforeach                             
                        </div>
                    </form>
                    </div>
                </div>
                <div class="panel panel-default">
              <div class="row"> 
                    <div class="panel-heading" id="detalles-header">                    
                        <div class="col-md-9">
                            <h3 class="module-title">Detalles</h3>
                                <div class="col-md-12">
                            <div>
                                <ul class="nav navbar-nav">
                                    <li class="detalles"><a id="anexos"><i class="fa fa-list-alt" aria-hidden="true"></i> Anexos</a></li>
                                    <li class="detalles"><a id= "recibos"><i class="fa fa-file-text-o" aria-hidden="true"></i> Recibos</a></li>
                                    <li class="detalles"><a id="liquidaciones"><i class="fa fa-eur" aria-hidden="true"></i> Liquidaciones</a></li>
                                    <li class="detalles"><a id="abonos"><i class="fa fa-money" aria-hidden="true"></i> Abonos multa</a></li>
                                </ul>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                    <div id="error" style="display:none" class="alert alert-danger alert-dismissible fade in" role="alert"></div>
                    <div id="success" style="display:none" class="alert alert-success alert-dismissible"></div>
                     <div class="table-responsive" id="tabla_contenido">
                    <table class="table">
                        <thead class="table-header">                       
                            <th>Matricula</th>
                            <th>Tipo</th>
                             <th>Alta</th>
                             <th>Baja</th>
                             <th>Importe</th>
                            <th></th>
                        </thead>
                        <tbody id="myTable">
                        @foreach ($anexos as $dato)
                       <tr>
                       <td>{!!$dato->Matricula !!}<input type="hidden" class="idAnexo" name="idAnexo" value="{!!$dato->CodAnexo !!}"></td>
                       <td>{!! $dato->tipoAnexo !!}</td>
                       <td>{{ date('d/m/Y', strtotime($dato->Alta))}}</td>
                       <td>{{ date('d/m/Y', strtotime($dato->Baja))}}</td>
                       <td>{!! $dato->importeVenta !!}</td>
                       <td><a class="btn btn-sm btn-success" href="{{URL::to('contratos/europa/anexos/'.$dato->CodAnexo)}}"><i class="fa fa-edit"></i> </a> <a class="btn btn-sm btn-danger borrar_anexo"><i class="fa fa-trash"></i></a></td>
                       </tr>
                            @endforeach 
                        </tbody>
                    </table>
                     
                </div>
                    </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">


$("#volver").click(function(){
        window.history.go(-1); return true;
    });


 $(document).on('focusin','.datepicker',function(){
         $(this).datepicker({
        format: "dd/mm/yyyy",
        dateFormat: 'yy-mm-dd',
        language: "es",
        autoclose: true
    });

         $(this).selectpicker("data-live-search","true");

    });


  $("#generar_recibos").click(function(){

     $codContrato = $("#codContrato").val();
     $importeCto = $("#importeCto").val();

       if($("input[name=baja]").val()!= ""){
      bootbox.alert("no puede editar el contrato, debe anular primero el campo de la fecha de baja");
    }else{
      setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('contratos/europa/mostrar_generar_recibos')}}',
                        data : {'codContrato':$codContrato,'importeCto':$importeCto},
                        success : function(data){
                           var dialog = bootbox.dialog({
                                    message: data,
                                    title: "Generar recibos",
                                    buttons: {
                                        success: {
                                            label: 'aceptar',
                                             callback: function() {
                                                $numRecibo = $("#num_recibo").val();
                                                $codFormaPago = $("#forma_pago_recibo option:selected").val();
                                                $importe_recibo = $("#importe_recibo").val();
                                                $vencimiento = $("#venc_recibo").val();
                                                $notas = $("#notas_recibo").val();
                                                generar_recibos($codContrato,$numRecibo,$codFormaPago,$importe_recibo,$vencimiento,$notas);
                                                dialog.modal('hide');
                                                 }
                                        }
                                    }
                                });
                             $('.selectpicker').selectpicker('refresh');
                             $('.selectpicker').selectpicker('render');
                             
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);
    }

      

});

  $("#sustituir_recibos").click(function(){

     $codContrato = $("#codContrato").val();
     $importeCto = $("#importeCto").val();

      if($("input[name=baja]").val()!= ""){
      bootbox.alert("no puede editar el contrato, debe anular primero el campo de la fecha de baja");
    }else{
      setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('contratos/europa/mostrar_sustitucion_recibos')}}',
                        data : {'codContrato':$codContrato,'importeCto':$importeCto},
                        success : function(data){
                           var dialog = bootbox.dialog({
                                    message: data,
                                    title: "Sustituir recibos",
                                    buttons: {
                                        success: {
                                            label: 'aceptar',
                                             callback: function() {
                                                $recibo_origen = $("#recibo_origen option:selected").val();
                                                $num_origen = $("#recibo_origen option:selected").text();
                                                $numRecibo = $("#num_recibo").val();
                                                $codFormaPago = $("#forma_pago_recibo option:selected").val();
                                                $importe_recibo = $("#importe_recibo").val();
                                                $vencimiento = $("#venc_recibo").val();
                                                $notas = $("#notas_recibo").val();
                                                sustituir_recibos($recibo_origen,$num_origen,$codContrato,$numRecibo,$codFormaPago,$importe_recibo,$vencimiento,$notas);
                                                dialog.modal('hide');
                                                 }
                                        }
                                    }
                                });
                             $('.selectpicker').selectpicker('refresh');
                             $('.selectpicker').selectpicker('render');
                             
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);
    }

      

});

    $("#devolver_recibos").click(function(){

     $codContrato = $("#codContrato").val();

     if($("input[name=baja]").val()!= ""){
      bootbox.alert("no puede editar el contrato, debe anular primero el campo de la fecha de baja");
    }else{
        setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('contratos/europa/mostrar_devolucion_recibo')}}',
                        data : {'codContrato':$codContrato},
                        success : function(data){
                           var dialog = bootbox.dialog({
                                    message: data,
                                    title: "Devolución de recibos",
                                    buttons: {
                                        success: {
                                            label: 'aceptar',
                                             callback: function() {
                                                $recibo_origen = $("#recibo_origen option:selected").val();
                                                $gastos = $("#gastosDevolucion").val();
                                                $devolucion = $("#devolucion").val();
                                                $notas = $("#notas_recibo").val();
                                                devolver_recibo($codContrato,$recibo_origen,$gastos,$devolucion,$notas);
                                                dialog.modal('hide');
                                                 }
                                        }
                                    }
                                });
                             $('.selectpicker').selectpicker('refresh');
                             $('.selectpicker').selectpicker('render');
                             
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);
    }

      

});

        $("#anular_recibos").click(function(){

     $codContrato = $("#codContrato").val();

     if($("input[name=baja]").val()!= ""){
      bootbox.alert("no puede editar el contrato, debe anular primero el campo de la fecha de baja");
    }else{
      setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('contratos/europa/mostrar_anular_recibo')}}',
                        data : {'codContrato':$codContrato},
                        success : function(data){
                           var dialog = bootbox.dialog({
                                    message: data,
                                    title: "anular recibos",
                                    buttons: {
                                        success: {
                                            label: 'aceptar',
                                             callback: function() {
                                                $recibo_origen = $("#recibo_origen option:selected").val();
                                                $notas = $("#notas_recibo").val();
                                                anular_recibos($codContrato,$recibo_origen,$notas);
                                                dialog.modal('hide');
                                                 }
                                        }
                                    }
                                });
                             $('.selectpicker').selectpicker('refresh');
                             $('.selectpicker').selectpicker('render');
                             
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);
    }

      

});



   $(document).on('focusout','#num_recibo',function(){

        $val = $("input[name='precio']:checked").val();
        if($val!="manual"){
            $importe = $("#importeCto").val();
            if($(".modal-title").text()=="Sustituir recibos"){
                 $importe =  $("#importe_fijo").val();
             }
           
            $numRecibo = $(this).val();

            $importe_final = $importe/$numRecibo;
            $("#importe_recibo").val($importe_final);
        }
         
    });

   $(document).on('change','#recibo_origen',function(){

    $origen = $(this).find('option:selected').val();

       setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/europa/get_importe_recibo')}}',
                data : {'origen':$origen},
                success : function(data){
                    console.log(JSON.stringify(data));
                    $("#importe_recibo").prop("disabled",false);
                    $("#importe_recibo").val(data);
                    $("#importe_fijo").val(data);
                    $("#loading").hide();

                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);
         
    });



  $(".bt-edit").click(function(){

    
    $ID = $("#codContrato").val();

    $form = $("#form_contrato").serialize();

    if($("input[name=baja]").val()!= ""){
      bootbox.alert("no puede editar el contrato, debe anular primero el campo de la fecha de baja");
    }else{
       setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '/contratos/europa/'+$ID+'/guardar',
                data : {'datos' : $("#form_contrato").serialize()},
                success : function(data){
                    console.log(JSON.stringify(data));
                    bootbox.alert({
                            message: data,
                            callback: function () {
                                location.reload();
                            }
                        });
                    $("#loading").hide();

                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);
    }
  
    

});

  $(".bt-anexo").click(function(){

    $ID = $("#codContrato").val();

    window.location.href = "/contratos/europa/"+$ID+"/crear_anexo";
  });

 $(document).on('click','.borrar_anexo',function(){

  if($("input[name=baja]").val()!= ""){
      bootbox.alert("no puede editar el contrato, debe anular primero el campo de la fecha de baja");
    }else{
       $ID = $(this).closest("tr").find('.idAnexo').val();

bootbox.confirm({
     title: "Eliminar Registro",
    message: "<i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> ¿Está seguro que desea eliminar el registro?</strong> Tenga en cuenta que esta acción es irreversible.",
    buttons: {
        cancel: {
            label: '<i class="fa fa-times"></i> Cancelar'
        },
        confirm: {
            label: '<i class="fa fa-check"></i> Aceptar'
        }
    },
    callback: function (result) {

            if(result == true){
                
                setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('contratos/borrar_anexo')}}',
                        data : {'ID':$ID},            
                        success : function(data){
                            console.log(JSON.stringify(data));
                            $("#success").empty();
                            $("#success").append(data);
                            $("#success").show();
                            location.reload();
                            $("#loading").hide();
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);

            } 
    }
});
    }

});

 $('#recibos').click(function(){
        //alert("reducciones");
        $ID = $("#codContrato").val();
        setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/europa/recibos')}}',
                data : {'codContrato':$ID},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('#tabla_contenido').html(data);
                    $('.selectpicker').selectpicker('refresh');
                    $('.selectpicker').selectpicker('render');
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

    });

 $(document).on('click','.update_recibo',function(){

  $codRecibo = $(this).closest("tr").find('.codRecibo').val();
  $estados_recibo = $(this).closest("tr").find('.estados_recibo option:selected').val();
  $forma_pago_recibo = $(this).closest("tr").find('.forma_pago_recibo option:selected').val();
  $importe = $(this).closest("tr").find('.importe_recibo').val();
  $FechaVencimiento = $(this).closest("tr").find('.FechaVencimiento').val();
  $fechaDevolucion = $(this).closest("tr").find('.fechaDevolucion').val();
  $fechaSustitucion = $(this).closest("tr").find('.fechaSustitucion').val();
  $gastosDevolucion = $(this).closest("tr").find('.gastosDevolucion').val();

  bootbox.confirm({
    title: "Modificar recibo",
    message: "¿Desea modificar el registro?.",
    buttons: {
        cancel: {
            label: '<i class="fa fa-times"></i> Cancelar'
        },
        confirm: {
            label: '<i class="fa fa-check"></i> Aceptar'
        }
    },
    callback: function (result) {
        if(result){
            setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/europa/update_recibo')}}',
                data : {'codRecibo':$codRecibo,'estados_recibo':$estados_recibo,'forma_pago_recibo':$forma_pago_recibo,'importe':$importe,'FechaVencimiento':$FechaVencimiento,'fechaDevolucion':$fechaDevolucion,'fechaSustitucion':$fechaSustitucion,'gastosDevolucion':$gastosDevolucion},   
                success : function(data){
                    bootbox.alert(data, function(){ location.reload(); });
                    
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);
        }
    }
});

 


 });

 $('#anexos').click(function(){
        //alert("reducciones");
        $ID = $("#codContrato").val();
        setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/europa/anexos')}}',
                data : {'codContrato':$ID},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('#tabla_contenido').html(data);
                    $('.selectpicker').selectpicker('refresh');
                    $('.selectpicker').selectpicker('render');
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

    });


 $('#recibos').click(function(){
        //alert("reducciones");
        $ID = $("#codContrato").val();
        setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/europa/recibos')}}',
                data : {'codContrato':$ID},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('#tabla_contenido').html(data);
                    $('.selectpicker').selectpicker('refresh');
                    $('.selectpicker').selectpicker('render');
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

    });

  $('#liquidaciones').click(function(){
        //alert("reducciones");
        $ID = $("#codContrato").val();
        setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/europa/liquidaciones')}}',
                data : {'codContrato':$ID},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('#tabla_contenido').html(data);
                    $('.selectpicker').selectpicker('refresh');
                    $('.selectpicker').selectpicker('render');
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

    });


  $('#abonos').click(function(){
        //alert("reducciones");
        $ID = $("#codContrato").val();
        setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/europa/abonos')}}',
                data : {'codContrato':$ID},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('#tabla_contenido').html(data);
                    $('.selectpicker').selectpicker('refresh');
                    $('.selectpicker').selectpicker('render');
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

    });


 $("#eliminar").click(function(){


    bootbox.confirm({
        title: "Eliminar Contrato",
        message: "<i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> ¿Está seguro que desea eliminar el Contrato?</strong> Tenga en cuenta que esta acción es irreversible.",
        buttons: {
            cancel: {
            label: '<i class="fa fa-times"></i> Cancelar'
        },
        confirm: {
            label: '<i class="fa fa-check"></i> Aceptar'
        }
        },
        callback: function (result) {
            if(result == true){
                $ID = $("#id").val();
                $tabla = 'its_contratos';
                $field = "IDContrato";

                setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('contratos/borrar')}}',
                        data : {'ID':$ID,'tabla': $tabla,'field':$field},            
                        success : function(data){
                            window.location = "/contratos/its";
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);
            }
        }
    });

});




 function generar_recibos(codContrato,numRecibo,forma_pago,importe_recibo,vencimiento,notas){

      setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('contratos/europa/generar_recibos')}}',
                        data : {'codContrato':codContrato,'num_recibo':numRecibo,'forma_pago':forma_pago,'importe_recibo':importe_recibo,'vencimiento':vencimiento,'notas_recibo':notas},            
                        success : function(data){
                            console.log(JSON.stringify(data));
                    if(data.includes("SQLSTATE")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                //location.reload();
                            }
                        });
                    }else{
                         bootbox.alert({
                            message: data,
                            callback: function () {
                                 location.reload();
                            }
                        });
                    }
                   
                    $("#loading").hide();
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);

  }

  function sustituir_recibos(recibo_origen,num_origen,codContrato,numRecibo,forma_pago,importe_recibo,vencimiento,notas){

      setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('contratos/europa/sustituir_recibos')}}',
                        data : {'recibo_origen':recibo_origen,'num_origen':num_origen,'codContrato':codContrato,'num_recibo':numRecibo,'forma_pago':forma_pago,'importe_recibo':importe_recibo,'vencimiento':vencimiento,'notas_recibo':notas},            
                        success : function(data){
                            console.log(JSON.stringify(data));
                    if(data.includes("SQLSTATE")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                //location.reload();
                            }
                        });
                    }else{
                         bootbox.alert({
                            message: data,
                            callback: function () {
                                 location.reload();
                            }
                        });
                    }
                   
                    $("#loading").hide();
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);

  }

   function devolver_recibo(codContrato,recibo_origen,gastos,devolucion,notas){

      setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('contratos/europa/devolver_recibo')}}',
                        data : {'codContrato':codContrato,'recibo_origen':recibo_origen,'gastos':gastos,'devolucion':devolucion,'notas_recibo':notas},            
                        success : function(data){
                            console.log(JSON.stringify(data));
                    if(data.includes("SQLSTATE")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                //location.reload();
                            }
                        });
                    }else{
                         bootbox.alert({
                            message: data,
                            callback: function () {
                                 location.reload();
                            }
                        });
                    }
                   
                    $("#loading").hide();
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);

  }

  function anular_recibos(codContrato,recibo_origen,notas){

      setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('contratos/europa/anular_recibo')}}',
                        data : {'codContrato':codContrato,'recibo_origen':recibo_origen,'notas_recibo':notas},            
                        success : function(data){
                            console.log(JSON.stringify(data));
                    if(data.includes("SQLSTATE")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                //location.reload();
                            }
                        });
                    }else{
                         bootbox.alert({
                            message: data,
                            callback: function () {
                                 location.reload();
                            }
                        });
                    }
                   
                    $("#loading").hide();
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);

  }

  $(document).on('click','.editar_liquidacion',function(){


    if($("input[name=baja]").val()!= ""){
      bootbox.alert("no puede editar el contrato, debe anular primero el campo de la fecha de baja");
    }else{
      $recibo = $(this).closest("tr").find('.recibo option:selected').val();
        $fecha = $(this).closest("tr").find('.fecha_de_liquidacion').val();
        $forma_pago = $(this).closest("tr").find('.forma_pago option:selected').val();
        $importe = $(this).closest("tr").find('.importe').val();
        setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/europa/editar_liquidacion')}}',
                data : {'recibo':$recibo,'fecha':$fecha,'forma_pago':$forma_pago,'importe':$importe},            
                success : function(data){
                    if(data.includes("SQLSTATE")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                //location.reload();
                            }
                        });
                    }else{
                         bootbox.alert({
                            message: data,
                            callback: function () {
                                 location.reload();
                            }
                        });
                    }
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);
    }

        

    });

    $(document).on('click','.add_liquidacion',function(){

       if($("input[name=baja]").val()!= ""){
      bootbox.alert("no puede editar el contrato, debe anular primero el campo de la fecha de baja");
    }else{
      $recibo = $(this).closest("tr").find('.recibo option:selected').val();
        $fecha = $(this).closest("tr").find('.fecha_de_liquidacion').val();
        $forma_pago = $(this).closest("tr").find('.forma_pago option:selected').val();
        $importe = $(this).closest("tr").find('.importe').val();
        setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/europa/add_liquidacion')}}',
                data : {'recibo':$recibo,'fecha':$fecha,'forma_pago':$forma_pago,'importe':$importe},            
                success : function(data){
                    if(data.includes("SQLSTATE")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                //location.reload();
                            }
                        });
                    }else{
                         bootbox.alert({
                            message: data,
                            callback: function () {
                                 location.reload();
                            }
                        });
                    }
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);
    }


        

    });

    $(document).on('click','.eliminar_liquidacion',function(){

      if($("input[name=baja]").val()!= ""){
      bootbox.alert("no puede editar el contrato, debe anular primero el campo de la fecha de baja");
    }else{
      $recibo = $(this).closest("tr").find('.recibo option:selected').val();
        bootbox.confirm({
    title: "Eliminar liquidación",
    message: "¿Desea realmente eliminar el registro? Tenga en cuenta que el proceso es irreversible.",
    buttons: {
        cancel: {
            label: '<i class="fa fa-times"></i> Cancelar'
        },
        confirm: {
            label: '<i class="fa fa-check"></i> Aceptar'
        }
    },
    callback: function (result) {
       if(result){
         setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('intervenciones/eliminar_liquidacion')}}',
                data : {'recibo':$recibo},            
                success : function(data){
                    if(data.includes("SQLSTATE")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                //location.reload();
                            }
                        });
                    }else{
                         bootbox.alert({
                            message: data,
                            callback: function () {
                                 location.reload();
                            }
                        });
                    }
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);
       }
    }
});
    }


    });

    $(document).on('click','.editar_abono',function(){

      if($("input[name=baja]").val()!= ""){
      bootbox.alert("no puede editar el contrato, debe anular primero el campo de la fecha de baja");
    }else{
      $abono = $(this).closest("tr").find('.idabono').val();
        $tipo = $(this).closest("tr").find('.tipo_abono option:selected').val();
        $expediente = $(this).closest("tr").find('.expediente').val();
        $importe = $(this).closest("tr").find('.importe').val();
        $notas = $(this).closest("tr").find('.notas').val();

        setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/europa/editar_abono')}}',
                data : {'abono':$abono,'tipo':$tipo,'expediente':$expediente,'importe':$importe,'notas':$notas},            
                success : function(data){
                    if(data.includes("SQLSTATE")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                //location.reload();
                            }
                        });
                    }else{
                         bootbox.alert({
                            message: data,
                            callback: function () {
                                 location.reload();
                            }
                        });
                    }
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);
    }

    });

    $(document).on('click','.add_abono',function(){

       if($("input[name=baja]").val()!= ""){
      bootbox.alert("no puede editar el contrato, debe anular primero el campo de la fecha de baja");
    }else{
       $abono = $(this).closest("tr").find('.idabono').val();
        $tipo = $(this).closest("tr").find('.tipo_abono option:selected').val();
        $expediente = $(this).closest("tr").find('.expediente').val();
        $importe = $(this).closest("tr").find('.importe').val();
        $notas = $(this).closest("tr").find('.notas').val()
        $idcontrato = $("#codContrato").val()

        setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/europa/editar_abono')}}',
                data : {'abono':$abono,'tipo':$tipo,'expediente':$expediente,'importe':$importe,'notas':$notas,'idcontrato':$idcontrato},            
                success : function(data){
                    if(data.includes("SQLSTATE")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                //location.reload();
                            }
                        });
                    }else{
                         bootbox.alert({
                            message: data,
                            callback: function () {
                                 location.reload();
                            }
                        });
                    }
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);
    } 

    });

    $(document).on('click','.eliminar_abono',function(){

       if($("input[name=baja]").val()!= ""){
      bootbox.alert("no puede editar el contrato, debe anular primero el campo de la fecha de baja");
    }else{

      $abono = $(this).closest("tr").find('.idabono').val();
        bootbox.confirm({
    title: "Eliminar liquidación",
    message: "¿Desea realmente eliminar el registro? Tenga en cuenta que el proceso es irreversible.",
    buttons: {
        cancel: {
            label: '<i class="fa fa-times"></i> Cancelar'
        },
        confirm: {
            label: '<i class="fa fa-check"></i> Aceptar'
        }
    },
    callback: function (result) {
       if(result){
         setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('intervenciones/eliminar_liquidacion')}}',
                data : {'abono':$abono},            
                success : function(data){
                    if(data.includes("SQLSTATE")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                //location.reload();
                            }
                        });
                    }else{
                         bootbox.alert({
                            message: data,
                            callback: function () {
                                 location.reload();
                            }
                        });
                    }
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);
       }
    }
  });
}
      
    });

     $("#imprimir_recibos").click(function(){

     $codContrato = $("#codContrato").val();

      setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('contratos/europa/imprimir_recibos')}}',
                        data : {'codContrato':$codContrato},
                        success : function(data){
                        if(data==1){
                          bootbox.alert("No existe ningún recibo para imprimir en este contrato")
                        }else{
                           window.location = '/contratos/europa/impresion_recibos?codContrato='+$codContrato;
                        }
                             
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);
    

      

});

$("#recibo_suelto").click(function(){

     $codContrato = $("#codContrato").val();

      setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('contratos/europa/mostrar_recibo_suelto')}}',
                        data : {},
                        success : function(data){
                           var dialog = bootbox.dialog({
                                    message: data,
                                    title: "Generar un recibo suelto",
                                    buttons: {
                                        success: {
                                            label: 'aceptar',
                                             callback: function() {
                                                $importe = $("#importe_suelto").val();
                                                $vencimiento = $("#vencimiento_suelto").val();
                                                window.location = '/contratos/europa/generar_recibo_suelto?codContrato='+$codContrato+'&importe='+$importe+'&vencimiento='+$vencimiento;
                                                dialog.modal('hide');
                                                 }
                                        }
                                    }
                                });
                             $('.selectpicker').selectpicker('refresh');
                             $('.selectpicker').selectpicker('render');
                             
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);



});

      $("#desinmovilizacion").click(function(){

     $codContrato = $("#codContrato").val();

      setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('contratos/europa/mostrar_desinmovilizacion')}}',
                        data : {'codContrato':$codContrato},
                        success : function(data){
                           var dialog = bootbox.dialog({
                                    message: data,
                                    title: "Petición Desinmovilización/Baja",
                                    buttons: {
                                        success: {
                                            label: 'generar',
                                             callback: function() {
                                                $fecha_informe = $("#fecha_informe").val();
                                                $fecha_baja_cambio = $("#fecha_baja_cambio").val();
                                                $concepto = $("#concepto_informe option:selected").val();
                                                $cobertura = $("#cobertura_informe option:selected").val();
                                                $comentario = $("#comentario_informe").val();
                                                $advertencia = $("#advertencia_informe").val();

                                                if($concepto==1 || $concepto==2){
                                                  window.location = '/contratos/europa/generar_desinmovilizacion?codContrato='+$codContrato+'&fecha_informe='+$fecha_informe+'&concepto='+$concepto+'&cobertura='+$cobertura+'&comentario='+$comentario+'&advertencia='+$advertencia;
                                                }
                                                if($concepto==3){
                                                  window.location = '/contratos/europa/informe_cambio_matriculas?codContrato='+$codContrato+'&fecha_informe='+$fecha_informe+'&concepto='+$concepto+'&fecha_baja_cambio='+$fecha_baja_cambio+'&comentario='+$comentario+'&advertencia='+$advertencia;
                                                }
                                                if($concepto==4){
                                                   window.location = '/contratos/europa/informe_baja_matriculas?codContrato='+$codContrato+'&fecha_informe='+$fecha_informe+'&concepto='+$concepto+'&fecha_baja_cambio='+$fecha_baja_cambio+'&comentario='+$comentario+'&advertencia='+$advertencia;
                                                }

                                                
                                                dialog.modal('hide');
                                                 }
                                        }
                                    }
                                });
                             $('.selectpicker').selectpicker('refresh');
                             $('.selectpicker').selectpicker('render');
                             
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);
    

      

});

      $("#imprimir").click(function(){

     $codContrato = $("#codContrato").val();

      setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('contratos/europa/mostrar_imprimir_contrato')}}',
                        data : {'codContrato':$codContrato},
                        success : function(data){
                           var dialog = bootbox.dialog({
                                    message: data,
                                    title: "Imprimir el contrato",
                                    buttons: {
                                        success: {
                                            label: 'generar',
                                             callback: function() {
                                                $ejemplar = $("input[name=ejemplar]:checked").val();
                                                $condiciones = $("input[name=condiciones]:checked").val();
                                                $borrar = $("input[name=borrar]:checked").val();
                                                $en_blanco= "";
                                                if($("input[name=en_blanco]:checked")){
                                                $en_blanco = $("input[name=en_blanco]:checked").val();
                                                }
                                                
                                                
                                                   window.location = '/contratos/europa/imprimir_contrato?codContrato='+$codContrato+'&ejemplar='+$ejemplar+'&condiciones='+$condiciones+'&borrar='+$borrar+'&en_blanco='+$en_blanco;
                                                

                                                
                                                dialog.modal('hide');
                                                 }
                                        }
                                    }
                                });
                             $('.selectpicker').selectpicker('refresh');
                             $('.selectpicker').selectpicker('render');
                             
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);
    

      

});


  $(document).on('change','#concepto_informe',function(){

      if($(this).find('option:selected').val()==3 || $(this).find('option:selected').val()==4 ){
        $("#coberturas").css("display", "none");
        $("#fechas").css("display", "block");
      }else{
        $("#coberturas").css("display", "block");
        $("#fechas").css("display", "none");
      }
    });



</script>

@endsection