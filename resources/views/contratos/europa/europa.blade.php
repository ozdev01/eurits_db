@extends('layouts.app')
@section('content')

<div class="container">
    <div class="row">
        <div class="col-sm-2">
         
         <!--   <h4>Búsqueda</h4>           
            <!--<label for="search" id="loading" style="display:none;">
                <i class="fa fa-refresh fa-spin fa-1x fa-fw"></i>
            </label>-->
                <!--  <input type="text" class="form-control" id="search" name="search" placeholder="Buscar...">-->
            <h4>Filtros</h4>
            <div class =filtros>
                 <a data-toggle="collapse" href="#fechallegada" aria-expanded="false" aria-controls="fechallegada"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> fecha de Contrato</h5></a>
                <div class="collapse" id="fechallegada">
                <input type="text" class="form-control datepicker" name="fecha_alta_inicio" id="fecha_alta_inicio" value="" placeholder="fecha de inicio...">
                <br>
                <input type="text" class="form-control datepicker" id="fecha_alta_fin" value="" placeholder="fecha de fin...">
                </div>
            </div>
            <div class="filtros">
                <a data-toggle="collapse" href="#fechabaja" aria-expanded="false" aria-controls="fechabaja"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> fecha de Baja</h5></a>
                <div class="collapse" id="fechabaja">
                <input type="text" class="form-control datepicker" name="fecha_baja_inicio" id="fecha_baja_inicio" value="" placeholder="fecha de inicio...">
                <br>
                <input type="text" class="form-control datepicker" id="fecha_baja_fin" name="fecha_baja_fin" value="" placeholder="fecha de fin...">
                </div>
            </div>
             <div class="filtros">
           
                <a data-toggle="collapse" href="#fechavenc" aria-expanded="false" aria-controls="fechavenc"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Vencimiento</h5></a>
                <div class="collapse" id="fechavenc">
                <input type="text" class="form-control datepicker" name="fecha_venc_inicio" id="fecha_venc_inicio" value="" placeholder="fecha de inicio...">
                <br>
                <input type="text" class="form-control datepicker" id="fecha_venc_fin" name="fecha_venc_fin" value="" placeholder="fecha de fin...">
                </div>
            </div>
              <div class="filtros">
                <a data-toggle="collapse" href="#fechaanexo" aria-expanded="false" aria-controls="fechaanexo"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Alta Anexo</h5></a>
                <div class="collapse" id="fechaanexo">
                <input type="text" class="form-control datepicker" name="alta_anexo_inicio" id="alta_anexo_inicio" value="" placeholder="fecha de inicio...">
                <br>
                <input type="text" class="form-control datepicker" id="alta_anexo_fin" name="alta_anexo_fin" value="" placeholder="fecha de fin...">
                </div>
            </div>
                <div class =filtros>
                 <a data-toggle="collapse" href="#colcodcontrato" aria-expanded="false" aria-controls="colcodcontrato"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Codigo Contrato</h5></a>
                <div class="collapse" id="colcodcontrato">
                <input type="text" class="form-control" name="codContrato" id="codContrato" value="" placeholder=".">
                </div>
            </div>
             <div class =filtros>
                 <a data-toggle="collapse" href="#colimporte" aria-expanded="false" aria-controls="colimporte"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Importe</h5></a>
                <div class="collapse" id="colimporte">
                <input type="text" class="form-control" name="importe" id="importe" value="" placeholder="">
                </div>
            </div>
                <div class =filtros>
                 <a data-toggle="collapse" href="#coldescuento" aria-expanded="false" aria-controls="coldescuento"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Descuento</h5></a>
                <div class="collapse" id="coldescuento">
                <input type="text" class="form-control" name="descuento" id="descuento" value="" placeholder="">
                </div>
            </div>
              <div class =filtros>
                 <a data-toggle="collapse" href="#colcomision" aria-expanded="false" aria-controls="colcomision"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Comisión</h5></a>
                <div class="collapse" id="colcomision">
                <input type="text" class="form-control" name="comision" id="comision" value="" placeholder="">
                </div>
            </div>
              <div class =filtros>
                 <a data-toggle="collapse" href="#colempresa" aria-expanded="false" aria-controls="colempresa"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Empresa</h5></a>
                <div class="collapse" id="colempresa">
               <select class="selectpicker" data-live-search="true" title="Buscar..." id="empresa" name="empresa">
                 <option selected></option>
                    @foreach ($empresas as $empresa)
                    <option value="{{$empresa->CodContacto}}">{{ $empresa->razonSocial}}</option>
                    @endforeach
                </select>
                </div>
            </div>
            <div class =filtros>
                 <a data-toggle="collapse" href="#colcliotro" aria-expanded="false" aria-controls="colcliotro"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Cliente Otro comercial</h5></a>
                <div class="collapse" id="colcliotro">
                <input type="checkbox" name="otrocomercial" value="" id="otrocomercial">
                </div>
            </div>
              <div class =filtros>
                 <a data-toggle="collapse" href="#colpendientepago" aria-expanded="false" aria-controls="colpendientepago"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Pendiente forma de pago</h5></a>
                <div class="collapse" id="colpendientepago">
                <input type="checkbox" name="pendienteforma" value="" id="pendienteforma">
                </div>
            </div>

              <div class =filtros>
                 <a data-toggle="collapse" href="#coltipoanexo" aria-expanded="false" aria-controls="coltipoanexo"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Tipo de Anexo</h5></a>
                <div class="collapse" id="coltipoanexo">
                <select class="selectpicker" data-live-search="true" title="Buscar..." id="tipo_anexo" name="tipo_anexo">
                 <option selected></option>
                    @foreach ($tipos_anexo as $tipo)
                    <option>{{ $tipo->tipoAnexo}}</option>
                    @endforeach
                </select>
                </div>
            </div>
            <div class="filtros">
                <a data-toggle="collapse" href="#colcontratante" aria-expanded="false" aria-controls="colcontratante"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Empresa Comercial</h5></a>
                 <div class="collapse" id="colcontratante">
                 <select class="selectpicker" data-live-search="true" title="Buscar..." id="contratante" name="contratante">
                 <option selected></option>
                    @foreach ($contratantes as $contratante)
                    <option value="{{$contratante->CodContacto}}">{{ $contratante->razonSocial}}</option>
                    @endforeach
                </select>
                </div>
            </div>
            <div class="filtros">
                <a data-toggle="collapse" href="#colcomercial" aria-expanded="false" aria-controls="colcomercial"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Nombre Comercial</h5></a>
                 <div class="collapse" id="colcomercial">
                  <select class="selectpicker" data-live-search="true" title="Buscar..." id="comercial" name="comercial">
                 <option selected></option>
                    @foreach ($comerciales as $comercial)
                    <option value="{{$comercial->ID}}">{{ $comercial->comercial}}</option>
                    @endforeach
                </select>
                </div>
            </div>
              <div class="filtros">
                 <a data-toggle="collapse" href="#colabono" aria-expanded="false" aria-controls="colabono"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Tipo de Abono</h5></a>
                <div class="collapse" id="colabono">
                  <select class="selectpicker" data-live-search="true" title="Buscar..." id="tipo_abono" name="tipo_abono">
                 <option selected></option>
                    <option value="-2">Abono Normal (reduce cto/comis)</option>
                    <option value="-3">Abono Multa, POST FACTURA (NO reduce contrato/comi.)</option>
                    <option value="0">Abono Multa, MODIFICA factura (NO reduce contrato/comi.)</option>
                    <option value="-1">Abono Multa, REDUCE contrato y comisión</option>
                </select>
                </div>
            </div>
            <div class="filtros">
            <a data-toggle="collapse" href="#colmatricula" aria-expanded="false" aria-controls="colmatricula"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Matricula</h5></a>
            <div class="collapse" id="colmatricula">
             <select class="selectpicker" data-live-search="true" title="Buscar..." id="matricula" name="matricula">
                 <option selected></option>
                    @foreach ($matriculas as $matricula)
                    <option>{{ $matricula->Matricula}}</option>
                    @endforeach
                </select>
                </div>
            <div class="filtros">
            <button type="button" id="filtrar" class="btn btn-default btn-primary">filtrar</button>
            </div>
        </div>
        </div>
        <div class="col-sm-10">
            <div class="panel panel-default">
                <div class="row"> 
                    <div class="panel-heading" id="sanciones-header">                    
                        <div class="col-md-2" style="width: 100%">
                            <h3 class="module-title">Contratos Europa</h3>
                        </div>
                        <div class="col-md-10">
                            <div>
                                <ul class="list-inline nav navbar-nav">
                                    <li><a href="{{url('contratos/europa/nuevo')}}"><i class="fa fa-plus"> </i> Nuevo</a></li>
                                     
                                     <li><a class="dropdown-toggle" href="#" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-list-alt" aria-hidden="true"></i> Anexos</a>
                                    <ul class="dropdown-menu" role="menu">
                                         <li><a class="dropdown-item" href="{{url('contratos/europa/ver_anexos')}}">Ver anexos</a></li>
                                        <li><a class="dropdown-item" href="{{url('contratos/europa/tipos_anexo')}}" id="newtipo">Tipos de Anexos</a></li>
                                    </ul>
                                    </li>

                                    <li><a href="{{url('contratos/europa/contactos')}}"><i class="fa fa-user" aria-hidden="true"></i> Contactos</a></li>
                                    <li><a href="{{url('contratos/europa/franquicias')}}"> <i class="fa fa-shopping-bag" aria-hidden="true"></i> Franquicias</a>
                                    <li><a href="#" id="excel"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Excel</a></li>
                                     <li><a href="#" id="ver_renovaciones"><i class="fa fa-plus"> </i> Renovaciones</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table-responsive"">
                    <table class="table">
                        <thead class="table-header">
                            <th>Cliente</th>                        
                            <th>Tipo</th>
                            <th>Fecha</th>
                            <th>Vencimiento</th>
                            <th>Importe</th>
                            <th>Observaciones</th>
                            <th></th>
                        </thead>
                        <tbody id="myTable">
                        @foreach ($contratos as $contrato)
                        <tr>
                            <td>{{ $contrato->razonSocial }} </td>
                            <td>{{ $contrato->codTipoContrato }} </td>
                            <td>{{ date('d - m - Y', strtotime($contrato->FechaContrato)) }} </td>
                            <td>{{ date('d - m - Y', strtotime($contrato->Vencimiento)) }} </td>
                            <td>{{ $contrato->importeCto }} </td>                            
                            <td>{{ $contrato->observaciones }} </td>
                            <td><a class="btn btn-sm btn-success" href="{!! url('contratos/europa/'.$contrato->codContrato) !!}"><i class="fa fa-edit"></i></a></td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    
                </div>

                 <div class="col-md-12 text-center">
      <ul class="pagination pagination-lg pager" id="myPager"></ul>
      </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

 $('.datepicker').datepicker({
        format: "dd/mm/yyyy",
        dateFormat: 'yy-mm-dd',
        language: "es",
        autoclose: true
    });
    
    $('#search').on('keyup',function(){
        $value=$(this).val();
        //alert($value);
        $("#loading").show();
        setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/search_europa')}}',
                data : {'search':$value},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('tbody').html(data);
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);
});

$( "#filtrar" ).click(function() {
$fecha_alta_inicio = $("#fecha_alta_inicio").val();
$fecha_alta_fin = $("#fecha_alta_fin").val();

$fecha_baja_inicio = $("#fecha_baja_inicio").val();
$fecha_baja_fin = $("#fecha_baja_fin").val();

$fecha_venc_inicio = $("#fecha_venc_inicio").val();
$fecha_venc_fin = $("#fecha_venc_fin").val();

$codContrato = $("#codContrato").val();
$importe = $("#importe").val();
$descuento = $("#descuento").val();
$comision = $("#comision").val();

$otrocomercial = 0;
$pendienteforma = 0;

if($("#otrocomercial").prop("checked")){
$otrocomercial = 1;
}

if($("#pendienteforma").prop("checked")){
$pendienteforma = 1;
}


$contratante = $("#contratante option:selected").val();

$empresa =  $("#empresa option:selected").val();

$comercial =  $("#comercial option:selected").val();

$matricula = $("#matricula option:selected").text();

$tipo_anexo = $("#tipo_anexo option:selected").text();

$tipo_abono = $("#tipo_abono option:selected").val();


$fecha_altaanexo_inicio = $("#fecha_baja_inicio").val();
$fecha_altaanexo_fin = $("#fecha_baja_fin").val();

$alta_inicio = $fecha_alta_inicio.split("/").reverse().join("-");
$alta_fin = $fecha_alta_fin.split("/").reverse().join("-");

$baja_inicio = $fecha_baja_inicio.split("/").reverse().join("-");
$baja_fin = $fecha_venc_fin.split("/").reverse().join("-");

$venc_inicio = $fecha_venc_inicio.split("/").reverse().join("-");
$venc_fin = $fecha_baja_fin.split("/").reverse().join("-");

$altaanexo_inicio = $fecha_venc_inicio.split("/").reverse().join("-");
$altaanexo_fin = $fecha_venc_fin.split("/").reverse().join("-");


//alert($inicio);

        $dialog = bootbox.dialog({
                        message: '<p class="text-center">Cargando datos, espere por favor...</p>',
                        closeButton: false
                    });

    setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/filtra_europa')}}',
                data : {'alta_inicio':$alta_inicio,'alta_fin':$alta_fin,'baja_inicio':$baja_inicio,'baja_fin':$baja_fin,'venc_inicio':$venc_inicio,'venc_fin':$venc_fin,'altaanexo_inicio':$altaanexo_inicio,'altaanexo_fin':$altaanexo_fin,'contratante':$contratante,'empresa':$empresa,'matricula':$matricula,'tipo_anexo':$tipo_anexo,'tipo_abono':$tipo_abono,'comercial':$comercial,'otrocomercial':$otrocomercial,'pendienteforma':$pendienteforma,'codContrato':$codContrato,'importe':$importe,'descuento':$descuento,'comision':$comision},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('tbody').html(data);

                    // do something in the background
                    $dialog.modal('hide');
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

});

$( "#excel" ).click(function() {
$fecha_alta_inicio = $("#fecha_alta_inicio").val();
$fecha_alta_fin = $("#fecha_alta_fin").val();

$fecha_baja_inicio = $("#fecha_baja_inicio").val();
$fecha_baja_fin = $("#fecha_baja_fin").val();

$fecha_venc_inicio = $("#fecha_venc_inicio").val();
$fecha_venc_fin = $("#fecha_venc_fin").val();

$codContrato = $("#codContrato").val();
$importe = $("#importe").val();
$descuento = $("#descuento").val();
$comision = $("#comision").val();

$otrocomercial = 0;
$pendienteforma = 0;

if($("#otrocomercial").prop("checked")){
$otrocomercial = 1;
}

if($("#pendienteforma").prop("checked")){
$pendienteforma = 1;
}


$contratante = $("#contratante option:selected").val();

$empresa =  $("#empresa option:selected").val();

$comercial =  $("#comercial option:selected").val();

$matricula = $("#matricula option:selected").text();

$tipo_anexo = $("#tipo_anexo option:selected").text();

$tipo_abono = $("#tipo_abono option:selected").val();


$fecha_altaanexo_inicio = $("#fecha_baja_inicio").val();
$fecha_altaanexo_fin = $("#fecha_baja_fin").val();

$alta_inicio = $fecha_alta_inicio.split("/").reverse().join("-");
$alta_fin = $fecha_alta_fin.split("/").reverse().join("-");

$baja_inicio = $fecha_baja_inicio.split("/").reverse().join("-");
$baja_fin = $fecha_venc_fin.split("/").reverse().join("-");

$venc_inicio = $fecha_venc_inicio.split("/").reverse().join("-");
$venc_fin = $fecha_baja_fin.split("/").reverse().join("-");

$altaanexo_inicio = $fecha_venc_inicio.split("/").reverse().join("-");
$altaanexo_fin = $fecha_venc_fin.split("/").reverse().join("-");


//alert($inicio);

        $dialog = bootbox.dialog({
                        message: '<p class="text-center">Generando Excel, espere por favor...</p>',
                        closeButton: false
                    });

    setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/exportar_excel_europa')}}',
                data : {'alta_inicio':$alta_inicio,'alta_fin':$alta_fin,'baja_inicio':$baja_inicio,'baja_fin':$baja_fin,'venc_inicio':$venc_inicio,'venc_fin':$venc_fin,'altaanexo_inicio':$altaanexo_inicio,'altaanexo_fin':$altaanexo_fin,'contratante':$contratante,'empresa':$empresa,'matricula':$matricula,'tipo_anexo':$tipo_anexo,'tipo_abono':$tipo_abono,'comercial':$comercial,'otrocomercial':$otrocomercial,'pendienteforma':$pendienteforma,'codContrato':$codContrato,'importe':$importe,'descuento':$descuento,'comision':$comision},            
                success : function(data){
                    window.location = this.url;
                    $dialog.modal('hide');
                },
                error : function(data){
                    bootbox.alert("Ha ocurrido un error generando su Excel.");
                    $dialog.modal('hide');
                }
            });
        }, 500);

});


 $("#nueva_franquicia").click(function(){


      setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('contratos/europa/mostrar_nueva_franquicia')}}',
                        data : {},
                        success : function(data){
                           var dialog = bootbox.dialog({
                                    message: data,
                                    size: "large",
                                    title: "Crear franquicias",
                                    buttons: {
                                        success: {
                                            label: 'Guardar',
                                             callback: function() {
                                                dialog.modal('hide');
                                                 }
                                        }
                                    }
                                });
                             $('.selectpicker').selectpicker('refresh');
                             $('.selectpicker').selectpicker('render');
                             
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);

});

 $("#ver_renovaciones").click(function(){

    var loading = bootbox.dialog({
    message: '<p class="text-center"><p><i class="fa fa-spin fa-spinner"></i> Cargando interfaz, espere por favor...</p>',
    closeButton: false
});
// do something in the background


      setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('contratos/europa/ver_renovaciones')}}',
                        data : {},
                        success : function(data){
                            loading.modal('hide');
                           var dialog = bootbox.dialog({
                                    message: data,
                                    title: "Renovación de contratos",
                                    buttons: {
                                        success: {
                                            label: 'aceptar',
                                             callback: function() {
                                                $contrato = $("#contrato_origen option:selected").val();
                                                $tipo = $('input[name=tipofac]:checked').val();
                                                $fechaemision = $("#fechaemision").val();
                                                $iva = $("#ivaporcentaje").val();
                                                $origen = "";
                                                if($('#factura_origen').length){
                                                    $origen = $('#factura_origen option:selected').val();
                                                }
                                                $notas = $("#notas_factura").val();
                                                generar_factura_decontrato($contrato,$tipo,$origen,$fechaemision,$notas,$iva);
                                                dialog.modal('hide');
                                                 }
                                        }
                                    }
                                });
                             $('.selectpicker').selectpicker('refresh');
                             $('.selectpicker').selectpicker('render');
                             $('.datepicker').datepicker({
                                format: "dd/mm/yyyy",
                                dateFormat: 'yy-mm-dd',
                                language: "es",
                                autoclose: true
                            });
                             
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);

});

</script>
@endsection