@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xl-12 col-md-offset-0">
            <div class="panel panel-default">
                <div class="row"> 
                    <div class="panel-heading" id="sanciones-header">                    
                        <div class="col-md-8">
                        <h3 class="module-title">{{$franquicia->Tarifa}}</h3>
                        </div>
                        <div class="col-md-4">
                            <div>
                                <ul class="nav navbar-nav">
                                    <li><a href="#" id="btn-save"><i class="fa fa-save"> </i> Guardar</a></li>                                 
                                    <li><a href="#" id="volver"><i class="fa fa-arrow-left"> </i> Volver</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="panel-body" style="border-top:2px solid #335599;">                        
                    <form class="inline-form" id="form_sancion">
                            <div class="form-group col-md-2">
                           <label for="matricula">Codigo</label>
                            <input type="text" class="form-control" name="codigo" id="codigo" value="{{$franquicia->CodTarifa}}" disabled>
                            <input type="hidden" class="form-control" name="codTarifa" id="codTarifa" value="{{$franquicia->CodTarifa}}">   
                        </div>
                        <div class="form-group col-md-3">
                           <label for="matricula">Nombre</label>
                            <input type="text" class="form-control" name="Tarifa" id="Tarifa" value="{{$franquicia->Tarifa}}">  
                        </div>
                        <div class="form-group col-md-3">
                            <label for="matricula">Empresa Comercial</label>
                            <select class="selectpicker" data-live-search="true" title="Buscar..." id="contratante" name="contratante">
                             @if($franquicia->codEmpresa == 10)
                            <option value="10" selected>EUROPA ITS SOLUCIONES S.L.</option>  
                            <option value="9">MERCURIO ASSISTANCE</option>
                            @else
                            <option value="10">EUROPA ITS SOLUCIONES S.L.</option>  
                            <option value="9" selected>MERCURIO ASSISTANCE</option>
                            @endif
                            </select> 
                        </div>
                            <div class="form-group col-md-2">
                            <label for="matricula">Tipo contrato</label>
                            <select class="selectpicker" data-live-search="true" title="Buscar..." id="tipo_contrato" name="tipo_contrato">
                            @if($franquicia->CodTipoContrato== "AJ")
                            <option value="AJ" selected>MULTAS</option>  
                            <option value="AM">ASISTENCIA</option>
                            @else
                             <option value="AJ">MULTAS</option>  
                            <option value="AM" selected >ASISTENCIA</option>
                            @endif
                            </select> 
                        </div>
                          <div class="col-md-9">
                          <h3>Condiciones</h3>
                           <div class="form-group col-md-3">
                           <label for="matricula">Fecha Efecto</label>
                            <input type="text" class="form-control" name="fechaEfecto" value="{{ date('d/m/Y', strtotime($condiciones->FechaEfecto)) }}">
                            <input type="hidden" name="codCondiciones" id="codCondiciones" value="{{$condiciones->codCondiciones}}">       
                        </div>
                          <div class="form-group col-md-2">
                           <label for="matricula">% Comisión</label>
                            <input type="number" class="form-control" name="porcentajeComision" id= value="{{$condiciones->porcentajeComision}}">
                                     
                        </div>
                         <div class="form-group col-md-4">
                  
                            <label for="acuerdo">% Com.Específica</label>
                            <input type="number" class="form-control" name="pctjeComisionExcep" value="{{$condiciones->pctjeComisionExcep}}">   
                                               
                        </div>
                         <div class="form-group col-md-2">
                         
                            <label for="ft">% Deducción</label>
                            <input type="number" class="form-control" name="porcentajeDeduccion" value="{{$condiciones->porcentajeDeduccion}}">      
                                               
                        </div>
                        </div>              
                        </div>     
                    </form>
                    </div>
                       <div class="panel panel-default">
              <div class="row"> 
                    <div class="panel-heading" id="detalles-header">                    
                        <div class="col-md-6">
                            <h3 class="module-title">Detalles</h3>
                        </div>
                        <div class="col-md-6">
                            <div>
                                <ul class="nav navbar-nav">
                                    <li class="detalles"><a id="precios"><i class="fa fa-phone" aria-hidden="true"></i> Lista de precios</a></li>
                                    <li class="detalles"><a id= "comerciales"><i class="fa fa-car" aria-hidden="true"></i> Comerciales asociados</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div id="error" style="display:none" class="alert alert-danger alert-dismissible fade in" role="alert"></div>
                    <div id="success" style="display:none" class="alert alert-success alert-dismissible"></div>
                     <div class="table-responsive" id="tabla_contenido">
                </div>
                    </div>                        
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

$("#volver").click(function(){
        window.history.go(-1); return true;
    });


$(function() {

  $codCondiciones =  $("input[name=codCondiciones]").val();
  $tipoContrato =  $("#tipo_contrato option:selected").val();
  setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/europa/franquicias/precios')}}',
                data : {'codCondiciones':$codCondiciones,'tipoContrato':$tipoContrato},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('#tabla_contenido').html(data);
                    $('.selectpicker').selectpicker('refresh');
                    $('.selectpicker').selectpicker('render');
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);
    });

$("#precios").click(function(){

         var dialog = bootbox.dialog({
    message: '<p class="text-center">Cargando datos...</p>',
    closeButton: false
});

        $("#error").hide();
    $("#success").hide();
    $("#success").empty();
 
    $codCondiciones =  $("input[name=codCondiciones]").val();
  $tipoContrato =  $("#tipo_contrato option:selected").val();
  setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/europa/franquicias/precios')}}',
                data : {'codCondiciones':$codCondiciones,'tipoContrato':$tipoContrato},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('#tabla_contenido').html(data);
                    $('.selectpicker').selectpicker('refresh');
                    $('.selectpicker').selectpicker('render');
                    $("#loading").hide();
                     dialog.modal('hide');
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                     dialog.modal('hide');
                }
            });
        }, 500);
    });

$("#comerciales").click(function(){

         var dialog = bootbox.dialog({
    message: '<p class="text-center">Cargando datos...</p>',
    closeButton: false
});

        $("#error").hide();
    $("#success").hide();
    $("#success").empty();
 
    $codTarifa =  $("#codTarifa").val();
     $tipoContrato =  $("#tipo_contrato option:selected").val();
  setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/europa/franquicias/comerciales')}}',
                data : {'CodTarifa':$codTarifa,'tipoContrato':$tipoContrato},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('#tabla_contenido').html(data);
                    $('.selectpicker').selectpicker('refresh');
                    $('.selectpicker').selectpicker('render');
                    $("#loading").hide();
                     dialog.modal('hide');
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                     dialog.modal('hide');
                }
            });
        }, 500);
    });


    $("#volver").click(function(){
        window.history.go(-1); return false;
    });
     $(document).on('focusin','.datepicker',function(){
         $(this).datepicker({
        format: "dd/mm/yyyy",
        dateFormat: 'yy-mm-dd',
        language: "es",
        autoclose: true
    });

         $(this).selectpicker("data-live-search","true");

    });


 $("#btn-save").click(function(){
    
    $form = $("#form_sancion").serialize();
  
     setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '/contratos/europa/franquicias/guardar',
                data : {'datos' : $("#form_sancion").serialize()},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    if(data.includes("error")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                //location.reload();
                            }
                        });
                    }else{
                         bootbox.alert({
                            message: data,
                            callback: function () {
                                 location.reload();
                            }
                        });
                    }
                   
                    $("#loading").hide();

                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);



});

  $(document).on('click', '.editar_telefono' ,function(){
    $action = 1;
$CodContacto =  $("input[name=codContacto]").val();
$telefono = $(this).closest("tr").find('.telefono').val();
$antiguo = $(this).closest("tr").find('.antiguo').val();
$tipo = $(this).closest("tr").find('.tipo option:selected').val();

if($tipo == "buscar..." || $telefono == ""){
    $("#error").empty();
    $("#error").append("<i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Se han encontrado errores.</strong> El campo del tipo o del teléfono no puede estar vacío.");
    $("#error").show();
}else{
    $("#error").hide();
    $("#success").hide();
    $("#success").empty();

     setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/europa/contactos/guardar_europa')}}',
                data : {'action':$action,'CodContacto':$CodContacto,'telefono':$telefono,'tipo':$tipo,'antiguo':$antiguo},
                success : function(data){
                    console.log(JSON.stringify(data));
                    $("#success").append(data);
                    $("#success").show();
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

     $("#telefono")[0].click();
}
  });


$(document).on('click', '.eliminar_precio' ,function(){
    $action = 1;
$codTarifa =  $("input[name=codTarifa]").val();
$codCondiciones = $(this).closest("tr").find('.codCondiciones').val();
$CodTipoAnexo = $(this).closest("tr").find('.tipoAnexo option:selected').val();


bootbox.confirm({
    title: "Eliminar registro",
    message: "¿Está seguro que desea eliminar el registro?. Tenga en cuenta que el proceso es irreversible",
    buttons: {
        cancel: {
            label: '<i class="fa fa-times"></i> Cancelar'
        },
        confirm: {
            label: '<i class="fa fa-check"></i> Aceptar'
        }
    },
    callback: function (result) {
        
        if(result){

           var dialog = bootbox.dialog({
    message: '<p class="text-center">Eliminando...</p>',
    closeButton: false
});

           setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/europa/franquicias/eliminar_franquicia')}}',
                data : {'action':$action,'codTarifa':$codTarifa,'codCondiciones':$codCondiciones,'CodTipoAnexo':$CodTipoAnexo},
                success : function(data){
                    console.log(JSON.stringify(data));
                    $("#success").append(data);
                    $("#success").show();
                    $("#loading").hide();
                    dialog.modal('hide');
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                    dialog.modal('hide');
                }
            });
        }, 500);

     $("#precios")[0].click();

        }
    }
});


 


  });

$(document).on('click', '.eliminar_comercial' ,function(){

$action = 2;
$CodComercial = $(this).closest("tr").find('.CodComercial').val();
$codTarifa =  $("input[name=codTarifa]").val();

bootbox.confirm({
    title: "Eliminar registro",
    message: "¿Está seguro que desea eliminar el registro?. Tenga en cuenta que el proceso es irreversible.",
    buttons: {
        cancel: {
            label: '<i class="fa fa-times"></i> Cancelar'
        },
        confirm: {
            label: '<i class="fa fa-check"></i> Aceptar'
        }
    },
    callback: function (result) {
        
        if(result){

           var dialog = bootbox.dialog({
    message: '<p class="text-center">Eliminando...</p>',
    closeButton: false
});
         setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/europa/franquicias/eliminar_franquicia')}}',
                data : {'action':$action,'CodComercial':$CodComercial,'codTarifa':$codTarifa},
                success : function(data){
                    console.log(JSON.stringify(data));
                    $("#success").append(data);
                    $("#success").show();
                    $("#loading").hide();
                    dialog.modal('hide');
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                    dialog.modal('hide');
                }
            });
        }, 500);

     $("#comerciales")[0].click();

        }
    }
  });

});


 $(document).on('click', '.editar_precio' ,function(){

$codTarifa =  $("input[name=codTarifa]").val();
$codCondiciones = $('#codCondiciones').val();
$CodTipoAnexo = $(this).closest("tr").find('.tipoAnexo option:selected').val();
$precioMinimoVenta = $(this).closest("tr").find('.precioMinimoVenta').val();
$comisionRenovacion = $(this).closest("tr").find('.comisionRenovacion').val();
$precioMinimoRenovacion = $(this).closest("tr").find('.precioMinimoRenovacion').val();

if($(this).closest("tr").find('.facturar').prop("checked")){
$activar = 1;
}


if($CodTipoAnexo == "buscar..." || $precioMinimoVenta == ""){
    $("#error").empty();
    $("#error").append("<i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> El campo del tipo de anexo o del precio mínimo no puede estar vacío.");
    $("#error").show();
}else{
    $("#error").hide();
    $("#success").hide();
    $("#success").empty();
     var dialog = bootbox.dialog({
    message: '<p class="text-center">Editando...</p>',
    closeButton: false
});

     setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/europa/franquicias/update_precio')}}',
                data : {'codTarifa':$codTarifa,'codCondiciones':$codCondiciones,'CodTipoAnexo':$CodTipoAnexo,'precioMinimoVenta':$precioMinimoVenta,'comisionRenovacion':$comisionRenovacion,'precioMinimoRenovacion':$precioMinimoRenovacion},
                success : function(data){
                    console.log(JSON.stringify(data));
                    $("#success").append(data);
                    $("#success").show();
                    $("#loading").hide();
                    dialog.modal('hide');
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                    dialog.modal('hide');
                }
            });
        }, 500);

     $("#precios")[0].click();
}
});




 $(document).on('click', '#add_precio' ,function(){

$action = 1;
$codTarifa =  $("input[name=codTarifa]").val();
$codCondiciones = $('#codCondiciones').val();
$CodTipoAnexo = $(this).closest("tr").find('.tipoAnexo option:selected').val();
$precioMinimoVenta = $(this).closest("tr").find('.precioMinimoVenta').val();
$comisionRenovacion = $(this).closest("tr").find('.comisionRenovacion').val();
$precioMinimoRenovacion = $(this).closest("tr").find('.precioMinimoRenovacion').val();

if($CodTipoAnexo == "buscar..." || $precioMinimoVenta == ""){
    $("#error").empty();
    $("#error").append("<i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Se han encontrado errores.</strong> El campo del tipo de anexo o del precio mínimo no puede estar vacío.");
    $("#error").show();
}else{
    $("#error").hide();
    $("#success").hide();
    $("#success").empty();

    var dialog = bootbox.dialog({
    message: '<p class="text-center">Añadiendo teléfono</p>',
    closeButton: false
});

     setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/europa/franquicias/insertar_precio')}}',
                data : {'action':$action,'codTarifa':$codTarifa,'codCondiciones':$codCondiciones,'CodTipoAnexo':$CodTipoAnexo,'precioMinimoVenta':$precioMinimoVenta,'comisionRenovacion':$comisionRenovacion,'precioMinimoRenovacion':$precioMinimoRenovacion},
                success : function(data){
                    console.log(JSON.stringify(data));
                    $("#success").append(data);
                    $("#success").show();
                    $("#loading").hide();
                    dialog.modal('hide');
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                     $("#error").append("<i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong>Error de base de datos</strong> Intentelo dentro de unos minutos.");
                    dialog.modal('hide');
                }
            });
        }, 500);

     $("#precios")[0].click();
}
});

  $(document).on('click', '#add_comercial' ,function(){

$action = 2;
$codTarifa =  $("input[name=codTarifa]").val();
$CodComercial = $(this).closest("tr").find('.CodComercial option:selected').val();
$fechaAplicacion = $(this).closest("tr").find('.fechaAplicacion').val();

if($CodComercial == "buscar..." || $fechaAplicacion == ""){
    $("#error").empty();
    $("#error").append("<i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Se han encontrado errores.</strong> El campo del comercial o de fecha no puede estar vacío.");
    $("#error").show();
}else{
    $("#error").hide();
    $("#success").hide();
    $("#success").empty();

    var dialog = bootbox.dialog({
    message: '<p class="text-center">Añadiendo teléfono</p>',
    closeButton: false
});

     setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/europa/franquicias/insertar_comercial')}}',
                data : {'action':$action,'codTarifa':$codTarifa,'CodComercial':$CodComercial,'fechaAplicacion':$fechaAplicacion},
                success : function(data){
                    console.log(JSON.stringify(data));
                    $("#success").append(data);
                    $("#success").show();
                    $("#loading").hide();
                    dialog.modal('hide');
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                     $("#error").append("<i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong>Error de base de datos</strong> Intentelo dentro de unos minutos.");
                    dialog.modal('hide');
                }
            });
        }, 500);

     $("#comerciales")[0].click();
}
});

  $(document).on('click', '#guardar_direccion' ,function(){

$action = 2;
$CodContacto =  $("input[name=codContacto]").val();
$direccion = $(this).closest("tr").find('.direccion').val();
$poblacion = $(this).closest("tr").find('.poblacion').val();
$cp = $(this).closest("tr").find('.codPostal').val();
$pais = $(this).closest("tr").find('.pais option:selected').val();
$provincia = $(this).closest("tr").find('.provincia option:selected').val();
$apartadoCorreos =$(this).closest("tr").find('.apartadoCorreos').val();
$facturarA = $(this).closest("tr").find('.contacto option:selected').val();
$activar = 0;

if($(this).closest("tr").find('.facturar').prop("checked")){
$activar = 1;
}


if($pais == "buscar..." || $cp == ""){
    $("#error").empty();
    $("#error").append("<i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Se han encontrado errores.</strong> El campo del pais o del Código Postal no puede estar vacío.");
    $("#error").show();
}else{
    $("#error").hide();
    $("#success").hide();
    $("#success").empty();

    var dialog = bootbox.dialog({
    message: '<p class="text-center">Añadiendo dirección</p>',
    closeButton: false
});

     setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/europa/contactos/insertar_europa')}}',
                data : {'action':$action,'CodContacto':$CodContacto,'direccion':$direccion,'poblacion':$poblacion,'cp':$cp,'pais':$pais,'provincia':$provincia,'apartadoCorreos':$apartadoCorreos,'facturarA':$facturarA,'activar':$activar},
                success : function(data){
                    console.log(JSON.stringify(data));
                    $("#success").append(data);
                    $("#success").show();
                    $("#loading").hide();
                      dialog.modal('hide');
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                      dialog.modal('hide');
                }
            });
        }, 500);

     $("#direcciones")[0].click();
}
});


  $(document).on('click', '#add_vinculacion' ,function(){

$action = 3;
$CodContacto =  $("input[name=codContacto]").val();
$categoria = $(this).closest("tr").find('.categoria option:selected').val();
$vinculacion = $(this).closest("tr").find('.vinculacion option:selected').val();

if($categoria == "buscar..." || $vinculacion == "buscar..."){
    $("#error").empty();
    $("#error").append("<i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Se han encontrado errores.</strong> El campo de categoria o vinculación no pueden estar vacíos.");
    $("#error").show();
}else{
    $("#error").hide();
    $("#success").hide();
    $("#success").empty();

     var dialog = bootbox.dialog({
    message: '<p class="text-center">Añadiendo función</p>',
    closeButton: false
});

     setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/europa/contactos/insertar_europa')}}',
                data : {'action':$action,'CodContacto':$CodContacto,'categoria':$categoria,'vinculacion':$vinculacion},
                success : function(data){
                    console.log(JSON.stringify(data));
                    $("#success").append(data);
                    $("#success").show();
                    $("#loading").hide();
                    dialog.modal('hide');
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                    dialog.modal('hide');
                }
            });
        }, 500);

     $("#funciones")[0].click();
}
});
</script>
@endsection