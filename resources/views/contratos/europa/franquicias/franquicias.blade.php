@extends('layouts.app')
@section('content')

<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="row"> 
                    <div class="panel-heading" id="sanciones-header">                    
                        <div class="col-md-2" style="width: 100%">
                            <h3 class="module-title">Franquicias Europa</h3>
                        </div>
                        <div class="col-md-10">
                            <div>
                                <ul class="list-inline nav navbar-nav">
                                    <li><a href="{{url('contratos/europa/franquicias/nuevo')}}"><i class="fa fa-plus"> </i> Nueva</a></li>
                                   <li><a class="dropdown-item" href="{{url('contratos/europa/tipos_anexo')}}" id="newtipo"><i class="fa fa-list-alt" aria-hidden="true"></i> Tipos de Anexos</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table-responsive"">
                    <table class="table">
                        <thead class="table-header">
                            <th>Nombre franquicia</th>                        
                            <th>Empresa comercial</th>
                            <th>Tipo contrato</th>
                            <th>Comercial</th>
                            <th>Fecha efecto</th>
                            <th></th>
                        </thead>
                        <tbody id="myTable">
                        @foreach ($franquicias as $franquicia)
                        <tr>
                            <td>{{ $franquicia->Tarifa }} </td>
                            <td>{{ $franquicia->razonSocial }} </td>
                            <td>{{ $franquicia->tipoServicio }} </td>
                            <td>{{ $franquicia->comercial }} </td>
                            <td>{{ date('d - m - Y', strtotime($franquicia->fechaAplicacion)) }}</td>                            
                            <td><a class="btn btn-sm btn-success" href="{!! url('contratos/europa/franquicias/'.$franquicia->CodTarifa) !!}"><i class="fa fa-edit"></i></a></td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    
                </div>

                 <div class="col-md-12 text-center">
      <ul class="pagination pagination-lg pager" id="myPager"></ul>
      </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

 $('.datepicker').datepicker({
        format: "dd/mm/yyyy",
        dateFormat: 'yy-mm-dd',
        language: "es",
        autoclose: true
    });
    
    $('#search').on('keyup',function(){
        $value=$(this).val();
        //alert($value);
        $("#loading").show();
        setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/search_europa')}}',
                data : {'search':$value},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('tbody').html(data);
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);
});

$( "#filtrar" ).click(function() {
$fecha_alta_inicio = $("#fecha_alta_inicio").val();
$fecha_alta_fin = $("#fecha_alta_fin").val();

$fecha_baja_inicio = $("#fecha_baja_inicio").val();
$fecha_baja_fin = $("#fecha_baja_fin").val();

$fecha_venc_inicio = $("#fecha_venc_inicio").val();
$fecha_venc_fin = $("#fecha_venc_fin").val();

$codContrato = $("#codContrato").val();
$importe = $("#importe").val();
$descuento = $("#descuento").val();
$comision = $("#comision").val();

$otrocomercial = 0;
$pendienteforma = 0;

if($("#otrocomercial").prop("checked")){
$otrocomercial = 1;
}

if($("#pendienteforma").prop("checked")){
$pendienteforma = 1;
}


$contratante = $("#contratante option:selected").val();

$empresa =  $("#empresa option:selected").val();

$comercial =  $("#comercial option:selected").val();

$matricula = $("#matricula option:selected").text();

$tipo_anexo = $("#tipo_anexo option:selected").text();

$tipo_abono = $("#tipo_abono option:selected").val();


$fecha_altaanexo_inicio = $("#fecha_baja_inicio").val();
$fecha_altaanexo_fin = $("#fecha_baja_fin").val();

$alta_inicio = $fecha_alta_inicio.split("/").reverse().join("-");
$alta_fin = $fecha_alta_fin.split("/").reverse().join("-");

$baja_inicio = $fecha_baja_inicio.split("/").reverse().join("-");
$baja_fin = $fecha_venc_fin.split("/").reverse().join("-");

$venc_inicio = $fecha_venc_inicio.split("/").reverse().join("-");
$venc_fin = $fecha_baja_fin.split("/").reverse().join("-");

$altaanexo_inicio = $fecha_venc_inicio.split("/").reverse().join("-");
$altaanexo_fin = $fecha_venc_fin.split("/").reverse().join("-");


//alert($inicio);

        $dialog = bootbox.dialog({
                        message: '<p class="text-center">Cargando datos, espere por favor...</p>',
                        closeButton: false
                    });

    setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/filtra_europa')}}',
                data : {'alta_inicio':$alta_inicio,'alta_fin':$alta_fin,'baja_inicio':$baja_inicio,'baja_fin':$baja_fin,'venc_inicio':$venc_inicio,'venc_fin':$venc_fin,'altaanexo_inicio':$altaanexo_inicio,'altaanexo_fin':$altaanexo_fin,'contratante':$contratante,'empresa':$empresa,'matricula':$matricula,'tipo_anexo':$tipo_anexo,'tipo_abono':$tipo_abono,'comercial':$comercial,'otrocomercial':$otrocomercial,'pendienteforma':$pendienteforma,'codContrato':$codContrato,'importe':$importe,'descuento':$descuento,'comision':$comision},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('tbody').html(data);

                    // do something in the background
                    $dialog.modal('hide');
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

});


 $("#nueva_franquicia").click(function(){


      setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('contratos/europa/mostrar_nueva_franquicia')}}',
                        data : {},
                        success : function(data){
                           var dialog = bootbox.dialog({
                                    message: data,
                                    size: "large",
                                    title: "Crear franquicias",
                                    buttons: {
                                        success: {
                                            label: 'Guardar',
                                             callback: function() {
                                                dialog.modal('hide');
                                                 }
                                        }
                                    }
                                });
                             $('.selectpicker').selectpicker('refresh');
                             $('.selectpicker').selectpicker('render');
                             
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);

});

</script>
@endsection