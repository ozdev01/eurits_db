@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xl-12 col-md-offset-0">
            <div class="panel panel-default">
                <div class="row"> 
                    <div class="panel-heading" id="sanciones-header">                    
                        <div class="col-md-8">
                        <h3 class="module-title">Nueva Franquicia</h3>
                        </div>
                        <div class="col-md-4">
                            <div>
                                <ul class="nav navbar-nav">
                                    <li><a href="#" id="btn-save"><i class="fa fa-save"> </i> Guardar</a></li>                                 
                                    <li><a href="#" id="volver"><i class="fa fa-arrow-left"> </i> Volver</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="panel-body" style="border-top:2px solid #335599;">                        
                    <form class="inline-form" id="form_sancion">
                        <div class="form-group col-md-3">
                           <label for="matricula">Nombre</label>
                            <input type="text" class="form-control" name="Tarifa" id="Tarifa" value="">  
                        </div>
                        <div class="form-group col-md-3">
                            <label for="matricula">Empresa Comercial</label>
                            <select class="selectpicker" data-live-search="true" title="Buscar..." id="contratante" name="contratante">
                            <option value="10">EUROPA ITS SOLUCIONES S.L.</option>  
                            <option value="9">MERCURIO ASSISTANCE</option>
                          
                            </select> 
                        </div>
                            <div class="form-group col-md-2">
                            <label for="matricula">Tipo contrato</label>
                            <select class="selectpicker" data-live-search="true" title="Buscar..." id="tipo_contrato" name="tipo_contrato">
                             <option value="AJ">MULTAS</option>  
                            <option value="AM"  >ASISTENCIA</option>
                            </select> 
                        </div>
                          <div class="col-md-9">
                          <h3>Condiciones</h3>
                           <div class="form-group col-md-3">
                           <label for="matricula">Fecha Efecto</label>
                            <input type="text" class="form-control datepicker" name="fechaEfecto" value="">     
                        </div>
                          <div class="form-group col-md-2">
                           <label for="matricula">% Comisión</label>
                            <input type="number" class="form-control" name="porcentajeComision" id= value="">
                                     
                        </div>
                         <div class="form-group col-md-4">
                  
                            <label for="acuerdo">% Com.Específica</label>
                            <input type="number" class="form-control" name="pctjeComisionExcep" value="">   
                                               
                        </div>
                         <div class="form-group col-md-2">
                         
                            <label for="ft">% Deducción</label>
                            <input type="number" class="form-control" name="porcentajeDeduccion" value="">      
                                               
                        </div>
                        </div>              
                        </div>     
                    </form>
                    </div>                       
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

 $("#volver").click(function(){
        window.history.go(-1); return false;
    });
     $(document).on('focusin','.datepicker',function(){
         $(this).datepicker({
        format: "dd/mm/yyyy",
        dateFormat: 'yy-mm-dd',
        language: "es",
        autoclose: true
    });

         $(this).selectpicker("data-live-search","true");

    });


 $("#btn-save").click(function(){
    
    $form = $("#form_sancion").serialize();
  
     setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/europa/franquicias/store')}}',
                data : {'datos' : $("#form_sancion").serialize()},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    if(data.includes("SQLSTATE")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                //location.reload();
                            }
                        });
                    }else{
                         bootbox.alert({
                            message: "La franquicia se ha creado correctamente",
                            callback: function () {
                                 window.location = '/contratos/europa/franquicias/'+data;
                            }
                        });
                    }
                   
                    $("#loading").hide();

                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);



});


</script>
@endsection