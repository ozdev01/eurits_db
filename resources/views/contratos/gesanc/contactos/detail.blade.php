@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xl-12 col-md-offset-0">
            <div class="panel panel-default">
                <div class="row"> 
                    <div class="panel-heading" id="sanciones-header">                    
                        <div class="col-md-8">
                        <h3 class="module-title">{{$contacto->empresa}}</h3>
                             @if($contacto->impagado_pdteDev == 1)
                          <label style="background:blue;color:white">Impagado</label>
                           @endif
                           @if($contacto->cteConflictivo == 1)
                          <label style="background:red;color:white">Conflictivo</label>
                           @endif
                        </div>
                        <div class="col-md-4">
                            <div>
                                <ul class="nav navbar-nav">
                                    <li><a href="#" id="btn-save"><i class="fa fa-save"> </i> Guardar</a></li>
                                    <li><a href="#" id="eliminar"><i class="fa fa-trash"> </i> Eliminar</a></li>                                 
                                    <li><a href="#" id="volver"><i class="fa fa-arrow-left"> </i> Volver</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="panel-body" style="border-top:2px solid #335599;">                        
                    <form class="inline-form" id="form_sancion">
                      <div class="form-group col-md-2">
                           <label for="matricula">Cod. Contacto</label>
                            <input type="text" class="form-control" name="codigo" value="{{$contacto->CODCLI}}" disabled>
                            <input type="hidden" class="form-control" name="codcli" id="codcli" value="{{$contacto->CODCLI}}">
                                     
                        </div>
                         <div class="form-group col-md-2">
                            <label for="matricula">CIF</label>
                    
                           <input type="text" class="form-control" name="cif" id="cif" value="{{$contacto->cif}}">
                                    
                        </div>
                             <div class="form-group col-md-2">
                            <label for="matricula">PWD</label>
                    
                           <input type="text" class="form-control" name="pwd" value="{{$contacto->PWD}}">
                                    
                        </div>
                        <div class="form-group col-md-3">
                           <label for="matricula">Empresa</label>
                            <input type="text" class="form-control" name="empresa" value="{{$contacto->empresa}}">
                                     
                        </div>
                         <div class="form-group col-md-4">
                            <label for="acuerdo">Dirección</label>
                            <input type="text" class="form-control" name="direccion" value="{{$contacto->direccion}}">   
                                               
                        </div>
                         <div class="form-group col-md-2">
                            <label for="ft">CP</label>
                            <input type="text" class="form-control" name="cp" value="{{$contacto->cp}}">      
                                               
                        </div>
                          <div class="form-group col-md-2">
                         
                            <label for="ft">apdo. Correo</label>
                            <input type="text" class="form-control" name="apartadoCorreos" value="{{$contacto->apartadoCorreos}}">      
                                               
                        </div>
                      <div class="form-group col-md-2">
                            <label for="matricula">Población</label>
                           <input type="text" class="form-control" name="poblacion" value="{{$contacto->poblacion}}">  
                         
                        </div>
                         <div class="form-group col-md-2">
                            <label for="matricula">País</label>
                            <select class="selectpicker" data-live-search="true" title="Buscar..." id="pais" name="pais">
                                @foreach ($paises as $dato)
                                @if($contacto->pais==$dato->Pais)
                                <option value="{{$dato->Pais}}" selected>{{ $dato->Pais}}</option>
                                @else
                                <option value="{{$dato->Pais}}">{{ $dato->Pais}}</option>
                                @endif
                                @endforeach
                            </select> 
                        </div>
                            <div class="form-group col-md-2">
                            <label for="matricula">Provincia</label>
                            <select class="selectpicker" data-live-search="true" title="Buscar..." id="provincia" name="provincia">
                                @foreach ($provincias as $dato)
                                @if($contacto->provincia==$dato->Provincia)
                                <option value="{{$dato->Provincia}}" selected>{{ $dato->Provincia}}</option>
                                @else
                                <option value="{{$dato->Provincia}}" >{{ $dato->Provincia}}</option>
                                @endif
                                @endforeach
                            </select> 
                        </div>
                            <div class="form-group col-md-4">
                            <label for="empresa">Delegación</label>
                            <select class="selectpicker" data-live-search="true" title="Buscar..." id="contratante" name="contratante">
                                @foreach ($contratantes as $contratante)
                                @if($contratante->Num_empresa==$contacto->delegacion)
                                <option value="{{$contratante->Num_empresa}}" selected>{{ $contratante->Empresa}}</option>
                                @else
                                 <option value="{{$contratante->Num_empresa}}">{{ $contratante->Empresa}}</option>
                                 @endif
                                @endforeach
                            </select>                              
                        </div>
                            <div class="form-group col-md-4">
                            <label for="cif">Comercial</label>
                            <select class="selectpicker" data-live-search="true" title="Buscar..." id="comercial" name="comercial">
                                @foreach ($comerciales as $comercial)
                                @if($comercial->ID==$contacto->id_comercial)
                                <option value="{{$comercial->ID}}" selected>{{ $comercial->comercial}}</option>
                                @else
                                 <option value="{{$comercial->ID}}">{{ $comercial->comercial}}</option>
                                 @endif
                                @endforeach
                            </select>      
                        </div>
                        <div class="form-group col-md-3">
                     
                            <label for="matricula">Web</label>
                            <input type="text" class="form-control" name="web" value="{{$contacto->url}}">  
                                                     
                        </div>
                           <div class="form-group col-md-3">
                      
                                <label for="matricula">Email</label>
                                <input type="text" class="form-control" name="email" value="{{$contacto->email}}">  
                                          
                        </div>
                                <div class="form-group col-md-3">
                      
                                <label for="matricula">Teléfono 1</label>
                                <input type="text" class="form-control" name="tel1" value="{{$contacto->tel1}}">  
                                          
                        </div>
                                <div class="form-group col-md-3">
                      
                                <label for="matricula">Teléfono 2</label>
                                <input type="text" class="form-control" name="tel2" value="{{$contacto->tel2}}">  
                                          
                        </div>
                                <div class="form-group col-md-3">
                      
                                <label for="matricula">Móvil 1</label>
                                <input type="text" class="form-control" name="movil1" value="{{$contacto->movil1}}">  
                                          
                        </div>
                                <div class="form-group col-md-3">
                      
                                <label for="matricula">Móvil 2</label>
                                <input type="text" class="form-control" name="movil2" value="{{$contacto->movil2}}">  
                                          
                        </div>
                                <div class="form-group col-md-3">
                      
                                <label for="matricula">Fax 1</label>
                                <input type="text" class="form-control" name="fax1" value="{{$contacto->fax1}}">  
                                          
                        </div>
                                <div class="form-group col-md-3">
                      
                                <label for="matricula">Fax 2</label>
                                <input type="text" class="form-control" name="fax2" value="{{$contacto->fax2}}">  
                                          
                        </div>
                           <div class="form-group col-md-4">
                      
                                <label for="matricula">CC</label>
                                <input type="text" class="form-control" name="cc" value="{{$contacto->CC}}">        
                                          
                        </div>
                            <div class="form-group col-md-6">
                            
                            <label for="matricula">Observaciones</label>
                            <textarea type="textarea" class="form-control" name="observaciones" style="" >{{$contacto->Observacion}}</textarea> 
                               
                            </div> 
                             <div class="form-group col-md-6">
                           <ul class="list-inline">
                         @if($contacto->mandato == 1)
                          <li><label for="matricula">Mandato</label> <input type="checkbox" name="mandato" value="" checked></li>
                          @else
                           <li><label for="matricula">Mandato</label> <input type="checkbox" name="mandato" value=""></li>
                           @endif
                             @if($contacto->impagado_pdteDev == 1)
                          <li><label for="matricula">Impago</label> <input type="checkbox" name="impagado_pdteDev" value="" checked></li>
                          @else
                           <li><label for="matricula">Impago</label> <input type="checkbox" name="impagado_pdteDev" value=""></li>
                           @endif
                           @if($contacto->cteConflictivo == 1)
                          <li><label for="matricula">Conflictivo</label> <input type="checkbox" name="cteConflictivo" value="" checked></li>
                          @else
                           <li><label for="matricula">Conflictivo</label> <input type="checkbox" name="cteConflictivo" value=""></li>
                           @endif
                               @if($contacto->Mailing == 1)
                          <li><label for="matricula">Mailing</label> <input type="checkbox" name="mailing" value="" checked></li>
                          @else
                           <li><label for="matricula">Mailing</label> <input type="checkbox" name="mailing" value=""></li>
                           @endif
                        </ul>
                            </div>
                                                 
                        </div>     
                    </form>
                    </div>                  
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

    $("#volver").click(function(){
        window.history.go(-1); return false;
    });
     $(document).on('focusin','.datepicker',function(){
         $(this).datepicker({
        format: "dd/mm/yyyy",
        dateFormat: 'yy-mm-dd',
        language: "es",
        autoclose: true
    });

         $(this).selectpicker("data-live-search","true");

    });


 $("#btn-save").click(function(){
    
    $form = $("#form_sancion").serialize();

     setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '/contratos/gesanc/contactos/guardar',
                data : {'datos' : $("#form_sancion").serialize()},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    if(data.includes("error")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                //location.reload();
                            }
                        });
                    }else{
                         bootbox.alert({
                            message: data,
                            callback: function () {
                                 location.reload();
                            }
                        });
                    }
                   
                    $("#loading").hide();

                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);



});

  $("#eliminar").click(function(){

    $CODCLI = $("#codcli").val();
    $cif = $("#cif").val();

    bootbox.confirm({
        title: "Eliminar Contacto",
        message: "<i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> ¿Está seguro que desea eliminar el Contrato?</strong> Tenga en cuenta que  eliminará el contacto pero también los contratos y sanciones que están asociados a este CIF en GESANC. Esta acción es irreversible.",
        buttons: {
            cancel: {
            label: '<i class="fa fa-times"></i> Cancelar'
        },
        confirm: {
            label: '<i class="fa fa-check"></i> Aceptar'
        }
        },
        callback: function (result) {
            if(result == true){
               

                setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('contratos/gesanc/contactos/eliminar')}}',
                        data : {'codcli':$CODCLI,'cif':$cif},            
                        success : function(data){
                                    if(data.includes("SQLSTATE")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                //location.reload();
                            }
                        });
                    }else{
                         bootbox.alert({
                            message: data,
                            callback: function () {
                                 window.location = "/contratos/gesanc/contactos";
                            }
                        });
                    }
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);
            }
        }
    });

});
</script>
@endsection