@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">

        <div class="col-sm-8">
            <div class="panel panel-default">
                <div class="row"> 
                    <div class="panel-heading" id="sanciones-header">                    
                        <div class="col-md-12">
                            <h3 class="module-title">Contrato {!! $contrato->idcontrato !!}</h3>
                           <input type="hidden" class="id" name="id" id="id" value="{!!$contrato->idcontrato !!}">
                        </div>
                        <div class="col-md-12">
                            <div>
                                <ul class="nav navbar-nav">
                                    <li><a href="#" class="bt-edit"><i class="fa fa-save"> </i> Guardar</a></li>
                                    <li><a href="#" class="bt-export"><i class="fa fa-print"> </i> Imprimir</a></li>
                                    <li><a href="#" class="bt-export"><i class="fa fa-print"> </i> Codigos matriculas</a></li>
                                    <li><a href="#" id="eliminar"><i class="fa fa-trash"> </i> Eliminar</a></li>
                                    <li><a href="#" id="des_sanciones"><i class="fa fa-stop-circle" aria-hidden="true"></i> Desactivar Sanciones</a></li>
                                    <li><a href="#" id="act_sanciones"><i class="fa fa-play-circle" aria-hidden="true"></i> Activar Sanciones</a></li>
                                    <li><a href="#" id="volver"><i class="fa fa-arrow-left"> </i> Volver</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="panel-body" style="border-top:2px solid #335599;">                        
                    <form class="inline-form" id="form_contrato">
                            <div class="form-group col-md-2">
                            <label for="codigo">Clave</label>
                            <input type="text" class="form-control" name="claveContrato" value="{!! $contrato->clavecontrato !!}" disabled>
                        </div> 
                       <div class="form-group col-md-2">
                            <label for="codigo">Clave</label>
                            <input type="text" class="form-control" name="claveContrato" value="{!! $contrato->clavecontrato !!}" disabled>
                        </div>
                        <div cl
                           <div class="form-group col-md-2">
                            <label for="codigo">ID</label>
                            <input type="text" class="form-control" name="codContrato" value="{!! $contrato->idcontrato !!}" disabled>
                            <input type="hidden" class="form-control" name="idcontrato" id="idcontrato" value="{!! $contrato->idcontrato !!}">
                        </div>
                        <div cl
                        <div class="form-group col-md-4">
                            <label for="matricula">Empresa</label>
                            <input type="text" class="form-control" name="empresa" value="{!! $empresa->empresa !!}" disabled>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="codigo">Cif</label>
                            <input type="text" class="form-control" name="cifempresa" id="cifempresa" value="{!! $empresa->cif !!}" disabled>
                            <input type="hidden" class="form-control" name="cif" value="{!! $empresa->cif !!}">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="cif">Población</label>
                            <input type="text" id="cif" class="form-control" name="tipo" value="{!! $empresa->poblacion !!}" disabled >                  
                        </div>
                         <div class="form-group col-md-3">
                            <label for="contrato">Matricula</label>
                                <select class="selectpicker" data-live-search="true" title="Buscar..." id="matricula" name="matricula">
                                    @foreach ($matriculas as $matricula)
                                    @if($matricula->Matricula == $contrato->Matricula)
                                    <option value="{{$matricula->IdMatricula}}" selected>{{ $matricula->Matricula}}</option>
                                    @else
                                     <option value="{{$matricula->IdMatricula}}">{{ $matricula->Matricula}}</option>
                                     @endif
                                    @endforeach
                                </select>                       
                        </div>
                     <div class="form-group col-md-3">
                            <label for="codigo">Tipo Contrato</label>
                          <select class="selectpicker" data-live-search="true" title="Buscar..." id="tipo_contrato" name="tipo_contrato">
                                @foreach ($tipos_contrato as $tipo)
                              @if($tipo->tipoAnexo== $contrato->Tipo)
                              <option value="{{$tipo->tipoAnexo}}}" selected>{{$tipo->tipoAnexo}}</option>
                              @else
                            <option value="{{$tipo->tipoAnexo}}}">{{$tipo->tipoAnexo}}</option>
                              @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="empresa">Contratante</label>
                            <select class="selectpicker" data-live-search="true" title="Buscar..." id="contratante" name="contratante">
                                @foreach ($contratantes as $contratante)
                                @if($contratante->Num_empresa==$contrato->contratante)
                                <option value="{{$contratante->Num_empresa}}" selected>{{ $contratante->Empresa}}</option>
                                @else
                                 <option value="{{$contratante->Num_empresa}}">{{ $contratante->Empresa}}</option>
                                 @endif
                                @endforeach
                            </select>                              
                        </div>
                        <div class="form-group col-md-4">
                            <label for="cif">Comercial</label>
                            <select class="selectpicker" data-live-search="true" title="Buscar..." id="comercial" name="comercial">
                                @foreach ($comerciales as $comercial)
                                @if($comercial->ID==$contrato->id_comercial)
                                <option value="{{$comercial->ID}}" selected>{{ $comercial->comercial}}</option>
                                @else
                                 <option value="{{$comercial->ID}}">{{ $comercial->comercial}}</option>
                                 @endif
                                @endforeach
                            </select>      
                        </div>
                        <div class="form-group col-md-3">
                            <label for="matricula">Alta</label>
                            <input type="text" class="form-control" name="Alta" value="{{ date('d/m/Y', strtotime($contrato->Alta)) }}">                            
                        </div>
                           <div class="form-group col-md-3">
                            <label for="matricula">Baja</label>
                            @if($contrato->Baja !=null)
                            <input type="text" class="form-control" name="Baja" value="{{ date('d/m/Y', strtotime($contrato->Baja)) }}">
                            @else
                             <input type="text" class="form-control" name="Baja" value="">
                             @endif                         
                        </div>
                         <div class="form-group col-md-3">
                            <label for="matricula">Vencimiento</label>
                            <input type="text" class="form-control" name="Vencimiento" value="{{ date('d/m/Y', strtotime($contrato->Vencimiento)) }}">                            
                        </div>  
                                 <div class="form-group col-md-6">
                            <label for="matricula">Notas del Anexo</label>
                            @if($notas_anexo!=null)
                            <textarea name="notas_anexo" class="form-control" rows="4" disabled>{{$notas_anexo->Notas}}</textarea>
                            @else
                             <textarea name="notas_anexo" class="form-control" rows="4" disabled></textarea>
                            @endif
                        </div>  
                                 <div class="form-group col-md-6">
                            <label for="matricula">Notas del cliente</label>
                            <textarea name="notas_cliente" class="form-control" rows="4">{{$empresa->Observacion}}</textarea>
                        </div>  
                                 <div class="form-group col-md-6">
                            <label for="matricula">Notas del contrato</label>
                            <textarea name="notas_contrato" class="form-control" rows="4">{{$contrato->Notas}}</textarea>
                        </div>
                             <div class="form-group col-md-4">
                               @if($empresa->impagado_pdteDev == 1)
                                <label style="background:#5B94E0;color:white"> IMPAGADO/PTE DEVOLUCION </label>
                                @endif
                        </div>
                        <div class="form-group col-md-4">
                               @if($inactivas->total > 0)
                                <label style="background:red;color:white">HAY SANCIONES INACTIVAS</label>
                                <input type="hidden" name="inactivas" id="inactivas" value="{{$inactivas->total}}">
                                @endif
                        </div>
                           <div class="form-group col-md-4">
                               @if($activo->total > 0)
                                <label style="background:green;color:white">CLIENTE ACTIVO</label>
                                @endif
                        </div>
                    </form>
                    </div>

                </div>
                <div class="panel panel-default">
              <div class="row"> 
                    <div class="panel-heading" id="detalles-header">                    
                        <div class="col-md-6">
                            <h3 class="module-title">Remolques</h3>
                        </div>
                    </div>
                    </div>
                    <div id="error" style="display:none" class="alert alert-danger alert-dismissible fade in" role="alert"></div>
                    <div id="success" style="display:none" class="alert alert-success alert-dismissible"></div>
                     <div class="table-responsive" id="tabla_contenido">
                    <table class="table">
                        <thead class="table-header">
                            <th>Matricula</th>                        
                            <th>Alta</th>
                            <th>Baja</th>
                        </thead>
                        <tbody id="myTable">
                        @foreach ($remolques as $remolque)
                       <tr>
                       <td>{{$remolque->matricula}}</td>
                       <td>{{ date('d/m/Y', strtotime($remolque->Alta))}}</td>
                       @if($remolque->Baja != null)
                       <td>{{ date('d/m/Y', strtotime($remolque->Baja))}}</td>
                       @else
                       <td></td>
                       @endif
                       </tr>
                            @endforeach 
                        </tbody>
                    </table>
                     
                </div>
                    </div>
            </div>
            <div class="col-sm-4">
            <div class="panel panel-default">
            <div class="panel-body">
                <div class="form-group col-md-12" style="overflow: scroll; height: 200px;">
                           <h3 class="module-title">Otros contratos</h3>
                            <table class="table">
                        <thead class="table-header">
                            <th>Fecha Contrato</th>                        
                            <th>Tipo</th>
                        </thead>
                        <tbody id="myTable" >
                       @foreach ($otros_contratos as $otro)
                       <tr style="cursor:pointer;"ondblclick='window.open("/contratos/gesanc/"+$(this).closest("tr").find(".idcontrato").val(),"_blank");'>
                       <td>{{ date('d/m/Y', strtotime($otro->Alta))}}<input type="hidden" name="id" class="idcontrato" value="{!! $otro->idcontrato !!}"></td>
                       <td>{!! $otro->Tipo !!}</td>
                       </tr>
                            @endforeach 
                        </tbody>
                        
                    </table>
                                            
                        </div>
                        </div>

            </div>
            <div class="panel panel-default">
            <div class="panel-body">
            <div class="form-group col-md-12" style="overflow: scroll;height: 200px;">
                            <h3 class="module-title">Fecha Selec.</h3>
                            <table class="table">
                        <thead class="table-header">
                            <th>Matricula</th>                        
                            <th>Tipo</th>
                        </thead>
                        
                        <tbody id="myTable" >
                       @foreach ($fecha_seleccionada as $dato)
                       <tr style="cursor:pointer;"ondblclick='window.open("/contratos/gesanc/"+$(this).closest("tr").find(".idcontrato").val(),"_blank");'>
                       <td>{!! $dato->Matricula !!}<input type="hidden" name="id" class="idcontrato" value="{!! $dato->idcontrato !!}"></td>
                       <td>{!! $dato->Tipo !!}</td>
                       </tr>
                            @endforeach 
                        </tbody>
                        
                    </table>
                                            
                        </div>
                    
                        </div>

            </div>
             <div class="panel panel-default">
            <div class="panel-body">
                <div class="form-group col-md-12" style="overflow: scroll; height: 200px;">
                           <h3 class="module-title">De Alta</h3>
                            <table class="table">
                        <thead class="table-header">
                            <th>Matricula</th>                        
                        </thead>
                        <tbody id="myTable" >
                       @foreach ($de_alta as $dato)
                       <tr style="cursor:pointer;"ondblclick='window.open("/contratos/gesanc/"+$(this).closest("tr").find(".contratoID").val(),"_blank");'>
                       <td>{{$dato->Matricula}}<input type="hidden" name="id" class="contratoID" value="{!! $dato->idcontrato !!}"></td>
                       </tr>
                            @endforeach 
                        </tbody>
                        
                    </table>
                                            
                        </div>
                        </div>

            </div>
             <div class="panel panel-default">
            <div class="panel-body">
                <div class="form-group col-md-12" style="overflow: scroll; height: 200px;">
                           <h3 class="module-title">No contratadas</h3>
                            <table class="table">
                        <thead class="table-header">
                            <th>Matricula</th>                        
                        </thead>
                        <tbody id="myTable" >
                       @foreach ($no_contratadas as $dato)
                       <tr style="cursor:pointer;"ondblclick='window.open("/contratos/gesanc/"+$(this).closest("tr").find(".contratoID").val(),"_blank");'>
                       <td>{{$dato->Matricula}}<input type="hidden" name="id" class="contratoID" value="{!! $dato->idcontrato !!}"></td>
                       </tr>
                            @endforeach 
                        </tbody>
                        
                    </table>
                                            
                        </div>
                        </div>
            </div>
            </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

$("#volver").click(function(){
        window.history.go(-1); return true;
    });


 $(document).on('focusin','.datepicker',function(){
         $(this).datepicker({
        format: "dd/mm/yyyy",
        dateFormat: 'yy-mm-dd',
        language: "es",
        autoclose: true
    });

         $(this).selectpicker("data-live-search","true");

    });

  $(".bt-edit").click(function(){

    
    $ID = $("#id").val();

    $form = $("#form_contrato").serialize();
  
     setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '/contratos/gesanc/guardar',
                data : {'datos' : $("#form_contrato").serialize()},
                success : function(data){
                    console.log(JSON.stringify(data));
                    bootbox.alert({
                            message: data,
                            callback: function () {
                                location.reload();
                            }
                        });
                    $("#loading").hide();

                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

});



 $("#eliminar").click(function(){
    bootbox.confirm({
        title: "Eliminar Contrato",
        message: "<i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> ¿Está seguro que desea eliminar el Contrato?</strong> Tenga en cuenta que esta acción es irreversible.",
        buttons: {
            cancel: {
            label: '<i class="fa fa-times"></i> Cancelar'
        },
        confirm: {
            label: '<i class="fa fa-check"></i> Aceptar'
        }
        },
        callback: function (result) {
            if(result == true){
                $idcontrato = $("#idcontrato").val();

                setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('contratos/gesanc/eliminar')}}',
                        data : {'idcontrato':$idcontrato},            
                        success : function(data){
                                    if(data.includes("SQLSTATE")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                //location.reload();
                            }
                        });
                    }else{
                         bootbox.alert({
                            message: data,
                            callback: function () {
                                 window.location = "/contratos/gesanc";
                            }
                        });
                    }
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);
            }
        }
    });

});

  $("#des_sanciones").click(function(){

     $cif = $("#cifempresa").val();

            bootbox.confirm({
        title: "Desactivar Sanciones",
        message: "<i class='fa fa-exclamation-circle' aria-hidden='true'></i></span> ¿Está seguro que desea desactivar todas las sanciones para este cliente?</span>",
        buttons: {
            cancel: {
            label: '<i class="fa fa-times"></i> Cancelar'
        },
        confirm: {
            label: '<i class="fa fa-check"></i> Aceptar'
        }
        },
        callback: function (result) {
            if(result == true){
               
                setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('contratos/gesanc/desactivar_sanciones')}}',
                        data : {'cif':$cif},            
                        success : function(data){
                              if(data.includes("SQLSTATE")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                //location.reload();
                            }
                        });
                    }else{
                         bootbox.alert({
                            message: data,
                            callback: function () {
                                 location.reload();
                            }
                        });
                    }
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);
            }
        }
    });

});

   $("#act_sanciones").click(function(){

     $cif = $("#cifempresa").val();

    if($("#inactivas").val() > 0){

            bootbox.confirm({
        title: "Eliminar Contrato",
        message: "<i class='fa fa-exclamation-circle' aria-hidden='true'></i></span> ¿Está seguro que desea activar todas las sanciones para este cliente?</span>",
        buttons: {
            cancel: {
            label: '<i class="fa fa-times"></i> Cancelar'
        },
        confirm: {
            label: '<i class="fa fa-check"></i> Aceptar'
        }
        },
        callback: function (result) {
            if(result == true){
                $cif = $("#cifempresa").val();



                setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('contratos/gesanc/activar_sanciones')}}',
                        data : {'cif':$cif},            
                        success : function(data){
                              if(data.includes("SQLSTATE")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                //location.reload();
                            }
                        });
                    }else{
                         bootbox.alert({
                            message: data,
                            callback: function () {
                                 location.reload();
                            }
                        });
                    }
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);
            }
        }
    });

    }else{
        bootbox.alert("No existen sanciones desactivadas");
    }
});

</script>

@endsection