@extends('layouts.app')
@section('content')

<div class="container">
    <div class="row">
        <div class="col-sm-2">
    <h4>Filtros</h4>
                  <div class="filtros">
                <a data-toggle="collapse" href="#fechaalta" aria-expanded="false" aria-controls="fechaalta"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> fecha de Alta</h5></a>
                <div class="collapse" id="fechaalta">
                <input type="text" class="form-control datepicker" name="fecha_alta_inicio" id="fecha_alta_inicio" value="" placeholder="fecha de inicio...">
                <br>
                <input type="text" class="form-control datepicker" id="fecha_alta_fin" name="fecha_alta_fin" value="" placeholder="fecha de fin...">
                </div>
            </div>
            <div class="filtros">
                <a data-toggle="collapse" href="#fechabaja" aria-expanded="false" aria-controls="fechabaja"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> fecha de Baja</h5></a>
                <div class="collapse" id="fechabaja">
                <input type="text" class="form-control datepicker" name="fecha_baja_inicio" id="fecha_baja_inicio" value="" placeholder="fecha de inicio...">
                <br>
                <input type="text" class="form-control datepicker" id="fecha_baja_fin" name="fecha_baja_fin" value="" placeholder="fecha de fin...">
                </div>
            </div>
             <div class="filtros">
                <a data-toggle="collapse" href="#fechavenc" aria-expanded="false" aria-controls="fechavenc"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Vencimiento</h5></a>
                <div class="collapse" id="fechavenc">
                <input type="text" class="form-control datepicker" name="fecha_venc_inicio" id="fecha_venc_inicio" value="" placeholder="fecha de inicio...">
                <br>
                <input type="text" class="form-control datepicker" id="fecha_venc_fin" name="fecha_venc_fin" value="" placeholder="fecha de fin...">
                </div>
            </div>
                     <div class =filtros>
                 <a data-toggle="collapse" href="#coltipo" aria-expanded="false" aria-controls="coltipo"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Tipo Contrato</h5></a>
                <div class="collapse" id="coltipo">
               <select class="selectpicker" data-live-search="true" title="Buscar..." id="tipoContrato" name="tipoContrato">
                 <option selected></option>
                    @foreach ($tipos_contrato as $tipo)
                    <option value="{{$tipo->tipoAnexo}}">{{ $tipo->tipoAnexo}}</option>
                    @endforeach
                </select>
                </div>
            </div>
                <div class =filtros>
                 <a data-toggle="collapse" href="#colcodcontrato" aria-expanded="false" aria-controls="colcodcontrato"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Codigo Contrato</h5></a>
                <div class="collapse" id="colcodcontrato">
                <input type="text" class="form-control" name="codContrato" id="codContrato" value="" placeholder=".">
                </div>
            </div>
              <div class =filtros>
                 <a data-toggle="collapse" href="#colempresa" aria-expanded="false" aria-controls="colempresa"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Empresa</h5></a>
                <div class="collapse" id="colempresa">
               <select class="selectpicker" data-live-search="true" title="Buscar..." id="empresa" name="empresa">
                 <option selected></option>
                    @foreach ($empresas as $empresa)
                    <option value="{{$empresa->CODCLI}}">{{ $empresa->empresa}}</option>
                    @endforeach
                </select>
                </div>
            </div>
                  <div class =filtros>
                 <a data-toggle="collapse" href="#colnotas" aria-expanded="false" aria-controls="colnotas"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Notas</h5></a>
                <div class="collapse" id="colnotas">
                   <input type="text" class="form-control" name="notas" id="notas" value="" placeholder="Buscar...">
                </div>
            </div>
            <div class =filtros>
                 <a data-toggle="collapse" href="#colclave" aria-expanded="false" aria-controls="colclave"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Clave Contrato</h5></a>
                <div class="collapse" id="colclave">
                   <input type="text" class="form-control" name="clave" id="clave" value="" placeholder="Buscar...">
                </div>
            </div>

              <div class="filtros">
            <a data-toggle="collapse" href="#colprovincia" aria-expanded="false" aria-controls="colprovincia"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Provincia</h5></a>
            <div class="collapse" id="colprovincia">
             <select class="selectpicker" data-live-search="true" title="Buscar..." id="matricula" name="matricula">
                 <option selected></option>
                    @foreach ($provincias as $provincia)
                    <option value="{{$provincia->IdProvincia}}">{{ $provincia->Provincia}}</option>
                    @endforeach
                </select>
                </div>
                </div>
            <div class="filtros">
                <a data-toggle="collapse" href="#colcontratante" aria-expanded="false" aria-controls="colcontratante"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Empresa Comercial</h5></a>
                 <div class="collapse" id="colcontratante">
                 <select class="selectpicker" data-live-search="true" title="Buscar..." id="contratante" name="contratante">
                 <option selected></option>
                    @foreach ($contratantes as $contratante)
                    <option value="{{$contratante->Num_empresa}}">{{ $contratante->Empresa}}</option>
                    @endforeach
                </select>
                </div>
            </div>
            <div class="filtros">
                <a data-toggle="collapse" href="#colcomercial" aria-expanded="false" aria-controls="colcomercial"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Nombre Comercial</h5></a>
                 <div class="collapse" id="colcomercial">
                  <select class="selectpicker" data-live-search="true" title="Buscar..." id="comercial" name="comercial">
                 <option selected></option>
                    @foreach ($comerciales as $comercial)
                    <option value="{{$comercial->ID}}">{{ $comercial->comercial}}</option>
                    @endforeach
                </select>
                </div>
            </div>
            <div class="filtros">
            <a data-toggle="collapse" href="#colmatricula" aria-expanded="false" aria-controls="colmatricula"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Matricula</h5></a>
            <div class="collapse" id="colmatricula">
             <select class="selectpicker" data-live-search="true" title="Buscar..." id="matricula" name="matricula">
                 <option selected></option>
                    @foreach ($matriculas as $matricula)
                    <option value="{{$matricula->IdMatricula}}">{{ $matricula->Matricula}}</option>
                    @endforeach
                </select>
                </div>
                </div>
            <div class="filtros">
            <button type="button" id="filtrar" class="btn btn-default btn-primary">filtrar</button>
            </div>
            </div>
        <div class="col-sm-10">
            <div class="panel panel-default">
                <div class="row"> 
                    <div class="panel-heading" id="sanciones-header">                    
                        <div class="col-md-2" style="width: 100%">
                            <h3 class="module-title">Contratos GESANC</h3>
                        </div>
                        <div class="col-md-10">
                            <div>
                                <ul class="list-inline nav navbar-nav">
                                    <li><a href="{{url('contratos/gesanc/contactos')}}"><i class="fa fa-user" aria-hidden="true"></i> Contactos</a></li>
                                    <li><a href="#" class="bt-export" id="codigos"><i class="fa fa-print"> </i> Codigos matriculas</a></li>
                                    <li><a href="#" class="bt-export" id="renovaciones"><i class="fa fa-print"> </i> Renovaciones</a></li>
                                    <li><a href="#" id="excel"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Excel</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table-responsive"">
                    <table class="table">
                        <thead class="table-header">
                            <th>Contrato</th>                        
                            <th>Tipo</th>
                            <th>Matrícula</th>
                            <th>CIF</th>
                            <th>Fecha Alta</th>
                            <th>Fecha Baja</th>
                            <th></th>
                        </thead>
                        <tbody id="myTable">
                        @foreach ($contratos as $contrato)
                        <tr>
                            <td>{{ $contrato->idcontrato }} </td>
                            <td>{{ $contrato->Tipo }} </td>
                            <td class="matricula">{{ $contrato->Matricula }} </td>                            
                            <td>{{ $contrato->cif }} </td>
                            <td>{{ date('d - m - Y', strtotime($contrato->Alta)) }} </td>
                            @if($contrato->Baja !=null && $contrato->Baja != "")
                            <td>{{ date('d - m - Y', strtotime($contrato->Baja)) }} </td>
                            @else
                            <td></td>
                            @endif
                            <td><a class="btn btn-sm btn-success" href="{{URL::to('contratos/gesanc/'.$contrato->idcontrato)}}"><i class="fa fa-edit"></i> </a> <a class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a></td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    
                </div>

                 <div class="col-md-12 text-center">
      <ul class="pagination pagination-lg pager" id="myPager"></ul>
      </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

$codigos = [];

 $('.datepicker').datepicker({
        format: "dd/mm/yyyy",
        dateFormat: 'yy-mm-dd',
        language: "es",
        autoclose: true
    });
  
$( "#filtrar" ).click(function() {
$fecha_alta_inicio = $("#fecha_alta_inicio").val();
$fecha_alta_fin = $("#fecha_alta_fin").val();

$fecha_baja_inicio = $("#fecha_baja_inicio").val();
$fecha_baja_fin = $("#fecha_baja_fin").val();

$fecha_venc_inicio = $("#fecha_venc_inicio").val();
$fecha_venc_fin = $("#fecha_venc_fin").val();

$codContrato = $("#codContrato").val();

$contratante = $("#contratante option:selected").val();

$empresa =  $("#empresa option:selected").val();

$comercial =  $("#comercial option:selected").val();

$matricula = $("#matricula option:selected").text();

$provincia = $("#provincia option:selected").text();

$tipoContrato = $("#tipoContrato option:selected").text();

$cif = $("#cif").val();

$notas = $("#notas").val();

$claveContrato = $("#clave").val();

$alta_inicio = $fecha_alta_inicio.split("/").reverse().join("-");
$alta_fin = $fecha_alta_fin.split("/").reverse().join("-");

$baja_inicio = $fecha_baja_inicio.split("/").reverse().join("-");
$baja_fin = $fecha_venc_fin.split("/").reverse().join("-");

$venc_inicio = $fecha_venc_inicio.split("/").reverse().join("-");
$venc_fin = $fecha_baja_fin.split("/").reverse().join("-");



//alert($inicio);

        $dialog = bootbox.dialog({
                        message: '<p class="text-center">Cargando datos, espere por favor...</p>',
                        closeButton: false
                    });

    setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/gesanc/filtra_gesanc')}}',
                data : {'alta_inicio':$alta_inicio,'alta_fin':$alta_fin,'baja_inicio':$baja_inicio,'baja_fin':$baja_fin,'venc_inicio':$venc_inicio,'venc_fin':$venc_fin,'contratante':$contratante,'empresa':$empresa,'matricula':$matricula,'tipoContrato':$tipoContrato,'comercial':$comercial,'codContrato':$codContrato,'notas':$notas,'clave':$claveContrato},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('tbody').html(data);

                    // do something in the background
                    $dialog.modal('hide');
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

});

 $("#renovaciones").click(function(){
    var loading = bootbox.dialog({
    message: '<p class="text-center"><p><i class="fa fa-spin fa-spinner"></i> Cargando interfaz, espere por favor...</p>',
    closeButton: false
});
// do something in the background


      setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('contratos/gesanc/mostrar_renovaciones')}}',
                        data : {},
                        success : function(data){
                            loading.modal('hide');
                           var dialog = bootbox.dialog({
                                    message: data,
                                    title: "Informe de renovaciones mensuales",
                                    buttons: {
                                        success: {
                                            label: 'generar',
                                             callback: function() {
                                                $comercial = $("#comercial_origen option:selected").val();
                                                $mes = $("#mes_siguiente option:selected").val();
                                                $year = $("#year_curso option:selected").text();
                                                generar_renovaciones($comercial,$mes,$year);
                                                if($comercial = ""){
                                                    bootbox.alert("No ha seleccionado ningún comercial, reporte canceclado");
                                                }else{
                                               // generar_renovaciones($comercial,$mes,$year);
                                                dialog.modal('hide');
                                                }
                                                 }
                                        }
                                    }
                                });
                             $('.selectpicker').selectpicker('refresh');
                             $('.selectpicker').selectpicker('render');
                             $('.datepicker').datepicker({
                                format: "dd/mm/yyyy",
                                dateFormat: 'yy-mm-dd',
                                language: "es",
                                autoclose: true
                            });
                             
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);

});

    function generar_renovaciones(comercial,mes,year){

    window.location = '/contratos/gesanc/generar_renovaciones?comercial='+comercial+'&mes='+mes+'&year='+year;
      bootbox.alert("Su informe se ha generado");
  }


  $("#codigos").click(function(){

       var loading = bootbox.dialog({
    message: '<p class="text-center"><p><i class="fa fa-spin fa-spinner"></i> Cargando interfaz, espere por favor...</p>',
    closeButton: false
});
// do something in the background


      setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('contratos/gesanc/mostrar_codigos')}}',
                        data : {},
                        success : function(data){
                            loading.modal('hide');
                           var dialog = bootbox.dialog({
                                    message: data,
                                    title: "Códigos de matrícula para imprimir",
                                    buttons: {
                                        success: {
                                            label: 'generar',
                                             callback: function() {
                                            
                                            if($codigos.length == 0){
                                                bootbox.alert("No ha seleccionado ninguna matricula para imprimir el código")
                                            }else{
                                                $matriculas = JSON.stringify($codigos);
                                                //alert($matriculas);
                                                window.location = '/contratos/gesanc/imprimir_codigos_matricula?matriculas='+$matriculas;
                                                

                                            }
                                                 }
                                        }
                                    }
                                });
                             $('.selectpicker').selectpicker('refresh');
                             $('.selectpicker').selectpicker('render');
                             $('.datepicker').datepicker({
                                format: "dd/mm/yyyy",
                                dateFormat: 'yy-mm-dd',
                                language: "es",
                                autoclose: true
                            });
                             
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);


  });


  $(document).on('change','#buscar_matriculas',function(){
    $matricula = $(this).find('option:selected').val();
    $codigos.push($(this).find('option:selected').val());
    var dialog = bootbox.dialog({
                        message: '<p class="text-center"><p><i class="fa fa-spin fa-spinner"></i> Buscando la matricula, un momento por favor...</p>',
                        closeButton: false
                    });
       setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/gesanc/buscar_matricula')}}',
                data : {'matricula': $matricula},
                success : function(data){
                    console.log(JSON.stringify(data));
                    //$('#cod_matriculas tr:last').after(data);
                    jQuery("#cod_matriculas tbody").append(data);
                    dialog.modal('hide');
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);
  });

  $(document).on('click','.borrar_matricula',function(){
   $codigo = $(this).closest('tr').find(".idmatricula").text();
   for ($i = 0; $i<=$codigos.length - 1;$i++) {
       if($codigos[$i]==$codigo){
        $codigos.splice($i,1);
       }
   }
   $(this).closest('tr').remove();
  });


$("#excel").click(function(){

$fecha_alta_inicio = $("#fecha_alta_inicio").val();
$fecha_alta_fin = $("#fecha_alta_fin").val();

$fecha_baja_inicio = $("#fecha_baja_inicio").val();
$fecha_baja_fin = $("#fecha_baja_fin").val();

$fecha_venc_inicio = $("#fecha_venc_inicio").val();
$fecha_venc_fin = $("#fecha_venc_fin").val();

$codContrato = $("#codContrato").val();

$contratante = $("#contratante option:selected").val();

$empresa =  $("#empresa option:selected").val();

$comercial =  $("#comercial option:selected").val();

$matricula = $("#matricula option:selected").text();

$provincia = $("#provincia option:selected").text();

$tipoContrato = $("#tipoContrato option:selected").text();

$cif = $("#cif").val();

$notas = $("#notas").val();

$claveContrato = $("#clave").val();

$alta_inicio = $fecha_alta_inicio.split("/").reverse().join("-");
$alta_fin = $fecha_alta_fin.split("/").reverse().join("-");

$baja_inicio = $fecha_baja_inicio.split("/").reverse().join("-");
$baja_fin = $fecha_venc_fin.split("/").reverse().join("-");

$venc_inicio = $fecha_venc_inicio.split("/").reverse().join("-");
$venc_fin = $fecha_baja_fin.split("/").reverse().join("-");



//alert($inicio);

        $dialog = bootbox.dialog({
                        message: '<p class="text-center">Generando Excel, espere por favor...</p>',
                        closeButton: false
                    });

    setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/gesanc/exportar_excel')}}',
                data : {'alta_inicio':$alta_inicio,'alta_fin':$alta_fin,'baja_inicio':$baja_inicio,'baja_fin':$baja_fin,'venc_inicio':$venc_inicio,'venc_fin':$venc_fin,'contratante':$contratante,'empresa':$empresa,'matricula':$matricula,'tipoContrato':$tipoContrato,'comercial':$comercial,'codContrato':$codContrato,'notas':$notas,'clave':$claveContrato},            
                success : function(data){
                    window.location = this.url;
                    $dialog.modal('hide');
                },
                error : function(data){
                    bootbox.alert("Ha ocurrido un error generando su Excel.");
                    $dialog.modal('hide');
                }
            });
        }, 500); 


    });

</script>
@endsection