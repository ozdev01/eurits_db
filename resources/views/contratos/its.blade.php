@extends('layouts.app')
@section('content')

<div class="container">
    <div class="row">
        <div class="col-sm-2">
         
            <h4>Búsqueda</h4>           
            <!--<label for="search" id="loading" style="display:none;">
                <i class="fa fa-refresh fa-spin fa-1x fa-fw"></i>
            </label>-->
            <input type="text" class="form-control" id="search" name="search" placeholder="Buscar...">
            <h4>Filtros</h4>
            <div class =filtros>
                 <a data-toggle="collapse" href="#fechallegada" aria-expanded="false" aria-controls="fechallegada"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> fecha de Alta</h5></a>
                <div class="collapse" id="fechallegada">
                <input type="text" class="form-control datepicker" name="fecha_alta_inicio" id="fecha_alta_inicio" value="" placeholder="fecha de inicio...">
                <br>
                <input type="text" class="form-control datepicker" id="fecha_alta_fin" value="" placeholder="fecha de fin...">
                </div>
            </div>
            <div class="filtros">
           
                <a data-toggle="collapse" href="#fechamulta" aria-expanded="false" aria-controls="fechamulta"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> fecha de Baja</h5></a>
                <div class="collapse" id="fechamulta">
                <input type="text" class="form-control datepicker" name="fecha_baja_inicio" id="fecha_baja_inicio" value="" placeholder="fecha de inicio...">
                <br>
                <input type="text" class="form-control datepicker" id="fecha_baja_fin" name="fecha_baja_fin" value="" placeholder="fecha de fin...">
                </div>
            </div>
             <div class="filtros">
           
                <a data-toggle="collapse" href="#fechamulta" aria-expanded="false" aria-controls="fechamulta"><h5 class="titulo_filtro"><i class="fa fa-plus"></i>Vencimiento</h5></a>
                <div class="collapse" id="fechamulta">
                <input type="text" class="form-control datepicker" name="fecha_venc_inicio" id="fecha_venc_inicio" value="" placeholder="fecha de inicio...">
                <br>
                <input type="text" class="form-control datepicker" id="fecha_venc_fin" name="fecha_venc_fin" value="" placeholder="fecha de fin...">
                </div>
            </div>
              <div class =filtros>
                 <a data-toggle="collapse" href="#collapseempresa" aria-expanded="false" aria-controls="collapseempresa"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Empresa</h5></a>
                <div class="collapse" id="collapseempresa">
                <select class="selectpicker" data-live-search="true" title="Buscar..." id="empresa" name="empresa">
                 <option selected></option>
                    @foreach ($clientes as $cliente)
                    <option>{{ $cliente->Empresa}}</option>
                    @endforeach
                </select>
                </div>
            </div>
            <div class="filtros">
                <a data-toggle="collapse" href="#collapseconductor" aria-expanded="false" aria-controls="collapseconductor"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Contratante</h5></a>
                 <div class="collapse" id="collapseconductor">
                 <select class="selectpicker" data-live-search="true" title="Buscar..." id="contratante" name="contratante">
                 <option selected></option>
                    @foreach ($contratantes as $contratante)
                    <option>{{ $contratante->Contratante}}</option>
                    @endforeach
                </select>
                </div>
            </div>
            <div class="filtros">
                <a data-toggle="collapse" href="#collapsematri" aria-expanded="false" aria-controls="collapseconductor"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Matricula</h5></a>
                 <div class="collapse" id="collapsematri">
                  <select class="selectpicker" data-live-search="true" title="Buscar..." id="matricula" name="matricula">
                 <option selected></option>
                    @foreach ($matriculas as $matricula)
                    <option>{{ $matricula->Matricula}}</option>
                    @endforeach
                </select>
                </div>
            </div>
              <div class="filtros">
                 <a data-toggle="collapse" href="#collapsematricula" aria-expanded="false" aria-controls="collapsematricula"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Tipo Contrato</h5></a>
                <div class="collapse" id="collapsematricula">
                  <select class="selectpicker" data-live-search="true" title="Buscar..." id="tipo_contrato" name="tipo_contrato">
                 <option selected></option>
                   @foreach ($tipo_contrato as $tipo)
                    <option>{{ $tipo->TipoServicio}}</option>
                    @endforeach
                </select>
                </div>
            </div>
            <div class="filtros">
            <a data-toggle="collapse" href="#collapselugar" aria-expanded="false" aria-controls="collapselugar"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Tipo de vehiculo</h5></a>
            <div class="collapse" id="collapselugar">
             <select class="selectpicker" data-live-search="true" title="Buscar..." id="tipo_vehiculo" name="tipo_vehiculo">
                <option selected></option>
                 <option value="C">Cabecera</option>
                <option value="R">Remolque</option>
                </select>
                </div>
            </div>
            <div class="filtros">
              <a data-toggle="collapse" href="#collapsepais" aria-expanded="false" aria-controls="collapsepais"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> País</h5></a>
              <div class="collapse" id="collapsepais">
               <select class="selectpicker" data-live-search="true" title="Buscar..." id="pais" name="pais">
                 <option selected></option>
                   @foreach ($paises as $pais)
                    <option>{{ $pais->Pais}}</option>
                    @endforeach
                </select>
                </div>
            </div>
            <div class="filtros">
                <a data-toggle="collapse" href="#collapseautoridad" aria-expanded="false" aria-controls="collapseautoridad"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Cobertura</h5></a>
                <div class="collapse" id="collapseautoridad">
              <select class="selectpicker form-control" data-live-search="true" title="Buscar..." id="cobertura">
                 <option selected></option>
                     @foreach ($coberturas as $cobertura)
                    <option>{{ $cobertura->Descripcion}}</option>
                    @endforeach
                    </select>
                </div>
            </div>
            <div class =filtros>
                 <a data-toggle="collapse" href="#collapsealtaanexo" aria-expanded="false" aria-controls="fechallegada"><h5 class="titulo_filtro"><i class="fa fa-plus"></i>Alta Anexo</h5></a>
                <div class="collapse" id="collapsealtaanexo">
                <input type="text" class="form-control datepicker" name="fecha_altaanexo_inicio" id="fecha_altaanexo_inicio" value="" placeholder="fecha de inicio...">
                <br>
                <input type="text" class="form-control datepicker" id="fecha_altaanexo_fin" value="" placeholder="fecha de fin...">
                </div>
            </div>
            <div class =filtros>
                 <a data-toggle="collapse" href="#collapsebajaanexo" aria-expanded="false" aria-controls="bajaanexo"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Baja Anexo</h5></a>
                <div class="collapse" id="collapsebajaanexo">
                <input type="text" class="form-control datepicker" name="fecha_bajaanexo_inicio" id="fecha_bajaanexo_inicio" value="" placeholder="fecha de inicio...">
                <br>
                <input type="text" class="form-control datepicker" id="fecha_bajaanexo_fin" value="" placeholder="fecha de fin...">
                </div>
            </div>
            <div class="filtros">
            <button type="button" id="filtrar" class="btn btn-default btn-primary">filtrar</button>
            </div>
        </div>
        <div class="col-sm-10">
            <div class="panel panel-default">
                <div class="row"> 
                    <div class="panel-heading" id="sanciones-header">                    
                        <div class="col-md-2" style="width: 100%">
                            <h3 class="module-title">Contratos ITS</h3>
                        </div>
                        <div class="col-md-10">
                            <div>
                                <ul class="list-inline">
                                 <a class="dropdown-toggle" href="#" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-plus"> </i> Añadir
                                      </a>
                                    <ul class="dropdown-menu" role="menu" id="listcontrato">
                                       <li><a class="dropdown-item" href="{{url('contratos/its/nuevo')}}">Contracto</a></li>
                                        <li><a class="dropdown-item" href="{{url('contratos/its/newcobertura')}}" id="newcobertura">Cobertura</a></li>
                                        <li><a class="dropdown-item" href="{{url('contratos/its/newtipo')}}" id="newtipo">Tipo de contrato</a></li>

                                    </ul>   
                                    <li>
                                      <a class="dropdown-toggle" href="#" id="dropdownMenucontact" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-user" aria-hidden="true"></i> Contactos
                                      </a>
                                    <ul class="dropdown-menu" role="menu">
                                       <li><a class="dropdown-item" href="/contratos/contactos/nuevo">Añadir</a></li>
                                        <li><a class="dropdown-item" href="/contratos/its/contactos">Gestionar</a></li>
                                           <li><a class="dropdown-item" href="/contratos/its/contactos">Ver asociados</a></li>
                                    </ul>   
                                    </li>
                                    <li><a href="{{url('contratos/tarjetas')}}" id="new_tramite"><i class="fa fa-credit-card" aria-hidden="true"></i> Tarjetas</a></li>
                                    <li><a href="{{url('contratos/its/cheques')}}" id="new_tramite"><i class="fa fa-money" aria-hidden="true"></i> Cheques</a></li>
                                    <li><a href="#" id="excel"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Excel</a></li>
                                    <!--<li><a href="#"><i class="fa fa-archive"> </i> Expedientes</a></li>-->
            
                                    <!--<li><a href="#"><i class="fa fa-file-pdf-o"> </i> PDF</a></li>-->
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table-responsive"">
                    <table class="table">
                        <thead class="table-header">
                            <th>Contrato</th>                        
                            <th>Tipo</th>
                            <th>Contratante</th>
                            <th>Cliente</th>
                            <th>Fecha Alta</th>
                            <th>Fecha Baja</th>
                            <th></th>
                        </thead>
                        <tbody id="myTable">
                        @foreach ($contratos as $contrato)
                        <tr>
                            <td>{{ $contrato->IDContrato }} </td>
                            <td>{{ $contrato->TipoContrato }} </td>
                            <td>{{ $contrato->Contratante }} </td>                            
                            <td>{{ $contrato->Contratado }} </td>
                            <td>{{ date('d - m - Y', strtotime($contrato->FechaContrato)) }} </td>
                            @if($contrato->Baja!=null)
                            <td>{{ date('d - m - Y', strtotime($contrato->Baja)) }} </td>
                            @else
                            <td></td>
                            @endif
                            <td><a class="btn btn-sm btn-success" href={!! url('contratos/its/'.$contrato->IDContrato) !!}#"><i class="fa fa-edit"></i> </a> <a class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a></td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    
                </div>

                 <div class="col-md-12 text-center">
      <ul class="pagination pagination-lg pager" id="myPager"></ul>
      </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

 $('.datepicker').datepicker({
        format: "dd/mm/yyyy",
        dateFormat: 'yy-mm-dd',
        language: "es",
        autoclose: true
    });
    
    $('#search').on('keyup',function(){
        $value=$(this).val();
        //alert($value);
        $("#loading").show();
        setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/search_its')}}',
                data : {'search':$value},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('tbody').html(data);
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);
});

$( "#filtrar" ).click(function() {
$fecha_alta_inicio = $("#fecha_alta_inicio").val();
$fecha_alta_fin = $("#fecha_alta_fin").val();

$fecha_baja_inicio = $("#fecha_baja_inicio").val();
$fecha_baja_fin = $("#fecha_baja_fin").val();

$fecha_venc_inicio = $("#fecha_venc_inicio").val();
$fecha_venc_fin = $("#fecha_venc_fin").val();

$contratante = $("#contratante option:selected").text();

$empresa =  $("#empresa option:selected").text();

$matricula = $("#matricula option:selected").text();

$tipo_contrato = $("#tipo_contrato option:selected").text();



$tipo_vehiculo = $("#tipo_vehiculo option:selected").text();

if($("#tipo_vehiculo option:selected").text() == "Cabecera"){
   $tipo_vehiculo = "C"; 
}else{
    $tipo_vehiculo = "R";
}

$cobertura =  $("#cobertura option:selected").text();

$pais = $("#pais option:selected").text();

if($pais=="España"){
    $pais= "Espana";
}

$fecha_altaanexo_inicio = $("#fecha_baja_inicio").val();
$fecha_altaanexo_fin = $("#fecha_baja_fin").val();

$fecha_bajaanexo_inicio = $("#fecha_venc_inicio").val();
$fecha_bajaanexo_fin = $("#fecha_venc_fin").val();



$alta_inicio = $fecha_alta_inicio.split("/").reverse().join("-");
$alta_fin = $fecha_alta_fin.split("/").reverse().join("-");

$baja_inicio = $fecha_baja_inicio.split("/").reverse().join("-");
$baja_fin = $fecha_venc_fin.split("/").reverse().join("-");

$venc_inicio = $fecha_venc_inicio.split("/").reverse().join("-");
$venc_fin = $fecha_baja_fin.split("/").reverse().join("-");

$altaanexo_inicio = $fecha_venc_inicio.split("/").reverse().join("-");
$altaanexo_fin = $fecha_venc_fin.split("/").reverse().join("-");

$bajaanexo_inicio = $fecha_bajaanexo_inicio.split("/").reverse().join("-");
$bajaanexo_fin = $fecha_bajaanexo_inicio.split("/").reverse().join("-");

//alert($inicio);

        $dialog = bootbox.dialog({
                        message: '<p class="text-center">Cargando datos, espere por favor...</p>',
                        closeButton: false
                    });

    setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/filtra_its')}}',
                data : {'alta_inicio':$alta_inicio,'alta_fin':$alta_fin,'baja_inicio':$baja_inicio,'baja_fin':$baja_fin,'venc_inicio':$venc_inicio,'venc_fin':$venc_fin,'altaanexo_inicio':$altaanexo_inicio,'altaanexo_fin':$altaanexo_fin,'bajaanexo_inicio':$bajaanexo_inicio,'bajaanexo_fin':$bajaanexo_fin,'contratante':$contratante,'empresa':$empresa,'matricula':$matricula,'tipo_contrato':$tipo_contrato,'cobertura':$cobertura,'pais':$pais},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('tbody').html(data);

                    // do something in the background
                    $dialog.modal('hide');
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

});

$("#contactos").click(function(){
$container = $('#collapseempresa').clone();
$container.find('select').attr('id', 'selectempresa');

 setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/contactos')}}',            
                success : function(data){
                    $('.selectpicker').selectpicker('refresh');
                    $('.selectpicker').selectpicker('render');
                    $('.table-responsive').html(data);

                    // do something in the background
                    $dialog.modal('hide');
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);


});

$("#excel").click(function () {
    $fecha_alta_inicio = $("#fecha_alta_inicio").val();
$fecha_alta_fin = $("#fecha_alta_fin").val();

$fecha_baja_inicio = $("#fecha_baja_inicio").val();
$fecha_baja_fin = $("#fecha_baja_fin").val();

$fecha_venc_inicio = $("#fecha_venc_inicio").val();
$fecha_venc_fin = $("#fecha_venc_fin").val();

$contratante = $("#contratante option:selected").text();

$empresa =  $("#empresa option:selected").text();

$matricula = $("#matricula option:selected").text();

$tipo_contrato = $("#tipo_contrato option:selected").text();



$tipo_vehiculo = $("#tipo_vehiculo option:selected").text();

if($("#tipo_vehiculo option:selected").text() == "Cabecera"){
   $tipo_vehiculo = "C"; 
}else{
    $tipo_vehiculo = "R";
}

$cobertura =  $("#cobertura option:selected").text();

$pais = $("#pais option:selected").text();

if($pais=="España"){
    $pais= "Espana";
}

$fecha_altaanexo_inicio = $("#fecha_baja_inicio").val();
$fecha_altaanexo_fin = $("#fecha_baja_fin").val();

$fecha_bajaanexo_inicio = $("#fecha_venc_inicio").val();
$fecha_bajaanexo_fin = $("#fecha_venc_fin").val();



$alta_inicio = $fecha_alta_inicio.split("/").reverse().join("-");
$alta_fin = $fecha_alta_fin.split("/").reverse().join("-");

$baja_inicio = $fecha_baja_inicio.split("/").reverse().join("-");
$baja_fin = $fecha_venc_fin.split("/").reverse().join("-");

$venc_inicio = $fecha_venc_inicio.split("/").reverse().join("-");
$venc_fin = $fecha_baja_fin.split("/").reverse().join("-");

$altaanexo_inicio = $fecha_venc_inicio.split("/").reverse().join("-");
$altaanexo_fin = $fecha_venc_fin.split("/").reverse().join("-");

$bajaanexo_inicio = $fecha_bajaanexo_inicio.split("/").reverse().join("-");
$bajaanexo_fin = $fecha_bajaanexo_inicio.split("/").reverse().join("-");

 $dialog = bootbox.dialog({
                        message: '<p class="text-center">Generando su Excel, espere por favor...</p>',
                        closeButton: false
                    });

    setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/its/exportar_excel_its')}}',
                data : {'alta_inicio':$alta_inicio,'alta_fin':$alta_fin,'baja_inicio':$baja_inicio,'baja_fin':$baja_fin,'venc_inicio':$venc_inicio,'venc_fin':$venc_fin,'altaanexo_inicio':$altaanexo_inicio,'altaanexo_fin':$altaanexo_fin,'bajaanexo_inicio':$bajaanexo_inicio,'bajaanexo_fin':$bajaanexo_fin,'contratante':$contratante,'empresa':$empresa,'matricula':$matricula,'tipo_contrato':$tipo_contrato,'cobertura':$cobertura,'pais':$pais},            
                success : function(data){
                    window.location = this.url;
                    $dialog.modal('hide');
                },
                error : function(data){
                    //console.log(JSON.stringify(data));
                    bootbox.alert("Ha ocurrido un error generando su Excel.");
                    $dialog.modal('hide');
                }
            });
        }, 500);

    });
    </script>
@endsection