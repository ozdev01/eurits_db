@extends('layouts.app')
@section('content')

   
<div class="container">
    <div class="row">
        <div class="col-sm-8">
            <div class="panel panel-default">
                <div class="row"> 
                    <div class="panel-heading" id="sanciones-header">                    
                        <div class="col-md-12">
                            <h3 class="module-title">Contrato @foreach ($contrato as $dato){!! $dato->IDContrato !!} @endforeach</h3>
                            @foreach ($contrato as $dato)<input type="hidden" class="id" name="id" id="id" value="{!!$dato->IDContrato !!}"> @endforeach
                        </div>
                        <div class="col-md-12">
                            <div>
                                <ul class="nav navbar-nav">
                                    <li><a href="#" class="bt-edit"><i class="fa fa-save"> </i> Guardar</a></li>
                                    <li><a href="#" class="bt-export"><i class="fa fa-print"> </i> Imprimir</a></li>
                                    <li><a href="#" id="eliminar"><i class="fa fa-trash"> </i> Eliminar</a></li>
                                    <li><a href="#" id="volver"><i class="fa fa-arrow-left"> </i> Volver</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="panel-body" style="border-top:2px solid #335599;">                        
                    <form class="inline-form" id="form_contrato">
                        <div class="form-group col-md-4">
                            <label for="matricula">Empresa</label>
                            @foreach ($clientes as $dato)
                            <input type="text" class="form-control" name="empresa" value="{!! $dato->Empresa !!}">
                            @endforeach                    
                        </div>
                        <div class="form-group col-md-2">
                            <label for="codigo">Contratado</label>
                            @foreach ($contrato as $dato)
                            <input type="text" class="form-control" name="contratado" value="{!! $dato->Contratado !!}">
                            @endforeach  
                        </div>
                        <div class="form-group col-md-3">
                            <label for="empresa">Tipo Contrato</label>
                            @foreach ($tipo_contrato as $dato)
                            <input type="text" id="empresa" class="form-control" name="tipo_contrato" value="{!! $dato->TipoServicio !!}">
                            @endforeach                                 
                        </div>
                        <div class="form-group col-md-2">
                            <label for="cif">Contratante</label>
                            @foreach ($contrato as $dato)
                            <input type="text" id="cif" class="form-control" name="contratante" value="{!! $dato->Contratante !!}">
                            @endforeach                       
                        </div>
                        <!-- <div class="form-group col-md-2">
                            <label for="contrato">Comercial</label>
                            <input type="text" class="form-control" name="contrato" value="" disabled>                            
                        </div>
                        <div class="form-group col-md-2">
                            <label for="expediente">Grupo Empres.</label>
                            <input type="text" class="form-control" name="expediente" value="">                         
                        </div>-->
                        <div class="form-group col-md-3">
                            <label for="matricula">Fecha Contrat</label>
                            @if($fecha_contrato!=null)
                            <input type="text" class="form-control datepicker" name="fechaContrato" value="{{ date('d/m/Y', strtotime($fecha_contrato)) }}">  
                            @else
                            <input type="text" class="form-control datepicker" name="fechaContrato" value="">
                            @endif                        
                        </div>
                         <div class="form-group col-md-3">
                            <label for="matricula">Renovación</label>
                            @if($fecha_renovacion!=null)
                            <input type="text" class="form-control datepicker" name="Renovacion" value="{{ date('d/m/Y', strtotime($fecha_renovacion)) }}">
                            @else
                            <input type="text" class="form-control datepicker" name="Renovacion" value="">
                            @endif                            
                        </div>  
                         <div class="form-group col-md-3">
                            <label for="matricula">Baja</label>
                             @foreach ($contrato as $dato)
                             @if($dato->Baja!=null)
                            <input type="text" class="form-control datepicker" name="Baja" value="{{ date('d/m/Y', strtotime($dato->Baja)) }}">
                            @else
                             <input type="text" class="form-control datepicker" name="Baja" value="">
                             @endif
                            @endforeach                             
                        </div>            
                    </form>
                    </div>

                </div>
                <div class="panel panel-default">
              <div class="row"> 
                    <div class="panel-heading" id="detalles-header">                    
                        <div class="col-md-6">
                            <h3 class="module-title">Anexos</h3>
                            <div>
                                <ul class="nav navbar-nav">
                                 @foreach ($contrato as $dato)
                                    <li><a href="{!! url('contratos/its/'.$dato->IDContrato.'/nuevoanexo') !!}" class="bt-edit"><i class="fa fa-save"> </i> Nuevo</a></li>
                                @endforeach
                            
                            </div>
                        </div>
                    </div>
                    </div>
                    <div id="error" style="display:none" class="alert alert-danger alert-dismissible fade in" role="alert"></div>
                    <div id="success" style="display:none" class="alert alert-success alert-dismissible"></div>
                     <div class="table-responsive" id="tabla_contenido">
                    <table class="table">
                        <thead class="table-header">
                            <th>Cobertura</th>                        
                            <th>Matricula</th>
                            <th>sin EC</th>
                             <th>Alta</th>
                             <th>Baja</th>
                            <th></th>
                        </thead>
                        <tbody id="myTable">
                        @foreach ($anexos as $dato)
                       <tr>
                       <td>{!!$dato->Cobertura !!}<input type="hidden" class="idAnexo" name="idAnexo" value="{!!$dato->IdAnexo !!}"></td>
                       <td>{!! $dato->Matricula !!}</td>
                       <td>
                       @if($dato->cheques > 0)
                     	<input type="checkbox" name="EC" checked>
                       @else
                       <input type="checkbox" name="EC">
                       @endif
                       </td>
                       <td>{{ date('d/m/Y', strtotime($dato->Alta))}}</td>
                       @if($dato->Baja!=null)
                       <td>{{ date('d/m/Y', strtotime($dato->Baja))}}</td>
                       @else
                       <td></td>
                       @endif
                       <td><a class="btn btn-sm btn-success" href="{{URL::to('contratos/its/anexos/'.$dato->IdAnexo)}}"><i class="fa fa-edit"></i> </a> <a class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a></td>
                       </tr>
                            @endforeach 
                        </tbody>
                    </table>
                     
                </div>
                    </div>
            </div>
            <div class="col-sm-4">
            <div class="panel panel-default">
            <div class="panel-body">
                <div class="form-group col-md-12" style="overflow: scroll; height: 200px;">
                           <h3 class="module-title">Otros contratos</h3>
                            <table class="table">
                        <thead class="table-header">
                            <th>Fecha Contrato</th>                        
                            <th>Tipo</th>
                            <th>Baja</th>
                        </thead>
                        
                        <tbody id="myTable" >
                       @foreach ($otros_contratos as $dato)
                       <tr style="cursor:pointer;"ondblclick='window.open("/contratos/its/"+$(this).closest("tr").find(".contratoID").val(),"_blank");'>
                       <td>{{ date('d/m/Y', strtotime($dato->FechaContrato))}}<input type="hidden" name="id" class="contratoID" value="{!! $dato->IDContrato !!}"></td>
                       <td>{!! $dato->TipoContrato !!}</td>
                       <td>{{ date('d/m/Y', strtotime($dato->Baja))}}</td>
                       </tr>
                            @endforeach 
                        </tbody>
                        
                    </table>
                                            
                        </div>
                        </div>

            </div>
            <div class="panel panel-default">
            <div class="panel-body">
            <div class="form-group col-md-12" style="overflow: scroll;height: 200px;">
                            <h3 class="module-title">Cheques</h3>
                            <table class="table">
                        <thead class="table-header">
                            <th>ID</th>                        
                            <th>EC</th>
                            <th>Estado</th>
                            <th>Enviado</th>
                        </thead>
                        
                        <tbody id="myTable" >
                       @foreach ($cheques as $dato)
                       <tr>
                       <td>{!! $dato->IdCheque !!}</td>
                       <td>{!! $dato->EC !!}</td>
                       <td>{!! $dato->estado !!}</td>
                       <td>{{ date('d/m/Y', strtotime($dato->enviado))}}</td>
                       </tr>
                            @endforeach 
                        </tbody>
                        
                    </table>
                                            
                        </div>
                    
                        </div>

            </div>
            </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

 $(document).on('focusin','.datepicker',function(){
         $(this).datepicker({
        format: "dd/mm/yyyy",
        dateFormat: 'yy-mm-dd',
        language: "es",
        autoclose: true
    });

         $(this).selectpicker("data-live-search","true");

    });

  $(".bt-edit").click(function(){

    
    $ID = $("#id").val();

    $form = $("#form_contrato").serialize();
  
     setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '/contratos/its/'+$ID+'/guardar',
                data : {'datos' : $("#form_contrato").serialize()},
                success : function(data){
                    console.log(JSON.stringify(data));
                    bootbox.alert({
                            message: data,
                            callback: function () {
                                location.reload();
                            }
                        });
                    $("#loading").hide();

                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

});



 $(document).on('click','.btn-danger',function(){

    $ID = $(this).closest("tr").find('.idAnexo').val();

bootbox.confirm({
     title: "Eliminar Registro",
    message: "<i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> ¿Está seguro que desea eliminar el registro?</strong> Tenga en cuenta que esta acción es irreversible.",
    buttons: {
        cancel: {
            label: '<i class="fa fa-times"></i> Cancelar'
        },
        confirm: {
            label: '<i class="fa fa-check"></i> Aceptar'
        }
    },
    callback: function (result) {

            if(result == true){
                
                setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('contratos/borrar_anexo')}}',
                        data : {'ID':$ID},            
                        success : function(data){
                            console.log(JSON.stringify(data));
                            $("#success").empty();
                            $("#success").append(data);
                            $("#success").show();
                            location.reload();
                            $("#loading").hide();
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);

            } 
    }
});

});


 $("#eliminar").click(function(){
    bootbox.confirm({
        title: "Eliminar Contrato",
        message: "<i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> ¿Está seguro que desea eliminar el Contrato?</strong> Tenga en cuenta que esta acción es irreversible.",
        buttons: {
            cancel: {
            label: '<i class="fa fa-times"></i> Cancelar'
        },
        confirm: {
            label: '<i class="fa fa-check"></i> Aceptar'
        }
        },
        callback: function (result) {
            if(result == true){
                $ID = $("#id").val();
                $tabla = 'its_contratos';
                $field = "IDContrato";

                setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('contratos/borrar')}}',
                        data : {'ID':$ID,'tabla': $tabla,'field':$field},            
                        success : function(data){
                            window.location = "/contratos/its";
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);
            }
        }
    });

});

</script>

@endsection