@extends('layouts.app')
@section('content')

   
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="row"> 
                    <div class="panel-heading" id="sanciones-header">                    
                        <div class="col-md-12">
                            <h3 class="module-title">Añadir nueva cobertura</h3>
                        </div>
                        <div class="col-md-12">
                            <div>
                                <ul class="nav navbar-nav">
                                    <li><a href="#" class="bt-edit" id="btn-save"><i class="fa fa-save"> </i> Guardar</a></li>
                                    <li><a href="#" id="volver"><i class="fa fa-arrow-left"> </i> Volver</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="panel-body" style="border-top:2px solid #335599;">                        
                    <form class="inline-form" id="form_sancion">
                        <div class="form-group col-md-4">
                            <label for="Beneficiario">Cobertura</label>
                          
                            <input type="text" class="form-control" name="cobertura" id="cobertura" value="">
                   
                                           
                        </div>
                        <div class="form-group col-md-2">
                            <label for="codigo">Tipo Contrato</label>
                     
                          <select class="selectpicker" data-live-search="true" title="Buscar..." id="tipo_contrato" name="tipo_contrato">
		                 <option selected></option>
		                   @foreach ($tipo_contrato as $tipo)
		                    <option>{{ $tipo->TipoServicio}}</option>
		                    @endforeach
		                </select>

                        </div>
                        <div class="form-group col-md-4">
                            <label for="empresa">Descripción</label>
                        
                            <input type="text" id="empresa" class="form-control" name="descripcion" id="descripcion" value="">
                                                 
                        </div>
                        <div class="form-group col-md-2">
                            <label for="cif">Tipo</label>
                               <input type="text" class="form-control" name="tipo" id="tipo" value="">                         
                        </div>
                     
                        <div class="form-group col-md-3">
                            <label for="matricula">Precio</label>
                                  <input type="text" class="form-control" name="precio" id="precio" value="">                         
                        </div>
                        <div class="form-group col-md-3">
                            <label for="matricula">PrecioT</label>
                           
                                  <input type="text" class="form-control" name="precioT" id="precioT" value="">  
                                               
                        </div>

                        <div class="form-group col-md-3">
                            <label for="matricula">PrecioR</label>
                           
                                 <input type="text" class="form-control" name="precioR" id="precioR" value="">  
                                               
                        </div>
                                 
                    </form>
                    </div>

                </div>
            </div>
</div>

<script type="text/javascript">

 $(document).on('focusin','.datepicker',function(){
         $(this).datepicker({
        format: "dd/mm/yyyy",
        dateFormat: 'yy-mm-dd',
        language: "es",
        autoclose: true
    });

         $(this).selectpicker("data-live-search","true");

    });

$("#btn-save").click(function(){
    
    $form = $("#form_sancion").serialize();
  
     setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/its/addcobertura')}}',
                data : {'datos' : $("#form_sancion").serialize()},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    if(data.includes("error")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                //location.reload();
                            }
                        });
                    }else{
                         bootbox.alert({
                            message: data,
                            callback: function () {
                                 window.location = '/contratos/its';
                            }
                        });
                    }
                   
                    $("#loading").hide();

                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);



});


</script>

@endsection