@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xl-12 col-md-offset-0">
            <div class="panel panel-default">
                <div class="row"> 
                    <div class="panel-heading" id="sanciones-header">                    
                        <div class="col-md-8">
                         @foreach ($tarjetas as $tarjeta)
                            <h3 class="module-title">{{$tarjeta->tarjeta}}</h3>
                          @endforeach
                        </div>
                        <div class="col-md-4">
                            <div>
                                <ul class="nav navbar-nav">
                                    <li><a href="#" id="guardar"><i class="fa fa-save"> </i> Guardar</a></li>                                 

                                    <li><a href="#" id="volver"><i class="fa fa-arrow-left"> </i> Volver</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="panel-body" style="border-top:2px solid #335599;">                        
                    <form class="inline-form" id="form_sancion">
                         <div class="form-group col-md-2">
                            <label for="matricula">Tarjeta</label>
                             @foreach ($tarjetas as $tarjeta)
                           <input type="text" class="form-control" name="numTarjeta" id="numTarjeta" value="{{$tarjeta->numTarjeta}}" disabled>
                           @endforeach                     
                        </div>
                        <div class="form-group col-md-3">
                            <label for="empresa">Empresa</label>
                            <select class="selectpicker" data-live-search="true" title="Buscar..." id="empresa" name="empresa">
                            @foreach ($clientes as $cliente)
                               <option value="{{$cliente->CIF}}" selected>{{$cliente->Empresa}}</option>
                             @endforeach
                             @foreach ($empresas as $empresa)
                              <option value="{{$empresa->CIF}}">{{$empresa->Empresa}}</option>
                              @endforeach
                            </select>                 
                        </div>
                        <div class="form-group col-md-3">
                            <label for="empresa">Matricula</label>
                            <select class="selectpicker" data-live-search="true" title="Buscar..." id="matricula" name="matricula">
                            @foreach ($tarjetas_matriculas as $tarjetas_matricula)
                               <option value="{{$tarjetas_matricula->matricula}}" selected>{{$tarjetas_matricula->matricula}}</option>
                             @endforeach
                             @foreach ($matriculas as $matricula)
                              <option>{{$matricula->Matricula}}</option>
                              @endforeach
                            </select>                 
                        </div>
                        <div class="form-group col-md-2">
                            <label for="expediente">Contratante</label>
                            <select class="selectpicker" data-live-search="true" title="Buscar..." id="contratante" name="contratante">
                                 <option selected></option>
                                    @foreach ($tarjetas as $tarjeta)
                                    @if($tarjeta->empCom==10)
                             <option value="10" selected>Europa ITS Soluciones SL</option>
                              <option value="9">Soler y Martín Eurogestión, S.L.</option>
                            @elseif($tarjeta->empCom==9)
                              <option value="10" >Europa ITS Soluciones SL</option>
                              <option value="9" selected>Soler y Martín Eurogestión, S.L.</option>
                                @else
                              <option value="10" >Europa ITS Soluciones SL</option>
                              <option value="9" >Soler y Martín Eurogestión, S.L.</option>
                             @endif
                                    @endforeach
                            </select>                       
                        </div>
                        <div class="form-group col-md-4">
                       <label for="matricula">Entidad</label>
                             <select class="selectpicker" data-live-search="true" title="Buscar..." id="entidad" name="entidad">
                                 <option selected></option>
                                    @foreach ($tarjetas as $tarjeta)
                                     @if($tarjeta->entidad==1)
                            <option value="1" selected>CAIXA</option>
                            <option value="2">C2A</option>
                            <option value="3">FRANCE TRUCK</option>
                            @elseif($tarjeta->entidad==2)
                            <option value="1" >CAIXA</option>
                            <option value="2" selected>C2A</option>
                            <option value="3">FRANCE TRUCK</option>
                            @elseif($tarjeta->entidad==3)
                            <option value="1" >CAIXA</option>
                            <option value="2" >C2A</option>
                            <option value="3" selected>FRANCE TRUCK</option>
                            @elseif($tarjeta->entidad==null)
                            <option value="1" >CAIXA</option>
                            <option value="2" >C2A</option>
                            <option value="3">FRANCE TRUCK</option>
                            @else
                            <option value="1" >CAIXA</option>
                            <option value="2" >C2A</option>
                            <option value="3">FRANCE TRUCK</option>
                            <option value="{{$tarjeta->entidad}}" selected>{{ $tarjeta->entidad }}</option>
                            @endif
                            @endforeach
                            </select>                              
                        </div>
                        <div class="form-group col-md-2">
                        @foreach ($tarjetas as $tarjeta)
                            <label for="matricula">Entregada</label>
                              @if($tarjeta->entregada!=null)
                             <input type="text" class="form-control datepicker" name="entregada" value="{!!date('d/m/Y', strtotime($tarjeta->entregada)) !!}">
                             @else
                              <input type="text" class="form-control datepicker" name="entregada" value="">
                              @endif
                               @endforeach                               
                        </div>
                         <div class="form-group col-md-2">
                        @foreach ($tarjetas as $tarjeta)
                            <label for="matricula">Caducidad</label>
                            @if($tarjeta->caducidad!=null)
                             <input type="text" class="form-control datepicker" name="caducidad" value="{!!date('d/m/Y', strtotime($tarjeta->caducidad))!!}">
                             @else
                              <input type="text" class="form-control datepicker" name="caducidad" value="">
                              @endif
                               @endforeach                               
                        </div>
                         <div class="form-group col-md-2">
                        @foreach ($tarjetas as $tarjeta)
                            <label for="matricula">Baja</label>
                            @if($tarjeta->baja!=null)
                             <input type="text" class="form-control datepicker" name="baja" value="{!!date('d/m/Y', strtotime($tarjeta->baja))!!}">
                             @else
                              <input type="text" class="form-control datepicker" name="baja" value="">
                              @endif
                               @endforeach                               
                        </div>
                      <div class="form-group col-md-2">
                      @foreach ($tarjetas as $tarjeta)
                            <label for="matricula">PIN</label>
                           <input type="text" class="form-control" name="pin" value="{{$tarjeta->pin}}">  
                             @endforeach 
                        </div>
                          <div class="form-group col-md-2">
                      @foreach ($tarjetas as $tarjeta)
                            <label for="matricula">Referencia</label>
                           <input type="text" class="form-control" name="referencia" value="{{$tarjeta->referencia}}">  
                             @endforeach 
                        </div>  
                        <div class="form-group col-md-2">
                      @foreach ($tarjetas as $tarjeta)
                            <label for="matricula">Nº Cajero</label>
                           <input type="text" class="form-control" name="noDeCajero" value="{{$tarjeta->noDeCajero}}">  
                             @endforeach 
                        </div>
                        
                        <div class="form-group col-md-2">
                        @foreach ($tarjetas as $tarjeta)
                            <label for="matricula">Recepción confirmada</label>
                            @if($tarjeta->recepcion!=null)
                             <input type="text" class="form-control datepicker" name="recepcion" value="{!!date('d/m/Y', strtotime($tarjeta->recepcion))!!}">
                             @else
                              <input type="text" class="form-control datepicker" name="recepcion" value="">
                              @endif
                               @endforeach                               
                        </div>
                           <div class="form-group col-md-2">
                         @foreach ($tarjetas as $tarjeta)
                         @if($tarjeta->docFirmado == 1)
                          <li><label for="matricula">Doc. Firmado</label> <input type="checkbox" name="docFirmado" value="" checked></li>
                          @else
                           <li><label for="matricula">Doc. Firmado</label> <input type="checkbox" name="docFirmado" value=""></li>
                           @endif
                             @endforeach 
                        </div>
                            <div class="form-group col-md-6">
                            @foreach ($tarjetas as $tarjeta)
                            <label for="matricula">Notas</label>
                            <textarea type="textarea" class="form-control" name="notas" style="" >{{$tarjeta->notas}}</textarea> 
                               @endforeach
                            </div>                            
                        </div>     
                    </form>
                    </div>
                       <div class="panel panel-default">
              <div class="row"> 
                    <div class="panel-heading" id="detalles-header">                    
                        <div class="col-md-6">
                            <h3 class="module-title">Cargas en la tarjeta</h3>
                        </div>
                    </div>
                    </div>
                    <div id="error" style="display:none" class="alert alert-danger alert-dismissible fade in" role="alert"></div>
                    <div id="success" style="display:none" class="alert alert-success alert-dismissible"></div>
                     <div class="table-responsive" id="tabla_contenido">
                </div>
                    </div>                        
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

$(function() {

    $("#volver").click(function(){

        window.history.go(-1); return false;
    });
     $(document).on('focusin','.datepicker',function(){
         $(this).datepicker({
        format: "dd/mm/yyyy",
        dateFormat: 'yy-mm-dd',
        language: "es",
        autoclose: true
    });

         $(this).selectpicker("data-live-search","true");

    });


 $("#guardar").click(function(){


    
    $form = $("#form_sancion").serialize();
    $numTarjeta = $("#numTarjeta").val();
  
     setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '/contratos/tarjetas/'+$numTarjeta+'/guardar',
                data : {'datos' : $("#form_sancion").serialize()},            
                success : function(data){
                  if(data==null){
                       bootbox.alert({
                            message: "no se ha insertado ningun dato",
                            callback: function () {
                                location.reload();
                            }
                        });
                  }
                    console.log(JSON.stringify(data));
                    else if(data.includes("SQLSTATE")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                location.reload();
                            }
                        });
                    }else{
                         bootbox.alert({
                            message: data,
                            callback: function () {
                                 window.location = '/contratos/tarjetas';
                            }
                        });
                    }
                   
                    $("#loading").hide();

                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);
});

 });
</script>
@endsection