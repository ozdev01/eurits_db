@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xl-12 col-md-offset-0">
            <div class="panel panel-default">
                <div class="row"> 
                    <div class="panel-heading" id="sanciones-header">                    
                        <div class="col-md-8">
                       
                            <h3 class="module-title">Nueva Tarjeta</h3>
                     
                        </div>
                        <div class="col-md-4">
                            <div>
                                <ul class="nav navbar-nav">
                                    <li><a href="#" id="guardar"><i class="fa fa-save"> </i> Guardar</a></li>                                 
                                    <li><a href="#" id="volver"><i class="fa fa-arrow-left"> </i> Volver</a></li>
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="panel-body" style="border-top:2px solid #335599;">                        
                    <form class="inline-form" id="form_sancion">
                         <div class="form-group col-md-4">
                            <label for="matricula">Tarjeta</label>
                           <input type="text" class="form-control" name="numTarjeta" id="numTarjeta" value="">               
                        </div>
                        <div class="form-group col-md-3">
                            <label for="empresa">Empresa</label>
                            <select class="selectpicker" data-live-search="true" title="Buscar..." id="empresa" name="empresa">
                             @foreach ($empresas as $empresa)
                              <option value="{{$empresa->CIF}}">{{$empresa->Empresa}}</option>
                              @endforeach
                            </select>                 
                        </div>
                        <div class="form-group col-md-3">
                            <label for="empresa">Matricula</label>
                            <select class="selectpicker" data-live-search="true" title="Buscar..." id="matricula" name="matricula">
                             @foreach ($matriculas as $matricula)
                              <option>{{$matricula->Matricula}}</option>
                              @endforeach
                            </select>                 
                        </div>
                        <div class="form-group col-md-2">
                            <label for="expediente">Contratante</label>
                            <select class="selectpicker" data-live-search="true" title="Buscar..." id="contratante" name="contratante">
                                    @foreach ($contratantes as $tarjeta)
                                    @if($tarjeta->empCom==10)
                             <option value="10">Europa ITS Soluciones SL</option>
                            @elseif($tarjeta->empCom==9)
                              <option value="9">Soler y Martín Eurogestión, S.L.</option>
                             @endif
                                    @endforeach
                            </select>                       
                        </div>
                        <div class="form-group col-md-2">
                       <label for="matricula">Entidad</label>
                             <select class="selectpicker" data-live-search="true" title="Buscar..." id="entidad" name="entidad">
                                    @foreach ($entidades as $tarjeta)
                                     @if($tarjeta->entidad==1)
                            <option value="1" >CAIXA</option>
                            @elseif($tarjeta->entidad==2)
                            <option value="2">C2A</option>
                            @elseif($tarjeta->entidad==3)
                            <option value="3">FRANCE TRUCK</option>
                            @else
                            <option value="{{$tarjeta->entidad}}">{{ $tarjeta->entidad }}</option>
                            @endif
                            @endforeach
                            </select>                              
                        </div>
                        <div class="form-group col-md-2">
                            <label for="matricula">Entregada</label>
                             <input type="text" class="form-control datepicker" name="entregada" value="">                               
                        </div>
                         <div class="form-group col-md-2">
                            <label for="matricula">Caducidad</label>
                             <input type="text" class="form-control datepicker" name="caducidad" value="">  
                        </div>
                         <div class="form-group col-md-2"> 
                            <label for="matricula">Baja</label>
                             <input type="text" class="form-control datepicker" name="baja" value="">                                   
                        </div>
                      <div class="form-group col-md-2">
                            <label for="matricula">PIN</label>
                           <input type="text" class="form-control" name="pin" value="">  
                        </div>
                          <div class="form-group col-md-2">
                            <label for="matricula">Referencia</label>
                           <input type="text" class="form-control" name="referencia" value="">  
                        </div>  
                        <div class="form-group col-md-2">
                            <label for="matricula">Nº Cajero</label>
                           <input type="text" class="form-control" name="noDeCajero" value="">  
                        </div>
                        <div class="form-group col-md-2">
                            <label for="matricula">Recepción confirmada</label>
                             <input type="text" class="form-control datepicker" name="recepcion" value="">                
                        </div>
                            <div class="form-group col-md-4">
                            <label for="matricula">Notas</label>
                            <textarea type="textarea" class="form-control" name="notas" style="" ></textarea> 
                            </div>
                             <div class="form-group col-md-2">
                          <label for="matricula">Doc. Firmado</label> <input type="checkbox" name="docFirmado" value="" checked>
                        </div>                            
                        </div>     
                    </form>
                    </div>                      
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

$(function() {

    $("#volver").click(function(){

        window.history.go(-1); return false;
    });
     $(document).on('focusin','.datepicker',function(){
         $(this).datepicker({
        format: "dd/mm/yyyy",
        dateFormat: 'yy-mm-dd',
        language: "es",
        autoclose: true
    });

         $(this).selectpicker("data-live-search","true");

    });


 $("#guardar").click(function(){


    
    $form = $("#form_sancion").serialize();
  
     setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '/contratos/tarjetas/store_tarjeta',
                data : {'datos' : $("#form_sancion").serialize()},            
                success : function(data){
                    //console.log(JSON.stringify(data));
                       if(data==""){
                       bootbox.alert({
                            message: "no se ha insertado ningun dato",
                            callback: function () {
                                //location.reload();
                            }
                        });
                  }
                    if(data.includes("SQLSTATE")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                //location.reload();
                            }
                        });
                    }else{
                         bootbox.alert({
                            message: data,
                            callback: function () {
                                 window.location = 'contratos/tarjetas';
                            }
                        });
                    }
                   
                    $("#loading").hide();

                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);
});

 });
</script>
@endsection