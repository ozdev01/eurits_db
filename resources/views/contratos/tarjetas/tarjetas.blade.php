@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xl-12 col-md-offset-0">
            <div class="panel panel-default">
                <div class="row"> 
                    <div class="panel-heading" id="sanciones-header">                    
                        <div class="col-md-8">
                            <h3 class="module-title">Tarjetas ITS</h3>
                        </div>
                        <div class="col-md-4">
                            <div>
                                <ul class="nav navbar-nav">
                                    <li><a href="{{url('contratos/tarjetas/nueva')}}"><i class="fa fa-plus"></i>Añadir</a></li>                             
                                    <li><a href="#" id="volver"><i class="fa fa-arrow-left"> </i> Volver</a></li>
                                    <li><a href="#" id="excel"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Excel</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="panel-body" style="border-top:2px solid #335599;">                        
                    <form class="inline-form" id="form_sancion">
                        <div class="form-group col-md-4">
                            <label for="matricula">Buscar por Cliente</label>
                             <select class="selectpicker" data-live-search="true" title="Buscar..." id="cliente" name="clientes">
                                 <option selected></option>
                                    @foreach ($clientes as $cliente)
                                    <option value="{{$cliente->CIF}}">{{ $cliente->Empresa}}</option>
                                    @endforeach
                            </select>                            
                            </div>                               
                        </div>  
                    </form>
                       <div class="table-responsive" id="tabla_contenido" style="border-top: none;">
                              <table class="table">
                        <thead class="table-header">
                            <th>Tarjeta</th>                        
                            <th>PIN</th>
                            <th>Matricula</th>
                            <th>Caducidad</th>
                            <th>DocFirmado</th>
                            <th>Entidad</th>
                            <th>Contratante</th>
                            <th></th>
                        </thead>
                        <tbody id="myTable">
                        @foreach ($tarjetas as $tarjeta)
                        <tr>
                            <td>{{ $tarjeta->numTarjeta }} </td>
                            <td>{{ $tarjeta->pin }} </td>
                            <td>{{ $tarjeta->matricula }} </td>
                            <td>{{ date('d - m - Y', strtotime($tarjeta->caducidad))}}</td>                            
                           
                            @if($tarjeta->docFirmado==1)
                             <td>Si</td>
                            @else
                            <td>No</td>
                            @endif
                            @if($tarjeta->entidad==1)
                            <td>CAIXA</td>
                            @elseif($tarjeta->entidad==2)
                            <td>C2A</td>
                            @elseif($tarjeta->entidad==3)
                            <td>FRANCE TRUCK</td>
                            @else
                            <td>{{ $tarjeta->entidad }}</td>
                            @endif
                            @if($tarjeta->empCom==10)
                             <td>Europa ITS Soluciones SL</td>
                            @elseif($tarjeta->empCom==9)
                              <td>Soler y Martín Eurogestión, S.L.</td>
                            @else
                             <td></td>
                             @endif
                            <td><a class="btn btn-sm btn-success" href={!! url('contratos/tarjetas/'.$tarjeta->numTarjeta) !!}#"><i class="fa fa-edit"></i> </a></td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                    </div>                        
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $("#volver").click(function(){
        window.history.go(-1); return false;
    });
     $(document).on('focusin','.datepicker',function(){
         $(this).datepicker({
        format: "dd/mm/yyyy",
        dateFormat: 'yy-mm-dd',
        language: "es",
        autoclose: true
    });

         $(this).selectpicker("data-live-search","true");

    });

     $( ".selectpicker" ).change(function(){

    $cif = $("#cliente").find("option:selected").val();
   
    setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/tarjetas/buscar')}}',
                data : {'cif': $cif},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('#myTable').html(data);
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

});

 $("#excel").click(function(){

    $cif = $("#cliente").find("option:selected").val();

     $dialog = bootbox.dialog({
                        message: '<p class="text-center">Generando su Excel, espere por favor...</p>',
                        closeButton: false
                    });
   
    setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/tarjetas/exportar_excel_tarjetas')}}',
                data : {'cif': $cif},            
               success : function(data){
                    window.location = this.url;
                    $dialog.modal('hide');
                },
                error : function(data){
                    //console.log(JSON.stringify(data));
                    bootbox.alert("Ha ocurrido un error generando su Excel.");
                    $dialog.modal('hide');
                }
            });
        }, 500);

 });


</script>
@endsection