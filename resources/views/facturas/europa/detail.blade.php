@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-xl-12 col-md-offset-0">
            <div class="panel panel-default">
                <div class="row"> 
                    <div class="panel-heading" id="sanciones-header">                    
                        <div class="col-md-12">
                            <h3 class="module-title">FACTURA {!! $factura->ClaveLiq !!}</h3>
                           <input type="hidden" class="id" name="clave" id="id" value="{!!$factura->ClaveLiq !!}">
                        </div>
                        <div class="col-md-12">
                            <div>
                                <ul class="nav navbar-nav">
                                    <li><a href="#" class="bt-edit"><i class="fa fa-save"> </i> Guardar</a></li>
                                    <li><a href="#" class="bt-export bt-export_factura"><i class="fa fa-print"> </i> Imprimir factura</a></li>
                                    <li><a href="#" class="bt-export bt-export_sobre"><i class="fa fa-print"> </i> Imprimir Sobre</a></li>
                                    <li><a href="#" id="eliminar"><i class="fa fa-trash"> </i> Eliminar</a></li>
                                    <li><a href="#" id="volver"><i class="fa fa-arrow-left"> </i> Volver</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="panel-body" style="border-top:2px solid #335599;">                        
                    <form class="inline-form" id="form_contrato">
                           <div class="form-group col-md-4">
                            <label for="empresa">Contratante</label>
                             <input type="text" class="form-control" name="numfactura" value="{!! $contratante->razonSocial !!}" disabled>
                        </div>
                            <div class="form-group col-md-2">
                            <label for="codigo">Nª Factura</label>
                            @if($factura->Entidad ==10)
                            <input type="text" class="form-control" name="numfactura" id="numfactura" value="E-{!! $factura->NumLiq !!}" disabled>
                            @else
                            <input type="text" class="form-control" name="numfactura" id="numfactura" value="M-{!! $factura->NumLiq !!}" disabled>
                            @endif
                        </div>
                         <div class="form-group col-md-2">
                            <label for="codigo">Tipo</label>
                            @if($factura->TipoLiq =='A')
                            <input type="text" class="form-control" name="TipoLiq" value="Abono" disabled>
                            <input type="hidden" class="form-control" name="TipoLiq" id="TipoLiq" value="Abono">
                            @else
                            <input type="text" class="form-control" name="TipoLiq" value="Factura" disabled>
                            <input type="hidden" class="form-control" name="TipoLiq" id="TipoLiq" value="Factura" disabled>
                            @endif
                        </div>
                         <div class="form-group col-md-2">
                            <label for="codigo">Clase</label>
                            @if($factura->ClaseLiq == 'C')
                            <input type="text" class="form-control" name="ClaseLiq" value="Corriente" disabled>
                            <input type="hidden" class="form-control" name="ClaseLiq" value="Corriente">
                            @elseif($factura->ClaseLiq == 'T')
                            <input type="text" class="form-control" name="ClaseLiq" value="De contrato" disabled>
                            <input type="hidden" class="form-control" name="ClaseLiq" value="De contrato">
                            @elseif($factura->ClaseLiq == 'F')
                            <input type="text" class="form-control" name="ClaseLiq" value="Provisión de fondos" disabled>
                            <input type="hidden" class="form-control" name="ClaseLiq" value=Provisión de fondos ">
                            @endif
                        </div> 
                        <div class="form-group col-md-2">
                            <label for="codigo">Origen</label>
                            <input type="text" class="form-control" name="facturaorigen" value="{!! $factura->FacturaOrigen !!}" disabled>
                            <input type="hidden" class="form-control" name="origen" id="origen" value="{!! $factura->FacturaOrigen !!}">
                        </div>
                           <div class="form-group col-md-2">
                            <label for="codigo">Nª Contrato</label>
                            <input type="text" class="form-control" name="codContrato" value="{!! $factura->codContrato !!}" disabled>
                            <input type="hidden" class="form-control" name="idcontrato" id="idcontrato" value="{!! $factura->codContrato !!}">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="matricula">Empresa</label>
                            <input type="text" class="form-control" name="empresa" value="{!! $factura->razonSocial !!}" disabled>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="codigo">Cif</label>
                            <input type="text" class="form-control" name="cifempresa" id="cifempresa" value="{!! $factura->cif !!}" disabled>
                            <input type="hidden" class="form-control" name="cif" value="{!! $factura->cif !!}">
                        </div>
                        <div class="form-group col-md-2">
                            <label for="cif">Cod. Contacto</label>
                            <input type="text" id="codContacto" class="form-control" name="codContacto" value="{!! $factura->codContacto !!}" disabled >             
                        </div>
                         <div class="form-group col-md-2">
                            <label for="codigo">Fecha Emisión</label>
                            @if($factura->fecha!=null)
                            <input type="text" class="form-control" name="fechaemision" value="{!! date('d/m/Y', strtotime($factura->fecha)) !!}">
                            @else
                            <input type="text" class="form-control" name="fechaemision" value="">
                            @endif
                        </div>
                         <div class="form-group col-md-4">
                            <label for="cif">Cuenta Bancaria</label>
                            <select class="selectpicker" data-live-search="true" title="Buscar..." id="cc" name="cc">
                            @if($factura->CuentaIngBanc !=null)
                             <option value="{{$factura->CuentaIngBanc}}" selected>{{$factura->CuentaIngBanc}}</option>
                             @endif
                                <option value="ES2700491247032810195302">ES2700491247032810195302</option>
                                <option value="ES7921002534830210086691">ES7921002534830210086691</option>
                                <option value="ES8321002534820210061157">ES8321002534820210061157</option>
                                <option value="ES9021002534870210093087">ES9021002534870210093087</option>
                            </select>                  
                        </div>
                         <div class="form-group col-md-5">
                            <label for="cif">Notas</label>
                            <textarea name="notas" class="form-control" rows="4">{{$factura->Notas}}</textarea>                
                        </div>
                          <div class="form-group col-md-2">
                          <label>Con provisión de fondo</label>
                          @if($factura->conProvisionFondos == 1)
                         <input type="checkbox" name="provisionfondos" value="" id="provisionfondos" checked>
                         @else
                         <input type="checkbox" name="provisionfondos" value="" id="provisionfondos">
                         @endif
                         </div>
                        <div class="form-group col-md-2">
                        <label>Cambiar IVA</label>
                        <input type="number" id="codContacto" class="form-control" name="iva" value="{{$factura->IVA}}">
                        </div>
                            <div class="form-group col-md-6">
                        <label for="cif">Importe</label>
                        <ul style="    border: 1px solid gray; border-radius: 4px; box-shadow: inset 0 1px 1px rgba(0,0,0,.075); padding: 20px;">
                        <ul class="list-inline" style="padding: 5px !important">
                        <li><label style="width: 150px">Base Imponible:</label></li>
                        <li> <div class="input-group"><span class="input-group-addon">€</span><input type="number" step="0.01"   class="form-control" name="baseimponible" id="baseimponible" value="{{$baseimponible}}" readonly></div></li>
                        </ul>
                        <ul class="list-inline" style="padding: 5px !important">
                        <li><label style="width: 150px">IVA {{$factura->IVA}} %:</label></li>
                        <li><div class="input-group"><span class="input-group-addon">€</span><input type="number" step="0.01"  class="form-control" name="ivaprecio" id="iva" value="{{$iva}}" readonly></div></li>
                        </ul>
                        @if($factura->conProvisionFondos == 1)
                          <ul class="list-inline" style="padding: 5px !important">
                        <li><label style="width: 150px">Provisión fondos:</label></li>
                        <li><div class="input-group"><span class="input-group-addon">€</span><input type="number" step="0.01" class="form-control" name="provision" id="provision" value="{{$provision}}" readonly></div></li>
                        </ul>
                        @endif
                        <ul class="list-inline" style="padding: 5px !important">
                        <li><label style="width: 150px">Total:</label></li>
                        <li><div class="input-group"><span class="input-group-addon">€</span><input type="number" step="0.01" class="form-control" name="total" id="total" value="{{$total}}"></div></li>
                        </ul>
                        </ul>
                        </div>
                    </form>
                    </div>
                    </div>
                <div class="panel panel-default">
              <div class="row"> 
                    <div class="panel-heading" id="detalles-header">                    
                        <div class="col-md-6">
                            <h3 class="module-title">Detalles</h3>
                        </div>
                    </div>
                    </div>
                    <div id="error" style="display:none" class="alert alert-danger alert-dismissible fade in" role="alert"></div>
                    <div id="success" style="display:none" class="alert alert-success alert-dismissible"></div>
                     <div class="table-responsive" id="tabla_contenido">
                    <table class="table">
                        <thead class="table-header">
                            <th>Código</th>
                            @if($factura->ClaseLiq == 'T')
                            <th>Matricula</th>
                            @endif                        
                            <th>Concepto</th>
                            @if($factura->ClaseLiq != 'T')
                            <th>Unidades</th>
                            <th>Precio U.</th>
                            <th>Total</th>
                              <th></th>
                            @endif  
                        </thead>
                        <tbody id="myTable">
                        @foreach ($detalles as $detalle)
                       <tr>
                        <td>{{$detalle->CODIGO}}</td>
                         @if($factura->ClaseLiq == 'T')
                         <td>{{$detalle->MATRICULA}}</td>
                         @endif
                        <td><input type="hidden" name="iddetalle" class="iddetalle" value="{{$detalle->iddetalle}}"><input type="text" class="form-control concepto" name="" value="{{$detalle->CONCEPTO}}" placeholder=""></td>
                         @if($factura->ClaseLiq != 'T')
                        <td><input type="text" class="form-control unidades" name="" value="{{$detalle->UNIDADES}}" placeholder=""></td>
                        <td><input type="number" class="form-control precio" name="" step="0.01" value="{{$detalle->PRECIO}}" placeholder=""></td>
                        <td><input type="number" class="form-control total" name="" step="0.01" value="{{$detalle->PRECIO * $detalle->UNIDADES}}" placeholder="" readonly></td> 
                        <td><a class="btn btn-sm btn-success editar" href="#"><i class="fa fa-edit"></i> </a> <a class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a></td>
                        @endif
                       </tr>
                            @endforeach
                         @if($factura->ClaseLiq != 'T')
                        <tr>
                        <td><input type="text" class="form-control codigo" name="" value="" placeholder=""></td>
                        <td><input type="text" class="form-control concepto" name="" value="" placeholder=""></td>
                        <td><input type="text" class="form-control unidades" name="" value="0" placeholder="" required></td>
                        <td><input type="number" class="form-control precio" name="" step="0.01" value="0" placeholder="" required></td>
                        <td><input type="number" class="form-control total" name="" step="0.01" value="0" placeholder="" readonly></td>
                        <td><a class="btn btn-success" id="add_detalle"><i class="fa fa-plus"></i></a></td>
                        </tr>
                         @endif
                        </tbody>
                    </table>
                </div>
                    </div>
            </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

$("#volver").click(function(){
        window.history.go(-1); return true;
    });


$(".bt-export_factura").click(function(){

    
    $clave = $("#id").val();

    $baseimponible = $("#baseimponible").val();
    $iva = $("#iva").val();
    $provision = $("#provision").val();
    $total = $("#total").val();
    $numfactura = $("#numfactura").val();

  window.location = '/facturas/europa/exportar_factura?clave='+$clave+'&baseimponible='+$baseimponible+'&iva='+$iva+'&provision='+$provision+'&total='+$total+'&numfactura='+$numfactura;



});

$(".bt-export_sobre").click(function(){

    
    $clave = $("#id").val();

    $baseimponible = $("#baseimponible").val();
    $iva = $("#iva").val();
    $provision = $("#provision").val();
    $total = $("#total").val();

  window.location = '/facturas/europa/exportar_sobre?clave='+$clave;



});

 $(document).on('focusin','.datepicker',function(){
         $(this).datepicker({
        format: "dd/mm/yyyy",
        dateFormat: 'yy-mm-dd',
        language: "es",
        autoclose: true
    });

         $(this).selectpicker("data-live-search","true");

    });

  $(".bt-edit").click(function(){

    
    $ID = $("#id").val();

    $form = $("#form_contrato").serialize();

     $total = $("#total").val();
  
     setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '/facturas/europa/editar',
                data : {'datos' : $("#form_contrato").serialize(),'clave':$ID,'total':$total},
                success : function(data){
                    console.log(JSON.stringify(data));
                    if(data.includes("SQLSTATE")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                //location.reload();
                            }
                        });
                    }else{
                         bootbox.alert({
                            message: data,
                            callback: function () {
                                 location.reload();
                            }
                        });
                    }
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

});

  $("#eliminar").click(function(){
    bootbox.confirm({
        title: "Eliminar Contrato",
        message: "<i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> ¿Está seguro que desea eliminar la factura?</strong> Tenga en cuenta que esta acción es irreversible.",
        buttons: {
            cancel: {
            label: '<i class="fa fa-times"></i> Cancelar'
        },
        confirm: {
            label: '<i class="fa fa-check"></i> Aceptar'
        }
        },
        callback: function (result) {
            if(result == true){
                $clave = $("#id").val();

                setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('facturas/europa/eliminar')}}',
                        data : {'clave':$clave},            
                        success : function(data){
                                    if(data.includes("SQLSTATE")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                //location.reload();
                            }
                        });
                    }else{
                         bootbox.alert({
                            message: data,
                            callback: function () {
                                 window.location = "/facturas/europa";
                            }
                        });
                    }
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);
            }
        }
    });

});


//eliminar el registro
  $(document).on('click','.btn-danger',function(){

                $iddetalle =  $(this).closest("tr").find('.iddetalle').val();
                $precio = $(this).closest("tr").find('.precio').val();
                $tipo = $("#TipoLiq").val();

bootbox.confirm({
        title: "Eliminar Contrato",
        message: "<i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> ¿Está seguro que desea eliminar el detalle de esta factura?</strong> Tenga en cuenta que esta acción es irreversible.",
        buttons: {
            cancel: {
            label: '<i class="fa fa-times"></i> Cancelar'
        },
        confirm: {
            label: '<i class="fa fa-check"></i> Aceptar'
        }
        },
        callback: function (result) {
            if(result == true){
             

                setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('facturas/europa/eliminar_detalle_europa')}}',
                        data : {'iddetalle':$iddetalle,'precio':$precio,'tipo':$tipo},            
                        success : function(data){
                                    if(data.includes("SQLSTATE")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                //location.reload();
                            }
                        });
                    }else{
                         bootbox.alert({
                            message: data,
                            callback: function () {
                                location.reload();
                            }
                        });
                    }
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);
            }
        }
    });

});

  $(document).on('focusout','.precio',function(){

    $unidades = $(this).closest("tr").find('.unidades').val();
    $precio = $(this).closest("tr").find('.precio').val();
    $(this).closest("tr").find('.total').val($precio * $unidades);
  });

   $(document).on('focusout','.unidades',function(){

    $unidades = $(this).closest("tr").find('.unidades').val();
    $precio = $(this).closest("tr").find('.precio').val();
    $(this).closest("tr").find('.total').val($precio * $unidades);
  });

  $(document).on('click','.editar',function(){

$iddetalle =  $(this).closest("tr").find('.iddetalle').val();
$unidades = $(this).closest("tr").find('.unidades').val();
$precio = $(this).closest("tr").find('.precio').val();
$concepto = $(this).closest("tr").find('.concepto').val();
$clave = $ID = $("#id").val();

    

setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('facturas/europa/editar_detalle')}}',
                        data : {'iddetalle':$iddetalle,'unidades':$unidades,'precio':$precio,'concepto':$concepto,'clave':$clave},            
                        success : function(data){
                            console.log(JSON.stringify(data));
                    if(data.includes("SQLSTATE")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                //location.reload();
                            }
                        });
                    }else{
                         bootbox.alert({
                            message: data,
                            callback: function () {
                                 location.reload();
                            }
                        });
                    }
                   
                    $("#loading").hide();
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);

});

  $(document).on('click','#add_detalle',function(){

$iddetalle =  $(this).closest("tr").find('.iddetalle').val();
$codigo = $(this).closest("tr").find('.codigo').val();
$unidades = $(this).closest("tr").find('.unidades').val();
$precio = $(this).closest("tr").find('.precio').val();
$concepto = $(this).closest("tr").find('.concepto').val();
$clave = $ID = $("#id").val();

    

setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('facturas/europa/insertar_detalle')}}',
                        data : {'iddetalle':$iddetalle,'unidades':$unidades,'precio':$precio,'concepto':$concepto,'clave':$clave,'codigo':$codigo},            
                        success : function(data){
                            console.log(JSON.stringify(data));
                    if(data.includes("SQLSTATE")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                //location.reload();
                            }
                        });
                    }else{
                         bootbox.alert({
                            message: data,
                            callback: function () {
                                 location.reload();
                            }
                        });
                    }
                   
                    $("#loading").hide();
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);

});

  

</script>

@endsection