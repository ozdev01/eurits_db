@extends('layouts.app')
@section('content')

<div class="container">
    <div class="row">
        <div class="col-sm-2">
            <h4>Factura</h4>
            <div class =filtros>
                 <a data-toggle="collapse" href="#fechallegada" aria-expanded="false" aria-controls="fechallegada"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> fecha de emisión</h5></a>
                <div class="collapse" id="fechallegada">
                <input type="text" class="form-control datepicker" name="fecha_alta_inicio" id="fecha_alta_inicio" value="" placeholder="fecha de inicio...">
                <br>
                <input type="text" class="form-control datepicker" id="fecha_alta_fin" value="" placeholder="fecha de fin...">
                </div>
            </div>
             <div class="filtros">
           
                <a data-toggle="collapse" href="#fechavenc" aria-expanded="false" aria-controls="fechavenc"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Vencimiento</h5></a>
                <div class="collapse" id="fechavenc">
                <input type="text" class="form-control datepicker" name="fecha_venc_inicio" id="fecha_venc_inicio" value="" placeholder="fecha de inicio...">
                <br>
                <input type="text" class="form-control datepicker" id="fecha_venc_fin" name="fecha_venc_fin" value="" placeholder="fecha de fin...">
                </div>
            </div>
                <div class =filtros>
                 <a data-toggle="collapse" href="#colcodcontrato" aria-expanded="false" aria-controls="colcodcontrato"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Nª Factura/Abono</h5></a>
                <div class="collapse" id="colcodcontrato">
                <input type="text" class="form-control" name="numfactura" id="numfactura" value="" placeholder=".">
                </div>
            </div>
                 <div class =filtros>
                 <a data-toggle="collapse" href="#colcodcontratof" aria-expanded="false" aria-controls="colcodcontratof"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Código de contrato</h5></a>
                <div class="collapse" id="colcodcontratof">
                <input type="text" class="form-control" name="codContrato" id="codContrato" value="" placeholder=".">
                </div>
            </div>
                <div class =filtros>
                 <a data-toggle="collapse" href="#colclavefac" aria-expanded="false" aria-controls="colclavefac"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Clave de facturación</h5></a>
                <div class="collapse" id="colclavefac">
                <input type="text" class="form-control" name="clavefactura" id="clavefactura" value="" placeholder=".">
                </div>
            </div>
             <div class =filtros>
                 <a data-toggle="collapse" href="#colcif" aria-expanded="false" aria-controls="colcif"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Cif del cliente</h5></a>
                <div class="collapse" id="colcif">
               <select class="selectpicker" data-live-search="true" title="Buscar..." id="cif" name="cif">
                 <option selected></option>
                    @foreach ($empresas as $empresa)
                    <option value="{{$empresa->CodContacto}}">{{ $empresa->cif}}</option>
                    @endforeach
                </select>
                </div>
            </div>
                  <div class =filtros>
                 <a data-toggle="collapse" href="#colempresa" aria-expanded="false" aria-controls="colempresa"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Cliente</h5></a>
                <div class="collapse" id="colempresa">
               <select class="selectpicker" data-live-search="true" title="Buscar..." id="empresa" name="empresa">
                 <option selected></option>
                    @foreach ($empresas as $empresa)
                    <option value="{{$empresa->cif}}">{{ $empresa->razonSocial}}</option>
                    @endforeach
                </select>
                </div>
            </div>
                <div class =filtros>
                 <a data-toggle="collapse" href="#colfac" aria-expanded="false" aria-controls="colfac"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Factura/Abono</h5></a>
                <div class="collapse" id="colfac">
                <select class="selectpicker" data-live-search="true" title="Buscar..." id="fac" name="fac">
                 <option selected></option>
                    <option value="A">Abono</option>
                    <option value="F">Factura</option>

                </select>
                </div>
                </div>
              <div class =filtros>
                 <a data-toggle="collapse" href="#coltipo" aria-expanded="false" aria-controls="coltipo"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Tipo Factura/Abono</h5></a>
                <div class="collapse" id="coltipo">
                <select class="selectpicker" data-live-search="true" title="Buscar..." id="tipo" name="tipo">
                 <option selected></option>
                    <option value="C">Corriente</option>
                    <option value="T">De Contrato</option>
                         <option value="F">Provisión de fondo</option>
                </select>
                </div>
            </div> 
            <div class =filtros>
                 <a data-toggle="collapse" href="#colprovision" aria-expanded="false" aria-controls="colprovision"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Provisión Fondo</h5></a>
                <div class="collapse" id="colprovision">
                <input type="checkbox" name="provisionfondos" value="" id="provisionfondos">
                </div>
            </div>
            <div class="filtros">
                <a data-toggle="collapse" href="#colcontratante" aria-expanded="false" aria-controls="colcontratante"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Entidad Emisora</h5></a>
                 <div class="collapse" id="colcontratante">
                 <select class="selectpicker" data-live-search="true" title="Buscar..." id="contratante" name="contratante">
                 <option selected></option>
                    @foreach ($contratantes as $contratante)
                    <option value="{{$contratante->CodContacto}}">{{ $contratante->razonSocial}}</option>
                    @endforeach
                </select>
                </div>
            </div>
            <div class =filtros>
                 <a data-toggle="collapse" href="#colnotas" aria-expanded="false" aria-controls="colnotas"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Notas</h5></a>
                <div class="collapse" id="colnotas">
                <input type="text" class="form-control" name="notas" id="notas" value="" placeholder=".">
                </div>
            </div>
                <h4>Detalles de factura</h4>
               <div class =filtros>
                 <a data-toggle="collapse" href="#colcoddetalle" aria-expanded="false" aria-controls="colcoddetalle"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Código</h5></a>
                <div class="collapse" id="colcoddetalle">
                <input type="text" class="form-control" name="coddetalle" id="coddetalle" value="" placeholder=".">
                </div>
            </div>
            <div class =filtros>
                 <a data-toggle="collapse" href="#colprecio" aria-expanded="false" aria-controls="colprecio"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Precio</h5></a>
                <div class="collapse" id="colprecio">
                <input type="text" class="form-control" name="precio" id="precio" value="" placeholder=".">
                </div>
            </div>
           <div class="filtros">
            <a data-toggle="collapse" href="#colmatricula" aria-expanded="false" aria-controls="colmatricula"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Matricula</h5></a>
            <div class="collapse" id="colmatricula">
             <select class="selectpicker" data-live-search="true" title="Buscar..." id="matricula" name="matricula">
                 <option selected></option>
                    @foreach ($matriculas as $matricula)
                    <option>{{ $matricula->Matricula}}</option>
                    @endforeach
                </select>
                </div>
                </div>
            <div class =filtros>
                 <a data-toggle="collapse" href="colconcepto" aria-expanded="false" aria-controls="colconcepto"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Concepto</h5></a>
                <div class="collapse" id="colconcepto">
                <input type="text" class="form-control" name="concepto" id="concepto" value="" placeholder=".">
                </div>
            </div>
            <div class =filtros>
                 <a data-toggle="collapse" href="#colunidades" aria-expanded="false" aria-controls="colunidades"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Unidades</h5></a>
                <div class="collapse" id="colunidades">
                <input type="text" class="form-control" name="unidades" id="unidades" value="" placeholder=".">
                </div>
            </div>
            <div class="filtros">
            <button type="button" id="filtrar" class="btn btn-default btn-primary">filtrar</button>
            </div>
            </div>
        <div class="col-sm-10">
            <div class="panel panel-default">
                <div class="row"> 
                    <div class="panel-heading" id="sanciones-header">                    
                        <div class="col-md-2" style="width: 100%">
                            <h3 class="module-title">Facturas EUROPA</h3>
                        </div>
                        <div class="col-md-10">
                            <div>
                                <ul class="list-inline nav navbar-nav">
                                    <li>
                                    <a class="dropdown-toggle" href="#" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="fa fa-file-text-o"></span> Crear nuevo Abono/Factura</a>
                                       <ul class="dropdown-menu" role="menu">
                                           <li><a class="dropdown-item" href="/facturas/europa/nuevo">Corriente</a></li>
                                            <li><a class="dropdown-item" href="#" id="de_contrato">de Contrato</a></li>
                                            <li><a class="dropdown-item" href="#" id="de_provision">Provisión de fondos</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                    <li><a href="{{url('facturas/its')}}"><i class="fa fa-exchange" aria-hidden="true"></i></i> Ir a ITS</a></li>
                                    <li><a href="#" id="excel"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Excel</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table-responsive"">
                    <table class="table">
                        <thead class="table-header">
                            <th>Cliente</th>                        
                            <th>Nª Fac.</th>
                            <th>Tipo</th>
                            <th>Clase</th>
                            <th>Importe</th>
                            <th>Notas</th>
                            <th></th>
                        </thead>
                        <tbody id="myTable">
                        @foreach ($facturas as $factura)
                        <tr>
                            <td>{{ $factura->cliente }} </td>
                            <td>{{ $factura->numero }} </td>
                            <td>{{ $factura->tipo }} </td>
                            <td>{{ $factura->clase }} </td>
                            @if($factura->importe!=null)
                            <td>{{ $factura->importe }} €</td>
                            @else
                            <td>0 €</td>
                            @endif                           
                            <td>{{ $factura->Notas }} </td>
                            <td><a class="btn btn-sm btn-success" href="{!! url('facturas/europa/'.$factura->idfactura) !!}"><i class="fa fa-edit"></i></a></td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    
                </div>

                 <div class="col-md-12 text-center">
      <ul class="pagination pagination-lg pager" id="myPager"></ul>
      </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

 $('.datepicker').datepicker({
        format: "dd/mm/yyyy",
        dateFormat: 'yy-mm-dd',
        language: "es",
        autoclose: true
    });
    
    $('#search').on('keyup',function(){
        $value=$(this).val();
        //alert($value);
        $("#loading").show();
        setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/search_europa')}}',
                data : {'search':$value},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('tbody').html(data);
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);
});

$( "#filtrar" ).click(function() {
$fecha_alta_inicio = $("#fecha_alta_inicio").val();
$fecha_alta_fin = $("#fecha_alta_fin").val();

$numfactura = $("#numfactura").val();
$codContrato = $("#codContrato").val();
$clavefactura = $("#clavefactura").val();
$cif = $("#cif option:selected").text();
$empresa = $("#empresa option:selected").val();
$fac = $("#fac option:selected").val();
$tipo = $("#tipo option:selected").val();
$contratante = $("#contratante option:selected").val();
$notas = $("#notas").val();

$coddetalle = $("#coddetalle").val();
$precio = $("#precio").val();
$matricula = $("#matricula option:selected").text();
$unidades = $("#unidades").val();


$provisionfondos = "";


if($("#provisionfondos").prop("checked")){
$provisionfondos = 1;
}



$alta_inicio = $fecha_alta_inicio.split("/").reverse().join("-");
$alta_fin = $fecha_alta_fin.split("/").reverse().join("-");

        $dialog = bootbox.dialog({
                        message: '<p class="text-center">Cargando datos, espere por favor...</p>',
                        closeButton: false
                    });

    setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('facturas/europa/filtra_europa')}}',
                data : {'alta_inicio':$alta_inicio,'alta_fin':$alta_fin,'contratante':$contratante,'empresa':$empresa,'matricula':$matricula,'codContrato':$codContrato,'numfactura':$numfactura,'clavefactura':$clavefactura,'fac':$fac,'tipo':$tipo,'notas':$notas,'coddetalle':$coddetalle,'precio':$precio,'unidades':$unidades,'cif':$cif,'provisionfondos':$provisionfondos},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('tbody').html(data);

                    // do something in the background
                    $dialog.modal('hide');
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

});

$( "#excel" ).click(function() {
$fecha_alta_inicio = $("#fecha_alta_inicio").val();
$fecha_alta_fin = $("#fecha_alta_fin").val();

$numfactura = $("#numfactura").val();
$codContrato = $("#codContrato").val();
$clavefactura = $("#clavefactura").val();
$cif = $("#cif option:selected").text();
$empresa = $("#empresa option:selected").val();
$fac = $("#fac option:selected").val();
$tipo = $("#tipo option:selected").val();
$contratante = $("#contratante option:selected").val();
$notas = $("#notas").val();

$coddetalle = $("#coddetalle").val();
$precio = $("#precio").val();
$matricula = $("#matricula option:selected").text();
$unidades = $("#unidades").val();


$provisionfondos = "";


if($("#provisionfondos").prop("checked")){
$provisionfondos = 1;
}



$alta_inicio = $fecha_alta_inicio.split("/").reverse().join("-");
$alta_fin = $fecha_alta_fin.split("/").reverse().join("-");

        $dialog = bootbox.dialog({
                        message: '<p class="text-center">Generando su Excel, espere por favor...</p>',
                        closeButton: false
                    });

    setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('facturas/europa/exportar_excel_europa')}}',
                data : {'alta_inicio':$alta_inicio,'alta_fin':$alta_fin,'contratante':$contratante,'empresa':$empresa,'matricula':$matricula,'codContrato':$codContrato,'numfactura':$numfactura,'clavefactura':$clavefactura,'fac':$fac,'tipo':$tipo,'notas':$notas,'coddetalle':$coddetalle,'precio':$precio,'unidades':$unidades,'cif':$cif,'provisionfondos':$provisionfondos},            
                success : function(data){
                    window.location = this.url;
                    $dialog.modal('hide');
                },
                error : function(data){
                    bootbox.alert("Ha ocurrido un error generando su Excel.");
                    $dialog.modal('hide');
                }
            });
        }, 500);

});

 $("#de_contrato").click(function(){

    var loading = bootbox.dialog({
    message: '<p class="text-center"><p><i class="fa fa-spin fa-spinner"></i> Cargando interfaz, espere por favor...</p>',
    closeButton: false
});
// do something in the background


      setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('facturas/europa/mostrar_decontrato_europa')}}',
                        data : {},
                        success : function(data){
                            loading.modal('hide');
                           var dialog = bootbox.dialog({
                                    message: data,
                                    title: "Crear Factura/Abono de contrato",
                                    buttons: {
                                        success: {
                                            label: 'aceptar',
                                             callback: function() {
                                                $contrato = $("#contrato_origen option:selected").val();
                                                $tipo = $('input[name=tipofac]:checked').val();
                                                $fechaemision = $("#fechaemision").val();
                                                $iva = $("#ivaporcentaje").val();
                                                $origen = "";
                                                if($('#factura_origen').length){
                                                    $origen = $('#factura_origen option:selected').val();
                                                }
                                                $notas = $("#notas_factura").val();
                                                generar_factura_decontrato($contrato,$tipo,$origen,$fechaemision,$notas,$iva);
                                                dialog.modal('hide');
                                                 }
                                        }
                                    }
                                });
                             $('.selectpicker').selectpicker('refresh');
                             $('.selectpicker').selectpicker('render');
                             $('.datepicker').datepicker({
                                format: "dd/mm/yyyy",
                                dateFormat: 'yy-mm-dd',
                                language: "es",
                                autoclose: true
                            });
                             
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);

});

  $("#de_provision").click(function(){

    var loading = bootbox.dialog({
    message: '<p class="text-center"><p><i class="fa fa-spin fa-spinner"></i> Cargando interfaz, espere por favor...</p>',
    closeButton: false
});
// do something in the background


      setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('facturas/europa/mostrar_deprovision_europa')}}',
                        data : {},
                        success : function(data){
                            loading.modal('hide');
                           var dialog = bootbox.dialog({
                                    message: data,
                                    title: "Crear Factura/Abono de provisión de fondos",
                                    size : 'large',
                                    buttons: {
                                        success: {
                                            label: 'Generar facturas',
                                             callback: function() {



                                                if($("input:radio[name=selec]").is(':checked') != true){
                                                    bootbox.alert("debe seleccionar al menos una factura.");
                                                }else{
                                                        $factura = $("input:radio[name=selec]:checked").closest("tr").find('.clavefactura').val();
                                                        $honorarios = $("input:radio[name=selec]:checked").closest("tr").find('.honorarios').text();
                                                     
                                                         bootbox.confirm({
                                                    title: "Generar facturas",
                                                    message: "Se generará una factura de provisión de fondo de la factura seleccionada. ¿Está seguro?",
                                                    buttons: {
                                                        cancel: {
                                                            label: '<i class="fa fa-times"></i> Cancelar'
                                                        },
                                                        confirm: {
                                                            label: '<i class="fa fa-check"></i> Aceptar'
                                                        }
                                                    },
                                                    callback: function (result) {

                                                        
                                                        generar_factura_deprovision($factura,$honorarios);
                                                    }
                                                });
                                                }
                                           

                                                 }
                                        }
                                    }
                                });
                             $('.selectpicker').selectpicker('refresh');
                             $('.selectpicker').selectpicker('render');
                             $('.datepicker').datepicker({
                                format: "dd/mm/yyyy",
                                dateFormat: 'yy-mm-dd',
                                language: "es",
                                autoclose: true
                            });
                             
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);

});

  $(document).on('change','#cliente_provision',function(){

        $fecha = $("#fechaactual option:selected").val();
        $empresa = $(this).find('option:selected').val();

        if($empresa != null){

                 $loading = bootbox.dialog({
                message: '<p><i class="fa fa-spin fa-spinner"></i> Buscando facturas pendientes...</p>',
               closeButton: false
});


        }
  
          setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('facturas/europa/mostrar_pte_liquidar')}}',
                data : {'fecha':$fecha,'empresa':$empresa},
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('#pteliquidar').html(data);
                    $('.selectpicker').selectpicker('refresh');
                    $('.selectpicker').selectpicker('render');
                    $loading.modal("hide");

                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

            setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('facturas/europa/mostrar_liquidadas')}}',
                data : {'fecha':$fecha,'empresa':$empresa},
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('#liquidadas').html(data);
                    $('.selectpicker').selectpicker('refresh');
                    $('.selectpicker').selectpicker('render');
                    $loading.modal("hide");

                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

    });

    $(document).on('change','#fechaactual',function(){

        if($("cliente_provision option:selected").val() == ""){
            bootbox.alert("Debe seleccionar un cliente para que el sistema busque facturas");
        }else{
        $fecha = $("#fechaactual option:selected").val();
        $empresa = $("#cliente_provision").find('option:selected').val();

        if($empresa != null){

                 $loading = bootbox.dialog({
                message: '<p><i class="fa fa-spin fa-spinner"></i> Buscando facturas pendientes...</p>',
               closeButton: false
});


        }
  
          setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('facturas/europa/mostrar_pte_liquidar')}}',
                data : {'fecha':$fecha,'empresa':$empresa},
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('#pteliquidar').html(data);
                    $('.selectpicker').selectpicker('refresh');
                    $('.selectpicker').selectpicker('render');
                    $loading.modal("hide");

                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

            setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('facturas/europa/mostrar_liquidadas')}}',
                data : {'fecha':$fecha,'empresa':$empresa},
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('#liquidadas').html(data);
                    $('.selectpicker').selectpicker('refresh');
                    $('.selectpicker').selectpicker('render');
                    $loading.modal("hide");

                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);
        }
    });

     $(document).on('click','.aplicacion',function(){

        $idfactura =  $(this).closest("tr").find('.idfactura').val();

       var loading = bootbox.dialog({
    message: '<p class="text-center"><p><i class="fa fa-spin fa-spinner"></i> Cargando interfaz, espere por favor...</p>',
    closeButton: false
});
            setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('facturas/europa/mostrar_aplicacion')}}',
                        data : {'idfactura':$idfactura},
                        success : function(data){
                            loading.modal('hide');
                           var dialog = bootbox.dialog({
                                    message: data,
                                    title: "Aplicar provisión de fondos",
                                    size : 'large',
                                    buttons: {
                                        Cancel: {
                                            label: 'Cerrar'
                                        }
                                    }
                                });
                             $('.selectpicker').selectpicker('refresh');
                             $('.selectpicker').selectpicker('render');
                             $('.datepicker').datepicker({
                                format: "dd/mm/yyyy",
                                dateFormat: 'yy-mm-dd',
                                language: "es",
                                autoclose: true
                            });
                             
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);
     });

     $(document).on('click','.agregar_aplicacion',function(){

        $clavefactura = $(".claveliq_aplicacion").val();
        $expediente = $(".expediente").val();
        $importe = $(".importe_aplicacion").val();
        $fecha = $("#fechaactual option:selected").val();
        $empresa = $("#cliente_provision").find('option:selected').val();

         setTimeout(function(){
            $.ajax({
                        type : 'get',
                        url  : '{{URL::to('facturas/europa/insertar_aplicacion')}}',
                        data : {'clavefactura':$clavefactura,'expendiente':$expediente,'importe':$importe},            
                        success : function(data){
                            console.log(JSON.stringify(data));
                    if(data.includes("SQLSTATE")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                //location.reload();
                            }
                        });
                    }else{
                      $confirm = bootbox.alert({
                            message: "La aplicación se ha agregado correctamente, a continuación se recalculará la factura.",
                            callback: function () {
                                  $( "#pteliquidar" ).empty();
                                   setTimeout(function(){
                                                    $.ajax({
                                                        type : 'get',
                                                        url  : '{{URL::to('facturas/europa/mostrar_pte_liquidar')}}',
                                                        data : {'fecha':$fecha,'empresa':$empresa},
                                                        success : function(data){
                                                            console.log(JSON.stringify(data));
                                                            $('#pteliquidar').html(data);
                                                            $('.selectpicker').selectpicker('refresh');
                                                            $('.selectpicker').selectpicker('render');
                                                            $loading.modal("hide");

                                                        },
                                                        error : function(data){
                                                            console.log(JSON.stringify(data));
                                                        }
                                                    });
                                                }, 500);
                                    $confirm.modal("hide");
                            }
                        });
                    }
                   
                    $("#loading").hide();
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);

     });




    $(document).on('change','input[name=tipofac]',function(){

    $tipo = $('input[name=tipofac]:checked').val();
    $contrato = $('#contrato_origen option:selected').val();
    
    //alert($contrato);



    if($tipo == "abono"){

        if($contrato == ""){
        bootbox.alert("No ha seleccionado ningún contrato");
        $('input[value=factura]').prop("checked",true);
    }else{

         $loading = bootbox.dialog({
    title: 'Buscando facturas',
    message: '<p><i class="fa fa-spin fa-spinner"></i> Espere por favor...</p>',
    closeButton: false
    });
        
          setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('facturas/europa/facturas_origen')}}',
                data : {'contrato':$contrato},
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('#div_origen').html(data);
                    $('.selectpicker').selectpicker('refresh');
                    $('.selectpicker').selectpicker('render');
                    $loading.modal("hide");

                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);
    }


    }

    });

    function generar_factura_decontrato(codContrato,tipo,origen,fechaemision,notas,iva){

      setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('facturas/europa/generar_factura_decontrato')}}',
                        data : {'codContrato':codContrato,'tipo':tipo,'origen':origen,'fechaemision':fechaemision,'notas':notas,'iva':iva},            
                        success : function(data){
                            console.log(JSON.stringify(data));
                    if(data.includes("SQLSTATE")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                //location.reload();
                            }
                        });
                    }else{
                         bootbox.alert({
                            message: "Se ha creado correctamente una nueva factura con los datos solicitados.",
                            callback: function () {
                                  window.location = "/facturas/europa/"+data;
                            }
                        });
                    }
                   
                    $("#loading").hide();
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);

  }

   function generar_factura_deprovision(factura,honorarios){

      setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('facturas/europa/generar_factura_deprovision')}}',
                        data : {'factura':factura,'honorarios':$honorarios},            
                        success : function(data){
                            console.log(JSON.stringify(data));
                    if(data.includes("SQLSTATE")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                //location.reload();
                            }
                        });
                    }else{
                         bootbox.alert({
                            message: "Se ha creado correctamente una nueva factura con los datos solicitados.",
                            callback: function () {
                                  window.location = "/facturas/europa/"+data;
                            }
                        });
                    }
                   
                    $("#loading").hide();
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);

  }

</script>
@endsection