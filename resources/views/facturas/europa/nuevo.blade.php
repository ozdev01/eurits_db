@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-xl-12 col-md-offset-0">
            <div class="panel panel-default">
                <div class="row"> 
                    <div class="panel-heading" id="sanciones-header">                    
                        <div class="col-md-12">
                            <h3 class="module-title">Crear nuevo Abono/Factura</h3>
                        </div>
                    </div>
                    <div class="col-md-12">
                            <div>
                                <ul class="nav navbar-nav">
                                    <li><a href="#" class="bt-edit"><i class="fa fa-save"> </i> Guardar</a></li>
                                    <li><a href="#" id="volver"><i class="fa fa-arrow-left"> </i> Volver</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body" style="border-top:2px solid #335599;">                        
                    <form class="inline-form" id="form_contrato">
                           <div class="form-group col-md-4">
                            <label for="empresa">Contratante</label>
                             <select class="selectpicker" data-live-search="true" title="Buscar..." id="contratante" name="contratante">
                              @foreach($contratantes as $contratante)
                              <option value="{{$contratante->CodContacto}}">{{$contratante->razonSocial}}</option>
                              @endforeach
                            </select> 
                        </div>
                         <div class="form-group col-md-2">
                            <label for="codigo">Tipo</label>
                            <select class="selectpicker" data-live-search="true" title="Buscar..." id="tipo" name="tipo">
                                <option value="A">Abono</option>
                                <option value="F">Factura</option>
                            </select> 
                        </div>
                         <div class="form-group col-md-2">
                            <label for="codigo">Clase</label>
                              <select class="selectpicker" data-live-search="true" title="Buscar..." id="clase" name="clase" readonly>
                                <option value="C" selected>Corriente</option>
                            </select> 
                        </div> 
                        <div class="form-group col-md-2">
                            <label for="codigo">Origen</label>
                              <select class="selectpicker" data-live-search="true" title="Buscar..." id="origen" name="origen">
                              @foreach($facturas as $factura)
                              <option value="{{$factura->clave}}">{{$factura->numero}}</option>
                              @endforeach
                            </select> 
                        </div>
                        <div class="form-group col-md-4">
                            <label for="matricula">Empresa</label>
                           <select class="selectpicker" data-live-search="true" title="Buscar..." id="empresa" name="empresa">
                              @foreach($empresas as $empresa)
                              <option value="{{$empresa->CodContacto}}">{{$empresa->razonSocial}}</option>
                              @endforeach
                            </select> 
                        </div>
                         <div class="form-group col-md-2">
                            <label for="codigo">Fecha Emisión</label>
                            <input type="text" class="form-control datepicker" name="fechaemision" value="">
                        </div>
                         <div class="form-group col-md-4">
                            <label for="cif">Cuenta Bancaria</label>
                            <select class="selectpicker" data-live-search="true" title="Buscar..." id="cc" name="cc">
                                <option value="1">ES2700491247032810195302</option>
                                <option value="2">ES7921002534830210086691</option>
                                <option value="3">ES8321002534820210061157</option>
                                <option value="4">ES9021002534870210093087</option>
                            </select>                  
                        </div>
                         <div class="form-group col-md-5">
                            <label for="cif">Notas</label>
                            <textarea name="notas" class="form-control" rows="4"></textarea>                
                        </div>
                          <div class="form-group col-md-2">
                          <label>Con provisión de fondo</label>
                         <input type="checkbox" name="provisionfondos" value="" id="provisionfondos">
                         </div>
                        <div class="form-group col-md-2">
                        <label>Cambiar IVA</label>
                        <input type="number" id="codContacto" class="form-control" name="iva" value="21">
                        </div>
                    </form>
                    </div>
                    </div>
            </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

$("#volver").click(function(){
        window.history.go(-1); return true;
    });

 $(document).on('focusin','.datepicker',function(){
         $(this).datepicker({
        format: "dd/mm/yyyy",
        dateFormat: 'yy-mm-dd',
        language: "es",
        autoclose: true
    });

         $(this).selectpicker("data-live-search","true");

    });

  $(".bt-edit").click(function(){

     setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '/facturas/europa/insertar',
                data : {'datos' : $("#form_contrato").serialize()},
                success : function(data){
                    console.log(JSON.stringify(data));
                    if(data.includes("SQLSTATE")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                //location.reload();
                            }
                        });
                    }else{
                         bootbox.alert({
                            message: "La factura se ha generado correctamente",
                            callback: function () {
                                 window.location = "/facturas/europa/"+data;
                            }
                        });
                    }
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

});

  $("#eliminar").click(function(){
    bootbox.confirm({
        title: "Eliminar Contrato",
        message: "<i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> ¿Está seguro que desea eliminar la factura?</strong> Tenga en cuenta que esta acción es irreversible.",
        buttons: {
            cancel: {
            label: '<i class="fa fa-times"></i> Cancelar'
        },
        confirm: {
            label: '<i class="fa fa-check"></i> Aceptar'
        }
        },
        callback: function (result) {
            if(result == true){
                $clave = $("#id").val();

                setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('facturas/europa/eliminar')}}',
                        data : {'clave':$clave},            
                        success : function(data){
                                    if(data.includes("SQLSTATE")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                //location.reload();
                            }
                        });
                    }else{
                         bootbox.alert({
                            message: data,
                            callback: function () {
                                 window.location = "/facturas/europa";
                            }
                        });
                    }
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);
            }
        }
    });

});


//eliminar el registro
  $(document).on('click','.btn-danger',function(){

$iddetalle=  $(this).closest("tr").find('.iddetalle').val();
    

bootbox.confirm({
     title: "Eliminar Registro",
    message: "<i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> ¿Está seguro que desea eliminar el detalle de la factura?</strong> Tenga en cuenta que esta acción es irreversible.",
    buttons: {
        cancel: {
            label: '<i class="fa fa-times"></i> Cancelar'
        },
        confirm: {
            label: '<i class="fa fa-check"></i> Aceptar'
        }
    },
    callback: function (result) {

            if(result == true){
                
                setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('facturas/europa/eliminar_detalle')}}',
                        data : {'clave':$clave},            
                        success : function(data){
                            console.log(JSON.stringify(data));
                    if(data.includes("SQLSTATE")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                //location.reload();
                            }
                        });
                    }else{
                         bootbox.alert({
                            message: data,
                            callback: function () {
                                 location.reload();
                            }
                        });
                    }
                   
                    $("#loading").hide();
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);

    }

    }
});

});

  $(document).on('focusout','.precio',function(){

    $unidades = $(this).closest("tr").find('.unidades').val();
    $precio = $(this).closest("tr").find('.precio').val();
    $(this).closest("tr").find('.total').val($precio * $unidades);
  });

   $(document).on('focusout','.unidades',function(){

    $unidades = $(this).closest("tr").find('.unidades').val();
    $precio = $(this).closest("tr").find('.precio').val();
    $(this).closest("tr").find('.total').val($precio * $unidades);
  });

  $(document).on('click','.editar',function(){

$iddetalle =  $(this).closest("tr").find('.iddetalle').val();
$unidades = $(this).closest("tr").find('.unidades').val();
$precio = $(this).closest("tr").find('.precio').val();
$concepto = $(this).closest("tr").find('.concepto').val();
$clave = $ID = $("#id").val();

    

setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('facturas/europa/editar_detalle')}}',
                        data : {'iddetalle':$iddetalle,'unidades':$unidades,'precio':$precio,'concepto':$concepto,'clave':$clave},            
                        success : function(data){
                            console.log(JSON.stringify(data));
                    if(data.includes("SQLSTATE")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                //location.reload();
                            }
                        });
                    }else{
                         bootbox.alert({
                            message: data,
                            callback: function () {
                                 location.reload();
                            }
                        });
                    }
                   
                    $("#loading").hide();
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);

});

  $(document).on('click','#add_detalle',function(){

$iddetalle =  $(this).closest("tr").find('.iddetalle').val();
$codigo = $(this).closest("tr").find('.codigo').val();
$unidades = $(this).closest("tr").find('.unidades').val();
$precio = $(this).closest("tr").find('.precio').val();
$concepto = $(this).closest("tr").find('.concepto').val();
$clave = $ID = $("#id").val();

    

setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('facturas/europa/insertar_detalle')}}',
                        data : {'iddetalle':$iddetalle,'unidades':$unidades,'precio':$precio,'concepto':$concepto,'clave':$clave,'codigo':$codigo},            
                        success : function(data){
                            console.log(JSON.stringify(data));
                    if(data.includes("SQLSTATE")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                //location.reload();
                            }
                        });
                    }else{
                         bootbox.alert({
                            message: data,
                            callback: function () {
                                 location.reload();
                            }
                        });
                    }
                   
                    $("#loading").hide();
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);

});

</script>

@endsection