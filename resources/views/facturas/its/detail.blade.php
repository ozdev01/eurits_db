@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-xl-12 col-md-offset-0">
            <div class="panel panel-default">
                <div class="row"> 
                    <div class="panel-heading" id="sanciones-header">                    
                        <div class="col-md-12">
                            <h3 class="module-title">FACTURA {!! $factura->ClaveLiq !!}</h3>
                           <input type="hidden" class="id" name="clave" id="id" value="{!!$factura->ClaveLiq !!}">
                        </div>
                        <div class="col-md-12">
                            <div>
                                <ul class="nav navbar-nav">
                                    <li><a href="#" class="bt-edit"><i class="fa fa-save"> </i> Guardar</a></li>
                                    <li><a href="#" class="bt-refresh"><i class="fa fa-refresh" aria-hidden="true"></i> Actualizar detalles</a></li>
                                    <li><a href="#" class="bt-export bt-export_factura"><i class="fa fa-print"> </i> Imprimir factura</a></li>
                                    <li><a href="#" id="eliminar"><i class="fa fa-trash"> </i> Eliminar</a></li>
                                    <li><a href="#" id="volver"><i class="fa fa-arrow-left"> </i> Volver</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="panel-body" style="border-top:2px solid #335599;">                        
                    <form class="inline-form" id="form_contrato">
                           <div class="form-group col-md-4">
                            <label for="empresa">Contratante</label>
                             <input type="text" class="form-control" name="contratante" value="{!! $contratante->Empresa !!}" disabled>
                        </div>
                            <div class="form-group col-md-2">
                            <label for="codigo">Nª Factura</label>
                            <input type="text" class="form-control" name="numfactura" id='numfactura' value="{!! $factura->NumLiq !!}" disabled>
                        </div>
                         <div class="form-group col-md-2">
                            <label for="codigo">Tipo</label>
                            @if($factura->TipoLiq =='A')
                            <input type="text" class="form-control" name="TipoLiq" value="Abono" disabled>
                            <input type="hidden" class="form-control" name="TipoLiq" value="Abono">
                            @else
                            <input type="text" class="form-control" name="TipoLiq" value="Factura" disabled>
                            <input type="hidden" class="form-control" name="TipoLiq" value="Factura" disabled>
                            @endif
                        </div>
                         <div class="form-group col-md-2">
                            <label for="codigo">Clase</label>
                            @if($factura->ClaseLiq == 'C')
                            <input type="text" class="form-control" name="TipoLiq" value="Corriente" disabled>
                            <input type="hidden" class="form-control" name="TipoLiq" value="Corriente">
                            @elseif($factura->ClaseLiq == 'T')
                            <input type="text" class="form-control" name="TipoLiq" value="De contrato" disabled>
                            <input type="hidden" class="form-control" name="TipoLiq" value="De contrato">
                            @elseif($factura->ClaseLiq == 'F')
                            <input type="text" class="form-control" name="TipoLiq" value="asis mec F" disabled>
                            <input type="hidden" class="form-control" name="TipoLiq" value="asis mec F">
                             @elseif($factura->ClaseLiq == 'E')
                            <input type="text" class="form-control" name="TipoLiq" value="Provisión de fondos" disabled>
                            <input type="hidden" class="form-control" name="TipoLiq" value=Provisión de fondos ">
                             @elseif($factura->ClaseLiq == 'D')
                            <input type="text" class="form-control" name="TipoLiq" value="asis mec D" disabled>
                            <input type="hidden" class="form-control" name="TipoLiq" value="asis mec D">
                             @elseif($factura->ClaseLiq == 'G')
                            <input type="text" class="form-control" name="TipoLiq" value="Elec cheque G" disabled>
                            <input type="hidden" class="form-control" name="TipoLiq" value="Elec cheque G">
                             @elseif($factura->ClaseLiq == 'H')
                            <input type="text" class="form-control" name="TipoLiq" value="Desinmov H" disabled>
                            <input type="hidden" class="form-control" name="TipoLiq" value="Desinmov H">
                             @elseif($factura->ClaseLiq == 'A')
                            <input type="text" class="form-control" name="TipoLiq" value="Elec cheque A" disabled>
                            <input type="hidden" class="form-control" name="TipoLiq" value="Elec cheque A">
                             @elseif($factura->ClaseLiq == 'B')
                            <input type="text" class="form-control" name="TipoLiq" value="asis mec B" disabled>
                            <input type="hidden" class="form-control" name="TipoLiq" value="asis mec B">
                            @endif
                        </div>
                        @if($factura->ClaseLiq=='H' || $factura->ClaseLiq=='G')
                         <div class="form-group col-md-2">
                            <label for="codigo">Precio</label>
                         <input type="text" class="form-control" name="precio" value="{{$precios->Precio}}" readonly>
                        </div>
                        @endif
                         @if($factura->ClaseLiq=='D' || $factura->ClaseLiq=='E' || $factura->ClaseLiq=='F')
                        <div class="form-group col-md-2">
                            <label for="codigo">Precio Cabecera</label>
                         <input type="text" class="form-control" name="precioT" value="{{$precios->PrecioT}}" readonly>
                        </div> 
                        <div class="form-group col-md-2">
                            <label for="codigo">Precio Remolque</label>
                         <input type="text" class="form-control" name="precioR" value="{{$precios->PrecioR}}" readonly>
                        </div>
                        @endif  
                        <div class="form-group col-md-4">
                            <label for="matricula">Empresa</label>
                            <input type="text" class="form-control" name="empresa" value="{!! $empresa->Empresa !!}" disabled>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="codigo">Cif</label>
                            <input type="text" class="form-control" name="cifempresa" id="cifempresa" value="{!! $empresa->CIF !!}" disabled>
                            <input type="hidden" class="form-control" name="cif" value="{!! $empresa->CIF !!}">
                        </div>
                         <div class="form-group col-md-2">
                            <label for="codigo">Fecha Emisión</label>
                             @if($factura->fecha!=null)
                            <input type="text" class="form-control" name="fechaemision" value="{!! date('d/m/Y', strtotime($factura->fecha)) !!}">
                            @else
                            <input type="text" class="form-control" name="fechaemision" value="">
                            @endif
                        </div>
                        <div class="form-group col-md-2">
                            <label for="codigo">Fecha de Vencimiento</label>
                            @if($factura->VenctoRecBanc!=null)
                            <input type="text" class="form-control datepicker" name="vencimiento" value="{!! date('d/m/Y', strtotime($factura->VenctoRecBanc)) !!}">
                            @else
                             <input type="text" class="form-control datepicker" name="vencimiento" value="">
                            @endif
                        </div>
                         <div class="form-group col-md-2">
                            <label for="codigo">Fecha Inicial</label>
                            @if($factura->PeriodoIni!=null)
                            <input type="text" class="form-control datepicker" name="fechainicial" value="{!! date('d/m/Y', strtotime($factura->PeriodoIni)) !!}">
                            @else
                             <input type="text" class="form-control datepicker" name="fechainicial" value="">
                            @endif
                        </div>
                         <div class="form-group col-md-2">
                            <label for="codigo">Fecha Final</label>
                            @if($factura->PeriodoFin!=null)
                            <input type="text" class="form-control datepicker" name="fechafinal" value="{!! date('d/m/Y', strtotime($factura->PeriodoFin)) !!}">
                            @else
                            <input type="text" class="form-control datepicker" name="fechafinal" value="">
                            @endif
                        </div>
                            @if($factura->ClaseLiq=='D' || $factura->ClaseLiq=='E' || $factura->ClaseLiq=='F')
                          <div class="form-group col-md-3">
                            <label for="codigo">Criterio de selec.</label>
                        <select class="selectpicker" data-live-search="true" title="Buscar..." id="CriterioSelec" name="CriterioSelec">
                        @if($factura->CriterioSelec==1)
                        <option value="1" selected>Fecha de contrato</option>
                        <option value="2">Fecha de introducción de datos</option>
                        @else
                         <option value="1" >Fecha de contrato</option>
                        <option value="2" selected>Fecha de introducción de datos</option>
                        @endif
                        </select>
                        </div>
                        @endif
                    
                          <div class="form-group col-md-4">
                            <label for="cif">Cuenta Bancaria</label>
                            <select class="selectpicker" data-live-search="true" title="Buscar..." id="cc" name="cc">
                            @foreach($cuentas as $cuenta)
                                @if($cuenta->IdCuenta == $factura->CuentaIngBanc)
                                <option value="{{$cuenta->IdCuenta}}" selected>{{$cuenta->Concepto}}</option>
                                @else
                                   <option value="{{$cuenta->IdCuenta}}">{{$cuenta->Concepto}}</option>
                                   @endif
                            @endforeach
                            </select>                  
                        </div>
                         <div class="form-group col-md-5">
                            <label for="cif">Notas</label>
                            <textarea name="notas" class="form-control" rows="4">{{$factura->Notas}}</textarea>                
                        </div>
                        <div class="form-group col-md-2">
                        <label>Cambiar IVA</label>
                        <input type="number" id="codContacto" class="form-control" name="iva" value="{{$factura->IVA}}">
                        </div>
                            <div class="form-group col-md-6">
                        <label for="cif">Importe</label>
                        <ul style="    border: 1px solid gray; border-radius: 4px; box-shadow: inset 0 1px 1px rgba(0,0,0,.075); padding: 20px;">
                        <ul class="list-inline" style="padding: 5px !important">
                        <li><label style="width: 150px">Base Imponible:</label></li>
                        <li><input type="number" step="0.01"  class="form-control" name="baseimponible" id='baseimponible' value="{{$baseimponible}}" readonly></li>
                        </ul>
                        <ul class="list-inline" style="padding: 5px !important">
                        <li><label style="width: 150px">IVA {{$factura->IVA}} %:</label></li>
                        <li><input type="number" step="0.01" class="form-control" name="ivaprecio" id='ivaprecio' value="{{$iva}}" readonly></li>
                        </ul>
                        @if($factura->ClaseLiq == 'T')
                        <ul class="list-inline" style="padding: 5px !important">
                        <li><label style="width: 150px">Provisión:</label></li>
                        <li><input type="number" step="0.01" class="form-control" name="provision" id='provision' value="{{$provision}}" readonly></li>
                        </ul>
                        @endif
                        <ul class="list-inline" style="padding: 5px !important">
                        <li><label style="width: 150px">Total:</label></li>
                        <li><input type="number" step="0.01" class="form-control" name="importe" id='importe' value="{{$total}}"></li>
                        </ul>
                        </ul>
                        </div>
                    </form>
                    </div>
                    </div>
                <div class="panel panel-default">
              <div class="row"> 
                    <div class="panel-heading" id="detalles-header">                    
                        <div class="col-md-6">
                            <h3 class="module-title">Detalles</h3>
                        </div>
                    </div>
                    </div>
                    <div id="error" style="display:none" class="alert alert-danger alert-dismissible fade in" role="alert"></div>
                    <div id="success" style="display:none" class="alert alert-success alert-dismissible"></div>
                     <div class="table-responsive" id="tabla_contenido">
                    <table class="table">
                        <thead class="table-header">
                        @if($factura->TipoLiq == 'F')
                            @if($factura->ClaseLiq == 'H' || $factura->ClaseLiq == 'G')
                            <th>CIF</th>
                            <th>BENEFICIARIO</th>
                            <th>MATRICULA</th>
                            <th>PRECIO</th>
                            <th>SIN IVA</th>
                            <th></th>
                            </thead>
                            <tbody id="myTable">
                        @foreach ($detalles as $detalle)
                       <tr>
                         <td><input type="hidden" name="iddetalle" class="iddetalle" value="{{$detalle->iddetalle}}"><input type="text" class="form-control cif" name="" value="{{$detalle->CIF}}" placeholder="" readonly></td>
                         <td><input type="text" class="form-control beneficiario" name="" value="{{$detalle->Empresa}}" placeholder="" readonly></td>
                         <td><input type="text" class="form-control matricula" name="" value="{{$detalle->MATRICULA}}" placeholder=""></td>
                         <td><input type="text" class="form-control precio" name="" value="{{$detalle->PRECIO}}" placeholder=""></td>
                         <td><input type="checkbox" class="iva" name="" value=""></td>
                         <td><a class="btn btn-sm btn-success editar" href="#"><i class="fa fa-edit"></i> </a> <a class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a></td>
                       </tr>
                            @endforeach
                       <!--<tr>
                            <td><input type="text" class="form-control cif" name="" value="" placeholder=""></td>
                         <td><input type="text" class="form-control beneficiario" name="" value="" placeholder=""></td>
                         <td><input type="text" class="form-control matricula" name="" value="" placeholder=""></td>
                          <td><input type="text" class="form-control remolque" name="" value="" placeholder=""></td>
                           <td><input type="text" class="form-control fecha" name="" value="" placeholder=""></td>
                         <td><input type="text" class="form-control precio" name="" value="" placeholder=""></td>
                          <td><a class="btn btn-success" id="add_detalle"><i class="fa fa-plus"></i></a></td>
                        </tr>-->
                        </tbody>
                         @endif
                        @if($factura->ClaseLiq == 'F' || $factura->ClaseLiq == 'D' || $factura->ClaseLiq == 'E')
                            <th>CIF</th>
                            <th>BENEFICIARIO</th>
                            <th>MATRICULA</th>
                            <th>REMOLQUE</th>
                            <th>FECHA</th>
                            <th>PRECIO</th>
                            <th></th>
                            </thead>
                            <tbody id="myTable">
                        @foreach ($detalles as $detalle)
                       <tr>
                         <td><input type="hidden" name="iddetalle" class="iddetalle" value="{{$detalle->iddetalle}}"><input type="text" class="form-control cif" name="" value="{{$detalle->CIF}}" placeholder="" readonly></td>
                         <td><input type="text" class="form-control beneficiario" name="" value="{{$detalle->Empresa}}" placeholder="" readonly></td>
                         <td><input type="text" class="form-control matricula" name="" value="{{$detalle->MATRICULA}}" placeholder=""></td>
                          <td><input type="text" class="form-control matricula" name="" value="{{$detalle->REMOLQUE}}" placeholder=""></td>
                          <td><input type="text" class="form-control fecha" name="" value="{!! date('d/m/Y', strtotime($detalle->fechaContrato)) !!}" placeholder=""></td>
                         <td><input type="text" class="form-control precio" name="" value="{{$detalle->PRECIO}}" placeholder=""></td>
                         <td><a class="btn btn-sm btn-success editar" href="#"><i class="fa fa-edit"></i> </a> <a class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a></td>
                       </tr>
                            @endforeach
                       
                        <!--<tr>
                        <td><input type="text" class="form-control cif" name="" value="" placeholder=""></td>
                         <td><input type="text" class="form-control beneficiario" name="" value="" placeholder=""></td>
                         <td><input type="text" class="form-control matricula" name="" value="" placeholder=""></td>
                          <td><input type="text" class="form-control remolque" name="" value="" placeholder=""></td>
                           <td><input type="text" class="form-control fecha" name="" value="" placeholder=""></td>
                         <td><input type="text" class="form-control precio" name="" value="" placeholder=""></td>
                          <td><a class="btn btn-success" id="add_detalle"><i class="fa fa-plus"></i></a></td>
                        </tr>-->
                        </tbody>
                         @endif
                           @if($factura->ClaseLiq == 'C' || $factura->ClaseLiq == 'A')
                            <th>Código</th>                    
                            <th>Concepto</th>
                            <th>Unidades</th>
                            <th>Precio U.</th>
                            <th>Total</th>
                            <th>SIN IVA</th>
                              <th></th>
                        </thead>
                        <tbody id="myTable">
                        @foreach ($detalles as $detalle)
                       <tr>
                        <td>{{$detalle->CODIGO}}</td>
                        <td><input type="hidden" name="iddetalle" class="iddetalle" value="{{$detalle->iddetalle}}"><input type="text" class="form-control concepto" name="" value="{{$detalle->CONCEPTO}}" placeholder="" ></td>
                        <td><input type="text" class="form-control unidades" name="" value="{{$detalle->UNIDADES}}" placeholder=""></td>
                        <td><input type="number" class="form-control precio" name="" step="0.01" value="{{$detalle->PRECIO}}" placeholder=""></td>
                        <td><input type="number" class="form-control total" name="" step="0.01" value="{{$detalle->PRECIO * $detalle->UNIDADES}}" placeholder="" readonly></td> 
                        <td><input type="checkbox" class="iva" name="" value=""></td>
                        <td><a class="btn btn-sm btn-success editar" href="#"><i class="fa fa-edit"></i> </a> <a class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a></td>
                       </tr>
                            @endforeach
                        <tr>
                        <td><input type="text" class="form-control codigo" name="" value="" placeholder=""></td>
                        <td><input type="text" class="form-control concepto" name="" value="" placeholder=""></td>
                        <td><input type="text" class="form-control unidades" name="" value="0" placeholder="" required></td>
                        <td><input type="number" class="form-control precio" name="" step="0.01" value="0" placeholder="" required></td>
                        <td><input type="number" class="form-control total" name="" step="0.01" value="0" placeholder="" readonly></td>
                        <td><input type="checkbox" class="iva" name="" value=""></td>
                        <td><a class="btn btn-success" id="add_detalle"><i class="fa fa-plus"></i></a></td>
                        </tr>
                        </tbody>
                         @endif
                         @else
                         <th>Código</th>                    
                            <th>Concepto</th>
                            <th>Unidades</th>
                            <th>Precio U.</th>
                            <th>Total</th>
                            <th>SIN IVA</th>
                              <th></th>
                        </thead>
                        <tbody id="myTable">
                        @foreach ($detalles as $detalle)
                       <tr>
                        <td>{{$detalle->CODIGO}}</td>
                        <td><input type="hidden" name="iddetalle" class="iddetalle" value="{{$detalle->iddetalle}}"><input type="text" class="form-control concepto" name="" value="{{$detalle->CONCEPTO}}" placeholder=""></td>
                        <td><input type="text" class="form-control unidades" name="" value="{{$detalle->UNIDADES}}" placeholder=""></td>
                        <td><input type="number" class="form-control precio" name="" step="0.01" value="{{$detalle->PRECIO}}" placeholder=""></td>
                        <td><input type="number" class="form-control total" name="" step="0.01" value="{{$detalle->PRECIO * $detalle->UNIDADES}}" placeholder="" readonly></td> 
                        <td><input type="checkbox" class="iva" name="" value=""></td>
                        <td><a class="btn btn-sm btn-success editar" href="#"><i class="fa fa-edit"></i> </a> <a class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a></td>
                       </tr>
                            @endforeach
                        <tr>
                        <td><input type="text" class="form-control codigo" name="" value="" placeholder=""></td>
                        <td><input type="text" class="form-control concepto" name="" value="" placeholder=""></td>
                        <td><input type="text" class="form-control unidades" name="" value="0" placeholder="" required></td>
                        <td><input type="number" class="form-control precio" name="" step="0.01" value="0" placeholder="" required></td>
                        <td><input type="number" class="form-control total" name="" step="0.01" value="0" placeholder="" readonly></td>
                        <td><input type="checkbox" class="iva" name="" value=""></td>
                        <td><a class="btn btn-success" id="add_detalle"><i class="fa fa-plus"></i></a></td>
                        </tr>
                        </tbody>
                        @endif  
                    </table>
                </div>
                    </div>
            </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

$("#volver").click(function(){
        window.history.go(-1); return true;
    });

 $(document).on('focusin','.datepicker',function(){
         $(this).datepicker({
        format: "dd/mm/yyyy",
        dateFormat: 'yy-mm-dd',
        language: "es",
        autoclose: true
    });

         $(this).selectpicker("data-live-search","true");

    });

  $(".bt-refresh").click(function(){

    
    $ID = $("#id").val();
    $eliminar = 0;

    bootbox.confirm({
    title: "Actualizar detalles de factura",
    message: "¿Desea volver a cargar los detalles con los parámetros actuales de la factura?",
    buttons: {
        cancel: {
            label: '<i class="fa fa-times"></i> Cancelar'
        },
        confirm: {
            label: '<i class="fa fa-check"></i> Aceptar'
        }
    },
    callback: function (result) {
        if(result){
            bootbox.confirm({
            title: "Actualizar detalles de factura",
            message: "¿Desea eliminar los detalles actuales?",
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> Mantener'
                },
                confirm: {
                    label: '<i class="fa fa-check"></i> Eliminar'
                }
            },
    callback: function (result) {
        if(result){
            $eliminar = 1;
        }
        setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '/facturas/its/refrescar_detalles',
                data : {'clave':$ID,'eliminar':$eliminar},
                success : function(data){
                    console.log(JSON.stringify(data));
                    if(data.includes("SQLSTATE")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                //location.reload();
                            }
                        });
                    }else{
                         bootbox.alert({
                            message: data,
                            callback: function () {
                                 location.reload();
                            }
                        });
                    }
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);
    }
});

     

        }


    }
});


});

  $(".bt-edit").click(function(){

    
    $ID = $("#id").val();

    $form = $("#form_contrato").serialize();
  
     setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '/facturas/its/editar',
                data : {'datos' : $("#form_contrato").serialize(),'clave':$ID},
                success : function(data){
                    console.log(JSON.stringify(data));
                    if(data.includes("SQLSTATE")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                //location.reload();
                            }
                        });
                    }else{
                         bootbox.alert({
                            message: data,
                            callback: function () {
                                 location.reload();
                            }
                        });
                    }
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

});

  $("#eliminar").click(function(){
    bootbox.confirm({
        title: "Eliminar Contrato",
        message: "<i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> ¿Está seguro que desea eliminar la factura?</strong> Tenga en cuenta que esta acción es irreversible.",
        buttons: {
            cancel: {
            label: '<i class="fa fa-times"></i> Cancelar'
        },
        confirm: {
            label: '<i class="fa fa-check"></i> Aceptar'
        }
        },
        callback: function (result) {
            if(result == true){
                $clave = $("#id").val();

                setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('facturas/europa/eliminar')}}',
                        data : {'clave':$clave},            
                        success : function(data){
                                    if(data.includes("SQLSTATE")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                //location.reload();
                            }
                        });
                    }else{
                         bootbox.alert({
                            message: data,
                            callback: function () {
                                 window.location = "/facturas/europa";
                            }
                        });
                    }
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);
            }
        }
    });

});


///eliminar el registro
  $(document).on('click','.btn-danger',function(){

                $iddetalle =  $(this).closest("tr").find('.iddetalle').val();
                $precio = $(this).closest("tr").find('.precio').val();
                $tipo = $("#TipoLiq").val();

bootbox.confirm({
        title: "Eliminar Contrato",
        message: "<i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> ¿Está seguro que desea eliminar el detalle de esta factura?</strong> Tenga en cuenta que esta acción es irreversible.",
        buttons: {
            cancel: {
            label: '<i class="fa fa-times"></i> Cancelar'
        },
        confirm: {
            label: '<i class="fa fa-check"></i> Aceptar'
        }
        },
        callback: function (result) {
            if(result == true){
             

                setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('facturas/its/eliminar_detalle')}}',
                        data : {'iddetalle':$iddetalle,'precio':$precio,'tipo':$tipo},            
                        success : function(data){
                                    if(data.includes("SQLSTATE")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                //location.reload();
                            }
                        });
                    }else{
                         bootbox.alert({
                            message: data,
                            callback: function () {
                                location.reload();
                            }
                        });
                    }
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);
            }
        }
    });

});

  $(document).on('focusout','.precio',function(){

    $unidades = $(this).closest("tr").find('.unidades').val();
    $precio = $(this).closest("tr").find('.precio').val();
    $(this).closest("tr").find('.total').val($precio * $unidades);
  });

   $(document).on('focusout','.unidades',function(){

    $unidades = $(this).closest("tr").find('.unidades').val();
    $precio = $(this).closest("tr").find('.precio').val();
    $(this).closest("tr").find('.total').val($precio * $unidades);
  });



  $(document).on('click','.editar',function(){

    $iddetalle = null;
     $unidades = null;
     $precio = null;
     $concepto  = null;
     $clave = $ID = $("#id").val();
     $iva = 0;
     $fecha = null;
     $matricula = null;
     $remolque = null;

if($(this).closest("tr").find('.iddetalle').val()){
   $iddetalle =  $(this).closest("tr").find('.iddetalle').val(); 
}

if($(this).closest("tr").find('.unidades').val()){
   $unidades = $(this).closest("tr").find('.unidades').val(); 
}

if($(this).closest("tr").find('.precio').val()){
  $precio = $(this).closest("tr").find('.precio').val();  
}


if($(this).closest("tr").find('.concepto').val()){
    $concepto = $(this).closest("tr").find('.concepto').val();
}


if($fecha = $(this).closest("tr").find('.fecha').val()){
  $fecha = $(this).closest("tr").find('.fecha').val();  
}



if($(this).closest("tr").find('.matricula').val()){
  $matricula = $(this).closest("tr").find('.matricula').val();  
}

if($(this).closest("tr").find('.remolque').val()){
  $remolque = $(this).closest("tr").find('.remolque').val(); 
}


if($(this).closest("tr").find('.fecha').prop("checked",true)){
    $iva = 1;
}

    

setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('facturas/its/editar_detalle')}}',
                        data : {'iddetalle':$iddetalle,'unidades':$unidades,'precio':$precio,'concepto':$concepto,'clave':$clave,'fecha':$fecha,'iva':$iva,'matricula':$matricula,'remolque':$remolque},            
                        success : function(data){
                            console.log(JSON.stringify(data));
                    if(data.includes("SQLSTATE")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                //location.reload();
                            }
                        });
                    }else{
                         bootbox.alert({
                            message: data,
                            callback: function () {
                                 location.reload();
                            }
                        });
                    }
                   
                    $("#loading").hide();
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);

});

  $(document).on('click','#add_detalle',function(){

$iddetalle = null;
     $unidades = null;
     $precio = null;
     $concepto  = null;
     $clave = $ID = $("#id").val();
     $iva = 0;
     $fecha = null;
     $matricula = null;
     $remolque = null;

if($(this).closest("tr").find('.iddetalle').val()){
   $iddetalle =  $(this).closest("tr").find('.iddetalle').val(); 
}

if($(this).closest("tr").find('.unidades').val()){
   $unidades = $(this).closest("tr").find('.unidades').val(); 
}

if($(this).closest("tr").find('.precio').val()){
  $precio = $(this).closest("tr").find('.precio').val();  
}


if($(this).closest("tr").find('.concepto').val()){
    $concepto = $(this).closest("tr").find('.concepto').val();
}


if($fecha = $(this).closest("tr").find('.fecha').val()){
  $fecha = $(this).closest("tr").find('.fecha').val();  
}



if($(this).closest("tr").find('.matricula').val()){
  $matricula = $(this).closest("tr").find('.matricula').val();  
}

if($(this).closest("tr").find('.remolque').val()){
  $remolque = $(this).closest("tr").find('.remolque').val(); 
}


if($(this).closest("tr").find('.fecha').prop("checked",true)){
    $iva = 1;
}

    

setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('facturas/its/insertar_detalle')}}',
                        data : {'iddetalle':$iddetalle,'unidades':$unidades,'precio':$precio,'concepto':$concepto,'clave':$clave,'fecha':$fecha,'iva':$iva,'matricula':$matricula,'remolque':$remolque},            
                        success : function(data){
                            console.log(JSON.stringify(data));
                    if(data.includes("SQLSTATE")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                //location.reload();
                            }
                        });
                    }else{
                         bootbox.alert({
                            message: data,
                            callback: function () {
                                 location.reload();
                            }
                        });
                    }
                   
                    $("#loading").hide();
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);

});


$(".bt-export_factura").click(function(){

    
    $clave = $("#id").val();

    $baseimponible = $("#baseimponible").val();
    $iva = $("#ivaprecio").val();
    $provision = $("#provision").val();
    $total = $("#importe").val();
    $numfactura = $("#numfactura").val();

  window.location = '/facturas/its/exportar_factura_its?clave='+$clave+'&baseimponible='+$baseimponible+'&iva='+$iva+'&provision='+$provision+'&total='+$total+'&numfactura='+$numfactura;



});

</script>

@endsection