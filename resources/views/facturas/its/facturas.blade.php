@extends('layouts.app')
@section('content')

<div class="container">
    <div class="row">
        <div class="col-sm-2">
            <h4>Factura</h4>
            <div class =filtros>
                 <a data-toggle="collapse" href="#fechallegada" aria-expanded="false" aria-controls="fechallegada"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> fecha de emisión</h5></a>
                <div class="collapse" id="fechallegada">
                <input type="text" class="form-control datepicker" name="fecha_alta_inicio" id="fecha_alta_inicio" value="" placeholder="fecha de inicio...">
                <br>
                <input type="text" class="form-control datepicker" id="fecha_alta_fin" value="" placeholder="fecha de fin...">
                </div>
            </div>
             <div class="filtros">
           
                <a data-toggle="collapse" href="#fechavenc" aria-expanded="false" aria-controls="fechavenc"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Vencimiento</h5></a>
                <div class="collapse" id="fechavenc">
                <input type="text" class="form-control datepicker" name="fecha_venc_inicio" id="fecha_venc_inicio" value="" placeholder="fecha de inicio...">
                <br>
                <input type="text" class="form-control datepicker" id="fecha_venc_fin" name="fecha_venc_fin" value="" placeholder="fecha de fin...">
                </div>
            </div>
                <div class =filtros>
                 <a data-toggle="collapse" href="#colcodcontrato" aria-expanded="false" aria-controls="colcodcontrato"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Nª Factura/Abono</h5></a>
                <div class="collapse" id="colcodcontrato">
                <input type="text" class="form-control" name="numfactura" id="numfactura" value="" placeholder=".">
                </div>
            </div>
                 <div class =filtros>
                 <a data-toggle="collapse" href="#colcodcontratof" aria-expanded="false" aria-controls="colcodcontratof"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Código de contrato</h5></a>
                <div class="collapse" id="colcodcontratof">
                <input type="text" class="form-control" name="codContrato" id="codContrato" value="" placeholder=".">
                </div>
            </div>
                <div class =filtros>
                 <a data-toggle="collapse" href="#colclavefac" aria-expanded="false" aria-controls="colclavefac"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Clave de facturación</h5></a>
                <div class="collapse" id="colclavefac">
                <input type="text" class="form-control" name="clavefactura" id="clavefactura" value="" placeholder=".">
                </div>
            </div>
             <div class =filtros>
                 <a data-toggle="collapse" href="#colcif" aria-expanded="false" aria-controls="colcif"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Cif del cliente</h5></a>
                <div class="collapse" id="colcif">
               <select class="selectpicker" data-live-search="true" title="Buscar..." id="cif" name="cif">
                 <option selected></option>
                    @foreach ($empresas as $empresa)
                    <option value="{{$empresa->CIF}}">{{ $empresa->CIF}}</option>
                    @endforeach
                </select>
                </div>
            </div>
                  <div class =filtros>
                 <a data-toggle="collapse" href="#colempresa" aria-expanded="false" aria-controls="colempresa"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Cliente</h5></a>
                <div class="collapse" id="colempresa">
               <select class="selectpicker" data-live-search="true" title="Buscar..." id="empresa" name="empresa">
                 <option selected></option>
                    @foreach ($empresas as $empresa)
                    <option value="{{$empresa->CIF}}">{{ $empresa->Empresa}}</option>
                    @endforeach
                </select>
                </div>
            </div>
                <div class =filtros>
                 <a data-toggle="collapse" href="#colfac" aria-expanded="false" aria-controls="colfac"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Factura/Abono</h5></a>
                <div class="collapse" id="colfac">
                <select class="selectpicker" data-live-search="true" title="Buscar..." id="fac" name="fac">
                 <option selected></option>
                    <option value="A">Abono</option>
                    <option value="F">Factura</option>

                </select>
                </div>
                </div>
              <div class =filtros>
                 <a data-toggle="collapse" href="#coltipo" aria-expanded="false" aria-controls="coltipo"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Tipo Factura/Abono</h5></a>
                <div class="collapse" id="coltipo">
                <select class="selectpicker" data-live-search="true" title="Buscar..." id="tipo" name="tipo">
                 <option selected></option>
                    <option value="C">Corriente</option>
                    <option value="T">De Contrato</option>
                    <option value="H">Desinmov H</option>
                    <option value="G">Elec cheque G</option>
                    <option value="A">Elec cheque A</option>
                    <option value="E">asis mec E</option>
                    <option value="F">asis mec F</option>
                    <option value="D">asis mec D</option>
                    <option value="B">asis mec B</option>
                </select>
                </div>
            </div> 
            <div class =filtros>
                 <a data-toggle="collapse" href="#colprovision" aria-expanded="false" aria-controls="colprovision"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Provisión Fondo</h5></a>
                <div class="collapse" id="colprovision">
                <input type="checkbox" name="provisionfondos" value="" id="provisionfondos">
                </div>
            </div>
            <div class="filtros">
                <a data-toggle="collapse" href="#colcontratante" aria-expanded="false" aria-controls="colcontratante"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Entidad Emisora</h5></a>
                 <div class="collapse" id="colcontratante">
                 <select class="selectpicker" data-live-search="true" title="Buscar..." id="contratante" name="contratante">
                 <option selected></option>
                    @foreach ($contratantes as $contratante)
                    <option value="{{$contratante->CIF}}">{{ $contratante->Empresa}}</option>
                    @endforeach
                </select>
                </div>
            </div>
            <div class =filtros>
                 <a data-toggle="collapse" href="#colnotas" aria-expanded="false" aria-controls="colnotas"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Notas</h5></a>
                <div class="collapse" id="colnotas">
                <input type="text" class="form-control" name="notas" id="notas" value="" placeholder=".">
                </div>
            </div>
                <h4>Detalles de factura</h4>
               <div class =filtros>
                 <a data-toggle="collapse" href="#colcoddetalle" aria-expanded="false" aria-controls="colcoddetalle"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Código</h5></a>
                <div class="collapse" id="colcoddetalle">
                <input type="text" class="form-control" name="coddetalle" id="coddetalle" value="" placeholder=".">
                </div>
            </div>
            <div class =filtros>
                 <a data-toggle="collapse" href="#colprecio" aria-expanded="false" aria-controls="colprecio"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Precio</h5></a>
                <div class="collapse" id="colprecio">
                <input type="text" class="form-control" name="precio" id="precio" value="" placeholder=".">
                </div>
            </div>
           <div class="filtros">
            <a data-toggle="collapse" href="#colmatricula" aria-expanded="false" aria-controls="colmatricula"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Matricula</h5></a>
            <div class="collapse" id="colmatricula">
             <select class="selectpicker" data-live-search="true" title="Buscar..." id="matricula" name="matricula">
                 <option selected></option>
                    @foreach ($matriculas as $matricula)
                    <option>{{ $matricula->Matricula}}</option>
                    @endforeach
                </select>
                </div>
                </div>
            <div class =filtros>
                 <a data-toggle="collapse" href="colconcepto" aria-expanded="false" aria-controls="colconcepto"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Concepto</h5></a>
                <div class="collapse" id="colconcepto">
                <input type="text" class="form-control" name="concepto" id="concepto" value="" placeholder=".">
                </div>
            </div>
            <div class =filtros>
                 <a data-toggle="collapse" href="#colunidades" aria-expanded="false" aria-controls="colunidades"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Unidades</h5></a>
                <div class="collapse" id="colunidades">
                <input type="text" class="form-control" name="unidades" id="unidades" value="" placeholder=".">
                </div>
            </div>
            <div class="filtros">
            <button type="button" id="filtrar" class="btn btn-default btn-primary">filtrar</button>
            </div>
            </div>
        <div class="col-sm-10">
            <div class="panel panel-default">
                <div class="row"> 
                    <div class="panel-heading" id="sanciones-header">                    
                        <div class="col-md-2" style="width: 100%">
                            <h3 class="module-title">Facturas ITS</h3>
                        </div>
                        <div class="col-md-10">
                            <div>
                                <ul class="list-inline nav navbar-nav">
                                    <li>
                                    <a class="dropdown-toggle" href="#" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="fa fa-file-text-o"></span> Crear nuevo Abono/Factura</a>
                                       <ul class="dropdown-menu" role="menu">
                                           <li><a class="dropdown-item crear_fac " href="#" value="C">Corriente</a></li>
                                           <li><a class="dropdown-item crear_fac"  href="#">Desinmov H</a></li>
                                           <li><a class="dropdown-item crear_fac"  href="#">Electrocheque G</a></li>
                                           <li><a class="dropdown-item crear_fac"  href="#">Electrocheque (anterior)</a></li>
                                           <li><a class="dropdown-item crear_fac"  href="#">asis mec E</a></li>
                                           <li><a class="dropdown-item crear_fac"  href="#">asis mec F</a></li>
                                           <li><a class="dropdown-item crear_fac"  href="#">asis mec D</a></li>
                                           <li><a class="dropdown-item crear_fac"  href="#">asis mec B</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                    <li><a href="{{url('facturas/europa')}}"><i class="fa fa-exchange" aria-hidden="true"></i></i> Ir a Europa</a></li>
                                    <li><a href="#" id="excel"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Excel</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table-responsive"">
                    <table class="table">
                        <thead class="table-header">
                            <th>Cliente</th>                        
                            <th>Nª Fac.</th>
                            <th>Tipo</th>
                            <th>Clase</th>
                            <th>Notas</th>
                            <th></th>
                        </thead>
                        <tbody id="myTable">
                        @foreach ($facturas as $factura)
                        <tr>
                            <td>{{ $factura->cliente }} </td>
                            <td>{{ $factura->numero }} </td>
                            <td>{{ $factura->tipo }} </td>
                            <td>{{ $factura->clase }} </td>                           
                            <td>{{ $factura->Notas }} </td>
                            <td><a class="btn btn-sm btn-success" href="{!! url('facturas/its/'.$factura->idfactura) !!}"><i class="fa fa-edit"></i></a></td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    
                </div>

                 <div class="col-md-12 text-center">
      <ul class="pagination pagination-lg pager" id="myPager"></ul>
      </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

 $('.datepicker').datepicker({
        format: "dd/mm/yyyy",
        dateFormat: 'yy-mm-dd',
        language: "es",
        autoclose: true
    });
    
    $('#search').on('keyup',function(){
        $value=$(this).val();
        //alert($value);
        $("#loading").show();
        setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('contratos/search_europa')}}',
                data : {'search':$value},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('tbody').html(data);
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);
});

$( "#filtrar" ).click(function() {
$fecha_alta_inicio = $("#fecha_alta_inicio").val();
$fecha_alta_fin = $("#fecha_alta_fin").val();

$numfactura = $("#numfactura").val();
$codContrato = $("#codContrato").val();
$clavefactura = $("#clavefactura").val();
$empresa = $("#empresa option:selected").val();
$cif = $("#cif option:selected").text();
$fac = $("#fac option:selected").val();
$tipo = $("#tipo option:selected").val();
$contratante = $("#contratante option:selected").val();
$notas = $("#notas").val();

$coddetalle = $("#coddetalle").val();
$precio = $("#precio").val();
$matricula = $("#matricula option:selected").text();
$unidades = $("#unidades").val();


$provisionfondos = 0;


if($("#provisionfondos").prop("checked")){
$provisionfondos = 1;
}



$alta_inicio = $fecha_alta_inicio.split("/").reverse().join("-");
$alta_fin = $fecha_alta_fin.split("/").reverse().join("-");

        $dialog = bootbox.dialog({
                        message: '<p class="text-center">Cargando datos, espere por favor...</p>',
                        closeButton: false
                    });

    setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('facturas/its/filtra_its')}}',
                data : {'alta_inicio':$alta_inicio,'alta_fin':$alta_fin,'contratante':$contratante,'empresa':$empresa,'matricula':$matricula,'codContrato':$codContrato,'numfactura':$numfactura,'clavefactura':$clavefactura,'fac':$fac,'tipo':$tipo,'notas':$notas,'coddetalle':$coddetalle,'precio':$precio,'unidades':$unidades,'cif':$cif},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('tbody').html(data);

                    // do something in the background
                    $dialog.modal('hide');
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

});


 $(".crear_fac").click(function(){

    $clase = "";
    if($(this).text() == "Corriente"){
        $clase = "C";
    }
     if($(this).text() == "Desinmov H"){
        $clase = "H";
    }
     if($(this).text() == "Electrocheque G"){
        $clase = "G";
    }
     if($(this).text() == "Electrocheque (anterior)"){
        $clase = "A";
    }
     if($(this).text() == "asis mec E"){
        $clase = "E";
    }
     if($(this).text() == "asis mec F"){
        $clase = "F";
    }
     if($(this).text() == "asis mec D"){
        $clase = "D";
    }
     if($(this).text() == "asis mec B"){
        $clase = "B";
    }

    window.location = "/facturas/its/nuevo/"+$clase;
});

</script>
@endsection