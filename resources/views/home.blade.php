@extends('layouts.app')

@section('content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.2.2/Chart.bundle.js"></script>

<div class="container">
    <div class="row">
        <div class="col-md-12 col-md-offset-0">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="charts">
                        <div id="chart1" class="col-md-4">
                            <canvas id="myChart" width="300" height="300" ></canvas>
                        </div>
                        <div id="chart2" class="col-md-4">
                            <canvas id="myChart2" width="300" height="300" ></canvas>
                        </div>
                        <div id="chart3" class="col-md-4">
                            <canvas id="myChart3" width="300" height="300" ></canvas>
                        </div>
                    
                        <div id="chart4" class="col-md-4">
                            <canvas id="myChart4" width="300" height="300" ></canvas>
                        </div>
                        <div id="chart5" class="col-md-4">
                            <canvas id="myChart5" width="300" height="300" ></canvas>
                        </div>
                        <div id="chart6" class="col-md-4">
                            <canvas id="myChart6" width="300" height="300" ></canvas>
                        </div>
                    </div>
                </div> 

                </div>
        </div>
    </div>
</div>
<script>
var ctx = document.getElementById("myChart");
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre"],
        datasets: [{
            label: 'Número de sanciones en los últimos 6 meses',
            data: [20, 40, 30, 50, 65, 90],
            backgroundColor: [                
                'rgba(54, 162, 235, 0.8)',
                'rgba(54, 162, 235, 0.8)',
                'rgba(54, 162, 235, 0.8)',
                'rgba(54, 162, 235, 0.8)',
                'rgba(54, 162, 235, 0.8)',
                'rgba(54, 162, 235, 0.8)'
            ],
            borderColor: [
                 'rgba(54, 162, 235, 0.8)',
                'rgba(54, 162, 235, 0.8)',
                'rgba(54, 162, 235, 0.8)',
                'rgba(54, 162, 235, 0.8)',
                'rgba(54, 162, 235, 0.8)',
                'rgba(54, 162, 235, 0.8)'
            ],
            borderWidth: 3
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
<script>
var ctx2 = document.getElementById("myChart2");
var myChart2 = new Chart(ctx2, {
    type: 'line',
    data: {
        labels: ["Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre"],
        datasets: [{
            label: 'Número de sanciones en los últimos 6 meses',
            data: [20, 40, 30, 50, 65, 90],
            fillColor : 'rgba(251,40,25,0.9)',
            borderWidth: 3
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
<script>
var ctx3 = document.getElementById("myChart3");
var myChart3 = new Chart(ctx3, {
    type: 'radar',
    data: {
        labels: ["Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre"],
        datasets: [{
            label: 'Número de sanciones en los últimos 6 meses',
            data: [20, 40, 30, 50, 65, 90],
            backgroundColor: [                
                'rgba(54, 162, 235, 0.8)',
                'rgba(54, 162, 235, 0.8)',
                'rgba(54, 162, 235, 0.8)',
                'rgba(54, 162, 235, 0.8)',
                'rgba(54, 162, 235, 0.8)',
                'rgba(54, 162, 235, 0.8)'
            ],
            borderColor: [
                 'rgba(54, 162, 235, 0.8)',
                'rgba(54, 162, 235, 0.8)',
                'rgba(54, 162, 235, 0.8)',
                'rgba(54, 162, 235, 0.8)',
                'rgba(54, 162, 235, 0.8)',
                'rgba(54, 162, 235, 0.8)'
            ],
            borderWidth: 3
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
<script>
var ctx4 = document.getElementById("myChart4");
var myChar4 = new Chart(ctx4, {
    type: 'pie',
    data: {
        labels: ["Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre"],
        datasets: [{
            label: 'Número de sanciones en los últimos 6 meses',
            data: [20, 40, 30, 50, 65, 90],
            backgroundColor: [                
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 3
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
<script>
var ctx5 = document.getElementById("myChart5");
var myChart5 = new Chart(ctx5, {
    type: 'polarArea',
    data: {
        labels: ["Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre"],
        datasets: [{
            label: 'Número de sanciones en los últimos 6 meses',
            data: [20, 40, 30, 50, 65, 90],
            fillColor : 'rgba(251,40,25,0.9)',
            borderWidth: 3
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
<script>
var ctx6 = document.getElementById("myChart6");
var myChart6 = new Chart(ctx6, {
    type: 'doughnut',
    data: {
        labels: ["Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre"],
        datasets: [{
            label: 'Número de sanciones en los últimos 6 meses',
            data: [20, 40, 30, 50, 65, 90],
            backgroundColor: [                
                'rgba(54, 162, 235, 0.8)',
                'rgba(54, 162, 235, 0.8)',
                'rgba(54, 162, 235, 0.8)',
                'rgba(54, 162, 235, 0.8)',
                'rgba(54, 162, 235, 0.8)',
                'rgba(54, 162, 235, 0.8)'
            ],
            borderColor: [
                 'rgba(54, 162, 235, 0.8)',
                'rgba(54, 162, 235, 0.8)',
                'rgba(54, 162, 235, 0.8)',
                'rgba(54, 162, 235, 0.8)',
                'rgba(54, 162, 235, 0.8)',
                'rgba(54, 162, 235, 0.8)'
            ],
            borderWidth: 3
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
@endsection
