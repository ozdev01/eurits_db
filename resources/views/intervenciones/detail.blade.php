@extends('layouts.app')
@section('content')

   
<div class="container">
    <div class="row">
        <div class="col-xl-12 col-md-offset-0">
            <div class="panel panel-default">
                <div class="row"> 
                    <div class="panel-heading" id="intervenciones-header">                    
                        <div class="col-md-6">
                            <h3 class="module-title">Intervención {!! $intervencion->ID !!} - {{$intervencion->CLIENTE}}</h3>
                            <input type="hidden" name="id" id="IDintervencion" value="{!! $intervencion->ID !!}">
                        </div>
                        <div class="col-md-6">
                            <div>
                                <ul class="nav navbar-nav">
                                    <li><a href="#" class="bt-edit" id="guardar"><i class="fa fa-save"> </i> Guardar</a></li>
                                    <li><a href="#" class="bt-edit" id="add_tramite"><i class="fa fa-plus" aria-hidden="true"></i> </i> Añadir trámite</a></li>
                                    <a class="dropdown-toggle" href="#" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                       <li> <i class="fa fa-file-text-o" aria-hidden="true"></i> Informes</a>
                                    <ul class="dropdown-menu" role="menu">
                                         <li><a class="dropdown-item" id="parte_multa" href="#">Parte de multa</a></li>
                                        <li><a class="dropdown-item" id="parte_cliente" href="#">Parte de cliente</a></li>
                                        <li><a class="dropdown-item" id="fax_france" href="#">Fax a France Truck</a></li>
                                        <li><a class="dropdown-item" id="parte_tramites" href="#">Informe de trámites</a></li>
                                    </ul></li>
                                    <li><a href="#" id="eliminar"><i class="fa fa-trash"> </i> Eliminar</a></li>
                                    <li><a class="dropdown-item" id="imprimir_recibos" href="#" id="newtipo"><i class="fa fa-print"> </i> Imprimir recibos</a></li>
                                    <li><a href="#" id="volver"><i class="fa fa-arrow-left"> </i> Volver</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="panel-body" style="border-top:2px solid #335599;">                        
                    <form class="inline-form" id="form_intervencion">
                        <div class="form-group col-md-12">
                        @if($contrato !=null)
                         @if($no_contratada == 0 && $contrato->Baja == null && $contrato->TipoContrato == 'AJT')
                            <label style="color:blue">Contrato de Multas, Alta {{date('d/m/Y', strtotime($contrato->FechaContrato))}}, Baja {{date('d/m/Y', strtotime($contrato->Vencimiento))}}</label>
                        @elseif($no_contratada == 0 && $contrato->Baja !=null && $contrato->TipoContrato == 'AJT')
                            <label style="color:red">Contrato de Multas, Alta {{date('d/m/Y', strtotime($contrato->FechaContrato))}}, Baja {{date('d/m/Y', strtotime($contrato->Baja))}}</label>
                        @elseif($no_contratada == 1)
                            <label style="color:red">No existe ningún contrato asociado a esta matrícula</label>
                            <input type="checkbox" name="no_contratada" value="">
                            @endif 
                            @endif
                        <label></label>
                        </div>
                        <div class="form-group col-md-12">
                        @if($cliente!=null)
                        @if($cliente->devolucion ==1)
                        <div class="form-group col-md-5">
                        <label style="background:purple;color:white">Paraliz. Serv. Impago/Pte Devolución</label>
                        </div>
                        @endif
                         @if($cliente->cteConflictivo ==1)
                        <div class="form-group col-md-3">
                        <label style="background:blue;color:white">Cliente Conflictivo</label>
                        </div>
                        @endif
                         @if($cliente->impago_multas ==1)
                        <div class="form-group col-md-3">
                        <label style="background:#b40091;color:white">Impago Multas</label>
                        </div>
                        @endif
                        @endif
                        </div>
                        <div class="form-group col-md-2">
                            <label for="expediente">Fecha Apertura</label>
                            <input type="text" class="form-control datepicker" name="fecha_apertura" value="{{ date('d/m/Y', strtotime($intervencion->F_APERTURA)) }}">                         
                        </div>
                            <div class="form-group col-md-2">
                            <label for="expediente">Hora Apertura</label>
                            <input type="text" class="form-control" name="hora_apertura" value="{{ date('H:i', strtotime($intervencion->HORA)) }}">                         
                        </div>
                        <div class="form-group col-md-2">
                            <label for="expediente">Fecha Cierre</label>
                            <input type="text" class="form-control datepicker" name="fecha" value="{{ date('d/m/Y', strtotime($intervencion->FECHA)) }}">                         
                        </div>
                         <div class="form-group col-md-2">
                            <label for="expediente">Hora Cierre</label>
                            <input type="text" class="form-control" name="hora" value="{{ date('H:i', strtotime($intervencion->Fecha_actualizacion)) }}">                         
                        </div>
                        <div class="form-group col-md-2">
                            <label for="expediente">Dossier Interno</label>
                            <input type="text" class="form-control" name="acuerdo" id="acuerdo" value="{{ $intervencion->ACUERDO }}">                         
                        </div>
                        <div class="form-group col-md-2">
                            <label for="expediente">Dossier externo</label>
                            <input type="text" class="form-control" name="dossier" value="{{ $intervencion->NumDossier }}">                         
                        </div>
                        <div class="form-group col-md-2">
                            <label for="expediente">Matricula</label>
                            <input type="text" class="form-control" name="matricula" value="{{ $intervencion->MATRICULA }}">                         
                        </div>
                        <div class="form-group col-md-2">
                            <label for="expediente">NIF</label>
                            <input type="text" class="form-control" name="cif" value="{{ $intervencion->CIF}}">                         
                        </div>
                         <div class="form-group col-md-3">
                            <label for="expediente">Cliente</label>
                            <input type="text" class="form-control" name="cliente" value="{{ $intervencion->CLIENTE}}">                         
                        </div>
                    
                          <div class="form-group col-md-3">
                            <label for="expediente">Empresa Comercial</label>
                           <select class="selectpicker" data-live-search="true" title="Buscar..." id="contratante" name="contratante">
                                    @foreach ($contratantes as $contratante)
                                    @if($contratante->Num_empresa == $intervencion->EMPRESA)
                                    <option value="{{$contratante->Num_empresa}}" selected>{{ $contratante->Empresa}}</option>
                                    @else
                                    <option value="{{$contratante->Num_empresa}}">{{ $contratante->Empresa}}</option>
                                    @endif
                                    @endforeach
                            </select>                           
                        </div>
                             <div class="form-group col-md-3">
                            <label for="expediente">Comercial Autorizante</label>
                           <select class="selectpicker" data-live-search="true" title="Buscar..." id="comercial" name="comercial">
                                    @foreach ($comerciales as $comercial)
                                    @if($comercial->idcomercial == $intervencion->IdComercial )
                                    <option value="{{$comercial->idcomercial}}" selected>{{ $comercial->Nombre.' '.$comercial->Apellidos}}</option>
                                    @else
                                     <option value="{{$comercial->idcomercial}}">{{ $comercial->Nombre.' '.$comercial->Apellidos}}</option>
                                    @endif
                                    @endforeach
                            </select>                           
                        </div>
                         <div class="form-group col-md-3">
                            <label for="expediente">Ordenante</label>
                            <input type="text" class="form-control" name="ordenante" value="{{ $intervencion->DEMANDANTE}}">      
                        </div>
                         <div class="form-group col-md-3">
                            <label for="expediente">Chófer</label>
                            <input type="text" class="form-control" name="conductor" value="{{ $intervencion->CHOFER}}">                         
                        </div>
                        <div class="form-group col-md-3">
                            <label for="expediente">Lugar</label>
                            <input type="text" class="form-control" name="lugar" value="{{ $intervencion->LUGAR}}">                         
                        </div>
                        <div class="form-group col-md-3">
                            <label for="expediente">Ciudad</label>
                            <input type="text" class="form-control" name="ciudad" value="{{ $intervencion->CIUDAD}}">                         
                        </div>
                        <div class="form-group col-md-3">
                            <label for="expediente">País</label>
                            <select class="selectpicker" data-live-search="true" title="Buscar..." id="pais" name="pais">
                                    @foreach ($paises as $pais)
                                    @if($pais->codpais == $intervencion->PAIS)
                                    <option value="{{$pais->codpais}}" selected>{{ $pais->Pais}}</option>
                                    @else
                                    <option value="{{$pais->codpais}}">{{ $pais->Pais}}</option>
                                    @endif
                                    @endforeach
                            </select>                          
                        </div>
                              
                          <div class="form-group col-md-3">
                            <label for="expediente">Autoridad</label>
                            <select class="selectpicker" data-live-search="true" title="Buscar..." id="autoridad" name="autoridad">
                                    @foreach ($autoridades as $autoridad)
                                    @if($autoridad->Autoridad == $intervencion->Autoridad)
                                    <option value="{{$autoridad->IDautoridad}}" selected>{{ $autoridad->Autoridad}}</option>
                                    @else
                                     <option value="{{$autoridad->IDautoridad}}">{{ $autoridad->Autoridad}}</option>
                                    @endif
                                    @endforeach
                            </select>                          
                        </div>
                            <div class="form-group col-md-3">
                            <label for="expediente">Tel. Fijo</label>
                            <input type="text" class="form-control" name="telefono" value="{{ $intervencion->TelefAutoridad}}">                         
                        </div>
                            <div class="form-group col-md-3">
                            <label for="expediente">Tel. Móvil</label>
                            <input type="text" class="form-control" name="asistido" value="{{ $intervencion->Asistido}}">                         
                        </div>
                            <div class="form-group col-md-3">
                            <label for="expediente">Boletín</label>
                            <input type="text" class="form-control" name="boletin" value="{{ $intervencion->NumBoletin}}">                         
                        </div>
                            <div class="form-group col-md-3">
                            <label for="expediente">Fax Autoridad</label>
                            <input type="text" class="form-control" name="fax" value="{{ $intervencion->FaxAutoridad}}">                         
                        </div>
                        <div class="form-group col-md-2">
                            <label for="expediente">Fecha Remisión</label>
                            <input type="text" class="form-control datepicker" name="fecharemision" value="{{ date('d/m/Y', strtotime($intervencion->fecharemision)) }}">                         
                        </div>
                        <div class="form-group col-md-6">
                        <label for="cif">Facturas</label>
                        <ul style="    border: 1px solid gray; border-radius: 4px; box-shadow: inset 0 1px 1px rgba(0,0,0,.075); padding: 20px;">
                        <ul class="list-inline" style="padding: 5px !important">
                        <li><label for="expediente">Factura Proveedor</label>
                            <input type="text" class="form-control" name="facturadoTAI" value="{{ $intervencion->FacturadoTAI}}"></li>
                        <li><label for="expediente">Importe Proveedor</label>
                            <input type="text" class="form-control" name="ImporteFProv" value="{{ $intervencion->ImporteFProv}}">  </li>
                        </ul>
                        <ul class="list-inline" style="padding: 5px !important">
                        <li><label for="expediente">Factura ITS</label>
                            <input type="text" class="form-control" name="FacturaITS" value="{{ $intervencion->FacturaITS}}">   </li>
                        <li><label for="expediente">Importe ITS</label>
                            <input type="text" class="form-control" name="ImporteFITS" value="{{ $intervencion->ImporteFITS}}"></li>
                        </ul>
                        </ul>
                        </div>
                         <div class="form-group col-md-3">
                            <label for="expediente">Importe</label>
                            <input type="number" class="form-control" name="importe" value="{{ $intervencion->IMPORTE}}">      
                        </div>
                         <div class="form-group col-md-3">
                            <label for="expediente">Gastos</label>
                            <input type="number" class="form-control" name="gastos" value="{{ $intervencion->Gastos}}">      
                        </div>
                          <div class="form-group col-md-5">
                            <label for="expediente">Notas</label>
                            <textarea name="notas" class="form-control" rows="4">{{$intervencion->Notas}}</textarea>    
                        </div>
                        @if($cliente!=null)
                        <div class="form-group col-md-5">
                            <label for="expediente">Oservaciones Cliente</label>
                            <textarea name="observaciones" class="form-control" rows="4" readonly>{{$observaciones_cliente->Observacion}}</textarea>
                        </div>
                            @endif  
                        <div class="form-group col-md-2">
                            <label for="expediente">F.C.</label>
                            @if($intervencion->FaxConf == 1)
                            <input type="checkbox" name="fc" value="" checked>
                            @else
                            <input type="checkbox" name="fc" value="">
                            @endif                    
                        </div>
                           <div class="form-group col-md-2">
                            <label for="expediente">P.Q.</label>
                            @if($intervencion->pendienteQuittance == 1)
                            <input type="checkbox" name="pq" value="" checked>
                            @else
                            <input type="checkbox" name="pq" value="">
                            @endif                    
                        </div>
                        <div class="form-group col-md-2">
                            <label for="expediente">No contratada</label>
                            @if($no_contratada == 1)
                            <input type="checkbox" name="no_contratada" value="" checked>
                            @else
                            <input type="checkbox" name="no_contratada" value="">
                            @endif                    
                        </div>
                        <div class="form-group col-md-2">
                            <label for="expediente">Comercial Autoriza</label>
                            <input type="checkbox" name="comercialautoriz" value="">                        
                        </div>
                        <div class="form-group col-md-12">
                        @if($intervencion->IMPORTE > 3000)
                        <div class="form-group col-md-3">
                        <label style="background:#006d1d;color:white">Aviso de autorización</label>
                        </div>
                        @endif
                          @if($no_contratada == 1)
                        <div class="form-group col-md-3">
                        <label style="background:#b40091;color:white">Avisar a Silvie</label>
                        </div>
                        @endif
                         @if($pedir_autorizacion == 1)
                        <div class="form-group col-md-3">
                        <label style="background:#0083c3;color:white">Pedir Autorización para CAUCIÓN</label>
                        </div>
                        @endif
                         @if($cliente_importante == 1)
                        <div class="form-group col-md-3">
                        <label style="background:red;color:white">Cliente Importante</label>
                        </div>
                        @endif
                          @if($en_renovacion == 1)
                        <div class="form-group col-md-3">
                        <label style="background:red;color:white">Avisar Comercial: en Período Renovación</label>
                        </div>
                        @endif
                         @if($avisar_comercial == 1)
                        <div class="form-group col-md-3">
                        <label style="background:#000c67;color:white">Informar al comercial</label>
                        </div>
                        @endif
                          @if($tarjetas_activas != null)
                        <div class="form-group col-md-3">
                        <label style="background:#75a900;color:black">Cliente con tarjetas Activas</label>
                        </div>
                        @endif
                         @if($avisar_empresa == 1)
                        <div class="form-group col-md-3">
                        <label style="background:#870000;color:white">Avisar a empresa</label>
                        </div>
                        @endif
                        @if($cobertura_francia != null)
                        <div class="form-group col-md-3">
                        <label style="background:#b40091;color:white">Cobertura Francia</label>
                        </div>
                        @endif
                        </div>
                    </form>
                    </div>                        
                </div>
            </div>
           <div class="panel panel-default">
              <div class="row"> 
                    <div class="panel-heading" id="detalles-header">                    
                        <div class="col-md-6">
                            <h3 class="module-title">Detalles</h3>
                        </div>
                        <div class="col-md-6">
                            <div>
                                <ul class="nav navbar-nav">
                                    <li class="detalles"><a id="motivos"><i> </i>Motivos</a></li>
                                    <li class="detalles"><a id= "tramites"><i> </i>Trámites</a></li>
                                    <li class="detalles"><a id="recibos"><i> </i>Recibos</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div id="error" style="display:none" class="alert alert-danger alert-dismissible fade in" role="alert"></div>
                    <div id="success" style="display:none" class="alert alert-success alert-dismissible"></div>
                     <div class="table-responsive" id="tabla_contenido">

                </div>
                    </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

$("#volver").click(function(){
        window.history.go(-1); return true;
    });



$(function() {
    $("#motivos")[0].click();
});

 $(document).on('focusin','.datepicker',function(){
         $(this).datepicker({
        format: "dd/mm/yyyy",
        dateFormat: 'yy-mm-dd',
        language: "es",
        autoclose: true
    });

         $(this).selectpicker("data-live-search","true");

    });

   $('#motivos').click(function(){
        $("#success").hide();
        $("#error").hide();
        //alert("reducciones");
        $ID = $("#IDintervencion").val();
        setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('intervenciones/motivos')}}',
                data : {'id':$ID},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('#tabla_contenido').html(data);
                    $('.selectpicker').selectpicker('refresh');
                    $('.selectpicker').selectpicker('render');
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

    });

   $('#tramites').click(function(){
         $("#success").hide();
        $("#error").hide();
        //alert("reducciones");
        $ID = $("#IDintervencion").val();
        setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('intervenciones/tramites')}}',
                data : {'id':$ID},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('#tabla_contenido').html(data);
                    $('.selectpicker').selectpicker('refresh');
                    $('.selectpicker').selectpicker('render');
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

    });

    $('#recibos').click(function(){
         $("#success").hide();
        $("#error").hide();
        //alert("reducciones");
        $ID = $("#IDintervencion").val();
        setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('intervenciones/recibos')}}',
                data : {'id':$ID},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('#tabla_contenido').html(data);
                    $('.selectpicker').selectpicker('refresh');
                    $('.selectpicker').selectpicker('render');
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

    });

     $("#guardar").click(function(){

    
    $ID = $("#IDintervencion").val();
    $form = $("#form_intervencion").serialize();
  
     setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '/intervenciones/'+$ID+'/update',
                data : {'datos' : $("#form_intervencion").serialize()},
                success : function(data){
                    console.log(JSON.stringify(data));
                   if(data.includes("SQLSTATE")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                //location.reload();
                            }
                        });
                    }else{
                         bootbox.alert({
                            message: data,
                            callback: function () {
                                 location.reload();
                            }
                        });
                    }

                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

});

$('#add_tramite').click(function(){

    $id = $("#IDintervencion").val();
    $acuerdo = $("#acuerdo").val();
 
       setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('intervenciones/add_tramite')}}',
                        data : {},
                        success : function(data){
                           var dialog = bootbox.dialog({
                                    message: data,
                                    title: "Añadir un trámite",
                                    size:'large',
                                    buttons: {
                                        success: {
                                            label: 'aceptar',
                                             callback: function() {
                                                $fecha = $("#fecha_del_tramite").val();
                                                $hora = $("#hora_tramite").val();
                                                $persona1 = $("#persona1 option:selected").val();
                                                $entidad1 = $("#entidad1 option:selected").val();
                                                $operacion = $("#operacion option:selected").val();
                                                $persona2 = $("#persona2 option:selected").val();
                                                $entidad2 = $("#entidad2 option:selected").val();
                                                $notas_tramite = $("#notas_tramite").val();
                                                $ocultar = 0;
                                                if($("#ocultar").prop("checked")){
                                                    $ocultar = 1;
                                                }

                                                insertar_tramite($fecha,$hora,$persona1,$entidad1,$operacion,$persona2,$entidad2,$acuerdo,$id,$ocultar,$notas_tramite);
                                                dialog.modal('hide');
                                                 }
                                        }
                                    }
                                });
                             $('.selectpicker').selectpicker('refresh');
                             $('.selectpicker').selectpicker('render');
                             
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);

    });

  function insertar_tramite(fecha,hora,persona1,entidad1,operacion,persona2,entidad2,acuerdo,id,ocultar,notas_tramite){

      setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('intervenciones/insertar_tramite')}}',
                        data : {'fecha':fecha,'hora':hora,'persona1':persona1,'entidad1':entidad1,'operacion':operacion,'persona2':persona2,'entidad2':entidad2,'acuerdo':acuerdo,'id':id,'ocultar':ocultar,'notas_tramite':notas_tramite},
                        success : function(data){
                            console.log(JSON.stringify(data));
                    if(data.includes("SQLSTATE")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                //location.reload();
                            }
                        });
                    }else{
                         bootbox.alert({
                            message: data,
                            callback: function () {
                                 location.reload();
                            }
                        });
                    }
                   
                    $("#loading").hide();
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);

  }


     $(document).on('click','.editar_motivo',function(){

        $caucion = 0;    
        if($(this).closest("tr").find('.caucion').prop("checked")){
            $caucion = 1;
        }

        $estado = $(this).closest("tr").find('.estado option:selected').val();
        $motivo = $(this).closest("tr").find('.motivo option:selected').val();
        $importe = $(this).closest("tr").find('.importe').val();
        $divisa = $(this).closest("tr").find('.divisa').val();
        $cantidad = $(this).closest("tr").find('.cantidad').val();
        $numTarjeta = $(this).closest("tr").find('.numTarjeta').val();
        $asistido = $(this).closest("tr").find('.asistido').val();
        $detalles = $(this).closest("tr").find('.detalles').val();

        $ID = $(this).closest("tr").find('.idmotivo').val();
        $acuerdo = $("#acuerdo").val();
        setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('intervenciones/editar_motivo')}}',
                data : {'id':$ID,'estado':$estado,'motivo':$motivo,'importe':$importe,'divisa':$divisa,'cantidad':$cantidad,'numTarjeta':$numTarjeta,'asistido':$asistido,'detalles':$detalles,'acuerdo':$acuerdo,'caucion':$caucion},            
                success : function(data){
                    if(data.includes("SQLSTATE")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                //location.reload();
                            }
                        });
                    }else{
                         bootbox.alert({
                            message: data,
                            callback: function () {
                                 location.reload();
                            }
                        });
                    }
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

    });

      $(document).on('click','.add_motivo',function(){

        $caucion = 0;    
        if($(this).closest("tr").find('.caucion').prop("checked")){
            $caucion = 1;
        }

        $estado = $(this).closest("tr").find('.estado option:selected').val();
        $motivo = $(this).closest("tr").find('.motivo option:selected').val();
        $importe = $(this).closest("tr").find('.importe').val();
        $divisa = $(this).closest("tr").find('.divisa').val();
        $cantidad = $(this).closest("tr").find('.cantidad').val();
        $numTarjeta = $(this).closest("tr").find('.numTarjeta').val();
        $asistido = $(this).closest("tr").find('.asistido').val();
        $detalles = $(this).closest("tr").find('.detalles').val();
        $acuerdo = $("#acuerdo").val();
        setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('intervenciones/add_motivo')}}',
                data : {'estado':$estado,'motivo':$motivo,'importe':$importe,'divisa':$divisa,'cantidad':$cantidad,'numTarjeta':$numTarjeta,'asistido':$asistido,'detalles':$detalles,'acuerdo':$acuerdo,'caucion':$caucion},            
                success : function(data){
                    if(data.includes("SQLSTATE")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                //location.reload();
                            }
                        });
                    }else{
                         bootbox.alert({
                            message: data,
                            callback: function () {
                                 location.reload();
                            }
                        });
                    }
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

    });

       $(document).on('click','.eliminar_motivo',function(){

        $ID = $(this).closest("tr").find('.idmotivo').val();
        bootbox.confirm({
    title: "Eliminar motivo de multa",
    message: "¿Desea realmente eliminar el registro? Tenga en cuenta que el proceso es irreversible.",
    buttons: {
        cancel: {
            label: '<i class="fa fa-times"></i> Cancelar'
        },
        confirm: {
            label: '<i class="fa fa-check"></i> Aceptar'
        }
    },
    callback: function (result) {
       if(result){
         setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('intervenciones/eliminar_motivo')}}',
                data : {'id':$ID},            
                success : function(data){
                    if(data.includes("SQLSTATE")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                //location.reload();
                            }
                        });
                    }else{
                         bootbox.alert({
                            message: data,
                            callback: function () {
                                 location.reload();
                            }
                        });
                    }
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);
       }
    }
});
       

    });

         $(document).on('click','.editar_recibo',function(){


        $vencimiento = $(this).closest("tr").find('.vencimiento').val();
        $correspondePagoCte = $(this).closest("tr").find('.correspondePagoCte').val();
        $importe = $(this).closest("tr").find('.importe').val();
        $forma_pago = $(this).closest("tr").find('.forma_pago option:selected').val();
        $estados_recibo = $(this).closest("tr").find('.estados_recibo option:selected').val();
        $notas = $(this).closest("tr").find('.notas').val();

        $codrecibo = $(this).closest("tr").find('.codrecibo').val();
        $acuerdo = $("#acuerdo").val();
        setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('intervenciones/editar_recibo')}}',
                data : {'vencimiento':$vencimiento,'correspondePagoCte':$correspondePagoCte,'importe':$importe,'forma_pago':$forma_pago,'estados_recibo':$estados_recibo,'notas':$notas,'codrecibo':$codrecibo,'acuerdo':$acuerdo},            
                success : function(data){
                    if(data.includes("SQLSTATE")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                //location.reload();
                            }
                        });
                    }else{
                         bootbox.alert({
                            message: data,
                            callback: function () {
                                 location.reload();
                            }
                        });
                    }
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

    });

         $(document).on('click','#add_recibo',function(){


        $vencimiento = $(this).closest("tr").find('.vencimiento').val();
        $correspondePagoCte = $(this).closest("tr").find('.correspondePagoCte').val();
        $importe = $(this).closest("tr").find('.importe').val();
        $forma_pago = $(this).closest("tr").find('.forma_pago option:selected').val();
        $estados_recibo = $(this).closest("tr").find('.estados_recibo option:selected').val();
        $notas = $(this).closest("tr").find('.notas').val();
        $acuerdo = $("#acuerdo").val();
        setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('intervenciones/add_recibo')}}',
                data : {'vencimiento':$vencimiento,'correspondePagoCte':$correspondePagoCte,'importe':$importe,'forma_pago':$forma_pago,'estados_recibo':$estados_recibo,'notas':$notas,'acuerdo':$acuerdo},            
                success : function(data){
                    if(data.includes("SQLSTATE")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                //location.reload();
                            }
                        });
                    }else{
                         bootbox.alert({
                            message: data,
                            callback: function () {
                                 location.reload();
                            }
                        });
                    }
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

    });

         $(document).on('click','.eliminar_recibo',function(){

        $codrecibo = $(this).closest("tr").find('.codrecibo').val();
        bootbox.confirm({
    title: "Eliminar motivo de multa",
    message: "¿Desea realmente eliminar el registro? Tenga en cuenta que el proceso es irreversible.",
    buttons: {
        cancel: {
            label: '<i class="fa fa-times"></i> Cancelar'
        },
        confirm: {
            label: '<i class="fa fa-check"></i> Aceptar'
        }
    },
    callback: function (result) {
       if(result){
         setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('intervenciones/eliminar_recibo')}}',
                data : {'codrecibo':$codrecibo},            
                success : function(data){
                    if(data.includes("SQLSTATE")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                //location.reload();
                            }
                        });
                    }else{
                         bootbox.alert({
                            message: data,
                            callback: function () {
                                 location.reload();
                            }
                        });
                    }
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);
       }
    }
});
       

    });

          $("#imprimir_recibos").click(function(){

     $acuerdo = $("#acuerdo").val();

      setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('intervenciones/imprimir_recibos')}}',
                        data : {'acuerdo':$acuerdo},
                        success : function(data){
                        if(data==1){
                          bootbox.alert("No existe ningún recibo para imprimir en este contrato")
                        }else{
                           window.location = '/intervenciones/impresion_recibos?acuerdo='+$acuerdo;
                        }
                             
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);

  });


$("#parte_multa").click(function(){
    $acuerdo = $("#acuerdo").val();
    $intervencion = $("#IDintervencion").val();
      setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('intervenciones/mostrar_parte_multa')}}',
                        data : {},
                        success : function(data){
                           var dialog = bootbox.dialog({
                                    message: data,
                                    title: "Parte de multa",
                                    buttons: {
                                        success: {
                                            label: 'generar',
                                             callback: function() {
                                                $ejemplar = $("input[name=ejemplar]:checked").val();
                                                $texto = $("input[name=texto]:checked").val();
                                                $caucion = $("input[name=caucion]:checked").val();
                                                   window.location = '/intervenciones/imprimir_parte_multa?intervencion='+$intervencion+'&ejemplar='+$ejemplar+'&texto='+$texto+'&caucion='+$caucion;
                                                dialog.modal('hide');
                                                 }
                                        }
                                    }
                                });
                             $('.selectpicker').selectpicker('refresh');
                             $('.selectpicker').selectpicker('render');
                             
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);
});

$("#parte_cliente").click(function(){
    $acuerdo = $("#acuerdo").val();
    $intervencion = $("#IDintervencion").val();
      setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('intervenciones/mostrar_parte_cliente')}}',
                        data : {},
                        success : function(data){
                           var dialog = bootbox.dialog({
                                    message: data,
                                    title: "Parte Cliente",
                                    buttons: {
                                        success: {
                                            label: 'generar',
                                             callback: function() {
                                                $gastos = $("#gastos_dossier").val();
                                                $texto = $("#nota_adicional").val();
                                                   window.location = '/intervenciones/imprimir_parte_cliente?intervencion='+$intervencion+'&gastos='+$gastos+'&texto='+$texto;
                                                dialog.modal('hide');
                                                 }
                                        }
                                    }
                                });
                             $('.selectpicker').selectpicker('refresh');
                             $('.selectpicker').selectpicker('render');
                             
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);
});

$("#fax_france").click(function(){
    $acuerdo = $("#acuerdo").val();
    $intervencion = $("#IDintervencion").val();
      setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('intervenciones/mostrar_france_truck')}}',
                        data : {},
                        success : function(data){
                           var dialog = bootbox.dialog({
                                    message: data,
                                    title: "Fax France Truck",
                                    buttons: {
                                        success: {
                                            label: 'generar',
                                             callback: function() {
                                                $texto = $("input[name=texto]:checked").val();
                                                $gastos = $("#gastos_dossier").val();
                                                   window.location = '/intervenciones/imprimir_france_truck?intervencion='+$intervencion+'&texto='+$texto+'&gastos='+$gastos;
                                                dialog.modal('hide');
                                                 }
                                        }
                                    }
                                });
                             $('.selectpicker').selectpicker('refresh');
                             $('.selectpicker').selectpicker('render');
                             
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);
});

$("#parte_tramites").click(function(){
    $acuerdo = $("#acuerdo").val();
    $intervencion = $("#IDintervencion").val();
      setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('intervenciones/mostrar_parte_tramites')}}',
                        data : {},
                        success : function(data){
                           var dialog = bootbox.dialog({
                                    message: data,
                                    title: "Parte de trámites",
                                    buttons: {
                                        success: {
                                            label: 'generar',
                                             callback: function() {
                                                $oculto = $("input[name=oculto]:checked").val();
                                                   window.location = '/intervenciones/imprimir_parte_tramites?intervencion='+$intervencion+'&oculto='+$oculto;
                                                 }
                                        }
                                    }
                                });
                             $('.selectpicker').selectpicker('refresh');
                             $('.selectpicker').selectpicker('render');
                             
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);
});
</script>

@endsection