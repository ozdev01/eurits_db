@extends('layouts.app')
@section('content')

<div class="container">
    <div class="row">
        <div class="col-sm-2">
         
            <h4>Filtros</h4>
               <div class="filtros">
                 <a data-toggle="collapse" href="#collapsematricula" aria-expanded="false" aria-controls="collapsematricula"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Matricula</h5></a>
                <div class="collapse" id="collapsematricula">
                 <select class="selectpicker" data-live-search="true" title="Buscar..." id="matircula" name="matricula">
                 <option selected></option>
                    @foreach ($matriculas as $matricula)
                    <option>{{ $matricula->Matricula}}</option>
                    @endforeach
                </select>
                </div>
            </div>
                  <div class="filtros">
            <a data-toggle="collapse" href="#collapseacuerdo" aria-expanded="false" aria-controls="collapselugar"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Acuerdo</h5></a>
            <div class="collapse" id="collapseacuerdo">
            <input type="text" class="form-control" id="acuerdo" name="acuerdo" placeholder="Buscar...">
                </div>
            </div>
                 <div class="filtros">
                 <a data-toggle="collapse" href="#collapseimporte" aria-expanded="false" aria-controls="collapseimporte"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Importe</h5></a>
                 <div class="collapse" id="collapseimporte">
            <input type="text" class="form-control" id="importe" name="importe" placeholder="Buscar...">
            </div>
            </div>
            <div class =filtros>
                 <a data-toggle="collapse" href="#fechallegada" aria-expanded="false" aria-controls="fechallegada"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> fecha de Apertura</h5></a>
                <div class="collapse" id="fechallegada">
                <input type="text" class="form-control datepicker" name="fecha_apertura" id="fecha_apertura" value="" placeholder="fecha de inicio...">
                </div>
            </div>
            <div class="filtros">
                <a data-toggle="collapse" href="#fechamulta" aria-expanded="false" aria-controls="fechamulta"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> fecha de Cierre</h5></a>
                <div class="collapse" id="fechamulta">
                <input type="text" class="form-control datepicker" name="fecha_cierre" id="fecha_cierre" value="" placeholder="fecha de inicio...">
                </div>
            </div>
              <div class =filtros>
                 <a data-toggle="collapse" href="#collapseempresa" aria-expanded="false" aria-controls="collapseempresa"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> CIF</h5></a>
                <div class="collapse" id="collapseempresa">
                <select class="selectpicker" data-live-search="true" title="Buscar..." id="cif" name="cif">
                 <option selected></option>
                    @foreach ($clientes as $cliente)
                    <option value="{{$cliente->CODCLI}}">{{ $cliente->cif}}</option>
                    @endforeach
                </select>
                </div>
            </div>
              <div class =filtros>
                 <a data-toggle="collapse" href="#collapseempresa" aria-expanded="false" aria-controls="collapseempresa"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Cliente</h5></a>
                <div class="collapse" id="collapseempresa">
                <select class="selectpicker" data-live-search="true" title="Buscar..." id="empresa" name="empresa">
                 <option selected></option>
                    @foreach ($clientes as $cliente)
                    <option value="{{$cliente->CODCLI}}">{{ $cliente->empresa}}</option>
                    @endforeach
                </select>
                </div>
            </div>
                <div class="filtros">
                <a data-toggle="collapse" href="#collapsecontratante" aria-expanded="false" aria-controls="collapseconductor"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Empresa Comercial</h5></a>
                 <div class="collapse" id="collapsecontratante">
                 <select class="selectpicker" data-live-search="true" title="Buscar..." id="contratante" name="contratante">
                 <option selected></option>
                    @foreach ($contratantes as $contratante)
                    <option value="{{$contratante->Num_empresa}}">{{ $contratante->Empresa}}</option>
                    @endforeach
                </select>
                </div>
            </div>
               <div class="filtros">
                <a data-toggle="collapse" href="#collapseordenante" aria-expanded="false" aria-controls="collapseconductor"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Ordenante</h5></a>
                 <div class="collapse" id="collapseordenante">
                 <select class="selectpicker" data-live-search="true" title="Buscar..." id="ordenante" name="ordenante">
                 <option selected></option>
                    @foreach ($comerciales as $comercial)
                    <option value="{{$comercial->idcomercial}}">{{ $comercial->Nombre.' '.$comercial->Apellidos}}</option>
                    @endforeach
                </select>
                </div>
            </div>
            <div class="filtros">
                <a data-toggle="collapse" href="#collapseconductor" aria-expanded="false" aria-controls="collapseconductor"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Conductor</h5></a>
                 <div class="collapse" id="collapseconductor">
             <input type="text" class="form-control" id="conductor" name="conductor" placeholder="Buscar...">
                </div>
            </div>
           
            <div class="filtros">
            <a data-toggle="collapse" href="#collapselugar" aria-expanded="false" aria-controls="collapselugar"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Lugar</h5></a>
            <div class="collapse" id="collapselugar">
            <input type="text" class="form-control" id="lugar" name="lugar" placeholder="Buscar...">
                </div>
            </div>
              <div class="filtros">
            <a data-toggle="collapse" href="#collapseciudad" aria-expanded="false" aria-controls="collapselugar"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Ciudad</h5></a>
            <div class="collapse" id="collapseciudad">
            <input type="text" class="form-control" id="lugar" name="lugar" placeholder="Buscar...">
                </div>
            </div>
            <div class="filtros">
              <a data-toggle="collapse" href="#collapsepais" aria-expanded="false" aria-controls="collapsepais"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> País</h5></a>
              <div class="collapse" id="collapsepais">
              <select class="selectpicker" data-live-search="true" title="Buscar..." id="pais" name="pais">
                    @foreach ($paises as $pais)
                    <option value="{{$pais->codpais}}">{{ $pais->Pais}}</option>
                    @endforeach
                </select>
                </div>
            </div>
            <div class="filtros">
                <a data-toggle="collapse" href="#collapseautoridad" aria-expanded="false" aria-controls="collapseautoridad"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Autoridad</h5></a>
                <div class="collapse" id="collapseautoridad">
              <select class="selectpicker form-control" data-live-search="true" title="Buscar..." id="autoridad">
                 <option selected></option>
                    @foreach ($autoridades as $autoridad)
                    <option value="{{$autoridad->IDautoridad}}">{{ $autoridad->Autoridad}}</option>
                    @endforeach
                </select>
                </div>
            </div>
              <div class="filtros">
            <a data-toggle="collapse" href="#collapsetelefono" aria-expanded="false" aria-controls="collapselugar"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Teléfono</h5></a>
            <div class="collapse" id="collapsetelefono">
            <input type="number" class="form-control" id="telefono" name="telefono" placeholder="Buscar...">
                </div>
            </div>
              <div class="filtros">
            <a data-toggle="collapse" href="#collapsemovil" aria-expanded="false" aria-controls="collapselugar"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Móvil</h5></a>
            <div class="collapse" id="collapsemovil">
            <input type="number" class="form-control" id="movil" name="movil" placeholder="Buscar...">
                </div>
            </div>
              <div class="filtros">
            <a data-toggle="collapse" href="#collapsefax" aria-expanded="false" aria-controls="collapselugar"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Fax Autoridad</h5></a>
            <div class="collapse" id="collapsefax">
            <input type="number" class="form-control" id="fax" name="fax" placeholder="Buscar...">
                </div>
            </div>
              <div class="filtros">
            <a data-toggle="collapse" href="#collapseboletin" aria-expanded="false" aria-controls="collapselugar"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Boletín</h5></a>
            <div class="collapse" id="collapseboletin">
            <input type="text" class="form-control" id="boletin" name="boletin" placeholder="Buscar...">
                </div>
            </div>
                <div class="filtros">
            <a data-toggle="collapse" href="#collapsefacprov" aria-expanded="false" aria-controls="collapselugar"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Nº Factura Prov.</h5></a>
            <div class="collapse" id="collapsefacprov">
            <input type="text" class="form-control" id="facprov" name="facprov" placeholder="Buscar...">
                </div>
            </div>
             <div class="filtros">
            <a data-toggle="collapse" href="#collapsefacits" aria-expanded="false" aria-controls="collapselugar"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Nº Factura ITS.</h5></a>
            <div class="collapse" id="collapsefacits">
            <input type="text" class="form-control" id="facits" name="facits" placeholder="Buscar...">
                </div>
            </div>
             <div class="filtros">
            <a data-toggle="collapse" href="#collapseimpprov" aria-expanded="false" aria-controls="collapselugar"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Importe fac. Prov.</h5></a>
            <div class="collapse" id="collapseimpprov">
            <input type="text" class="form-control" id="importeprov" name="importeprov" placeholder="Buscar...">
                </div>
            </div>
             <div class="filtros">
            <a data-toggle="collapse" href="#collapseimpits" aria-expanded="false" aria-controls="collapselugar"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Importe fac. ITS</h5></a>
            <div class="collapse" id="collapseimpits">
            <input type="text" class="form-control" id="importeits" name="importeits" placeholder="Buscar...">
                </div>
            </div>
             <div class="filtros">
            <a data-toggle="collapse" href="#collapsegastos" aria-expanded="false" aria-controls="collapselugar"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Gastos</h5></a>
            <div class="collapse" id="collapsegastos">
            <input type="text" class="form-control" id="gastos" name="gastos" placeholder="Buscar...">
                </div>
            </div>
                        <div class="filtros">
                <a data-toggle="collapse" href="#collapseremision" aria-expanded="false" aria-controls="fechamulta"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> fecha de Remisión</h5></a>
                <div class="collapse" id="collapseremision">
                <input type="text" class="form-control datepicker" name="remision_inicio" id="remision_inicio" value="" placeholder="fecha de inicio...">
                </div>
            </div>
                 <div class="filtros">
            <a data-toggle="collapse" href="#collapsedossier" aria-expanded="false" aria-controls="collapselugar"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Num Dossier</h5></a>
            <div class="collapse" id="collapsedossier">
            <input type="text" class="form-control" id="dossier" name="dossier" placeholder="Buscar...">
                </div>
            </div>
            <div class="filtros">
            <a data-toggle="collapse" href="#collapsenotas" aria-expanded="false" aria-controls="collapselugar"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Notas</h5></a>
            <div class="collapse" id="collapsenotas">
            <input type="text" class="form-control" id="notas" name="notas" placeholder="Buscar...">
                </div>
            </div>
            <div class="filtros">
            <a data-toggle="collapse" href="#collapseautoriza" aria-expanded="false" aria-controls="collapselugar"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Comercial Autoriza</h5></a>
            <div class="collapse" id="collapseautoriza">
            <input type="checkbox"  id="comercialautoriza" name="comercialautoriza" placeholder="Buscar...">
                </div>
            </div>
            <div class="filtros">
            <a data-toggle="collapse" href="#collapseanulada" aria-expanded="false" aria-controls="collapselugar"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Anulada Intervención</h5></a>
            <div class="collapse" id="collapseanulada">
            <input type="checkbox"  id="anulada" name="anulada" placeholder="Buscar...">
                </div>
            </div>
            <div class="filtros">
            <a data-toggle="collapse" href="#collapsefc" aria-expanded="false" aria-controls="collapselugar"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> F.C.</h5></a>
            <div class="collapse" id="collapsefc">
            <input type="checkbox" class="form-control" id="fc" name="fc" placeholder="Buscar...">
                </div>
            </div>
             <h4>Motivos de Multa</h4>
              <div class="filtros">
              <a data-toggle="collapse" href="#collapsemotivo" aria-expanded="false" aria-controls="collapsepais"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Código Motivo</h5></a>
              <div class="collapse" id="collapsemotivo">
              <select class="selectpicker" data-live-search="true" title="Buscar..." id="motivo" name="motivo">
                 <option selected></option>
                    @foreach ($motivos as $motivo)
                    <option value="{{$motivo->COD_MOTIVO}}">{{ $motivo->COD_MOTIVO.'-'.$motivo->MOTIVO}}</option>
                    @endforeach
                </select>
                </div>
            </div>
               <div class="filtros">
              <a data-toggle="collapse" href="#collapseestado" aria-expanded="false" aria-controls="collapsepais"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Estado</h5></a>
              <div class="collapse" id="collapseestado">
              <select class="selectpicker" data-live-search="true" title="Buscar..." id="estado" name="estado">
                 <option value="">En Blanco</option>
                 <option value="S">Sobreseida</option>
                 <option value="R">Reducida</option>
                 <option value="A">Anulada</option>
                 <option value="P">Pagada</option>
                 <option value="C">Caducada</option>
                </select>
                </div>
            </div>
              <div class="filtros">
            <a data-toggle="collapse" href="#collapseimportemotivo" aria-expanded="false" aria-controls="collapselugar"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Importe</h5></a>
            <div class="collapse" id="collapseimportemotivo">
            <input type="text" class="form-control" id="importe_motivo" name="importe_motivo" placeholder="Buscar...">
                </div>
            </div>
              <div class="filtros">
            <a data-toggle="collapse" href="#collapsecheque" aria-expanded="false" aria-controls="collapselugar"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Cheque</h5></a>
            <div class="collapse" id="collapsecheque">
            <input type="text" class="form-control" id="cheque" name="cheque" placeholder="Buscar...">
                </div>
            </div>
            <div class="filtros">
              <a data-toggle="collapse" href="#collapseasistido" aria-expanded="false" aria-controls="collapsepais"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Asistido Por</h5></a>
              <div class="collapse" id="collapseasistido">
              <select class="selectpicker" data-live-search="true" title="Buscar..." id="asistido" name="asistido">
                <option value="">EUROPA ITS CAJERO</option>
                 <option value="S">EUROPA ITS TPV</option>
                 <option value="R">ITS2002</option>
                 <option value="A">MERCURIO CAJERO</option>
                 <option value="P">MERCURIO TPV</option>
                 <option value="C">SYLVIE</option>
                 <option value="C">TAI</option>
                </select>
                </div>
            </div>
              <div class="filtros">
            <a data-toggle="collapse" href="#collapsedetalles" aria-expanded="false" aria-controls="collapselugar"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Detalles</h5></a>
            <div class="collapse" id="collapsedetalles">
            <input type="text" class="form-control" id="detalles" name="detalles" placeholder="Buscar...">
                </div>
            </div>
              <div class="filtros">
            <a data-toggle="collapse" href="#collapsetarjeta" aria-expanded="false" aria-controls="collapselugar"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Tarjeta</h5></a>
            <div class="collapse" id="collapsetarjeta">
            <input type="text" class="form-control" id="tarjeta" name="tarjeta" placeholder="Buscar...">
                </div>
            </div>
             <div class="filtros">
            <a data-toggle="collapse" href="#collapsecaucion" aria-expanded="false" aria-controls="collapselugar"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Caución Pagada</h5></a>
            <div class="collapse" id="collapsecaucion">
            <input type="checkbox"  id="caucionpagada" name="caucionpagada" placeholder="Buscar...">
                </div>
            </div>
            <h4>Trámites</h4>
            <div class =filtros>
                 <a data-toggle="collapse" href="#collapsefechatramite" aria-expanded="false" aria-controls="fechallegada"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Fecha del trámite</h5></a>
                <div class="collapse" id="collapsefechatramite">
                <input type="text" class="form-control datepicker" name="fecha_tramite" id="fecha_tramite" value="" placeholder="fecha de trámite...">
                </div>
            </div>
            <div class =filtros>
                 <a data-toggle="collapse" href="#collapsehoratrasmite" aria-expanded="false" aria-controls="fechallegada"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Hora del trámite</h5></a>
                <div class="collapse" id="collapsehoratrasmite">
                <input type="text" class="form-control" name="hora_tramite" id="hora_tramite" value="" placeholder="hora de trámite...">
                </div>
            </div>
               <div class="filtros">
              <a data-toggle="collapse" href="#collapsepersona1" aria-expanded="false" aria-controls="collapsepais"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Persona 1</h5></a>
              <div class="collapse" id="collapsepersona1">
              <select class="selectpicker" data-live-search="true" title="Buscar..." id="persona1" name="persona1">
                 <option selected></option>
                    @foreach ($actores as $actor)
                    <option value="{{$actor->IdActor}}">{{ $actor->Actor}}</option>
                    @endforeach
                </select>
                </div>
            </div>
               <div class="filtros">
              <a data-toggle="collapse" href="#collapsepersona2" aria-expanded="false" aria-controls="collapsepais"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Persona 2</h5></a>
              <div class="collapse" id="collapsepersona2">
              <select class="selectpicker" data-live-search="true" title="Buscar..." id="persona2" name="persona2">
                 <option selected></option>
                    @foreach ($actores as $actor)
                    <option value="{{$actor->IdActor}}">{{ $actor->Actor}}</option>
                    @endforeach
                </select>
                </div>
            </div>
               <div class="filtros">
              <a data-toggle="collapse" href="#collapseentidad1" aria-expanded="false" aria-controls="collapsepais"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Entidad 1</h5></a>
              <div class="collapse" id="collapseentidad1">
              <select class="selectpicker" data-live-search="true" title="Buscar..." id="entidad1" name="entidad1">
                 <option selected></option>
                    @foreach ($entidades as $entidad)
                    <option value="{{$entidad->IdNombre}}">{{ $entidad->Nombre}}</option>
                    @endforeach
                </select>
                </div>
            </div>
               <div class="filtros">
              <a data-toggle="collapse" href="#collapseentidad2" aria-expanded="false" aria-controls="collapsepais"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Entidad 2</h5></a>
              <div class="collapse" id="collapseentidad2">
              <select class="selectpicker" data-live-search="true" title="Buscar..." id="entidad2" name="entidad2">
                 <option selected></option>
                    @foreach ($entidades as $entidad)
                    <option value="{{$entidad->IdNombre}}">{{ $entidad->Nombre}}</option>
                    @endforeach
                </select>
                </div>
            </div>
               <div class="filtros">
              <a data-toggle="collapse" href="#collapseoperacion" aria-expanded="false" aria-controls="collapsepais"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Operación</h5></a>
              <div class="collapse" id="collapseoperacion">
              <select class="selectpicker" data-live-search="true" title="Buscar..." id="operacion" name="operacion">
                 <option selected></option>
                    @foreach ($operaciones as $operacion)
                    <option value="{{$operacion->IdOperacion}}">{{ $operacion->Operacion}}</option>
                    @endforeach
                </select>
                </div>
            </div>
             <div class="filtros">
              <a data-toggle="collapse" href="#collapsetextotramite" aria-expanded="false" aria-controls="collapsepais"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Texto del trámite</h5></a>
              <div class="collapse" id="collapsetextotramite">
                   <input type="text" class="form-control" name="texto_tramite" id="texto_tramite" value="" placeholder="hora de trámite...">
                </div>
            </div>
            <div class="filtros">
            <button type="button" id="filtrar" class="btn btn-default btn-primary">filtrar</button>
            </div>
        </div>
        <div class="col-sm-10">
            <div class="panel panel-default">
                <div class="row"> 
                    <div class="panel-heading" id="sanciones-header">                    
                        <div class="col-md-10">
                            <h3 class="module-title">Intervenciones</h3>
                        </div>
                      
                    </div>
                </div>
                <div class="row"> 
                    <div class="panel-heading" id="sanciones-header">                    
                        <div class="col-md-10">
                            <div>
                                <ul class="list-inline">
                                    <li><a href="{{url('intervenciones/nuevo')}}"><i class="fa fa-plus"> </i> Añadir</a></li>
                                    <li><a href="#" id="excel"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Excel</a></li>
                                </ul>
                            </div>
                        </div>
                        </div>
                    </div>
                <div class="table-responsive"">
                    <table class="table">
                        <thead class="table-header">
                            <th>Acuerdo</th>                        
                            <th>Matricula</th>
                            <th>Fecha</th>
                            <th>Dossier</th>
                            <th>Cliente</th>
                            <th>Pais</th>
                            <th></th>
                        </thead>
                        <tbody id="myTable">
                        @foreach ($intervenciones as $intervencion)
                        <tr>
                            <td>{{ $intervencion->ACUERDO }} </td>
                            <td>{{ $intervencion->MATRICULA }} </td>
                            <td>{{ date('d - m - Y', strtotime($intervencion->FECHA)) }} </td>
                            <td>{{ $intervencion->NumDossier }} </td>                            
                            <td>{{ $intervencion->CLIENTE }} </td>
                            <td>{{ $intervencion->Pais }} </td>
                            <td><a class="btn btn-sm btn-success" href="{!! url('intervenciones/'.$intervencion->ID) !!}"><i class="fa fa-edit"></i> </a> <!--<a class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>--></td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    
                </div>

                 <div class="col-md-12 text-center">
      <ul class="pagination pagination-lg pager" id="myPager"></ul>
      </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

 $('.datepicker').datepicker({
        format: "dd/mm/yyyy",
        dateFormat: 'yy-mm-dd',
        language: "es",
        autoclose: true
    });
    
    $('#search').on('keyup',function(){
        $value=$(this).val();
        //alert($value);
        $("#loading").show();
        setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('sanciones/search')}}',
                data : {'search':$value},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('tbody').html(data);
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);
});

$( "#filtrar" ).click(function() {
//Intervencion
$matricula = $("#matricula option:selected").val();
$acuerdo = $("#acuerdo").val();
$importe = $("#importe").val();
$apertura = $("#fecha_apertura").val();
$cierre = $("#fecha_cierre").val()
$cif = $("#cif option:selected").val();
$empresa = $("#empresa option:selected").val();
$contratante = $("#contratante option:selected").val();
$ordenante = $("#ordenante option:selected").val();
$conductor = $("#conductor").val();
$lugar = $("#lugar").val();
$ciudad = $("#ciudad").val();
$pais = $("#pais option:selected").val();
$autoridad = $("#autoridad option:selected").val();
$telefono = $("#telefono").val();
$movil = $("#movil").val();
$fax = $("#fax").val();
$facprov = $("#facprov").val();
$facits = $("#facits").val();
$importeprov = $("#importeprov").val();
$importeits = $("#importeits").val();
$gastos = $("#gastos").val();
$remision = $("#remision_inicio").val();
$dossier = $("#dossier").val();
$notas = $("#notas").val();

$comercialautoriza = 0;
$anulada = 0;
$fc = 0;

$fecha_apertura = $apertura.split("/").reverse().join("-");
$fecha_cierre = $cierre.split("/").reverse().join("-");
$remision_inicio = $remision.split("/").reverse().join("-");

if($("#comercialautoriza").prop("checked")){
  $comercialautoriza = 1;
}
if($("#anulada").prop("checked")){
  $anulada = 1;
}
if($("#fc").prop("checked")){
  $fc = 1;
}


//motivos
$motivo = $("#motivo option:selected").val();
$estado = $("#estado option:selected").val();
$importe_motivo = $("#importe_motivo").val();
$cheque = $("#cheque").val();
$detalles = $("#detalles").val();
$tarjeta = $("#tarjeta").val();

$caucionpagada = 0;

if($("#caucionpagada").prop("checked")){
  $caucionpagada = 1;
}

//tramites
$fecha_t = $("#fecha_tramite").val();
$hora_tramite = $("#hora_tramite").val();
$persona1 = $("#persona1").val();
$persona2 = $("#persona2").val();
$entidad1 = $("#entidad1").val();
$entidad2 = $("#entidad2").val();
$operacion = $("#operacion option:selected").val();
$texto_tramite = $("#texto_tramite").val();

$fecha_tramite = $fecha_t.split("/").reverse().join("-");

//alert($inicio);

        $dialog = bootbox.dialog({
                        message: '<p class="text-center">Cargando datos, espere por favor...</p>',
                        closeButton: false
                    });

    setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('intervenciones/filtrar')}}',
                data : {'matricula':$matricula,'acuerdo':$acuerdo,'importe':$importe,'fecha_apertura':$fecha_apertura,'fecha_cierre':$fecha_cierre,'cif':$cif,'empresa':$empresa,'contratante':$contratante,'ordenante':$ordenante,'conductor':$conductor,'lugar':$lugar,'ciudad':$ciudad,'pais':$pais,'autoridad':$autoridad,'telefono':$telefono,'movil':$movil,'fax':$fax,'facprov':$facprov,'facits':$facits,'importeprov':$importeprov,'importeits':$importeits,'gastos':$gastos,'remision_inicio':$remision_inicio,'fax':$fax,'dossier':$dossier,'notas':$notas,'comercialautoriza':$comercialautoriza,'anulada':$anulada,'fc':$fc,'motivo':$motivo,'estado':$estado,'importe_motivo':$importe_motivo,'cheque':$cheque,'detalles':$detalles,'tarjeta':$tarjeta,'caucionpagada':$caucionpagada,'fecha_tramite':$fecha_tramite,'hora_tramite':$hora_tramite,'persona1':$persona1,'persona2':$persona2,'entidad1':$entidad1,'entidad2':$entidad2,'operacion':$operacion,'texto_tramite':$texto_tramite},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('tbody').html(data);

                    // do something in the background
                    $dialog.modal('hide');
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

});

$( "#excel" ).click(function() {
//Intervencion
$matricula = $("#matricula option:selected").val();
$acuerdo = $("#acuerdo").val();
$importe = $("#importe").val();
$apertura = $("#fecha_apertura").val();
$cierre = $("#fecha_cierre").val()
$cif = $("#cif option:selected").val();
$empresa = $("#empresa option:selected").val();
$contratante = $("#contratante option:selected").val();
$ordenante = $("#ordenante option:selected").val();
$conductor = $("#conductor").val();
$lugar = $("#lugar").val();
$ciudad = $("#ciudad").val();
$pais = $("#pais option:selected").val();
$autoridad = $("#autoridad option:selected").val();
$telefono = $("#telefono").val();
$movil = $("#movil").val();
$fax = $("#fax").val();
$facprov = $("#facprov").val();
$facits = $("#facits").val();
$importeprov = $("#importeprov").val();
$importeits = $("#importeits").val();
$gastos = $("#gastos").val();
$remision = $("#remision_inicio").val();
$dossier = $("#dossier").val();
$notas = $("#notas").val();

$comercialautoriza = 0;
$anulada = 0;
$fc = 0;

$fecha_apertura = $apertura.split("/").reverse().join("-");
$fecha_cierre = $cierre.split("/").reverse().join("-");
$remision_inicio = $remision.split("/").reverse().join("-");

if($("#comercialautoriza").prop("checked")){
  $comercialautoriza = 1;
}
if($("#anulada").prop("checked")){
  $anulada = 1;
}
if($("#fc").prop("checked")){
  $fc = 1;
}


//motivos
$motivo = $("#motivo option:selected").val();
$estado = $("#estado option:selected").val();
$importe_motivo = $("#importe_motivo").val();
$cheque = $("#cheque").val();
$detalles = $("#detalles").val();
$tarjeta = $("#tarjeta").val();

$caucionpagada = 0;

if($("#caucionpagada").prop("checked")){
  $caucionpagada = 1;
}

//tramites
$fecha_t = $("#fecha_tramite").val();
$hora_tramite = $("#hora_tramite").val();
$persona1 = $("#persona1").val();
$persona2 = $("#persona2").val();
$entidad1 = $("#entidad1").val();
$entidad2 = $("#entidad2").val();
$operacion = $("#operacion option:selected").val();
$texto_tramite = $("#texto_tramite").val();

$fecha_tramite = $fecha_t.split("/").reverse().join("-");

//alert($inicio);

        $dialog = bootbox.dialog({
                        message: '<p class="text-center">Cargando datos, espere por favor...</p>',
                        closeButton: false
                    });

    setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('intervenciones/exportar_excel')}}',
                data : {'matricula':$matricula,'acuerdo':$acuerdo,'importe':$importe,'fecha_apertura':$fecha_apertura,'fecha_cierre':$fecha_cierre,'cif':$cif,'empresa':$empresa,'contratante':$contratante,'ordenante':$ordenante,'conductor':$conductor,'lugar':$lugar,'ciudad':$ciudad,'pais':$pais,'autoridad':$autoridad,'telefono':$telefono,'movil':$movil,'fax':$fax,'facprov':$facprov,'facits':$facits,'importeprov':$importeprov,'importeits':$importeits,'gastos':$gastos,'remision_inicio':$remision_inicio,'fax':$fax,'dossier':$dossier,'notas':$notas,'comercialautoriza':$comercialautoriza,'anulada':$anulada,'fc':$fc,'motivo':$motivo,'estado':$estado,'importe_motivo':$importe_motivo,'cheque':$cheque,'detalles':$detalles,'tarjeta':$tarjeta,'caucionpagada':$caucionpagada,'fecha_tramite':$fecha_tramite,'hora_tramite':$hora_tramite,'persona1':$persona1,'persona2':$persona2,'entidad1':$entidad1,'entidad2':$entidad2,'operacion':$operacion,'texto_tramite':$texto_tramite},            
                   success : function(data){
                    window.location = this.url;
                    $dialog.modal('hide');
                },
                error : function(data){
                    //console.log(JSON.stringify(data));
                    bootbox.alert("Ha ocurrido un error generando su Excel.");
                    $dialog.modal('hide');
                }
            });
        }, 500);

});


</script>
@endsection