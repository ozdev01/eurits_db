@extends('layouts.app')
@section('content')

   
<div class="container">
    <div class="row">
        <div class="col-xl-12 col-md-offset-0">
            <div class="panel panel-default">
                <div class="row"> 
                    <div class="panel-heading" id="intervenciones-header">                    
                        <div class="col-md-6">
                            <h3 class="module-title">Nueva Intervención</h3>
                        </div>
                        <div class="col-md-6">
                            <div>
                                <ul class="nav navbar-nav">
                                    <li><a href="#" class="bt-edit" id="guardar"><i class="fa fa-save"> </i> Guardar</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="panel-body" style="border-top:2px solid #335599;">                        
                    <form class="inline-form" id="form_intervencion">
                    <div class="form-group col-md-12" id="aviso_contrato">
                     
                            <label style="color:blue; display: none" id='contrato_alta'></label>
                      
                            <label style="color:red; display: none" id='contrato_baja'></label>

                            <label style="color:red; display: none" id='no_contrato'">No existe ningún contrato asociado a esta matrícula</label>
                            <input type="checkbox" name="no_contratada" value="" style='display:none'>
                        <label></label>
                        </div>
                        <div class="form-group col-md-12" id="etiquetas">

                        <div class="form-group col-md-5">
                        <label id="pendiente" style="background:purple;color:white;display: none">Paraliz. Serv. Impago/Pte Devolución</label>
                        </div>
 
                 
                        <div class="form-group col-md-3">
                        <label id="conflictivo" style="background:blue;color:white;display: none">Cliente Conflictivo</label>
                        </div>
                    
                        <div class="form-group col-md-3">
                        <label id="impago_multas" style="background:#b40091;color:white;display: none">Impago Multas</label>
                        </div>
                   
                        </div>
                        <div class="form-group col-md-2">
                            <label for="expediente">Fecha Apertura</label>
                            <input type="text" class="form-control datepicker" name="fecha_apertura" value="">                         
                        </div>
                            <div class="form-group col-md-2">
                            <label for="expediente">Hora Apertura</label>
                            <input type="text" class="form-control" name="hora_apertura" value="">                         
                        </div>
                        <div class="form-group col-md-2">
                            <label for="expediente">Fecha Cierre</label>
                            <input type="text" class="form-control datepicker" name="fecha" value="">                         
                        </div>
                         <div class="form-group col-md-2">
                            <label for="expediente">Hora Cierre</label>
                            <input type="text" class="form-control" name="hora" value="">                         
                        </div>
                        <div class="form-group col-md-2">
                            <label for="expediente">Dossier Interno</label>
                            <input type="text" class="form-control" name="acuerdo" id="acuerdo" value="">                         
                        </div>
                        <div class="form-group col-md-2">
                            <label for="expediente">Dossier externo</label>
                            <input type="text" class="form-control" name="dossier" value="">                         
                        </div>
                        <div class="form-group col-md-2">
                            <label for="expediente">Matricula</label>
                            <input type="text" class="form-control" name="matricula" value="" id="matricula">                         
                        </div>
                        <div class="form-group col-md-2">
                            <label for="expediente">NIF/CIF</label>
                            <select class="selectpicker" data-live-search="true" title="Buscar..." id="cif" name="cif">
                             <option selected></option>
                                @foreach ($clientes as $cliente)
                                <optgroup label="{{$cliente->Empresa}}">
                                <option>{{ $cliente->CIF}}</option>
                                </optgroup>
                                @endforeach
                            </select>                         
                        </div>
                         <div class="form-group col-md-3">
                            <label for="expediente">Cliente</label>
                            <input type="text" class="form-control" name="cliente" id="cliente" value="">                         
                        </div>
                    
                          <div class="form-group col-md-3">
                            <label for="expediente">Empresa Comercial</label>
                           <select class="selectpicker" data-live-search="true" title="Buscar..." id="contratante" name="contratante">
                                    @foreach ($contratantes as $contratante)
                                    <option value="{{$contratante->Num_empresa}}">{{ $contratante->Empresa}}</option>
                                    @endforeach
                            </select>                           
                        </div>
                             <div class="form-group col-md-3">
                            <label for="expediente">Comercial Autorizante</label>
                           <select class="selectpicker" data-live-search="true" title="Buscar..." id="comercial" name="comercial">
                                    @foreach ($comerciales as $comercial)
                                     <option value="{{$comercial->idcomercial}}">{{ $comercial->Nombre.' '.$comercial->Apellidos}}</option>
                                    @endforeach
                            </select>                           
                        </div>
                         <div class="form-group col-md-3">
                            <label for="expediente">Ordenante</label>
                            <input type="text" class="form-control" name="ordenante" value="">      
                        </div>
                         <div class="form-group col-md-3">
                            <label for="expediente">Chófer</label>
                            <input type="text" class="form-control" name="conductor" value="">                         
                        </div>
                        <div class="form-group col-md-3">
                            <label for="expediente">Lugar</label>
                            <input type="text" class="form-control" name="lugar" value="">                         
                        </div>
                        <div class="form-group col-md-3">
                            <label for="expediente">Ciudad</label>
                            <input type="text" class="form-control" name="ciudad" value="">                         
                        </div>
                        <div class="form-group col-md-3">
                            <label for="expediente">País</label>
                            <select class="selectpicker" data-live-search="true" title="Buscar..." id="pais" name="pais">
                                    @foreach ($paises as $pais)
                                    <option value="{{$pais->codpais}}">{{ $pais->Pais}}</option>
                              
                                    @endforeach
                            </select>                          
                        </div>
                              
                          <div class="form-group col-md-3">
                            <label for="expediente">Autoridad</label>
                            <select class="selectpicker" data-live-search="true" title="Buscar..." id="autoridad" name="autoridad">
                                    @foreach ($autoridades as $autoridad)
                                     <option value="{{$autoridad->IDautoridad}}">{{ $autoridad->Autoridad}}</option>
                               
                                    @endforeach
                            </select>                          
                        </div>
                            <div class="form-group col-md-3">
                            <label for="expediente">Tel. Fijo</label>
                            <input type="text" class="form-control" name="telefono" value="">                         
                        </div>
                            <div class="form-group col-md-3">
                            <label for="expediente">Tel. Móvil</label>
                            <input type="text" class="form-control" name="asistido" value="">                         
                        </div>
                            <div class="form-group col-md-3">
                            <label for="expediente">Boletín</label>
                            <input type="text" class="form-control" name="boletin" value="">                         
                        </div>
                            <div class="form-group col-md-3">
                            <label for="expediente">Fax Autoridad</label>
                            <input type="text" class="form-control" name="fax" value="">                         
                        </div>
                        <div class="form-group col-md-2">
                            <label for="expediente">Fecha Remisión</label>
                            <input type="text" class="form-control datepicker" name="fecharemision" value="">                         
                        </div>
                        <div class="form-group col-md-6">
                        <label for="cif">Facturas</label>
                        <ul style="    border: 1px solid gray; border-radius: 4px; box-shadow: inset 0 1px 1px rgba(0,0,0,.075); padding: 20px;">
                        <ul class="list-inline" style="padding: 5px !important">
                        <li><label for="expediente">Factura Proveedor</label>
                            <input type="text" class="form-control" name="facturadoTAI" value=""></li>
                        <li><label for="expediente">Importe Proveedor</label>
                            <input type="text" class="form-control" name="ImporteFProv" value="">  </li>
                        </ul>
                        <ul class="list-inline" style="padding: 5px !important">
                        <li><label for="expediente">Factura ITS</label>
                            <input type="text" class="form-control" name="FacturaITS" value="">   </li>
                        <li><label for="expediente">Importe ITS</label>
                            <input type="text" class="form-control" name="ImporteFITS" value=""></li>
                        </ul>
                        </ul>
                        </div>
                        <!-- <div class="form-group col-md-3">
                            <label for="expediente">Importe</label>
                            <input type="number" class="form-control" name="importe" value="">      
                        </div>
                         <div class="form-group col-md-3">
                            <label for="expediente">Gastos</label>
                            <input type="number" class="form-control" name="gastos" value="">      
                        </div>-->
                          <div class="form-group col-md-5">
                            <label for="expediente">Notas</label>
                            <textarea name="notas" class="form-control" id="notas"></textarea>    
                        </div>

                        <div class="form-group col-md-2">
                            <label for="expediente">F.C.</label>
                            <input type="checkbox" name="fc" value="">                 
                        </div>
                           <div class="form-group col-md-2">
                            <label for="expediente">P.Q.</label>
                            <input type="checkbox" name="pq" value="">                   
                        </div>
                        <div class="form-group col-md-2">
                            <label for="expediente">No contratada</label>
                            <input type="checkbox" name="no_contratada" value="">                 
                        </div>
                        <div class="form-group col-md-2">
                            <label for="expediente">Comercial Autoriza</label>
                            <input type="checkbox" name="comercialautoriz" value="">                        
                        </div>
                        <div class="form-group col-md-12">
                       
                        <div class="form-group col-md-3">
                        <label id="aviso_aut" style="background:#006d1d;color:white;display: none">Aviso de autorización</label>
                        </div>
                      
                       
                        <div class="form-group col-md-3">
                        <label id="aviso_silvie" style="background:#b40091;color:white;display: none">Avisar a Silvie</label>
                        </div>
                  
                      
                        <div class="form-group col-md-3">
                        <label id="pedir_autorizacion" style="background:#0083c3;color:white;display: none">Pedir Autorización para CAUCIÓN</label>
                        </div>
                   
                     
                        <div class="form-group col-md-3">
                        <label id="importante" style="background:red;color:white;display: none">Cliente Importante</label>
                        </div>
                   
                        <div class="form-group col-md-3">
                        <label id="aviso_comercial" style="background:red;color:white;display: none">Avisar Comercial: en Período Renovación</label>
                        </div>
                   
                        <div class="form-group col-md-3">
                        <label id="informar_comercial" style="background:#000c67;color:white;display: none">Informar al comercial</label>
                        </div>
           
                        <div class="form-group col-md-3">
                        <label id="tarjetas_activas" style="background:#75a900;color:black;display: none">Cliente con tarjetas Activas</label>
                        </div>
                  
                        <div class="form-group col-md-3">
                        <label id="avisar_empresa" style="background:#870000;color:white;display: none">Avisar a empresa</label>
                        </div>
                      
                        <div class="form-group col-md-3">
                        <label id="cobertura_francia" style="background:#b40091;color:white;display: none">Cobertura Francia</label>
                        </div>
                 
                        </div>
                    </form>
                    </div>                        
                </div>
            </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

 $(document).on('focusin','.datepicker',function(){
         $(this).datepicker({
        format: "dd/mm/yyyy",
        dateFormat: 'yy-mm-dd',
        language: "es",
        autoclose: true
    });

         $(this).selectpicker("data-live-search","true");

    });

     $("#guardar").click(function(){

    
    $form = $("#form_intervencion").serialize();
  
     setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '/intervenciones/store',
                data : {'datos' : $("#form_intervencion").serialize()},
                success : function(data){
                    console.log(JSON.stringify(data));
                   if(data.includes("SQLSTATE")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                //location.reload();
                            }
                        });
                    }else{
                         bootbox.alert({
                            message: "La intervención se ha creado correctamente",
                            callback: function () {
                                 window.location = "/intervenciones/"+data;
                            }
                        });
                    }

                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

});

$(document).on('focusout','#matricula',function(){

    var dialog = bootbox.dialog({
    message: '<p class="text-center">Buscando matrícula...</p>',
    closeButton: false
});


    setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('intervenciones/check_contrato')}}',
                data : {'matricula':$("#matricula").val()},
                dataType: 'json',          //Added this            
                success : function(data){
                
               
                dialog.modal('hide');
                  if(data[0]!=undefined){
                    $('#contrato_alta').css('display', 'block');
                    $('#contrato_baja').css('display', 'none');
                    $('#no_contrato').css('display', 'none');
                    $('#no_contratada').css('display', 'none');
                     $('#aviso_silvie').css('display', 'none');
                    $('#contrato_alta').text(data[0]);
                  
                  }
                  else if(data[1]!=undefined){
                    $('#contrato_alta').css('display', 'none');
                    $('#contrato_baja').css('display', 'block');
                    $('#no_contrato').css('display', 'none');
                    $('#no_contratada').css('display', 'none');
                     $('#aviso_silvie').css('display', 'none');
                    $('#contrato_baja').text(data[1]);
                    //alert("aqui");
                 
                  }
                  else if(data[2]!=undefined){
                  $('#contrato_alta').css('display', 'none');
                    $('#contrato_baja').css('display', 'none');
                    $('#no_contrato').css('display', 'block');
                    $('#no_contratada').css('display', 'block');
                    $('#aviso_silvie').css('display', 'block');
                   $('#no_contrato').text(data[2]);
               
                  }
                  else{
                    $('#contrato_alta').css('display', 'none');
                    $('#contrato_baja').css('display', 'none');
                    $('#no_contrato').css('display', 'none');
                    $('#no_contratada').css('display', 'none');
                     $('#aviso_silvie').css('display', 'none');
                  }

                  if(data[3]!=undefined){
                    $('#cobertura_francia').css('display', 'block');
                  }else{
                    $('#cobertura_francia').css('display', 'none');
                  }
                  if(data[4]!=undefined){
                    $('#pedir_autorizacion').css('display', 'block');
                  }else{
                    $('#pedir_autorizacion').css('display', 'none');
                  }
                  if(data[5]!=undefined){
                    $('#aviso_comercial').css('display', 'block');
                  }else{
                    $('#aviso_comercial').css('display', 'none');
                  }
                  console.log(data);
                    
                },
                error : function(data){
                    
                    dialog.modal('hide');
                    bootbox.alert(data);
                }
            });
        }, 500);
});

$(document).on('change','#cif',function(){

    var dialog = bootbox.dialog({
    message: '<p class="text-center">Buscando cliente...</p>',
    closeButton: false
});

    setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('intervenciones/check_cliente')}}',
                data : {'CIF':$("#cif option:selected").val()},
                dataType: 'json',          //Added this            
                success : function(data){
                
               
                dialog.modal('hide');
                  if(data[0]==1){
                    $('#pendiente').css('display', 'block');
                  }else{
                    $('#pendiente').css('display', 'none');
                  }
                  if(data[1]==1){
                    $('#conflictivo').css('display', 'block');
                  }else{
                    $('#conflictivo').css('display', 'none');
                  }
                  if(data[2]==1){
                  $('#impago_multas').css('display', 'block');
                  }else{
                   $('#impago_multas').css('display', 'none'); 
                  }

                  if(data[3]!=undefined){
                    $('#notas').text(data[3]);
                  }else{
                    $('#notas').text("");
                  }

                   if(data[4]!=undefined){
                    $('#cliente').val(data[4]);
                  }else{
                    $('#cliente').val("");
                  }

                   if(data[5]==1){
                    $('#informar_comercial').css('display', 'block');
                  }else{
                    $('#informar_comercial').css('display', 'none');
                  }
                   if(data[6]==1){
                    $('#avisar_empresa').css('display', 'block');
                  }else{
                    $('#avisar_empresa').css('display', 'none');
                  }
                   if(data[7]==1){
                    $('#importante').css('display', 'block');
                  }else{
                    $('#importante').css('display', 'none');
                  }
                   if(data[8]==1){
                    $('#tarjetas_activas').css('display', 'block');
                  }else{
                    $('#tarjetas_activas').css('display', 'none');
                  }
               
                  console.log(data);
                    
                },
                error : function(data){
                    
                    dialog.modal('hide');
                    bootbox.alert(data);
                }
            });
        }, 500);
});

</script>

@endsection