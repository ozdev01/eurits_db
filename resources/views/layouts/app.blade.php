<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Europa ITS</title>

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">
    <link href="/css/custom.css" rel="stylesheet">
    <link href='//fonts.googleapis.com/css?family=Montserrat:400,300' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <!--<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.0/themes/smoothness/jquery-ui.css">-->

    <!-- Scripts -->
      <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
   <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <!--<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js" integrity="sha256-eGE6blurk5sHj+rmkfsGYeKyZx3M4bG+ZlFyA7Kns7E=" crossorigin="anonymous"></script>-->


    <!-- Datepicker Files -->
    <link rel="stylesheet" href="/css/bootstrap-datepicker3.css">
    <script src="/js/bootstrap-datepicker.js"></script>
    <!-- Languaje -->
    <script src="/local/bootstrap-datepicker.es.min.js"></script>
    

   
    



    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>

    
</head>
<body>
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                <img class="logo-nav" style="width: 150px !important" src="../../img/logo.png"/></a>
            </div>
            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                @if (Auth::guest())

                @else

                    <li>                    
                        <a href="{{ url('/sanciones') }}"><span class="fa fa-legal"></span> Sanciones</a>
                    </li>
                    <li>
                    <a class="dropdown-toggle" href="#" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="fa fa-file-text-o"></span> Facturación</a>
                       <ul class="dropdown-menu" role="menu">
                           <li><a class="dropdown-item" href="/facturas/europa">EUROPA</a></li>
                            <li><a class="dropdown-item" href="/facturas/its">ITS</a></li>
                        </ul>
                    </li>
                    <li>
                       <!-- <a href="/contratos"><span class="fa fa-certificate"></span> Contratos</a>-->
                       
                          <a class="dropdown-toggle" href="#" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="fa fa-certificate"></span> Contratos
                          </a>
                           <ul class="dropdown-menu" role="menu">
                           <li><a class="dropdown-item" href="/contratos/europa">Europa</a></li>
                            <li><a class="dropdown-item" href="/contratos/its">ITS</a></li>
                             <li><a class="dropdown-item" href="/contratos/gesanc">Gesanc</a></li>
                        </ul>
                      
                    </li>
                    <li>
                        <a href="/intervenciones"><span class="fa fa-life-buoy"></span> Intervenciones</a>
                    </li>
                @endif
                    
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <!--<li><a href="{{ url('/login') }}">Login</a></li>
                        <li><a href="{{ url('/register') }}">Registrarse</a></li>-->
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="{{ url('/logout') }}"
                                        onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                        Cerrar sesión
                                    </a>

                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>
    @yield('content')

    <!-- Scripts -->
    <script src="/js/app.js"></script>
        <link rel="stylesheet" href="/css/bootstrap-datepicker3.css">
    <script src="/js/bootstrap-datepicker.js"></script>
    <!-- Languaje -->
    <script src="/local/bootstrap-datepicker.es.min.js"></script>

     <!--Select -->
    <link rel="stylesheet" href="/css/bootstrap-select.css">
    <script src="/js/bootstrap-select.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js" type="text/javascript" charset="utf-8"></script>

</body>
</html>
