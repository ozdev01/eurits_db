@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xl-12 col-md-offset-0">
            <div class="panel panel-default">
                <div class="row"> 
                    <div class="panel-heading" id="sanciones-header">                    
                        <div class="col-md-8">
                            <h3 class="module-title">FCESCE</h3>
                        </div>
                        <div class="col-md-4">
                            <div>
                                <ul class="nav navbar-nav">                             
                                    <li><a href="#" id="volver"><i class="fa fa-arrow-left"> </i> Volver</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="panel-body" style="border-top:2px solid #335599;">                        
                    <form class="inline-form" id="form_sancion">
                        <!--<div class="form-group col-md-4">
                            <label for="empresa">Buscar por empresa</label>
                             <select class="selectpicker" data-live-search="true" title="Buscar..." id="empresa" name="empresa">
                 <option selected></option>
                    @foreach ($clientes as $cliente)
                    <option>{{ $cliente->empresa}}</option>
                    @endforeach
                </select>                          
                        </div>-->
                        <div class="form-group col-md-4">
                            <label for="matricula">Buscar por CIF</label>
                             <select class="selectpicker" data-live-search="true" title="Buscar..." id="cif" name="cif">
                                 <option selected></option>
                                    @foreach ($clientes as $cliente)
                                    <option>{{ $cliente->cif}}</option>
                                    @endforeach
                            </select>                            
                            </div>
                            <div class="form-group col-md-4">
                            <label for="matricula">Buscar por Cliente</label>
                             <select class="selectpicker" data-live-search="true" title="Buscar..." id="cliente" name="cliente">
                                 <option selected></option>
                                    @foreach ($clientes as $cliente)
                                    <option>{{ $cliente->empresa}}</option>
                                    @endforeach
                            </select>                            
                            </div>                      
                        </div>  
                    </form>
                       <div class="table-responsive" id="tabla_contenido" style="border-top: none;">
                    
                </div>
                <div id="tabla_contratos"></div>
                    </div>                        
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $("#volver").click(function(){
        window.history.go(-1); return false;
    });
     $(document).on('focusin','.datepicker',function(){
         $(this).datepicker({
        format: "dd/mm/yyyy",
        dateFormat: 'yy-mm-dd',
        language: "es",
        autoclose: true
    });

         $(this).selectpicker("data-live-search","true");

    });


    $( "#cif" ).change(function(){

    $cif = $(this).find("option:selected").text();

    setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('sanciones/buscar_contratos')}}',
                data : {'cif': $cif},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('#tabla_contenido').html(data);
                    $('.selectpicker').selectpicker('refresh');
                    $('.selectpicker').selectpicker('render');
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

});

        $( "#cliente" ).change(function(){

        $nombre_empresa = $(this).find("option:selected").text();



    setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('sanciones/buscar_contratos')}}',
                data : {'nombre_empresa': $nombre_empresa},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('#tabla_contenido').html(data);
                    $('.selectpicker').selectpicker('refresh');
                    $('.selectpicker').selectpicker('render');
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

});

         $(document).on('change','#contratos',function(){
         
         $fecha = $(this).find("option:selected").text();
         $FechaContrato= $fecha.split("/").reverse().join("-");
            $cif = $("#cif option:selected").text();

         setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('sanciones/cargar_contratos')}}',
                data : {'FechaContrato': $FechaContrato, 'cif': $cif},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('#tabla_contratos').html(data);
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);
        });


</script>
@endsection