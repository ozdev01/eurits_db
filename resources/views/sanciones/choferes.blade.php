@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xl-12 col-md-offset-0">
            <div class="panel panel-default">
                <div class="row"> 
                    <div class="panel-heading" id="sanciones-header">                    
                        <div class="col-md-8">
                            <h3 class="module-title">Chóferes</h3>
                        </div>
                        <div class="col-md-4">
                            <div>
                                <ul class="nav navbar-nav">                             
                                    <li><a href="#" id="volver"><i class="fa fa-arrow-left"> </i> Volver</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="panel-body" style="border-top:2px solid #335599;">                        
                    <form class="inline-form" id="form_sancion">
                        <!--<div class="form-group col-md-4">
                            <label for="empresa">Buscar por empresa</label>
                             <select class="selectpicker" data-live-search="true" title="Buscar..." id="empresa" name="empresa">
                 <option selected></option>
                    @foreach ($clientes as $cliente)
                    <option>{{ $cliente->empresa}}</option>
                    @endforeach
                </select>                          
                        </div>-->
                        <div class="form-group col-md-4">
                            <label for="matricula">Buscar por ITS</label>
                             <select class="selectpicker" data-live-search="true" title="Buscar..." id="its" name="its">
                                 <option selected></option>
                                    @foreach ($its as $conductor)
                                    <option>{{ $conductor->Chofer}}</option>
                                    @endforeach
                            </select>                            
                            </div>
                            <div class="form-group col-md-4">
                            <label for="matricula">Buscar por Indufisa</label>
                             <select class="selectpicker" data-live-search="true" title="Buscar..." id="indufisa" name="indufisa">
                                 <option selected></option>
                                    @foreach ($indufisa as $conductor)
                                    <option>{{ $conductor->Chofer}}</option>
                                    @endforeach
                            </select>                            
                            </div>                      
                        </div>  
                    </form>
                       <div class="table-responsive" id="tabla_contenido" style="border-top: none;">
                    
                </div>
                    </div>                        
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $("#volver").click(function(){
        window.history.go(-1); return false;
    });
     $(document).on('focusin','.datepicker',function(){
         $(this).datepicker({
        format: "dd/mm/yyyy",
        dateFormat: 'yy-mm-dd',
        language: "es",
        autoclose: true
    });

         $(this).selectpicker("data-live-search","true");

    });

     $( "#empresa" ).change(function(){

        $nombre_empresa = $(this).find("option:selected").text();

    setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('sanciones/buscar_choferes')}}',
                data : {'nombre_empresa': $nombre_empresa},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('#tabla_contenido').html(data);
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

});


 $( "#its" ).change(function(){

        $nombre_chofer = $(this).find("option:selected").text();
        $origen = "its";

    setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('sanciones/buscar_choferes')}}',
                data : {'nombre_chofer': $nombre_chofer,'origen':$origen},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('#tabla_contenido').html(data);
                    $('.selectpicker').selectpicker('refresh');
                    $('.selectpicker').selectpicker('render');
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

});

  $( "#indufisa" ).change(function(){

        $nombre_chofer = $(this).find("option:selected").text();
        $origen = "indufisa";

    setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('sanciones/buscar_choferes')}}',
                data : {'nombre_chofer': $nombre_chofer,'origen':$origen},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('#tabla_contenido').html(data);
                    $('.selectpicker').selectpicker('refresh');
                    $('.selectpicker').selectpicker('render');
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

});


     $(document).on('click','.btn-edit',function(){

    $DNI = $(this).closest("tr").find('.dni').val();
    $nombre_chofer = $(this).closest("tr").find('.nombre_chofer').val();
    $mandato = 0;
    $codchofer = $(this).closest("tr").find('#codchofer').val()
    $gestion= "editar";

    if($(this).closest("tr").find('.mandato').prop("checked")){
        $mandato = 1;
    }


    setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('sanciones/gestion_chofer')}}',
                data : {'nombre_chofer': $DNI,'nombre_chofer':$nombre_chofer,'mandato':$mandato,'dni':$DNI,'codchofer':$codchofer,'gestion':$gestion},            
                success : function(data){
                    console.log(JSON.stringify(data));
                     bootbox.alert({
                            message: data,
                            callback: function () {
                                
                            }
                        });
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);


     });

     $(document).on('click','.btn-delete',function(){

    $DNI = $(this).closest("tr").find('.dni').val();
    $nombre_chofer = $(this).closest("tr").find('.nombre_chofer').val();
    $mandato = 0;
    $codchofer = $(this).closest("tr").find('#codchofer').val()
    $gestion= "borrar";

    if($(this).closest("tr").find('.mandato').prop("checked")){
        $mandato = 1;
    }


    setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('sanciones/gestion_chofer')}}',
                data : {'codchofer':$codchofer,'gestion':$gestion},            
                success : function(data){
                    console.log(JSON.stringify(data));
                      bootbox.alert({
                            message: data,
                            callback: function () {
                                
                            }
                        });
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);


     });

       $(document).on('click','#add_chofer',function(){

    $codchofer = $("#chofer option:selected").val();

    $mandato = 0;
    $codigo_cliente = $("#codigo_cliente").val();
    alert($codigo_cliente);
    $gestion ="insertar";

    if($("#mandato").prop("checked")){
    $mandato = 1;
    }


    setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('sanciones/gestion_chofer')}}',
                data : {'codchofer':$codchofer,'mandato':$mandato,'codigo_cliente':$codigo_cliente,'gestion':$gestion},            
                success : function(data){
                    console.log(JSON.stringify(data));
                     bootbox.alert({
                            message: data,
                            callback: function () {
                                
                            }
                        });
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);


     });
</script>
@endsection