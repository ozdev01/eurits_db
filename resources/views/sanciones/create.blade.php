@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xl-12 col-md-offset-0">
            <div class="panel panel-default">
                <div class="row"> 
                    <div class="panel-heading" id="sanciones-header">                    
                        <div class="col-md-8">
                            <h3 class="module-title">Nueva Sanción</h3>
                        </div>
                        <div class="col-md-4">
                            <div>
                                <ul class="nav navbar-nav">
                                    <li><a href="#" id="btn-save"><i class="fa fa-save"> </i> Guardar</a></li>                                 

                                    <li><a href="#" id="volver"><i class="fa fa-arrow-left"> </i> Volver</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="panel-body" style="border-top:2px solid #335599;">                        
                    <form class="inline-form" id="form_sancion">
                         <div class="form-group col-md-2">
                            <label for="matricula">Matrícula</label>
                           <select class="selectpicker" data-live-search="true" title="Buscar..." id="matircula" name="matricula">
                             <option selected></option>
                                @foreach ($matriculas as $matricula)
                                <option>{{ $matricula->matricula}}</option>
                                @endforeach
                            </select>                    
                        </div>
                        <div class="form-group col-md-3">
                            <label for="empresa">Empresa</label>
                             <select class="selectpicker" data-live-search="true" title="Buscar..." id="empresa" name="empresa">
                 <option selected></option>
                    @foreach ($clientes as $cliente)
                    <option>{{ $cliente->empresa}}</option>
                    @endforeach
                </select>                          
                        </div>
                        <div class="form-group col-md-4">
                            <label for="expediente">Expdiente</label>
                            <input type="text" class="form-control" name="expediente" value="">                         
                        </div>
                        <div class="form-group col-md-2">
                            <label for="matricula">Boletín</label>
                            <input type="text" class="form-control" name="boletin" value="">                            
                        </div>
                        <div class="form-group col-md-3">
                            <label for="matricula">Conductor</label>
                             <select class="selectpicker" data-live-search="true" title="Buscar..." id="conductor" name="conductor">
                                 <option selected></option>
                                    @foreach ($conductores as $conductor)
                                    <option>{{ $conductor->conductor}}</option>
                                    @endforeach
                            </select>                            
                        </div>
                         <div class="form-group col-md-2">
                            <label for="acuerdo">Acuerdo</label>
                            <input type="text" class="form-control" name="acuerdo" value="">                            
                        </div>
                         <div class="form-group col-md-2">
                            <label for="ft">FT</label>
                            <input type="text" class="form-control" name="ft" value="">                            
                        </div>
                      <div class="form-group col-md-3">
                            <label for="matricula">Autoridad</label>
                            <select class="selectpicker form-control" data-live-search="true" title="Buscar..." id="autoridad" name="autoridad">
                 <option selected></option>
                    @foreach ($autoridades as $autoridad)
                    <option>{{ $autoridad->Autoridad}}</option>
                    @endforeach
                </select>                           
                        </div>
                        <div class="form-group col-md-2">
                            <label for="matricula">País</label>
                            <!--<input type="text" class="form-control" name="matricula" value="ESPAÑA">-->
                            <select class="selectpicker" data-live-search="true" title="Buscar..." id="pais" name="pais">
                            @foreach ($paises as $dato)
                             <option selected>{{$dato->Pais}}</option>
                            @endforeach
                                @foreach ($paises as $dato)
                                <option>{{ $dato->Pais}}</option>
                                @endforeach
                            </select> 
                        </div>
                        <div class="form-group col-md-6">
                            <label for="matricula">Lugar</label>
                            <input type="text" class="form-control" name="lugar" value="">                            
                        </div>
                        
                        <div class="form-group col-md-2">                      
                                <label for="matricula">Importe</label>
                                <div class="input-group"><span class="input-group-addon">€</span>
                                <input type="text" class="form-control" name="importe" value=""> </div>                  
                        </div>
                        <div class="form-group col-md-2">
                                <label for="divisa">Divisa</label>
                                
                                 <select class="selectpicker" data-live-search="true" title="Buscar..." id="divisas" name="divisas">
                                 <option selected></option>
                                    @foreach ($divisas as $divisa)
                                    <option>{{ $divisa->divisa}}</option>
                                    @endforeach
                            </select>                   
                        </div>
                        
                        <div class="form-group col-md-2">
                                <label for="matricula">Pagado ITS</label>
                                <div class="input-group"><span class="input-group-addon">€</span>
                                <input type="text" class="form-control" name="pagado" value=""></div>                   
                        </div>
                         <div class="form-group col-md-2">
                            <label for="matricula">Fecha Multa</label>
                            <input type="text" class="form-control datepicker" name="fecha_multa" value="">
                        </div>
                         <div class="form-group col-md-2">
                            <label for="matricula">Fecha Entrada</label>
                            <input type="text" class="form-control datepicker" name="fecha_entrada" value="">               
                        </div>
                         <div class="form-group col-md-2">
                            <label for="matricula">Fecha Archivado</label>
                            <input type="text" class="form-control datepicker" name="fecha_archivado" value="">                            
                        </div>                        
                        <div class="form-group col-md-2">
                            <label for="gestionadogure">Gestionado</label>      
                            <input type="checkbox" name="gestionadogure" value="">                                           
                        </div>
                         <div class="form-group col-md-2">
                            <label for="estado">Estado</label> 
                            <input type="checkbox" name="estado" id="estadosancion">
                            <label for="estado" id="inactivo" style="background: red; display:none;color:white;width:100px;padding:inherit">INACTIVO</label>                   
                            </div>
                         <div class="form-group col-md-2">
                            <label for="gestionadogure">Justif. Pago</label> 
                            <input type="checkbox" name="justif" id="justif">
                            <label for="estado" id="labeljustif" style="background: #f3f422; display:none;width:100px;padding:inherit">RECIBIDO</label>
                            <input type="text" class="form-control datepicker" name="fecha_justif" id="fecha_justif" style="display:none" value="">
                            </div>
                            <div class="form-group col-md-6">
                            <label for="matricula">Notas</label>
                            <textarea type="textarea" class="form-control" name="notas" style="" ></textarea> 
                            </div>                        
                        </div>     
                    </form>
                    </div>                        
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $("#volver").click(function(){
        window.history.go(-1); return false;
    });
     $(document).on('focusin','.datepicker',function(){
         $(this).datepicker({
        format: "dd/mm/yyyy",
        dateFormat: 'yy-mm-dd',
        language: "es",
        autoclose: true
    });

         $(this).selectpicker("data-live-search","true");

    });

    $("#justif").click(function(){

if($("#justif").is(':checked')){
    $("#labeljustif").css("display","");
    $("#fecha_justif").css("display","");
}else{
    $("#labeljustif").css("display","none");
    $("#fecha_justif").css("display","none");
}

});

 $("#estadosancion").click(function(){


if($("#estadosancion").is(':checked')){
    $("#inactivo").css("display","");
}else{
    $("#inactivo").css("display","none");
}

});


 $("#btn-save").click(function(){
    
    $form = $("#form_sancion").serialize();
  
     setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('sanciones/store')}}',
                data : {'datos' : $("#form_sancion").serialize()},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    if(data.includes("error")){
                        bootbox.alert({
                            message: data,
                            callback: function () {
                                //location.reload();
                            }
                        });
                    }else{
                         bootbox.alert({
                            message: data,
                            callback: function () {
                                 window.location = '/sanciones';
                            }
                        });
                    }
                   
                    $("#loading").hide();

                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);



});
</script>
@endsection