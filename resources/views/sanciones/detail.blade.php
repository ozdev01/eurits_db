@extends('layouts.app')
@section('content')

   
<div class="container">
    <div class="row">
        <div class="col-xl-12 col-md-offset-0">
            <div class="panel panel-default">
                <div class="row"> 
                    <div class="panel-heading" id="sanciones-header">                    
                        <div class="col-md-6">
                            <h3 class="module-title">Sanción {!! $sancion->IDSancion !!}</h3>
                            <input type="hidden" name="id" id="IDSancion" value="{!! $sancion->IDSancion !!}">
                        </div>
                        <div class="col-md-6">
                            <div>
                                <ul class="nav navbar-nav">
                                    <li><a href="#" class="bt-edit"><i class="fa fa-save"> </i> Guardar</a></li>
                                    <li><a href="#" class="bt-export"><i class="fa fa-print"> </i> Imprimir</a></li>
                                    <li><a href="#"><i class="fa fa-at"> </i> E-mail</a></li>
                                    <li><a href="#" id="eliminar"><i class="fa fa-trash"> </i> Eliminar</a></li>
                                    <li><a href="#" id="volver"><i class="fa fa-arrow-left"> </i> Volver</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="panel-body" style="border-top:2px solid #335599;">                        
                    <form class="inline-form" id="form_sancion">
                        <div class="form-group col-md-2">
                            <label for="matricula">Matrícula</label>
                            <input type="text" class="form-control" name="matricula" value="{!! $sancion->Matricula !!}">
                            <!--<select class="selectpicker" data-live-search="true" title="Buscar..." id="matircula" name="matricula">
                             <option selected>{!! $sancion->Matricula !!}</option>
                                @foreach ($matriculas as $matricula)
                                <option>{{ $matricula->matricula}}</option>
                                @endforeach
                            </select>-->                         
                        </div>
                        <div class="form-group col-md-2">
                            <label for="codigo">Código</label>
                            <input type="text" class="form-control" name="codigo" value="{!! $codigo !!}" disabled>
                            <input type="hidden" class="form-control" name="codigo" id="codigosancion" value="{!! $codigo !!}">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="empresa">Empresa</label>
                            <input type="text" id="empresa" class="form-control" name="empresa" value="{!! $cliente->empresa !!}">                            
                        </div>
                        <div class="form-group col-md-2">
                            <label for="cif">CIF</label>
                            <input type="text" id="cif" class="form-control" name="cif" value="{!! $cliente->cif !!}">                            
                        </div>
                         <div class="form-group col-md-2">
                            <label for="contrato">Contrato</label>
                            <input type="text" class="form-control" name="contrato" value="{!! $sancion->IDCONTRATO !!}" disabled>                            
                        </div>
                        <div class="form-group col-md-2">
                            <label for="expediente">Expdiente</label>
                            <input type="text" class="form-control" name="expediente" value="{!! $sancion->expediente !!}">                         
                        </div>
                        <div class="form-group col-md-2">
                            <label for="matricula">Boletín</label>
                            <input type="text" class="form-control" name="boletin" value="{!! $sancion->Boletin !!}">                            
                        </div>
                        <div class="form-group col-md-3">
                            <label for="matricula">Conductor</label>
                            <!--<input type="text" class="form-control" name="matricula" value="{!! $sancion->Conductor !!}">-->
                             <select class="selectpicker" data-live-search="true" title="Buscar..." id="conductor" name="conductor">
                                 <option selected>{!! $sancion->Conductor !!}</option>
                                    @foreach ($conductores as $conductor)
                                    <option>{{ $conductor->conductor}}</option>
                                    @endforeach
                            </select>                            
                        </div>
                         <div class="form-group col-md-2">
                            <label for="acuerdo">Acuerdo</label>
                            <input type="text" class="form-control" name="acuerdo" value="{!! $sancion->Acuerdo !!}">                            
                        </div>
                         <div class="form-group col-md-2">
                            <label for="ft">FT</label>
                            <input type="text" class="form-control" name="ft" value="{!! $sancion->CodigoFT !!}">                            
                        </div>
                    
                        <div class="form-group col-md-6">
                            <label for="matricula">Lugar</label>
                            <input type="text" class="form-control" name="lugar" value="{!! $sancion->Lugar !!}">                            
                        </div>
                         <div class="form-group col-md-3">
                            <label for="matricula">Autoridad</label>
                            <input type="text" class="form-control" name="autoridad" value="{!! $sancion->Autoridad !!}">                            
                        </div>
                        <div class="form-group col-md-2">
                            <label for="matricula">País</label>
                            <!--<input type="text" class="form-control" name="matricula" value="ESPAÑA">-->
                            <select class="selectpicker" data-live-search="true" title="Buscar..." id="pais" name="pais">
                            @foreach ($pais as $dato)
                             <option selected>{{$dato->Pais}}</option>
                            @endforeach
                                @foreach ($paises as $pais)
                                <option>{{ $pais->Pais}}</option>
                                @endforeach
                            </select> 
                        </div>
                        <div class="form-group col-md-2">
                                <label for="matricula">Importe</label>
                                <div class="input-group"><span class="input-group-addon">€</span>
                                <input type="text" class="form-control" name="importe" value="{{$sancion->Importe}}"></div>                
                        </div>
                        <div class="form-group col-md-2">
                                <label for="divisa">Divisa</label>
                                <!--<input type="text" class="form-control" name="divisa" value="{{$sancion->Moneda}}">-->
                                 <select class="selectpicker" data-live-search="true" title="Buscar..." id="divisas" name="divisas">
                                 <option selected>{{$sancion->Moneda}}</option>
                                    @foreach ($divisas as $divisa)
                                    <option>{{ $divisa->divisa}}</option>
                                    @endforeach
                            </select>                   
                        </div>
                        <div class="form-group col-md-2">
                                <label for="matricula">Cambio €</label>
                                <div class="input-group"><span class="input-group-addon">€</span>
                                <input type="text" class="form-control" name="cambio" value="{{round($sancion->Importe / $cambio,2)}} €" disabled>
                                </div>                   
                        </div>
                        <div class="form-group col-md-2">
                                <label for="matricula">Pagado ITS</label>
                                <div class="input-group"><span class="input-group-addon">€</span>
                                <input type="text" class="form-control" name="pagado" value="{{$sancion->Importepagado}}"></div>                   
                        </div>
                         <div class="form-group col-md-3">
                            <label for="matricula">Notas</label>
                            <textarea type="textarea" class="form-control" name="notas" style="" >{{$sancion->Notas}}</textarea>                         
                        </div>
                         <div class="form-group col-md-2">
                            <label for="matricula">Fecha Multa</label>
                            @if($sancion->FechaMulta!=null)
                            <input type="text" class="form-control datepicker" name="fecha_multa" value="{{ date('d/m/Y', strtotime($sancion->FechaMulta)) }}">
                            @else
                            <input type="text" class="form-control datepicker" name="fecha_multa" value="">
                            @endif
                        </div>
                         <div class="form-group col-md-2">
                            <label for="matricula">Fecha Entrada</label>
                            @if($sancion->FECHALLEGADA!=null)
                            <input type="text" class="form-control datepicker" name="fecha_entrada" value="{{ date('d/m/Y', strtotime($sancion->FECHALLEGADA)) }}">
                            @else
                            <input type="text" class="form-control datepicker" name="fecha_entrada" value="">
                            @endif                
                        </div>
                         <div class="form-group col-md-2">
                            <label for="matricula">Fecha Archivado</label>
                            @if($sancion->FECHACIERRE!=null)
                            <input type="text" class="form-control datepicker" name="fecha_archivado" value="{{ date('d/m/Y', strtotime($sancion->FECHACIERRE)) }}">
                            @else
                            <input type="text" class="form-control datepicker" name="fecha_archivado" value="">
                            @endif                         
                        </div>                        
                        <div class="form-group col-md-2">
                            <label for="gestionadogure">Gestionado</label>      
                            <!--<input type="checkbox" name="gestionadogure" value="{!! $sancion->GestionadoGURE !!}">-->
                            @if ($sancion->GestionadoGURE == 1)
                            <input type="checkbox" name="gestionadogure" id="gestionadogure" class="opcionales" checked>
                            @elseif ($sancion->GestionadoGURE == 0)
                            <input type="checkbox" name="gestionadogure" id="gestionadogure" class="opcionales">
                            @endif                                                           
                        </div>
                         <div class="form-group col-md-2">
                            <label for="estado">Estado</label> 
                            <!--<input type="checkbox" name="gestionadogure" value="{!! $sancion->Inactivo !!}">-->
                            @if ($sancion->Inactivo == 1)
                            <input type="checkbox" name="estado" id="estadosancion" checked>
                            <label style="background: red; display:block;color:white;width:100px;padding:inherit;" id="inactivo">INACTIVO</label>
                            @elseif ($sancion->Inactivo == 0)
                            <input type="checkbox" name="estado" id="estadosancion">
                            <label for="estado" id="inactivo" style="background: red; display:none;color:white;width:100px;padding:inherit">INACTIVO</label>
                            @endif                                                 
                        </div>
                         <div class="form-group col-md-2">
                            <label for="gestionadogure">Justif. Pago</label> 
                            <!--<input type="checkbox" name="gestionadogure" value="{!! $sancion->RecibidoJP !!}">-->
                             @if ($sancion->RecibidoJP  == 1)
                            <input type="checkbox" name="justif" id="justif" checked>
                            <label style="background: #f3f422; display:block;width:100px;padding:inherit;" id="labeljustif">RECIBIDO</label>
                            <input type="text" class="form-control datepicker" name="fecha_justif" id="fecha_justif" value="{{ date('d/m/Y', strtotime($sancion->fechaJustificante)) }}">
                            @elseif ($sancion->RecibidoJP  == 0)
                            <input type="checkbox" name="justif" id="justif">
                            <label for="estado" id="labeljustif" style="background: #f3f422; display:none;width:100px;padding:inherit">RECIBIDO</label>
                            <input type="text" class="form-control datepicker" name="fecha_justif" id="fecha_justif" style="display:none" value="{{ date('d/m/Y', strtotime($sancion->fechaJustificante)) }}">
                            @endif                                                     
                        </div>           
                    </form>
                    </div>                        
                </div>
            </div>
           <div class="panel panel-default">
              <div class="row"> 
                    <div class="panel-heading" id="detalles-header">                    
                        <div class="col-md-6">
                            <h3 class="module-title">Detalles</h3>
                        </div>
                        <div class="col-md-6">
                            <div>
                                <ul class="nav navbar-nav">
                                    <li class="detalles"><a id="motivos"><i> </i>Motivos</a></li>
                                    <li class="detalles"><a id= "tramites"><i> </i>Trámites</a></li>
                                    <li class="detalles"><a id="reducciones"><i> </i>Reducciones</a></li>
                                    <li class="detalles"><a id="contencioso"><i> </i>Contencioso</a></li>
                                    <li class="detalles"><a id="pendiente"><i> </i>Pendiente</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div id="error" style="display:none" class="alert alert-danger alert-dismissible fade in" role="alert"></div>
                    <div id="success" style="display:none" class="alert alert-success alert-dismissible"></div>
                     <div class="table-responsive" id="tabla_contenido">
                    <!--<table class="table">
                        <thead class="table-header">
                            <th>ID</th>                        
                            <th>Fecha</th>
                            <th>Motivo</th>
                            <th></th>
                        </thead>
                        <tbody id="myTable">
                         @foreach ($motivos as $motivo)
                        <tr>
                            <td>{{ $motivo->IdListaMotivos }} </td>
                            <td>{{ date('d - m - Y', strtotime($motivo->FechaModif)) }}</td>
                            <td>{{ $motivo->MOTIVO }} </td>                            
                            <td><a class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a></td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>-->
                </div>
                    </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

 $(document).on('focusin','.datepicker',function(){
         $(this).datepicker({
        format: "dd/mm/yyyy",
        dateFormat: 'yy-mm-dd',
        language: "es",
        autoclose: true
    });

         $(this).selectpicker("data-live-search","true");

    });

$("#eliminar").click(function(){
    bootbox.confirm({
        title: "Eliminar Sanción",
        message: "<i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> ¿Está seguro que desea eliminar la sanción?</strong> Tenga en cuenta que esta acción es irreversible.",
        buttons: {
            cancel: {
            label: '<i class="fa fa-times"></i> Cancelar'
        },
        confirm: {
            label: '<i class="fa fa-check"></i> Aceptar'
        }
        },
        callback: function (result) {
            if(result == true){
                $tabla = 'gesanc_sanciones';
                $field = "IDSancion";

                setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('sanciones/borrar')}}',
                        data : {'ID':$ID,'tabla': $tabla,'field':$field},            
                        success : function(data){
                            window.location = "/sanciones";
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);
            }
        }
    });

});

//eliminar el registro
  $(document).on('click','.btn-danger',function(){

    $ID = $(this).closest("tr").find('.ID').text();

    $IDSancion = $("#IDSancion").val();

bootbox.confirm({
     title: "Eliminar Registro",
    message: "<i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> ¿Está seguro que desea eliminar el registro?</strong> Tenga en cuenta que esta acción es irreversible.",
    buttons: {
        cancel: {
            label: '<i class="fa fa-times"></i> Cancelar'
        },
        confirm: {
            label: '<i class="fa fa-check"></i> Aceptar'
        }
    },
    callback: function (result) {

            if(result == true){
                
                if($("#tabla_tipo").val() == 'motivo'){
                $tabla = 'gesanc_lista_motivos_denuncia';
                $field = "IdListaMotivos";

                setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('sanciones/borrar')}}',
                        data : {'ID':$ID,'tabla': $tabla,'field':$field},            
                        success : function(data){
                            console.log(JSON.stringify(data));
                            $("#success").empty();
                            $("#success").append(data);
                            $("#success").show();
                            //bootbox.alert(data);
                            $("#loading").hide();
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);



                setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('sanciones/motivos')}}',
                        data : {'IDSancion':$IDSancion},            
                        success : function(data){
                            console.log(JSON.stringify(data));
                            $('#tabla_contenido').html(data);
                            $('.selectpicker').selectpicker('refresh');
                            $('.selectpicker').selectpicker('render');
                            $("#loading").hide();
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);
        
            }

            if($("#tabla_tipo").val() == 'reducciones'){
                $tabla = 'gesanc_reduccion';
                $field= '`ID-REDUCCION`';

                setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('sanciones/borrar')}}',
                data : {'ID':$ID,'tabla': $tabla,'field':$field},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $("#success").empty();
                    $("#success").append(data);
                    $("#success").show();
                    //bootbox.alert(data);
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);



        setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('sanciones/reducciones')}}',
                data : {'IDSancion':$IDSancion},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('#tabla_contenido').html(data);
                    $('.selectpicker').selectpicker('refresh');
                    $('.selectpicker').selectpicker('render');
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);
            }

             if($("#tabla_tipo").val() == 'tramites'){
                $tabla = 'gesanc_recursos';
                $field = "id_recurso";

                setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('sanciones/borrar')}}',
                data : {'ID':$ID,'tabla': $tabla,'field':$field},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $("#success").empty();
                    $("#success").append(data);
                    $("#success").show();
                    //bootbox.alert(data);
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);



        setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('sanciones/tramites')}}',
                data : {'IDSancion':$IDSancion},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('#tabla_contenido').html(data);
                    $('.selectpicker').selectpicker('refresh');
                    $('.selectpicker').selectpicker('render');
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);
               
            }

    
    }
    }
});

});



    $("#volver").click(function(){
        window.history.go(-1); return true;
    });

  $(document).ready(function() {

//Visualizar campos ocultos
$("#estadosancion").click(function(){


if($("#estadosancion").is(':checked')){
    $("#inactivo").css("display","");
}else{
    $("#inactivo").css("display","none");
}

});

$("#justif").click(function(){

if($("#justif").is(':checked')){
    $("#labeljustif").css("display","");
    $("#fecha_justif").css("display","");
}else{
    $("#labeljustif").css("display","none");
    $("#fecha_justif").css("display","none");
}

});


//botón editar
$(".bt-edit").click(function(){

    
    $ID = $("#IDSancion").val();

    $form = $("#form_sancion").serialize();
  
     setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('sanciones/'.$sancion->IDSancion.'/guardar')}}',
                data : {'datos' : $("#form_sancion").serialize()},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    bootbox.alert({
                            message: data,
                            callback: function () {
                                location.reload();
                            }
                        });
                    $("#loading").hide();

                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);



});

$(".bt-export").click(function(){

    
    $ID = $("#IDSancion").val();

    $codigosancion = $("#codigosancion").val();
    $empresa = $("#empresa").val();
    $cif = $("#cif").val();

  window.location = '/sanciones/'+$ID+'/exportar?codigosancion='+$codigosancion+'&empresa='+$empresa+'&cif='+$cif;
     /*setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('sanciones/'.$sancion->IDSancion.'/exportar')}}',
                data : {},            
                success : function(data){
                console.log(JSON.stringify(data));   
                window.location = '/sanciones/'+$ID+'/exportar?codigosancion='+$codigosancion;
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);*/



});





  $ID = $("#IDSancion").val();
  setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('sanciones/motivos')}}',
                data : {'IDSancion':$ID},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('#tabla_contenido').html(data);
                    $('.selectpicker').selectpicker('refresh');
                    $('.selectpicker').selectpicker('render');
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

            

});

    $('#reducciones').click(function(){
        $("#success").hide();
        $("#error").hide();
        //alert("reducciones");
        $ID = $("#IDSancion").val();
        setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('sanciones/reducciones')}}',
                data : {'IDSancion':$ID},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('#tabla_contenido').html(data);
                    $('.selectpicker').selectpicker('refresh');
                    $('.selectpicker').selectpicker('render');
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

    });

     $('#motivos').click(function(){
        $("#success").hide();
        $("#error").hide();
        //alert("reducciones");
        $ID = $("#IDSancion").val();
        setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('sanciones/motivos')}}',
                data : {'IDSancion':$ID},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('#tabla_contenido').html(data);
                    $('.selectpicker').selectpicker('refresh');
                    $('.selectpicker').selectpicker('render');
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

    });

     $('#contencioso').click(function(){
         $("#success").hide();
        $("#error").hide();
        //alert("reducciones");
        $ID = $("#IDSancion").val();
        setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('sanciones/contencioso')}}',
                data : {'IDSancion':$ID},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('#tabla_contenido').html(data);
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

    });

     $('#tramites').click(function(){
         $("#success").hide();
        $("#error").hide();
        //alert("reducciones");
        $ID = $("#IDSancion").val();
        setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('sanciones/tramites')}}',
                data : {'IDSancion':$ID},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('#tabla_contenido').html(data);
                    $('.selectpicker').selectpicker('refresh');
                    $('.selectpicker').selectpicker('render');
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

    });

     $('#pendiente').click(function(){
         $("#success").hide();
        $("#error").hide();
        //alert("reducciones");
        $ID = $("#IDSancion").val();
        setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('sanciones/pendiente')}}',
                data : {'IDSancion':$ID},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('#tabla_contenido').html(data);
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

    });

      

$('.detalles').on('click',function(){
    $('.detalles.active').removeClass('active');
    $(this).addClass('active');
}); 


$(document).on('click', '#add_motivo' ,function(){

$action = 1;
$motivo = $("#id_motivo option:selected").text();
$ID = $("#IDSancion").val();

if($motivo == "buscar..." || $motivo == ""){
    $("#error").empty();
    $("#error").append("<i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Se han encontrado errores.</strong> El campo del motivo no puede estar vacío.");
    $("#error").show();
}else{
    $("#error").hide();
    $("#success").hide();
    $("#success").empty();

     setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('sanciones/insertar')}}',
                data : {'action':$action,'IDSancion':$ID,'motivo':$motivo},
                success : function(data){
                    console.log(JSON.stringify(data));
                    $("#success").append(data);
                    $("#success").show();
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

     $("#motivos")[0].click();
}
});

$(document).on('click', '#add_tramite' ,function(){
$("#error").hide();
$("#error").empty();
$action = 2;
$tramite = $("#id_tramite option:selected").text();
$fecha = $("#fecha_tramite").val();
$fecha_tramite= $fecha.split("/").reverse().join("-");
$ID = $("#IDSancion").val();

if($tramite == "buscar..." || $tramite == "" || $fecha_tramite == ""){

    if($tramite == "buscar..." || $tramite == ""){
         $("#error").append("<i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Se han encontrado errores.</strong> El campo de trámite no puede estar vacío.<br>");
    }

    if($fecha_tramite == ""){
         $("#error").append("<i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Se han encontrado errores.</strong> El campo de fecha no puede estar vacío.<br>");
    }
    $("#error").show();
}else{
    $("#error").hide();
    $("#success").hide();
    $("#success").empty();

     setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('sanciones/insertar')}}',
                data : {'action':$action,'IDSancion':$ID,'tramite':$tramite,'fecha_tramite': $fecha_tramite},
                success : function(data){
                    console.log(JSON.stringify(data));
                    $("#success").append(data);
                    $("#success").show();
                    //bootbox.alert(data);
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

       setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('sanciones/tramites')}}',
                data : {'IDSancion':$ID},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('#tabla_contenido').html(data);
                    $('.selectpicker').selectpicker('refresh');
                    $('.selectpicker').selectpicker('render');
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);
}
});

$(document).on('click', '#add_reduccion' ,function(){
$("#error").hide();
$("#error").empty();
$action = 3;
$moneda = $("#id_moneda option:selected").text();
$fecha = $("#fecha_reduccion").val();
$fecha_reduccion= $fecha.split("/").reverse().join("-");
$importe = $("#importe").val();

$ID = $("#IDSancion").val();

if($moneda == "buscar..." || $moneda == "" || $fecha_reduccion == "" || $importe == ""){

    if($moneda == "buscar..." || $moneda == ""){
         $("#error").append("<i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Se han encontrado errores.</strong> El campo de la divisa no puede estar vacío.<br>");
    }

    if($fecha_reduccion == ""){
         $("#error").append("<i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Se han encontrado errores.</strong> El campo de fecha no puede estar vacío.<br>");
    }

    if($importe == ""){
         $("#error").append("<i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Se han encontrado errores.</strong> El campo de importe no puede estar vacío.<br>");
    }

    $("#error").show();
}else{
    $("#error").hide();
    $("#success").hide();
    $("#success").empty();

     setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('sanciones/insertar')}}',
                data : {'action':$action,'IDSancion':$ID,'importe':$importe,'fecha_reduccion': $fecha_reduccion, 'moneda': $moneda},
                success : function(data){
                    console.log(JSON.stringify(data));
                    $("#success").append(data);
                    $("#success").show();
                    //bootbox.alert(data);
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

       setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('sanciones/reducciones')}}',
                data : {'IDSancion':$ID},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('#tabla_contenido').html(data);
                    $('.selectpicker').selectpicker('refresh');
                    $('.selectpicker').selectpicker('render');
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);
}
});

$(document).on('click', '#save_contencioso' ,function(){
$("#error").hide();
$("#error").empty();
$action = 4;
$letrado = $("#Letrado").val();
$recurso = 0
$honorarios = 0;
$Viable = 0;
$fecha = $("#fechaEntrega").val();
$fechaEntrega= $fecha.split("/").reverse().join("-");
$importe = $("#importe").val();
$ID = $("#IDSancion").val();


if($('#honorarios').prop("checked") == true){
    $honorarios = 1;
}

if($('#Viable').prop("checked") == true){
    $Viable = 1;
}

if($('#Recurso').val()!= ""){
    $recurso = $('#Recurso').val();
}



if($letrado == "" || $fechaEntrega == ""){

    if($letrado == ""){
         $("#error").append("<i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Se han encontrado errores.</strong> El campo de letrado no puede estar vacío.<br>");
    }

    if($fechaEntrega == ""){
         $("#error").append("<i class='fa fa-exclamation-circle' aria-hidden='true'></i></span><strong> Se han encontrado errores.</strong> El campo de fecha no puede estar vacío.<br>");
    }

    $("#error").show();
}else{
    $("#error").hide();
    $("#success").hide();
    $("#success").empty();

     setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('sanciones/insertar')}}',
                data : {'action':$action,'IDSancion':$ID,'letrado':$letrado,'recurso': $recurso, 'fechaEntrega': $fechaEntrega, 'honorarios': $honorarios, 'viable': $Viable},
                success : function(data){
                    console.log(JSON.stringify(data));
                    $("#success").append(data);
                    $("#success").show();
                    //bootbox.alert(data);
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

       setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('sanciones/contencioso')}}',
                data : {'IDSancion':$ID},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('#tabla_contenido').html(data);
                    $('.selectpicker').selectpicker('refresh');
                    $('.selectpicker').selectpicker('render');
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);
}
});

$(document).on('click', '#save_pendiente' ,function(){
$("#error").hide();
$("#error").empty();
$action = 5;
$Notaspendiente = $("#Notaspendiente").val();
$marca_pdte = 0
$ID = $("#IDSancion").val();



$marca_pdte = $("input[name=Estado]:checked").val();

$("#success").hide();
$("#success").empty();

     setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('sanciones/insertar')}}',
                data : {'action':$action,'IDSancion':$ID,'Notaspendiente': $Notaspendiente, 'marca_pdte': $marca_pdte},
                success : function(data){
                    console.log(JSON.stringify(data));
                    $("#success").append(data);
                    $("#success").show();
                    //bootbox.alert(data);
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

       setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('sanciones/pendiente')}}',
                data : {'IDSancion':$ID},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('#tabla_contenido').html(data);
                    $('.selectpicker').selectpicker('refresh');
                    $('.selectpicker').selectpicker('render');
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

});

</script>

@endsection