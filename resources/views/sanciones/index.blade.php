@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-sm-2">
         
            <h4>Búsqueda</h4>           
            <!--<label for="search" id="loading" style="display:none;">
                <i class="fa fa-refresh fa-spin fa-1x fa-fw"></i>
            </label>-->
            <input type="text" class="form-control" id="search" name="search" placeholder="Buscar...">
            <h4>Filtros</h4>
            <div class =filtros>
            
                 <a data-toggle="collapse" href="#fechallegada" aria-expanded="false" aria-controls="fechallegada"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> fecha de llegada</h5></a>
                <div class="collapse" id="fechallegada">
                <input type="text" class="form-control datepicker" name="fecha_inicio" id="fecha_inicio" value="" placeholder="fecha de inicio...">
                <br>
                <input type="text" class="form-control datepicker" id="fecha_fin" value="" placeholder="fecha de fin...">
                </div>
            </div>
            <div class="filtros">
           
                <a data-toggle="collapse" href="#fechamulta" aria-expanded="false" aria-controls="fechamulta"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> fecha de Multa</h5></a>
                <div class="collapse" id="fechamulta">
                <input type="text" class="form-control datepicker" name="multa_inicio" id="multa_inicio" value="" placeholder="fecha de inicio...">
                <br>
                <input type="text" class="form-control datepicker" id="multa_fin" name="multa_fin" value="" placeholder="fecha de fin...">
                </div>
            </div>
            <div class="filtros">
                 <a data-toggle="collapse" href="#collapseexpe" aria-expanded="false" aria-controls="collapseexpe"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Expediente</h5></a>
                 <div class="collapse" id="collapseexpe">
             <input type="text" class="form-control" id="expediente" name="expediente" placeholder="Buscar...">
             </div>
            </div>
            <div class="filtros">
                 <a data-toggle="collapse" href="#collapseboletin" aria-expanded="false" aria-controls="collapseboletin"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Boletín</h5></a>
                 <div class="collapse" id="collapseboletin">
             <input type="text" class="form-control" id="boletin" name="boletin" placeholder="Buscar...">
             </div>
            </div>
              <div class =filtros>
                 <a data-toggle="collapse" href="#collapseempresa" aria-expanded="false" aria-controls="collapseempresa"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Empresa</h5></a>
                <div class="collapse" id="collapseempresa">
                <select class="selectpicker" data-live-search="true" title="Buscar..." id="empresa" name="empresa">
                 <option selected></option>
                    @foreach ($clientes as $cliente)
                    <option>{{ $cliente->empresa}}</option>
                    @endforeach
                </select>
                
                </div>
            </div>
            <div class="filtros">
                <a data-toggle="collapse" href="#collapseconductor" aria-expanded="false" aria-controls="collapseconductor"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Conductor</h5></a>
                 <div class="collapse" id="collapseconductor">
                 <select class="selectpicker" data-live-search="true" title="Buscar..." id="conductor" name="conductor">
                 <option selected></option>
                    @foreach ($conductores as $conductor)
                    <option>{{ $conductor->conductor}}</option>
                    @endforeach
                </select>
                </div>
            </div>
              <div class="filtros">
                 <a data-toggle="collapse" href="#collapsematricula" aria-expanded="false" aria-controls="collapsematricula"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Matricula</h5></a>
                <div class="collapse" id="collapsematricula">
                 <select class="selectpicker" data-live-search="true" title="Buscar..." id="matricula" name="matricula">
                 <option selected></option>
                    @foreach ($matriculas as $matricula)
                    <option>{{ $matricula->matricula}}</option>
                    @endforeach
                </select>
                </div>
            </div>
            <div class="filtros">
            <a data-toggle="collapse" href="#collapselugar" aria-expanded="false" aria-controls="collapselugar"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Lugar</h5></a>
            <div class="collapse" id="collapselugar">
            <input type="text" class="form-control" id="lugar" name="lugar" placeholder="Buscar...">
                </div>
            </div>
            <div class="filtros">
              <a data-toggle="collapse" href="#collapsepais" aria-expanded="false" aria-controls="collapsepais"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> País</h5></a>
              <div class="collapse" id="collapsepais">
              <select class="selectpicker" data-live-search="true" title="Buscar..." id="pais" name="pais">
                 <option selected></option>
                    @foreach ($paises as $pais)
                    <option>{{ $pais->Pais}}</option>
                    @endforeach
                </select>
                </div>
            </div>
            <div class="filtros">
                <a data-toggle="collapse" href="#collapseautoridad" aria-expanded="false" aria-controls="collapseautoridad"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Autoridad</h5></a>
                <div class="collapse" id="collapseautoridad">
              <select class="selectpicker form-control" data-live-search="true" title="Buscar..." id="autoridad">
                 <option selected></option>
                    @foreach ($autoridades as $autoridad)
                    <option>{{ $autoridad->Autoridad}}</option>
                    @endforeach
                </select>
                </div>
            </div>
             <div class="filtros">
                 <a data-toggle="collapse" href="#collapseimporte" aria-expanded="false" aria-controls="collapseimporte"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Importe</h5></a>
                 <div class="collapse" id="collapseimporte">
            <input type="text" class="form-control" id="importe" name="importe" placeholder="Buscar...">
            </div>
            </div>
             <div class="filtros">
                 <a data-toggle="collapse" href="#collapsepagado" aria-expanded="false" aria-controls="collapsepagado"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Pagado ITS</h5></a>
                 <div class="collapse" id="collapsepagado">
            <input type="text" class="form-control" id="pagadoITS" name="pagadoITS" placeholder="Buscar...">
            </div>
            </div>
             <div class="filtros">
                 <a data-toggle="collapse" href="#collapsecomercial" aria-expanded="false" aria-controls="collapsecomercial"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Comercial</h5></a>
                 <div class="collapse" id="collapsecomercial">
             <input type="text" class="form-control" id="Comercia" name="comercial" placeholder="Buscar...">
             </div>
            </div>
            <div class="filtros">
            <button type="button" id="filtrar" class="btn btn-default btn-primary">filtrar</button>
            </div>
        </div>
        <div class="col-sm-10">
            <div class="panel panel-default">
                <div class="row"> 
                    <div class="panel-heading" id="sanciones-header">                    
                        <div class="col-md-10">
                            <h3 class="module-title">Sanciones</h3>
                        </div>
                      
                    </div>
                </div>
                <div class="row"> 
                    <div class="panel-heading" id="sanciones-header">                    
                        <div class="col-md-10">
                            <div>
                                <ul class="list-inline">
                                    <li><a href="{{url('sanciones/create')}}"><i class="fa fa-plus"> </i> Añadir</a></li>
                                    <li><a href="#" id="new_motivo"><i class="fa fa-pencil-square-o"> </i> Motivos</a></li>
                                    <li><a href="#" id="new_tramite"><i class="fa fa-pencil-square-o"> </i> Tramites</a></li>
                                    <!--<li><a href="#"><i class="fa fa-archive"> </i> Expedientes</a></li>-->
                                    <li><a href="{{url('sanciones/choferes')}}"><i class="fa fa-car"> </i> Chóferes</a></li>
                                    <!--<li><a href="#"><i class="fa fa-file-pdf-o"> </i> PDF</a></li>-->
                                    <li><a href="{{url('sanciones/cesce')}}"><i class="fa fa-file-o"> </i> CESCE</a></li>
                                    <li><a href="#" id="excel"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Excel</a></li>
                                    <li><a href="#" id="informe_sanciones"><i class="fa fa-file-word-o" aria-hidden="true"></i></i> Informe sanciones</a></li>
                                </ul>
                            </div>
                        </div>
                        </div>
                    </div>
                <div class="table-responsive"">
                    <table class="table">
                        <thead class="table-header">
                            <th>Contrato</th>                        
                            <th>Expediente</th>
                            <th>Matrícula</th>
                            <th>CIF</th>
                            <th>Autoridad</th>
                            <th>Fecha Llegada</th>
                            <th></th>
                        </thead>
                        <tbody id="myTable">
                        @foreach ($sanciones as $sancion)
                        <tr>
                            <td>{{ $sancion->IDCONTRATO }} </td>
                            <td>{{ $sancion->expediente }} </td>
                            <td>{{ $sancion->Matricula }} </td>                            
                            <td>{{ $sancion->cif }} </td>
                            <td>{{ $sancion->Autoridad }} </td>
                            <td>{{ date('d - m - Y', strtotime($sancion->FECHALLEGADA)) }} </td>
                            <td><a class="btn btn-sm btn-success" href="{!! url('sanciones/'.$sancion->IDSancion) !!}"><i class="fa fa-edit"></i> </a> <!--<a class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>--></td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    
                </div>

                 <div class="col-md-12 text-center">
      <ul class="pagination pagination-lg pager" id="myPager"></ul>
      </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

 $('.datepicker').datepicker({
        format: "dd/mm/yyyy",
        dateFormat: 'yy-mm-dd',
        language: "es",
        autoclose: true
    });
    
    $('#search').on('keyup',function(){
        $value=$(this).val();
        //alert($value);
        $("#loading").show();
        setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('sanciones/search')}}',
                data : {'search':$value},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('tbody').html(data);
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);
});

$( "#filtrar" ).click(function() {
$fecha_inicio = $("#fecha_inicio").val();
$fecha_fin = $("#fecha_fin").val();

$autoridad = $("#autoridad option:selected").text();

$multa_inicio = $("#multa_inicio").val();

$multa_fin = $("#multa_fin").val();

$empresa =  $("#empresa option:selected").text();

$matricula =  $("#matricula option:selected").text();

$conductor = $("#conductor option:selected").text();

$pais = $("#pais option:selected").text();

if($pais=="España"){
    $pais= "Espana";
}

$lugar = $("#lugar").val();

$importe = $("#importe").val();

$pagadoITS = $("#pagadoITS").val();

$comercial = $("#comercial").val();

$expediente = $("#expediente").val();

$boletin = $("#boletin").val();



$inicio = $fecha_inicio.split("/").reverse().join("-");
$fin = $fecha_fin.split("/").reverse().join("-");

$multa_ini = $multa_inicio.split("/").reverse().join("-");

$multa_fn = $multa_fin.split("/").reverse().join("-");

//alert($inicio);

        $dialog = bootbox.dialog({
                        message: '<p class="text-center">Cargando datos, espere por favor...</p>',
                        closeButton: false
                    });

    setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('sanciones/filtrar')}}',
                data : {'fecha_inicio':$inicio,'fecha_fin': $fin,'autoridad':$autoridad,'multa_inicio': $multa_ini, 'multa_fin': $multa_fn, 'empresa': $empresa, 'conductor': $conductor, 'pais': $pais, 'lugar': $lugar, 'importe': $importe, 'pagadoITS': $pagadoITS, 'comercial': $comercial,'matricula':$matricula,'expediente':$expediente,'boletin':$boletin},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('tbody').html(data);

                    // do something in the background
                    $dialog.modal('hide');
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

});


$( "#new_motivo" ).click(function(){


bootbox.prompt("Añadir un nuevo motivo a la lista", function(result){
        if(result){

            if(result==""){
                bootbox.alert("Inserción Cancelada. No ha introducido ningún valor");
            }else{
                setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('sanciones/new_motivo')}}',
                        data : {'motivo': result},            
                        success : function(data){
                            console.log(JSON.stringify(data));
                             bootbox.alert({
                                    message: data,
                                    callback: function () {
                                        
                                    }
                                });
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);    
            }
        } 
    });

});

$( "#new_tramite" ).click(function(){

bootbox.prompt("Añadir un nuevo tramite a la lista", function(result){ 


    if(result){

            if(result==""){
                bootbox.alert("Inserción Cancelada. No ha introducido ningún valor");
            }else{
                setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('sanciones/new_tramite')}}',
                        data : {'tramite': result},            
                        success : function(data){
                            console.log(JSON.stringify(data));
                             bootbox.alert({
                                    message: data,
                                    callback: function () {
                                        
                                    }
                                });
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);    
            }
        } 

    });


});

$("#excel").click(function () {
    $fecha_inicio = $("#fecha_inicio").val();
$fecha_fin = $("#fecha_fin").val();

$autoridad = $("#autoridad option:selected").text();

$multa_inicio = $("#multa_inicio").val();

$multa_fin = $("#multa_fin").val();

$empresa =  $("#empresa option:selected").text();

$matricula =  $("#matricula option:selected").text();

$conductor = $("#conductor option:selected").text();

$pais = $("#pais option:selected").text();

if($pais=="España"){
    $pais= "Espana";
}

$lugar = $("#lugar").val();

$importe = $("#importe").val();

$pagadoITS = $("#pagadoITS").val();

$comercial = $("#comercial").val();

$expediente = $("#expediente").val();

$boletin = $("#boletin").val();

$inicio = $fecha_inicio.split("/").reverse().join("-");
$fin = $fecha_fin.split("/").reverse().join("-");

$multa_ini = $multa_inicio.split("/").reverse().join("-");

$multa_fn = $multa_fin.split("/").reverse().join("-");

//alert($inicio);

        $dialog = bootbox.dialog({
                        message: '<p class="text-center">Generando su Excel, espere por favor...</p>',
                        closeButton: false
                    });

    setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('sanciones/exportar_excel')}}',
                data : {'fecha_inicio':$inicio,'fecha_fin': $fin,'autoridad':$autoridad,'multa_inicio': $multa_ini, 'multa_fin': $multa_fn, 'empresa': $empresa, 'conductor': $conductor, 'pais': $pais, 'lugar': $lugar, 'importe': $importe, 'pagadoITS': $pagadoITS, 'comercial': $comercial,'matricula':$matricula,'expediente':$expediente,'boletin':$boletin},            
                success : function(data){
                    window.location = this.url;
                    $dialog.modal('hide');
                },
                error : function(data){
                    //console.log(JSON.stringify(data));
                    bootbox.alert("Ha ocurrido un error generando su Excel.");
                    $dialog.modal('hide');
                }
            });
        }, 500);

});

$("#informe_sanciones").click(function(){ 
 var loading = bootbox.dialog({
    message: '<p class="text-center"><p><i class="fa fa-spin fa-spinner"></i> Cargando interfaz, espere por favor...</p>',
    closeButton: false
});
// do something in the background


      setTimeout(function(){
                    $.ajax({
                        type : 'get',
                        url  : '{{URL::to('sanciones/mostrar_informe_sanciones')}}',
                        data : {},
                        success : function(data){
                            loading.modal('hide');
                           var dialog = bootbox.dialog({
                                    message: data,
                                    title: "Informe de Sanciones",
                                    buttons: {
                                        success: {
                                            label: 'generar',
                                             callback: function() {
                                            
                                                generar_informe();
                                                 }
                                        }
                                    }
                                });
                             $('.selectpicker').selectpicker('refresh');
                             $('.selectpicker').selectpicker('render');
                             $('.datepicker').datepicker({
                                format: "dd/mm/yyyy",
                                dateFormat: 'yy-mm-dd',
                                language: "es",
                                autoclose: true
                            });
                             
                        },
                        error : function(data){
                            console.log(JSON.stringify(data));
                        }
                    });
                }, 500);

});

function generar_informe(){
       $fecha_inicio = $("#fecha_inicio").val();
$fecha_fin = $("#fecha_fin").val();

$autoridad = $("#autoridad option:selected").text();

$multa_inicio = $("#multa_inicio").val();

$multa_fin = $("#multa_fin").val();

$empresa =  $("#empresa option:selected").text();

$matricula =  $("#matricula option:selected").text();

$conductor = $("#conductor option:selected").text();

$pais = $("#pais option:selected").text();

if($pais=="España"){
    $pais= "Espana";
}

$lugar = $("#lugar").val();

$importe = $("#importe").val();

$pagadoITS = $("#pagadoITS").val();

$comercial = $("#comercial").val();

$expediente = $("#expediente").val();

$boletin = $("#boletin").val();


$inicio = $fecha_inicio.split("/").reverse().join("-");
$fin = $fecha_fin.split("/").reverse().join("-");

$multa_ini = $multa_inicio.split("/").reverse().join("-");

$multa_fn = $multa_fin.split("/").reverse().join("-");

//alert($inicio);

        $dialog = bootbox.dialog({
                        message: '<p class="text-center">Generando su Excel, espere por favor...</p>',
                        closeButton: false
                    });

    setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('sanciones/generar_informe')}}',
                data : {'fecha_inicio':$inicio,'fecha_fin': $fin,'autoridad':$autoridad,'multa_inicio': $multa_ini, 'multa_fin': $multa_fn, 'empresa': $empresa, 'conductor': $conductor, 'pais': $pais, 'lugar': $lugar, 'importe': $importe, 'pagadoITS': $pagadoITS, 'comercial': $comercial,'matricula':$matricula,'expediente':$expediente,'boletin':$boletin},            
                success : function(data){
                    window.location = this.url;
                    $dialog.modal('hide');
                },
                error : function(data){
                    //console.log(JSON.stringify(data));
                    bootbox.alert("Ha ocurrido un error generando su informe.");
                    $dialog.modal('hide');
                }
            });
        }, 500);
}
</script>
@endsection