<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
use App\gesanc_sanciones;
use App\Http\Controllers\Auth\LoginController;

Auth::routes();


Route::get('/', 'SancionesController@index');

Route::get('/home', 'SancionesController@index');

Route::get('sanciones', 'SancionesController@index');

Route::get('sanciones/search','SancionesController@search');

Route::get('sanciones/filtrar','SancionesController@filtrar');

Route::get('sanciones/exportar_excel','SancionesController@exportar_excel');

Route::get('sanciones/mostrar_informe_sanciones','SancionesController@mostrar_informe_sanciones');

Route::get('sanciones/generar_informe','SancionesController@generar_informe');

Route::get('sanciones/create', 'SancionesController@create');

Route::get('sanciones/cesce', 'SancionesController@cesce');

Route::get('sanciones/buscar_contratos', 'SancionesController@buscar_contratos');

Route::get('sanciones/cargar_contratos', 'SancionesController@cargar_contratos');

Route::get('sanciones/choferes','SancionesController@choferes');

Route::get('sanciones/buscar_choferes', 'SancionesController@buscar_choferes');

Route::get('sanciones/gestion_chofer', 'SancionesController@gestion_chofer');

Route::get('sanciones/motivos','SancionesController@motivos');

Route::get('sanciones/tramites','SancionesController@tramites');

Route::get('sanciones/new_motivo','SancionesController@new_motivo');

Route::get('sanciones/new_tramite','SancionesController@new_tramite');

Route::get('sanciones/contencioso','SancionesController@contencioso');

Route::get('sanciones/pendiente','SancionesController@pendiente');

Route::get('sanciones/reducciones','SancionesController@reducciones');

Route::get('sanciones/insertar','SancionesController@insertar');

Route::get('sanciones/store','SancionesController@store');

Route::get('sanciones/borrar','SancionesController@borrar');

Route::get('sanciones/{id}','SancionesController@show');

Route::get('sanciones/{id}/guardar','SancionesController@update');

Route::get('sanciones/{id}/exportar','SancionesController@exportar');

Route::get('autocomplete', 'AutocompleteController@index');

Route::get('search/autocomplete', 'AutocompleteController@autocomplete');


//INTERVENCIONES 24 HR
Route::get('intervenciones','IntervencionesController@index');
Route::get('intervenciones/nuevo','IntervencionesController@create');
Route::get('intervenciones/check_contrato','IntervencionesController@check_contrato');
Route::get('intervenciones/check_cliente','IntervencionesController@check_cliente');
Route::get('intervenciones/store','IntervencionesController@store');
Route::get('intervenciones/filtrar','IntervencionesController@filtrar');
Route::get('intervenciones/exportar_excel','IntervencionesController@exportar_excel');
Route::get('intervenciones/mostrar_parte_multa','IntervencionesController@mostrar_parte_multa');
Route::get('intervenciones/imprimir_parte_multa','IntervencionesController@imprimir_parte_multa');
Route::get('intervenciones/mostrar_parte_cliente','IntervencionesController@mostrar_parte_cliente');
Route::get('intervenciones/imprimir_parte_cliente','IntervencionesController@imprimir_parte_cliente');
Route::get('intervenciones/mostrar_france_truck','IntervencionesController@mostrar_france_truck');
Route::get('intervenciones/imprimir_france_truck','IntervencionesController@imprimir_france_truck');
Route::get('intervenciones/mostrar_parte_tramites','IntervencionesController@mostrar_parte_tramites');
Route::get('intervenciones/imprimir_parte_tramites','IntervencionesController@imprimir_parte_tramites');
Route::get('intervenciones/recibos','IntervencionesController@recibos');
Route::get('intervenciones/tramites','IntervencionesController@tramites');
Route::get('intervenciones/motivos','IntervencionesController@motivos');
Route::get('intervenciones/add_tramite','IntervencionesController@add_tramite');
Route::get('intervenciones/insertar_tramite','IntervencionesController@insertar_tramite');
Route::get('intervenciones/editar_motivo','IntervencionesController@editar_motivo');
Route::get('intervenciones/add_motivo','IntervencionesController@add_motivo');
Route::get('intervenciones/eliminar_motivo','IntervencionesController@eliminar_motivo');
Route::get('intervenciones/editar_recibo','IntervencionesController@editar_recibo');
Route::get('intervenciones/eliminar_recibo','IntervencionesController@eliminar_recibo');
Route::get('intervenciones/add_recibo','IntervencionesController@add_recibo');
Route::get('intervenciones/imprimir_recibos','IntervencionesController@imprimir_recibos');
Route::get('intervenciones/impresion_recibos','IntervencionesController@impresion_recibos');
Route::get('intervenciones/{id}','IntervencionesController@show');
Route::get('intervenciones/{id}/update','IntervencionesController@update');


Route::get('/datepicker', function () {
    return view('datepicker');
});

Route::post('/save', ['as' => 'save-date',
                           'uses' => 'DateController@showDate', 
                            function () {
                                return '';
                            }]);




//Contratos EUROPA
Route::get('contratos/europa', 'EuropaController@europa');
Route::get('contratos/search_europa', 'EuropaController@search_europa');
Route::get('contratos/filtra_europa', 'EuropaController@filtra_europa');
Route::get('contratos/exportar_excel_europa', 'EuropaController@exportar_excel_europa');
Route::get('contratos/europa/mostrar_imprimir_contrato', 'EuropaController@mostrar_imprimir_contrato');
Route::get('contratos/europa/imprimir_contrato', 'EuropaController@imprimir_contrato');
Route::get('contratos/europa/ver_renovaciones', 'EuropaController@ver_renovaciones');
Route::get('contratos/europa/store', 'EuropaController@store_contrato');
Route::get('contratos/europa/store_anexo', 'EuropaController@store_anexo');
Route::get('contratos/europa/update_anexo', 'EuropaController@update_anexo');
Route::get('contratos/europa/create_anexo_remolque', 'EuropaController@create_anexo_remolque');
Route::get('contratos/europa/update_anexo_remolque', 'EuropaController@update_anexo_remolque');
Route::get('contratos/europa/eliminar_anexo_remolque', 'EuropaController@eliminar_anexo_remolque');
Route::get('contratos/europa/mostrar_sustitucion', 'EuropaController@mostrar_sustitucion');
Route::get('contratos/europa/sustitucion', 'EuropaController@sustitucion');
Route::get('contratos/europa/mostrar_sustitucion_remolque', 'EuropaController@mostrar_sustitucion_remolques');
Route::get('contratos/europa/sustitucion_remolque', 'EuropaController@sustitucion_remolque');
Route::get('contratos/europa/editar_liquidacion', 'EuropaController@editar_liquidacion');
Route::get('contratos/europa/add_liquidacion', 'EuropaController@add_liquidacion');
Route::get('contratos/europa/editar_abono', 'EuropaController@editar_abono');
Route::get('contratos/europa/add_abono', 'EuropaController@add_abono');
Route::get('contratos/europa/eliminar_abono', 'EuropaController@eliminar_abono');
Route::get('contratos/europa/eliminar_anexo_remolque', 'EuropaController@eliminar_anexo_remolque');

//contactos
Route::get('contratos/europa/contactos', 'ContactosController@europa');
Route::get('contratos/europa/contactos/nuevo','ContactosController@create_europa');
Route::get('contratos/europa/contactos/store','ContactosController@store_europa');
Route::get('contratos/europa/contactos/filtra_europa', 'ContactosController@filtra_europa');
Route::get('contratos/europa/contactos/generar_excel_contactos', 'ContactosController@generar_excel_contactos');
Route::get('contratos/europa/contactos/insertar_europa','ContactosController@insertar_europa');
Route::get('contratos/europa/contactos/guardar_europa','ContactosController@guardar_europa');
Route::get('contratos/europa/contactos/eliminar_europa','ContactosController@eliminar_europa');
Route::get('contratos/europa/contactos/telefono','ContactosController@telefonos_europa');
Route::get('contratos/europa/contactos/direccion','ContactosController@direcciones_europa');
Route::get('contratos/europa/contactos/funciones','ContactosController@funciones_europa');
Route::get('contratos/europa/contactos/{id}', 'ContactosController@show_europa');
Route::get('contratos/europa/contactos/{id}/guardar', 'ContactosController@update_europa');

//anexos
Route::get('contratos/europa/ver_anexos', 'EuropaController@ver_anexos');
Route::get('contratos/europa/filtra_anexo', 'EuropaController@filtra_anexo');
Route::get('contratos/europa/generar_excel_anexos', 'EuropaController@generar_excel_anexos');
Route::get('contratos/europa/tipos_anexo', 'EuropaController@mostrar_tipos_anexos');
Route::get('contratos/europa/guardar_tipo_anexo', 'EuropaController@guardar_tipo_anexo');
Route::get('contratos/europa/store_tipo_anexo', 'EuropaController@store_tipo_anexo');


//franquicias
Route::get('contratos/europa/franquicias', 'EuropaController@franquicias');
Route::get('contratos/europa/franquicias/precios', 'EuropaController@precios');
Route::get('contratos/europa/franquicias/comerciales', 'EuropaController@comerciales');
Route::get('contratos/europa/franquicias/guardar', 'EuropaController@update_franquicia');
Route::get('contratos/europa/franquicias/insertar_precio', 'EuropaController@insertar_franquicia');
Route::get('contratos/europa/franquicias/insertar_comercial', 'EuropaController@insertar_franquicia');
Route::get('contratos/europa/franquicias/update_precio', 'EuropaController@update_precio');
Route::get('contratos/europa/franquicias/nuevo', 'EuropaController@nueva_franquicia');
Route::get('contratos/europa/franquicias/store', 'EuropaController@store_franquicia');
Route::get('contratos/europa/franquicias/eliminar_franquicia', 'EuropaController@eliminar_franquicia');
Route::get('contratos/europa/franquicias', 'EuropaController@franquicias');
Route::get('contratos/europa/franquicias/{id}', 'EuropaController@show_franquicias');

//recibos
Route::get('contratos/europa/mostrar_generar_recibos', 'EuropaController@mostrar_generar_recibos');
Route::get('contratos/europa/get_importe_recibo', 'EuropaController@get_importe_recibo');
Route::get('contratos/europa/generar_recibos', 'EuropaController@generar_recibos');
Route::get('contratos/europa/mostrar_sustitucion_recibos', 'EuropaController@mostrar_sustitucion_recibos');
Route::get('contratos/europa/sustituir_recibos', 'EuropaController@sustituir_recibos');
Route::get('contratos/europa/mostrar_devolucion_recibo', 'EuropaController@mostrar_devolucion_recibo');
Route::get('contratos/europa/devolver_recibo', 'EuropaController@devolver_recibo');
Route::get('contratos/europa/mostrar_anular_recibo', 'EuropaController@mostrar_anular_recibo');
Route::get('contratos/europa/anular_recibo', 'EuropaController@anular_recibo');
Route::get('contratos/europa/update_recibo', 'EuropaController@update_recibo');
Route::get('contratos/europa/imprimir_recibos', 'EuropaController@imprimir_recibos');
Route::get('contratos/europa/impresion_recibos', 'EuropaController@impresion_recibos');

//contratos
Route::get('contratos/europa/{ID}/guardar', 'EuropaController@update_contrato');
Route::get('contratos/europa/recibos', 'EuropaController@recibos');
Route::get('contratos/europa/liquidaciones', 'EuropaController@liquidaciones');
Route::get('contratos/europa/abonos', 'EuropaController@abonos');
Route::get('contratos/europa/anexos', 'EuropaController@anexos');
Route::get('contratos/europa/mostrar_recibo_suelto', 'EuropaController@mostrar_recibo_suelto');
Route::get('contratos/europa/generar_recibo_suelto', 'EuropaController@generar_recibo_suelto');
Route::get('contratos/europa/mostrar_desinmovilizacion', 'EuropaController@mostrar_desinmovilizacion');
Route::get('contratos/europa/generar_desinmovilizacion', 'EuropaController@generar_desinmovilizacion');
Route::get('contratos/europa/informe_baja_matriculas', 'EuropaController@informe_baja_matriculas');
Route::get('contratos/europa/informe_cambio_matriculas', 'EuropaController@informe_cambio_matriculas');
Route::get('contratos/europa/clonar_gesanc', 'EuropaController@clonar_gesanc');
Route::get('contratos/europa/anexos/{id}', 'EuropaController@show_anexos');
Route::get('contratos/europa/nuevo', 'EuropaController@create');
Route::get('contratos/europa/precio_anexo', 'EuropaController@precio_anexo');
Route::get('contratos/europa/{id}', 'EuropaController@show_europa');
Route::get('contratos/europa/{id}/crear_anexo', 'EuropaController@create_anexo');




//contactos gesanc
Route::get('contratos/gesanc/contactos','Contactoscontroller@gesanc');
Route::get('contratos/gesanc/contactos/exportar_excel','Contactoscontroller@exportar_excel');
Route::get('contratos/gesanc/contactos/filtrar','Contactoscontroller@filtra_gesanc');
Route::get('contratos/gesanc/contactos/guardar','Contactoscontroller@update_gesanc');
Route::get('contratos/gesanc/contactos/eliminar','Contactoscontroller@eliminar_gesanc');
Route::get('contratos/gesanc/contactos/{id}','Contactoscontroller@show_gesanc');

//contratos GESANC
Route::get('contratos/gesanc', 'ContratosController@gesanc');
Route::get('contratos/gesanc/filtra_gesanc','ContratosController@filtra_gesanc');
Route::get('contratos/gesanc/mostrar_codigos','ContratosController@mostrar_codigos');
Route::get('contratos/gesanc/buscar_matricula','ContratosController@buscar_matricula');
Route::get('contratos/gesanc/imprimir_codigos_matricula','ContratosController@imprimir_codigos_matricula');
Route::get('contratos/gesanc/exportar_excel','ContratosController@exportar_excel');
Route::get('contratos/gesanc/mostrar_renovaciones','ContratosController@mostrar_renovaciones');
Route::get('contratos/gesanc/generar_renovaciones','ContratosController@generar_renovaciones');

Route::get('contratos/gesanc/guardar','ContratosController@update_gesanc');
Route::get('contratos/gesanc/eliminar','ContratosController@eliminar_gesanc');
Route::get('contratos/gesanc/desactivar_sanciones','ContratosController@desactivar_sanciones');
Route::get('contratos/gesanc/activar_sanciones','ContratosController@activar_sanciones');
Route::get('contratos/gesanc/{id}','ContratosController@show_gesanc');






//contratos ITS
Route::get('contratos/borrar', 'ContratosController@borrar');

Route::get('contratos/gesanc', 'ContratosController@gesanc');
Route::get('contratos/store', 'ContratosController@store');
Route::get('contratos/its', 'ContratosController@its');
Route::get('contratos/its/nuevo', 'ContratosController@nuevo_its');

Route::get('contratos/its/cheques','ContratosController@cheques');
Route::get('contratos/its/cheques/guardar','ContratosController@guardar_cheque');
Route::get('contratos/its/cheques/buscar','ContratosController@search_cheques');
Route::get('contratos/its/cheques/{id}','ContratosController@show_cheque');
Route::get('contratos/its/{id}/guardar', 'ContratosController@update_its');

Route::get('contratos/search_its','ContratosController@search_its');
Route::get('contratos/filtra_its','ContratosController@filtra_its');
Route::get('contratos/its/exportar_excel_its','ContratosController@exportar_excel_its');
Route::get('contratos/its/newcobertura','ContratosController@newcobertura_its');
Route::get('contratos/its/addcobertura','ContratosController@store_cobertura');
Route::get('contratos/its/newtipo',function () {
    return view('contratos/newtipo');
});



Route::get('contratos/tarjetas','ContratosController@tarjetas');
Route::get('contratos/tarjetas/buscar','ContratosController@search_tarjetas');
Route::get('contratos/tarjetas/exportar_excel_tarjetas','ContratosController@exportar_excel_tarjetas');
Route::get('contratos/tarjetas/nueva','ContratosController@nueva_tarjeta');
Route::get('contratos/tarjetas/store_tarjeta','ContratosController@store_tarjeta');

Route::get('contratos/tarjetas/{id}','ContratosController@show_tarjetas');
Route::get('contratos/tarjetas/{id}/guardar','ContratosController@update_tarjeta');




Route::get('contratos/its/addtipo','ContratosController@store_tipo');

Route::get('contratos/its/contactos','ContactosController@index_its');
Route::get('contratos/its/contactos/exportar_excel_its','ContactosController@exportar_excel_its');
Route::get('contratos/its/contactos/nuevo','ContactosController@create_its');
Route::get('contratos/its/contactos/store','ContactosController@store_its');
Route::get('contratos/its/contactos/telefono','ContactosController@telefonos_its');
Route::get('contratos/its/contactos/direccion','ContactosController@direcciones_its');
Route::get('contratos/its/contactos/funciones','ContactosController@funciones_its');
Route::get('contratos/its/contactos/tarjetas','ContactosController@tarjetas_its');
Route::get('contratos/its/contactos/insertar_its','ContactosController@insertar_its');
Route::get('contratos/its/contactos/exportar_sobre','ContactosController@exportar_sobre');
Route::get('contratos/its/contactos/{id}','ContactosController@show_its');
Route::get('contratos/its/contactos/{id}/guardar','ContactosController@update_its');


Route::get('contratos/its/contratos/search_its','ContactosController@search_its');

Route::get('contratos/insertaranexo','ContratosController@insertar');
Route::get('contratos/borrar_anexo','ContratosController@borrar_anexo');
Route::get('contratos/its/anexos/{id}', 'ContratosController@show_anexos_its');
Route::get('contratos/its/anexos/{id}/guardar', 'ContratosController@update_anexo');

Route::get('contratos/its/{id}','ContratosController@show_its');

Route::get('contratos/its/{id}/nuevoanexo', 'ContratosController@nuevoanexo_its');

//facturas EUROPA

Route::get('facturas/europa','FacturasController@index_europa');
Route::get('facturas/europa/mostrar_decontrato_europa','FacturasController@mostrar_decontrato_europa');
Route::get('facturas/europa/mostrar_deprovision_europa','FacturasController@mostrar_deprovision_europa');
Route::get('facturas/europa/generar_factura_decontrato','FacturasController@generar_factura_decontrato');
Route::get('facturas/europa/exportar_factura', 'FacturasController@exportar_factura');
Route::get('facturas/europa/exportar_sobre', 'FacturasController@exportar_sobre');
Route::get('facturas/europa/facturas_origen','FacturasController@mostrar_origen');
Route::get('facturas/europa/nuevo','FacturasController@nuevo_europa');
Route::get('facturas/europa/insertar','FacturasController@store_europa');
Route::get('facturas/europa/editar','FacturasController@editar_europa');
Route::get('facturas/europa/mostrar_pte_liquidar','FacturasController@mostrar_pte_liquidar');
Route::get('facturas/europa/mostrar_liquidadas','FacturasController@mostrar_liquidadas');
Route::get('facturas/europa/mostrar_aplicacion','FacturasController@mostrar_aplicacion');
Route::get('facturas/europa/insertar_aplicacion','FacturasController@insertar_aplicacion');
Route::get('facturas/europa/generar_factura_deprovision','FacturasController@generar_factura_deprovision');
Route::get('facturas/europa/eliminar_detalle_europa','FacturasController@eliminar_detalle_europa');
Route::get('facturas/europa/editar_detalle','FacturasController@editar_detalle_europa');
Route::get('facturas/europa/insertar_detalle','FacturasController@insertar_detalle_europa');
Route::get('facturas/europa/eliminar','FacturasController@destroy_europa');
Route::get('facturas/europa/filtra_europa','FacturasController@filtra_europa');
Route::get('facturas/europa/exportar_excel_europa','FacturasController@exportar_excel_europa');
Route::get('facturas/europa/{id}','FacturasController@show_europa');

//facturas its
Route::get('facturas/its','FacturasController@index_its');
Route::get('facturas/its/insertar','FacturasController@store_its');
Route::get('facturas/its/editar','FacturasController@editar_its');
Route::get('facturas/its/refrescar_detalles','FacturasController@refrescar_detalles');
Route::get('facturas/its/eliminar_detalle','FacturasController@eliminar_detalle_its');
Route::get('facturas/its/editar_detalle','FacturasController@editar_detalle_its');
Route::get('facturas/its/insertar_detalle','FacturasController@insertar_detalle_its');
Route::get('facturas/its/exportar_factura_its', 'FacturasController@exportar_factura_its');
Route::get('facturas/its/eliminar','FacturasController@destroy_its');
Route::get('facturas/its/filtra_its','FacturasController@filtra_its');
Route::get('facturas/its/{id}','FacturasController@show_its');
Route::get('facturas/its/nuevo/{clase}','FacturasController@nuevo_its');


